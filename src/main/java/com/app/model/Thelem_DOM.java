package com.app.model;

import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="thelem", schema="scrap")
public class Thelem_DOM {

	
	@Id
	@SequenceGenerator(name="scrap.thelem_id_seq",
	                    sequenceName ="scrap.thelem_id_seq",
	                     allocationSize=1)
    @GeneratedValue( strategy=GenerationType.SEQUENCE,
                     generator="scrap.thelem_id_seq")
	  
	@Column(name="id",updatable=false)
	private int id;
	
	@Column(name="nbagences", length=4)
	private String nbAgences;
	
	@Column(name="lien_agence",  columnDefinition="varchar")
	private String lienAgence;
	
	@Column(name="nom_agence",  columnDefinition="varchar")
	private String nomAgence;
	
	@Column(name="adresse",  columnDefinition="varchar")
	private String adresse;
	
	@Column(name="tel",  columnDefinition="varchar")
	private String tel;
	
	@Column(name="fax",  columnDefinition="varchar")
	private String fax;
	
	@Column(name="noms_agents",  columnDefinition="varchar")
	private String nomsAgents;
	
	@Column(name="orias", length=10)
	private String orias;
	
	@Column(name="horaires",  columnDefinition="varchar")
	private String horaires;
	
	@Column(name="agencehtm", columnDefinition="varchar")
	private String agenceHtm;
	
	@Column(name="annee_du_scrap" ,columnDefinition="varchar(4) DEFAULT date_part('year',CURRENT_DATE)")
	private String anneeScrap;

	public Thelem_DOM() {
		LocalDate ld = new Date().toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
		anneeScrap = String.valueOf(ld.getYear() + 1);
	}
	public int getId() {
		return id;
	}

	public Thelem_DOM setId(int id) {
		this.id = id;
		return this;
	}

	public String getNbAgences() {
		return nbAgences;
	}

	public Thelem_DOM setNbAgences(String nbAgences) {
		this.nbAgences = nbAgences;
		return this;
	}

	public String getLienAgence() {
		return lienAgence;
	}

	public Thelem_DOM setLienAgence(String lienAgence) {
		this.lienAgence = lienAgence;
		return this;
	}

	public String getNomAgence() {
		return nomAgence;
	}

	public Thelem_DOM setNomAgence(String nomAgence) {
		this.nomAgence = nomAgence;
		return this;
	}

	public String getAdresse() {
		return adresse;
	}

	public Thelem_DOM setAdresse(String adresse) {
		this.adresse = adresse;
		return this;
	}

	public String getTel() {
		return tel;
	}

	public Thelem_DOM setTel(String tel) {
		this.tel = tel;
		return this;
	}

	public String getFax() {
		return fax;
	}

	public Thelem_DOM setFax(String fax) {
		this.fax = fax;
		return this;
	}

	public String getNomsAgents() {
		return nomsAgents;
	}

	public Thelem_DOM setNomsAgents(String nomsAgents) {
		this.nomsAgents = nomsAgents;
		return this;
	}

	public String getOrias() {
		return orias;
	}

	public Thelem_DOM setOrias(String orias) {
		this.orias = orias;
		return this;
	}

	public String getHoraires() {
		return horaires;
	}

	public Thelem_DOM setHoraires(String horaires) {
		this.horaires = horaires;
		return this;
	}

	public String getAgenceHtm() {
		return agenceHtm;
	}

	public Thelem_DOM setAgenceHtm(String agenceHtm) {
		this.agenceHtm = agenceHtm;
		return this;
	}

	public String getAnneeScrap() {
		return anneeScrap;
	}

	public Thelem_DOM setAnneeScrap(String anneeScrap) {
		this.anneeScrap = anneeScrap;
		return this;
	}
}
