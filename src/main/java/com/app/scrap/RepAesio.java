package com.app.scrap;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.app.model.Aesio;

@Repository
public interface RepAesio extends JpaRepository<Aesio, Integer>,RepositoryCustom<Aesio> {

}
