package com.app.service;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;

import java.io.IOException;
import java.net.SocketTimeoutException;
import java.text.Normalizer;
import java.util.Date;
import java.util.Scanner;

import org.json.JSONObject;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.app.model.BTP;
import com.app.scrap.RepBTP;
import com.app.utils.Parametres;
import com.app.utils.Utilitaires;

import fa.browserUtils.FactoryBrowser;
import fa.browserUtils.HtmlUnitBrowser;
import fa.log.LogApp.LogScrap;
import fa.log.config.LogConst;

@Service
public class ServiceBTP extends Utilitaires {

	@Autowired
	RepBTP repBTP;

	private HtmlUnitDriver hu = null;
	private String nomSite = "BTP";
	private String ficVal = "lien_btp_banque.txt";
	private String url = "https://agences.btp-banque.fr/";

	private int nbRecuperes;

	private boolean saveItem(String type, String free, String val, String name, String address, String schedule,
			String tel) {
		BTP t = new BTP();
		t.setValSoumise(val).setAgenceType(type).setFreeService(free).setName(name).setAddress(address)
				.setSchedule(schedule).setTel(tel);
		boolean b = (repBTP.save(t) != null) ? true : false;
		return b;
	}

	public void setup() {
		FactoryBrowser factB = new FactoryBrowser();
		HtmlUnitBrowser hb = (HtmlUnitBrowser) factB.create("htmlUnit");
		hu = hb.setOptions(true, false, false, 12000).initBrowserDriver();
	}

	public void logout() {
		hu.quit();
	}

	public void scrpBTP() throws IOException {

		String resultFolderName = Parametres.REPERTOIRE_RESULT + "\\BQ\\" + nomSite;
		Scanner sc = new Scanner(
				new BufferedInputStream(new FileInputStream(new File(Parametres.REPERTOIRE_DATA_SOURCES + ficVal))));
		if (!new File(resultFolderName).exists())
			new File(resultFolderName).mkdirs();

		LogScrap log = new LogScrap("BanqueBTPLog.txt", LogConst.TYPE_LOG_FILE);
		int currentLine = ((new File(LogConst.STORE_LOG_PATH + LogConst.PREFIX_LOG_FILE + "_BanqueBTPLog.txt"))
				.exists()) ? Integer.parseInt(log.readLog().getLastLineProcessed().split("\t")[4]) : 0;

		int counter = 1;
		int l = 0;
		while (sc.hasNextLine()) {

			String path = sc.nextLine();

			if (counter > currentLine) {

				String completeURL = url + path;
				System.out.println(completeURL);

				try {
					hu.get(completeURL);
				} catch (Exception e) {
					if (e instanceof SocketTimeoutException) {

						this.setup();
						hu.get(completeURL);
					}
				}

				String pageSource = hu.getPageSource();
				Document doc = Jsoup.parse(pageSource);

				Element root = doc.selectFirst("body > script");

				String agencys[] = {};

				if (root.html().toString().contains("AddPoi")) {

					agencys = Normalizer.normalize(root.html().toString(), Normalizer.Form.NFD)
							.replaceAll("[^\\p{ASCII}]", "").split("AddPoi");
				} else {
					System.out.println("Aucun agence trouve");
				}

				nbRecuperes = 0;

				l = agencys.length;

				JSONObject scheduleJSON = null;
				JSONObject rdvScheduleJSON = null;
				JSONObject jsonDatas = null;
				JSONObject json = null;

				int NbAgences = l;

				if (l > 0) {
					String nomAgence = "", zipCode = "", city = "", adresse = "", postalAdress = "", tel = "", fax = "",
							horairesTXT = "", agenceType = "", libreService = "";

					for (int i = 0; i < l - 1; i++) {
						nomAgence = "";
						zipCode = "";
						city = "";
						adresse = "";
						tel = "";
						fax = "";
						horairesTXT = "";
						agenceType = "";
						libreService = "";
						String strJSON = agencys[i + 1].replaceAll("[\\(\\)]", "")
								.replaceAll("^['0-9,]{1,}\\{\"datas\"", "");
						strJSON = "{\"datas\"" + strJSON;

						json = new JSONObject(strJSON);
						jsonDatas = json.getJSONObject("datas");

						nomAgence = (jsonDatas.has("label")) ? jsonDatas.getString("label") : "";
						nomAgence= this.normalize(nomAgence);
						System.out.println("le nom de l'agence " + nomAgence);

						zipCode = (jsonDatas.has("zip")) ? jsonDatas.getString("zip") : "";
						city = (jsonDatas.has("city")) ? jsonDatas.getString("city") : "";
						adresse = (jsonDatas.has("address1"))
								? jsonDatas.getString("address1") + " " + zipCode + " " + city
								: "";
						adresse= this.normalize(adresse).replaceAll(",","").toUpperCase();
						System.out.println("l'adresse est " + adresse);

						postalAdress = (jsonDatas.has("postalAddress")) ? jsonDatas.getString("postalAddress") : "";
						System.out.println("postalAdress " + postalAdress);

						tel = (jsonDatas.has("tel")) ? jsonDatas.getString("tel") : "";
						System.out.println("tel " + tel);

						fax = (jsonDatas.has("fax")) ? jsonDatas.getString("fax") : "";
						System.out.println("fax" + fax);

						agenceType = (jsonDatas.has("agenceType")) ? jsonDatas.getString("agenceType") : "";
						System.out.println("agence type" + agenceType);

						libreService = (jsonDatas.has("libreservice")) ? jsonDatas.getString("libreservice") : "";
						System.out.println("libreservice" + libreService);

						scheduleJSON = (jsonDatas.has("schedule")) ? jsonDatas.getJSONObject("schedule") : null;
						rdvScheduleJSON = (jsonDatas.has("rdvSchedules")) ? jsonDatas.getJSONObject("rdvSchedules") : null;
						
						String lundi = scheduleJSON.getString("day1").toLowerCase().contains("h") ? scheduleJSON.getString("day1") : "";
						String mardi = scheduleJSON.getString("day2").toLowerCase().contains("h") ? scheduleJSON.getString("day2") : "";
						String mercredi = scheduleJSON.getString("day3").toLowerCase().contains("h") ? scheduleJSON.getString("day3") : "";
						String jeudi = scheduleJSON.getString("day4").toLowerCase().contains("h") ? scheduleJSON.getString("day4") : "";
						String vendredi = scheduleJSON.getString("day5").toLowerCase().contains("h") ? scheduleJSON.getString("day5") : "";
						String samedi = scheduleJSON.getString("day6").toLowerCase().contains("h") ? scheduleJSON.getString("day6") : "";
						String dimanche = scheduleJSON.getString("day7").toLowerCase().contains("h") ? scheduleJSON.getString("day7") : "";
						
						String lundiRdv = rdvScheduleJSON.getString("day1").toLowerCase().contains("h") ? " " + rdvScheduleJSON.getString("day1") + " sur RDV" : "";
						String mardiRdv = rdvScheduleJSON.getString("day2").toLowerCase().contains("h") ? " " + rdvScheduleJSON.getString("day2") + " sur RDV" : "";
						String mercrediRdv = rdvScheduleJSON.getString("day3").toLowerCase().contains("h") ? " " + rdvScheduleJSON.getString("day3") + " sur RDV" : "";
						String jeudiRdv = rdvScheduleJSON.getString("day4").toLowerCase().contains("h") ? " " + rdvScheduleJSON.getString("day4") + " sur RDV" : "";
						String vendrediRdv = rdvScheduleJSON.getString("day5").toLowerCase().contains("h") ? " " + rdvScheduleJSON.getString("day5") + " sur RDV" : "";
						String samediRdv = rdvScheduleJSON.getString("day6").toLowerCase().contains("h") ? " " + rdvScheduleJSON.getString("day6") + " sur RDV" : "";
						String dimancheRdv = rdvScheduleJSON.getString("day7").toLowerCase().contains("h") ? " " + rdvScheduleJSON.getString("day7") + " sur RDV" : "";
						
						lundi = (lundi + lundiRdv).length() > 0 ? lundi + lundiRdv:"Fermee";
						mardi = (mardi + mardiRdv).length() > 0 ? mardi + mardiRdv:"Fermee";
						mercredi = (mercredi + mercrediRdv).length() > 0 ? mercredi + mercrediRdv:"Fermee";
						jeudi = (jeudi + jeudiRdv).length() > 0 ? jeudi + jeudiRdv:"Fermee";
						vendredi = (vendredi + vendrediRdv).length() > 0 ? vendredi + vendrediRdv:"Fermee";
						samedi = (samedi + samediRdv).length() > 0 ? samedi + samediRdv:"Fermee";
						dimanche = (dimanche + dimancheRdv).length() > 0 ? dimanche + dimancheRdv:"Fermee";
						
						if (scheduleJSON != null) {
							horairesTXT = "lundi " + lundi + " mardi "
									+ mardi + " mercredi " + mercredi
									+ " jeudi " + jeudi + " vendredi "
									+ vendredi + " samedi " + samedi + " dimanche " + dimanche;
						}
						
						horairesTXT= this.normalize(horairesTXT);
						
						System.out.println("HorairesTXT : " + horairesTXT);

						this.saveItem(agenceType, libreService, path, nomAgence, adresse, horairesTXT, tel);

						nbRecuperes++;

					}
				}
				log.writeLog(new Date(), path, "no_error", l, counter, 0);
			}
			counter++;

		}

	}
}
