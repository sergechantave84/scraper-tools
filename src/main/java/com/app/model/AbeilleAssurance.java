package com.app.model;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="abeille_assurance",schema="scrap")
public class AbeilleAssurance {

	@Id
	@SequenceGenerator(name="scrap.abeille_assurance_id_seq",
	                     sequenceName ="scrap.abeille_assurance_id_seq",
	                     allocationSize=1)
    @GeneratedValue( strategy=GenerationType.SEQUENCE,
                     generator="scrap.abeille_assurance_id_seq")
	  
	@Column(name="id",updatable=false)
	private int id;
	
	@Column(name="departement_soumise", columnDefinition="varchar")
	private String depSoumise;
	
	@Column(name="lien_agence", columnDefinition="varchar")
	private String lienAgence;
	
	@Column(name="nom_agence", columnDefinition="varchar")
	private String nomAgence;
	
	@Column(name="nom_agents", columnDefinition="varchar")
	private String nomAgents;
	
	@Column(name="adresse", columnDefinition="varchar")
	private String adresse;
	
	@Column(name="tel", columnDefinition="varchar")
	private String tel;
	
	@Column(name="fax", columnDefinition="varchar")
	private String fax;
	
	@Column(name="orias", columnDefinition="varchar")
	private String orias;
	
	@Column(name="horaires", columnDefinition="varchar")
	private String horaires;
	
	
	@Column(name="annee_du_scrap" ,columnDefinition="varchar(4) DEFAULT date_part('year',CURRENT_DATE)" )
	private String anneeScrap;
	
	public AbeilleAssurance() {
		LocalDate ld= new Date().toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
		anneeScrap=String.valueOf(ld.getYear()+1);
	}
	
	public int getId() {
		return id;
	}
	public AbeilleAssurance setId(int id) {
		this.id = id;
		return this;
	}
	public String getLienAgence() {
		return lienAgence;
	}
	public AbeilleAssurance setLienAgence(String lienAgence) {
		this.lienAgence = lienAgence;
		return this;
	}
	public String getNomAgence() {
		return nomAgence;
	}
	public AbeilleAssurance setNomAgence(String nomAgence) {
		this.nomAgence = nomAgence;
		return this;
	}
	public String getNomAgents() {
		return nomAgents;
	}
	public AbeilleAssurance setNomAgents(String nomAgents) {
		this.nomAgents = nomAgents;
		return this;
	}
	public String getAdresse() {
		return adresse;
	}
	public AbeilleAssurance setAdresse(String adresse) {
		this.adresse = adresse;
		return this;
	}
	public String getTel() {
		return tel;
	}
	public AbeilleAssurance setTel(String tel) {
		this.tel = tel;
		return this;
	}
	public String getFax() {
		return fax;
	}
	public AbeilleAssurance setFax(String fax) {
		this.fax = fax;
		return this;
	}
	public String getOrias() {
		return orias;
	}
	public AbeilleAssurance setOrias(String orias) {
		this.orias = orias;
		return this;
	}
	public String getHoraires() {
		return horaires;
	}
	public AbeilleAssurance setHoraires(String horaires) {
		this.horaires = horaires;
		return this;
	}
	
	public String getAnneeScrap() {
		return anneeScrap;
	}
	public AbeilleAssurance setAnneeScrap(String anneeScrap) {
		this.anneeScrap = anneeScrap;
		return this;
	}

	public String getDepSoumise() {
		return depSoumise;
	}

	public AbeilleAssurance setDepSoumise(String depSoumise) {
		this.depSoumise = depSoumise;
		return this;
	}
}
