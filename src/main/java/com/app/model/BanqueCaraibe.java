package com.app.model;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="banque_des_caraibes", schema="scrap")
public class BanqueCaraibe {

	@Id
	@SequenceGenerator(name="scrap.banque_des_caraibes_id_seq", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "scrap.banque_des_caraibes_id_seq")
	private int id;
	
	@Column(name="ville_soumise", columnDefinition="varchar")
	private String ville;
	
	@Column(name="nom", columnDefinition="varchar")
	private String nom;
	
	@Column(name="adresse", columnDefinition="varchar")
	private String adresse;
	
	@Column(name="tel_commercial", columnDefinition="varchar")
	private String tel_com;
	
	@Column(name="tel_pro", columnDefinition="varchar")
	private String tel_pro;
	
	@Column(name="horaires", columnDefinition="varchar")
	private String horaires;
	
	
	@Column(name="annee_du_scrap" ,columnDefinition="varchar" )
	private String anneeScrap;
	
	public BanqueCaraibe() {
		LocalDate ld= new Date().toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
		anneeScrap=String.valueOf(ld.getYear()+1);
	}

	public String getVille() {
		return ville;
	}

	public BanqueCaraibe setVille(String ville) {
		this.ville = ville;
		return this;
	}

	public String getNom() {
		return nom;
	}

	public BanqueCaraibe setNom(String nom) {
		this.nom = nom;
		return this;
	}

	public String getAdresse() {
		return adresse;
	}

	public BanqueCaraibe setAdresse(String adresse) {
		this.adresse = adresse;
		return this;
	}

	public String getTel_com() {
		return tel_com;
	}

	public BanqueCaraibe setTel_com(String tel_com) {
		this.tel_com = tel_com;
		return this;
	}

	public String getTel_pro() {
		return tel_pro;
	}

	public BanqueCaraibe setTel_pro(String tel_pro) {
		this.tel_pro = tel_pro;
		return this;
	}

	public String getHoraires() {
		return horaires;
	}

	public BanqueCaraibe setHoraires(String horaires) {
		this.horaires = horaires;
		return this;
	}
	
	
	
	
	
}
