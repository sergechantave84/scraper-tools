package com.app.service;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.SocketTimeoutException;
import java.text.Normalizer;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Scanner;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.PageLoadStrategy;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.app.model.SG;

import com.app.scrap.RepSg;
import com.app.utils.BrowserUtils;
import com.app.utils.Parametres;
import com.app.utils.Utilitaires;

import fa.browserUtils.FactoryBrowser;
import fa.browserUtils.HtmlUnitBrowser;
import fa.log.LogApp.LogScrap;
import fa.log.config.LogConst;

@Service
public class ServiceSG extends Utilitaires {

	@Autowired
	private RepSg rep;

	private List<com.app.model.SG> sgList = new ArrayList<>();
	static String driverPath = "C:\\ChromeDriver\\";

	String nomSITE = "SG";
	String ficval = "lien_SG";
	String dossierRESU = Parametres.REPERTOIRE_RESULT + "\\BQ\\" + nomSITE;
	String url_accueil = "https://agences.societegenerale.fr";
	String fichier_valeurs;
	String CPSoumis;
	String ligne;
	// 97218 BASSE POINTE
	int num_ligne = 2;
	int NbAgences;
	int numreponse = 1;
	int DAB = 1;
	int HORS_SITE;
	int nbRecuperes;
	int num_ligneficval;
	int parcoursficval;
	int Resultats;

	HtmlUnitDriver htmlUnitDriver;
	BufferedWriter sortie;
	BufferedWriter sortie_log;

	Date Temps_Fin;

	public SG saveData(SG t) {

		return rep.save(t);

	}

	public List<SG> saveDataAll(List<SG> t) {

		return rep.saveAll(t);

	}

	public List<SG> getElmnt() {

		return rep.findAll();
	}

	public SG getById(int id) {

		return rep.findById(id).orElse(null);
	}

	public String deleteProduct(int id) {
		try {
			rep.deleteById(id);

		} catch (Exception e) {
			e.printStackTrace();
		}

		return "removed";
	}

	public SG updateProduct(SG t) {
		// SGODO Auto-generated method stub
		return null;
	}

	public SG setData(String depSoumis, String nbAgences, String nomWeb, String adresse, String tel, String horairesHtm,
			String horairestxt, String dab, String services, String agenceHtm,String codeGuichet) {
		SG sg = new SG();
		sg.setDepSoumis(depSoumis).setNbAgences(nbAgences).setNomWeb(nomWeb).setAdresse(adresse).setTel(tel)
				.setHorairesHtm(horairesHtm).setHorairestxt(horairestxt).setDab(dab).setServices(services)
				.setAgenceHtm(agenceHtm).setCode_guichet(codeGuichet);
		this.saveData(sg);
		return sg;
	}

	public List<SG> showFirstRows() {
		List<SG> a = rep.getFirstRow(100, SG.class);
		return a;
	}

	public List<SG> showLastRows() {
		List<SG> a = rep.getLastRow(100, SG.class);
		return a;
	}

	public void setup() {
		FactoryBrowser factBrowser = new FactoryBrowser();
		HtmlUnitBrowser hB = (HtmlUnitBrowser) factBrowser.create("htmlUnit");
		htmlUnitDriver = hB.setOptions(false, false, false, 60000).initBrowserDriver();
	}

	public List<com.app.model.SG> scrapSG() throws IOException, InterruptedException {

		String javaScript = "";
		fichier_valeurs = Parametres.REPERTOIRE_DATA_SOURCES + ficval + ".txt";
		Scanner sc = new Scanner(new BufferedInputStream(new FileInputStream(new File(fichier_valeurs))));

		if (!new File(dossierRESU).exists()) {
			new File(dossierRESU).mkdirs();
		}
		fichier = new File(dossierRESU + "\\" + "RESULTATS_" + nomSITE + "_" + System.currentTimeMillis() + ".txt");
		LogScrap log = new LogScrap("AssuranceSG.txt", LogConst.TYPE_LOG_FILE);
		num_ligneficval = ((new File(LogConst.STORE_LOG_PATH + LogConst.PREFIX_LOG_FILE + "_AssuranceSG.txt")).exists())
				? num_ligneficval = Integer.parseInt(log.readLog().getLastLineProcessed().split("\\t")[4])
				: 1;

		// this.initialiser();
		parcoursficval = 1;
		while (sc.hasNextLine()) {

			String[] fic_valeurs = sc.nextLine().split("\\t");
			if (parcoursficval > num_ligneficval) {
				CPSoumis = fic_valeurs[0];
				String pageSource = "";
				System.out.println("URL : " + url_accueil + fic_valeurs[1]);
				try {
					htmlUnitDriver.get(url_accueil + fic_valeurs[1]);

				} catch (Exception e) {
					if (e instanceof SocketTimeoutException) {
						htmlUnitDriver.get(url_accueil);

					}
				}

				pageSource = htmlUnitDriver.getPageSource();

				String agences_HTM = "", nom_agence = "", adresse = "", tel = "", HorairesHTM = "", HorairesTXT = "",
						Services = "",codeGuichet;

				Document doc = Jsoup.parse(pageSource);
				Element root = doc.select("#agencies").first();

				if (root != null) {
					Elements agenciesList = root.select("li.agency");
					NbAgences = agenciesList.size();
					System.out.println("NbAgences : " + NbAgences);
					nbRecuperes = 0;
					for (Element tmp : agenciesList) {
						agences_HTM = "";
						nom_agence = "";
						adresse = "";
						tel = "";
						HorairesHTM = "";
						HorairesTXT = "";
						Services = "";
						codeGuichet="";
						DAB = 1;
						agences_HTM = this.normalize(tmp.outerHtml().replaceAll("\\s{1,}", ""));

						Element codeGuichetElement=tmp.selectFirst("div.agency-inner > div.guichet-bloc > span:nth-child(2)");
						codeGuichet=(codeGuichetElement!=null) ? codeGuichetElement.text().replaceAll("[^0-9]",""):"";
						System.out.println("codeGuichet : "+codeGuichet);
						// retrieve agency name
						Element agencyNameElement = tmp.select("div.agency-inner > div.agencylabel > a").first();
						nom_agence = (agencyNameElement != null)
								? Normalizer.normalize(agencyNameElement.text(), Normalizer.Form.NFD).replaceAll(
										"[^\\p{ASCII}]", "")
								: "";
						System.out.println("nomAgence : " + nom_agence);

						// retrieve agency address
						Element agencyAddressElement = tmp.select("div.agency-inner > div.agencyaddress").first();
						adresse = (agencyAddressElement != null)
								? Normalizer.normalize(agencyAddressElement.text(), Normalizer.Form.NFD).replaceAll(
										"[^\\p{ASCII}]", "")
								: "";
						System.out.println("Adresse : " + adresse);

						// retrieve agency tel
						Element agencyTelElement = tmp.select("div.links > a.btntel").first();
						tel = (agencyTelElement != null) ? agencyTelElement.attr("href").replaceAll("[^0-9]", "") : "";
						System.out.println("Tel : " + tel);

						// retrieve agency schedule 
						
						Elements scheduleNormal = tmp.select("div.horaires-tab-wrapper > div:nth-child(2) > div.day");
						Elements scheduleRDV = tmp.select("div.horaires-tab-wrapper > div:nth-child(4) > div.day > span:nth-child(2)");
						String scheduleNormalText = "";
						String scheduleRDVText;
						Element sRDV = tmp.selectFirst("div.horaires-tab-wrapper > div:nth-child(4)");
						
						for(int i = 0; i < scheduleNormal.size(); i++) {
							scheduleNormalText = this.normalize(scheduleNormal.get(i).text().trim());
							
							 if (sRDV != null) {
								 String rdvText = this.normalize(scheduleRDV.get(i).text().trim());
								 if(rdvText.toLowerCase().contains("ferme")){
					                    scheduleRDVText = "";
					                }else{
					                    scheduleRDVText = rdvText + " sur RDV ";
					                }
								 HorairesTXT += scheduleNormalText.replaceAll("-", "") + " " + scheduleRDVText;
								 HorairesHTM += scheduleNormal.get(i).outerHtml().replaceAll("\\s{1,}", " ") + scheduleRDV.get(i)
								 				.outerHtml().replaceAll("\\s{1,}", " ");
							 }else{
								 HorairesTXT += scheduleNormalText.replaceAll("-", "") + " ";
								 HorairesHTM += scheduleNormal.get(i).outerHtml().replaceAll("\\s{1,}", " ");
					            }
							 
						}
					
						HorairesTXT = Normalizer.normalize(HorairesTXT, Normalizer.Form.NFD).replaceAll("[^\\p{ASCII}]",
								"");
						HorairesHTM = Normalizer.normalize(HorairesHTM, Normalizer.Form.NFD).replaceAll("[^\\p{ASCII}]",
								"");
						System.out.println("horaires : " + HorairesTXT);
						//System.out.println("HorairesHTM : " + HorairesHTM);
						// retrieve agency service
						Element agencyServiceElement = tmp.select("div.agency-inner > div.retraitdepot-bloc").first();
						Services = (agencyServiceElement != null) ? agencyServiceElement.text() : "";
						nbRecuperes++;
						
						this.setData(CPSoumis, String.valueOf(NbAgences), nom_agence, adresse, tel, HorairesHTM,
								HorairesTXT, String.valueOf(DAB), Services, agences_HTM,codeGuichet);
					}
				}

				log.writeLog(new Date(), CPSoumis, "no_error", NbAgences, parcoursficval, 0);
			}
			parcoursficval++;
		}
		sc.close();
		return sgList;
	}

	public void tearDowns() {
		htmlUnitDriver.close();
	}

	public void enregistrer(String agences_HTM, String nom_agence, String adresse, String tel, String HorairesHTM,
			String HorairesTXT, String Services) {

//    	this.fichier = new File(Parametres.REPERTOIRE + "RESULTATS_BNP.");
		try {

			sortie = new BufferedWriter(new FileWriter(fichier, true));
			sortie.write(CPSoumis + "\t" + NbAgences + "\t" + nom_agence + "\t" + adresse + "\t" + tel + "\t"
					+ HorairesHTM + "\t" + HorairesTXT + "\t" + DAB + "\t" + Services + "\t" + agences_HTM + "\r\n");

			sortie.close();
		} catch (IOException e) {

			e.printStackTrace();
		}

	}

	public void enregistrer_journal(int numreponse) {

//    	this.fichier = new File(Parametres.REPERTOIRE + "RESULTATS_BNP.");
		try {
			Temps_Fin = new Date();
			SimpleDateFormat formater = null;
			formater = new SimpleDateFormat("'le' dd/MM/yyyy '�' hh:mm:ss");
			String date = formater.format(Temps_Fin);
			date = formater.format(Temps_Fin);

			sortie_log = new BufferedWriter(new FileWriter(log, true));
			sortie_log.write(parcoursficval + ";" + "FinValeur" + ";" + CPSoumis + ";" + this.NbAgences + ";"
					+ nbRecuperes + ";" + (this.NbAgences - nbRecuperes) + ";" + date + "\r\n");
			sortie_log.close();
			long taille_fichier = fichier.length() / 1000000; // la length est en octets, on divise par 1000 pour mettre
																// en Ko et par 1000000 pour mettre en Mo

			if (taille_fichier > 4) {
				// INITIALISATION FICHIER DE RESULTAT
				this.fichier = new File(
						dossierRESU + "\\" + "RESULTATS_" + nomSITE + "_" + System.currentTimeMillis() + ".txt");
				// Attention pas d'accent dans les noms de colonnes !
				sortie = new BufferedWriter(new FileWriter(fichier, true));
				sortie.write("DepSoumis" + "\t" + "NbAgences" + "\t" + "NOMWEB" + "\t" + "ADRESSE" + "\t" + "TEL" + "\t"
						+ "HORAIRESHTM" + "\t" + "HORAIRESTXT" + "\t" + "DAB" + "\t" + "SERVICES" + "\t" + "AGENCEHTM"
						+ "\r\n");
				sortie.close();
			}

		} catch (IOException e) {

			e.printStackTrace();
		}

	}

	/**
	 * La m�thode <code>initialiser()</code> permet d'ajouter la ligne d'ent�te dans
	 * le fichier r�sultat.
	 */

	public void initialiser() {

		try {
			// Attention pas d'accent dans les noms de colonnes !
			sortie = new BufferedWriter(new FileWriter(fichier, true));
			sortie.write("DepSoumis" + "\t" + "NbAgences" + "\t" + "NOMWEB" + "\t" + "ADRESSE" + "\t" + "TEL" + "\t"
					+ "HORAIRESHTM" + "\t" + "HORAIRESTXT" + "\t" + "DAB" + "\t" + "SERVICES" + "\t" + "AGENCEHTM"
					+ "\r\n");
			sortie.close();

			File toto = new File(dossierRESU + "\\" + "LOG_" + nomSITE + ".csv");
			if (!toto.exists()) {
				// if (num_ligneficval == num_ligne) {
				sortie_log = new BufferedWriter(new FileWriter(log, true));
				sortie_log.write("NumLigne" + ";" + "FinValeur" + ";" + "DepSoumis" + ";" + "NbAgences" + ";"
						+ "Nbrecuperes" + ";" + "Reste" + ";" + "Heure_Fin" + "\r\n");
				sortie_log.close();
			}

		} catch (IOException e) {

			e.printStackTrace();
		}
	}
}
