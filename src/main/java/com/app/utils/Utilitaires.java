package com.app.utils;


import java.io.File;

import java.io.FileOutputStream;

import java.io.IOException;

import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.channels.FileChannel;
import java.nio.charset.CharsetEncoder;
import java.text.Normalizer;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;



public class Utilitaires {
	
	/**
     * Le champ <code>resultat</code> de type <code>StringBuffer</code> est une chaine de caract�res dynamique pour le stockage des donn�es extraites.
     */
    protected StringBuffer resultat;

    /**
     * Le champ <code>fichier</code> de type <code>File</code> repr�sente le fichier de sauvegarde des donn�es extraites.
     */
    protected File fichier;
    //V2
    protected File fichierHTML;
	
    protected File log;
    
    protected CharsetEncoder encodeur;
    
    /**
     * La m�thode <code>recuperer</code> permet d'extraire une donn�e pr�cise d'un texte par l'intermdiaire d'une expression r�guli�re et d'un num�ro de groupe.
     *
     * @param expression repr�sente le texte sur lequel la r�cup�ration doit s'effectuer.
     * @param regexp repr�sente l'expression r�guli�re permettant de r�cup�rer la donn�e.
     * @param numgroup indique le num�ro de groupe repr�sent� par les parenth�ses ans une expression r�guli�re. La valeur z�ro sp�cifie la r�gio enti�re correspondant � l'expression r�guli�re.
     * @return
     */
    
	 public String recuperer(String expression, String regexp, int numgroup) {
	        Pattern p = Pattern.compile(regexp);
	        Matcher m = p.matcher(expression);
	        if(m.find()) {
	            return m.group(numgroup);
	        }
	        return "";
	    }
	 /**
	     * La m�thode <code>getNombre()</code> convertit la cha�ne de caract�res en valeur num�rique si elle est essentiellement compos�e de chiffres.
	     *
	     * @param valeur est une cha�ne de caract�res compos�e uniquement de chiffre arabe.
	     * @return La m�thode rtourne un nombre.
	     */
	    public int getNombre(String valeur) {
	        valeur = valeur.replaceAll("_\\s+", "");
	        if(valeur.matches("\\d+"))
	            return Integer.parseInt(valeur);
	        return 0;
	    }
	    
	 public int compter(String chaine, String expression, boolean direct) {
	        Pattern p = Pattern.compile(expression);
	        Matcher m = p.matcher(chaine);
	        int compteur = 0;
	        if(direct) {
	            if(m.find()) {
	                compteur = this.getNombre(m.group(1));
	            }
	        }
	        else {
	            while(m.find()) {
	                compteur++;
	            }
	        }
	        return compteur;
	    }
	 /**
	  * @deprecated
	  * use listensIfPageFullyLoaded
	  * 
	  * */
	 public void pause(Integer milliseconds){
		    try {
		    	int tps = (int)(Math.random() * milliseconds *1000);
		    	TimeUnit.MILLISECONDS.sleep(tps);
		    } catch (InterruptedException e) {
		        e.printStackTrace();
		    }
		}
	 
	 /**
	     * La m�thode <code>nettoyerChampBaliseAll()</code> permet de nettoyer un champ contenant du code HTM en fonction d'un contenu d'une balise
	     * 
	     * ex: CodeHTM = <div> source1 <svg> source2 <\svg> <a> source3 <\a> <svg> source4 <\svg> <\div>
	     * va renvoyer � l'issue de cette methode CodeHTM = <div> source1 <a> source3 <\a> <\div>
	     *
	     * 
	     */
	 public String nettoyerChampBaliseAll (String champ, String expression_baliseHTM) {
		 
		 String baliseDeb = "<" + expression_baliseHTM + ">";
		 String baliseFin = "</" + expression_baliseHTM + ">";
		 
		 int posdebBalise = champ.indexOf(baliseDeb);
		 int posfinBalise = champ.indexOf(baliseFin);
		 while ((posdebBalise>-1 ) &&  (posdebBalise<posfinBalise)) {
			 String avtBalise = "";
			 if (posdebBalise > 0) avtBalise = champ.substring(0,posdebBalise-1); 
			 String apsBalise = champ.substring(posfinBalise+ baliseFin.length()) ;
			 champ = avtBalise + " " + apsBalise;
			 posdebBalise = champ.indexOf(baliseDeb);
			 posfinBalise = champ.indexOf(baliseFin);
		 }
		 return champ;
	 }
	    
	 
public String nettoyerChampExpressionDebutFin (String champ, String expressionDebut, String expressionFin ) {
		 
		 
		 
		 int posdebExpr = champ.indexOf(expressionDebut);

		 int fini = 0;
		 
		 while ((posdebExpr>-1 ) &&  (fini ==0)) {
			 String debchamp = "";
			 String finchamp = ""; 
			 if (posdebExpr > 0) {
				 debchamp = champ.substring(0,posdebExpr-1);
				 finchamp = champ.substring(posdebExpr+expressionDebut.length());
			 }
			 
			 int posfinExpr = finchamp.indexOf(expressionFin);
			 if (posfinExpr >-1) {
				 finchamp = finchamp.substring(posfinExpr+ expressionFin.length()) ;
				 champ = debchamp + " " + finchamp;
			 }
			 else fini = 1;
			 posdebExpr = champ.indexOf(expressionDebut);
			 posfinExpr = champ.indexOf(expressionFin);
		 }
		 return champ;
	 }
	    

public String getCodeHTM (String codeHTM, WebElement elementweb ) {
	 
	codeHTM = elementweb.getAttribute("innerHTML");
	codeHTM = codeHTM.replaceAll("[\r\n]+", "#CRLF#");
	codeHTM = codeHTM.replaceAll("[\t]+", " ");
    codeHTM = codeHTM.replaceAll("( ){2,}", " ");
	 return codeHTM;
}	 
	

public String getCodeTXT (String codeTXT, WebElement elementweb ) {
	 
	codeTXT = elementweb.getText();
	System.out.println(codeTXT);
	codeTXT = codeTXT.replaceAll("[\r\n]+", "#CRLF#");
	codeTXT = codeTXT.replaceAll("[\t]+", " ");
	//codeTXT = codeTXT.replaceAll("( ){2,}", " ");
	 return codeTXT;
}

public String recuperer (String champinit, String champarrivee,String expDebut, String ExpFin) {
	
	int posDEB = 0;String exp = expDebut;
	posDEB = champinit.indexOf(exp) + exp.length();
	 if (posDEB>exp.length()) champarrivee = champinit.substring(posDEB);
	 int posFIN = 0;
	 posFIN = champarrivee.indexOf(ExpFin);
	 if (posFIN>0) champarrivee = champarrivee.substring(0, posFIN);
	 return champarrivee;
}

public void ecrire(CharSequence contenu, File fichier, boolean ajout) {
	//System.out.println("IO.ecrire : " + this.encodeur.charset().name() + " " + fichier);
	        if (contenu == null)// || fichier.getAbsolutePath().length() >= 255)
	            return;
	        
	        try {
	            FileOutputStream fos = new FileOutputStream(fichier, ajout);
	            FileChannel fc = fos.getChannel();
	            
	               
	            CharBuffer cb = CharBuffer.wrap(contenu);
	            ByteBuffer bb = encodeur.encode(cb);
	            fc.write(bb);
	            fc.close();
	            fos.flush();
	            fos.close();
//	            System.out.println("IO.ecrire() : FIN " + fc.isOpen());
	        }
	        catch (java.io.FileNotFoundException e) {
//	            JOptionPane.showMessageDialog(
//						                    null, 
//						                    Constantes.getString(classe, 0) 
//						                  + fichier.toString() 
//						                  + Constantes.getString(classe, 1) 
//						                  + e.getMessage(),
//						                    Constantes.getString(classe, 2),
//				return;
	        }
	        catch (IOException e) {
	            //e.printStackTrace();
	            //			System.out.println (InfosVoie.getString("Configuration", 12)
	            // + fichier.toString());
	        }
	    }

	public  WebElement ElementSelectCSS (ChromeDriver driver , WebElement element,String  chemin) {
		try {
			element = driver.findElementByCssSelector(chemin);
			
		} catch (NoSuchElementException ex) { 
	  	  
	    }
		return element;
	}
	
	public  WebElement ElementSelectCSSFirefox (FirefoxDriver driver , WebElement element,String  chemin) {
		try {
			element = driver.findElementByCssSelector(chemin);
			
		} catch (NoSuchElementException ex) { 
	  	  
	    }
		return element;
	}
	
	public  WebElement ElementSelectXPath (ChromeDriver driver , WebElement element,String  chemin) {
		try {
			element = driver.findElement(By.xpath(chemin));
			
		} catch (NoSuchElementException ex) { 
	  	  
	    }
		return element;
	}
	/**
	 * 
	 * this function listens if the page is fully loaded
	 * @author Faniry Andriamihaingo 
	 * 
	 * @param WebDriver 
	 * @return boolean
	 * 
	 * */
	public  boolean listensIfPageFullyLoaded(WebDriver driver) 
	 {
		
		 String jsScript="return document.readyState", dReadyState="";
		 JavascriptExecutor jsExecutor=(JavascriptExecutor)driver;
		 try 
		   {
			   dReadyState=(String) jsExecutor.executeScript(jsScript);
			   while( !dReadyState.equals("complete")) 
			   {
				   dReadyState=(String) jsExecutor.executeScript(jsScript);
			   }
		   }
		   catch(Exception e) 
		   {
			  return(false);
		   }
		   return (dReadyState.equals("complete"));
		   
	 }
	
	
	public String normalize(String input) {
		return Normalizer.normalize(input, Normalizer.Form.NFD).replaceAll("[^\\p{ASCII}]", "");
	}
	
	public boolean isDOM(String target) {
		Pattern p=Pattern.compile("97[123456][0-9]{2}");
		Matcher m=p.matcher(target);
		return m.find();
	}
}
