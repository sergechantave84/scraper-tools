package com.app.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.app.model.Nickel;

import com.app.scrap.RepNickel;

@Service
public class ServiceNickel {

	@Autowired
	private RepNickel rep;


	public Nickel saveData(Nickel t) {

		
			return  rep.save(t);
		
	}

	
	public List<Nickel> saveDataAll(List<Nickel> t) {
		
			return rep.saveAll(t);
		
	}

	
	public List<Nickel> getElmnt() {

		return  rep.findAll();
	}


	public Nickel getById(int id) {

		return rep.findById(id).orElse(null);
	}

	
	public String deleteProduct(int id) {
		try {
			rep.deleteById(id);

		} catch (Exception e) {
			e.printStackTrace();
		}

		return "removed";
	}

	
	public Nickel updateProduct(Nickel t) {
		// NickelODO Auto-generated method stub
		return null;
	}
	
	public Nickel setData( String valeurSoumis,String nbPointNickel,String nomWeb,String adresse,String tel,String lienPointNickel,String horairesHtm,
			               String horairesNickelxt,String dab,String services,String pointHtm) {
		Nickel nickel= new Nickel();
		nickel.setValeurSoumis(valeurSoumis).
		       setNbPointNickel(nbPointNickel).
		       setNomWeb(nomWeb).
		       setAdresse(adresse).
		       setTel(tel).
		       setLienPointNickel(lienPointNickel).
		       setHorairesHtm(horairesHtm).
		       setHorairesTxt(horairesNickelxt).
		       setDab(dab).
		       setServices(services).
		       setPointHtm(pointHtm);
		return nickel;
	}
	public List<Nickel> showFirstRows() {
		 List<Nickel> a=rep.getFirstRow(100, Nickel.class);
			 return a;
	}
		 
	public List<Nickel> showLastRows(){
			 List<Nickel> a=rep.getLastRow(100, Nickel.class);
			 return a;
	}
}
