package com.app.service;

import java.io.BufferedInputStream;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.text.Normalizer;
import java.util.List;
import java.util.Scanner;
import org.json.simple.JSONObject;
import org.json.simple.parser.ParseException;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.PageLoadStrategy;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

@Service
public class ServiceFerme {
	
	public String detectP(String id, Element doc , String href) {

		String selector = "#content_acheter_produits > h5" + id + href;

		String textContent = "";

		while (doc.selectFirst(selector) != null) {

			Element newP = doc.selectFirst(selector);

			if (newP != null) {
				
				if(href.equals("+p")) {
					textContent += newP.text();
				}else {
					textContent += newP.attr("href");
				}

			}

			selector = selector + "+p";

		}

		return textContent;

	}
	
	
	public String detectPclassPresentFerme(String selecteur, Element doc) {

		String selector = selecteur;

		String textContent = "";
		
		if(doc.selectFirst("#ff_description > div.col-1 > div > p.presentation_ferme") != null) {

		while (!doc.selectFirst(selector).attr("class").equals("presentation_ferme")) {

			Element newP = doc.selectFirst(selector);

			if (newP != null) {
				
				
				textContent += newP.text();
				

			}

			selector = selector + "+p";

		}
		
		}

		return textContent;

	}


	@SuppressWarnings("unchecked")
	public void scrap() throws InterruptedException, IOException, ParseException {

		WebDriver driver = getDriver();

		String nosPdv = "Directe chez le producteur";

		// Bloc resultat

		File lien = new File("liste_lien/lien.txt");

		// Open file
		BufferedWriter writeLien;
		
		int lasCompteur = 0;

		if (!lien.exists()) {
			writeLien = new BufferedWriter(new FileWriter(lien, true));
			writeLien.write(
					"Compteur~Page~Activité~Nom1~Nom2~Adresse1~Commentaire1~Produit1~Produit2~Produit3~Produit4~Produit5~"
							+ "Produit6~Produit7~Produit8~Lien\n");
			writeLien.close();
			lasCompteur = 1;
		}else {
			Scanner readLien = new Scanner(new BufferedInputStream(new FileInputStream(lien)));

			while (readLien.hasNextLine()) {
				readLien.nextLine();
				lasCompteur++;
			}
		}

		File json = new File("ferme_log/jsonData.txt");
		BufferedWriter writeJson;

		if (!json.exists()) {
			writeJson = new BufferedWriter(new FileWriter(json, true));
			writeJson.write("DataResults\n");
			writeJson.close();
		}

		File autreLogo = new File("autres/autreLogo.txt");
		BufferedWriter writeAutreLogo;

		if (!autreLogo.exists()) {
			writeAutreLogo = new BufferedWriter(new FileWriter(autreLogo, true));
			writeAutreLogo.write("AutresLogos\n");
			writeAutreLogo.close();
		}

		File log_page = new File("ferme_log/log_page.txt");

		BufferedWriter writeLogPage;

		int lastPage = 0;

		if (log_page.exists()) {

			Scanner readLogPage = new Scanner(new BufferedInputStream(new FileInputStream(log_page)));

			String lastLine = "";
			while (readLogPage.hasNextLine()) {
				lastLine = readLogPage.nextLine();
			}

			lastPage = Integer.parseInt(lastLine);

		} else {

			writeLogPage = new BufferedWriter(new FileWriter(log_page, true));
			writeLogPage.write("Numéro_page\n");
			writeLogPage.close();
		}

		if (lastPage < 531) {

			String activite = "Producteur";
			String link = "";
			String nom1 = "";
			String nom2 = "";
			String adresse1 = "";
			String commentaire1 = "";
			String produit1 = "";
			String produit2 = "";
			String produit3 = "";
			String produit4 = "";
			String produit5 = "";
			String produit6 = "";
			String produit7 = "";
			String produit8 = "";

			// BASE URL

			String baseUrl = "https://www.bienvenue-a-la-ferme.com/recherche/produits-fermiers/2651";

			int compteur = lasCompteur;

			for (int i = 1; i <= 531; i++) {

				if (i > lastPage) {

					driver.get(baseUrl + "/page:" + i);

					// #input_chez_le_producteur
					JavascriptExecutor js = (JavascriptExecutor) driver;
					// js.executeScript("document.querySelector(\"#input_chez_le_producteur\").click()",
					// "");
					String script = String.format(
							"let checbox = document.querySelector('#input_chez_le_producteur'); if(checbox!=null){checbox.click()}",
							"");

					js.executeScript(script);

					List<WebElement> results = driver
							.findElements(By.cssSelector("div#liste_resultats.liste_resultats"));

					if (results.size() > 0) {

						WebElement root = driver.findElement(By.cssSelector("div#liste_resultats.liste_resultats"));

						Thread.sleep(5000);

						driver.switchTo().defaultContent();

						Thread.sleep(5000);

						String pageSource = Normalizer.normalize(root.getAttribute("outerHTML"), Normalizer.Form.NFD)
								.replaceAll("[^\\p{ASCII}]", "");

						Document doc = Jsoup.parse(pageSource);

						Element racine = doc.selectFirst("div#liste_resultats.liste_resultats");

						Element total = racine.selectFirst("div.total");

						String resultBrut = total.ownText().split(" ")[0];

						int resultTotal = Integer.parseInt(resultBrut);

						System.out.println("Total : " + resultTotal);

						System.out.println("Page numero : " + i);

						Elements lists = racine.select("ul.list_data > li.result");

						writeLien = new BufferedWriter(new FileWriter(lien, true));

						String res = "";

						for (Element li : lists) {

							System.out.println("Compteur : " + compteur);

							activite = li.selectFirst("p.type_resultat > span.type").text();

							nom1 = li.selectFirst("h2 > a").text();

							nom2 = li.selectFirst("p.nom_fermier > strong").text();

							adresse1 = li.selectFirst("p.nom_commune").text();

							commentaire1 = li.selectFirst("div.produits_fermiers").text();

							Element ul = li.selectFirst("ul.mots_cles");

							if (ul != null) {

								produit1 = ul.select("li:nth-child(1)") != null ? ul.select("li:nth-child(1)").text()
										: "";
								produit2 = ul.select("li:nth-child(2)") != null ? ul.select("li:nth-child(2)").text()
										: "";
								produit3 = ul.select("li:nth-child(3)") != null ? ul.select("li:nth-child(3)").text()
										: "";
								produit4 = ul.select("li:nth-child(4)") != null ? ul.select("li:nth-child(4)").text()
										: "";
								produit5 = ul.select("li:nth-child(5)") != null ? ul.select("li:nth-child(5)").text()
										: "";
								produit6 = ul.select("li:nth-child(6)") != null ? ul.select("li:nth-child(6)").text()
										: "";
								produit7 = ul.select("li:nth-child(7)") != null ? ul.select("li:nth-child(7)").text()
										: "";
								produit8 = ul.select("li:nth-child(8)") != null ? ul.select("li:nth-child(8)").text()
										: "";
							}

							link = li.selectFirst("p.plus_d_infos > a") != null
									? "https://www.bienvenue-a-la-ferme.com"
											+ li.selectFirst("p.plus_d_infos > a").attr("href")
									: "https://www.bienvenue-a-la-ferme.com" + li.selectFirst("h2 > a").attr("href");
							
							commentaire1 = commentaire1.replaceAll("~", "");

							res = compteur + "~" + i + "~" + activite + "~" + nom1 + "~" + nom2 + "~" + adresse1 + "~"
									+ commentaire1 + "~" + produit1 + "~" + produit2 + "~" + produit3 + "~" + produit4
									+ "~" + produit5 + "~" + produit6 + "~" + produit7 + "~" + produit8 + "~" + link
									+ "\n";

							//System.out.println(res);

							//writeLien.write(res.replace("~", ""));
							
							writeLien.write(res);

							compteur++;

						}

						writeLien.close();

					}
					
					writeLogPage = new BufferedWriter(new FileWriter(log_page, true));
					writeLogPage.write(i + "\n");
					writeLogPage.close();
					
				}

			}
			
			driver.quit();

		} else {

			// BLOCK SCRAP

			File log = new File("ferme_log/log.txt");

			// Open file
			BufferedWriter writeLog;

			int w = 0;
			if (log.exists()) {

				Scanner readLog = new Scanner(new BufferedInputStream(new FileInputStream(log)));

				while (readLog.hasNextLine()) {
					readLog.nextLine();
					w++;
				}
			} else {
				writeLog = new BufferedWriter(new FileWriter(log, true));
				writeLog.write("Logging\n");
				writeLog.close();
				w = 1;
			}

			Gson gson = new GsonBuilder().disableHtmlEscaping().create();

			int j = 0;

			Scanner readLien = new Scanner(new BufferedInputStream(new FileInputStream(lien)));

			while (readLien.hasNextLine()) {

				String[] tab = readLien.nextLine().split("~");

				if (j >= w) {

					JSONObject JSONFin = new JSONObject();

					int compteur = Integer.parseInt(tab[0]);
					System.out.println("Compteur :  " + compteur);
					// System.out.println("Length : " + tab.length);

					int page = Integer.parseInt(tab[1]);
					String activite = tab[2];
					String nom1 = tab[3];
					String nom2 = tab[4];
					String adresse1 = tab[5];
					String commentaire1 = tab[6];
					String produit1 = tab[7];
					String produit2 = tab[8];
					String produit3 = tab[9];
					String produit4 = tab[10];
					String produit5 = tab[11];
					String produit6 = tab[12];
					String produit7 = tab[13];
					String produit8 = tab[14];
					String link = tab[15];

					JSONObject BlocResultat = new JSONObject();
					BlocResultat.put("activite", activite);
					BlocResultat.put("lien", link);
					BlocResultat.put("nom1", nom1);
					BlocResultat.put("nom2", nom2);
					BlocResultat.put("adresse1", adresse1);
					BlocResultat.put("commentaire1", commentaire1);
					BlocResultat.put("produit1", produit1);
					BlocResultat.put("produit2", produit2);
					BlocResultat.put("produit3", produit3);
					BlocResultat.put("produit4", produit4);
					BlocResultat.put("produit5", produit5);
					BlocResultat.put("produit6", produit6);
					BlocResultat.put("produit7", produit7);
					BlocResultat.put("produit8", produit8);

					System.out.println("Page : " + page);
					System.out.println("Activité : " + activite);
					System.out.println("Nom1 : " + nom1);
					System.out.println("Nom2 : " + nom2);
					System.out.println("adresse1 : " + adresse1);
					System.out.println("commentaire1 : " + commentaire1);
					System.out.println("produit1 : " + produit1);
					System.out.println("produit2 : " + produit2);
					System.out.println("produit3 : " + produit3);
					System.out.println("produit4 : " + produit4);
					System.out.println("produit5 : " + produit5);
					System.out.println("produit6 : " + produit6);
					System.out.println("produit7 : " + produit7);
					System.out.println("produit8 : " + produit8);

					System.out.println("Lien : " + link);

					driver.get(link);

					WebElement root = driver.findElement(By.cssSelector("body"));

					Thread.sleep(2000);

					String pageSource = root.getAttribute("outerHTML");
					// Normalizer.Form.NFD).replaceAll("[^\\p{ASCII}]", "");
					pageSource = (Normalizer.normalize(pageSource, Normalizer.Form.NFD)).replaceAll("[^\\p{ASCII}]",
							"");
					Document doc = Jsoup.parse(pageSource);

					Element erreur404 = doc.selectFirst("div.Erreur404");

					if (erreur404 != null) {
						System.out.println("WOUPSS 404 ERRORS");
					} else {

						// Bloc pages

						String commentaire2 = "";
						String productionFerme = "";
						String productionBio = "";

						// Bloc Logos

						int agriBio = 0;
						int accHandi = 0;
						int cb = 0;
						int animaux = 0;
						int car = 0;
						int chequeVacance = 0;
						int marcheProd = 0;
						int internet = 0;
						int stationVerte = 0;
						int handMent = 0;
						int handMot = 0;
						int handVis = 0;
						int handAud = 0;
						int adeve = 0;
						int tickets = 0;

						// Bloc produit fermiers

						String produitProposes = "";
						int atelier = 0;
						int degustation = 0;
						int engagement = 0;
						String venteALaFerme = "";
						String venteEnLigne = "";
						String venteSurMarche = "";
						String magasinProducteur = "";
						String autreVente = "";

						// Bloc contact & accèss
						String contact = "";
						String adresse2 = "";
						String adresse3 = "";
						String couriel1 = "";
						String couriel2 = "";
						String site = "";
						String site2 = "";
						String telTravail = "";
						String telDomicile = "";
						String mobile = "";
						String fax = "";
						double LONG = 0;
						double LAT = 0;

						// BLOC PAGE 1
						
						Element elem_commentaire2 = doc
								.selectFirst("#ff_description > div.col-1 > div > p.presentation_ferme");

						Element elem_productionFerme = doc
								.selectFirst("#ff_description > div.col-1 > div > p.prod_ferme");

						Element elem_productionBio = doc
								.selectFirst("#ff_description > div.col-1 > div > p.type_prod_ferme");

						commentaire2 = elem_productionFerme != null ? elem_commentaire2.text() : "";

						Element msgAcueil = doc.selectFirst("#ff_description > div.col-1 > div > p.msg_accueil_ferme");
						
						Element rotu = doc.selectFirst("#ff_description > div.col-1 > div");

						String com = "";

						if (msgAcueil != null) {

							com = detectPclassPresentFerme("p.msg_accueil_ferme + p", rotu);
							
						} else {
							
							com = detectPclassPresentFerme("p.nom_commune + p", rotu);

						}

						commentaire2 = com + commentaire2;

						productionFerme = elem_productionFerme != null ? elem_productionFerme.ownText() : "";
						productionBio = elem_productionBio != null ? elem_productionBio.ownText() : "";

						JSONObject BlocPage = new JSONObject();
						BlocPage.put("commentaire2", commentaire2);
						BlocPage.put("productionFerme", productionFerme);
						BlocPage.put("productionBio", productionBio);

						System.out.println("commentaire2 :" + " " + commentaire2);
						System.out.println("productionFerme :" + " " + productionFerme);
						System.out.println("productionBio :" + " " + productionBio);

						// BLOC LOGOS

						Element icon = doc.selectFirst("#ff_description > div.col-1 > div > div.icones");

						if (icon != null) {
							Elements imgs = icon.select("ul > li >img");

							for (Element img : imgs) {

								String key = img.attr("title").toLowerCase();

								switch (key) {
								case "agriculture biologique" -> agriBio = 1;
								case "accessible aux handicapes" -> accHandi = 1;
								case "cb" -> cb = 1;
								case "animaux domestiques" -> animaux = 1;
								case "accueil d'autocar" -> car = 1;
								case "cheques vacances" -> chequeVacance = 1;
								case "marche des producteurs de pays" -> marcheProd = 1;
								case "connexion internet sur place" -> internet = 1;
								case "handicap mental" -> handMent = 1;
								case "handicap moteur" -> handMot = 1;
								case "station verte" -> stationVerte = 1;
								case "handicap visuel" -> handVis = 1;
								case "handicap auditif" -> handAud = 1;
								case "adherent a l'adeve" -> adeve = 1;
								case "tickets restaurant" -> tickets = 1;
								default -> {

									System.out.println("AUTRE LOGOS");

									writeAutreLogo = new BufferedWriter(new FileWriter(autreLogo, true));
									writeAutreLogo.write(compteur + ";" + key + ";" + link + "\n");
									writeAutreLogo.close();

								}
								}

							}

						}

						JSONObject BlocLogos = new JSONObject();
						BlocLogos.put("agriBio", agriBio);
						BlocLogos.put("accHandi", accHandi);
						BlocLogos.put("cb", cb);
						BlocLogos.put("animaux", animaux);
						BlocLogos.put("car", car);
						BlocLogos.put("chequeVacance", chequeVacance);
						BlocLogos.put("marcheProd", marcheProd);
						BlocLogos.put("internet", internet);
						BlocLogos.put("handMent", handMent);
						BlocLogos.put("handMot", handMot);
						BlocLogos.put("handVis", handVis);
						BlocLogos.put("handAud", handAud);
						BlocLogos.put("stationVerte", stationVerte);
						BlocLogos.put("adeve", adeve);
						BlocLogos.put("tickets", tickets);

						System.out.println("agriBio :" + " " + agriBio);
						System.out.println("accHandi :" + " " + accHandi);
						System.out.println("cb :" + " " + cb);
						System.out.println("animaux :" + " " + animaux);
						System.out.println("car :" + " " + car);
						System.out.println("chequeVacance :" + " " + chequeVacance);
						System.out.println("marcheProd :" + " " + marcheProd);
						System.out.println("internet :" + " " + internet);
						System.out.println("handMental :" + " " + handMent);
						System.out.println("handMoteur :" + " " + handMot);
						System.out.println("handVis :" + " " + handVis);
						System.out.println("handAud :" + " " + handAud);
						System.out.println("stationVerte :" + " " + stationVerte);
						System.out.println("adeve :" + " " + adeve);
						System.out.println("tickets :" + " " + tickets);

						// BLOC PRODUITS FERMIERS

						produitProposes = doc.selectFirst("#content_decouvrir_produits > p") != null
								? doc.selectFirst("#content_decouvrir_produits > p").ownText()
								: "";

						Elements siblings = doc.select("#content_decouvrir_produits > p > strong");

						if (siblings.size() > 1) {

							for (Element p : siblings) {

								if (p.text().contains("Produits propo")) {
									System.out.println("Produits proposés");
								} else if (p.text().contains("Atelier")) {
									atelier = 1;
								} else if (p.text().contains("Degustation")) {
									degustation = 1;
								} else if (p.text().contains("engagements du producteur")) {
									engagement = 1;
								} else {
									System.out.println("AUTRE");
								}

							}

						}

						Element vente = doc.selectFirst("#content_acheter_produits");

						Elements h5 = vente.select("h5");

						if (h5.size() > 0) {
							
							for(Element titre : h5) {
								if (titre.text().contains("Vente a la ferme")) {

									titre.attr("id", "ferme");
									
									venteALaFerme = detectP("#"+titre.attr("id"), vente, "+p");

								} else if (titre.text().contains("Vente sur les marches")) {

									titre.attr("id", "marches");
									venteSurMarche = detectP("#"+titre.attr("id"), vente, "+p");

								} else if (titre.text().contains("Magasin du producteur")) {

									titre.attr("id", "magasin");
									magasinProducteur = detectP("#"+titre.attr("id"), vente, "+p");
									

								} else if (titre.text().contains("Autres lieux de vente")) {

									titre.attr("id", "autres");
									autreVente = detectP("#"+titre.attr("id"), vente, "+p");

								} else if (titre.text().contains("Vente en ligne")) {

									titre.attr("id", "ligne");
									venteEnLigne = detectP("#"+titre.attr("id"), vente, "+div+p>a");

								}

							}
						}

						JSONObject BlocProduitsFermier = new JSONObject();
						BlocProduitsFermier.put("produitProposes", produitProposes);
						BlocProduitsFermier.put("atelier", atelier);
						BlocProduitsFermier.put("degustation", degustation);
						BlocProduitsFermier.put("engagement", engagement);
						BlocProduitsFermier.put("venteALaFerme", venteALaFerme);
						BlocProduitsFermier.put("venteEnLigne", venteEnLigne);
						BlocProduitsFermier.put("venteSurMarche", venteSurMarche);
						BlocProduitsFermier.put("magasinProducteur", magasinProducteur);
						BlocProduitsFermier.put("autreVente", autreVente);

						System.out.println("produitProposes : " + " " + produitProposes);
						System.out.println("atelier : " + " " + atelier);
						System.out.println("degustation :" + " " + degustation);
						System.out.println("engagement :" + " " + engagement);

						System.out.println("venteALaFerme :" + " " + venteALaFerme);
						System.out.println("venteEnLigne :" + " " + venteEnLigne);
						System.out.println("venteSurMarche :" + " " + venteSurMarche);
						System.out.println("magasinProducteur :" + " " + magasinProducteur);
						System.out.println("autreVente :" + " " + autreVente);

						// BLOC CONTACT & ACCES

						contact = doc.selectFirst("address > p.fn > span") != null
								? doc.selectFirst("address > p.fn > span").text()
								: "";
						adresse2 = doc.selectFirst("address > p.org") != null
								? doc.selectFirst("address > p.org").text()
								: "";

						Element elementAdresse3 = doc.selectFirst("address > p.adr");

						if (elementAdresse3 != null) {

							adresse3 = elementAdresse3.selectFirst("span.locality") != null
									? elementAdresse3.selectFirst("span.locality").text()
									: "";

							Elements othersAdress = elementAdresse3.nextElementSiblings();

							String coord = "";
							for (Element otherAdress : othersAdress) {

								String key = otherAdress.selectFirst("strong").text();
								String autres = otherAdress.selectFirst("strong").nextElementSiblings().text();

								if (key.contains("Courriel :")) {
									couriel1 = autres;
								} else if (key.contains("Courriel 2")) {
									couriel2 = autres;
								} else if (key.contains("Site :")) {
									site = autres;
								} else if (key.contains("Site 2")) {
									site2 = autres;
								} else if (key.contains("Tel travail")) {
									telTravail = autres;
								} else if (key.contains("Tel domicile")) {
									telDomicile = autres;
								} else if (key.contains("Tel portable")) {
									mobile = autres;
								} else if (key.contains("Fax")) {
									fax = autres;
								} else if (key.contains("GPS")) {
									coord = autres;
									String[] tabCoord = coord.split("Lat");
									LONG = Double.parseDouble(tabCoord[0].split(" : ")[1]);
									LAT = Double.parseDouble(tabCoord[1].split(" : ")[1]);
								} else {
									System.out.println("\t AUTRE ADRESSE");
								}
							}
						}

						JSONObject BlocContactAccess = new JSONObject();
						BlocContactAccess.put("couriel1", couriel1);
						BlocContactAccess.put("couriel2", couriel2);
						BlocContactAccess.put("site", site);
						BlocContactAccess.put("site2", site2);
						BlocContactAccess.put("telTravail", telTravail);
						BlocContactAccess.put("telDomicile", telDomicile);
						BlocContactAccess.put("mobile", mobile);
						BlocContactAccess.put("fax", fax);
						BlocContactAccess.put("LONG", LONG);
						BlocContactAccess.put("LAT", LAT);

						System.out.println("couriel1 : " + couriel1);
						System.out.println("couriel2 : " + couriel2);
						System.out.println("site : " + site);
						System.out.println("site2 : " + site2);
						System.out.println("telTravai : " + telTravail);
						System.out.println("telDomicile : " + telDomicile);
						System.out.println("mobile : " + mobile);
						System.out.println("Fax : " + fax);
						// System.out.println("GPS : " + coord);
						System.out.println("LONG : " + LONG);
						System.out.println("LAT : " + LAT);

						JSONFin.put("NosPDV", nosPdv);
						JSONFin.put("BlocResultat", BlocResultat);
						JSONFin.put("BlocPage", BlocPage);
						JSONFin.put("BlocLogos", BlocLogos);
						JSONFin.put("BlocProduitsFermier", BlocProduitsFermier);
						JSONFin.put("BlocContactAccess", BlocContactAccess);

						System.out.println("*********** JSON **************");
						String gsonString = gson.toJson(JSONFin);
						System.out.println(gsonString);

						writeJson = new BufferedWriter(new FileWriter(json, true));
						writeJson.write(gsonString + "\n");
						writeJson.close();

						writeLog = new BufferedWriter(new FileWriter(log, true));
						writeLog.write(compteur + " " + link + "\n");

						writeLog.close();
					}

				}

				j++;
			}

			readLien.close();
			driver.quit();

		}

	}

	public String normalize(String s) {
		if (s == null) {
			return null;
		} else {
			s = Normalizer.normalize(s, Normalizer.Form.NFD);
			s = s.replaceAll("[\\p{InCombiningDiacriticalMarks}]", "");
			return s;
		}

	}

	public WebDriver getDriver() {
		
		System.setProperty("webdriver.chrome.driver", "D:/ChromeDriver/chromedriver.exe");
		ChromeOptions options = new ChromeOptions();
		
		options.addArguments("start-maximized");
		options.setPageLoadStrategy(PageLoadStrategy.NORMAL);

		WebDriver driver = new ChromeDriver(options);
		return driver;
	}

}
