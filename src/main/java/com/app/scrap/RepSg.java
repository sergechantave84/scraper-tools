package com.app.scrap;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.app.model.SG;

@Repository
public interface RepSg extends JpaRepository<SG,Integer>,RepositoryCustom<SG>{

}
