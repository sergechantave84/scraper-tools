package com.app.model;

import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "banque_cic_gab", schema = "scrap")
public class CIC_GAB {

	@Id
	@SequenceGenerator(name = "scrap.banque_cic_gab_id_seq", sequenceName = "scrap.banque_cic_gab_id_seq", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "scrap.banque_cic_gab_id_seq")

	@Column(name = "id", updatable = false)
	private int id;

	@Column(name = "communesoumise", columnDefinition = "varchar")
	private String communeSoumise;

	@Column(name = "nbagences", columnDefinition = "varchar")
	private String nbAgences;

	@Column(name = "nomweb", columnDefinition = "varchar")
	private String nomWeb;

	@Column(name = "adresse", columnDefinition = "varchar")
	private String adresse;

	@Column(name = "gabcic", columnDefinition = "varchar")
	private String gabCIC;

	@Column(name = "gabcm", columnDefinition = "varchar")
	private String gabCM;

	@Column(name = "agencehtm", columnDefinition = "varchar")
	private String agenceHtm;
	
	/*@Column(name = "cp", columnDefinition = "varchar")
	private String cp;

	public String getCp() {
		return cp;
	}

	public CIC_GAB setCp(String cp) {
		this.cp = cp;
		return this;
	}*/

	public String getDep() {
		return dep;
	}

	public CIC_GAB setDep(String dep) {
		this.dep = dep;
		return this;
	}

	@Column(name = "dep", columnDefinition = "varchar")
	private String dep;

	@Column(name = "annee_du_scrap", columnDefinition = "varchar(4) DEFAULT date_part('year',CURRENT_DATE)")
	private String anneeScrap;

	public CIC_GAB() {
		LocalDate ld = new Date().toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
		anneeScrap = String.valueOf(ld.getYear() + 1);
	}

	public int getId() {
		return id;
	}

	public CIC_GAB setId(int id) {
		this.id = id;
		return this;
	}

	public String getCommuneSoumise() {
		return communeSoumise;
	}

	public CIC_GAB setCommuneSoumise(String communeSoumise) {
		this.communeSoumise = communeSoumise;
		return this;
	}

	public String getNbAgences() {
		return nbAgences;
	}

	public CIC_GAB setNbAgences(String nbAgences) {
		this.nbAgences = nbAgences;
		return this;
	}

	public String getNomWeb() {
		return nomWeb;
	}

	public CIC_GAB setNomWeb(String nomWeb) {
		this.nomWeb = nomWeb;
		return this;
	}

	public String getAdresse() {
		return adresse;
	}

	public CIC_GAB setAdresse(String adresse) {
		this.adresse = adresse;
		return this;
	}

	public String getGabCIC() {
		return gabCIC;
	}

	public CIC_GAB setGabCIC(String gabCIC) {
		this.gabCIC = gabCIC;
		return this;
	}

	public String getGabCM() {
		return gabCM;
	}

	public CIC_GAB setGabCM(String gabCM) {
		this.gabCM = gabCM;
		return this;
	}

	public String getAgenceHtm() {
		return agenceHtm;
	}

	public CIC_GAB setAgenceHtm(String agenceHtm) {
		this.agenceHtm = agenceHtm;
		return this;
	}

	public String getAnneeScrap() {
		return anneeScrap;
	}

	public CIC_GAB setAnneeScrap(String anneeScrap) {
		this.anneeScrap = anneeScrap;
		return this;
	}
}
