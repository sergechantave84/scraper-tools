package com.app.scrap;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.app.model.Harmonie_mutuelle;

@Repository
public interface RepHarmonieMutu extends JpaRepository<Harmonie_mutuelle,Integer>,RepositoryCustom<Harmonie_mutuelle> {

}
