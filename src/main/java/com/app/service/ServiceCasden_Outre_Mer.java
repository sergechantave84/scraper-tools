package com.app.service;

import java.io.File;
import java.io.FileInputStream;

import java.net.SocketTimeoutException;
import java.text.Normalizer;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Scanner;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import org.openqa.selenium.htmlunit.HtmlUnitDriver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.app.model.CASDEN_Outre_Mer;

import com.app.scrap.RepCasdenOutreMer;

import com.app.utils.Parametres;

import fa.log.LogApp.LogScrap;
import fa.log.config.LogConst;

@Service
public class ServiceCasden_Outre_Mer extends Scraper {

	@Autowired
	private RepCasdenOutreMer rep;

	String nomSITE = "CASDEN_Outre_Mer";
	String ficval = "lien_casden_outre_mer";
	String dossierRESU = Parametres.REPERTOIRE_RESULT + "\\BQ\\" + nomSITE;;
	String url_accueil = "https://agences.banquepopulaire.fr/banque-assurance";
	// "https://www.casden.fr/Votre-banque-cooperative/Annuaires/Annuaire-des-agences-Banque-Populaire";
	String fichier_valeurs;
	String CPSoumis;

	int num_ligne = 2;
	int NbAgences;

	int nbRecuperes;
	int num_ligneficval;
	int parcoursficval;

	HtmlUnitDriver htmlUnitDriver;

	public CASDEN_Outre_Mer updateProduct(CASDEN_Outre_Mer t) {

		return null;
	}

	public CASDEN_Outre_Mer setData(String codePostale, String total, String nbAgences, String enseigne, String nomWeb,
			String adresse, String tel, String fax, String horairesHtm, String horairesCASDEN_Outre_Merxt,
			String services, String dab, String agenceHtm) {
		CASDEN_Outre_Mer casden = new CASDEN_Outre_Mer();
		casden.setCodePostale(codePostale).setTotal(total).setNbAgences(nbAgences).setEnseigne(enseigne)
				.setNomWeb(nomWeb).setAdresse(adresse).setTel(tel).setFax(fax).setHorairesHtm(horairesHtm)
				.setHorairesTxt(horairesCASDEN_Outre_Merxt).setServices(services).setDAB(dab).setAgenceHtm(agenceHtm);
		rep.save(casden);
		return casden;
	}

	public void setup() {

		htmlUnitDriver = (HtmlUnitDriver) this.setupHU();
	}

	public void tearDown() {
		htmlUnitDriver.quit();

	}

	@Override
	public void saveItem(String... args) {
		// TODO Auto-generated method stub

	}

	@Override
	public void scrap() throws Exception {
		String url = "";
		List<String> agencyLinks = new ArrayList<>();

		fichier_valeurs = Parametres.REPERTOIRE_DATA_SOURCES + ficval + ".txt";
		Scanner sc = new Scanner(new FileInputStream(new File(fichier_valeurs)));

		if (!new File(dossierRESU).exists()) {
			new File(dossierRESU).mkdirs();
		}
		// fichier = new File(dossierRESU + "\\" + "RESULTATS_" + nomSITE + "_" +
		// System.currentTimeMillis() + ".txt");
		LogScrap log = new LogScrap("BanqueCasden_dom.txt", LogConst.TYPE_LOG_FILE);
		num_ligneficval = ((new File(LogConst.STORE_LOG_PATH + LogConst.PREFIX_LOG_FILE + "_BanqueCasden_dom.txt"))
				.exists()) ? Integer.parseInt(log.readLog().getLastLineProcessed().split("\\t")[4]) : 1;

		parcoursficval = 1;
		while (sc.hasNextLine()) {
			String line = sc.nextLine();
			String[] fic_valeurs = line.split("\\t");
			if (parcoursficval > num_ligneficval) {

				nbRecuperes = 0;
				CPSoumis = fic_valeurs[0];
				// ville=fic_valeurs[1];
				url = url_accueil + fic_valeurs[1];
				System.out.println(url);
				try {
					htmlUnitDriver.get(url);

				} catch (Exception e) {
					if (e instanceof SocketTimeoutException) {
						htmlUnitDriver.get(url);

					}
				}

				String pageSource = "";
				Document doc = null;
				if (!agencyLinks.isEmpty()) {
					agencyLinks.clear();
				}
				pageSource = htmlUnitDriver.getPageSource();
				doc = Jsoup.parse(pageSource);
				Element rootJElement = doc.select("#main").first();
				Elements agencyLinksJElement = rootJElement
						.select("#agencies >li> div:nth-child(1) > h2 > span.seolink-bloc > a");
				for (Element elmntTmp : agencyLinksJElement) {
					String tmp = "";
					tmp = elmntTmp.attr("href");

					agencyLinks.add(tmp);
				}
				NbAgences = agencyLinks.size();
				System.out.println(NbAgences);

				nbRecuperes = 0;
				for (String strTmp : agencyLinks) {
					url = url_accueil + strTmp;
					System.out.println(url);
					try {
						htmlUnitDriver.get(url);

					} catch (Exception e) {
						if (e instanceof SocketTimeoutException) {
							htmlUnitDriver.get(url);

						}
					}

					String agences_HTM = "", nom_agence = "", adresse = "", tel = "", fax = "", HorairesHTM = "",
							HorairesTXT = "", services = "", enseigne = "";
					int total = 0, DAB = 0;
					total = NbAgences;
					pageSource = htmlUnitDriver.getPageSource();
					doc = Jsoup.parse(pageSource);

					Element root2JElement = doc.select("#content").first();
					agences_HTM = root2JElement.outerHtml().trim().replaceAll("\\s{1,}", "");
					agences_HTM = this.normalize(agences_HTM);
					// retrieve agency name
					Element agencyNameJElement = root2JElement.select("#aside>div>div> h2").first();
					nom_agence = (agencyNameJElement != null) ? Normalizer
							.normalize(agencyNameJElement.text(), Normalizer.Form.NFKD).replaceAll("[^\\p{ASCII}]", "")
							: "";
					System.out.println("nom " + nom_agence);
					// retrieve agency address
					Element agencyAddressJElement = root2JElement.select("#aside > div > div > div:nth-child(3) > div")
							.first();
					adresse = (agencyAddressJElement != null)
							? Normalizer.normalize(agencyAddressJElement.text(), Normalizer.Form.NFKD).replaceAll(
									"[^\\p{ASCII}]", "")
							: "";
					System.out.println("adresse " + adresse);

					// retrieve agency tel
					Element agencyTelJElement = root2JElement.select("#aside > div > div > a.agencytel").first();
					tel = (agencyTelJElement != null)
							? this.normalize(agencyTelJElement.text().replaceAll("[^0-9 ]", ""))
							: "";
					System.out.println("tel " + tel);

					// retrieve agency fax
					Element agencyFaxJElement = root2JElement.select("#aside > div > div > div.agencyfax > span")
							.first();
					fax = (agencyFaxJElement != null)
							? this.normalize(agencyFaxJElement.text().replaceAll("[^0-9 ]", ""))
							: "";
					System.out.println("fax " + fax);

					// retrieve agency schedule
					Elements agencyScheduleJElement = root2JElement.select("div.horaires-bloc > div > div.day");
					if (agencyScheduleJElement != null) {
						for (Element elementTmp : agencyScheduleJElement) {
							HorairesHTM += elementTmp.outerHtml().trim() + " ";
							HorairesTXT += elementTmp.text() + " ";
						}
						HorairesHTM = HorairesHTM.replaceAll("\\s{1,}", "");
					} else {
						HorairesTXT = "";
						HorairesHTM = "";
					}
					HorairesHTM = this.normalize(HorairesHTM);
					HorairesTXT = Normalizer.normalize((HorairesTXT), Normalizer.Form.NFKD).replaceAll("[^\\p{ASCII}]",
							"");
					System.out.println(HorairesTXT);
					// retrieve agency services
					Elements agencyServiceJElement = root2JElement
							.select("div.services-bloc.bloc-bp > div > ul > li.service-cell > span.text");

					if (agencyServiceJElement != null) {
						for (Element elementTmp : agencyServiceJElement) {
							services += elementTmp.text() + " ";
						}
						if (services.contains("Distributeur automatique de billets")) {
							DAB = 1;
						}
					} else {
						services = "";
						DAB = 0;
					}
					services = Normalizer.normalize(services, Normalizer.Form.NFKD).replaceAll("[^\\p{ASCII}]", "");
					System.out.println(services);
					// retrieve agency tag
					Element agencyTagJElement = root2JElement.select("#aside > div > div > a.agencyreseau").first();
					enseigne = (agencyTagJElement != null) ? agencyTagJElement.text() : "";

					nbRecuperes++;
					this.setData(CPSoumis, String.valueOf(total), String.valueOf(NbAgences), enseigne, nom_agence,
							adresse, tel, fax, HorairesHTM, HorairesTXT, services, String.valueOf(DAB), agences_HTM);

				}

				log.writeLog(new Date(), CPSoumis, "no_error", NbAgences, parcoursficval, NbAgences - nbRecuperes);
			}
			parcoursficval++;
		}

		sc.close();

	}

}
