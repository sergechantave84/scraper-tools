package com.app.service;

import java.io.BufferedInputStream;

import java.io.File;
import java.io.FileInputStream;

import java.net.SocketTimeoutException;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Scanner;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import org.openqa.selenium.htmlunit.HtmlUnitDriver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.app.model.MATMUT;

import com.app.scrap.RepMatmut;

import com.app.utils.Parametres;

import fa.log.LogApp.LogScrap;
import fa.log.config.LogConst;

@Service
public class ServiceMatmut extends Scraper {

	@Autowired
	private RepMatmut rep;

	private String ficVal = "lien_matmut_new";

	private String fichier_valeurs;

	private int num_ligneficval;
	private int currentLine;
	private int NbAgencesTOT;

	private HtmlUnitDriver htmlUnitDriver;

	@Override
	public void saveItem(String... args) {
		MATMUT matmut = new MATMUT();
		matmut.setNbAgences(args[0]).setNumAgence(args[1]).setLienAgence(args[2]).setNomAgence(args[3])
				.setAdresse(args[4]).setTel(args[5]).setFax(args[6]).setHoraires(args[7]).setAgenceHtm(args[8]).setDep(args[9]);
		rep.save(matmut);

	}

	/*
	 * public MATMUT setData(String nbAgences, String numAgence, String lienAgence,
	 * String nomAgence, String adresse, String tel, String fax, String horaires,
	 * String agenceHtm) { MATMUT matmut = new MATMUT();
	 * matmut.setNbAgences(nbAgences).setNumAgence(numAgence).setLienAgence(
	 * lienAgence).setNomAgence(nomAgence)
	 * .setAdresse(adresse).setTel(tel).setFax(fax).setHoraires(horaires).
	 * setAgenceHtm(agenceHtm); rep.save(matmut); return matmut; }
	 */

	public void setUp() throws Exception {
		htmlUnitDriver = (HtmlUnitDriver) this.setupHU();
	}

	@Override
	public void scrap() throws Exception {

		Document doc = null;
		String pageSource = "";
		List<String> agenciesLinks = new ArrayList<>();
		fichier_valeurs = Parametres.REPERTOIRE_DATA_SOURCES + ficVal + ".txt";
		Scanner sc = new Scanner(new BufferedInputStream(new FileInputStream(new File(fichier_valeurs))));

		LogScrap log = new LogScrap("AssuranceMATMUT.txt", LogConst.TYPE_LOG_FILE);
		num_ligneficval = ((new File(LogConst.STORE_LOG_PATH + LogConst.PREFIX_LOG_FILE + "_AssuranceMATMUT.txt"))
				.exists()) ? Integer.parseInt(log.readLog().getLastLineProcessed().split("\\t")[4]) : 1;

		currentLine = 1;
		NbAgencesTOT = 478;
        int num_agence=0;
		while (sc.hasNextLine()) {
			String lines[] = sc.nextLine().split("\\t");
			if (currentLine > num_ligneficval) {
				System.out.println(lines[1]);
				try {
					htmlUnitDriver.get(lines[1]);
				} catch (Exception e) {
					if (e instanceof SocketTimeoutException) {
						this.setUp();
						htmlUnitDriver.get(pageSource);
					}

				}
               

				pageSource = htmlUnitDriver.getPageSource();

				doc = Jsoup.parse(pageSource);

				String agencyName = "", agencySchedule = "", agencyPhone = "", agencyFax = "", agency = "",
						agencyAddress = "";

				// retrieve agency name
				Element agencyNameJsoupElement = doc.select("#location-name").first();

				agencyName = (agencyNameJsoupElement != null) ? this.normalize(agencyNameJsoupElement.text()) : "";
				System.out.println("name : " + agencyName);

				// retrieve agency html
				Element agencyHtmlJsoupElement = doc.select("#main").first();
				agency = this.normalize(agencyHtmlJsoupElement.outerHtml().trim());

				// retrieve agency coordinate
				Elements agencyAddressElements = doc.select("#address > a > div");
				for (Element agencyAddressElement : agencyAddressElements)
					agencyAddress += agencyAddressElement.text() + " ";
				System.out.println("address : " + agencyAddress);

				Element agencyPhoneElement = doc.selectFirst("#phone-main");
				agencyPhone = (agencyPhoneElement != null) ? agencyPhoneElement.text() : "";
				System.out.println("phone : " + agencyPhone);

				Elements agencySchedulesElements = doc.select(
						"#main > div > div.Core.js-core > div.Core-detailsWrapper > div > div > div > div.Core-hours > div > div > table > tbody");
				for (Element agencyScheduleElement : agencySchedulesElements)
					agencySchedule += agencyScheduleElement.text() + " ";
				System.out.println("horraires : " + agencySchedule);
                num_agence++;
                
                System.out.println("----------------------------\n");
				this.saveItem(String.valueOf(NbAgencesTOT), String.valueOf(num_agence), (lines[1]), agencyName,
						agencyAddress, agencyPhone, agencyFax, agencySchedule, agency,lines[0]);
				// }
				log.writeLog(new Date(), lines[0], "no-error", 0, currentLine, 0);
			}
			currentLine++;
		}

	}

	public void tearDown() {
		try {
			htmlUnitDriver.quit();
		} catch (Exception e) {

		}

	}

}
