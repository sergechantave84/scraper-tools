package com.app.service;

import java.io.BufferedInputStream;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.SocketTimeoutException;
import java.util.Date;
import java.util.Scanner;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.app.model.InAndFI;
import com.app.scrap.RepInAndFi;
import com.app.utils.Parametres;

import fa.log.LogApp.LogScrap;
import fa.log.config.LogConst;

@Service
public class ServiceInAndFi extends Scraper {

	@Autowired
	private RepInAndFi rep;

	String nomSITE = "CA";
	// TODO change
	String ficval = "lien_inandfi"; // liste des enseignes du CA

	String url = "https://www.inandfi-credits.fr";

	String fichier_valeurs;

	int NbVilles;
	int NbAgences;

	int num_ligneficval;
	int parcoursficval;

	HtmlUnitDriver htmlUnitDriver;
	// htmlUnitDriver htmlUnitDriver;
	BufferedWriter sortie;
	BufferedWriter sortie_log;
	Date Temps_Fin;
	
	ChromeDriver chromeDriver;

	@Override
	public void saveItem(String... args) {
		InAndFI courtier = new InAndFI();
		courtier.setNomAgence(args[0]).setAdresse(args[1]).setDepSoumis(args[2]).setLienAgence(args[3])
		.setHoraire(args[4]).setTel(args[5]).setHorairehtml(args[6]).setNomAgents(args[7]).setAgenceHTM(args[8])
		.setNbAgence(args[9]).setServices(args[10]);
		rep.save(courtier);
		
	}

	public void setup() {

		htmlUnitDriver = (HtmlUnitDriver) this.setupHU();
		
	}
	
	public String deleteAll() {
		try {
			
			rep.deleteAll();
			System.out.println("Supprimee avec success");
	
		} catch (Exception e) {
			e.printStackTrace();
		}
	
		return "removed";
	}

	@Override
	public void scrap() throws IOException, InterruptedException {
		// boolean documentReadyState = false;
		fichier_valeurs = Parametres.REPERTOIRE_DATA_SOURCES + ficval + ".txt";
		Scanner sc = new Scanner(new BufferedInputStream(new FileInputStream(new File(fichier_valeurs))));

		LogScrap log = new LogScrap("CourtierInAndFI.txt", LogConst.TYPE_LOG_FILE);
		num_ligneficval = ((new File(LogConst.STORE_LOG_PATH + LogConst.PREFIX_LOG_FILE + "_CourtierInAndFI.txt")).exists())
				? Integer.parseInt(log.readLog().getLastLineProcessed().split("\\t")[4])
				: 1;

		parcoursficval = 1;

		while (sc.hasNextLine()) {
			String line = sc.nextLine();

			if (parcoursficval > num_ligneficval) {

				String uri1 = url+line;
				try {
					System.out.println("Uri : " + uri1);
					htmlUnitDriver.get(uri1);

				} catch (Exception e) {
					if (e instanceof SocketTimeoutException) {
						htmlUnitDriver.get(uri1);

					}
				}
				
				String pageSource = htmlUnitDriver.getPageSource();
				Document doc = null;
				doc = Jsoup.parse(pageSource);

				Element root = doc.selectFirst(
						"#__next > div > main");
				
				Element name_elem = root.selectFirst("section.AgencyHero_agency-hero__Om_uZ > div > div.AgencyHero_agency-hero__content__dHt9d > h3");
				
				String nom_agence = name_elem != null? this.normalize(name_elem.text().replaceAll("\\s{2,}", " ").trim()) : "";
				
				Element add_elem = root.selectFirst("section.AgencyHero_agency-hero__Om_uZ > div > div.AgencyHero_agency-hero__card__YhHZ8 > address > p");
				
				String adresse_agence = add_elem != null? this.normalize(add_elem.text().replaceAll("\\s{2,}", " ").trim()) : "";
				
				Element tel_elem = root.selectFirst("section.AgencyHero_agency-hero__Om_uZ > div > div.AgencyHero_agency-hero__card__YhHZ8 > address > a");
				
				String tel_agence = tel_elem != null? this.normalize(tel_elem.text().replaceAll("\\s{2,}", " ").trim()) : "";
				
				Element horaire_elem = root.selectFirst("section.AgencyOpeningHours_agency-opening-hours__S_32L.container.grid--12.spacing--secondary--top.spacing--primary--bottom > div.AgencyOpeningHours_opening-hours__H68q2.col-span--4 > ul");
				
				String horaire_agence = horaire_elem != null? this.normalize(horaire_elem.text().replaceAll("\\s{2,}", " ").replaceAll("Horaires", "").trim()) : "";
				
				Elements equipes_elem = root.select("section.AgencyTeam_agency-team__sZL_k.padding--secondary--top > div > ul > li");
				
				Element service_elem = root.selectFirst("section.AgencyOpeningHours_agency-opening-hours__S_32L.container.grid--12.spacing--secondary--top.spacing--primary--bottom > div.AgencyOpeningHours_opening-hours__H68q2.col-span--4 > p");
				
				String services = service_elem != null ? this.normalize(service_elem.text().replaceAll("\\s{2,}", " ")): "";
				
				if(!horaire_agence.toLowerCase().contains("lun.") && !services.toLowerCase().contains("lundi")) {
					horaire_agence ="Lun. Ferme "+horaire_agence;
				}
				
				if(!horaire_agence.toLowerCase().contains("sam.") && !services.toLowerCase().contains("samedi")) {
					horaire_agence +=" Sam. Ferme ";
				}
				
				if(!horaire_agence.toLowerCase().contains("dim.") && !services.toLowerCase().contains("dimanche")) {
					horaire_agence +=" Dim. Ferme ";
				}
				
				String equipes_agence = "";
				
				if(equipes_elem.size()>0) {
					for(Element eq : equipes_elem) {
						equipes_agence+=eq.text()+" & ";
					}
				}
				
				String HorairesHTM = horaire_elem!= null ? horaire_elem.outerHtml() : "";
				
				NbAgences = 96;
				
				String dep_soumise= adresse_agence.split(",")[adresse_agence.split(",").length -1].substring(0,3);
				
				adresse_agence=adresse_agence.toUpperCase().replaceAll(",", "");
				
				
				if(nom_agence != "") {
					
					//nom, adresse, dep, lien, horaire, tel, horaire_html, agents, agence_html, nb_agence 
					
					this.saveItem(nom_agence, adresse_agence, dep_soumise, uri1,
							horaire_agence, tel_agence, HorairesHTM, equipes_agence, "", String.valueOf(NbAgences),services);
				}
				System.out.println("nom_agence : "+nom_agence);
				System.out.println("adresse_agence : "+adresse_agence);
				System.out.println("tel_agence : "+tel_agence);
				System.out.println("horaire_agence : "+horaire_agence);
				System.out.println("services : "+services);
				System.out.println("equipes_agence : "+equipes_agence);
				System.out.println("dep : "+dep_soumise);
				System.out.println("\n---------------------\n");
				
				
				log.writeLog(new Date(), line, "no_error", 1, parcoursficval, 0);
				
			}
			parcoursficval++;
		}
		sc.close();

	}
	

	public void tearDown() {
		htmlUnitDriver.quit();

	}

}
