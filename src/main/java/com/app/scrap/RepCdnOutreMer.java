package com.app.scrap;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


import com.app.model.CDN_Outre_Mer;

@Repository
public interface RepCdnOutreMer extends JpaRepository<CDN_Outre_Mer, Integer>, RepositoryCustom<CDN_Outre_Mer> {

}
