package com.app.model;

import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="mutuelle_poitier", schema="scrap")
public class Mutuelle_Poitier {

	@Id
	@SequenceGenerator(name="scrap.mutuelle_poitier_id_seq",
	                    sequenceName ="scrap.mutuelle_poitier_id_seq",
	                     allocationSize=1)
    @GeneratedValue( strategy=GenerationType.SEQUENCE,
                     generator="scrap.mutuelle_poitier_id_seq")
	  
	@Column(name="id",updatable=false)
	private int id;
	
	@Column(name="nbagences", columnDefinition="varchar")
	private String nbAgences;
	
	@Column(name="lienagence", columnDefinition="varchar")
	private String lienAgence;
	
	@Column(name="nom_agence", columnDefinition="varchar")
	private String nomAgence;
	
	@Column(name="adresse", columnDefinition="varchar")
	private String adresse;
	
	@Column(name="tel", columnDefinition="varchar")
	private String tel;
	
	@Column(name="noms_agents", columnDefinition="varchar")
	private String nomsAgents;
	
	@Column(name="orias", columnDefinition="varchar")
	private String orias;
	
	@Column(name="horaires", columnDefinition="varchar")
	private String horaires;
	
	@Column(name="agencehtm", columnDefinition="varchar")
	private String agenceHtm;
	
	@Column(name="annee_du_scrap" ,columnDefinition="varchar(4) DEFAULT date_part('year',CURRENT_DATE)")
	private String anneeScrap;
  
	public Mutuelle_Poitier() {
		LocalDate ld = new Date().toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
		anneeScrap = String.valueOf(ld.getYear() + 1);
	}
		
	public int getId() {
		return id;
	}

	public Mutuelle_Poitier setId(int id) {
		this.id = id;
		return this;
	}

	public String getNbAgences() {
		return nbAgences;
	}

	public Mutuelle_Poitier setNbAgences(String nbAgences) {
		this.nbAgences = nbAgences;
		return this;
	}

	public String getLienAgence() {
		return lienAgence;
	}

	public Mutuelle_Poitier setLienAgence(String lienAgence) {
		this.lienAgence = lienAgence;
		return this;
	}

	public String getNomAgence() {
		return nomAgence;
	}

	public Mutuelle_Poitier setNomAgence(String nomAgence) {
		this.nomAgence = nomAgence;
		return this;
	}

	public String getAdresse() {
		return adresse;
	}

	public Mutuelle_Poitier setAdresse(String adresse) {
		this.adresse = adresse;
		return this;
	}

	public String getTel() {
		return tel;
	}

	public Mutuelle_Poitier setTel(String tel) {
		this.tel = tel;
		return this;
	}

	public String getNomsAgents() {
		return nomsAgents;
	}

	public Mutuelle_Poitier setNomsAgents(String nomsAgents) {
		this.nomsAgents = nomsAgents;
		return this;
	}

	public String getOrias() {
		return orias;
	}

	public Mutuelle_Poitier setOrias(String orias) {
		this.orias = orias;
		return this;
	}

	public String getHoraires() {
		return horaires;
	}

	public Mutuelle_Poitier setHoraires(String horaires) {
		this.horaires = horaires;
		return this;
	}

	public String getAgenceHtm() {
		return agenceHtm;
	}

	public Mutuelle_Poitier setAgenceHtm(String agenceHtm) {
		this.agenceHtm = agenceHtm;
		return this;
	}

	public String getAnneeScrap() {
		return anneeScrap;
	}

	public Mutuelle_Poitier setAnneeScrap(String anneeScrap) {
		this.anneeScrap = anneeScrap;
		return this;
	}
}
