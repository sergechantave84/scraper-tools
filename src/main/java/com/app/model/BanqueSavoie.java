package com.app.model;

import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="banque_savoie", schema="scrap")
public class BanqueSavoie {

	 @Id
	 @SequenceGenerator(name="scrap.banque_savoie_id_seq",
	                     sequenceName ="scrap.banque_savoie_id_seq",
	                    allocationSize=1)
	 @GeneratedValue(strategy=GenerationType.SEQUENCE,
	                  generator="scrap.banque_savoie_id_seq")
	 @Column( name="id",updatable=false)
	 private int id;
	 
	 @Column(name="departement_soumis", length=500)
	 private String DepSoumis;
	 
	 @Column(name="nombre_agences",length=100)
	 private String nbAgences;
	 
	 @Column(name="enseigne", length=500)
	 private String enseigne;
	 
	 @Column(name="adresse", length=500)
	 private String adresse;
	 
	 @Column(name="telephone", length=500)
	 private String tel;
	 
	 @Column(name="fax", length=500)
	 private String fax;
	 
	 @Column(name="horaireshtm", columnDefinition="varchar")
	 private String horairesHtm;
	 
	 @Column(name="horairestxt", length=500)
	 private String horaires;
	 
	 @Column(name="services", length=500)
	 private String services;
	 
	 @Column(name="dab", length=500)
	 private String dab;
	 
	 @Column(name="agencehtm", columnDefinition = "varchar")
	 private String agenceHtm;
	 
	 @Column(name="nom_web", length=500)
	 private String nomWeb;
	 
	 @Column(name="annee_du_scrap" ,columnDefinition="varchar(4) DEFAULT date_part('year',CURRENT_DATE)")
	    private String anneeScrap;
	    
		public BanqueSavoie() {
			LocalDate ld= new Date().toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
			anneeScrap=String.valueOf(ld.getYear()+1);
		}

		public int getId() {
			return id;
		}

		public BanqueSavoie setId(int id) {
			this.id = id;
			return this;
		}

		public String getDepSoumis() {
			return DepSoumis;
		}

		public BanqueSavoie setDepSoumis(String depSoumis) {
			DepSoumis = depSoumis;
			return this;
		}

		public String getNbAgences() {
			return nbAgences;
		}

		public BanqueSavoie setNbAgences(String nbAgences) {
			this.nbAgences = nbAgences;
			return this;
		}

		public String getEnseigne() {
			return enseigne;
		}

		public BanqueSavoie setEnseigne(String enseigne) {
			this.enseigne = enseigne;
			return this;
		}

		public String getAdresse() {
			return adresse;
		}

		public BanqueSavoie setAdresse(String adresse) {
			this.adresse = adresse;
			return this;
		}

		public String getTel() {
			return tel;
		}

		public BanqueSavoie setTel(String tel) {
			this.tel = tel;
			return this;
		}

		public String getFax() {
			return fax;
		}

		public BanqueSavoie setFax(String fax) {
			this.fax = fax;
			return this;
		}

		public String getHorairesHtm() {
			return horairesHtm;
		}

		public BanqueSavoie setHorairesHtm(String horairesHtm) {
			this.horairesHtm = horairesHtm;
			return this;
		}

		public String getHoraires() {
			return horaires;
		}

		public BanqueSavoie setHoraires(String horaires) {
			this.horaires = horaires;
			return this;
		}

		public String getServices() {
			return services;
		}

		public BanqueSavoie setServices(String services) {
			this.services = services;
			return this;
		}

		public String getDab() {
			return dab;
		}

		public BanqueSavoie setDab(String dab) {
			this.dab = dab;
			return this;
		}

		public String getAgenceHtm() {
			return agenceHtm;
		}

		public BanqueSavoie setAgenceHtm(String agenceHtm) {
			this.agenceHtm = agenceHtm;
			return this;
		}

		public String getNomWeb() {
			return nomWeb;
		}

		public BanqueSavoie setNomWeb(String nomWeb) {
			this.nomWeb = nomWeb;
			return this;
		}

		public String getAnneeScrap() {
			return anneeScrap;
		}

		public BanqueSavoie setAnneeScrap(String anneeScrap) {
			this.anneeScrap = anneeScrap;
			return this;
		}
}
