package com.app.service;

import java.io.IOException;
import java.net.SocketTimeoutException;
import java.text.Normalizer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.openqa.selenium.WebDriver;

public interface SuperClassPJ {

	default String iscaptcha(String html, WebDriver wd) throws InterruptedException {

		while (html.toLowerCase().contains("id=\"cf-captcha-bookmark\"")) {
			// Thread.sleep(120000);
			html = wd.getPageSource().toLowerCase();
		}

		return html;
	}

	/**
	 * this method check the page contents,
	 * 
	 * @param String html
	 * @param String goTo
	 * @throws IOException
	 * @throws InterruptedException
	 */
	default String checkValidityOfPage(WebDriver wd, String url, String html) throws IOException, InterruptedException {

		int i = 0;
		while (html.contains("nous sommes desoles, nous rencontrons un probleme technique")) {
			i++;
			System.out.println("la page html n'est pas valide");
			wd.quit();

			if (i == 4) {
				break;
			}
			try {
				wd.get(url);

			} catch (Exception e) {
				if (e instanceof SocketTimeoutException) {
					wd.quit();
					wd.get(url);
				}
			}

			html = Normalizer.normalize(wd.getPageSource().toLowerCase(), Normalizer.Form.NFKD)
					.replaceAll("[^\\p{ASCII}]", "");

		}
		return html;

	}

	/**
	 * this methode check if request don't have response
	 * 
	 * @param String html page source
	 * @param String activiteS activité soumise
	 * @param String cp code postale
	 * @param String communeS commune soumise
	 */
	default boolean noResponse(String html) {
		return html.contains("oups-no-response") || html.contains("wording-no-responses") || html.contains("no-result")
				|| html.contains("nous n’avons pas encore de reponse a cette recherche!") || html.contains("resultats a proximite")
				||html.contains("aucun resultat")|| html.contains("probleme technique");
	}

	/***
	 * retrieve value that we need which contained in string
	 * 
	 * @param String str
	 */
	default String retrieveValueInString(String str, String pattern1, String pattern2) {
		String result = "";
		int supplement = 0, beginIndex = 0, endIndex = 0;
		switch (pattern1) {
		case "quoiqui=": {
			supplement = 8;
			beginIndex = str.indexOf(pattern1) + supplement;
			endIndex = str.indexOf(pattern2);
			result = str.substring(beginIndex, endIndex);
		}
			break;
		case "contexte=": {
			supplement = 9;
			beginIndex = str.indexOf(pattern1) + supplement;
			endIndex = str.indexOf(pattern2);
			result = str.substring(beginIndex, endIndex);
		}
			break;
		case "idOu=": {
			supplement = 5;
			beginIndex = str.indexOf(pattern1) + supplement;
			result = str.substring(beginIndex);
		}
			break;

		}

		return result.replace("%20", "-");
	}

	default boolean isNOCorrectUrl(String html) throws InterruptedException {

		return html.contains("filariane");

	}

	default void goTo(String uri, WebDriver wd) throws IOException {

		try {
			wd.get(uri);
		} catch (Exception e) {
			if (e instanceof SocketTimeoutException) {
				wd.close();
				wd.get(uri);
			}
		}

	}

	/**
	 * this method rerieve all activity of society
	 */
	default String retrieveGeneralActivity(String html) {
		String activite = "";
		int index = 0, indexSumm = 0;
		String[] arrayMatch = { "p", "o", "u", "r" };
		Elements blocGeneralActivity = null;
		Document doc = Jsoup.parse(html);
		if (html.contains("main-title pj-on-autoload"))
			blocGeneralActivity = doc.select("div[class=\"main-title pj-on-autoload\"] > h1");
		else
			blocGeneralActivity = doc.select("div[class=\"reformulation\"] > h1");

		activite = blocGeneralActivity.text().toLowerCase();
		for (String str : arrayMatch) {
			index = activite.indexOf(str);
			indexSumm += index;

		}
		if (indexSumm == 6)
			activite = blocGeneralActivity.text().toLowerCase().replace("pour", "").trim();
		return activite;
	}

	default boolean isNotExpectedResponse(String html) {

		Document doc = Jsoup.parse(html);
		Element h1 = doc
				.selectFirst("#main > div.main-content.clearfix > div > div.zone-reformulation.clearfix > div > h1");
		if (h1 == null)
			h1 = doc.selectFirst(
					"#main > div.main-content.clearfix > div > div.zone-reformulation.clearfix > div > div > h1");
		return h1.text().contains("autour de");
	}
	
	default int retrievePageNumber(Document doc) {
		String pageNumber="";
		
		Element elementPageNumber = doc.selectFirst("#sel-compteur");
		String pageNumberTmp = elementPageNumber.text();
		Pattern pattern = Pattern.compile("/ \\d{1,}");
		Matcher match = pattern.matcher(pageNumberTmp);
		while (match.find())
			pageNumber = match.group().replaceAll("/", " ").trim();
		return Integer.parseInt(pageNumber);
	}
}
