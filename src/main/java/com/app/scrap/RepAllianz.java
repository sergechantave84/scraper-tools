package com.app.scrap;

import org.springframework.stereotype.Repository;
import org.springframework.data.jpa.repository.JpaRepository;
import com.app.model.Allianz;

@Repository
public interface RepAllianz extends JpaRepository<Allianz,Integer> , RepositoryCustom<Allianz>{

}
