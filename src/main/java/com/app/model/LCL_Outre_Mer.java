package com.app.model;

import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="banque_lcl_dep_outre_mer", schema="scrap")
public class LCL_Outre_Mer {
	@Id
	@SequenceGenerator(name="scrap.banque_lcl_dep_outre_mer_id_seq",
	                    sequenceName ="scrap.banque_lcl_dep_outre_mer_id_seq",
	                     allocationSize=1)
    @GeneratedValue( strategy=GenerationType.SEQUENCE,
                     generator="scrap.banque_lcl_dep_outre_mer_id_seq")
	  
	@Column(name="id",updatable=false)
	private int id;
	
	@Column(name="valeursoumise", columnDefinition="varchar")
	private String valeurSoumise;
	
	@Column(name="nbagences", columnDefinition="varchar")
	private String nbAgences;
	
	@Column(name="nblienstrouves", columnDefinition="varchar")
	private String nbLiensTrouves;
	
	@Column(name="numagence", columnDefinition="varchar")
	private String numAgence;
	
	@Column(name="nomweb", columnDefinition="varchar")
	private String nomWeb;
	
	@Column(name="adresse", columnDefinition="varchar")
	private String adresse;
	
	@Column(name="tel", columnDefinition="varchar")
	private String tel;
	
	@Column(name="fax", columnDefinition="varchar")
	private String fax;
	
	@Column(name="horaireshtm", columnDefinition="varchar")
	private String horairesHtm;
	
	@Column(name="horairestxt", columnDefinition="varchar")
	private String horairesTxt;
	
	@Column(name="services", columnDefinition="varchar")
	private String services;
	
	@Column(name="agencehtm", columnDefinition="varchar")
	private String agenceHtm;
	
	@Column(name="annee_du_scrap" ,columnDefinition="varchar(4) DEFAULT date_part('year',CURRENT_DATE)")
	private String anneeScrap;
	
	public LCL_Outre_Mer() {
		LocalDate ld = new Date().toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
		anneeScrap = String.valueOf(ld.getYear() + 1);
	}
	public int getId() {
		return id;
	}
	public LCL_Outre_Mer setId(int id) {
		this.id = id;
		return this;
	}
	public String getValeurSoumise() {
		return valeurSoumise;
	}
	public LCL_Outre_Mer setValeurSoumise(String valeurSoumise) {
		this.valeurSoumise = valeurSoumise;
		return this;
	}
	public String getNbAgences() {
		return nbAgences;
	}
	public LCL_Outre_Mer setNbAgences(String nbAgences) {
		this.nbAgences = nbAgences;
		return this;
	}
	public String getNbLiensTrouves() {
		return nbLiensTrouves;
	}
	public LCL_Outre_Mer setNbLiensTrouves(String nbLiensTrouves) {
		this.nbLiensTrouves = nbLiensTrouves;
		return this;
	}
	public String getNumAgence() {
		return numAgence;
	}
	public LCL_Outre_Mer setNumAgence(String numAgence) {
		this.numAgence = numAgence;
		return this;
	}
	public String getNomWeb() {
		return nomWeb;
	}
	public LCL_Outre_Mer setNomWeb(String nomWeb) {
		this.nomWeb = nomWeb;
		return this;
	}
	public String getAdresse() {
		return adresse;
	}
	public LCL_Outre_Mer setAdresse(String adresse) {
		this.adresse = adresse;
		return this;
	}
	public String getTel() {
		return tel;
	}
	public LCL_Outre_Mer setTel(String tel) {
		this.tel = tel;
		return this;
	}
	public String getFax() {
		return fax;
	}
	public LCL_Outre_Mer setFax(String fax) {
		this.fax = fax;
		return this;
	}
	public String getHorairesHtm() {
		return horairesHtm;
	}
	public LCL_Outre_Mer setHorairesHtm(String horairesHtm) {
		this.horairesHtm = horairesHtm;
		return this;
	}
	public String getHorairesTxt() {
		return horairesTxt;
	}
	public LCL_Outre_Mer setHorairesTxt(String horairesTxt) {
		this.horairesTxt = horairesTxt;
		return this;
	}
	public String getServices() {
		return services;
	}
	public LCL_Outre_Mer setServices(String services) {
		this.services = services;
		return this;
	}
	public String getAgenceHtm() {
		return agenceHtm;
	}
	public LCL_Outre_Mer setAgenceHtm(String agenceHtm) {
		this.agenceHtm = agenceHtm;
		return this;
	}
	public String getAnneeScrap() {
		return anneeScrap;
	}
	public LCL_Outre_Mer setAnneeScrap(String anneeScrap) {
		this.anneeScrap = anneeScrap;
		return this;
	}
}
