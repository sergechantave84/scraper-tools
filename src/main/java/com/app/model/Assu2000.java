package com.app.model;

import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="assu2000", schema="scrap")
public class Assu2000 {
   
	@Id
	@SequenceGenerator(name="scrap.assu2000_id_seq",
	                     sequenceName ="scrap.assu2000_id_seq",
	                     allocationSize=1)
    @GeneratedValue( strategy=GenerationType.SEQUENCE,
                     generator="scrap.assu2000_id_seq")
	  
	@Column(name="id",updatable=false)
	private int id;
	
	@Column(name="ville_soumise", columnDefinition = "varchar") 
	private String dep;
	
	@Column(name="lien_agence", columnDefinition="varchar")
	private String lienAgence;
	
	@Column(name="nom_agence", columnDefinition="varchar")
	private String nomAgence;
	
	@Column(name="adresse", columnDefinition="varchar")
	private String adresse;
	
	@Column(name="tel", columnDefinition="varchar")
	private String tel;
	
	@Column(name="horaires", columnDefinition="varchar")
	private String horaires;
	
	
	
	@Column(name="annee_du_scrap" ,columnDefinition="varchar(4) DEFAULT date_part('year',CURRENT_DATE)" )
	private String anneeScrap;
	
	public Assu2000() {
		LocalDate ld= new Date().toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
		anneeScrap=String.valueOf(ld.getYear()+1);
	}
	
	
	public String getLienAgence() {
		return lienAgence;
	}
	public Assu2000 setLienAgence(String lienAgence) {
		this.lienAgence = lienAgence;
		return this;
	}
	public String getNomAgence() {
		return nomAgence;
	}
	public Assu2000 setNomAgence(String nomAgence) {
		this.nomAgence = nomAgence;
		return this;
	}
	public String getAdresse() {
		return adresse;
	}
	public Assu2000 setAdresse(String adresse) {
		this.adresse = adresse;
		return this;
	}
	public String getTel() {
		return tel;
	}
	public Assu2000 setTel(String tel) {
		this.tel = tel;
		return this;
	}
	public String getHoraires() {
		return horaires;
	}
	public Assu2000 setHoraires(String horaires) {
		this.horaires = horaires;
		return this;
	}
	
	public String getAnneeScrap() {
		return anneeScrap;
	}
	public Assu2000 setAnneeScrap(String anneeScrap) {
		this.anneeScrap = anneeScrap;
		return this;
	}


	public String getDep() {
		return dep;
	}


	public Assu2000 setDep(String dep) {
		this.dep = dep;
		return this;
	}
}
