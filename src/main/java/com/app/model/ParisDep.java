package com.app.model;

import java.util.ArrayList;
import java.util.List;

public class ParisDep {
      
	private List<String> parisDep=new ArrayList<>();
	
	public ParisDep() {
		this.parisDep.add("75001");
		this.parisDep.add("75002");
		this.parisDep.add("75003");
		this.parisDep.add("75004");
		this.parisDep.add("75005");
		this.parisDep.add("75006");
		this.parisDep.add("75007");
		this.parisDep.add("75008");
		this.parisDep.add("75009");
		this.parisDep.add("75010");
		this.parisDep.add("75011");
		this.parisDep.add("75012");
		this.parisDep.add("75013");
		this.parisDep.add("75014");
		this.parisDep.add("75015");
		this.parisDep.add("75016");
		this.parisDep.add("75017");
		this.parisDep.add("75018");
		this.parisDep.add("75019");
		this.parisDep.add("75020");
	}
	
	public List<String> getListParisDep(){
		return this.parisDep;
	}
	
	public void setListParisDep(String str) {
		this.parisDep.add(str);
	}
}
