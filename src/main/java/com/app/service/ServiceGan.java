package com.app.service;

import java.io.BufferedInputStream;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.net.SocketTimeoutException;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Scanner;

import org.json.JSONObject;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.NoAlertPresentException;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.app.model.GAN;
import com.app.scrap.RepGan;
import com.app.utils.Parametres;
import com.app.utils.Utilitaires;

import fa.browserUtils.FactoryBrowser;
import fa.browserUtils.HtmlUnitBrowser;
import fa.log.LogApp.LogScrap;
import fa.log.config.LogConst;

@Service
public class ServiceGan extends Utilitaires {

	@Autowired
	private RepGan rep;

	private List<com.app.model.GAN> ganList = new ArrayList<>();
	private String nomSITE = "GAN";
	private String dossierRESU = Parametres.REPERTOIRE_RESULT + "\\ASSU\\" + nomSITE;;
	private String url_accueil = "https://www.agence.gan.fr";

	private int NbAgences;
	private int NbAgences_page;

	private int parcoursficval;
	private int num_ligneficval = 0;
	private HtmlUnitDriver htmlUnitDriver;
	private BufferedWriter sortie;
	private BufferedWriter sortie_log;

	public GAN saveData(GAN t) {

		return rep.save(t);

	}

	public List<GAN> saveDataAll(List<GAN> t) {

		return rep.saveAll(t);

	}

	public List<GAN> getElmnt() {

		return rep.findAll();
	}

	public GAN getById(int id) {

		return rep.findById(id).orElse(null);
	}

	public String deleteProduct(int id) {
		try {
			rep.deleteById(id);

		} catch (Exception e) {
			e.printStackTrace();
		}

		return "removed";
	}

	public GAN updateProduct(GAN t) {
		// GANODO Auto-generated method stub
		return null;
	}

	public GAN setData(String depSoumis, String lienSoumis, String nbAgences, String lienAgence, String nomAgence,
			String adresse, String tel, String nomAgents, String orias, String horaires, String agenceHtm,
			String interlocuteurHtm) {
		GAN gan = new GAN();
		gan.setDepSoumis(depSoumis).setLienSoumis(lienSoumis).setNbAgences(nbAgences).setLienAgence(lienAgence)
				.setNomAgence(nomAgence).setAdresse(adresse).setTel(tel).setNomAgents(nomAgents).setOrias(orias)
				.setHoraires(horaires).setAgenbceHtm(agenceHtm).setInterlocuteurHtm(interlocuteurHtm);
		this.saveData(gan);
		return gan;
	}

	public List<GAN> showFirstRows() {
		List<GAN> a = rep.getFirstRow(100, GAN.class);
		return a;
	}

	public List<GAN> showLastRows() {
		List<GAN> a = rep.getLastRow(100, GAN.class);
		return a;
	}

	public void setUp() throws Exception {
		FactoryBrowser fact = new FactoryBrowser();
		HtmlUnitBrowser hB = (HtmlUnitBrowser) fact.create("htmlUnit");
		htmlUnitDriver = hB.setOptions(false, false, false, 60000).initBrowserDriver();
	}

	public void open_GAN_byselenium() throws Exception {

		String fichierValeur = Parametres.REPERTOIRE_DATA_SOURCES + "lien_gan.txt";
		Scanner sc = new Scanner(new BufferedInputStream(new FileInputStream(new File(fichierValeur))));
		if (!new File(dossierRESU).exists())
			new File(dossierRESU).mkdirs();

		List<String> linkList = new ArrayList<>();
		Document doc = null;

		fichier = new File(dossierRESU + "\\" + "RESULTATS_" + nomSITE + "_" + System.currentTimeMillis() + ".txt");
		LogScrap log = new LogScrap("AssuranceGAN.txt", LogConst.TYPE_LOG_FILE);
		num_ligneficval = ((new File(LogConst.STORE_LOG_PATH + LogConst.PREFIX_LOG_FILE + "_AssuranceGAN.txt")).exists())
				? Integer.parseInt(log.readLog().getLastLineProcessed().split("\\t")[4])
				: 1;
		
		parcoursficval = 1;

		while (sc.hasNextLine()) {
			String lines[] = sc.nextLine().split("\\t");
			if (parcoursficval > num_ligneficval) {
				String url = url_accueil + lines[1];
				System.out.println(url);
				try {

					htmlUnitDriver.get(url);
				} catch (Exception e) {

					if (e instanceof SocketTimeoutException) {
						this.tearDown();
						this.setUp();
						htmlUnitDriver.get(url);

					}
				}
				String pageSource = htmlUnitDriver.findElement(By.cssSelector("html")).getAttribute("outerHTML");
				doc = Jsoup.parse(pageSource);
				Elements agencyLinks = doc.select("#lf-content > " + "div.lf-geo-divisions__main-content__locations > "
						+ "ul > " + "li > "
						+ "a.lf-location-default__content.lf-geo-divisions__main-content__locations__list__location__content");
				NbAgences = (!agencyLinks.isEmpty()) ? agencyLinks.size() : 0;
				if (!linkList.isEmpty())
					linkList.clear();
				if (NbAgences > 0) {
					for (Element agencyLink : agencyLinks) {
						linkList.add(agencyLink.attr("href"));
					}

					for (String linkTmp : linkList) {
						System.out.println("agency link: " + linkTmp);
						String urlForAgencyInfo = "https://www.agence.gan.fr" + linkTmp;
						System.out.println("url : "+urlForAgencyInfo);
						try {
							htmlUnitDriver.get(urlForAgencyInfo);
							System.out.println("on c'est connecté");
						} catch (Exception e) {
							if (e instanceof SocketTimeoutException) {
								this.tearDown();
								this.setUp();
								htmlUnitDriver.get(urlForAgencyInfo);

							}
						}

						pageSource = htmlUnitDriver.findElement(By.cssSelector("html")).getAttribute("outerHTML");
						doc = Jsoup.parse(pageSource);
						String nomAgence = "", adresse = "", tel = "", horaires_HTM = "", nomAgents = "", ORIAS = "",
								nomagents = "", agence_htm = "", interlocuteur_htm = "";

						Element bigBlockInfo = doc.select("#lf-location").first();
						if (bigBlockInfo != null) {
							agence_htm = bigBlockInfo.outerHtml();
							agence_htm = this.normalize(agence_htm.replaceAll("\\s{1,}", ""));
							// #lf-body > script:nth-child(8)
							Element agencyInfoJsoupElement = doc.select("#lf-body>script[type=\"application/ld+json\"]")
									.first();
							String agencyInfo = agencyInfoJsoupElement.html();
							try {
								agencyInfo = agencyInfo.replaceAll("//\\<\\!\\[CDATA\\[", "");
								agencyInfo = agencyInfo.replaceAll("//\\]{1,}>", "");
								agencyInfo = agencyInfo.replaceAll("\\s{2,}", "");
								JSONObject json = new JSONObject(agencyInfo);

								// retrieve agence name
								nomAgence = this.normalize(json.getString("name"));
								System.out.println("agence name : " + nomAgence);

								// retrieve phone
								tel = json.getString("telephone");
								tel = tel.replaceAll("[^0-9]", "");
								System.out.println("tel : " + tel);

								// retrieve adresse
								JSONObject jsonAdd = json.getJSONObject("address");
								adresse = jsonAdd.getString("streetAddress") + " " + jsonAdd.getString("postalCode")
										+ " " + jsonAdd.getString("addressLocality");
								adresse = this.normalize(adresse);
								System.out.println("adresse : " + adresse);

								// retrieve schedule
								
								horaires_HTM = json.getString("openingHours").toLowerCase();
								horaires_HTM = horaires_HTM.replaceAll("mo", "lundi");
								horaires_HTM = horaires_HTM.replaceAll("tu", "mardi");
								horaires_HTM = horaires_HTM.replaceAll("we", "mercredi");
								horaires_HTM = horaires_HTM.replaceAll("th", "jeudi");
								horaires_HTM = horaires_HTM.replaceAll("fr", "vendredi");
								horaires_HTM = this.normalize(horaires_HTM);
								
								
								System.out.println("horaires : " + horaires_HTM);
							} catch (Exception e ) {
								e.printStackTrace();
							}
						}
						Element agentBlockInfo = doc.selectFirst("#lf-agents-slider");

						if (agentBlockInfo != null) {
							Elements agents = agentBlockInfo.select("div.lf-slider-swiper__wrapper.swiper-wrapper>"
									+ "div.lf-slider-swiper__wrapper__slide.swiper-slide ");
							interlocuteur_htm = agents.outerHtml();
							interlocuteur_htm = this.normalize(interlocuteur_htm.replaceAll("\\s{1,}", ""));
							for (Element tmp : agents) {
								// retrieve agent name
								Element agentNameElement = tmp.selectFirst("div>.lf-persons-item__name");
								if (agentNameElement != null) {
									nomAgents += agentNameElement.text() + " & ";
								}

								// retrieve orias
								Element oriasElement = tmp.selectFirst(".lf-persons-item__orias");
								if (oriasElement != null) {
									ORIAS += oriasElement.text() + " & ";
								}

							}
							nomAgents = this.normalize(nomAgents);
							ORIAS = ORIAS.replaceAll("[^0-9&]{1,}", "");
							ORIAS = this.normalize(ORIAS.replaceAll("\\s{1,}", ""));
							System.out.println(nomAgents + " " + ORIAS);
						}
						System.out.println("nomAgents : "+nomAgents);
						System.out.println("ORIAS : "+ORIAS);

					this.setData(lines[0], url, String.valueOf(NbAgences), urlForAgencyInfo, nomAgence,
								adresse, tel, nomAgents, ORIAS, horaires_HTM, agence_htm, interlocuteur_htm);
						System.out.println("-----------------------------------\n");
					}
				}
				log.writeLog(new Date(), lines[0], "no_error", NbAgences, parcoursficval, 0);
			}

			parcoursficval++;
		}
		// return ganList;
	}

	public void enregistrer(String lien_agence, String nom_agence, String adresse, String tel, String nomagents,
			String orias, String Horaires, String agence, String interlocuteur) {

//    	this.fichier = new File(Parametres.REPERTOIRE + "RESULTATS_BNP.");
		try {

			sortie = new BufferedWriter(new FileWriter(fichier, true));
			sortie.write(NbAgences + "\t"

					+ NbAgences_page + "\t" + lien_agence + "\t" + nom_agence + "\t" + adresse + "\t" + tel + "\t"
					+ nomagents + "\t" + orias + "\t" + Horaires + "\t" + agence + "\t" + interlocuteur + "\r\n");

			sortie.close();
		} catch (IOException e) {

			e.printStackTrace();
		}

	}

	/**
	 * La m�thode <code>initialiser()</code> permet d'ajouter la ligne d'ent�te dans
	 * le fichier r�sultat.
	 */

	public void initialiser() {

		try {
			// Attention pas d'accent dans les noms de colonnes !
			sortie = new BufferedWriter(new FileWriter(fichier, true));
			sortie.write("NbAgences" + "\t" + "NB_PAGES" + "\t" + "NUM_PAGE" + "\t" + "NbAgences_page" + "\t"
					+ "LIEN_AGENCE" + "\t" + "NOM_AGENCE" + "\t" + "ADRESSE" + "\t" + "TEL" + "\t" + "NOM_AGENTS" + "\t"
					+ "ORIAS" + "\t" + "HORAIRESHTM" + "\t" + "AGENCEHTM" + "\t" + "INTERLOCUTEURHTM" + "\r\n");
			sortie.close();

			File toto = new File(dossierRESU + "\\" + "LOG_" + nomSITE + ".csv");
			if (!toto.exists()) {
				// if (num_page == num_ligne) {
				sortie_log = new BufferedWriter(new FileWriter(log, true));
				sortie_log.write("num_page" + ";" + "FinValeur" + ";" + "NbAgences_page" + ";" + "Nbrecuperes" + ";"
						+ "Reste" + ";" + "Heure_Fin" + "\r\n");
				sortie_log.close();
			}

		} catch (IOException e) {

			e.printStackTrace();
		}
	}

	public void tearDown() {
		htmlUnitDriver.quit();

	}

	public static boolean isAlertPresent(FirefoxDriver chromeDriver) {
		try {
			chromeDriver.switchTo().alert();
			return true;
		} catch (NoAlertPresentException e) {
			return false;
		}
	}

}
