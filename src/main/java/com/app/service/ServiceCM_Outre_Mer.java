package com.app.service;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.SocketTimeoutException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Scanner;
import java.util.regex.Pattern;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.app.model.CM_Outre_Mer;
import com.app.scrap.RepCmOutreMer;
import com.app.utils.BrowserUtils;
import com.app.utils.Parametres;
import com.app.utils.Utilitaires;

import fa.browserUtils.FactoryBrowser;
import fa.browserUtils.HtmlUnitBrowser;
import fa.log.LogApp.LogScrap;
import fa.log.config.LogConst;

@Service
public class ServiceCM_Outre_Mer extends Utilitaires {

	@Autowired
	private RepCmOutreMer rep;

	private List<CM_Outre_Mer> cmList = new ArrayList<>();

	String driverPath = "C:\\ChromeDriver\\";
	String nomSITE = "CM_Outre_Mer_Agences";
	String ficval = "lienCM_DOM";

	String dossierRESU = Parametres.REPERTOIRE_RESULT + "\\BQ\\" + nomSITE;
	String url_debut = "https://www.creditmutuel.fr/fr/banques/contact/trouver-une-agence/BrowseLocality.aspx?";

	// ACQUISITION DE LIEN VIA JS
	/*
	 * let tr = document.
	 * querySelectorAll("#ei_tpl_content > div > article > section > div > div.a_blocfctl.lister > table > tbody > tr"
	 * ); let j = 1 for(let i of tr){
	 * 
	 * let cp = i.querySelector("td:nth-child(2)").textContent; let lien =
	 * i.querySelector("td:nth-child(1) > a").href; let test = /^(97)/
	 * if(cp.match(test)){ //console.log("NUMERO : " + j) console.log(cp + "\t" +
	 * lien); } j++;
	 * 
	 * }
	 */

	String fichier_valeurs;
	String CPSoumis;
	String ligne;

	int num_ligne = 2;
	int NbVilles;
	int NbAgences;
	int numreponse = 1;
	int nbRecuperes;
	int num_ligneficval;
	int parcoursficval;
	int Resultats;

	HtmlUnitDriver htmlUnitDriver;
	BufferedWriter sortie;
	BufferedWriter sortie_log;

	Date Temps_Fin;

	public CM_Outre_Mer saveData(CM_Outre_Mer t) {

		return rep.save(t);

	}

	public List<CM_Outre_Mer> saveDataAll(List<CM_Outre_Mer> t) {

		return rep.saveAll(t);

	}

	public List<CM_Outre_Mer> getElmnt() {

		return rep.findAll();
	}

	public CM_Outre_Mer getById(int id) {

		return rep.findById(id).orElse(null);
	}

	public String deleteProduct(int id) {
		try {
			rep.deleteById(id);

		} catch (Exception e) {
			e.printStackTrace();
		}

		return "removed";
	}

	public CM_Outre_Mer updateProduct(CM_Outre_Mer t) {
		// CM_Outre_MerODO Auto-generated method stub
		return null;
	}

	public CM_Outre_Mer setData(String depSoumis, String nbVilles, String numVilles, String villeSoumise,
			String nbAgences, String nomWeb, String adresse, String horaire, String telephone, String lienAgence,
			String agenceHtm, String fax, String codeGuchet, int gab, int dab, int borneDepot, String latitude,
			String longitude) {
		CM_Outre_Mer cm = new CM_Outre_Mer();
		cm.setDepSoumis(depSoumis).setNbVilles(nbVilles).setNumVilles(numVilles).setVilleSoumise(villeSoumise)
				.setNbAgences(nbAgences).setNomWeb(nomWeb).setAdresse(adresse).setHoraire(horaire)
				.setTelephone(telephone).setLienAgence(lienAgence).setAgenceHtm(agenceHtm).setFax(fax)
				.setCodeGuchet(codeGuchet).setGab(gab).setDab(dab).setBorneDepot(borneDepot).setLatitude(latitude)
				.setLongitude(longitude);
		this.saveData(cm);
		return cm;
	}

	public List<CM_Outre_Mer> showFirstRows() {
		List<CM_Outre_Mer> a = rep.getFirstRow(100, CM_Outre_Mer.class);
		return a;
	}

	public List<CM_Outre_Mer> showLastRows() {
		List<CM_Outre_Mer> a = rep.getLastRow(100, CM_Outre_Mer.class);
		return a;
	}

	public void setup() {
		FactoryBrowser fact = new FactoryBrowser();
		HtmlUnitBrowser hB = (HtmlUnitBrowser) fact.create("htmlUnit");
		htmlUnitDriver = hB.setOptions(false, false, false, 60000).initBrowserDriver();
	}

	public List<CM_Outre_Mer> scrapCM_Outre_Mer() throws IOException {
		List<String> listVille = new ArrayList<>();
		fichier_valeurs = Parametres.REPERTOIRE_DATA_SOURCES + ficval + ".txt";
		Scanner sc = new Scanner(new BufferedInputStream(new FileInputStream(new File(fichier_valeurs))));
		Scanner sc1 = new Scanner(new BufferedInputStream(new FileInputStream(new File(fichier_valeurs))));

		if (!new File(dossierRESU).exists()) {
			new File(dossierRESU).mkdirs();
		}

		fichier = new File(dossierRESU + "\\" + "RESULTATS_" + nomSITE + "_" + System.currentTimeMillis() + ".txt");
		LogScrap log = new LogScrap("BanqueCM_OUTRE_MER.txt", LogConst.TYPE_LOG_FILE);
		num_ligneficval = ((new File(LogConst.STORE_LOG_PATH + LogConst.PREFIX_LOG_FILE + "_BanqueCM_OUTRE_MER.txt"))
				.exists()) ? Integer.parseInt(log.readLog().getLastLineProcessed().split("\\t")[4]) : 1;

		parcoursficval = 0;
		int numville = 0;
		int i = 1;
		while (sc1.hasNextLine()) {
			String line = sc1.nextLine();
			if (i > 1) {
				NbVilles++;
			}
			i++;
		}
		sc1.close();
		while (sc.hasNextLine()) {

			String line = sc.nextLine();
			String[] fic_valeurs = line.split("\\t");
			if (parcoursficval >= num_ligneficval) {

				numville++;

				CPSoumis = fic_valeurs[0];

				String uri = fic_valeurs[1];

				try {
					System.out.println(uri);
					htmlUnitDriver.get(uri);
				} catch (Exception e) {
					if (e instanceof SocketTimeoutException) {
						System.out.println(uri);
						htmlUnitDriver.get(uri);
					}
				}
				if (!listVille.isEmpty()) {
					listVille.clear();
				}
				/*
				 * String pageSource = htmlUnitDriver.getPageSource(); Document doc =
				 * Jsoup.parse(pageSource); Element villeRoot = doc
				 * .selectFirst("#ei_tpl_content > div > article > section > div > div.a_blocfctl.lister"
				 * ); if (villeRoot != null) { Elements villeLinkElement =
				 * villeRoot.select("table > tbody > tr");
				 * 
				 * for (Element tmp : villeLinkElement) { Element cpElement =
				 * tmp.selectFirst("td:nth-child(2)"); if (this.isDOM(cpElement.text())) {
				 * Element pathElement = tmp.selectFirst("td.a_actions.nowrap>a");
				 * listVille.add(pathElement.attr("href")); }
				 * 
				 * } } if (!listVille.isEmpty()) { String url =
				 * "https://www.creditmutuel.fr/fr/banques/contact/trouver-une-agence/"; for
				 * (String link : listVille) {
				 * 
				 * String uri2 = url + link; try { System.out.println(uri2);
				 * htmlUnitDriver.get(uri2); } catch (Exception e) { if (e instanceof
				 * SocketTimeoutException) { htmlUnitDriver.get(uri2); } }
				 */
				String pageSource = htmlUnitDriver.getPageSource();
				Document doc = Jsoup.parse(pageSource);
				Element root = doc.select("#rslt").first();
				String agences_HTM = "", nom_agence = "", adresse = "", LienAgence = "", tel = "", horaires = "",
						villeSoumise = "", fax = "", latitude = "", longitude = "", codeGuchet = "";
				int gab = 0, dab = 0, borneDepot = 0;

				if (root != null) {

					Elements agenciesLinkJElement = root.select("#rslt_lst> li>div.body > span > em > a");
					NbAgences = agenciesLinkJElement.size();
					System.out.println("NbAgences : " + NbAgences);
					nbRecuperes = 0;
					List<String> listLink = new ArrayList<>();
					if (!listLink.isEmpty()) {
						listLink.clear();
					}
					for (Element tmp : agenciesLinkJElement) {

						listLink.add(tmp.attr("href"));

					}

					System.out.println("NbAgencesTab : " + listLink.size());

					for (String link : listLink) {

						try {
							System.out.println("uri_Fin : " + link);
							htmlUnitDriver.get(link);
						} catch (Exception e) {
							if (e instanceof SocketTimeoutException) {
								htmlUnitDriver.get(link);
							}
						}

						pageSource = htmlUnitDriver.getPageSource();
						doc = Jsoup.parse(pageSource);

						Element rootAgence = doc.selectFirst("#ei_tpl_contener");

						// TODO change
						agences_HTM = "";
						nom_agence = "";
						adresse = "";
						LienAgence = "";
						tel = "";
						horaires = "";
						villeSoumise = "";

						fax = "";
						latitude = "";
						longitude = "";
						codeGuchet = "";
						gab = 0;
						dab = 0;
						borneDepot = 0;
						
						Element agencyNamaElement = rootAgence.selectFirst("p[id$=\"title\"]");
						
						Element agencyNamaElement2 = rootAgence.selectFirst("div.grow > h1");
						
						if(agencyNamaElement != null) {
							agences_HTM = this.normalize(agencyNamaElement.outerHtml()
									.replaceAll("\\s{1,}", " "));
							nom_agence = this.normalize(agencyNamaElement.text().toUpperCase().replace("BIENVENUE DANS VOTRE", "").trim());
						}else if(agencyNamaElement2 != null) {
							agences_HTM = this.normalize(agencyNamaElement2.outerHtml()
									.replaceAll("\\s{1,}", " "));
							nom_agence = this.normalize(
									agencyNamaElement2.text().toUpperCase().replace("BIENVENUE DANS VOTRE", "").trim());
						}else {
							agences_HTM = "";
							nom_agence = "";
						}
						
						// retrieve agency link
						LienAgence = link;
						System.out.println("le lien de l'agences est " + LienAgence);

						// retrieve agency name
						System.out.println("le nom de l'agence est : " + nom_agence);

						// retrieve agency tel
						Element agencyTelElement = rootAgence.selectFirst("span[itemprop=\"telephone\"]");
						tel = (agencyTelElement != null) ? agencyTelElement.text().trim() : "";
						System.out.println("tel : " + tel);

						// retrieve horaires
						Element scheduleElement = rootAgence
								.selectFirst("#page_details > div.ei_gpblock-redac.ei_gpblock > div.ei_gpblock_body > "
										+ " div.cpc.eir_xs_table > div > div:nth-child(3) > div > div.ei_gpblock_body > table.days > tbody");
						horaires = scheduleElement != null ? scheduleElement.text().replaceAll("\\s{1,}", " ").trim() : "";
						horaires = this.normalize(horaires);
						System.out.println("horaires : " + horaires);

						// retrieve agency address
						Element agencyAddressElement = rootAgence
								.selectFirst("span[itemprop=\"address\"]");
						adresse = (agencyAddressElement != null)
								? this.normalize(agencyAddressElement.text().toUpperCase().trim())
								: "";
						System.out.println("adresse : " + adresse);

						String[] tab = adresse.split(" ");

						for (String t : tab) {
							if (Pattern.matches("[0-9]{5}", t)) {
								CPSoumis = t;
							}

						}

						System.out.println("CPSoumis : " + CPSoumis);

						nbRecuperes++;

						// TODO change
						villeSoumise = uri.replace(
								"https://www.creditmutuel.fr/fr/banques/contact/trouver-une-agence/SearchList.aspx?sub=true&type=branch&loca=",
								"");

						villeSoumise = this.normalize(villeSoumise.replace("%20", " "));

						System.out.println("villeSoumise : " + villeSoumise);
						
						// retrieve agency fax
						Element agencyFaxElement = rootAgence
								.selectFirst("#page_details > div.ei_gpblock-redac.ei_gpblock > div.ei_gpblock_body > div.cpc.eir_xs_table > "
										+ "div > div:nth-child(1) > div:nth-child(2) > div.ei_gpblock_body > table > tbody > tr:nth-child(2) > td");
						fax = (agencyFaxElement != null) ? agencyFaxElement.text().trim() : "";
						System.out.println("fax : " + fax);
						
						// retrieve agency codeGuchet
						Element codeGuchetElement = rootAgence
								.selectFirst("span[itemprop=\"branchCode\"]");
						codeGuchet = (codeGuchetElement != null) ? codeGuchetElement.text().trim() : "";
						System.out.println("codeGuchet : " + codeGuchet);
						
						// retrieve agency gab
						Element dabGabBorneDepotElement = rootAgence
								.selectFirst("div.ei_gpblock_body > ul");
						
						String dabGabBorneDepot = dabGabBorneDepotElement != null ? this.normalize(dabGabBorneDepotElement.text().toLowerCase().trim()) : "";
						
						gab = dabGabBorneDepot.contains("guichet automatique") || dabGabBorneDepot.contains("guichets automatiques") ? 1 : 0;
						System.out.println("gab : " + gab);
						dab = dabGabBorneDepot.contains("distributeur de billets") || dabGabBorneDepot.contains("distributeurs de billets") ? 1 : 0;
						System.out.println("dab : " + dab);
						borneDepot = dabGabBorneDepot.contains("borne de depot") || dabGabBorneDepot.contains("bornes de depot") ? 1 : 0;
						
						latitude = rootAgence.selectFirst("meta[itemprop=\"latitude\"]") != null ? doc.selectFirst("meta[itemprop=\"latitude\"]").attr("content") : "";
						System.out.println("latitude : " + latitude);
						
						longitude = rootAgence.selectFirst("meta[itemprop=\"longitude\"]") != null ? doc.selectFirst("meta[itemprop=\"longitude\"]").attr("content") : "";
						System.out.println("longitude : " + longitude);


						this.setData(CPSoumis, String.valueOf(NbVilles), String.valueOf(numville), villeSoumise,
								String.valueOf(NbAgences), nom_agence, adresse, horaires, tel, LienAgence, agences_HTM,
								fax, codeGuchet, gab, dab, borneDepot, latitude, longitude);
					}

				}
				
			// }
			// }
			log.writeLog(new Date(), CPSoumis, "no_error", NbAgences, parcoursficval, 0);
		}
		parcoursficval++;
	}
	
	sc.close();
	
	return cmList;

	}

	public void tearsDown() {
		htmlUnitDriver.close();
	}

	public void enregistrer(String agences_HTM, String nom_agence, String adresse, String LienAgence, int numville,
			String Villesoumise) {

		try {

			/*
			 * String depTXT = DepSoumis; depTXT = depTXT.replaceAll("SubdivisionId=FR-",
			 * "");
			 */
			sortie = new BufferedWriter(new FileWriter(fichier, true));
			sortie.write(CPSoumis + "\t" + NbVilles + "\t" + numville + "\t" + Villesoumise + "\t" + NbAgences + "\t"
					+ nom_agence + "\t" + adresse + "\t" + LienAgence + "\t" + agences_HTM + "\r\n");

			sortie.close();
		} catch (IOException e) {

			e.printStackTrace();
		}

	}

	public void enregistrer_journal(int numreponse, int numville) {

//	    	this.fichier = new File(Parametres.REPERTOIRE + "RESULTATS_BNP.");
		try {
			Temps_Fin = new Date();
			SimpleDateFormat formater = null;
			formater = new SimpleDateFormat("'le' dd/MM/yyyy '�' hh:mm:ss");
			String date = formater.format(Temps_Fin);
			date = formater.format(Temps_Fin);

			/*
			 * String depTXT = DepSoumis; depTXT = depTXT.replaceAll("SubdivisionId=FR-",
			 * "");
			 */

			sortie_log = new BufferedWriter(new FileWriter(log, true));
			sortie_log.write(parcoursficval + ";" + "FinValeur" + ";" + CPSoumis + ";" + this.NbVilles + ";" + "VILLE_"
					+ numville + ";" + this.NbAgences + ";" + nbRecuperes + ";" + (this.NbAgences - nbRecuperes) + ";"
					+ date + "\r\n");
			sortie_log.close();
			long taille_fichier = fichier.length() / 1000000; // la length est en octets, on divise par 1000 pour mettre
																// en Ko et par 1000000 pour mettre en Mo

			if (taille_fichier > 8) {
				// INITIALISATION FICHIER DE RESULTAT
				this.fichier = new File(
						dossierRESU + "\\" + "RESULTATS_" + nomSITE + "_" + System.currentTimeMillis() + ".txt");
				// Attention pas d'accent dans les noms de colonnes !
				sortie = new BufferedWriter(new FileWriter(fichier, true));
				sortie.write("DepSoumis" + "\t" + "NbVilles" + "\t" + "Numville" + "\t" + "VilleSoumise" + "\t"
						+ "NbAgences" + "\t" + "NOMWEB" + "\t" + "ADRESSE" + "\t" + "LIEN_AGENCE" + "\t" + "AGENCEHTM"
						+ "\r\n");
				sortie.close();
			}

		} catch (IOException e) {

			e.printStackTrace();
		}

	}

	/**
	 * La m�thode <code>initialiser()</code> permet d'ajouter la ligne d'ent�te dans
	 * le fichier r�sultat.
	 */

	public void initialiser() {

		try {
			// Attention pas d'accent dans les noms de colonnes !
			sortie = new BufferedWriter(new FileWriter(fichier, true));
			sortie.write(
					"DepSoumis" + "\t" + "NbVilles" + "\t" + "Numville" + "\t" + "VilleSoumise" + "\t" + "NbAgences"
							+ "\t" + "NOMWEB" + "\t" + "ADRESSE" + "\t" + "LIEN_AGENCE" + "\t" + "AGENCEHTM" + "\r\n");
			sortie.close();

			File toto = new File(dossierRESU + "\\" + "LOG_" + nomSITE + ".csv");
			if (!toto.exists()) {
				// if (num_ligneficval == num_ligne) {
				sortie_log = new BufferedWriter(new FileWriter(log, true));
				sortie_log
						.write("NumLigne" + ";" + "FinValeur" + ";" + "DepSoumis" + ";" + "NbVilles" + ";" + "NumVILLE"
								+ ";" + "NbAgences" + ";" + "Nbrecuperes" + ";" + "Reste" + ";" + "Heure_Fin" + "\r\n");
				sortie_log.close();
			}

		} catch (IOException e) {

			e.printStackTrace();
		}
	}

}