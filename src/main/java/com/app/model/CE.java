package com.app.model;

import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="banque_ce", schema="scrap")
public class CE {

	
	@Id
	@SequenceGenerator(name="scrap.banque_ce_id_seq",
	                    sequenceName ="scrap.banque_ce_id_seq",
	                     allocationSize=1)
    @GeneratedValue( strategy=GenerationType.SEQUENCE,
                     generator="scrap.banque_ce_id_seq")
	  
	@Column(name="id",updatable=false)
	private int id;
	
	@Column(name="depsoumis", columnDefinition="varchar")
	private String depSoumis;
	
	@Column(name="nbagences", columnDefinition="varchar")
	private String nbAgences;
	
	@Column(name="nomweb", columnDefinition="varchar")
	private String nomWeb;
	
	@Column(name="adresse", columnDefinition="varchar")
	private String adresse;
	
	@Column(name="tel", columnDefinition="varchar")
	private String tel;
	
	@Column(name="fax", columnDefinition="varchar")
	private String fax;
	
	@Column(name="horaireshtm", columnDefinition="varchar")
	private String horairesHtm;
	
	@Column(name="horairestxt", columnDefinition="varchar")
	private String horairestxt;
	
	@Column(name="dab", columnDefinition="varchar")
	private String dab;
	
	@Column(name="agencehtm", columnDefinition="varchar")
	private String agenceHtm;
	
	@Column(name="services", columnDefinition = "varchar")
	private String service;
	
	@Column(name="lien_agence", columnDefinition="varchar")
	private String lien_agence;
	
	@Column(name="annee_du_scrap" ,columnDefinition="varchar(4) DEFAULT date_part('year',CURRENT_DATE)")
	private String anneeScrap;
	
    public CE() {
    	LocalDate ld = new Date().toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
		anneeScrap = String.valueOf(ld.getYear() + 1);
	}
	
	public int getId() {
		return id;
	}
	public CE setId(int id) {
		this.id = id;
		return this;
	}
	public String getDepSoumis() {
		return depSoumis;
	}
	public CE setDepSoumis(String depSoumis) {
		this.depSoumis = depSoumis;
		return this;
	}
	public String getNbAgences() {
		return nbAgences;
	}
	public CE setNbAgences(String nbAgences) {
		this.nbAgences = nbAgences;
		return this;
	}
	public String getNomWeb() {
		return nomWeb;
	}
	public CE setNomWeb(String nomWeb) {
		this.nomWeb = nomWeb;
		return this;
	}
	public String getAdresse() {
		return adresse;
	}
	public CE setAdresse(String adresse) {
		this.adresse = adresse;
		return this;
	}
	public String getTel() {
		return tel;
	}
	public CE setTel(String tel) {
		this.tel = tel;
		return this;
	}
	public String getFax() {
		return fax;
	}
	public CE setFax(String fax) {
		this.fax = fax;
		return this;
	}
	public String getHorairesHtm() {
		return horairesHtm;
	}
	public CE setHorairesHtm(String horairesHtm) {
		this.horairesHtm = horairesHtm;
		return this;
	}
	public String getHorairestxt() {
		return horairestxt;
	}
	public CE setHorairestxt(String horairestxt) {
		this.horairestxt = horairestxt;
		return this;
	}
	public String getDab() {
		return dab;
	}
	public CE setDab(String dab) {
		this.dab = dab;
		return this;
	}
	public String getAgenceHtm() {
		return agenceHtm;
	}
	public CE setAgenceHtm(String agenceHtm) {
		this.agenceHtm = agenceHtm;
		return this;
	}
	public String getAnneeScrap() {
		return anneeScrap;
	}
	public CE setAnneeScrap(String anneeScrap) {
		this.anneeScrap = anneeScrap;
		return this;
	}

	public String getService() {
		return service;
	}

	public CE setService(String service) {
		this.service = service;
		return this;
	}

	public String getLien_agence() {
		return lien_agence;
	}

	public CE setLien_agence(String lien_agence) {
		this.lien_agence = lien_agence;
		return this;
	}
}
