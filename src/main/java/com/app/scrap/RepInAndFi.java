package com.app.scrap;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.app.model.InAndFI;

@Repository
public interface RepInAndFi extends JpaRepository<InAndFI,Integer>,RepositoryCustom<InAndFI>{

}
