package com.app.scrap;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.app.model.CASDEN_Outre_Mer;

@Repository
public interface RepCasdenOutreMer extends JpaRepository<CASDEN_Outre_Mer, Integer>, RepositoryCustom<CASDEN_Outre_Mer>{

}
