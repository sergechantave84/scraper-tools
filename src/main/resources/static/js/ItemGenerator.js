class ItemGenerator{

    
    constructor(motherElement){
       this.motherElement=motherElement
    }

    getAllItem(url){
        return new Promise((resolve,reject)=>{
            let xhr= new XMLHttpRequest()
            if(xhr){
              xhr.onreadystatechange=()=>{
                if(xhr.readyState==4){
                    if(xhr.status==200)
                       resolve(xhr.responseText)
                }        
              }

              xhr.onerror=()=>{
                reject(xhr.statusText)
              }

              xhr.open("GET",url,true)
              xhr.send()
           }
       
        });
    }

    renderItem(str){
        let items=JSON.parse(str)
        Array.from(items).forEach((item)=>{
            let nom= item.name
            let etat=item.status

            let cardContainer=document.createElement("div")
            cardContainer.setAttribute("class","card_container" )
            

            let cardHead=document.createElement("div")
            cardHead.setAttribute("class","card_header")

            let link=document.createElement("a")
            link.setAttribute("href",`/scrap/${nom}`)
            link.setAttribute("class","call_scrap")
            
            link.textContent=nom
            cardHead.appendChild(link)
              //<i class="fas fa-check"></i>
            let icon=document.createElement("i")
            icon.setAttribute("class","fas fa-check")

            if(etat){
              icon.style.color="#13A76B"
            }else{
              icon.style.color="red"
            }

            this.motherElement.appendChild(cardContainer).appendChild(cardHead)
            cardContainer.appendChild(icon)

        });
    }

} 