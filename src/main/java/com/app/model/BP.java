package com.app.model;

import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "banque_bp", schema = "scrap")
public class BP {

	@Id
	@SequenceGenerator(name = "scrap.banque_bp_id_seq", sequenceName = "scrap.banque_bp_id_seq", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "scrap.banque_bp_id_seq")

	@Column(name = "id", updatable = false)
	private int id;

	@Column(name = "codepostale", columnDefinition = "varchar")
	private String codePostale;

	@Column(name = "total", columnDefinition = "varchar")
	private String total;

	@Column(name = "nbagences", columnDefinition = "varchar")
	private String nbAgences;

	@Column(name = "enseigne", columnDefinition = "varchar")
	private String enseigne;

	@Column(name = "nomweb", columnDefinition = "varchar")
	private String nomWeb;

	@Column(name = "adresse", columnDefinition = "varchar")
	private String adresse;

	@Column(name = "tel", columnDefinition = "varchar")
	private String tel;

	@Column(name = "fax", columnDefinition = "varchar")
	private String fax;

	@Column(name = "horaireshtm", columnDefinition = "varchar")
	private String horairesHtm;

	@Column(name = "horairestxt", columnDefinition = "varchar")
	private String horairesTxt;

	@Column(name = "services", columnDefinition = "varchar")
	private String services;

	@Column(name = "dab", columnDefinition = "varchar")
	private String dab;

	@Column(name = "agencehtm", columnDefinition = "varchar")
	private String agenceHtm;

	@Column(name = "annee_du_scrap", columnDefinition = "varchar(4) DEFAULT date_part('year',CURRENT_DATE)")
	private String anneeScrap;

	public BP() {
		LocalDate ld = new Date().toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
		anneeScrap = String.valueOf(ld.getYear() + 1);
	}

	public int getId() {
		return id;
	}

	public BP setId(int id) {
		this.id = id;
		return this;
	}

	public String getCodePostale() {
		return codePostale;
	}

	public BP setCodePostale(String codePostale) {
		this.codePostale = codePostale;
		return this;
	}

	public String getTotal() {
		return total;
	}

	public BP setTotal(String total) {
		this.total = total;
		return this;
	}

	public String getNbAgences() {
		return nbAgences;
	}

	public BP setNbAgences(String nbAgences) {
		this.nbAgences = nbAgences;
		return this;
	}

	public String getEnseigne() {
		return enseigne;
	}

	public BP setEnseigne(String enseigne) {
		this.enseigne = enseigne;
		return this;
	}

	public String getNomWeb() {
		return nomWeb;
	}

	public BP setNomWeb(String nomWeb) {
		this.nomWeb = nomWeb;
		return this;
	}

	public String getAdresse() {
		return adresse;
	}

	public BP setAdresse(String adresse) {
		this.adresse = adresse;
		return this;
	}

	public String getTel() {
		return tel;
	}

	public BP setTel(String tel) {
		this.tel = tel;
		return this;
	}

	public String getFax() {
		return fax;
	}

	public BP setFax(String fax) {
		this.fax = fax;
		return this;
	}

	public String getHorairesHtm() {
		return horairesHtm;
	}

	public BP setHorairesHtm(String horairesHtm) {
		this.horairesHtm = horairesHtm;
		return this;
	}

	public String getHorairesTxt() {
		return horairesTxt;
	}

	public BP setHorairesTxt(String horairesTxt) {
		this.horairesTxt = horairesTxt;
		return this;
	}

	public String getServices() {
		return services;
	}

	public BP setServices(String services) {
		this.services = services;
		return this;
	}

	public String getDab() {
		return dab;
	}

	public BP setDab(String dab) {
		this.dab = dab;
		return this;
	}

	public String getAgenceHtm() {
		return agenceHtm;
	}

	public BP setAgenceHtm(String agenceHtm) {
		this.agenceHtm = agenceHtm;
		return this;
	}

	public String getAnneeScrap() {
		return anneeScrap;
	}

	public BP setAnneeScrap(String anneeScrap) {
		this.anneeScrap = anneeScrap;
		return this;
	}
}
