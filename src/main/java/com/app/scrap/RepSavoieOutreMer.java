package com.app.scrap;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.app.model.Savoie_Outre_Mer;

@Repository
public interface RepSavoieOutreMer extends JpaRepository<Savoie_Outre_Mer, Integer>,RepositoryCustom<Savoie_Outre_Mer>  {

}
