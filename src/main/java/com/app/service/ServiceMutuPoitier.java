package com.app.service;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.SocketTimeoutException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Scanner;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.PageLoadStrategy;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.app.model.Mutuelle_Poitier;
import com.app.model.ParisDep;
import com.app.scrap.RepMutuPoitier;
import com.app.utils.BrowserUtils;
import com.app.utils.Parametres;
import com.app.utils.Utilitaires;
import com.gargoylesoftware.htmlunit.javascript.background.JavaScriptExecutor;

import fa.browserUtils.ChromeBrowser;
import fa.browserUtils.FactoryBrowser;
import fa.browserUtils.HtmlUnitBrowser;
import fa.log.LogApp.LogScrap;
import fa.log.config.LogConst;

@Service
public class ServiceMutuPoitier extends Utilitaires {

	@Autowired
	private RepMutuPoitier rep;

	ChromeDriver chromeDriver;
	HtmlUnitDriver htmlUnitDriver;
	List<Mutuelle_Poitier> mutuPoitierList = new ArrayList<>();
	BufferedWriter logs;
	BufferedWriter bf;
	File logsF;
	File resultFile;

	public Mutuelle_Poitier saveData(Mutuelle_Poitier t) {

		return rep.save(t);

	}

	public List<Mutuelle_Poitier> saveDataAll(List<Mutuelle_Poitier> t) {

		return rep.saveAll(t);

	}

	public List<Mutuelle_Poitier> getElmnt() {

		return rep.findAll();
	}

	public Mutuelle_Poitier getById(int id) {

		return rep.findById(id).orElse(null);
	}

	public String deleteProduct(int id) {
		try {
			rep.deleteById(id);

		} catch (Exception e) {
			e.printStackTrace();
		}

		return "removed";
	}

	public Mutuelle_Poitier updateProduct(Mutuelle_Poitier t) {
		// Mutuelle_PoitierODO Auto-generated method stub
		return null;
	}

	public Mutuelle_Poitier setData(String lienAgence, String nbAgences, String nomAgence, String adresse, String tel,
			String nomsAgents, String orias, String horaires, String agenceHtm) {
		Mutuelle_Poitier mp = new Mutuelle_Poitier();
		mp.setLienAgence(lienAgence).setNbAgences(nbAgences).setNomAgence(nomAgence).setAdresse(adresse).setTel(tel)
				.setNomsAgents(nomsAgents).setOrias(orias).setHoraires(horaires).setAgenceHtm(agenceHtm);
		this.saveData(mp);
		return mp;
	}

	public List<Mutuelle_Poitier> showFirstRows() {
		List<Mutuelle_Poitier> a = rep.getFirstRow(100, Mutuelle_Poitier.class);
		return a;
	}

	public List<Mutuelle_Poitier> showLastRows() {
		List<Mutuelle_Poitier> a = rep.getLastRow(100, Mutuelle_Poitier.class);
		return a;
	}
	
	public boolean isAdded(Mutuelle_Poitier mp) {
		boolean result = false;
		List<Mutuelle_Poitier> res  = rep.findAll();
		for(Mutuelle_Poitier poitier : res) {
			if(poitier==mp) {
				result = true;
			}
		}
		
		return result;
	}

	public void setup() {
		String driverPath = "C:\\ChromeDriver\\";
		FactoryBrowser factBrowser = new FactoryBrowser();
		HtmlUnitBrowser hB = (HtmlUnitBrowser) factBrowser.create("htmlUnit");
		htmlUnitDriver = hB.setOptions(false, false, false, 60000).initBrowserDriver();
		
		final String chromeBeta = "C:\\Program Files\\Google\\Chrome Beta\\Application\\chrome.exe";

		factBrowser = new FactoryBrowser();
		ChromeBrowser b = (ChromeBrowser) factBrowser.create("chrome");
		chromeDriver = b.setOptions(PageLoadStrategy.NORMAL, driverPath, 60000, 60000, 60000, false, false, chromeBeta)
				.initBrowserDriver();
	}

	public List<Mutuelle_Poitier> scrapMutuellePoitier() throws IOException, InterruptedException {

		String driverPath = "C:\\ChromeDriver\\";
		// htmlUnitDriver=BrowserUtils.HtmlUnitConstruct(true,false,false,1);
		// chromeDriver =BrowserUtils.chromeConstruct(driverPath, 60000,60000,60000,
		// PageLoadStrategy.NORMAL);
		ParisDep parisDep = new ParisDep();
		JavascriptExecutor jsExec = (JavascriptExecutor) chromeDriver;
		// JavascriptExecutor jsExec2=(JavascriptExecutor) chromeDriver;
		String urlMutuPoitierLien = "https://agence.assurance-mutuelle-poitiers.fr";
		boolean dReadyState = false;
		String pageSource = "";
		String lienAgence = "";
		String line = "";
		String valeurSoumis = "";
		String lines[] = null;

		int counter = 0;
		int recentLine = 1;
		long timeMillis = System.currentTimeMillis();

		List<String> depParisList = parisDep.getListParisDep();

		Document doc = null;
		List<WebElement> listChoises = null;
		WebElement inputSearch = null;
		WebElement submitBtn = null;
		Elements listAgences = null;
		Element link = null;

		if (!new File(Parametres.REPERTOIRE_RESULT + "\\ASSU\\Mutuelle_Poitier").exists())
			new File(Parametres.REPERTOIRE_RESULT + "\\ASSU\\Mutuelle_Poitier").mkdirs();

		resultFile = new File(Parametres.REPERTOIRE_RESULT + "\\ASSU\\Mutuelle_Poitier\\" + "Mutuelle_Poitier_"
				+ timeMillis + ".txt");
		LogScrap log = new LogScrap("Mutuelle_Poitier.txt", LogConst.TYPE_LOG_FILE);
		recentLine = ((new File(LogConst.STORE_LOG_PATH + LogConst.PREFIX_LOG_FILE + "_Mutuelle_Poitier.txt")).exists())
				? Integer.parseInt(log.readLog().getLastLineProcessed().split("\\t")[4])
				: 1;

		//String pathSrc = Parametres.REPERTOIRE_DATA_SOURCES + "CP_cantons_poitiers.txt";
		String pathSrc = Parametres.REPERTOIRE_DATA_SOURCES + "CP_cantons.txt";
		Scanner sc = new Scanner(new BufferedInputStream(new FileInputStream(new File(pathSrc))));

		int s = 0;
		
		while (sc.hasNextLine()) {
			line = sc.nextLine();
			if (counter > recentLine) {

				lines = line.split("\t");
				//valeurSoumis = lines[0]+" "+lines[2];
				valeurSoumis = lines[0];
				
				System.out.println("CP : "+valeurSoumis);
				//valeurSoumis = valeurSoumis.replaceAll("[^0-9A-Za-z|\\s]", "");
				//nomSoumis = lines[2];
				try {
					chromeDriver.get(urlMutuPoitierLien);
				} catch (Exception e) {
					if (e instanceof SocketTimeoutException) {
						chromeDriver.get(urlMutuPoitierLien);
					}
				}
				dReadyState = this.listensIfPageFullyLoaded(chromeDriver);
				if (dReadyState) {
					Thread.sleep(3000);
					inputSearch = chromeDriver.findElement(By.cssSelector("#search_agency_recherche"));
					inputSearch.sendKeys(valeurSoumis);
					listChoises = chromeDriver.findElements(By.cssSelector("#recherche_box>ul>li"));
					s = listChoises.size();
					if (s > 0) {
						/*
						 * List<String> strTmp=new ArrayList<String>(); for(WebElement tmp: listChoises)
						 * { System.out.println(tmp.getText().replaceAll("[^0-9a-zA-Z]{1,}",""));
						 * strTmp.add(tmp.getText().replaceAll("[^0-9a-zA-Z]{1,}","")); }
						 */
						// if(strTmp.contains(valeurSoumis+lines[2].replaceAll("[^0-9a-zA-Z]{1,}","")))
						// {
						for (WebElement tmp : listChoises) {
							// case for Paris
							if (depParisList.contains(valeurSoumis)) {
								if (tmp.getText().contains(valeurSoumis + " " + "PARIS")) {
									try {
										// tmp.click();
										jsExec.executeScript("arguments[0].click()", tmp);
									} catch (Exception e) {
										e.printStackTrace();
									}

									String numeroDep = lines[2].replaceAll("[a-zA-Z]{1,}", "").trim();
									numeroDep = numeroDep.replaceAll("\\s{1,}", "");
									try {
										submitBtn = chromeDriver
												.findElement(By.cssSelector("#formSearchAgency > div.fields > button"));
										/*
										 * WebDriverWait wait= new WebDriverWait(chromeDriver, 20);
										 * wait.until(ExpectedConditions.elementToBeClickable(submitBtn));
										 * submitBtn.click();
										 */
										JavascriptExecutor executor = (JavascriptExecutor) chromeDriver;
										executor.executeScript("arguments[0].click();", submitBtn);

										// jsExec2.executeScript("arguments[0].click()", submitBtn);
									} catch (Exception e) {
										e.printStackTrace();
									}

									dReadyState = this.listensIfPageFullyLoaded(chromeDriver);
									if (dReadyState) {
										pageSource = chromeDriver.getPageSource();
										doc = Jsoup.parse(pageSource);
										if (!doc.html().contains(
												"Votre commune est en dehors du secteur d'activité de la Mutuelle de Poitiers, nous regrettons de ne pas pouvoir donner suite à votre demande")) {
											listAgences = doc
													.select("#list-agency > div.container > div.agences > div.row");
											String tmpLink = "";
											for (Element tmp0 : listAgences) {
												link = tmp0.selectFirst(
														"div > div > div.container-fluid > div.row > div.push-agency-cta.col-sm-3.text-center > a");
												lienAgence = link.attr("href");
												tmpLink = lienAgence.replaceAll("[^a-zA-Z0-9]{1,}", "").trim();
												// if(tmpLink.equals("agenceparis"+numeroDep)) {
												lienAgence = urlMutuPoitierLien + lienAgence;
											
												this.retrieveInfos(valeurSoumis, lienAgence, urlMutuPoitierLien,
														counter, bf, logs);

												// }
											}
										}
									}
									break;
								}
							}
						}

						try {
							jsExec.executeScript("arguments[0].click()", listChoises.get(1));
							submitBtn = chromeDriver
									.findElement(By.cssSelector("#formSearchAgency > div.fields > button"));
							JavascriptExecutor executor = (JavascriptExecutor) chromeDriver;
							executor.executeScript("arguments[0].click();", submitBtn);
							// jsExec2.executeScript("arguments[0].click()", submitBtn);
						} catch (Exception e) {
							e.printStackTrace();
						}
						dReadyState = this.listensIfPageFullyLoaded(chromeDriver);
						if (dReadyState) {
							pageSource = chromeDriver.getPageSource();
							doc = Jsoup.parse(pageSource);
							// TODO change
							String tmps = doc.html();
							if (!tmps.contains(
									"Votre commune est en dehors du secteur d'activité de la Mutuelle de Poitiers, nous regrettons de ne pas pouvoir donner suite à votre demande")) {

								listAgences = doc.select("#list-agency > div.container > div.agences > div.row");
								for (Element tmp0 : listAgences) {

									link = tmp0.selectFirst(
											"div > div > div.container-fluid > div.row > div.push-agency-cta.col-sm-3.text-center > a");
									if (link != null) {
										lienAgence = link.attr("href");
										lienAgence = urlMutuPoitierLien + lienAgence;
										this.retrieveInfos(valeurSoumis, lienAgence, urlMutuPoitierLien, counter, bf,
												logs);
									}

								}
							}
						}

					}

				}

				log.writeLog(new Date(), valeurSoumis, "no_error", 1, counter, 0);
			}
			counter++;

		}
		sc.close();
		return mutuPoitierList;

	}

	public void retrieveInfos(String valeurSoumis, String lienAgence, String urlMutuPoitierLien, int counter,
			BufferedWriter bf, BufferedWriter logs) throws IOException {

		String pageSource = "";
		String nomAgence = "";
		String telephone = "";
		String nomAgents = "";
		String orias = "";
		String horaires = "";
		String adresse = "";
		String agenceHtml = "";
		int nbrAgence = 1;
		int nbrRecuperes = 0;
		try {
			System.out.println(lienAgence);
			htmlUnitDriver.get(lienAgence);
		} catch (Exception e) {
			if (e instanceof SocketTimeoutException) {
				htmlUnitDriver.get(lienAgence);
			}
		}
		//System.out.println("lienAgence : "+lienAgence);
		pageSource = htmlUnitDriver.getPageSource();
		Document doc = Jsoup.parse(pageSource);

		Element agencyInfosRoot = doc.selectFirst(
				" div > div > div[class^=\"push-agency-details\"] > div.container > div.row > div[class*=\"push-agency-content\"]");
		Element navBarRoot = doc.selectFirst("div>div>nav[class=\"nav nav-agency\"]> div.container > div.row");

		// retrieve detail
		agenceHtml = doc.select("body > div.container-inner > div.page-content").html().replaceAll("\\s{1,}", "")
				.trim();

		// fetch agent name
		Element agentNameElmnt = agencyInfosRoot.selectFirst("p[class=\"push-agent\"] > strong");
		nomAgents = (agentNameElmnt != null) ? this.normalize(agentNameElmnt.text().trim()) : "";
		//nomAgents = nomAgents.contains("Mutuelle de Poitiers Assurances en direct") ? "" : nomAgents;
		//System.out.println("nom agent : " + nomAgents);
		// fetch agency name
		Element agencyNameElmnt = agencyInfosRoot.selectFirst(" div:nth-child(1)"); 
		nomAgence = (agencyNameElmnt != null) ? this.normalize(agencyNameElmnt.text().replaceAll("\\s{1,}", " ").replaceAll("Votre agence", " ").trim())
				: "";
		//System.out.println("nom agence : " + nomAgence);
		// fetch agency orias
		Element agencyNumOriasElmnt = agencyInfosRoot.selectFirst("div.push-agency-orias");
		orias = (agencyNumOriasElmnt != null) ? agencyNumOriasElmnt.text().replaceAll("[^0-9 ]{1,}", "").trim() : "";
		orias = orias.replaceAll("\\s{2,}", " ");
		//System.out.println("orias : " + orias);
		// fetch agency telephone
		Element agencyTelephoneElmnt = doc.selectFirst(
				"div > div > div[class*=\"push-agency-details\"]> div > div > div[class*=\"push-agency-hours\"] > p.push-agency-phone > a");
		telephone = (agencyTelephoneElmnt != null) ? agencyTelephoneElmnt.text().trim() : "";
		//System.out.println("tel : " + telephone);
		// fetch agency address
		Element agencyAddressElmnt = doc.selectFirst("head > meta[name=\"description\"]");
		String adresseTmp = agencyAddressElmnt.attr("content");
		adresseTmp = adresseTmp.replaceAll("\\.", " ");
		String[] arrayTmp = adresseTmp.split("Tél");
		String adresseTmp2 = arrayTmp[0].replaceAll(" - ", "deadPool");
		adresse = this.normalize(adresseTmp2.split("deadPool")[1]);
		//System.out.println("l'adresse est : " + adresse);

		// fetch schedule

		Element linkforScheduleElmnt = navBarRoot.selectFirst("ul > li:nth-child(3) > a[title=\"Nos horaires\"]");
		String scheduleLink = (linkforScheduleElmnt != null)
				? this.normalize(urlMutuPoitierLien + linkforScheduleElmnt.attr("href"))
				: "";

		try {
			htmlUnitDriver.get(scheduleLink);
		} catch (Exception e) {
			if (e instanceof SocketTimeoutException) {
				htmlUnitDriver.get(scheduleLink);
			}
		}
		pageSource = htmlUnitDriver.getPageSource();
		doc = Jsoup.parse(pageSource);

		Elements agencySchedule = doc
				.select("div.container-inner>div.page-content>div.content>div.container>div>div.opening-hours>ul>li");
		for (Element tmp : agencySchedule) {
			horaires += tmp.text() + " ";
		}
		horaires = this.normalize(horaires.replaceAll("\\s{2,}", ""));
		
		if(adresse.contains(valeurSoumis)) {
			System.out.println("nom agent : " + nomAgents);
			System.out.println("nom agence : " + nomAgence);
			System.out.println("tel : " + telephone);
			System.out.println("orias : " + orias);
			System.out.println("l'adresse est : " + adresse);
			System.out.println("lienAgence : "+lienAgence);
			System.out.println("horaires : " + horaires);
			
			mutuPoitierList.add(this.setData(lienAgence, String.valueOf(nbrAgence), nomAgence, adresse, telephone,
					nomAgents, orias, horaires, agenceHtml));
			
			System.out.println("------------------------------------\n");
		}
		
	}

	public void enregistrer(String nomAgence, String adresse, String telephone, String nomAgents, String orias,
			String horaires, String agenceHtml, String lienAgence, int nbrAgence) throws IOException {

		try {
			bf = new BufferedWriter(new FileWriter(resultFile, true));
			/*bf.write(lienAgence + "\t" + nbrAgence + "\t" + nomAgence + "\t" + adresse + "\t" + telephone + "\t"
					+ nomAgents + "\t" + orias + "\t" + horaires + "\t" + agenceHtml + "\r\n");
			bf.close();*/
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public void enregistrerLogs(String valeurSoumis, int counter, int nbrRecuperes, int nbVillesCP) {
		try {
			Date Temps_Fin = new Date();
			SimpleDateFormat formater = null;
			formater = new SimpleDateFormat("'le' dd/MM/yyyy '�' hh:mm:ss");
			String date = formater.format(Temps_Fin);
			date = formater.format(Temps_Fin);
			logs = new BufferedWriter(new FileWriter(logsF, true));
			/*logs.write(counter + ";" + "FinValeur" + ";" + valeurSoumis + ";" + nbVillesCP + ";" + nbrRecuperes + ";"
					+ (nbVillesCP - nbrRecuperes) + ";" + date + "\r\n");

			logs.close();*/
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void initialiser() {

		try {

			bf = new BufferedWriter(new FileWriter(resultFile, true));
			/*bf.write("LienAgence" + "\t" + "NbAgences" + "\t" + "NOM_AGENCE" + "\t" + "ADRESSE" + "\t" + "TEL" + "\t"
					+ "NOMS_AGENTS" + "\t" + "ORIAS" + "\t" + "HORAIRES" + "\t" + "AGENCEHTM" + "\r\n");
			if (!logsF.exists()) {
				logs = new BufferedWriter(new FileWriter(logsF, true));
				logs.write("NumLigne" + ";" + "FinValeur" + ";" + "CPSoumis" + ";" + "NbVillesCP" + ";" + "Nbrecuperes"
						+ ";" + "Reste" + ";" + "Heure_Fin" + "\r\n");
				logs.close();
			}*/
			bf.close();

		} catch (IOException e) {

			e.printStackTrace();
		}
	}

	public void tearsDown() {
		chromeDriver.quit();
	}
}
