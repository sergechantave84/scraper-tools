package com.app.scrap;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.app.model.ActionLogement;

@Repository
public interface RepActionLogement  extends JpaRepository<ActionLogement, Integer>{

}
