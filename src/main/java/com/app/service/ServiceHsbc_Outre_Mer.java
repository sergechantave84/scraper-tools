package com.app.service;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.SocketTimeoutException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Scanner;

import org.json.JSONException;
import org.json.JSONObject;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.PageLoadStrategy;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.app.model.HSBC_Outre_Mer;
import com.app.scrap.RepHsbcOutreMer;
import com.app.utils.BrowserUtils;
import com.app.utils.Parametres;
import com.app.utils.Utilitaires;

import fa.browserUtils.ChromeBrowser;
import fa.browserUtils.FactoryBrowser;
import fa.log.LogApp.LogScrap;
import fa.log.config.LogConst;

@Service
public class ServiceHsbc_Outre_Mer extends Utilitaires {

	@Autowired
	private RepHsbcOutreMer rep;

	String driverPath = "C:\\ChromeDriver\\";
	
	//String driverPath = "C:\\chromeDriverSimple\\";

	private List<HSBC_Outre_Mer> hsbcList = new ArrayList<>();
	String nomSITE = "HSBC_Outre_Mer";
	String ficval = Parametres.OUTRE_MER_NOM_FICHIER;/*"departements_sans2A2B"*/;
	String dossierRESU = Parametres.REPERTOIRE_RESULT + "\\BQ\\" + nomSITE;;
	String url_accueil = "https://www.hsbc.fr/1/2/hsbc-france/trouver-une-agence";
	// String newUrl_accueil="https://agences.hsbc.fr/search?geo=&lat=&lon=&page=";
	String fichier_valeurs;
	String cp;
	String VilleSoumise;
	String ligne;

	int num_ligne = 2;
	int NbAgences;
	int numreponse = 1;
	int nbRecuperes;
	int num_ligneficval;
	int parcoursficval;
	int Resultats;
	int numPage = 100;

	static ChromeDriver chromeDriver;
	static HtmlUnitDriver htmlUnitDriver;
	BufferedWriter sortie;
	BufferedWriter sortie_log;

	Date Temps_Fin;

	public HSBC_Outre_Mer saveData(HSBC_Outre_Mer t) {

		return rep.save(t);

	}

	public List<HSBC_Outre_Mer> saveDataAll(List<HSBC_Outre_Mer> t) {

		return rep.saveAll(t);

	}

	public List<HSBC_Outre_Mer> getElmnt() {

		return rep.findAll();
	}

	public HSBC_Outre_Mer getById(int id) {

		return rep.findById(id).orElse(null);
	}

	public String deleteProduct(int id) {
		try {
			rep.deleteById(id);

		} catch (Exception e) {
			e.printStackTrace();
		}

		return "removed";
	}

	public HSBC_Outre_Mer updateProduct(HSBC_Outre_Mer t) {
		// HSBC_Outre_MerODO Auto-generated method stub
		return null;
	}

	public HSBC_Outre_Mer setData(String depSoumise, String nbAgences, String nomWeb, String adresse, String tel,
			String fax, String horairesHtm, String horairesHSBC_Outre_Merxt, String cadreInfos, String agenceHtm) {
		HSBC_Outre_Mer hsbc = new HSBC_Outre_Mer();
		hsbc.setDepSoumise(depSoumise).setNbAgences(nbAgences).setNomWeb(nomWeb).setAdresse(adresse).setTel(tel)
				.setFax(fax).setHorairesHtm(horairesHtm).setHorairesTxt(horairesHSBC_Outre_Merxt)
				.setCadreInfos(cadreInfos).setAgenceHtm(agenceHtm);
		this.saveData(hsbc);
		return hsbc;
	}

	public List<HSBC_Outre_Mer> showFirstRows() {
		List<HSBC_Outre_Mer> a = rep.getFirstRow(100, HSBC_Outre_Mer.class);
		return a;
	}

	public List<HSBC_Outre_Mer> showLastRows() {
		List<HSBC_Outre_Mer> a = rep.getLastRow(100, HSBC_Outre_Mer.class);
		return a;
	}

	public void setup() {
		
		FactoryBrowser factBrowser = new FactoryBrowser();
		ChromeBrowser b = (ChromeBrowser) factBrowser.create("chrome");
		final String binaryPath = "C:\\Program Files\\Google\\Chrome Beta\\Application\\chrome.exe";
		chromeDriver = b.setOptions(PageLoadStrategy.NORMAL, driverPath, 60000, 60000, 60000, false, false, binaryPath)
				.initBrowserDriver();
		
		//chromeDriver = BrowserUtils.chromeConstruct(driverPath, 60000, 60000, 60000, PageLoadStrategy.NORMAL);

	}

	public List<HSBC_Outre_Mer> scrapHSBC_Outre_Mer() throws IOException, InterruptedException {
		boolean documentReadyState = false;
		JavascriptExecutor jsExecutor = (JavascriptExecutor) chromeDriver;
		String javaScript = "";
		fichier_valeurs = Parametres.REPERTOIRE_DATA_SOURCES + ficval + ".txt";
		Scanner sc = new Scanner(new File(fichier_valeurs));
		File f = new File(dossierRESU);
		if (!f.exists()) {
			f.mkdirs();
		}
		fichier = new File(dossierRESU + "\\" + "RESULTATS_" + nomSITE + "_" + System.currentTimeMillis() + ".txt");
		LogScrap log = new LogScrap("HSBC_Outre_Mer.txt", LogConst.TYPE_LOG_FILE);
		//log = new File(dossierRESU + "\\" + "LOG_" + nomSITE + ".csv");
		File toto = new File(dossierRESU + "\\" + "LOG_" + nomSITE + ".csv");
		if (toto.exists()) {
			String fichierLog = /*dossierRESU + "\\" + "LOG_" + nomSITE + ".csv";*/LogConst.STORE_LOG_PATH + "HSBC_Outre_Mer.txt";
			InputStream ips = new FileInputStream(fichierLog);
			InputStreamReader ipsr = new InputStreamReader(ips);
			BufferedReader br = new BufferedReader(ipsr);
			String ligne;
			num_ligneficval = 0;
			while ((ligne = br.readLine()) != null) {
				num_ligneficval = this.compter(ligne, "(\\d+);FinValeur;", true);
			}
			br.close();
		} else {
			num_ligneficval = num_ligne;
		}
		//this.initialiser();
		parcoursficval = 0;
		while (sc.hasNextLine()) {

			String line = sc.nextLine();
			String[] fic_valeurs = line.split("\\t");
			if (parcoursficval >= num_ligneficval) {
				cp = fic_valeurs[0];
				int pagCounted = 1;
				try {
					chromeDriver.get(url_accueil);

				} catch (Exception e) {
					if (e instanceof SocketTimeoutException) {
						chromeDriver.get(url_accueil);
					}
				}
				/*documentReadyState = this.listensIfPageFullyLoaded(chromeDriver);
				if (documentReadyState) {
					try {
						String strTmp = chromeDriver.getPageSource();
						if (strTmp.contains("Cookies présents sur ce site")) {
							WebElement cookieAccept = chromeDriver
									.findElement(By.cssSelector("#consent_prompt_submit"));
							WebDriverWait wait = new WebDriverWait(chromeDriver, 10);
							wait.until(ExpectedConditions.elementToBeClickable(cookieAccept)).click();
						}*/

						/*
						 * WebElement annonce=chromeDriver.findElement(By.
						 * cssSelector("#modal-covid > div > div > div.modal-body.lf-modal-default__dialog__content__body > div > span"
						 * )); annonce.click();
						 */
					/*} catch (Exception e) {
						if (e instanceof NoSuchElementException) {

						}
					}*/
				
					WebElement input = chromeDriver.findElement(By.cssSelector("#lf-search-query"));
					javaScript = String.format("arguments[0].value=\"%s\"", cp);
					jsExecutor.executeScript(javaScript, input);

					WebElement btnSearch = chromeDriver.findElement(By.cssSelector("#lf-search-submit"));
					btnSearch.click();
					/*
					 * javaScript=String.format("arguments[0].click()","");
					 * jsExecutor.executeScript(javaScript,btnSearch);
					 */

					documentReadyState = this.listensIfPageFullyLoaded(chromeDriver);
					
					if (documentReadyState) {
						Thread.sleep(2000);
						Document doc = null;
						String pageSource = chromeDriver.getPageSource();
						doc = Jsoup.parse(pageSource);
						Element rootJElement = null;
						rootJElement = doc.select("#lf-body-wrapper > div.lf > div > div.lf-results__results").first();
						if (rootJElement != null && !rootJElement.outerHtml().contains("lf-results__results__no-result")) {
							// retrieve nombrepage

							Elements agenciesListJElement = rootJElement.select("#lf-locations > ul >li");
							NbAgences = agenciesListJElement.size();
							int count = 0;
							for (Element tmp : agenciesListJElement) {
								count++;
								String agences_HTM = "", nom_agence = "", adresse = "", tel = "", fax = "",
										HorairesHTM = "", HorairesTXT = "", id_agence = "", json = "";
								id_agence = "#marker" + (count - 1);
								Element jsonJElement = tmp.select("script").first();
								json = jsonJElement.html().replaceAll("\\s{1,}", " ");
								agences_HTM = tmp.outerHtml().trim().replaceAll("\\s{1,}", " ");
								agences_HTM = this.normalize(agences_HTM);
								JSONObject jso = new JSONObject(json);

								// retrieve agency name
								nom_agence = this.normalize(jso.getString("name"));

								// retrieve agency address
								JSONObject jsonAddress = jso.getJSONObject("address");
								adresse = jsonAddress.getString("streetAddress") + " "
										+ jsonAddress.getString("postalCode") + " "
										+ jsonAddress.getString("addressLocality");
								adresse = this.normalize(adresse.replaceAll("[^0-9a-zA-Z ]{1,}", ""));

								// retrieve agency tel
								try {
									tel = jso.getString("telephone");
								} catch (JSONException e) {

								}

								// retrieve agency fax
								fax = "";

								// retrieve agency schedule
								HorairesTXT = this.normalize(jso.getString("openingHours"));
								HorairesHTM = this.normalize("{openingHours:" + HorairesTXT + "}");

								nbRecuperes++;
								this.setData(cp, String.valueOf(NbAgences), nom_agence, adresse, tel, fax, HorairesHTM,
										HorairesTXT, json, agences_HTM);
							}

							pagCounted++;
						}else {
							System.out.println("Aucun résultat");
						}
					}
					
					log.writeLog(new Date(), cp, "no_error", NbAgences, parcoursficval, 0);
				//}
			}
			parcoursficval++;
		}
		sc.close();
		return hsbcList;
	}

	public void enregistrer(String agences_HTM, String nom_agence, String adresse, String tel, String fax,
			String HorairesHTM, String HorairesTXT, String cadre_infos) {

//	    	this.fichier = new File(Parametres.REPERTOIRE + "RESULTATS_BNP.");
		try {

			sortie = new BufferedWriter(new FileWriter(fichier, true));
			sortie.write(cp + "\t" + NbAgences + "\t" + nom_agence + "\t" + adresse + "\t" + tel + "\t" + fax + "\t"
					+ HorairesHTM + "\t" + HorairesTXT + "\t" + cadre_infos + "\t" + agences_HTM + "\r\n");

			sortie.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public void enregistrer_journal(int numreponse) {

//	    	this.fichier = new File(Parametres.REPERTOIRE + "RESULTATS_BNP.");
		try {
			Temps_Fin = new Date();
			SimpleDateFormat formater = null;
			formater = new SimpleDateFormat("'le' dd/MM/yyyy '�' hh:mm:ss");
			String date = formater.format(Temps_Fin);
			date = formater.format(Temps_Fin);

			sortie_log = new BufferedWriter(new FileWriter(log, true));
			sortie_log.write(parcoursficval + ";" + "FinValeur" + ";" + cp + ";" + this.NbAgences + ";" + nbRecuperes
					+ ";" + (this.NbAgences - nbRecuperes) + ";" + date + "\r\n");
			sortie_log.close();
			long taille_fichier = fichier.length() / 1000000; // la length est en octets, on divise par 1000 pour mettre
																// en Ko et par 1000000 pour mettre en Mo

			if (taille_fichier > 4) {
				// INITIALISATION FICHIER DE RESULTAT
				this.fichier = new File(
						dossierRESU + "\\" + "RESULTATS_" + nomSITE + "_" + System.currentTimeMillis() + ".txt");
				// Attention pas d'accent dans les noms de colonnes !
				sortie = new BufferedWriter(new FileWriter(fichier, true));
				sortie.write("CPSoumis" + "\t" + "NbAgences" + "\t" + "NOMWEB" + "\t" + "ADRESSE" + "\t" + "TEL" + "\t"
						+ "FAX" + "\t" + "HORAIRESHTM" + "\t" + "HORAIRESTXT" + "\t" + "CADRE_INFOS" + "\t"
						+ "AGENCEHTM" + "\r\n");
				sortie.close();
			}

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	/**
	 * La m�thode <code>initialiser()</code> permet d'ajouter la ligne d'ent�te dans
	 * le fichier r�sultat.
	 */

	public void initialiser() {

		try {
			// Attention pas d'accent dans les noms de colonnes !
			sortie = new BufferedWriter(new FileWriter(fichier, true));
			sortie.write("DepSoumis" + "\t" + "NbAgences" + "\t" + "NOMWEB" + "\t" + "ADRESSE" + "\t" + "TEL" + "\t"
					+ "FAX" + "\t" + "HORAIRESHTM" + "\t" + "HORAIRESTXT" + "\t" + "CADRE_INFOS" + "\t" + "AGENCEHTM"
					+ "\r\n");
			sortie.close();

			File toto = new File(dossierRESU + "\\" + "LOG_" + nomSITE + ".csv");
			if (!toto.exists()) {
				// if (num_ligneficval == num_ligne) {
				sortie_log = new BufferedWriter(new FileWriter(log, true));
				sortie_log.write("NumLigne" + ";" + "FinValeur" + ";" + "DepSoumis" + ";" + "NbAgences" + ";"
						+ "Nbrecuperes" + ";" + "Reste" + ";" + "Heure_Fin" + "\r\n");
				sortie_log.close();
			}

		} catch (IOException e) {

			e.printStackTrace();
		}
	}

	public void tearDown() {
		chromeDriver.quit();

	}
}
