package com.app.service;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.SocketTimeoutException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Scanner;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.app.model.CIC_Outre_Mer;
import com.app.scrap.RepCicOutreMer;
import com.app.utils.BrowserUtils;
import com.app.utils.Parametres;
import com.app.utils.Utilitaires;

import fa.browserUtils.FactoryBrowser;
import fa.browserUtils.HtmlUnitBrowser;
import fa.log.LogApp.LogScrap;
import fa.log.config.LogConst;

@Service
public class ServiceCIC_Outre_Mer extends Utilitaires {

	@Autowired
	private RepCicOutreMer rep;

	String driverPath = "C:\\ChromeDriver\\";

	private List<CIC_Outre_Mer> cicList = new ArrayList<>();
	String nomSITE = "CIC_Outre_Mer_Agences";
	String ficval = Parametres.OUTRE_MER_NOM_FICHIER;
	String dossierRESU = Parametres.REPERTOIRE_RESULT + "\\BQ\\" + nomSITE;
	String url_debut = "https://www.cic.fr/fr/banques/particuliers/BrowseLocality.aspx?";
	/*
	 * String url_debut =
	 * "https://www.cic.fr/fr/banques/particuliers/SearchList.aspx?type=branch&b2c.p=&b2b.p=&osat.p=&omon.p=&loca=";
	 * String url_fin=
	 * "&Btn.Ok.x=0&Btn.Ok.y=0&sub=true&loadmap=False&adv=&selflat=&selflng=";
	 */
	// https://www.cic.fr/fr/banques/particuliers/SearchList.aspx?type=branch&b2c.p=&b2b.p=&osat.p=&omon.p=&loca=75000&Btn.Ok.x=0&Btn.Ok.y=0&sub=true&loadmap=False&adv=&selflat=&selflng=
	// "https://www.creditmutuel.fr/banques/contact/trouver-une-caisse/BrowseSubdivision.aspx?";
	String fichier_valeurs;
	String CPSoumis;
	String ligne;

	int num_ligne = 2;
	int NbVilles;
	int NbAgences;
	int numreponse = 1;
	int nbRecuperes;
	int num_ligneficval;
	int parcoursficval;
	int Resultats;

	HtmlUnitDriver htmlUnitDriver;
	BufferedWriter sortie;
	BufferedWriter sortie_log;

	Date Temps_Fin;

	public CIC_Outre_Mer saveData(CIC_Outre_Mer t) {

		return (CIC_Outre_Mer) rep.save((CIC_Outre_Mer) t);

	}

	public List<CIC_Outre_Mer> saveDataAll(List<CIC_Outre_Mer> t) {

		return (List<CIC_Outre_Mer>) rep.saveAll((List<CIC_Outre_Mer>) t);

	}

	public List<CIC_Outre_Mer> getElmnt() {

		return rep.findAll();
	}

	public CIC_Outre_Mer getById(int id) {

		return rep.findById(id).orElse(null);
	}

	public String deleteProduct(int id) {
		try {
			rep.deleteById(id);

		} catch (Exception e) {
			e.printStackTrace();
		}

		return "removed";
	}

	public CIC_Outre_Mer updateProduct(CIC_Outre_Mer t) {
		// CIC_Outre_MerODO Auto-generated method stub
		return null;
	}

	public CIC_Outre_Mer setData(String depSoumis, String nbVilles, String numVilles, String villeSoumise,
			String nbAgences, String nomWeb, String adresse, String horaire, String telephone, String lienAgence,
			String agenceHtm) {
		CIC_Outre_Mer cic = new CIC_Outre_Mer();
		cic.setDepSoumis(depSoumis).setNbVilles(nbVilles).setNumVilles(numVilles).setVilleSoumise(villeSoumise)
				.setNbAgences(nbAgences).setNomWeb(nomWeb).setAdresse(adresse).setHoraire(horaire)
				.setTelephone(telephone).setLienAgence(lienAgence).setAgenceHtm(agenceHtm);
		this.saveData(cic);
		return cic;
	}

	public List<CIC_Outre_Mer> showFirstRows() {
		List<CIC_Outre_Mer> a = rep.getFirstRow(100, CIC_Outre_Mer.class);
		return a;
	}

	public List<CIC_Outre_Mer> showLastRows() {
		List<CIC_Outre_Mer> a = rep.getLastRow(100, CIC_Outre_Mer.class);
		return a;
	}

	public void setup() {
		FactoryBrowser fact = new FactoryBrowser();
		HtmlUnitBrowser hB = (HtmlUnitBrowser) fact.create("htmlUnit");
		htmlUnitDriver = hB.setOptions(false, false, false, 60000).initBrowserDriver();
	}

	public List<CIC_Outre_Mer> scrapCIC_Outre_Mer() throws IOException {
		fichier_valeurs = Parametres.REPERTOIRE_DATA_SOURCES + ficval + ".txt";
		List<String> listVille = new ArrayList<>();
		Scanner sc = new Scanner(new BufferedInputStream(new FileInputStream(new File(fichier_valeurs))));
		Scanner sc1 = new Scanner(new BufferedInputStream(new FileInputStream(new File(fichier_valeurs))));
		if (!new File(dossierRESU).exists()) {
			new File(dossierRESU).mkdirs();

		}
		fichier = new File(dossierRESU + "\\" + "RESULTATS_" + nomSITE + "_" + System.currentTimeMillis() + ".txt");
		LogScrap log = new LogScrap("BanqueCIC_OUTRE_MER.txt", LogConst.TYPE_LOG_FILE);
		num_ligneficval = ((new File(LogConst.STORE_LOG_PATH + LogConst.PREFIX_LOG_FILE + "_BanqueCIC_OUTRE_MER.txt"))
				.exists()) ? Integer.parseInt(log.readLog().getLastLineProcessed().split("\\t")[4]) : 1;

		parcoursficval = 1;
		int numville = 0;
		int i = 1;

		while (sc1.hasNextLine()) {
			String line = sc1.nextLine();
			if (i > 1) {
				NbVilles++;
			}
			i++;
		}
		sc1.close();
		while (sc.hasNextLine()) {

			String line = sc.nextLine();
			String[] fic_valeurs = line.split("\\t");
			if (parcoursficval >= num_ligneficval) {

				numville++;
				CPSoumis = fic_valeurs[0];
				// https://www.creditmutuel.fr/cmag/fr/banques/contact/trouver-une-agence/SearchList.aspx?type=branch&osat.p=&omon.p=&loca=75000&Btn.Ok.x=53&Btn.Ok.y=23&sub=true&loadmap=False&adv=&selflat=&selflng=
				String uri = url_debut + CPSoumis;
				try {
					System.out.println(uri);
					htmlUnitDriver.get(uri);
				} catch (Exception e) {
					if (e instanceof SocketTimeoutException) {
						htmlUnitDriver.get(uri);
					}
				}
				if (!listVille.isEmpty()) {
					listVille.clear();
				}
				String pageSource = htmlUnitDriver.getPageSource();
				Document doc = Jsoup.parse(pageSource);
				// System.out.println(pageSource);
				Element villeRoot = doc
						.selectFirst("#ei_tpl_content > div > article > div.body > div > div.a_blocfctl.lister");

				if (villeRoot != null) {
					Elements villeLinkElements = villeRoot.select("table > tbody > tr");

					for (Element villeLinkElement : villeLinkElements) {
						Element cpElement = villeLinkElement.selectFirst("td:nth-child(2)");
						if (this.isDOM(cpElement.text())) {
							Element pathElement = villeLinkElement.selectFirst("td.i.a_actions.nowrap>a");
							listVille.add(pathElement.attr("href"));
						}

					}
				}

				if (!listVille.isEmpty()) {
					String url = "https://www.cic.fr/fr/banques/particuliers/";
					for (String link : listVille) {

						String uri2 = url + link;
						try {
							System.out.println(uri2);
							htmlUnitDriver.get(uri2);
						} catch (Exception e) {
							if (e instanceof SocketTimeoutException) {
								htmlUnitDriver.get(uri2);
							}
						}
						pageSource = htmlUnitDriver.getPageSource();
						doc = Jsoup.parse(pageSource);
						Element root = doc.select("#rslt").first();
						String agences_HTM = "", nom_agence = "", adresse = "", LienAgence = "", tel = "",
								horaires = "", villeSoumise = "";
						if (root != null) {

							Elements agenciesLinkJElement = root.select("#rslt_lst> li>div.body");
							NbAgences = agenciesLinkJElement.size();
							nbRecuperes = 0;
							for (Element tmp : agenciesLinkJElement) {
								agences_HTM = "";
								nom_agence = "";
								adresse = "";
								LienAgence = "";
								tel = "";
								horaires = "";
								villeSoumise = "";

								agences_HTM = this.normalize(tmp.outerHtml().replaceAll("\\s{1,}", " "));

								// retrieve agency link
								Element agencyNamaElement = tmp.select("span.lbl.titre3 > em > a").first();
								LienAgence = "https://www.creditmutuel.fr/cmag/fr/banques/contact/trouver-une-agence/"
										+ agencyNamaElement.attr("href");
								System.out.println("le lien de l'agences est " + LienAgence);

								// retrieve agency name
								nom_agence = (agencyNamaElement != null) ? this.normalize(agencyNamaElement.text())
										: "";
								System.out.println("le nom de l'agence est " + nom_agence);

								// retrieve agency tel
								Element agencyTelElement = tmp
										.selectFirst("span:nth-child(3)>span[itemprop=\"telephone\"]");
								tel = (agencyTelElement != null) ? agencyTelElement.text() : "";
								System.out.println(tel);

								// retrieve horaires
								Elements scheduleElements = tmp
										.select("span:nth-child(3)>div.schdl.schdl_short.g>table>tbody>tr");
								for (Element scheduleElement : scheduleElements) {
									horaires += scheduleElement.text() + " ";
								}
								horaires = this.normalize(horaires);
								System.out.println("horaires " + horaires);

								// retrieve agency address
								Element agencyAddressElement = tmp.select("span:nth-child(3) > em > span.invisible")
										.first();
								adresse = (agencyAddressElement != null) ? agencyAddressElement.text() : "";
								System.out.println(adresse);
								nbRecuperes++;

								villeSoumise = uri.replace("SearchList.aspx?sub=true&amp;type=branch&amp;loca=", "");

								cicList.add(this.setData(CPSoumis, String.valueOf(NbVilles), String.valueOf(numville),
										villeSoumise, String.valueOf(NbAgences), nom_agence, adresse, horaires, tel,
										LienAgence, agences_HTM));
							}

						}
					}
				}
				log.writeLog(new Date(), CPSoumis, "no_error", NbAgences, parcoursficval, 0);
			}
			parcoursficval++;
		}
		sc.close();
		return cicList;
	}

	public void tearsDown() {
		htmlUnitDriver.close();
	}

	public void enregistrer(String agences_HTM, String nom_agence, String adresse, String LienAgence, int numville,
			String Villesoumise) {

		try {

			/*
			 * String depTXT = DepSoumis; depTXT = depTXT.replaceAll("SubdivisionId=FR-",
			 * "");
			 */
			sortie = new BufferedWriter(new FileWriter(fichier, true));
			sortie.write(CPSoumis + "\t" + NbVilles + "\t" + numville + "\t" + Villesoumise + "\t" + NbAgences + "\t"
					+ nom_agence + "\t" + adresse + "\t" + LienAgence + "\t" + agences_HTM + "\r\n");

			sortie.close();
		} catch (IOException e) {

			e.printStackTrace();
		}

	}

	public void enregistrer_journal(int numreponse, int numville) {

//	    	this.fichier = new File(Parametres.REPERTOIRE + "RESULTATS_BNP.");
		try {
			Temps_Fin = new Date();
			SimpleDateFormat formater = null;
			formater = new SimpleDateFormat("'le' dd/MM/yyyy '�' hh:mm:ss");
			String date = formater.format(Temps_Fin);
			date = formater.format(Temps_Fin);

			/*
			 * String depTXT = DepSoumis; depTXT = depTXT.replaceAll("SubdivisionId=FR-",
			 * "");
			 */

			sortie_log = new BufferedWriter(new FileWriter(log, true));
			sortie_log.write(parcoursficval + ";" + "FinValeur" + ";" + CPSoumis + ";" + this.NbVilles + ";" + "VILLE_"
					+ numville + ";" + this.NbAgences + ";" + nbRecuperes + ";" + (this.NbAgences - nbRecuperes) + ";"
					+ date + "\r\n");
			sortie_log.close();
			long taille_fichier = fichier.length() / 1000000; // la length est en octets, on divise par 1000 pour mettre
																// en Ko et par 1000000 pour mettre en Mo

			if (taille_fichier > 8) {
				// INITIALISATION FICHIER DE RESULTAT
				this.fichier = new File(
						dossierRESU + "\\" + "RESULTATS_" + nomSITE + "_" + System.currentTimeMillis() + ".txt");
				// Attention pas d'accent dans les noms de colonnes !
				sortie = new BufferedWriter(new FileWriter(fichier, true));
				sortie.write("DepSoumis" + "\t" + "NbVilles" + "\t" + "Numville" + "\t" + "VilleSoumise" + "\t"
						+ "NbAgences" + "\t" + "NOMWEB" + "\t" + "ADRESSE" + "\t" + "LIEN_AGENCE" + "\t" + "AGENCEHTM"
						+ "\r\n");
				sortie.close();
			}

		} catch (IOException e) {

			e.printStackTrace();
		}

	}

	/**
	 * La m�thode <code>initialiser()</code> permet d'ajouter la ligne d'ent�te dans
	 * le fichier r�sultat.
	 */

	public void initialiser() {

		try {
			// Attention pas d'accent dans les noms de colonnes !
			sortie = new BufferedWriter(new FileWriter(fichier, true));
			sortie.write(
					"DepSoumis" + "\t" + "NbVilles" + "\t" + "Numville" + "\t" + "VilleSoumise" + "\t" + "NbAgences"
							+ "\t" + "NOMWEB" + "\t" + "ADRESSE" + "\t" + "LIEN_AGENCE" + "\t" + "AGENCEHTM" + "\r\n");
			sortie.close();

			File toto = new File(dossierRESU + "\\" + "LOG_" + nomSITE + ".csv");
			if (!toto.exists()) {
				// if (num_ligneficval == num_ligne) {
				sortie_log = new BufferedWriter(new FileWriter(log, true));
				sortie_log
						.write("NumLigne" + ";" + "FinValeur" + ";" + "DepSoumis" + ";" + "NbVilles" + ";" + "NumVILLE"
								+ ";" + "NbAgences" + ";" + "Nbrecuperes" + ";" + "Reste" + ";" + "Heure_Fin" + "\r\n");
				sortie_log.close();
			}

		} catch (IOException e) {

			e.printStackTrace();
		}
	}

}
