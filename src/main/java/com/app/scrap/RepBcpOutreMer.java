package com.app.scrap;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.app.model.BCP_Outre_Mer;

@Repository
public interface RepBcpOutreMer extends JpaRepository<BCP_Outre_Mer, Integer>,RepositoryCustom<BCP_Outre_Mer>{

}
