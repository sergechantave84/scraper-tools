package com.app.model;

import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "banque_ce_dep_outre_mer", schema = "scrap")
public class CE_Outre_Mer {

	@Id
	@SequenceGenerator(name = "scrap.banque_ce_dep_outre_mer_id_seq", sequenceName = "scrap.banque_ce_dep_outre_mer_id_seq", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "scrap.banque_ce_dep_outre_mer_id_seq")

	@Column(name = "id", updatable = false, columnDefinition = "serial")
	private int id;

	@Column(name = "depsoumis", columnDefinition = "varchar")
	private String depSoumis;

	@Column(name = "nbagences", columnDefinition = "varchar")
	private String nbAgences;

	@Column(name = "nomweb", columnDefinition = "varchar")
	private String nomWeb;

	@Column(name = "adresse", columnDefinition = "varchar")
	private String adresse;

	@Column(name = "tel", columnDefinition = "varchar")
	private String tel;

	@Column(name = "fax", columnDefinition = "varchar")
	private String fax;

	@Column(name = "horaireshtm", columnDefinition = "varchar")
	private String horairesHtm;

	@Column(name = "horairestxt", columnDefinition = "varchar")
	private String horairestxt;

	@Column(name = "dab", columnDefinition = "varchar")
	private String dab;

	@Column(name = "agencehtm", columnDefinition = "varchar")
	private String agenceHtm;

	@Column(name="service", columnDefinition = "varchar")
	private String services;
	
	@Column(name = "annee_du_scrap", columnDefinition = "varchar(4) DEFAULT date_part('year',CURRENT_DATE)")
	private String anneeScrap;

	public CE_Outre_Mer() {
		LocalDate ld = new Date().toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
		anneeScrap = String.valueOf(ld.getYear() + 1);
		
	}

	public int getId() {
		return id;
	}

	public CE_Outre_Mer setId(int id) {
		this.id = id;
		return this;
	}

	public String getDepSoumis() {
		return depSoumis;
	}

	public CE_Outre_Mer setDepSoumis(String depSoumis) {
		this.depSoumis = depSoumis;
		return this;
	}

	public String getNbAgences() {
		return nbAgences;
	}

	public CE_Outre_Mer setNbAgences(String nbAgences) {
		this.nbAgences = nbAgences;
		return this;
	}

	public String getNomWeb() {
		return nomWeb;
	}

	public CE_Outre_Mer setNomWeb(String nomWeb) {
		this.nomWeb = nomWeb;
		return this;
	}

	public String getAdresse() {
		return adresse;
	}

	public CE_Outre_Mer setAdresse(String adresse) {
		this.adresse = adresse;
		return this;
	}

	public String getTel() {
		return tel;
	}

	public CE_Outre_Mer setTel(String tel) {
		this.tel = tel;
		return this;
	}

	public String getFax() {
		return fax;
	}

	public CE_Outre_Mer setFax(String fax) {
		this.fax = fax;
		return this;
	}

	public String getHorairesHtm() {
		return horairesHtm;
	}

	public CE_Outre_Mer setHorairesHtm(String horairesHtm) {
		this.horairesHtm = horairesHtm;
		return this;
	}

	public String getHorairestxt() {
		return horairestxt;
	}

	public CE_Outre_Mer setHorairestxt(String horairestxt) {
		this.horairestxt = horairestxt;
		return this;
	}

	public String getDab() {
		return dab;
	}

	public CE_Outre_Mer setDab(String dab) {
		this.dab = dab;
		return this;
	}

	public String getAgenceHtm() {
		return agenceHtm;
	}

	public CE_Outre_Mer setAgenceHtm(String agenceHtm) {
		this.agenceHtm = agenceHtm;
		return this;
	}

	public String getAnneeScrap() {
		return anneeScrap;
	}

	public CE_Outre_Mer setAnneeScrap(String anneeScrap) {
		this.anneeScrap = anneeScrap;
		return this;
	}

	public String getServices() {
		return services;
	}

	public CE_Outre_Mer  setServices(String services) {
		this.services = services;
		return this;
	}
}
