package com.app.model;

import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="axa", schema="scrap")
public class AXA {
 
	@Id
	@SequenceGenerator( name="scrap.axa_id_seq",
	                    sequenceName ="scrap.axa_id_seq",
	                    allocationSize=1)
    @GeneratedValue( strategy=GenerationType.SEQUENCE,
                     generator="scrap.axa_id_seq")
	  
	@Column(name="id",updatable=false)
	private int id;
	
	@Column(name="depsoumis", columnDefinition="varchar")
	private String depSoumis;
	
	@Column(name="nbvilles", columnDefinition="varchar")
	private String nbVilles;
	
	@Column(name="ville_souimse", columnDefinition="varchar")
	private String villeSoumise;
	
	@Column(name="nbagences_byville", columnDefinition="varchar")
	private String nbAgenceVille;
	
	@Column(name="nom_agent", columnDefinition="varchar")
	private String nomAgent;
	
	@Column(name="nom_agence", columnDefinition="varchar")
	private String nomAgence;
	
	@Column(name="adresse", columnDefinition="varchar")
	private String adresse;
	
	@Column(name="telephone", columnDefinition="varchar")
	private String tel;
	
	@Column(name="orias", columnDefinition="varchar")
	private String orias;
	
	@Column(name="horaires", columnDefinition="varchar")
	private String horaire;
	
	@Column(name="agencehtml", columnDefinition="varchar")
	private String agenceHTM;
	
	@Column(name="annee_du_scrap" ,columnDefinition="varchar(4) DEFAULT date_part('year',CURRENT_DATE)" )
	private String anneeScrap;
	
	public AXA() {
		LocalDate ld= new Date().toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
		anneeScrap=String.valueOf(ld.getYear()+1);
	}
	
	public int getId() {
		return id;
	}
	public AXA setId(int id) {
		this.id = id;
		return this;
	}
	public String getDepSoumis() {
		return depSoumis;
	}
	public AXA setDepSoumis(String depSoumis) {
		this.depSoumis = depSoumis;
		return this;
	}
	public String getNbVilles() {
		return nbVilles;
	}
	public AXA setNbVilles(String nbVilles) {
		this.nbVilles = nbVilles;
		return this;
	}
	public String getVilleSoumise() {
		return villeSoumise;
	}
	public AXA setVilleSoumise(String villeSoumise) {
		this.villeSoumise = villeSoumise;
		return this;
	}
	public String getNbAgenceVille() {
		return nbAgenceVille;
	}
	public AXA setNbAgenceVille(String nbAgenceVille) {
		this.nbAgenceVille = nbAgenceVille;
		return this;
	}
	public String getAgenceHTM() {
		return agenceHTM;
	}
	public AXA setAgenceHTM(String agenceHTM) {
		this.agenceHTM = agenceHTM;
		return this;
	}
	public String getAnneeScrap() {
		return anneeScrap;
	}
	public AXA setAnneeScrap(String anneeScrap) {
		this.anneeScrap = anneeScrap;
		return this;
	}

	public String getNomAgent() {
		return nomAgent;
	}

	public AXA setNomAgent(String nomAgent) {
		this.nomAgent = nomAgent;
		return this;
	}

	public String getNomAgence() {
		return nomAgence;
	}

	public AXA setNomAgence(String nomAgence) {
		this.nomAgence = nomAgence;
		return this;
	}

	public String getAdresse() {
		return adresse;
	}

	public AXA setAdresse(String adresse) {
		this.adresse = adresse;
		return this;
	}

	public String getTel() {
		return tel;
	}

	public AXA setTel(String tel) {
		this.tel = tel;
		return this;
	}

	public String getOrias() {
		return orias;
	}

	public AXA setOrias(String orias) {
		this.orias = orias;
		return this;
	}

	public String getHoraire() {
		return horaire;
	}

	public AXA setHoraire(String horaire) {
		this.horaire = horaire;
		return this;
	}
}
