package com.app.scrap;

import org.springframework.data.jpa.repository.JpaRepository;

import com.app.model.BanqueCMB;

public interface RepBanqueCMB extends JpaRepository<BanqueCMB,Integer>,RepositoryCustom<BanqueCMB> {

}
