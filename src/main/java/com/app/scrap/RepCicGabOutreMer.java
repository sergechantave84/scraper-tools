package com.app.scrap;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.app.model.CIC_GAB_Outre_Mer;

@Repository
public interface RepCicGabOutreMer extends JpaRepository<CIC_GAB_Outre_Mer, Integer>, RepositoryCustom<CIC_GAB_Outre_Mer> {

}
