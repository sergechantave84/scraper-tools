package com.app.scrap;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.app.model.BNP_PRIV;

@Repository
public interface RepBnpPriv extends JpaRepository<BNP_PRIV, Integer> {

}
