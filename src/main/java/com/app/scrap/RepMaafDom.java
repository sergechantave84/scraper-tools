package com.app.scrap;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.app.model.MAAF;

@Repository
public interface RepMaafDom extends JpaRepository<MAAF, Integer>,RepositoryCustom<MAAF> {

}
