package com.app.scrap;



import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.app.model.Areas;
import com.app.model.Areas_DOM;

@Repository
public interface RepAreaDom extends JpaRepository <Areas_DOM,Integer>,RepositoryCustom<Areas_DOM> {

}
