package com.app.service;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.SocketTimeoutException;
import java.text.Normalizer;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Scanner;

import org.json.JSONArray;
import org.json.JSONObject;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.NoAlertPresentException;
import org.openqa.selenium.PageLoadStrategy;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.app.model.BP_Outre_Mer;
import com.app.scrap.RepBpOutreMer;
import com.app.utils.BrowserUtils;
import com.app.utils.Parametres;
import com.app.utils.Utilitaires;

import fa.log.LogApp.LogScrap;
import fa.log.config.LogConst;

@Service
public class ServiceBP_Outre_Mer extends Scraper {

	private static String driverPath = "C:\\ChromeDriver\\";
	private List<BP_Outre_Mer> bpList = new ArrayList<>();

	private String nomSITE = "BP_Outre_Mer";
	private String ficval = "lien_bp_outre_mer";
	private String dossierRESU = Parametres.REPERTOIRE_RESULT + "\\BQ\\" + nomSITE;
	private String url_accueil = "https://agences.banquepopulaire.fr/banque-assurance/";
	private String fichier_valeurs;
	private String codePostale;
	private String ville;
	// private String ligne;

	private int num_ligne = 2;
	private int NbAgences;
	// private int numreponse = 1;
	private int nbRecuperes;
	private int num_ligneficval;
	private int parcoursficval;

	private static HtmlUnitDriver htmlUnitDriver;

	@Autowired
	private RepBpOutreMer rep;

	public void setup() {
		htmlUnitDriver = (HtmlUnitDriver) this.setupHU();
	}

	public void setData(String codePostale, String total, String nbAgences, String enseigne, String nomWeb,
			String adresse, String tel, String fax, String horairesHtm, String horairesBP_Outre_Merxt, String services,
			String dab, String agenceHtm) {
		BP_Outre_Mer bp = new BP_Outre_Mer();
		bp.setCodePostale(codePostale).setTotal(total).setNbAgences(nbAgences).setEnseigne(enseigne).setNomWeb(nomWeb)
				.setAdresse(adresse).setTel(tel).setFax(fax).setHorairesHtm(horairesHtm)
				.setHorairesTxt(horairesBP_Outre_Merxt).setServices(services).setDab(dab).setAgenceHtm(agenceHtm);
		rep.save(bp);

	}

	@Override
	public void saveItem(String... args) {
		// TODO Auto-generated method stub

	}

	@Override
	public void scrap() throws Exception {
		fichier_valeurs = Parametres.REPERTOIRE_DATA_SOURCES + ficval + ".txt";
		Scanner sc = new Scanner(new BufferedInputStream(new FileInputStream(new File(fichier_valeurs))));

		if (!new File(dossierRESU).exists()) {
			new File(dossierRESU).mkdirs();
		}

		LogScrap log = new LogScrap("BanqueBP_dom.txt", LogConst.TYPE_LOG_FILE);
		num_ligneficval = ((new File(LogConst.STORE_LOG_PATH + LogConst.PREFIX_LOG_FILE + "_BanqueBP_dom.txt"))
				.exists()) ? Integer.parseInt(log.readLog().getLastLineProcessed().split("\\t")[4]) : 1;

		parcoursficval = 0;
		while (sc.hasNextLine()) {
			String line = sc.nextLine();
			String[] fic_valeurs = line.split("\\t");
			if (parcoursficval >= num_ligneficval) {
				nbRecuperes = 0;
				codePostale = fic_valeurs[0];

				String url = url_accueil + fic_valeurs[1];
				System.out.println(url);
				try {
					htmlUnitDriver.get(url);

				} catch (Exception e) {
					if (e instanceof SocketTimeoutException) {
						htmlUnitDriver.get(url);

					}
				}

				// Thread.sleep(10000);
				String pageSource = "";
				Document doc = null;
				List<String> agencyLinks = new ArrayList<>();
				pageSource = htmlUnitDriver.getPageSource();
				doc = Jsoup.parse(pageSource);
				
				Element rootJElement = doc.select(
						"body > div.em-page > main > div.em-results-wrapper > div > div.em-results__list-wrapper > div")
						.first();
				Elements agencyLinksJElement = rootJElement.select("ul > li");
				
				if (!agencyLinks.isEmpty()) agencyLinks.clear();
				
				for (Element elmntTmp : agencyLinksJElement) {
					Element hrefElement = elmntTmp.selectFirst("h2>a");
					agencyLinks.add(hrefElement.attr("href"));
				}
				
				NbAgences = agencyLinks.size();
				System.out.println("NbAgences : " + NbAgences);
				nbRecuperes = 0;

				for (String strTmp : agencyLinks) {
					try {
						htmlUnitDriver.get("https://agences.banquepopulaire.fr" + strTmp);

					} catch (Exception e) {
						if (e instanceof SocketTimeoutException) {
							htmlUnitDriver.get("https://agences.banquepopulaire.fr" + strTmp);

						}
					}

					Thread.sleep(5000);
					String agences_HTM = "", nom_agence = "", adresse = "", tel = "", fax = "", HorairesHTM = "",
							HorairesTXT = "", Services = "", enseigne = "";
					int total = 0, DAB = 0;
					total = NbAgences;
					pageSource = htmlUnitDriver.getPageSource();
					doc = Jsoup.parse(pageSource);

					// TODO change
					Element root2JElement = doc.select("div.em-page > main > div.em-details > div.em-details__poi-card")
							.first();
					agences_HTM = root2JElement.outerHtml().trim().replaceAll("\\s{1,}", "");
					agences_HTM = this.normalize(agences_HTM);

					// retrieve agency name
					Element agencyNameJElement = root2JElement.select("h2.em-details__label").first();
					nom_agence = (agencyNameJElement != null) ? this.normalize(agencyNameJElement.text()) : "";
					System.out.println("nom agence : " + nom_agence);

					// retrieve agency address
					Element agencyAddressJElement = root2JElement
							.select("div.em-details__info>div:nth-child(1)>div.em-details__address>div").first();
					adresse = (agencyAddressJElement != null) ? this.normalize(agencyAddressJElement.text()) : "";
					System.out.println("adresse : " + adresse);

					// retrieve agency tel
					Element agencyTelJElement = root2JElement.select("div.em-details__info>div:nth-child(2)>a").first();
					tel = (agencyTelJElement != null) ? agencyTelJElement.attr("href").replaceAll("[^0-9]", "") : "";
					System.out.println("telephone : " + tel);

					// retrieve agency fax
					Element agencyFaxJElement = root2JElement.select("div.em-details__info>div:nth-child(2)>div>span")
							.first();
					fax = (agencyFaxJElement != null) ? agencyFaxJElement.text().replaceAll("[^0-9]", " ") : "";
					System.out.println("fax : " + fax);

					// retrieve agency schedule
					Element agencyScheduleJElement = doc.selectFirst("head > script[type=\"application/ld+json\"]");
					//System.out.println(agencyScheduleJElement);
					String agencyInfo = agencyScheduleJElement.html();
					try {
						/*agencyInfo = agencyInfo.replaceAll("//\\<\\!\\[CDATA\\[", "");
						agencyInfo = agencyInfo.replaceAll("//\\]{1,}>", "");
						agencyInfo = agencyInfo.replaceAll("\\s{2,}", "");
						HorairesHTM = agencyInfo;
						JSONObject jsonRoot = new JSONObject(agencyInfo);
						JSONArray jsons = jsonRoot.getJSONArray("openingHoursSpecification");
						int jl = jsons.length();
						for (int i = 0; i < jl; i++) {
							JSONObject json = jsons.getJSONObject(i);

							String day = json.getString("dayOfWeek");
							day = day.replaceAll("http://schema.org/", "").trim();
							switch (day) {
							case "Monday": {
								day = "lundi";
								break;
							}
							case "Tuesday": {
								day = "mardi";
								break;
							}
							case "Wednesday": {
								day = "mercredi";
								break;
							}
							case "Thursday": {
								day = "jeudi";
								break;
							}
							case "Friday": {
								day = "vendredi";
								break;
							}
							case "Saturday": {
								day = "samedi";
								break;
							}
							case "Sunday": {
								day = "dimanche";
								break;
							}

							}
							String hours = json.getString("opens") + "-" + json.getString("closes");
							HorairesTXT += day + " " + hours + " ";

						}
						HorairesTXT = this.normalize(HorairesTXT);
						System.out.println("horaires " + HorairesTXT);*/
						
						// ADD BY NANTE
						Element emPage = doc.selectFirst("div.em-page");
						Element script = emPage.nextElementSibling();
						/*Elements agencySheduleJElement = root2JElement.select(
								"div.em-details__horaires-bloc > div > div.em-graphical-schedules > div > ul.graphicalSchedules__days > li");*/
						
						String[] splitScripts = (script.html().toString()).split("day1");
						
						String days = splitScripts[1].replaceAll("/", "").replaceAll("\"", "").replace("day2:", "").replace("day3:", "").replace("day4:", "").replace("day5:", "").replace("day6:", "");
					
						//System.out.println("day1 : " + days);
						
						String[] listHoraire = days.split(",");
						
						String lundi = listHoraire[0].contains("h")?listHoraire[0].replaceAll("day1 : :", "").replaceAll(":", "") : "Fermee";
						String mardi = listHoraire[1].contains("h")?listHoraire[1] : "Fermee";
						String mercredi = listHoraire[2].contains("h")?listHoraire[2] : "Fermee";
						String jeudi = listHoraire[3].contains("h")?listHoraire[3] : "Fermee";
						String vendredi = listHoraire[4].contains("h")?listHoraire[4] : "Fermee";
						String samedi = listHoraire[5].contains("h")?listHoraire[5] : "Fermee";
						
						lundi = "Lundi : " + lundi;
						mardi = "Mardi : " + mardi;
						mercredi = "Mercredi : " + mercredi;
						jeudi = "Jeudi : " + jeudi;
						vendredi = "Vendredi : " + vendredi;
						samedi = "Samedi : " + samedi;
						
						String dim = " Dimanche : Ferme";
						
						/*for (Element tmp : agencySheduleJElement) {
							HorairesTXT += tmp.text() + " ";
						}
						HorairesTXT = this.normalize(HorairesTXT.replaceAll("[^0-9a-zA-z ]", " "));*/
						
						HorairesTXT = lundi + " " + mardi + " " + mercredi + " " + jeudi + " " + vendredi + " " + samedi+dim;
						HorairesTXT = this.normalize(HorairesTXT);
						System.out.println("Horaires : " + HorairesTXT);
						
						
					} catch (Exception e) {
						e.printStackTrace();
					}

					// retrieve agency services

					Elements agencyServiceJElement = doc
							.select("div.em-page > main > div.em-details__services-bloc.bloc-bp>ul>li");
					if (agencyServiceJElement != null) {
						for (Element elementTmp : agencyServiceJElement) {
							Services += elementTmp.text() + " ";
						}
						if (Services.contains("Distributeur automatique de billets")) {
							DAB = 1;
						}
					} else {
						Services = "";
						DAB = 0;
					}
					System.out.println("DAB " + DAB);
					Services = this.normalize(Services);
					System.out.println("services " + Services);
					// retrieve agency tag
					Element agencyTagJElement = root2JElement.select("#aside > div > div > a.agencyreseau").first();
					enseigne = (agencyTagJElement != null) ? agencyTagJElement.text() : "";

					nbRecuperes++;
					this.setData(codePostale, String.valueOf(total), String.valueOf(NbAgences), enseigne, nom_agence,
							adresse, tel, fax, HorairesHTM, HorairesTXT, Services, String.valueOf(DAB), agences_HTM);

				}

				log.writeLog(new Date(), url, "no_error", NbAgences, parcoursficval, NbAgences - nbRecuperes);
			}
			parcoursficval++;
		}

		sc.close();

	}

	public void tearDown() {
		htmlUnitDriver.quit();

	}

}
