package com.app.service;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;

import java.io.InputStreamReader;

import java.net.URL;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Scanner;

import javax.net.ssl.HttpsURLConnection;

import org.apache.commons.text.StringEscapeUtils;
import org.json.JSONObject;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import org.openqa.selenium.htmlunit.HtmlUnitDriver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.app.model.MAIF;

import com.app.scrap.RepMaif;

import com.app.utils.Parametres;



import fa.log.LogApp.LogScrap;
import fa.log.config.LogConst;

@Service
public class ServiceMAIF extends Scraper {

	@Autowired
	private RepMaif rep;

	

	String driverPath = "C:\\ChromeDriver\\";
	String nomSITE = "MAIF";
	String ficval = "lien_maif.txt";// "liens_MAIF";
	String dossierRESU = Parametres.REPERTOIRE_RESULT + "\\ASSU\\" + nomSITE;
	String url_accueil = "https://agence.maif.fr";
	String fichier_valeurs;
	String LienAgence;
	String ligne;

	int num_ligne = 2;
	int NbInscriptions;
	int numreponse = 1;
	int nbRecuperes;
	int num_ligneficval;
	int parcoursficval;
	int Resultats;
	int NbAgences_trouvees;
	int NbAgences;

	HtmlUnitDriver htmlUnitDriver;
	BufferedWriter sortie;
	BufferedWriter sortie_log;
	Date Temps_Fin;

	public void setData(String lienAgence, String nbAgences, String nomAgence, String adresse, String tel,
			String horaires, String agenceHtm) {
		MAIF maif = new MAIF();
		maif.setLienAgence(lienAgence).setNbAgences(nbAgences).setNomAgence(nomAgence).setAdresse(adresse).setTel(tel)
				.setHoraires(horaires).setAgenceHtm(agenceHtm);
		rep.save(maif);

	}

	public void setUp() throws Exception {
		/*
		 * FactoryBrowser factBrowser = new FactoryBrowser(); HtmlUnitBrowser hB =
		 * (HtmlUnitBrowser) factBrowser.create("htmlUnit"); htmlUnitDriver =
		 * hB.setOptions(false, false, false, 60000).initBrowserDriver();
		 */
	}

	@Override
	public void saveItem(String... args) {
		// TODO Auto-generated method stub

	}

	@Override
	public void scrap() throws Exception {

		List<String> agenciesLinks = new ArrayList<>();
		Document doc = null;

		Scanner sc = new Scanner(new BufferedInputStream(new FileInputStream(new File(Parametres.REPERTOIRE_DATA_SOURCES+ficval))));

		LogScrap log = new LogScrap("AssuranceMaif.txt", LogConst.TYPE_LOG_FILE);
		num_ligneficval = ((new File(LogConst.STORE_LOG_PATH + LogConst.PREFIX_LOG_FILE + "_AssuranceMaif.txt")).exists())
				? Integer.parseInt(log.readLog().getLastLineProcessed().split("\\t")[4])
				: 1;

		parcoursficval = 1;
		while (sc.hasNextLine()) {

			String[] lines = sc.nextLine().split("\\t");
			if (parcoursficval > num_ligneficval) {

				String url = url_accueil + lines[1];
				System.out.println(url);
				URL urlb = new URL(url);
				HttpsURLConnection con = (HttpsURLConnection) urlb.openConnection();
				con.setRequestMethod("GET");
				con.setConnectTimeout(600000);
				con.setReadTimeout(60000);
				BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream(), "UTF-8"));
				String pageSource;
				StringBuilder strB = new StringBuilder();
				while ((pageSource = in.readLine()) != null) {
					strB.append(pageSource);
				}
				pageSource = StringEscapeUtils.unescapeHtml4(strB.toString());

				doc = Jsoup.parse(pageSource);
				Elements agenciesLinksElement = doc.select(
						"#content > div > div.mod.maif-form > div > div.col-md-4.em-list-container > ul > li > a.details.em-seo-link");
				
				if (!agenciesLinks.isEmpty())
					agenciesLinks.clear();
				for (Element agencyLinkElement : agenciesLinksElement)
					agenciesLinks.add(agencyLinkElement.attr("href"));

				for (String agencyLink : agenciesLinks) {
					url = url_accueil + agencyLink;
					System.out.println("url : "+url);
					urlb = new URL(url);
					con = (HttpsURLConnection) urlb.openConnection();
					con.setRequestMethod("GET");
					con.setConnectTimeout(600000);
					con.setReadTimeout(60000);
					in = new BufferedReader(new InputStreamReader(con.getInputStream(), "UTF-8"));

					strB = new StringBuilder();
					while ((pageSource = in.readLine()) != null) {
						strB.append(pageSource);
					}
					pageSource = StringEscapeUtils.unescapeHtml4(strB.toString());

					doc = Jsoup.parse(pageSource);

					//document.querySelector("head > script:nth-child(84)")
					
					Element jsonElement = doc.select("script[type=\"application/ld+json\"]").last();
					
					//System.out.println(jsonElement.text());
					
					
					
					String json = "", agencyName = "", agencyPhone = "", agencyAddress = "", agencySchedule = "",
							agencyHtm = "";
					Element htm = doc.select("div[class=\"emDetail emDetails--delegation\"]").first();
					if (htm != null)
						agencyHtm = htm.outerHtml().replaceAll("\\s{1,}", "").trim();

					if (jsonElement != null) {
						JSONObject address = null;
						// JSONArray schedule=null;
						json = jsonElement.html();

						if (json.startsWith("[") && json.endsWith("]")) {
							json = json.substring(1, json.length() - 1);
						}
						
						JSONObject root = new JSONObject(json);

						// fetch agency address
						address = root.getJSONObject("address");
						agencyAddress = address.getString("streetAddress") + " " + address.getString("postalCode") + " "
								+ address.getString("addressLocality");
						agencyAddress = this.normalize(agencyAddress);
						System.out.println("l'adresse de l'agence est : " + agencyAddress);

						// fecth agency name
						agencyName = (String) root.get("name") + " (" + address.getString("postalCode") + ")";
						agencyName = this.normalize(agencyName);
						System.out.println("le nom de l'agence est : " + agencyName);

						// fetch agency phone
						agencyPhone = root.getString("telephone");
						System.out.println("Tel : " + agencyPhone);

						// fetch agency schedule

						Elements openingHoursElement = doc.select("#content > div > div.mod.flex-section > "
								+ "div > div.col-md-8 > div.mod.mod-schedule > div.col-bg-container.visible-xs.visible-sm > div > div.opening-hours.emTextSchedules > table > tbody > tr");
						Elements appointementHoursElement = doc.select("#content > div > div.mod.flex-section > "
								+ "div > div.col-md-8 > div.mod.mod-schedule > div.col-bg-container.visible-xs.visible-sm > div > div.appointment-hours.emTextSchedules > table > tbody > tr");
						String shedules[] = new String[openingHoursElement.size()];
						String str = "";
						for (int i = 0; i < openingHoursElement.size(); i++) {
							Element day = openingHoursElement.get(i).selectFirst("th");
							Element hour = openingHoursElement.get(i).selectFirst("td");
							shedules[i] = day.text() + " " + hour.text();
						}

						for (int i = 0; i < appointementHoursElement.size(); i++) {
							Element hour = appointementHoursElement.get(i).selectFirst("td");
							agencySchedule += shedules[i] + " " + hour.text() + "RDV" + " ";
						}

						System.out.println("agencySchedule : "+agencySchedule);
						System.out.println("------------------------------\n");

					}
					this.setData(LienAgence, String.valueOf(NbAgences), agencyName, agencyAddress, agencyPhone,
							agencySchedule, agencyHtm);
				}

				log.writeLog(new Date(), url, "no_error", NbAgences, parcoursficval, 0);
			}
			parcoursficval++;

		}
		sc.close();

	}

	public void tearDown() {
		htmlUnitDriver.quit();

	}

}
