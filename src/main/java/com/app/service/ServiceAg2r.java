package com.app.service;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.util.Date;
import java.util.Scanner;

import javax.net.ssl.HttpsURLConnection;

import org.apache.commons.text.StringEscapeUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import org.openqa.selenium.htmlunit.HtmlUnitDriver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.app.model.AG2R;

import com.app.scrap.RepAg2r;

import com.app.utils.Parametres;

import fa.browserUtils.FactoryBrowser;
import fa.browserUtils.HtmlUnitBrowser;
import fa.log.LogApp.LogScrap;
import fa.log.config.LogConst;

@Service
public class ServiceAg2r extends Scraper {

	@Autowired
	private RepAg2r rep;

	String nomSITE = "AG2R";
	String ficval = "AG2R_LiensAgences";
	String dossierRESU = Parametres.REPERTOIRE_RESULT + "\\ASSU\\" + nomSITE;;;
	String fichier_valeurs;
	String LienAgence;

	int NbAgences;

	int nbRecuperes;
	int num_ligneficval;
	int parcoursficval;

	HtmlUnitDriver htmlUnitDriver;

	@Override
	public void saveItem(String... args) {
		AG2R ag2r = new AG2R();
		ag2r.setNomAgence(args[0]).setAdresse(args[1]).setTel(args[2]).setHoraires(args[3]).setNbAgences(args[4])
				.setLienAgence(args[5]).setAgenceHTM(args[6]);
		rep.save(ag2r);

	}

	/*
	 * public AG2R setData(String nomAgence, String tel, String horaires, String
	 * nbAgences, String lienAgence, String adresse, String agenceHTM) {
	 * 
	 * AG2R ag2r = new AG2R();
	 * ag2r.setNomAgence(nomAgence).setAdresse(adresse).setTel(tel).setHoraires(
	 * horaires).setNbAgences(nbAgences)
	 * .setLienAgence(lienAgence).setAgenceHTM(agenceHTM); this.saveData(ag2r);
	 * return ag2r; }
	 */

	public void setUp() throws Exception {
		FactoryBrowser factBrowser = new FactoryBrowser();
		HtmlUnitBrowser hB = (HtmlUnitBrowser) factBrowser.create("htmlUnit");
		htmlUnitDriver = hB.setOptions(false, false, false, 60000).initBrowserDriver();

	}

	public void scrap() throws Exception {

		fichier_valeurs = Parametres.REPERTOIRE_DATA_SOURCES + ficval + ".txt";
		Scanner sc = new Scanner(new File(fichier_valeurs));

		LogScrap log = new LogScrap("AssuranceAg2r.txt", LogConst.TYPE_LOG_FILE);
		num_ligneficval = (new File(LogConst.STORE_LOG_PATH + LogConst.PREFIX_LOG_FILE + "_AssuranceAg2r.txt")).exists()
				? Integer.parseInt(log.readLog().getLastLineProcessed().split("\\t")[4])
				: 1;

		parcoursficval = 1;

		while (sc.hasNextLine()) {

			String[] lines = sc.nextLine().split("\\t");
			if (parcoursficval > num_ligneficval) {
				URL url = new URL(lines[1]);
				HttpsURLConnection con = (HttpsURLConnection) url.openConnection();
				con.setRequestMethod("GET");
				con.setConnectTimeout(600000);
				con.setReadTimeout(60000);
				BufferedReader in = null;
				try {
					in = new BufferedReader(new InputStreamReader(con.getInputStream(), "UTF-8"));
					
					String pageSource;
					StringBuilder strB = new StringBuilder();
					while ((pageSource = in.readLine()) != null) {
						strB.append(pageSource);
					}
					pageSource = StringEscapeUtils.unescapeHtml4(strB.toString());
					Document doc = Jsoup.parse(pageSource);

					System.out.println("url : " + url);
					
					Element agencyInfoBlock = doc.select("div[class^=\"full-size-content\"]").first();
					
					if (agencyInfoBlock != null) {

						String agencyHTM = "";
						String agencyName = "";
						String agencyTel = "";
						String agencyAddress = "";
						String agencySchedule = "";
						// agencyHTM
						agencyHTM = agencyInfoBlock.outerHtml().replaceAll("\\s{1,}", "");
						agencyHTM = this.normalize(agencyHTM);
						nbRecuperes++;
						// fetch agency Name
						Element agencyHtmlName = agencyInfoBlock.select(
								"section[class=\"container blocbase\"]>div[class=\"bloc-bandeau agence\"]>div[class=\"content\"] >div[class=\"titre\"]>h1")
								.first();
						agencyName = (agencyHtmlName != null) ? this.normalize(agencyHtmlName.text().toUpperCase()) : "";
						System.out.println("le nom de l'adresse est : " + agencyName);

						Element agencyCoordinateHtmlElment = agencyInfoBlock
								.select("section[class=\"bloc-agence blocbase\"]").first();
						if (agencyCoordinateHtmlElment != null) {
							Elements contents = agencyCoordinateHtmlElment.select("div[class=\"description\"]");
							for (int i = 0; i < contents.size(); i++) {

								if (i == 0) {

									// fetch agency phone
									Element phoneHtmlElement = contents.get(i).select("a[class=\"btbleu btmobile\"]")
											.first();
									agencyTel = (phoneHtmlElement != null)
											? phoneHtmlElement.attr("href").replaceAll("tel:", "")
											: "";

									System.out.println("le téléphone de l'adresse est : " + agencyTel);

									// fetch agency address
									Element agencyAddressElement = contents.get(i).select("div[class=\"descriptif\"]")
											.first();
									agencyAddress = (agencyAddressElement != null)
											? agencyAddressElement.text().replaceAll("Adresse|Tél\\s{1,}:|" + agencyTel,
													" ")
											: "";
									agencyAddress = this.normalize(agencyAddress);
									System.out.println("l'adresse est : " + agencyAddress);

								}
								if (i == 1) {

									Element scheduleHtmlElement = contents.get(i).select("div[class=\"horaires\"]").first();
									agencySchedule = (scheduleHtmlElement != null) ? this.normalize(scheduleHtmlElement.text()) : " ";
									System.out.println("l'horaire de l'agence est : " + agencySchedule);
								}
							}

							
							  this.saveItem(agencyName, agencyTel, agencySchedule,
							  String.valueOf(NbAgences), LienAgence, agencyAddress, agencyHTM);
							 
							System.out.println("-------------------------------------------\n");
							
						}
						

					}
					
				}catch (IOException e) {
					// TODO Auto-generated catch block
					//e.printStackTrace();
					System.out.println(url+"\tPage not found\n");
				}
				
				

				// write log
				log.writeLog(new Date(), LienAgence, "no_error", NbAgences, parcoursficval,
				(NbAgences - nbRecuperes));
			}

			parcoursficval++;

		}
		sc.close();

	}

}
