package com.app.service;

import java.io.BufferedWriter;
import java.io.File;

import java.io.FileWriter;
import java.io.IOException;

import java.net.SocketTimeoutException;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Scanner;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.app.model.AXA;
import com.app.scrap.RepAXA;

import com.app.utils.Parametres;
import com.app.utils.Utilitaires;

import fa.browserUtils.FactoryBrowser;
import fa.browserUtils.HtmlUnitBrowser;
import fa.log.LogApp.LogScrap;
import fa.log.config.LogConst;

@Service
public class ServiceAxa extends Scraper {

	@Autowired
	private RepAXA rep;

	private List<com.app.model.AXA> axaList = new ArrayList<>();

	private String nomSITE = "AXA";
	private String ficval = "line_ca";

	private int nbRecuperes;
	private int num_ligneficval;
	private int parcoursficval;
	private int NbVilles;
	private int NbAgences_Ville;

	private HtmlUnitDriver wd;

	public void setUp() throws Exception {
		wd = (HtmlUnitDriver) this.setupHU();
	}
	
	public void show() {
		//rep.getLastRow(100,AXA.class);
		List<AXA> res = rep.findAll();
		for(AXA axa : res) {
			System.out.println(axa.getId()+"\t"+axa.getNomAgence()+"\t"+axa.getAdresse());
		}
		
	}
	
	public void show2() {
		//rep.getLastRow(100,AXA.class);
		List<AXA> res = rep.getFirstRow(1000,AXA.class);
		for(AXA axa : res) {
			System.out.println(axa.getId()+"\t"+axa.getNomAgence()+"\t"+axa.getAdresse());
		}
		
		List<AXA> res2 = rep.getLastRow(10,AXA.class);
		for(AXA axa : res2) {
			System.out.println(axa.getId()+"\t"+axa.getNomAgence()+"\t"+axa.getAdresse());
		}
		
	}
	

	@Override
	public void saveItem(String... args) {
		AXA axa = new AXA();
		axa.setDepSoumis(args[0]).setNbVilles(args[1]).setVilleSoumise(args[2]).setNbAgenceVille(args[3])
				.setNomAgent(args[4]).setNomAgence(args[5]).setAdresse(args[6]).setTel(args[7]).setOrias(args[8])
				.setHoraire(args[9]).setAgenceHTM(args[10]);
		rep.save(axa);
	}

	@Override
	public void scrap() throws IOException {
		String dataSourceFilePathStr = Parametres.REPERTOIRE_DATA_SOURCES + ficval + ".txt";

		Scanner sc = new Scanner(new File(dataSourceFilePathStr));

		LogScrap log = new LogScrap("AssuranceAxaLog.txt", LogConst.TYPE_LOG_FILE);
		if ((new File(LogConst.STORE_LOG_PATH + LogConst.PREFIX_LOG_FILE + "_AssuranceAxaLog.txt")).exists()) {

			num_ligneficval = Integer.parseInt(log.readLog().getLastLineProcessed().split("\t")[4]);

		} else {
			num_ligneficval = 1;
		}

		parcoursficval = 1;

		String pageSource = "";
		Document doc = null;
		while (sc.hasNextLine()) {
			String line[] = sc.nextLine().split("\\t");

			if (parcoursficval >= num_ligneficval) {
				String depSoumis = line[0];
				String url = line[1];

				System.out.println("url principale : "+url);
				try {
					wd.get(url);
				} catch (Exception e) {

					if (e instanceof SocketTimeoutException) {
						wd.get(url);
					}
				}
				pageSource = wd.getPageSource();
				doc = Jsoup.parse(pageSource);

				// retrieve number of agency per city
				// #liste_villes > ul > li:nth-child(1) > a
				Element agencyInfoRootJsoupElement = doc.select("#MainContainer").first();
				if (agencyInfoRootJsoupElement != null) {
					Elements listConseillerJsoupElement = agencyInfoRootJsoupElement.select("#liste_villes > ul > li");
					NbVilles = listConseillerJsoupElement.size();
					for (Element elementTmp : listConseillerJsoupElement) {

						url = elementTmp.selectFirst("a").attr("href");
						System.out.println("url axa : "+url);
						try {
							wd.get(url);
						} catch (Exception e) {

							if (e instanceof SocketTimeoutException) {
								wd.get(url);
							}
						}
						pageSource = wd.getPageSource();
						doc = Jsoup.parse(pageSource);

						Elements agencyLinksElements = doc.select(
								"#BoxListConseillers > div.ListConseiller > ul > li > div > div.conseiller__main-infos > a");
						NbAgences_Ville = agencyLinksElements.size();
						for (Element agencyLinkElement : agencyLinksElements) {

							String nomAgence = "";
							String adresse = "";
							String tel = "";
							String horaires = "";
							String orias = "";
							String nomAgents = "";
							url = agencyLinkElement.selectFirst("a").attr("href");
							System.out.println(url);
							try {
								wd.get(url);
							} catch (Exception e) {

								if (e instanceof SocketTimeoutException) {
									wd.get(url);
								}
							}
							pageSource = wd.getPageSource();
							doc = Jsoup.parse(pageSource);
							Element agencyCoordinateElement = doc.selectFirst("#header-fiche");
							if (agencyCoordinateElement != null) {

								Element agencyNameElement = agencyCoordinateElement
										.selectFirst("#header-titre-accueil > div > div > h1");
								nomAgence = (agencyNameElement != null) ? this.normalize(agencyNameElement.text()) : "";
								System.out.println("nom : " + nomAgence);

								Element adresseElement = agencyCoordinateElement
										.selectFirst("div > div.section-content.section-sticky > div > div > div > div.fiche-content.fiche-content-after-picture > div.agent-details-item.agent-adresse.notselectable > div.item-content.adresse");
								adresse = (adresseElement != null) ? this.normalize(adresseElement.text()) : "";
								System.out.println("adresse : " + adresse);

								Element telElement = agencyCoordinateElement
										.selectFirst("div > div.section-content.section-sticky > div > div > div > "
												+ "div.fiche-content.fiche-content-after-picture > div.agent-details-item.agent-telephone.notselectable > div.item-content.NumTel > a");
								tel = (telElement != null) ? telElement.attr("href").replaceAll("[^0-9 ]", "") : "";
								System.out.println("tel : " + tel);

							}
							Elements agencyScheduleElements = doc.select("#horaires > div > div > div > ul>li");
							if (!agencyScheduleElements.isEmpty()) {
								for (Element agencyScheduleElement : agencyScheduleElements) {
									horaires += agencyScheduleElement.text() + " ";
								}
							}
							horaires = this.normalize(horaires);
							System.out.println("horaires : " + horaires);
							
							Elements agentsInfoElements = doc.select("#informations-agent > div > div > div > div.section-inner > div.mobile-wrapper.content-informations-agent > div > div");
							if (!agentsInfoElements.isEmpty()) {
								for (Element agentsInfoElement : agentsInfoElements) {
									Element agentNameElement = agentsInfoElement.selectFirst("span.nom");
									nomAgents += agentNameElement.text() + " & ";

									Element oriasElement = agentsInfoElement.selectFirst("span.details-orias");
									orias += oriasElement.text().replaceAll("[^0-9]","") + " & ";
								}
							} /*
								 * else { Element otherNameAgents = doc.
								 * selectFirst("#informations-agent > div > div > div > div.section-title-container > p > span.title.notselectable > span.nom"
								 * ); nomAgents += otherNameAgents != null ? otherNameAgents.text() : "";
								 * 
								 * }
								 */
							System.out.println("Nom : "+nomAgents + "  \nOrias : " + orias);
							System.out.println("-------------------------\n");
							this.saveItem(depSoumis, String.valueOf(NbVilles), url, String.valueOf(NbAgences_Ville), nomAgents,
									nomAgence, adresse, tel, orias, horaires, pageSource.replaceAll("\\s{1,}", ""));
						}

					}

				}

				log.writeLog(new Date(), depSoumis, "no_ERROR", NbAgences_Ville, parcoursficval, 0);
			}
			parcoursficval++;
		}
		sc.close();

	}

	public void tearDown() {
		try {
			wd.quit();
		} catch (Exception e) {

		}

	}

}
