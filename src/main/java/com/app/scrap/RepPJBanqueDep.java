package com.app.scrap;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.app.model.PJAssuranceCP;
import com.app.model.PJBanque;
import com.app.model.PJBanqueDep;

@Repository
public interface RepPJBanqueDep extends JpaRepository<PJBanqueDep, Integer> {

}
