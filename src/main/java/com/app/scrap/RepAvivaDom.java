package com.app.scrap;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.app.model.Aviva;
import com.app.model.Aviva_DOM;

@Repository
public interface RepAvivaDom extends JpaRepository<Aviva_DOM,Integer>, RepositoryCustom<Aviva_DOM> {

}
