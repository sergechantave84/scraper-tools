package com.app.model;

import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="banque_bcp", schema="scrap")
public class BCP {
	
	
	@Id
	@SequenceGenerator(name="scrap.banque_bcp_id_seq",
	                     sequenceName ="scrap.banque_bcp_id_seq",
	                     allocationSize=1)
    @GeneratedValue( strategy=GenerationType.SEQUENCE,
                     generator="scrap.banque_bcp_id_seq")
	  
	@Column(name="id",updatable=false)
	private int id;
	
	@Column(name="valeursoumise", columnDefinition="varchar")
	private String valeurSoumise;
	
	@Column(name="nbagences", columnDefinition="varchar")
	private String nbAgences;
	
	@Column(name="nbidtrouves", columnDefinition="varchar")
	private String nbIdTrouves;
	
	@Column(name="numagence", columnDefinition="varchar")
	private String numAgence;
	
	@Column(name="nomweb", columnDefinition="varchar")
	private String nomWeb;
	
	@Column(name="adresse", columnDefinition="varchar")
	private String adresse;
	
	@Column(name="tel", columnDefinition="varchar")
	private String tel;
	
	@Column(name="horaires_htm", columnDefinition="varchar")
	private String horairesHtm;
	
	@Column(name="horaires_txt", columnDefinition="varchar")
	private String horairesTxt;
	
	@Column(name="agence_htm", columnDefinition="varchar")
	private String agenceHtm;
	
	@Column(name="annee_du_scrap" ,columnDefinition="varchar(4) DEFAULT date_part('year',CURRENT_DATE)" )
	private String anneeScrap;
	
	public BCP() {
		LocalDate ld= new Date().toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
		anneeScrap=String.valueOf(ld.getYear()+1);
	}
	
	public int getId() {
		return id;
	}
	public BCP setId(int id) {
		this.id = id;
		return this;
	}
	public String getValeurSoumise() {
		return valeurSoumise;
	}
	public BCP setValeurSoumise(String valeurSoumise) {
		this.valeurSoumise = valeurSoumise;
		return this;
	}
	public String getNbAgences() {
		return nbAgences;
	}
	public BCP setNbAgences(String nbAgences) {
		this.nbAgences = nbAgences;
		return this;
	}
	public String getNbIdTrouves() {
		return nbIdTrouves;
	}
	public BCP setNbIdTrouves(String nbIdTrouves) {
		this.nbIdTrouves = nbIdTrouves;
		return this;
	}
	public String getNumAgence() {
		return numAgence;
	}
	public BCP setNumAgence(String numAgence) {
		this.numAgence = numAgence;
		return this;
	}
	public String getNomWeb() {
		return nomWeb;
	}
	public BCP setNomWeb(String nomWeb) {
		this.nomWeb = nomWeb;
		return this;
	}
	public String getAdresse() {
		return adresse;
	}
	public BCP setAdresse(String adresse) {
		this.adresse = adresse;
		return this;
	}
	public String getTel() {
		return tel;
	}
	public BCP setTel(String tel) {
		this.tel = tel;
		return this;
	}
	public String getHorairesHtm() {
		return horairesHtm;
	}
	public BCP setHorairesHtm(String horairesHtm) {
		this.horairesHtm = horairesHtm;
		return this;
	}
	public String getHorairesTxt() {
		return horairesTxt;
	}
	public BCP setHorairesTxt(String horairesTxt) {
		this.horairesTxt = horairesTxt;
		return this;
	}
	public String getAgenceHtm() {
		return agenceHtm;
	}
	public BCP setAgenceHtm(String agenceHtm) {
		this.agenceHtm = agenceHtm;
		return this;
	}
	public String getAnneeScrap() {
		return anneeScrap;
	}
	public BCP setAnneeScrap(String anneeScrap) {
		this.anneeScrap = anneeScrap;
		return this;
	}

}
