package com.app.scrap;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.app.model.Mutualia;
import com.app.model.Mutualia_DOM;

@Repository
public interface RepMutualliaDom extends JpaRepository<Mutualia_DOM,Integer>,RepositoryCustom<Mutualia_DOM>{

}
