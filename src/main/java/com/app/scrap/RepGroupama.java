package com.app.scrap;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.app.model.Groupama;

@Repository
public interface RepGroupama extends JpaRepository<Groupama,Integer>,RepositoryCustom<Groupama>  {

}
