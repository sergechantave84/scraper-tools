package com.app.scrap;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.app.model.CA;

@Repository
public interface RepCA extends JpaRepository<CA,Integer>,RepositoryCustom<CA>{

}
