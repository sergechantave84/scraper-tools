package com.app.service;

import java.io.BufferedInputStream;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;

import java.net.SocketTimeoutException;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Scanner;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.PageLoadStrategy;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.app.model.BCP;
import com.app.scrap.RepBcp;

import com.app.utils.Parametres;
import com.app.utils.Utilitaires;

import fa.browserUtils.ChromeBrowser;
import fa.browserUtils.FactoryBrowser;
import fa.log.LogApp.LogScrap;
import fa.log.config.LogConst;

@Service
public class ServiceBCP extends Utilitaires {

	@Autowired
	private RepBcp rep;

	private static String driverPath = "C:\\ChromeDriver\\";

	private List<BCP> bcpList = new ArrayList<BCP>();
	private String nomSITE = "BCP";
	private String ficval = "departements_sans20";
	private String dossierRESU = Parametres.REPERTOIRE_RESULT + "\\BQ\\" + nomSITE;;
	//private String url_accueil = "https://fc1.1bis.com/bcp-v2/region.asp";
	private String url_accueil = "https://fc1.1bis.com/bcp-v2/region.asp?dept=";

	private static ChromeDriver chromeDriver;
	private String ValeurSoumise;

	private int num_ligneficval;
	private int parcoursficval;
	private int nbAgences;

	private int nbID;

	private BufferedWriter sortie;

	public BCP saveData(BCP t) {

		return rep.save((BCP) t);

	}

	public List<BCP> saveDataAll(List<BCP> t) {
		return rep.saveAll(t);
	}

	public List<BCP> getElmnt() {

		return (List<BCP>) rep.findAll();
	}

	public BCP getById(int id) {

		return (BCP) rep.findById(id).orElse(null);
	}

	public String deleteProduct(int id) {
		try {
			rep.deleteById(id);

		} catch (Exception e) {
			e.printStackTrace();
		}

		return "removed";
	}

	public BCP updateProduct(BCP t) {

		return null;
	}

	public BCP setData(String valeurSoumise, String nbAgences, String nbIdBCProuves, String numAgence, String nomWeb,
			String adresse, String tel, /*String email,*/ String horairesHtm, String horairesBCPxt, String agenceHtm) {
		BCP bcp = new BCP();
		bcp.setValeurSoumise(valeurSoumise).setNbAgences(nbAgences).setNbIdTrouves(nbIdBCProuves)
				.setNumAgence(numAgence).setNomWeb(nomWeb).setAdresse(adresse).setTel(tel)/*.setEmail(email)*/.setHorairesHtm(horairesHtm)
				.setHorairesTxt(horairesBCPxt).setAgenceHtm(agenceHtm);
		this.saveData(bcp);
		return bcp;
	}

	public List<BCP> showFirstRows() {
		List<BCP> a = rep.getFirstRow(100, BCP.class);
		return a;
	}

	public List<BCP> showLastRows() {
		List<BCP> a = rep.getLastRow(100, BCP.class);
		return a;
	}

	public void setup() {
		FactoryBrowser factBrowser = new FactoryBrowser();
		ChromeBrowser b = (ChromeBrowser) factBrowser.create("chrome");
		/*chromeDriver = b.setOptions(PageLoadStrategy.NORMAL, driverPath, 60000, 60000, 60000, true, false)
				.initBrowserDriver();*/
		final String binaryPath = "C:\\Program Files\\Google\\Chrome Beta\\Application\\chrome.exe";
		
		chromeDriver = b.setOptions(PageLoadStrategy.NORMAL, driverPath, 60000, 60000, 60000, false, false, binaryPath)
				.initBrowserDriver();
		
	}

	public List<BCP> scrapBCP() throws IOException {
		String pageSource = "", jsScript = "return document.readyState";
		boolean documentReadyState = false;
		Document doc = null;
		JavascriptExecutor jsExecutor = (JavascriptExecutor) chromeDriver;

		Scanner sc = new Scanner(new BufferedInputStream(
				new FileInputStream(new File(Parametres.REPERTOIRE_DATA_SOURCES + ficval + ".txt"))));

		if (!new File(dossierRESU).exists()) {
			new File(dossierRESU).mkdirs();
		}
		fichier = new File(dossierRESU + "\\" + "RESULTATS_" + nomSITE + "_" + System.currentTimeMillis() + ".txt");

		LogScrap log = new LogScrap("BanqueBCPLog.txt", LogConst.TYPE_LOG_FILE);
		if ((new File(LogConst.STORE_LOG_PATH + LogConst.PREFIX_LOG_FILE + "_BanqueBCPLog.txt")).exists()) {

			num_ligneficval = Integer.parseInt(log.readLog().getLastLineProcessed().split("\t")[4]);

		} else {
			num_ligneficval = 1;
		}

		parcoursficval = 0;

		while (sc.hasNextLine()) {
			jsScript = "return document.readyState";
			String line = sc.nextLine();
			ValeurSoumise = line.split("\\t")[0];
			if (parcoursficval >= num_ligneficval) {

				String agencyName = "", agencyAddress = "", agencyPhone = "", agencyEmail = "", agencySchedule = "",
						agencyScheduleHTML = "", agencyHtml = "";
				
				System.out.println("url : " + url_accueil+ValeurSoumise);

				try {
					//chromeDriver.get(url_accueil);
					chromeDriver.get(url_accueil+ValeurSoumise);

				} catch (Exception e) {
					if (e instanceof SocketTimeoutException) {
						chromeDriver.get(url_accueil+ValeurSoumise);

					}
				}
				try {

					documentReadyState = this.listensIfPageFullyLoaded(chromeDriver);

					if (documentReadyState) {
						Thread.sleep(2000);
						System.out.println(documentReadyState);
					
							pageSource = chromeDriver.getPageSource();
							doc = Jsoup.parse(pageSource);
							String textIfNoresponse = doc.outerHtml();
							Element noResult = doc.selectFirst("#divNoResult > div.dlg-message");
							
							if (noResult != null) {
								System.out.println("aucune reponse");

								nbID = 0;
								nbAgences = 0;
								this.enregistrer(agencyName, agencyAddress, agencyPhone, agencyScheduleHTML,
										agencySchedule, agencyHtml, 0);

							}else {
								//Element rootJElement = doc.select("#agencies>div>div").first();
								Element rootJElement = doc.select("ul#agencies").first();
								Elements agenciesListJElements = rootJElement.select("li.agency.agency-type-bcp");
								nbID = agenciesListJElements.size();
								nbAgences = agenciesListJElements.size();
								System.out.println("nbAgences : " + nbAgences);
								int responseNumber = 0;
								for (Element elmnt : agenciesListJElements) {
									agencyScheduleHTML = "";
									agencySchedule = "";
									agencyHtml = elmnt.outerHtml().trim().replaceAll("\\s{1,}", "");
									agencyHtml = this.normalize(agencyHtml);

									Element primaryInfoJElement = elmnt.select(".infoprimary").first();
									Element secondaryInfoJElement = elmnt.select(".infosecondary").first();

									// retrieve agency name
									Element agencyNameJElement = primaryInfoJElement.select("h2").first();
									agencyName = (agencyNameJElement != null) ? agencyNameJElement.text() : "";
									agencyName = this.normalize(agencyName).toUpperCase();
									
									System.out.println("agencyName : " + agencyName);

									// retrieve agency address
									Element agencyAddressJElement = primaryInfoJElement.select("div.agencyaddress")
											.first();
									agencyAddress = (agencyAddressJElement != null)
											? agencyAddressJElement.text().replaceAll("\\s{1,}", " ")
											: "";
									
									agencyAddress = this.normalize(agencyAddress).toUpperCase();
									
									System.out.println("agencyAddress : " + agencyAddress);

									// retrieve agency tel
									Element agencyPhoneJElement = secondaryInfoJElement.select(".agencytel").first();
									agencyPhone = (agencyPhoneJElement != null)
											? agencyPhoneJElement.text().replaceAll("[^0-9]", "")
											: "";
									agencyPhone = this.normalize(agencyPhone);
									
									System.out.println("agencyPhone : " + agencyPhone);
									
									// ADD By NANTE
									Element agencyEmailJElement = secondaryInfoJElement.select(".agencyemail > a > span.emailTxt").first();
									agencyEmail = (agencyEmailJElement != null)
											? agencyEmailJElement.text() : "";
									
									agencyEmail = this.normalize(agencyEmail);
									
									System.out.println("agencyEmail : " + agencyEmail);
									// END NANTE

									// retrieve agency schedule
									Elements agencyScheduleJELements = secondaryInfoJElement
											.select(".schedule>div[class*=\"day\"]");
									for (Element elmnt1 : agencyScheduleJELements) {
										agencyScheduleHTML += elmnt1.outerHtml().trim();
										agencySchedule += elmnt1.text().trim() + " ";
									}
									agencySchedule = this.normalize(agencySchedule);
									System.out.println("agencySchedule : " + agencySchedule);
									agencyScheduleHTML = this.normalize(agencyScheduleHTML);
									agencyScheduleHTML = agencyScheduleHTML.replaceAll("\\s{1,}", "");
									responseNumber++;

									this.setData(ValeurSoumise, String.valueOf(nbAgences), String.valueOf(nbID),
											String.valueOf(responseNumber), agencyName, agencyAddress, agencyPhone,
											/*agencyEmail,*/ agencyScheduleHTML, agencySchedule, agencyHtml);
								}

							}

					} else {
						System.out.println("pouf");
					}
				} catch (Exception e) {

					e.printStackTrace();

				}
				//log.writeLog(new Date(), ValeurSoumise, "no-erroe", nbAgences, parcoursficval, nbAgences);
			}
			parcoursficval++;
		}
		sc.close();
		return bcpList;
	}

	public void enregistrer(String nomAgence, String adresse, String tel, String HorairesHTM, String HorairesTXT,
			String bloc_detail_agenceHTM, int numreponse) {

//	    	this.fichier = new File(Parametres.REPERTOIRE + "RESULTATS_BCP.txt");
		try {

			sortie = new BufferedWriter(new FileWriter(fichier, true));
			sortie.write(ValeurSoumise + "\t" + this.nbAgences + "\t" + nbID + "\t" + numreponse + "\t" + nomAgence
					+ "\t" + adresse + "\t" + tel + "\t" + HorairesHTM + "\t" + HorairesTXT + "\t"
					+ bloc_detail_agenceHTM + "\r\n");

			sortie.close();
		} catch (IOException e) {

			e.printStackTrace();
		}

	}

	/**
	 * La m�thode <code>initialiser()</code> permet d'ajouter la ligne d'ent�te dans
	 * le fichier r�sultat.
	 */

	public void initialiser() {

		try {
			// Attention pas d'accent dans les noms de colonnes !
			sortie = new BufferedWriter(new FileWriter(fichier, true));
			sortie.write("ValeurSoumise" + "\t" + "NbAgences" + "\t" + "nbIDTrouves" + "\t" + "numAgence" + "\t"
					+ "NOMWEB" + "\t" + "ADRESSE" + "\t" + "TEL" + "\t" + "horaires_HTM" + "\t" + "horaires_TXT" + "\t"
					+ "agence_HTM" + "\r\n");
			sortie.close();

		} catch (IOException e) {

			e.printStackTrace();
		}
	}

	public void tearDown() {
		chromeDriver.quit();

	}
}
