package com.app.model;

import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "banque_ca_dep_outre_mer", schema = "scrap")
public class CA_Outre_Mer {

	@Id
	@SequenceGenerator(name = "scrap.banque_ca_dep_outre_mer_id_seq", sequenceName = "scrap.banque_ca_dep_outre_mer_id_seq", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "scrap.banque_ca_dep_outre_mer_id_seq")

	@Column(name = "id", updatable = false)
	private int id;

	@Column(name = "lien_enseigne", columnDefinition = "varchar")
	private String lienEnseigne;

	@Column(name = "nbvilles", columnDefinition = "varchar")
	private String nbVilles;

	@Column(name = "numville", columnDefinition = "varchar")
	private String numVilles;

	@Column(name = "villesoumise", columnDefinition = "varchar")
	private String villeSoumise;

	@Column(name = "nbagences", columnDefinition = "varchar")
	private String nbAgences;

	@Column(name = "lien_agence", columnDefinition = "varchar")
	private String lienAgence;

	@Column(name = "nomweb", columnDefinition = "varchar")
	private String nomWeb;

	@Column(name = "adresse", columnDefinition = "varchar")
	private String adresse;

	@Column(name = "tel", columnDefinition = "varchar")
	private String tel;

	@Column(name = "fax", columnDefinition = "varchar")
	private String fax;

	@Column(name = "horaires_htm", columnDefinition = "varchar")
	private String horairesHtm;

	@Column(name = "horaires_txt", columnDefinition = "varchar")
	private String horairesTxt;

	@Column(name = "services", columnDefinition = "varchar")
	private String services;

	@Column(name = "coordonnees", columnDefinition = "varchar")
	private String coordonnees;

	@Column(name = "agencehtm", columnDefinition = "varchar")
	private String agenceHtm;

	@Column(name = "agencehtm_bas", columnDefinition = "varchar")
	private String agenceHtmBas;

	@Column(name = "annee_du_scrap", columnDefinition = "varchar(4) DEFAULT date_part('year',CURRENT_DATE)")
	private String anneeScrap;

	public CA_Outre_Mer() {
		LocalDate ld = new Date().toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
		anneeScrap = String.valueOf(ld.getYear() + 1);
	}

	public int getId() {
		return id;
	}

	public CA_Outre_Mer setId(int id) {
		this.id = id;
		return this;
	}

	public String getLienEnseigne() {
		return lienEnseigne;
	}

	public CA_Outre_Mer setLienEnseigne(String lienEnseigne) {
		this.lienEnseigne = lienEnseigne;
		return this;
	}

	public String getNbVilles() {
		return nbVilles;
	}

	public CA_Outre_Mer setNbVilles(String nbVilles) {
		this.nbVilles = nbVilles;
		return this;
	}

	public String getNumVilles() {
		return numVilles;
	}

	public CA_Outre_Mer setNumVilles(String numVilles) {
		this.numVilles = numVilles;
		return this;
	}

	public String getVilleSoumise() {
		return villeSoumise;
	}

	public CA_Outre_Mer setVilleSoumise(String villeSoumise) {
		this.villeSoumise = villeSoumise;
		return this;
	}

	public String getNbAgences() {
		return nbAgences;
	}

	public CA_Outre_Mer setNbAgences(String nbAgences) {
		this.nbAgences = nbAgences;
		return this;
	}

	public String getLienAgence() {
		return lienAgence;
	}

	public CA_Outre_Mer setLienAgence(String lienAgence) {
		this.lienAgence = lienAgence;
		return this;
	}

	public String getNomWeb() {
		return nomWeb;
	}

	public CA_Outre_Mer setNomWeb(String nomWeb) {
		this.nomWeb = nomWeb;
		return this;
	}

	public String getAdresse() {
		return adresse;
	}

	public CA_Outre_Mer setAdresse(String adresse) {
		this.adresse = adresse;
		return this;
	}

	public String getTel() {
		return tel;
	}

	public CA_Outre_Mer setTel(String tel) {
		this.tel = tel;
		return this;
	}

	public String getFax() {
		return fax;
	}

	public CA_Outre_Mer setFax(String fax) {
		this.fax = fax;
		return this;
	}

	public String getHorairesHtm() {
		return horairesHtm;
	}

	public CA_Outre_Mer setHorairesHtm(String horairesHtm) {
		this.horairesHtm = horairesHtm;
		return this;
	}

	public String getHorairesTxt() {
		return horairesTxt;
	}

	public CA_Outre_Mer setHorairesTxt(String horairesTxt) {
		this.horairesTxt = horairesTxt;
		return this;
	}

	public String getServices() {
		return services;
	}

	public CA_Outre_Mer setServices(String services) {
		this.services = services;
		return this;
	}

	public String getCoordonnees() {
		return coordonnees;
	}

	public CA_Outre_Mer setCoordonnees(String coordonnees) {
		this.coordonnees = coordonnees;
		return this;
	}

	public String getAgenceHtm() {
		return agenceHtm;
	}

	public CA_Outre_Mer setAgenceHtm(String agenceHtm) {
		this.agenceHtm = agenceHtm;
		return this;
	}

	public String getAgenceHtmBas() {
		return agenceHtmBas;
	}

	public CA_Outre_Mer setAgenceHtmBas(String agenceHtmBas) {
		this.agenceHtmBas = agenceHtmBas;
		return this;
	}

	public String getAnneeScrap() {
		return anneeScrap;
	}

	public CA_Outre_Mer setAnneeScrap(String anneeScrap) {
		this.anneeScrap = anneeScrap;
		return this;
	}

}
