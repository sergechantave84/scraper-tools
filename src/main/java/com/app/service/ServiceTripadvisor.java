package com.app.service;

import java.io.BufferedInputStream;

import java.io.File;
import java.io.FileInputStream;

import java.io.IOException;
import java.text.Normalizer;

import java.util.Date;

import java.util.List;
import java.util.Scanner;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.app.model.Tripadvisor;
import com.app.scrap.RepTripadvisor;

import com.app.utils.Parametres;

import fa.log.LogApp.LogScrap;
import fa.log.config.LogConst;

@Service
public class ServiceTripadvisor extends Scraper {

	@Autowired
	RepTripadvisor rep;

	private ChromeDriver ch = null;
	private String nomSITE = "tripadvisor";
	private String ficval = "resto.txt"; // "adresse_rest_2022.txt";// adresse_rest_2022 _test.txt
	private String dossierRESU = Parametres.REPERTOIRE_RESULT + "\\Restaurant\\" + nomSITE;
	private int numLigneDernierLu = 2;

	public void save(Tripadvisor trip) {
		rep.save(trip);
	}

	public void save(List<Tripadvisor> trip) {
		rep.saveAll(trip);
	}

	public Tripadvisor setData(String depSoumise, String adresse, String nom, int nombreAgence, String tel,
			String horaires, String uri, String agenceHtml, String typeCuisine, String regimeSpeciaux, String repas,
			String fonctionnalites, String fourchettePrix, String note, String classement) {
		Tripadvisor trip = new Tripadvisor();

		trip.setDepSoumis(depSoumise).setAdresse(adresse).setNom(nom).setNombreAgence(nombreAgence).setTel(tel)
				.setHoraires(horaires).setUri(uri).setAgenceHtml(agenceHtml).setTypeCuisine(typeCuisine)
				.setRegimeSpeciaux(regimeSpeciaux).setRepas(repas).setFonctionalite(fonctionnalites)
				.setFourchette_prix(fourchettePrix).setNote(note).setClassement(classement);
		return trip;
	}

	public void setup() {
		final String binaryPath = "C:\\Program Files\\Google\\Chrome Beta\\Application\\chrome.exe";
		ch = (ChromeDriver) this.setupCh(Parametres.CHROME_DRIVER_PATH, false, false, "", "", binaryPath);
	}

	public void logOut() {
		ch.close();
	}

	public void scrapTripadvisor() throws IOException, InterruptedException {

		if (!new File(dossierRESU).exists())
			new File(dossierRESU).mkdirs();

		LogScrap log = new LogScrap("tripadvisorLog.txt", LogConst.TYPE_LOG_FILE);

		if ((new File(LogConst.STORE_LOG_PATH + LogConst.PREFIX_LOG_FILE + "_tripadvisorLog.txt")).exists()) {

			numLigneDernierLu = Integer.parseInt(log.readLog().getLastLineProcessed().split("\t")[4]);

		} else {
			numLigneDernierLu = 1;
		}

		System.out.println("on reprend à la ligne" + numLigneDernierLu);
		Scanner sc = new Scanner(new BufferedInputStream(new FileInputStream(new File("C:\\datasources\\" + ficval))));
		int compteur = 1;
		String path = "";
		String uri = "";
		String depSoumise;
		String adresse;
		String nom;
		int nombreAgence;
		String tel;
		String horaires;
		String agenceHtml;
		String pageSource = "";
		String typeCuisine;
		String regimeSpeciaux;
		String repas;
		String fonctionnalites;
		String fourchettePrix;
		String note;
		String classement;
		String detailTitle;
		String detailValue;
		Element detailTitleJElement = null;
		Element detailValueJElement = null;
		Document doc = null;

		JavascriptExecutor jsExec = (JavascriptExecutor) ch;

		int i = 0;
		while (sc.hasNextLine()) {
			String[] lines = sc.nextLine().split(";");

			if (i > 0) {

				path = lines[0];
				depSoumise = lines[1];
				adresse = "";
				nom = "";
				//nombreAgence = 48636;
				//nombreAgence = 21839;
				nombreAgence = 1336;
				tel = "";
				horaires = "";
				agenceHtml = "";
				pageSource = "";
				typeCuisine = "";
				regimeSpeciaux = "";
				repas = "";
				fonctionnalites = "";
				fourchettePrix = "";
				note = "";
				classement = "";
				detailTitle = "";
				detailValue = "";
				System.out.println(uri);
				if (compteur >= numLigneDernierLu) {
					System.out.println(compteur);
					uri = path;
					System.out.println(uri);
					ch.get(uri);
					try {
						// Thread.sleep(4000);
						/*
						 * List<WebElement> showHoriares = ch.findElements( By.
						 * cssSelector("div > div > div:nth-child(3) > span.dyeJW.bcYVS > div > span.cSjwK"
						 * ));
						 */

						// if (!showHoriares.isEmpty()) {
						// System.out.println("not empty");
						String script = String.format(
								"const btnHoraires= document.querySelector(\"div[id^=component] >  div > div:nth-child(3) > span.DsyBj.YTODE > div > span.mMkhr\"); if(btnHoraires !==null) btnHoraires.click()",
								"");
						jsExec.executeScript(script);
						// }

					} catch (Exception e) {
						// e.printStackTrace();
						System.out.println(e.getMessage());
					} finally {
						System.out.println("on continue");
					}

					Thread.sleep(2000);
					WebElement body = ch.findElementByCssSelector("#BODY_BLOCK_JQUERY_REFLOW");
					pageSource = Normalizer.normalize(body.getAttribute("outerHTML"), Normalizer.Form.NFD)
							.replaceAll("[^\\p{ASCII}]", "");

					doc = Jsoup.parse(pageSource);

					Element root = doc.selectFirst("#taplc_top_info_0");

					Element nameElement = root.selectFirst("div[id^=\"component\"]> div > div > h1");
					nom = (nameElement != null) ? nameElement.text().replaceAll("[^a-zA-Z0-9 ]", " ").toUpperCase()
							: "";
					System.out.println("le nom est " + nom);

					Element adresseElement = root
							.selectFirst("div > div > div:nth-child(3) > span:nth-child(1) > span > a");
					adresse = (adresseElement != null)
							? adresseElement.text().replaceAll("[^a-zA-Z0-9 ]", " ").toUpperCase()
							: "";
					System.out.println("l'adresse est " + adresse);

					// #component_45 > div > div:nth-child(3) > span:nth-child(2) > span >
					// span.fhGHT > a
					Element telElement = root.selectFirst("div> " + "div > " + "div:nth-child(3) > "
							+ "span:nth-child(2) > " + "span > " + "span > a");
					tel = (telElement != null) ? telElement.attr("href").replaceAll("[^0-9 ]", "") : "";
					System.out.println("le num de tel est " + tel);

					// #component_46 > div:nth-child(2) > div > div:nth-child(1) > div >
					// div:nth-child(1) > div.eEwDq > span.fdsdx
					// #component_47 > div.eaPeM.MD > div > div:nth-child(1) > div >
					// div:nth-child(1) > div.eEwDq > span.fdsdx
					// #component_48 > div > div:nth-child(2) > span:nth-child(1) > a > svg
					// #component_50 > div > div:nth-child(2) > span:nth-child(1) > a > svg
					// #component_48 > div > div:nth-child(2) > span:nth-child(1) > a > svg

					Element noteJElement = doc.selectFirst(
							"div[id^=\"component\"]  > div> div:nth-child(2) > span:nth-child(1) > a > svg ");
					if (noteJElement != null) {
						note = ((noteJElement.attr("aria-label").replaceAll("sur", "/")).replaceAll("[^0-9,/]", ""))
								.replaceAll(",", ".");
					} else {
						noteJElement = doc.selectFirst(
								"div[id^=\"component\"]  > div.eaPeM.MD > div > div:nth-child(1) > div > div:nth-child(1) > div> span:nth-child(1)");
						note = (noteJElement != null) ? noteJElement.text() : "";
					}
					note = Normalizer.normalize(note, Normalizer.Form.NFD);
					note = note.replaceAll("[^\\p{ASCII}]", "");
					System.out.println("la note " + note);

					// #component_46 > div:nth-child(2) > div > div:nth-child(1) > div >
					// div:nth-child(1) > div:nth-child(3)
					// #component_48 > div > div:nth-child(2) > span.DsyBj.cNFrA
					Element classementsJElement = doc
							.selectFirst("div[id^=\"component\"]  >  div > div:nth-child(2) > span.DsyBj.cNFrA");

					classement = (classementsJElement != null) ? classementsJElement.text() : "";
					/*
					 * if (!classementsJElements.isEmpty()) { for (Element classementJElement :
					 * classementsJElements) { classement += classementJElement.text() + " "; } }
					 * else { classementsJElements = doc.select(
					 * "div[id^=\"component\"]  >div.eaPeM.MD > div > div:nth-child(1) > div > div:nth-child(1) > div.fYCpi"
					 * ); if (!classementsJElements.isEmpty()) { for (Element classementJElement :
					 * classementsJElements) { classement += classementJElement.text() + " "; } } }
					 */

					System.out.println("classement " + classement);

					// #BODY_BLOCK_JQUERY_REFLOW > div.cBtAm.Za.f.e
					// #BODY_BLOCK_JQUERY_REFLOW > div.KWdaU.Za.f.e > div > div > div:nth-child(2) >
					// div > div.zuYLj > div:nth-child(2)
					// #BODY_BLOCK_JQUERY_REFLOW > div.KWdaU.Za.f.e > div > div > div:nth-child(2) >
					// div > div.zuYLj > div:nth-child(2)
					Elements shedulesElements = doc.select(
							"#BODY_BLOCK_JQUERY_REFLOW > div.Za.f.e > div > div > div:nth-child(2) > div > div > div:nth-child(2)");
					if (!shedulesElements.isEmpty()) {
						for (Element tmp : shedulesElements) {
							horaires += tmp.text() + " ";
						}
						horaires = horaires.replaceAll("L'", "");
						System.out.println("horaires est " + horaires);
					} else {
						horaires = "";
					}

					try {

						String script = String.format(
								"const details=document.getElementsByClassName(\"_S b\"); if(details !==null){ for(let i=0; i< details.length;i++){ if(details[i].classList.length ===3) details[i].click()} }",
								"");
						jsExec.executeScript(script);

					} catch (Exception e) {
						e.printStackTrace();
					} finally {
						System.out.println("on continue");
					}

					try {
						// #BODY_BLOCK_JQUERY_REFLOW > div.VZmgo.D.X0.X1.Za
						Thread.sleep(2000);

						WebElement detailsBody = ch
								.findElement(By.cssSelector("#BODY_BLOCK_JQUERY_REFLOW > div.D.X0.X1.Za"));

						// div > div.dQSnI._Z.S2.H2._f > div > div > div.gapux > div > div:nth-child(2)
						// > div > div:nth-child(1)
						pageSource = Normalizer.normalize(detailsBody.getAttribute("outerHTML"), Normalizer.Form.NFD)
								.replaceAll("[^\\p{ASCII}]", "");
						doc = Jsoup.parse(pageSource);

						// #BODY_BLOCK_JQUERY_REFLOW > div.eDPnv.D.X0.X1.Za >
						Elements columnsJElements = doc
								.select("div > div._Z.S2.H2._f > div > div > div:nth-child(1) > div > div");
						System.out.println("size " + columnsJElements.size());
						if (columnsJElements.size() > 0) {
							for (Element columnJElement : columnsJElements) {

								// if (!listDetailsJElement.isEmpty()) {
								// for (Element detail : listDetailsJElement) {
								// div > div.dQSnI._Z.S2.H2._f > div > div > div.gapux > div > div:nth-child(2)
								// > div > div:nth-child(1) > div.dMshX.b
								Elements details = columnJElement.select("div > div ");
								if (!details.isEmpty())

									for (Element detail : details) {

										detailTitleJElement = detail.selectFirst("div:nth-child(1).b");
										detailValueJElement = detail.selectFirst("div:nth-child(2)");
										if (detailTitleJElement != null && detailValueJElement != null) {
											detailTitle = detailTitleJElement.text().toLowerCase();
											detailValue = detailValueJElement.text();

											System.out.println("detail tilte " + detailTitle);
											switch (detailTitle) {
											case "cuisines" -> {
												typeCuisine = detailValue;
											}
											case "fourchette de prix" -> {
												
												/*String[] detailsValues = detailValue.split("-");
												fourchettePrix = " ";
												int rankEuroAr = 4223;
												for (int e = 0; e < detailsValues.length; e++) {
													int priceInEuro = (Integer
															.parseInt(detailsValues[e].replaceAll("[^0-9]", "")))
															/ rankEuroAr;
													fourchettePrix += String.valueOf(priceInEuro) + "-";
												}
												fourchettePrix += "euro";
												fourchettePrix = fourchettePrix.replaceAll("-e", " e");*/
												 
												fourchettePrix = detailValue + " euro";
											}
											case "repas" -> {
												repas = detailValue;
											}
											case "fonctionnalites" -> {
												fonctionnalites = detailValue;
											}

											case "regimes speciaux" -> {
												regimeSpeciaux = detailValue;
											}
											}
										}

									}

							}

						} else {
							columnsJElements = doc.select(
									"div > div.dQSnI._Z.S2.H2._f > div > div > div.gapux > div > div > div > div");
							if (!columnsJElements.isEmpty()) {
								for (Element columnJElement : columnsJElements) {

									// if (!listDetailsJElement.isEmpty()) {
									// for (Element detail : listDetailsJElement) {
									// div > div.dQSnI._Z.S2.H2._f > div > div > div.gapux > div > div:nth-child(2)
									// > div > div:nth-child(1) > div.dMshX.b
									detailTitleJElement = columnJElement.selectFirst("div.dMshX.b");
									detailValueJElement = columnJElement.selectFirst("div.cfvAV");
									if (detailTitleJElement != null) {
										detailTitle = detailTitleJElement.text().toLowerCase();

										detailValue = detailValueJElement.text();

										switch (detailTitle) {
										case "cuisines" -> {
											typeCuisine = detailValue;
										}
										// 2870;
										case "fourchette de prix" -> {
											
											
											/*String[] detailsValues = detailValue.split("-");
											fourchettePrix = " ";
											int rankEuroAr = 4223;
											for (int e = 0; e < detailsValues.length; e++) {
												int priceInEuro = (Integer
														.parseInt(detailsValues[e].replaceAll("[^0-9]", "")))
														/ rankEuroAr;
												fourchettePrix += String.valueOf(priceInEuro) + "-";
											}
											fourchettePrix += "euro";
											fourchettePrix = fourchettePrix.replaceAll("-e", " e");*/
											 
											 
											fourchettePrix=detailValue +" euro";
										}
										case "repas" -> {
											repas = detailValue;
										}
										case "fonctionnalites" -> {
											fonctionnalites = detailValue;
										}

										case "regimes speciaux" -> {
											regimeSpeciaux = detailValue;
										}
										}
									}

									// }
									// }

								}

							}
						}
					} catch (Exception e) {
						e.printStackTrace();
					} finally {
						System.out.println("continue");
					}

					System.out.println("cuisine " + typeCuisine);
					System.out.println("fourchette de prix " + fourchettePrix);
					System.out.println("repas " + repas);
					System.out.println("fonctionnalites " + fonctionnalites);
					System.out.println("regimes speciaux " + regimeSpeciaux);
					this.save(this.setData(depSoumise, adresse, nom, nombreAgence, tel, horaires, uri, agenceHtml,
							typeCuisine, regimeSpeciaux, repas, fonctionnalites, fourchettePrix, note, classement));

					log.writeLog(new Date(), uri, "no_error", nombreAgence, compteur, (nombreAgence - compteur));
				}

				compteur++;
			}
			i++;
		}
		sc.close();
		// ch.close();

	}

	@Override
	public void saveItem(String... args) {
		// TODO Auto-generated method stub

	}

	@Override
	public void scrap() throws Exception {
		// TODO Auto-generated method stub

	}

}
