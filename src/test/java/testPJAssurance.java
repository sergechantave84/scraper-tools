import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import com.app.scrap.Scrapper1Application;
import com.app.service.ServicePJAssurance;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = Scrapper1Application.class)
@TestPropertySource(locations="classpath:test_application.properties")
public class testPJAssurance {

	@Autowired
	private ServicePJAssurance srv;
	
	@Test
	public void test() throws Exception {
		srv.setDep(true);
		srv.setup();
		srv.scrap();
	}
}
