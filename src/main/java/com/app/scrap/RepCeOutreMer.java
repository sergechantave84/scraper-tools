package com.app.scrap;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.app.model.CE_Outre_Mer;

@Repository
public interface RepCeOutreMer extends JpaRepository<CE_Outre_Mer, Integer>, RepositoryCustom<CE_Outre_Mer> {

}
