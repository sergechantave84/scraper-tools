package com.app.scrap;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.app.model.LCL_Outre_Mer;

@Repository
public interface RepLclOutreMer extends JpaRepository<LCL_Outre_Mer, Integer>, RepositoryCustom<LCL_Outre_Mer>{

}
