package com.app.scrap;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.app.model.Mutuelle_Generale;

@Repository
public interface RepMutuGenerale extends JpaRepository<Mutuelle_Generale,Integer>,RepositoryCustom<Mutuelle_Generale>{

}
