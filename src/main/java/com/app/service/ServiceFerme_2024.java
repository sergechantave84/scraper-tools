package com.app.service;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.text.Normalizer;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.net.ssl.HttpsURLConnection;

import org.json.JSONArray;
import org.json.JSONObject;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.openqa.selenium.PageLoadStrategy;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.app.model.Ferme_2024;
import com.app.scrap.Ferme_2024_Repository;

import fa.browserUtils.FactoryBrowser;
import fa.browserUtils.HtmlUnitBrowser;
import fa.log.LogApp.LogScrap;
import fa.log.config.LogConst;

@Service
public class ServiceFerme_2024 {
	
	private HtmlUnitDriver Hdriver;
	
	@Autowired
	private Ferme_2024_Repository rep;
	
	int num_ligneficval;
	int parcoursficval;

	public void initDriver() throws Exception {
		Hdriver = (HtmlUnitDriver) this.setupHU();
	}
	
	public WebDriver setupHU() {
		FactoryBrowser factB = new FactoryBrowser();
		HtmlUnitBrowser hb = (HtmlUnitBrowser) factB.create("htmlUnit");
		HtmlUnitDriver hu = hb.setOptions(true, false, false, 12000).initBrowserDriver();
		return hu;
	}
	
	public void scrapeFromAPI() throws NumberFormatException, IOException {
	
		int max_page = 450;
		
		String url = "https://api.bienvenue-a-la-ferme.com/api/producer/search?categories[]=products&sales_mode[]=farm";
		
		LogScrap log = new LogScrap("Ferme_2024.txt", LogConst.TYPE_LOG_FILE);
		num_ligneficval = ((new File(LogConst.STORE_LOG_PATH + LogConst.PREFIX_LOG_FILE + "_Ferme_2024.txt"))
				.exists()) ? num_ligneficval = Integer.parseInt(log.readLog().getLastLineProcessed().split("\\t")[4])
						: 1;
		
		for(int i = num_ligneficval+1; i<=max_page; i++) {
			
			String url_page = url+"&page="+i;
	        
	        URL url8;
			HttpsURLConnection con;
			try {
				url8 = new URL(url_page);
				con = (HttpsURLConnection) url8.openConnection();
				con.setRequestMethod("GET");
				con.setRequestProperty("Access-Control-Allow-Credentials", "true");
				con.setRequestProperty("Content-Type", "application/json");
				con.setRequestProperty("Content-length", "0");
				con.setRequestProperty("Content-Encoding", "gzip, deflate, br, zstd");
				con.setRequestProperty("Vary", "Origin");
				con.setRequestProperty("X-Xss-Protection", "1; mode=block");
				con.setRequestProperty("User-Agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.0.0 Safari/537.36");
				con.setConnectTimeout(60000);
				con.setReadTimeout(60000);
				con.setInstanceFollowRedirects(true);
			
				
				BufferedReader in = new BufferedReader(
						new InputStreamReader(con.getInputStream(), StandardCharsets.UTF_8));
				String pageSource;
				StringBuilder strB = new StringBuilder();
				while ((pageSource = in.readLine()) != null) {
					strB.append(pageSource);
				}

				String htm = strB.toString();
			
				JSONObject jsonOb = new JSONObject(htm);
				
				if (jsonOb.has("results")) {
					JSONObject jsonResult = jsonOb.getJSONObject("results");
					
					if(jsonResult.has("items")) {
						JSONArray  jsonItems = jsonResult.getJSONArray("items");
						
						for(int j=0; j<jsonItems.length(); j++) {
							
							JSONObject item = jsonItems.getJSONObject(j);
							
							//System.out.println("item : "+item);
							
							int bafId = item.getInt("bafId");
							String name = item.getString("name");
							String description = item.has("description")?item.getString("description"):"";
							String activite = "Producteur";
							
							String locality = item.getJSONObject("address").has("locality")?
									" "+item.getJSONObject("address").getString("locality")+" ":" ";
							
							String adresse_label = item.getJSONObject("address").has("address")?
									" "+item.getJSONObject("address").getString("address")+" ":" ";
							
							String postalCode = item.getJSONObject("address").has("postalCode")?
									" "+item.getJSONObject("address").getString("postalCode")+" ":" ";
							
							String department = item.getJSONObject("address").has("department")?
									" "+item.getJSONObject("address").getString("department")+" ":" ";
							
							String dep_code = postalCode.trim().substring(0,2);
							
							String ville = item.getJSONObject("address").has("city")?
									" "+item.getJSONObject("address").getString("city")+" ":" ";
							
							String proprietaire = item.getJSONObject("contact").getString("firstName") + " " 
							+item.getJSONObject("contact").getString("lastName");
							
							
							String adresse = adresse_label +locality + postalCode + ville + department;
							
							JSONObject url_ferme = item.getJSONObject("url");
							
							/***---------------SCRAP START------------------***/
							
							String pathURL = "https://www.bienvenue-a-la-ferme.com/"+url_ferme.get("region")+"/"+url_ferme.get("department")+"/"+
									url_ferme.get("city")+"/ferme/"+url_ferme.get("slugName")+"/"+url_ferme.get("id");
							
							Hdriver.get(pathURL);
							
							String pageS = Hdriver.getPageSource();
							Document doc = Jsoup.parse(pageS);
							
							Element main = doc.selectFirst("#main");
							
							/* Bloc nom */
							
							Element nom_elem = main.selectFirst("div.container.svelte-11btw5i > h2");
							String name_string = nom_elem!=null?nom_elem.text().trim():name;
							
							/* Bloc production de la ferme */
							Element produitFerme = main.selectFirst("#accordion-products > div > div.ServiceLayout.ServiceLayout--leftLayout.svelte-1llfxb1 > div > div > p");
							String productionFerme = produitFerme!= null? produitFerme.text():"";
							
							Element prod_div = main.selectFirst("#accordion-products > div > div.ServiceLayout.ServiceLayout--leftLayout.svelte-1llfxb1 > div > div:nth-child(2)");
							
							int engagement_prod = prod_div!=null && prod_div.text().toLowerCase().contains("nos engagements")?1:0;
							
							Element prododuits_elem = main.selectFirst("div.container.svelte-11btw5i > div > div.main.svelte-11btw5i > div.Description-text.svelte-1o90tsm > div > p:nth-child(3)");
							String productions_ferme = prododuits_elem != null && prododuits_elem.text().contains("Productions de la ferme")?
									prododuits_elem.text().replace("Productions de la ferme :", "").trim():"";
							
							Element prod_ferme_elem = main.selectFirst("#accordion-products > div > div.ServiceLayout.ServiceLayout--leftLayout.svelte-1llfxb1 > div > div> p.ServiceLayout-description");
							String all_produits_ferme = prod_ferme_elem != null ? prod_ferme_elem.text().trim():"";
							String [] produits = all_produits_ferme.split(",");
							
							/* Bloc horaire */
							Element hr_ferme = main.selectFirst("#accordion-products > div > div.BuyProducts.svelte-15uja1y > div > div > div > div > div > div > ul");
							String horaire_ferme = hr_ferme!= null ? hr_ferme.text() :"";
							
							Elements hr_marches = main.select("#accordion-products > div > div.Stores.svelte-1oneyk1 > div.OtherStores.svelte-86h5k7 > div");
							
							String horaire_marche ="";
							if(hr_marches.size()>0) {
								for(Element hr_marche : hr_marches) {
									horaire_marche = (hr_marche!= null && hr_marche.text().contains("Sur les marchés")) ? 
											hr_marche.text().replace("Sur les marchés :", "").trim() :"";
								}
							}
							//String horaire_marche = hr_marche!= null ? hr_marche.text() :"";
							
							Elements hr_magasins = main.select("#accordion-products > div > div.BuyProducts.svelte-15uja1y > div > div > div > div > div");
							
							String horaire_magasin = "";
							
							if(hr_magasins.size()>0) {
								for(Element hr_magasin : hr_magasins) {
									horaire_magasin = (hr_magasin!=null && hr_magasin.text().contains("Notre magasin"))? 
											hr_magasin.text().replace("Notre magasin :", "").trim():"";
								}
							}
							Elements mobilite_elem = main.select("#accordion-products > div > div.ServiceLayout.ServiceLayout--leftLayout.svelte-1llfxb1 > div > div > div");
							String mobilite = "";
							
							/* Bloc accees */
							int degustation = 0;
							if(mobilite_elem.size()>0) {
								for(Element mob : mobilite_elem) {
									if(mob.text().contains("Modalités de visite de la ferme")) {
										mobilite = mob.text().trim();
										
									}else if(mob.text().toLowerCase().contains("dégustation")) {
										degustation = 1;
									}
								}
							}
							
							Element vente_enline_div = main.selectFirst("#accordion-products > div > div.Stores");
							int hasOnLine = (vente_enline_div!= null && vente_enline_div.text().contains("vente en ligne"))?1:0;
							
							Element atelier_elem = main.selectFirst("#accordion-products > div > div.ServiceLayout.ServiceLayout--leftLayout.svelte-1llfxb1");
							int hasAtelier = (atelier_elem !=null && atelier_elem.text().contains("atelier"))?1:0;
							
							/** Block coordonées **/
							
							String email_str = "";
							
							Element coord = main.selectFirst("#accordion-contact > div > div.Contact-location.svelte-13fbbq8 > div.Contact-locationInfos.svelte-13fbbq8 > div > a");
							String coord_str = coord!= null? coord.attr("href"):"Location&daddr=000,000";
							
							URL url_coord = new URL(coord_str);
							String queries = url_coord.getQuery().replace("Location&daddr=","");
							Float lat = Float.parseFloat(queries.split(",")[0]);
							Float lng = Float.parseFloat(queries.split(",")[1]);
							
							Element add = main.selectFirst("#accordion-contact > div > div.Contact-definitions.svelte-13fbbq8 > div:nth-child(2) > div > div > p");
							String add_string = add!= null? add.text() : adresse;
							
							String telephone_domicile="";	
							String telephone_mobile="";
							String telephone_travail="";
							String sites_urls = "";
							List<String> tels = new ArrayList<>();
							Element coord_elem = main.selectFirst("#accordion-contact > div > div.Contact-definitions.svelte-13fbbq8");
							if(coord_elem != null) {
								Elements coords = coord_elem.select("div.DefinitionWithIcon.svelte-1rzw202");
								
								for(Element c : coords) {
									
									if(c.text().contains("Contact")) {
										
										Element proprietaire_elem = c.selectFirst("div > div.DefinitionWithIcon-content > p");
										proprietaire = proprietaire_elem !=null? proprietaire_elem.text():"";
									}else if(c.text().contains("Adresse")) {
										Element adresse_elem = c.selectFirst("div > div.DefinitionWithIcon-content > p");
										add_string = adresse_elem!=null? adresse_elem.text():"";			
									}else if(c.text().contains("Courriel")) {
										
										Elements couriel_elem = c.select("div > div > ul > li");
										
										if(couriel_elem.size()>0) {
											for(int k = 0; k<couriel_elem.size(); k++) {
												if(k<couriel_elem.size()-1) {
													
													email_str += couriel_elem.get(k).text()+" & ";
												}else {
													email_str += couriel_elem.get(k).text();
												}
											}
										}
									}else if(c.text().contains("internet")) {
										
										Elements site_elems = c.select("div > div > ul > li");
										if(site_elems.size()>0) {
											for(int k = 0; k<site_elems.size(); k++) {
												if(k<site_elems.size()-1) {
													
													sites_urls += site_elems.get(k).text()+" & ";
												}else {
													sites_urls += site_elems.get(k).text();
												}
											}
										}
									}else if(c.text().contains("Nous appeler")) {
										Elements phone_elems = c.select("div > div > ul > li");
										if(phone_elems.size()>0) {
											
											telephone_travail = (0 >= phone_elems.size() || 0 < 0)? "":phone_elems.get(0).text();
											telephone_mobile = (1 >= phone_elems.size() || 1 < 0)? "":phone_elems.get(1).text();
											telephone_domicile = (2 >= phone_elems.size() || 2 < 0)? "":phone_elems.get(2).text();
												
										}
									}else {
										System.out.println("Autres coordonnées!!");
										
										System.out.println(c.text());
									}
								}
							}
							
							/* Bloc accès*/
							Element flag_elem = main.selectFirst("div.container.svelte-11btw5i > div > div.main.svelte-11btw5i > ul");
							
							int agriBio = 0; 
							int accHandi = 0;
							int cb = 0;
							int animaux = 0;
							int car = 0;
							int chequeVacance = 0;
							int marcheProd = 0;
							int internet = 0;
							int handMent = 0;
							int handMot = 0;
							int stationVerte = 0;
							int handVis = 0;
							int handAud = 0;
							int adeve = 0;
							int tickets = 0;
							
							if (flag_elem != null) {
								Elements imgs = flag_elem.select("li > svg");

								for (Element img : imgs) {

									String key = img.attr("aria-label").toLowerCase();

									switch (key) {
									case "agriculture biologique" -> agriBio = 1;
									case "accessibilité" -> accHandi = 1;
									case "carte bancaire" -> cb = 1;
									case "animaux domestiques autorisés" -> animaux = 1;
									case "bus" -> car = 1;
									case "cheques vacances" -> chequeVacance = 1;
									case "marche des producteurs de pays" -> marcheProd = 1;
									case "wifi" -> internet = 1;
									case "handicap mental" -> handMent = 1;
									case "handicap moteur" -> handMot = 1;
									case "handicap visuel" -> handVis = 1;
									case "station verte" -> stationVerte = 1;
									case "handicap auditif" -> handAud = 1;
									case "adherent a l'adeve" -> adeve = 1;
									case "tickets restaurant" -> tickets = 1;
									default -> {

										System.out.println("AUTRE LOGOS");
										System.out.println(key);

									}
									}

								}

							}
							
							String full_horaire_ferme = (horaire_ferme + "\n"+mobilite.trim()).trim();
							String produit_1 = isValidIndex(produits,0)?produits[0].trim():"";
							String produit_2 = isValidIndex(produits,1)?produits[1].trim():"";
							String produit_3 = isValidIndex(produits,2)?produits[2].trim():"";
							String produit_4 = isValidIndex(produits,3)?produits[3].trim():"";
							String produit_5 = isValidIndex(produits,4)?produits[4].trim():"";
							String produit_6 = isValidIndex(produits,5)?produits[5].trim():"";
							String produit_7 = isValidIndex(produits,6)?produits[6].trim():"";
							String produit_8 = isValidIndex(produits,7)?produits[7].trim():"";
							
							System.out.println("Page : "+i);
							System.out.println("PathURL : "+pathURL);
							System.out.println("bafId : "+bafId);
							System.out.println("Name : "+name_string);
							//System.out.println("Contact : "+proprietaire);
							System.out.println("Proprietaire : "+proprietaire);
							System.out.println("Coordonné : "+queries);
							System.out.println("Adresse : "+add_string);
							System.out.println("Description : "+description);
							System.out.println("Email : "+email_str);
							System.out.println("Telephone Mobile : "+telephone_mobile);
							System.out.println("Telephone travail : "+telephone_travail);
							System.out.println("Telephone domicile : "+telephone_domicile);
							System.out.println("Site : "+sites_urls);
							System.out.println("ProduitFerme : "+productionFerme);
							System.out.println("Horaire_ferme : "+full_horaire_ferme);
							System.out.println("Horaire_marche : "+horaire_marche);
							System.out.println("Horaire_magasin : "+horaire_magasin);
							System.out.println("Vente en ligne : "+hasOnLine);
							System.out.println("Degustation : "+degustation);
							System.out.println("Produit1 : "+all_produits_ferme);
							/*System.out.println("Produit2 : "+produit_2);
							System.out.println("Produit3 : "+produit_3);
							System.out.println("Produit4 : "+produit_4);
							System.out.println("Produit5 : "+produit_5);
							System.out.println("Produit6 : "+produit_6);
							System.out.println("Produit7 : "+produit_7);
							System.out.println("Produit8 : "+produit_8);*/
							
							
							System.out.println(" \n-------------***********----------- \n ");
							
							/* Mise à jour de champ de la ferme */
							//Ferme_2024 ferme = new Ferme_2024();
							Ferme_2024 new_ferme_bafID = rep.findByBafId(bafId);
							Ferme_2024 new_ferme_nomAdresse = rep.findByNameAndAddress(name_string.trim(), add_string.trim());
							Ferme_2024 ferme;
							
							if( new_ferme_bafID!= null) {
								ferme = new_ferme_bafID;
							}else if(new_ferme_nomAdresse!= null) {
								ferme = new_ferme_nomAdresse;
							}else {
								ferme = new Ferme_2024();
							}
							ferme.setBafId(bafId);
							ferme.setActivite(activite);
							ferme.setAddress(add_string.trim());
							ferme.setEmail(email_str);
							ferme.setNomFerme(name_string.trim());
							ferme.setCodePostale(postalCode.trim());
							ferme.setDepartement(dep_code);
							ferme.setDepartementName(department);
							ferme.setNomProprietaire(proprietaire.trim());
							ferme.setEngagementProd(engagement_prod);
							ferme.setLatitude(lat);
							ferme.setLongitude(lng);
							ferme.setGenre("Direct chez le producteur");
							ferme.setMotFermier(description);
							ferme.setProduitFerme(productions_ferme);
							ferme.setAgricultureBio(agriBio);
							ferme.setHorairesFerme(full_horaire_ferme);
							ferme.setHorairesMagasin(horaire_magasin);
							ferme.setHorairesMarche(horaire_marche);
							ferme.setAccesAuditif(handAud);
							ferme.setAccesHandicape(accHandi);
							ferme.setAccesMental(handMent);
							ferme.setAccesMotrice(handMot);
							ferme.setAccesVisuel(handVis);
							ferme.setAccesVoiture(car);
							ferme.setAnimauxAutoriser(animaux);
							ferme.setAdherentAdeve(adeve);
							ferme.setCarteBancaire(cb);
							ferme.setChequeVacance(chequeVacance);
							ferme.setStationVerte(stationVerte);
							ferme.setTicketsRestaurant(tickets);
							ferme.setMarcherProduit(marcheProd);
							ferme.setVille(ville.trim());
							ferme.setSiteUrls(sites_urls);
							ferme.setTelephoneDomicile(telephone_domicile);
							ferme.setTelephoneMobile(telephone_mobile);
							ferme.setTelephoneTravail(telephone_domicile);
							ferme.setVenteEnLigne(hasOnLine);
							ferme.setDegustation(degustation);
							ferme.setAtelier(hasAtelier);
							ferme.setProduit1(all_produits_ferme);
							/*ferme.setProduit2(produit_2);
							ferme.setProduit3(produit_3);
							ferme.setProduit4(produit_4);
							ferme.setProduit5(produit_5);
							ferme.setProduit6(produit_6);
							ferme.setProduit7(produit_7);
							ferme.setProduit8(produit_8);*/
							ferme.setProduit2("");
							ferme.setProduit3("");
							ferme.setProduit4("");
							ferme.setProduit5("");
							ferme.setProduit6("");
							ferme.setProduit7("");
							ferme.setProduit8("");
							
							/* Sauvegarder dans la base de données */
							rep.save(ferme);
							
							
						}
						
					}
				}
				
				//System.out.println(json);
				
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			log.writeLog(new Date(), "Ferme", "no_error", 10 , i, 0);
			
			//parcoursficval++;
		}
		
		System.err.println("\n************** Scrap terminé. **************\n");
		
	}
	
	public static boolean isValidIndex(String[] arr, int index) {
        return index >= 0 && index < arr.length;
    }
	
	public String normalize(String s) {
		if (s == null) {
			return null;
		} else {
			s = Normalizer.normalize(s, Normalizer.Form.NFD);
			s = s.replaceAll("[\\p{InCombiningDiacriticalMarks}]", "");
			return s;
		}

	}

	public WebDriver getDriver() {
		
		System.setProperty("webdriver.chrome.driver", "D:/ChromeDriver/chromedriver.exe");
		ChromeOptions options = new ChromeOptions();
		
		options.addArguments("start-maximized");
		options.setPageLoadStrategy(PageLoadStrategy.NORMAL);

		WebDriver driver = new ChromeDriver(options);
		return driver;
	}

}
