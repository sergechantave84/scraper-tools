package com.app.scrap;

import org.springframework.data.jpa.repository.JpaRepository;

import com.app.model.Aviva;
import com.app.model.BanqueCMSO;

public interface RepBanqueCMSO extends JpaRepository<BanqueCMSO,Integer>,RepositoryCustom<BanqueCMSO>{

}
