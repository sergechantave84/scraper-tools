package com.app.model;

import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="banque_visa_adresse", schema="scrap")
public class Visa_adresse {
	
	@Id
	@SequenceGenerator(name="scrap.banque_visa_adresse_id_seq",
	                    sequenceName ="scrap.banque_visa_adresse_id_seq",
	                     allocationSize=1)
    @GeneratedValue( strategy=GenerationType.SEQUENCE,
                     generator="scrap.banque_visa_adresse_id_seq")
	  
	@Column(name="id",updatable=false)
	private int id;
	
	@Column(name="adresseSoumise", columnDefinition="varchar")
	private String adresse;
	
	@Column(name="nbpages", columnDefinition="varchar")
	private String nbPages;
	
	@Column(name="numpage", columnDefinition="varchar")
	private String numPage;
	
	@Column(name="nbagences_pages", columnDefinition="varchar")
	private String nbAgencesPage;
	
	@Column(name="nomweb", columnDefinition="varchar")
	private String nomWeb;
	
	@Column(name="voie", columnDefinition="varchar")
	private String voie;
	
	@Column(name="cp", columnDefinition="varchar")
	private String cp;
	
	@Column(name="ville", columnDefinition="varchar")
	private String ville;
	
	@Column(name="dab", columnDefinition="varchar")
	private String dab;
	
	@Column(name="agencehtm", columnDefinition="varchar")
	private String agenceHtm;
	
	@Column(name="annee_du_scrap" ,columnDefinition="varchar(4) DEFAULT date_part('year',CURRENT_DATE)")
	private String anneeScrap;
	
	public Visa_adresse() {
		LocalDate ld = new Date().toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
		anneeScrap = String.valueOf(ld.getYear() + 1);
	}
	public int getId() {
		return id;
	}
	public Visa_adresse setId(int id) {
		this.id = id;
		return this;
	}
	public String getCpSoumis() {
		return adresse;
	}
	public Visa_adresse setAdresseSoumis(String adresse) {
		this.adresse = adresse;
		return this;
	}
	public String getNbPages() {
		return nbPages;
	}
	public Visa_adresse setNbPages(String nbPages) {
		this.nbPages = nbPages;
		return this;
	}
	public String getNumPage() {
		return numPage;
	}
	public Visa_adresse setNumPage(String numPage) {
		this.numPage = numPage;
		return this;
	}
	public String getNbAgencesPage() {
		return nbAgencesPage;
	}
	public Visa_adresse setNbAgencesPage(String nbAgencesPage) {
		this.nbAgencesPage = nbAgencesPage;
		return this;
	}
	public String getNomWeb() {
		return nomWeb;
	}
	public Visa_adresse setNomWeb(String nomWeb) {
		this.nomWeb = nomWeb;
		return this;
	}
	public String getVoie() {
		return voie;
	}
	public Visa_adresse setVoie(String voie) {
		this.voie = voie;
		return this;
	}
	public String getCp() {
		return cp;
	}
	public Visa_adresse setCp(String cp) {
		this.cp = cp;
		return this;
	}
	public String getVille() {
		return ville;
	}
	public Visa_adresse setVille(String ville) {
		this.ville = ville;
		return this;
	}
	public String getDab() {
		return dab;
	}
	public Visa_adresse setDab(String dab) {
		this.dab = dab;
		return this;
	}
	public String getAgenceHtm() {
		return agenceHtm;
	}
	public Visa_adresse setAgenceHtm(String agenceHtm) {
		this.agenceHtm = agenceHtm;
		return this;
	}
	public String getAnneeScrap() {
		return anneeScrap;
	}
	public Visa_adresse setAnneeScrap(String anneeScrap) {
		this.anneeScrap = anneeScrap;
		return this;
	}
}
