package com.app.scrap;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.app.model.CM_GAB;

@Repository
public interface RepCM_GAB extends JpaRepository<CM_GAB, Integer>,RepositoryCustom<CM_GAB> {

}
