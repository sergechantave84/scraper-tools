package com.app.service;

import java.io.BufferedInputStream;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;

import java.net.SocketTimeoutException;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Scanner;

import org.json.JSONObject;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import org.openqa.selenium.htmlunit.HtmlUnitDriver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.app.model.CA;
import com.app.model.CA_Outre_Mer;
import com.app.scrap.RepCaOutreMer;

import com.app.utils.Parametres;

import fa.log.LogApp.LogScrap;
import fa.log.config.LogConst;

@Service
public class ServiceCA_Outre_Mer extends Scraper {

	@Autowired
	private RepCaOutreMer rep;

	String nomSITE = "CA_Outre_Mer";

	String ficval = "lienCA_DOM";
	String dossierRESU = Parametres.REPERTOIRE_RESULT + "\\BQ\\" + nomSITE;
	String url = "https://www.credit-agricole.fr";

	String cp;
	String ville;
	String fichier_valeurs;
	String ligne;

	int num_ligne = 2;
	int NbVilles;
	int NbAgences;
	int numreponse = 1;
	int casNormandie = 0;
	int indice_ville;
	int nbRecuperes;
	int num_ligneficval;
	int parcoursficval;
	int Resultats;

	HtmlUnitDriver htmlUnitDriver;
	// HtmlUnitDriver htmlUnitDriver;
	BufferedWriter sortie;
	BufferedWriter sortie_log;
	Date Temps_Fin;

	public CA_Outre_Mer setData(String lienEnseigne, String nbVilles, String numVilles, String villeSoumise,
			String nbAgences, String lienAgence1, String nomWeb, String adresse, String tel, String fax,
			String horairesHtm, String horairesCA_Outre_Merxt, String services, String coordonnees, String agenceHtm) {
		CA_Outre_Mer ca = new CA_Outre_Mer();
		ca.setLienEnseigne(lienEnseigne).setNbVilles(nbVilles).setNumVilles(numVilles).setVilleSoumise(villeSoumise)
				.setNbAgences(nbAgences).setLienAgence(lienAgence1).setNomWeb(nomWeb).setAdresse(adresse).setTel(tel)
				.setFax(fax).setHorairesHtm(horairesHtm).setHorairesTxt(horairesCA_Outre_Merxt).setServices(services)
				.setCoordonnees(coordonnees).setAgenceHtm(agenceHtm);
		rep.save(ca);
		return ca;
	}

	public void setup() {
		htmlUnitDriver = (HtmlUnitDriver) this.setupHU();
	}

	public void tearDown() {
		htmlUnitDriver.quit();

	}

	@Override
	public void saveItem(String... args) {
		// TODO Auto-generated method stub
	}

	@Override
	public void scrap() throws Exception {

		fichier_valeurs = Parametres.REPERTOIRE_DATA_SOURCES + ficval + ".txt";
		Scanner sc = new Scanner(new BufferedInputStream(new FileInputStream(new File(fichier_valeurs))));
		Scanner sc1 = new Scanner(new File(fichier_valeurs));

		if (!new File(dossierRESU).exists()) {
			new File(dossierRESU).mkdirs();
		}
		// fichier = new File(dossierRESU + "\\" + "RESULTATS_" + nomSITE + "_" +
		// System.currentTimeMillis() + ".txt");
		LogScrap log = new LogScrap("BanqueCA_DOM.txt", LogConst.TYPE_LOG_FILE);
		num_ligneficval = ((new File(LogConst.STORE_LOG_PATH + LogConst.PREFIX_LOG_FILE + "_BanqueCA_DOM.txt"))
				.exists()) ? Integer.parseInt(log.readLog().getLastLineProcessed().split("\\t")[4]) : 1;

		parcoursficval = 1;
		int i = 1;

		while (sc1.hasNextLine()) {
			String l = sc1.nextLine();
			if (i > 1) {
				NbVilles++;
			}
			i++;
		}
		sc1.close();
		while (sc.hasNextLine()) {
			String line = sc.nextLine();
			String[] fic_valeurs = line.split("\\t");

			if (parcoursficval > num_ligneficval) {

				cp = fic_valeurs[1];
				ville = fic_valeurs[0];
				// TODO change
				String uri1 = url+ville;
				try {
					System.out.println("first uri " + uri1);
					htmlUnitDriver.get(uri1);

				} catch (Exception e) {
					if (e instanceof SocketTimeoutException) {
						htmlUnitDriver.get(uri1);

					}
				}

				String pageSource = htmlUnitDriver.getPageSource();
				Document doc = null;
				doc = Jsoup.parse(pageSource);

				Element root = doc.selectFirst(
						"body > main > div > div > div > div.container > div.indexTown-Content > div > div");
				List<String> agenciesLinkList = new ArrayList<>();
				List<String> subAgenciesLinks = new ArrayList<>();
				if (root != null) {

					if (!agenciesLinkList.isEmpty()) {
						agenciesLinkList.clear();
					}

					Elements agenciesLinkElement = root.select("ul");

					for (Element tmp4 : agenciesLinkElement) {

						Elements hrefs = tmp4.select("li>a");
						for (Element href : hrefs) {
							String strHref = href.attr("href");
							agenciesLinkList.add(strHref);
							//System.out.println(strHref);
						}
					}
				}

				if (!agenciesLinkList.isEmpty()) {
					NbAgences = agenciesLinkList.size();
					for (String path : agenciesLinkList) {

						String nom_agence = "";
						String adresse = "";
						String tel = "";
						String fax = "";
						String Services = "";
						String HorairesHTM = "";
						String HorairesTXT = "";
						String coord = "";
						String agences_HTM = "";
						String villeSoumise = "";
						String uri = "https://www.credit-agricole.fr" + path;
						
						try {
							System.out.println("alphabetic : " + uri);
							htmlUnitDriver.get(uri);

						} catch (Exception e) {
							if (e instanceof SocketTimeoutException) {
								System.out.println(uri);
								htmlUnitDriver.get(uri);

							}
						}

						pageSource = htmlUnitDriver.getPageSource();
						doc = Jsoup.parse(pageSource);

						Element main = doc.select(
								"body > main > div > div > div > div.container-fluid > div > div.container > div > "
										+ "div.StoreLocatorMap-content.js-StoreLocatorMap-content > div.StoreLocatorMap-Agencies.js-StoreLocatorMap-Agencies")
								.first();

						if (main != null) {
							System.out.println("not null");
							Elements pathsElements = main.select("ul>li");

							subAgenciesLinks.clear();

							for (Element pathElement : pathsElements) {

								Element href = pathElement.selectFirst("a");
								if (href != null) {
									String tmp1 = href.attr("href");
									subAgenciesLinks.add(tmp1);

								}

							}
							int numVille = 0;
							for (String path1 : subAgenciesLinks) {
								String uriVille = "https://www.credit-agricole.fr" + path1;
								villeSoumise = path1;
								try {
									System.out.println("uriVille " + uriVille);
									htmlUnitDriver.get(uriVille);

								} catch (Exception e) {
									if (e instanceof SocketTimeoutException) {
										System.out.println(uriVille);
										htmlUnitDriver.get(uriVille);

									}
								}

								pageSource = htmlUnitDriver.getPageSource();
								doc = Jsoup.parse(pageSource);
								
								this.retrieveData(doc, path1, numVille, uriVille, cp);
								
								/*Element infosRoot = doc.selectFirst(
										"body > main > div.Template > " + "div.container-fluid > div.StoreLocatorCard");
								if (infosRoot != null) {
									nom_agence = "";
									adresse = "";
									tel = "";
									fax = "";
									Services = "";
									HorairesHTM = "";
									HorairesTXT = "";
									coord = "";
									agences_HTM = "";

									agences_HTM = infosRoot.outerHtml();
									agences_HTM = this.normalize(agences_HTM.replaceAll("\\s{1,}", ""));

									Elements containers = infosRoot.select(".container");

									Element container0 = containers.get(0);
									Element container1 = containers.get(2);

									// retrieve agency name

									Element agencyNameElmnt = container0.selectFirst("div.row>div>h1");

									nom_agence = (agencyNameElmnt != null) ? this.normalize(agencyNameElmnt.text())
											: "";
									System.out.println("nom agence " + nom_agence);

									Element agencyCoordinate = container1.selectFirst("div.row>div>div.CardAgencyFunc");

									// retrieve agency adresse
									Element agencyAddress = agencyCoordinate
											.selectFirst("div:nth-child(1)>p.CardAgencyFunc-address");
									adresse = (agencyAddress != null) ? this.normalize(agencyAddress.text()) : "";
									System.out.println("agency address " + adresse);

									// retrieve agency tel
									Element agencyTelElement = agencyCoordinate
											.selectFirst("div:nth-child(1)>div.CardAgencyFunc-bloc>p:nth-child(1)>a");
									tel = (agencyTelElement != null) ? this.normalize(agencyTelElement.attr("href"))
											: "";
									tel = tel.replaceAll("[^0-9]", "");
									System.out.println("tel " + tel);

									// retrieve agency fax
									Element agencyFaxElement = agencyCoordinate
											.selectFirst("div:nth-child(1)>div.CardAgencyFunc-bloc>p:nth-child(2)");
									fax = (agencyFaxElement != null) ? this.normalize(agencyFaxElement.text()) : "";
									fax = fax.replaceAll("[^0-9]", "");
									System.out.println("fax " + fax);

									// retrieve agency schedule
									Elements agencyScheduleElements = agencyCoordinate.select("div:nth-child(2)>ul>li");
									HorairesHTM = agencyScheduleElements.outerHtml();
									HorairesHTM = HorairesHTM.replaceAll("\\s{1,}", "");
									HorairesHTM = this.normalize(HorairesHTM);
									for (Element tmp3 : agencyScheduleElements) {
										HorairesTXT += tmp3.text() + " ";
									}
									HorairesTXT = this.normalize(HorairesTXT);
									System.out.println("horaires " + HorairesTXT);
									numVille++;

									// retrieve services
									Elements servicesElements = agencyCoordinate.select("div:nth-child(3)>ul>li");

									for (Element servElement : servicesElements) {
										Services += servElement.text() + " ";
									}
									Services = this.normalize(Services);
									System.out.println("les services sont " + Services);
									numVille++;

									// retrieve coord
									Element geoCoord = containers.get(3).selectFirst("div>div>div.CardAgencyMap");
									if (geoCoord != null) {
										String strGeoCoord = geoCoord.attr("data-value");
										JSONObject json = new JSONObject(strGeoCoord);
										String lat = "", longt = "";
										lat = (String) json.get("latitude");
										longt = (String) json.get("longitude");
										coord = "Lat:" + " " + lat + " " + "Long:" + " " + longt;
										System.out.println("Lat:" + " " + lat + " " + "Long:" + " " + longt);
									}

									this.setData(cp, String.valueOf(NbVilles), String.valueOf(numVille), villeSoumise,
											String.valueOf(NbAgences), uriVille, nom_agence, adresse, tel, fax,
											HorairesHTM, HorairesTXT, Services, coord, agences_HTM);
								}*/
								// }

							}

						}else {
							
							System.out.println("\n ************** Tonga dia scrap*****************\n");
							
							this.retrieveData(doc, uri.replace("https://www.credit-agricole.fr", ""), 0, uri, cp);
							
						}

						// }
						nbRecuperes++;
					}

					// enregistrer_journal(countAgencyNumber, 1);
				} else {
					// this.enregistrer("", "", "","",0, ville,"","","","","","");
				}

				// }

				// }
				log.writeLog(new Date(), ville, "no_error", NbVilles, parcoursficval, 0);
			}
			parcoursficval++;
		}
		sc.close();

	}
	
	public void retrieveData(Document doc, String villeSoumise, int numVille, String uriVille, String cp) {
		
		String nom_agence = "";
		String adresse = "";
		String tel = "";
		String fax = "";
		String Services = "";
		String HorairesHTM = "";
		String HorairesTXT = "";
		String coord = "";
		String agences_HTM = "";
		//String villeSoumise = "";
		int relais = 0;
		
		
		Element infosRoot = doc.selectFirst(
				"#main > div > div > div");
		if (infosRoot != null) {
			nom_agence = "";
			adresse = "";
			tel = "";
			fax = "";
			Services = "";
			HorairesHTM = "";
			HorairesTXT = "";
			coord = "";
			agences_HTM = "";
			relais = 0;

			agences_HTM = infosRoot.outerHtml();
			agences_HTM = this.normalize(agences_HTM.replaceAll("\\s{1,}", ""));
			
			//System.out.println(infosRoot);
			
			Element agencyNameElmnt = infosRoot.selectFirst("div > div.npc-sl-strct-coordonnee > div > div > h1");
			
			nom_agence = (agencyNameElmnt != null) ? this.normalize(agencyNameElmnt.text().replaceAll("\\s{2,}", " ").trim())
					: "";
			
			Element agencyAddress = infosRoot.selectFirst("div > div.npc-sl-strct-coordonnee > div > div > div.npc-sl-strct-infos-ctct-adresse");
			
			adresse = (agencyAddress != null) ? this.normalize(agencyAddress.text().replaceAll("\\s{2,}", " ").trim().toUpperCase()) : "";
			
			
			System.out.println("nom agence : " + nom_agence);
			
			Element agencyCoordinate = infosRoot.selectFirst("div > div.npc-sl-strct-coordonnee > div > div > div.npc-sl-strct-infos-ctct-actions");
			
			System.out.println("agency address : " + adresse);
			
			// RootElement for phone // Fax
			Elements rootTelFaxElems = agencyCoordinate.select("a");
			
			for(Element rootTelFaxElem : rootTelFaxElems) {
				//Element label = rootTelFaxElem.selectFirst("span.CardAgencyFunc-label");
				String labelText = this.normalize(rootTelFaxElem.text().toLowerCase());
				if(labelText.contains("appeler")) {
					tel = rootTelFaxElem.attr("href");
				}else if(labelText.contains("fax")){
					fax = labelText;
				}
			}
			
			tel = tel.replaceAll("[^0-9]", "");
			System.out.println("tel : " + tel);
			
			fax = fax.replaceAll("[^0-9]", "");
			System.out.println("fax : " + fax);

			// retrieve agency schedule
			
			Element agencyScheduleElement = infosRoot.selectFirst("div > div.npc-sl-strct-horairesMap > div.npc-sl-strct-horaires > ul");
			
			Elements agencyScheduleElements = infosRoot.select("div > div.npc-sl-strct-horairesMap > div.npc-sl-strct-horaires > ul > li");
			
			String hh ="";
			for(Element shedule : agencyScheduleElements) {
				//String jour_name = shedule.select(".ficheAgence-jourNom").first()!= null ? shedule.select(".ficheAgence-jourNom").first().text() : "";
				
				Element jour_ouvert_name = shedule.select("span.ficheAgence-jourNom").first();
				String jour_name = jour_ouvert_name!= null ? jour_ouvert_name.text() : "";
				
				//Element horaires= jour_ouvert_elem.get(1);
				Elements horairesList = shedule.select("span.ficheAgence-jourHorairesOuverture > span");
				String h_str = "";
				for(Element h : horairesList) {
					if(h.hasClass("ficheAgence-sur-rendez-vous")) {
						h_str+=h.text()+"(Sur RDV) ";
					}else {
						h_str+=h.text()+" ";
					}
				}
				hh+=jour_name+" : "+h_str;
				
				//String jour_ouvert = shedule.select(".ficheAgence-jourHorairesOuverture").first()!= null ? shedule.select(".ficheAgence-jourHorairesOuverture").first().text() : "";
			}
			
			//System.out.println("hh : " + hh);
			Element vqh = infosRoot.selectFirst("div > div.npc-sl-strct-horairesMap > div.npc-sl-strct-horaires > div.npc-sl-strct-horaires--24h");
			if(agencyScheduleElement!= null) {
				HorairesHTM = agencyScheduleElement.outerHtml();
				HorairesHTM = HorairesHTM.replaceAll("\\s{1,}", " ").trim();
				HorairesHTM = this.normalize(HorairesHTM);
				
				//HorairesTXT = agencyScheduleElement.text().replaceAll("\\s{2,}", " ").trim();
				HorairesTXT = hh.replaceAll("\\s{2,}", " ").trim();
		
				HorairesTXT = this.normalize(HorairesTXT);
				
			}else if(vqh != null){
				HorairesHTM = vqh.outerHtml();
				HorairesHTM = HorairesHTM.replaceAll("\\s{1,}", " ").trim();
				HorairesHTM = this.normalize(HorairesHTM);
				
				//HorairesTXT = agencyScheduleElement.text().replaceAll("\\s{2,}", " ").trim();
				HorairesTXT = vqh.text().replaceAll("\\s{2,}", " ").trim();
		
				HorairesTXT = this.normalize(HorairesTXT);
				
			}
			
			//HorairesTXT = HorairesTXT.replaceAll("\\*", " Sur RDV");
			
			System.out.println("horaires : " + HorairesTXT);
		

			// retrieve services

			Element servicesElement = infosRoot.selectFirst("div > div.npc-sl-strct-services > div > div");
			
			Services = (servicesElement != null )?servicesElement.text().replaceAll("\\s{2,}", " ").trim() : "";

			Services = this.normalize(Services);
			
			System.out.println("Services : " + Services);
			
			// retrieve coord
			
			Element geoCoord = infosRoot.selectFirst("div > div.npc-sl-strct-horairesMap > div.npc-sl-strct-mapInfosLegales > div.npc-sl-strct-map.Card.js-CardAgencyMap");
			
			if (geoCoord != null) {
				String strGeoCoord = geoCoord.attr("data-value");
				JSONObject json = new JSONObject(strGeoCoord);
				String lat = "", longt = "";
				lat = json.getString("latitude");
				longt = json.getString("longitude");
				coord = "Lat:" + " " + lat + " " + "Long:" + " " + longt;
				System.out.println("Lat :" + " " + lat + " " + "Long :" + " " + longt);
			}

			// body > main > div > div > div > div:nth-child(5) > div > div > div >
			// div:nth-child(2) > p:nth-child(1)
			Element relaisStatusElement = doc.selectFirst(
					"body > main > div > div > div > div:nth-child(5) > div > div > div > div:nth-child(2) > p:nth-child(1)");
			String tmp = (relaisStatusElement != null)
					? this.normalize(relaisStatusElement.text().toLowerCase())
					: "";
			System.out.println("tmp : " + tmp);

			Element relaisStatusElement2 = doc.selectFirst(
					"body > main > div > div > div > div:nth-child(6) > div > div > div > div:nth-child(2) > ul");
			String tmp2 = (relaisStatusElement2 != null) ? this.normalize(
					relaisStatusElement2.text().toLowerCase().replaceAll("\\*", "Sur RDV"))
					: "";
			//System.out.println("tmp2 " + tmp2);

			if (nom_agence.toLowerCase().contains("relais ca")) {
				relais = 1;
			} else if ((tmp.contains("relais ca")) || (tmp2.contains("relais ca"))) {
				relais = 1;
			} else {
				relais = 0;
			}
			
			numVille++;
			
			System.out.println("relais : " + relais);
			if(nom_agence != "") {
				
				this.setData(cp, String.valueOf(NbVilles), String.valueOf(numVille), villeSoumise,
						String.valueOf(NbAgences), uriVille, nom_agence, adresse, tel, fax,
						HorairesHTM, HorairesTXT, Services, coord, agences_HTM);
			}
			
			System.out.println("\n---------------------\n");
		}
		
	}

}
