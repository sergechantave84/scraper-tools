package com.app.service;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.SocketTimeoutException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Scanner;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.openqa.selenium.By;
import org.openqa.selenium.PageLoadStrategy;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.app.model.Savoie_Outre_Mer;
import com.app.scrap.RepSavoieOutreMer;
import com.app.utils.BrowserUtils;
import com.app.utils.Parametres;
import com.app.utils.Utilitaires;

import fa.browserUtils.ChromeBrowser;
import fa.browserUtils.FactoryBrowser;
import fa.log.LogApp.LogScrap;
import fa.log.config.LogConst;

@Service
public class ServiceSavoie_Outre_Mer extends Utilitaires {

	static String driverPath = "C:\\ChromeDriver\\";

	String nomSITE = "BQ_SAVOIE";
	String ficval = Parametres.OUTRE_MER_NOM_FICHIER;
	String dossierRESU = Parametres.REPERTOIRE_RESULT + "\\BQ\\" + nomSITE;
	String url_AVTDEP = "http://agences.banquepopulaire.fr/bpgen/list.asp?dept=";
	String url_APSDEP = "&dataMask=______&typeId=BFBP_BS&env_info=10548";
	String url_debutAgence = "http://agences.banquepopulaire.fr/bpgen";
	String orl = "https://agence.banque-de-savoie.fr";
	String fichier_valeurs;
	String DepSoumis;
	String ligne;

	int num_ligne = 2;
	int NbAgences;
	int numreponse = 1;
	int nbRecuperes;
	int num_ligneficval;
	int parcoursficval;
	int Resultats;

	ChromeDriver chromeDriver;
	BufferedWriter sortie;
	BufferedWriter sortie_log;
	private static HtmlUnitDriver htmlUnitDriver;

	Date Temps_Fin;

	@Autowired
	RepSavoieOutreMer rep;

	public Savoie_Outre_Mer saveData(Savoie_Outre_Mer bqSavoie) {

		return rep.save(bqSavoie);
	}

	public List<Savoie_Outre_Mer> saveDataAll(List<Savoie_Outre_Mer> adreas) {

		return rep.saveAll(adreas);
	}

	public List<Savoie_Outre_Mer> getAll() {
		return rep.findAll();
	}

	public Savoie_Outre_Mer getAdreaById(int id) {

		return rep.findById(id).orElse(null);
	}

	public String deleteProduct(int id) {

		try {
			rep.deleteById(id);

		} catch (Exception e) {
			e.printStackTrace();
		}

		return "removed";

	}

	public List<Savoie_Outre_Mer> showFirstRows() {
		List<Savoie_Outre_Mer> a = rep.getFirstRow(100, Savoie_Outre_Mer.class);
		return a;
	}

	public List<Savoie_Outre_Mer> showLastRows() {
		List<Savoie_Outre_Mer> a = rep.getLastRow(100, Savoie_Outre_Mer.class);
		return a;
	}

	public Savoie_Outre_Mer setData(String depSoumis, String nbAgences, String enseigne, String nomWeb, String adresse,
			String tel, String fax, String horairesHtm, String horaires, String services, String dab,
			String agenceHtm) {
		Savoie_Outre_Mer bqSavoie = new Savoie_Outre_Mer();
		bqSavoie.setDepSoumis(depSoumis).setNbAgences(nbAgences).setEnseigne(enseigne).setNomWeb(nomWeb)
				.setAdresse(adresse).setTel(tel).setFax(fax).setHorairesHtm(horairesHtm).setHoraires(horaires)
				.setServices(services).setDab(dab).setAgenceHtm(agenceHtm);
		this.saveData(bqSavoie);

		return bqSavoie;
	}

	public void setUp() throws Exception {
		FactoryBrowser factBrowser = new FactoryBrowser();
		ChromeBrowser b = (ChromeBrowser) factBrowser.create("chrome");
		final String binaryPath = "C:\\Program Files\\Google\\Chrome Beta\\Application\\chrome.exe";
		chromeDriver = b.setOptions(PageLoadStrategy.NORMAL, driverPath, 60000, 60000, 60000, false, false, binaryPath)
				.initBrowserDriver();
		// htmlUnitDriver = (HtmlUnitDriver) this.setupHU();
		// chromeDriver = BrowserUtils.chromeConstruct(driverPath, 60000, 60000, 10,
		// PageLoadStrategy.NORMAL);
	}

	public void scrapBqSavoie() throws IOException, InterruptedException {

		String line = "";
		List<String> paths = new ArrayList<>();
		boolean isPageLoaded = false;
		fichier_valeurs = Parametres.REPERTOIRE_DATA_SOURCES + ficval + ".txt";
		Scanner sc = new Scanner(new BufferedInputStream(new FileInputStream(new File(fichier_valeurs))));

		if (!new File(dossierRESU).exists()) {
			new File(dossierRESU).mkdirs();
		}

		LogScrap log = new LogScrap("BanqueSavoie_dom.txt", LogConst.TYPE_LOG_FILE);
		num_ligneficval = ((new File(LogConst.STORE_LOG_PATH + LogConst.PREFIX_LOG_FILE + "_BanqueSavoie_dom.txt"))
				.exists()) ? Integer.parseInt(log.readLog().getLastLineProcessed().split("\\t")[4]) : 1;


		parcoursficval = 1;
		while (sc.hasNextLine()) {
			line = sc.nextLine();
			if (parcoursficval > num_ligneficval) {
				String[] values = line.split("\\t");
				nbRecuperes = 0;
				DepSoumis = values[0];
				// String uri = url_AVTDEP + DepSoumis + url_APSDEP;
				String uri = orl;
				try {
					// System.out.println("uri : "+uri);
					chromeDriver.get(uri);

				} catch (Exception e) {
					if (e instanceof SocketTimeoutException) {
						System.out.println(uri);
						chromeDriver.get(uri);
					}
				}
				isPageLoaded = this.listensIfPageFullyLoaded(chromeDriver);
				if (isPageLoaded) {

					Thread.sleep(2000);
					WebElement btn_cab = chromeDriver.findElement(By.cssSelector("#mdc-tab-1"));
					btn_cab.click();

					WebElement input = chromeDriver.findElement(By.cssSelector("#em-search-form__searchcity"));
					input.sendKeys(DepSoumis);

					Thread.sleep(2000);

					WebElement btnSearch = chromeDriver.findElement(By.cssSelector(
							"#em-search-form > div > div.em-search-form__fieldset-wrapper > fieldset.em-search-form__fieldset.em-search-form__buttons > div:nth-child(2) > button"));
					btnSearch.click();
					
					Thread.sleep(5000);

					String pageSource = "";
					pageSource = chromeDriver.getPageSource();
					Document doc = Jsoup.parse(pageSource);
					Elements rootListAgencies = doc.select(
							"#body > div.em-page > main > div.em-results-wrapper > div > div.em-results__list-wrapper > div > div > div.jspPane > ul > li");
					NbAgences = rootListAgencies.size();

					if (NbAgences > 0) {

						if (!paths.isEmpty()) {
							paths.clear();
						}
						for (Element tmp : rootListAgencies) {
							Element href = tmp.selectFirst("h2 > a");
							String strHref = href.attr("href");
							paths.add(strHref);
						}

						for (String strTmp : paths) {
							try {
								System.out.println("uri : https://agence.banque-de-savoie.fr" + strTmp);
								chromeDriver.get("https://agence.banque-de-savoie.f" + strTmp);

							} catch (Exception e) {
								if (e instanceof SocketTimeoutException) {
									chromeDriver.get("https://agence.banque-de-savoie.fr" + strTmp);

								}
							}

							Thread.sleep(5000);
							String agences_HTM = "", nom_agence = "", adresse = "", tel = "", fax = "",
									HorairesHTM = "", HorairesTXT = "", Services = "", enseigne = "";
							int total = 0, DAB = 0;
							total = NbAgences;
							pageSource = htmlUnitDriver.getPageSource();
							doc = Jsoup.parse(pageSource);

							// TODO change
							Element root2JElement = doc
									.select("div.em-page > main > div.em-details > div.em-details__poi-card").first();
							agences_HTM = root2JElement.outerHtml().trim().replaceAll("\\s{1,}", "");
							agences_HTM = this.normalize(agences_HTM);

							// retrieve agency name
							Element agencyNameJElement = root2JElement.select("h2.em-details__label").first();
							nom_agence = (agencyNameJElement != null) ? this.normalize(agencyNameJElement.text()) : "";
							System.out.println("nom agence : " + nom_agence);

							// retrieve agency address
							Element agencyAddressJElement = root2JElement
									.select("div.em-details__info>div:nth-child(1)>div.em-details__address>div")
									.first();
							adresse = (agencyAddressJElement != null) ? this.normalize(agencyAddressJElement.text())
									: "";
							System.out.println("adresse : " + adresse);

							// retrieve agency tel
							Element agencyTelJElement = root2JElement.select("div.em-details__info>div:nth-child(2)>a")
									.first();
							tel = (agencyTelJElement != null) ? agencyTelJElement.attr("href").replaceAll("[^0-9]", "")
									: "";
							System.out.println("telephone : " + tel);

							// retrieve agency fax
							Element agencyFaxJElement = root2JElement
									.select("div.em-details__info>div:nth-child(2)>div>span").first();
							fax = (agencyFaxJElement != null) ? agencyFaxJElement.text().replaceAll("[^0-9]", " ") : "";
							System.out.println("fax : " + fax);

							// retrieve agency schedule
							Element agencyScheduleJElement = doc
									.selectFirst("head > script[type=\"application/ld+json\"]");
							// System.out.println(agencyScheduleJElement);
							String agencyInfo = agencyScheduleJElement.html();
							try {

								// ADD BY NANTE
								Element emPage = doc.selectFirst("div.em-page");
								Element script = emPage.nextElementSibling();
								/*
								 * Elements agencySheduleJElement = root2JElement.select(
								 * "div.em-details__horaires-bloc > div > div.em-graphical-schedules > div > ul.graphicalSchedules__days > li"
								 * );
								 */

								String[] splitScripts = (script.html().toString()).split("day1");

								String days = splitScripts[1].replaceAll("/", "").replaceAll("\"", "")
										.replace("day2:", "").replace("day3:", "").replace("day4:", "")
										.replace("day5:", "").replace("day6:", "");

								// System.out.println("day1 : " + days);

								String[] listHoraire = days.split(",");

								String lundi = listHoraire[0].contains("h")
										? listHoraire[0].replaceAll("day1 : :", "").replaceAll(":", "")
										: "Fermee";
								String mardi = listHoraire[1].contains("h") ? listHoraire[1] : "Fermee";
								String mercredi = listHoraire[2].contains("h") ? listHoraire[2] : "Fermee";
								String jeudi = listHoraire[3].contains("h") ? listHoraire[3] : "Fermee";
								String vendredi = listHoraire[4].contains("h") ? listHoraire[4] : "Fermee";
								String samedi = listHoraire[5].contains("h") ? listHoraire[5] : "Fermee";

								lundi = "Lundi : " + lundi;
								mardi = "Mardi : " + mardi;
								mercredi = "Mercredi : " + mercredi;
								jeudi = "Jeudi : " + jeudi;
								vendredi = "Vendredi : " + vendredi;
								samedi = "Samedi : " + samedi;

								String dim = " Dimanche : Ferme";

								/*
								 * for (Element tmp : agencySheduleJElement) { HorairesTXT += tmp.text() + " ";
								 * } HorairesTXT = this.normalize(HorairesTXT.replaceAll("[^0-9a-zA-z ]", " "));
								 */

								HorairesTXT = lundi + " " + mardi + " " + mercredi + " " + jeudi + " " + vendredi + " "
										+ samedi + dim;
								HorairesTXT = this.normalize(HorairesTXT);
								System.out.println("Horaires : " + HorairesTXT);

							} catch (Exception e) {
								e.printStackTrace();
							}

							// retrieve agency services

							Elements agencyServiceJElement = doc
									.select("div.em-page > main > div.em-details__services-bloc.bloc-bp>ul>li");
							if (agencyServiceJElement != null) {
								for (Element elementTmp : agencyServiceJElement) {
									Services += elementTmp.text() + " ";
								}
								if (Services.contains("Distributeur automatique de billets")) {
									DAB = 1;
								}
							} else {
								Services = "";
								DAB = 0;
							}
							System.out.println("DAB " + DAB);
							Services = this.normalize(Services);
							System.out.println("services " + Services);
							// retrieve agency tag
							Element agencyTagJElement = root2JElement.select("#aside > div > div > a.agencyreseau")
									.first();
							enseigne = (agencyTagJElement != null) ? agencyTagJElement.text() : "";

							nbRecuperes++;
							
							this.setData(DepSoumis, String.valueOf(NbAgences), enseigne, nom_agence,
							 adresse, tel, fax, HorairesHTM, HorairesTXT, Services, String.valueOf(DAB),
							 agences_HTM);
							 
						}

					} else {
						System.out.println("AUCUNE AGENCE TROUVEE");
					}
				}
				log.writeLog(new Date(), DepSoumis, "no_error", NbAgences, parcoursficval, NbAgences - nbRecuperes);
			}
			parcoursficval++;
		}
	}

	public String getText(String subject, String firstStr, String lastStr) {
		int beginIndex = subject.indexOf(firstStr);
		int lastIndex = subject.indexOf(lastStr);
		String strCuted = subject.substring(beginIndex, lastIndex);
		return strCuted;
	}

	public void enregistrer(String agences_HTM, String nom_agence, String adresse, String tel, String fax,
			String HorairesHTM, String HorairesTXT, String Services, String enseigne, int DAB) {

		try {

			sortie = new BufferedWriter(new FileWriter(fichier, true));
			sortie.write(DepSoumis + "\t" + NbAgences + "\t" + enseigne + "\t" + nom_agence + "\t" + adresse + "\t"
					+ tel + "\t" + fax + "\t" + HorairesHTM + "\t" + HorairesTXT + "\t" + Services + "\t" + DAB + "\t"
					+ agences_HTM + "\r\n");

			sortie.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public void enregistrer_journal(int numreponse) {

//	   	this.fichier = new File(Parametres.REPERTOIRE + "RESULTATS_BNP.");
		try {
			Temps_Fin = new Date();
			SimpleDateFormat formater = null;
			formater = new SimpleDateFormat("'le' dd/MM/yyyy '�' hh:mm:ss");
			String date = formater.format(Temps_Fin);
			date = formater.format(Temps_Fin);

			sortie_log = new BufferedWriter(new FileWriter(log, true));
			sortie_log.write(parcoursficval + ";" + "FinValeur" + ";" + DepSoumis + ";" + this.NbAgences + ";"
					+ nbRecuperes + ";" + (this.NbAgences - nbRecuperes) + ";" + date + "\r\n");
			sortie_log.close();
			long taille_fichier = fichier.length() / 1000000; // la length est en octets, on divise par 1000 pour mettre
																// en Ko et par 1000000 pour mettre en Mo

			if (taille_fichier > 8) {
				// INITIALISATION FICHIER DE RESULTAT
				this.fichier = new File(
						dossierRESU + "\\" + "RESULTATS_" + nomSITE + "_" + System.currentTimeMillis() + ".txt");
				// Attention pas d'accent dans les noms de colonnes !
				sortie = new BufferedWriter(new FileWriter(fichier, true));
				sortie.write("DepSoumis" + "\t" + "NbAgences" + "\t" + "ENSEIGNE" + "\t" + "NOMWEB" + "\t" + "ADRESSE"
						+ "\t" + "TEL" + "\t" + "FAX" + "\t" + "HORAIRESHTM" + "\t" + "HORAIRESTXT" + "\t" + "SERVICES"
						+ "\t" + "DAB" + "\t" + "AGENCEHTM" + "\r\n");
				sortie.close();
			}

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	/**
	 * La m�thode <code>initialiser()</code> permet d'ajouter la ligne d'ent�te dans
	 * le fichier r�sultat.
	 */

	public void initialiser() {

		try {
			// Attention pas d'accent dans les noms de colonnes !
			sortie = new BufferedWriter(new FileWriter(fichier, true));
			sortie.write("DepSoumis" + "\t" + "NbAgences" + "\t" + "ENSEIGNE" + "\t" + "NOMWEB" + "\t" + "ADRESSE"
					+ "\t" + "TEL" + "\t" + "FAX" + "\t" + "HORAIRESHTM" + "\t" + "HORAIRESTXT" + "\t" + "SERVICES"
					+ "\t" + "DAB" + "\t" + "AGENCEHTM" + "\r\n");
			sortie.close();

			File toto = new File(dossierRESU + "\\" + "LOG_" + nomSITE + ".csv");
			if (!toto.exists()) {
				// if (num_ligneficval == num_ligne) {
				sortie_log = new BufferedWriter(new FileWriter(log, true));
				sortie_log.write("NumLigne" + ";" + "FinValeur" + ";" + "DepSoumis" + ";" + "NbAgences" + ";"
						+ "Nbrecuperes" + ";" + "Reste" + ";" + "Heure_Fin" + "\r\n");
				sortie_log.close();
			}

		} catch (IOException e) {

			e.printStackTrace();
		}
	}

	public void tearDowns() {
		chromeDriver.quit();
	}
}
