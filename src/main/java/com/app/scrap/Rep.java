package com.app.scrap;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import com.app.model.Adrea;

@Repository
public interface Rep extends JpaRepository<Adrea,Integer>, RepositoryCustom<Adrea> {
}