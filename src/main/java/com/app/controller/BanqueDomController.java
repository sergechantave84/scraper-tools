package com.app.controller;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.app.service.ServiceActionLogement_DOM;
import com.app.service.ServiceBCP_Outre_Mer;
import com.app.service.ServiceBNP_Outre_Mer;
import com.app.service.ServiceBP_GAB_Outre_Mer;
import com.app.service.ServiceBP_Outre_Mer;
import com.app.service.ServiceBanqueCaraibes;
import com.app.service.ServiceBcfoi;
import com.app.service.ServiceCA_Outre_Mer;
import com.app.service.ServiceCDN_GAB_Outre_Mer;
import com.app.service.ServiceCDN_Outre_Mer;
import com.app.service.ServiceCE_GAB_Outre_Mer;
import com.app.service.ServiceCE_Outre_Mer;
import com.app.service.ServiceCIC_GAB_Outre_Mer;
import com.app.service.ServiceCIC_Outre_Mer;
import com.app.service.ServiceCM_GAB_Outre_Mer;
import com.app.service.ServiceCM_Outre_Mer;
import com.app.service.ServiceCasden_Outre_Mer;
import com.app.service.ServiceCcoop_Outre_Mer;
import com.app.service.ServiceHsbc_Outre_Mer;
import com.app.service.ServiceLcl_Outre_Mer;
import com.app.service.ServicePalatine_Outre_Mer;
import com.app.service.ServiceSavoie_Outre_Mer;

@RestController
@RequestMapping("/scrap")
public class BanqueDomController {

	@Autowired
	ServiceBcfoi srvBCFOI;
	
	@Autowired
	ServiceBCP_Outre_Mer srvBCP;
	
	@Autowired
	ServiceBNP_Outre_Mer srvBNP;
	@Autowired
	ServiceBP_Outre_Mer srvBP;
	
	@Autowired
	ServiceBP_GAB_Outre_Mer srvBP_GAB;
	
	@Autowired
	ServiceCA_Outre_Mer srvCA;
	
	@Autowired
	ServiceCasden_Outre_Mer srvCasdebn;
	
	@Autowired
	ServiceCDN_GAB_Outre_Mer srvCDN_GAB;
	
	@Autowired
	ServiceCDN_Outre_Mer srvCDN;
	
	@Autowired
	ServiceCE_Outre_Mer srvCE;
	
	@Autowired
	ServiceCE_GAB_Outre_Mer srvCE_GAB;
	
	@Autowired
	ServiceCcoop_Outre_Mer srvCCOOP;
	
	@Autowired
	ServiceCIC_GAB_Outre_Mer srvCIC_GAB;
	
	@Autowired
	ServiceCIC_Outre_Mer srvCIC;
	
	@Autowired
	ServiceCM_GAB_Outre_Mer srvCM_GAB;
	
	@Autowired
	ServiceCM_Outre_Mer srvCM;
	
	@Autowired
	ServiceHsbc_Outre_Mer srvHSBC;
	
	@Autowired
	ServiceLcl_Outre_Mer srvLCL;
	
	@Autowired
	ServicePalatine_Outre_Mer srvPalatine;
	
	@Autowired
	ServiceSavoie_Outre_Mer srvSavoie;
	
	@Autowired
	ServiceActionLogement_DOM srvActlogDom;
	
	@Autowired
	private ServiceBanqueCaraibes srvBC;
	
	
	@RequestMapping("/BC")
	public void setBC() throws Exception {
		srvBC.scrap();
	}
	
	@RequestMapping("/BCFOI")
	public void setBCFOI() throws IOException, InterruptedException {
		srvBCFOI.setup();
		srvBCFOI.scrap();
		//srvBCFOI.tearsDowns();
	}
	
	@RequestMapping("/BCP_DOM")
	public void setBCPDOM() throws IOException {
		srvBCP.setup();
		srvBCP.scrapBCP_Outre_Mer();
		srvBCP.tearDown();
	}
	@RequestMapping("/BNP_DOM")
	public void setBNPDOM() throws Exception {
		
		srvBNP.scrap();
		//srvBNP.deleteAll();
		
	}
	@RequestMapping("/BP_DOM")
	public void setBPDOM() throws Exception {
		srvBP.setup();
		srvBP.scrap();
		srvBP.tearDown();
		
	}
	@RequestMapping("/BP_GAB_DOM")
	public void setBP_GABDOM() throws IOException, InterruptedException {
		srvBP_GAB.setup();
		srvBP_GAB.scrapBPGAB();
		srvBP_GAB.tearDown();
	}
	@RequestMapping("/CA_DOM")
	public void setCADOM() throws Exception {
		srvCA.setup();
		srvCA.scrap();
		srvCA.tearDown();
	}
	@RequestMapping("/CASDEN_DOM")
	public void setCasdenDOM() throws Exception {
		srvCasdebn.setup();
		srvCasdebn.scrap();
		srvCasdebn.tearDown();
	}
	@RequestMapping("/CDN_GAB_DOM")
	public void setCDN_GABDOM() throws IOException, InterruptedException {
		srvCDN_GAB.setup();
		srvCDN_GAB.scrapCDNGAb();
		srvCDN.tearsDown();
	}
	@RequestMapping("/CDN_DOM")
	public void setCDNDOM() throws IOException, InterruptedException {
		srvCDN.setup();
		srvCDN.scrapCDN_Outre_Mer();
		srvCDN.tearsDown();
	}
	@RequestMapping("/CE_GAB_DOM")
	public void setCE_GABDOM() throws Exception {
		srvCE_GAB.setup();
		srvCE_GAB.scrap();
		srvCE_GAB.tearDown();
	}
	@RequestMapping("/CE_DOM")
	public void setCEDOM() throws IOException, InterruptedException {
		srvCE.setup();
		srvCE.scrap();
		srvCE.tearDown();
	}
	@RequestMapping("/CCOOP_DOM")
	public void setCcoopDOM() throws IOException {
		srvCCOOP.setup();
		srvCCOOP.scrapCCOOP_Outre_Mer();
		srvCCOOP.tearsDown();
	}
	
	@RequestMapping("/CIC_GAB_DOM")
	public void setCIC_GABDOM() throws IOException {
		srvCIC_GAB.setup();
		srvCIC_GAB.scrap();
		srvCIC_GAB.tearsDown();
	}
	
	@RequestMapping("/CIC_DOM")
	public void setCICDOM() throws IOException {
		srvCIC.setup();
		srvCIC.scrapCIC_Outre_Mer();
		srvCIC.tearsDown();
	}
	
	@RequestMapping("/CM_DOM")
	public void setCMDOM() throws IOException {
		srvCM.setup();
		srvCM.scrapCM_Outre_Mer();
		srvCM.tearsDown();
	}
	@RequestMapping("/CM_GAB_DOM")
	public void setCM_GABDOM() throws IOException {
		srvCM_GAB.setup();
		srvCM_GAB.scrapCMGab();
		srvCM_GAB.tearsDown();
	}
	@RequestMapping("/HSBC_DOM")
	public void setHsbcDOM() throws IOException, InterruptedException {
		srvHSBC.setup();
		srvHSBC.scrapHSBC_Outre_Mer();
		srvHSBC.tearDown();
	}
	@RequestMapping("/LCL_DOM")
	public void setLCLDOM() throws IOException, InterruptedException {
		srvLCL.setup();
		srvLCL.letScrapLCL();
		srvLCL.tearDown();
	}
	@RequestMapping("/PALATINE_DOM")
	public void setPalatineDOM() throws Exception {
		
		srvPalatine.scrap();
	
	}
	@RequestMapping("/SAVOIE_DOM")
	public void setSavoieDOM() throws Exception {
		srvSavoie.setUp();
		srvSavoie.scrapBqSavoie();
		srvSavoie.tearDowns();
	}
	
	@RequestMapping("/ACTION_LOGEMENT_DOM")
	public void setActLogDOM() throws Exception {
		srvActlogDom.setup();
		srvActlogDom.scrapActionlogement();
	}
}
