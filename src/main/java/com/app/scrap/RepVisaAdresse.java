package com.app.scrap;

import org.springframework.data.jpa.repository.support.JpaRepositoryImplementation;
import org.springframework.stereotype.Repository;

import com.app.model.Visa_adresse;

@Repository
public interface  RepVisaAdresse extends JpaRepositoryImplementation<Visa_adresse, Integer>, RepositoryCustom<Visa_adresse>{

}
