package com.app.scrap;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.app.model.MeilleurTaux;

@Repository
public interface RepMeilleurTaux extends JpaRepository<MeilleurTaux, Integer> {

}
