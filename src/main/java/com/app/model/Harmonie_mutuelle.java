package com.app.model;

import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "harmonie_mutelle", schema = "scrap")
public class Harmonie_mutuelle {

	@Id
	@SequenceGenerator(name = "scrap.harmonie_mutelle_id_seq", sequenceName = "scrap.harmonie_mutelle_id_seq", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "scrap.harmonie_mutelle_id_seq")

	@Column(name = "id", updatable = false)
	private int id;

	@Column(name = "nom_agence", columnDefinition = "varchar")
	private String nomAgence;

	@Column(name = "adresse", columnDefinition = "varchar")
	private String adresse;

	@Column(name = "tel", columnDefinition = "varchar")
	private String tel;

	@Column(name = "fax", columnDefinition = "varchar")
	private String fax;

	@Column(name = "horaires", columnDefinition = "varchar")
	private String horaires;

	@Column(name = "agencehtm", columnDefinition = "varchar")
	private String agenceHtm;

	@Column(name = "annee_du_scrap", columnDefinition = "varchar(4) DEFAULT date_part('year',CURRENT_DATE)")
	private String anneeScrap;

	public Harmonie_mutuelle() {
		LocalDate ld = new Date().toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
		anneeScrap = String.valueOf(ld.getYear() + 1);
	}

	public int getId() {
		return id;
	}

	public Harmonie_mutuelle setId(int id) {
		this.id = id;
		return this;
	}

	public String getNomAgence() {
		return nomAgence;
	}

	public Harmonie_mutuelle setNomAgence(String nomAgence) {
		this.nomAgence = nomAgence;
		return this;
	}

	public String getAdresse() {
		return adresse;
	}

	public Harmonie_mutuelle setAdresse(String adresse) {
		this.adresse = adresse;
		return this;
	}

	public String getTel() {
		return tel;
	}

	public Harmonie_mutuelle setTel(String tel) {
		this.tel = tel;
		return this;
	}

	public String getFax() {
		return fax;
	}

	public Harmonie_mutuelle setFax(String fax) {
		this.fax = fax;
		return this;
	}

	public String getHoraires() {
		return horaires;
	}

	public Harmonie_mutuelle setHoraires(String horaires) {
		this.horaires = horaires;
		return this;
	}

	public String getAgenceHtm() {
		return agenceHtm;
	}

	public Harmonie_mutuelle setAgenceHtm(String agenceHtm) {
		this.agenceHtm = agenceHtm;
		return this;
	}

	public String getAnneeScrap() {
		return anneeScrap;
	}

	public Harmonie_mutuelle setAnneeScrap(String anneeScrap) {
		this.anneeScrap = anneeScrap;
		return this;
	}
}
