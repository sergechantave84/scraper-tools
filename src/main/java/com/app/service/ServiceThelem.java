package com.app.service;

import java.io.BufferedInputStream;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.net.SocketTimeoutException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Scanner;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.openqa.selenium.By;
import org.openqa.selenium.PageLoadStrategy;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.app.model.Thelem;

import com.app.scrap.RepThelem;
import com.app.utils.BrowserUtils;
import com.app.utils.Parametres;
import com.app.utils.Utilitaires;

import fa.browserUtils.FactoryBrowser;
import fa.browserUtils.HtmlUnitBrowser;
import fa.log.LogApp.LogScrap;
import fa.log.config.LogConst;

@Service
public class ServiceThelem extends Scraper {

	@Autowired
	private RepThelem rep;

	private ChromeDriver ch;
	// private ChromeDriver ch2;

	HtmlUnitDriver htmlUnitDriver;

	private List<com.app.model.Thelem> thelemList = new ArrayList<>();

	public Thelem setData(String nbAgences, String lienAgence, String nomAgence, String adresse, String tel, String fax,
			String nomsAgents, String orias, String horaires, String agenceHtm) {
		Thelem thelem = new Thelem();
		thelem.setNbAgences(nbAgences).setLienAgence(lienAgence).setNomAgence(nomAgence).setAdresse(adresse).setTel(tel)
				.setFax(fax).setNomsAgents(nomsAgents).setOrias(orias).setHoraires(horaires).setAgenceHtm(agenceHtm);
		rep.save(thelem);
		return thelem;
	}

	public List<Thelem> showFirstRows() {
		List<Thelem> a = rep.getFirstRow(100, Thelem.class);
		return a;
	}

	public List<Thelem> showLastRows() {
		List<Thelem> a = rep.getLastRow(100, Thelem.class);
		return a;
	}

	public void setup() {

		FactoryBrowser factBrowser = new FactoryBrowser();
		HtmlUnitBrowser hB = (HtmlUnitBrowser) factBrowser.create("htmlUnit");
		htmlUnitDriver = hB.setOptions(true, false, false, 60000).initBrowserDriver();

		final String chromeBeta = "C:\\Program Files\\Google\\Chrome Beta\\Application\\chrome.exe";

		ch = (ChromeDriver) this.setupCh(Parametres.CHROME_DRIVER_PATH, false, false, null, null, chromeBeta);
		// ch2 = (ChromeDriver) this.setupCh(Parametres.CHROME_DRIVER_PATH, false,
		// false, null , null ,chromeBeta);
	}

	@Override
	public void scrap() throws IOException, InterruptedException {

		String ficval = "lien_thelem";
		String urlThelemAgences = "https://www.thelem-assurances.fr/agences/";
		String fichier_valeurs = Parametres.REPERTOIRE_DATA_SOURCES + ficval + ".txt";
		Scanner sc = new Scanner(new BufferedInputStream(new FileInputStream(new File(fichier_valeurs))));

		int agencyNumbers = 0;
		String pageSource = "";

		String lienAgences = "";
		String nomAgence = "";
		String adresse = "";
		String telephone = "";
		String fax = "";
		String nomAgents = "";
		String orias = "";
		String horaires = "";
		String agenceHTML = "";
		long timeMillis = System.currentTimeMillis();

		Document doc = null;

		LogScrap log = new LogScrap("AssuranceThelem.txt", LogConst.TYPE_LOG_FILE);
		int num_ligneficval = ((new File(LogConst.STORE_LOG_PATH + LogConst.PREFIX_LOG_FILE + "_AssuranceThelem.txt"))
				.exists()) ? num_ligneficval = Integer.parseInt(log.readLog().getLastLineProcessed().split("\\t")[4])
						: 1;

		Element agencyNumberElmnt = null;
		Element agencyRoot = null;
		Element agencyNameElmnt = null;
		Element agencyAddressElmnt = null;
		Element agencyTelElmnt = null;
		Element agencyAgentsElmnt = null;
		Elements agencyScheduleElmnt = null;
		Element agencyOriasElement = null;
		int currentLine = 1;
		while (sc.hasNextLine()) {

			String line = sc.nextLine();

			if (currentLine > num_ligneficval) {
				String url = urlThelemAgences + line;
				System.out.println(url);
				
				ch.get(url);
				
				try {
					pageSource = ch.getPageSource();

					Thread.sleep(2000);

					doc = Jsoup.parse(pageSource);

					List<WebElement> links = ch.findElements(By.cssSelector(
							"#main > div.agences__content__shadow.mobile > div > div:nth-child(1) > div > div.ss-wrapper > div > a"));
					
					for (WebElement tmp : links) {
						
						String path = tmp.getAttribute("href");

						lienAgences = "";
						nomAgence = "";
						adresse = "";
						telephone = "";
						fax = "";
						nomAgents = "";
						orias = "";
						horaires = "";
						agenceHTML = "";

						lienAgences = path;
						try {
							System.out.println("lienAgences : " + lienAgences);
							htmlUnitDriver.get(lienAgences);
						} catch (Exception e) {
							if (e instanceof SocketTimeoutException) {
								htmlUnitDriver.get(lienAgences);
							}
						}
						pageSource = htmlUnitDriver.getPageSource();

						doc = Jsoup.parse(pageSource);

						agencyRoot = doc
								.selectFirst("#post-2 > div > div > div.column.small-12.large-7.agence-page__data");
						if (agencyRoot != null) {
							agenceHTML = this.normalize(agencyRoot.outerHtml().replaceAll("\\s{1,}", ""));

							// retrieve agency name

							agencyNameElmnt = agencyRoot
									.selectFirst("div.agence-page__data--store > div.agence-page__data--name > h1");
							nomAgence = (agencyNameElmnt != null)
									? this.normalize(agencyNameElmnt.text().replaceAll("\\s{2,}", " ").trim())
									: "";
							System.out.println("nom : " + nomAgence);
							// retrieve agency Address

							agencyAddressElmnt = agencyRoot
									.selectFirst("div.agence-page__data--store > div.agence-page__data--address");
							adresse = (agencyAddressElmnt != null) ? this.normalize(agencyAddressElmnt.text()) : "";
							System.out.println("adresse : " + adresse);
							// retrieve agency schedule

							// agencyScheduleElmnt = agencyRoot.select("#today > table > tbody > tr");
							agencyScheduleElmnt = agencyRoot.select("#horaire_full > table > tbody > tr");
							if (agencyScheduleElmnt != null) {
								for (Element tmp1 : agencyScheduleElmnt) {
									horaires += tmp1.text() + " ";
								}
							} else {
								horaires = "";
							}
							horaires = this.normalize(horaires.replaceAll("\\s{2,}", " ").trim());
							System.out.println("horaires : " + horaires);
							// retrieve agency telephone
							agencyTelElmnt = agencyRoot.selectFirst("#tel");
							telephone = (agencyTelElmnt != null)
									? (agencyTelElmnt.attr("href").replaceAll("tel:", "").trim())
									: "";
							System.out.println("tel : " + telephone);
							// retrieve agency agent name
							agencyAgentsElmnt = agencyRoot.selectFirst(
									"div.agence-page__data--agents > div:nth-child(1) > div.small-12.large-6.text-left > div > span.agence-page__agents--name");
							nomAgents = (agencyAgentsElmnt != null)
									? this.normalize(agencyAgentsElmnt.text().replaceAll("\\s{2,}", "").trim())
									: "";

							// retrieve orias number
							agencyOriasElement = agencyRoot.selectFirst(
									"div.agence-page__data--agents > div:nth-child(1) > div.small-12.large-6.text-left > div > span.agence-page__agents--text");
							orias = (agencyOriasElement != null)
									? (agencyOriasElement.text().replaceAll("\\s{1,}", "").trim())
									: "";
							orias = orias.replaceAll("[^0-9 ]", "");
							System.out.println("orias : " + orias);

							System.out.println("-------------------------------------\n");
						}
						this.setData(String.valueOf(agencyNumbers), lienAgences, nomAgence, adresse, telephone, fax,
								nomAgents, orias, horaires, agenceHTML);
					}

				} catch (Exception e) {
					e.printStackTrace();
				}

				log.writeLog(new Date(), line, "no_error", agencyNumbers, currentLine, 0);
			}

			currentLine++;

		}

	}

	public void tearsDown() {
		System.out.println("scrap finis");
		htmlUnitDriver.quit();
	}

	@Override
	public void saveItem(String... args) {
		// TODO Auto-generated method stub

	}
}
