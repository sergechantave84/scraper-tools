package com.app.service;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;

import java.nio.charset.StandardCharsets;
import java.text.Normalizer;
import java.util.Date;
import java.util.Scanner;


import org.apache.commons.io.FileUtils;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.app.model.BNP_Outre_Mer;
import com.app.scrap.RepBnpOutreMer;
import com.app.utils.Parametres;

import fa.log.LogApp.LogScrap;
import fa.log.config.LogConst;

@Service
public class ServiceBNP_Outre_Mer extends Scraper {

	@Autowired
	private RepBnpOutreMer rep;

	private String nomSITE = "BNP_Outre_Mer";
	private String ficval = "lien_bnp_outre_mer";
	private String dossierRESU = Parametres.REPERTOIRE_RESULT + "\\BQ\\" + nomSITE;

	private String fichier_valeurs;

	private String ValeurSoumise;
	// private String ligne;

	private int nbAgences;
	private int nbRecuperes;
	private int num_ligneficval;

	public BNP_Outre_Mer setData(String valeurSoumise, String nbAgences, String nomWeb, String formatAgence,
			String adresse, String adresseComp, String cp, String ville, String tel, String fax, String horaires,
			String gab, String particulier, String professionel, String entreprise, String change, String coffreFort,
			String depotPermanent, String automateBillet, String automateCheque, String latitude, String longitude) {
		BNP_Outre_Mer bnp = new BNP_Outre_Mer();
		bnp.setValeurSoumise(valeurSoumise).setNbAgences(nbAgences).setNomWeb(nomWeb).setFormatAgence(formatAgence)
				.setAdresse(adresse).setAdresseComp(adresseComp).setCp(cp).setVille(ville).setTel(tel).setFax(fax)
				.setHoraires(horaires).setGab(gab).setParticulier(particulier).setProfessionel(professionel)
				.setEntreprise(entreprise).setChange(change).setCoffreFort(coffreFort).setDepotPermanent(depotPermanent)
				.setAutomateBillet(automateBillet).setAutomateCheque(automateCheque).setLatitude(latitude)
				.setLongitude(longitude);
		rep.save(bnp);
		return bnp;
	}

	@Override
	public void scrap() throws Exception {
		fichier_valeurs = Parametres.REPERTOIRE_DATA_SOURCES + ficval + ".txt";
		Scanner sc = new Scanner(new BufferedInputStream(new FileInputStream(new File(fichier_valeurs))));

		String fichier_val_json = Parametres.REPERTOIRE_DATA_SOURCES + "bnp_dom.json";

		File file_bnp = new File(fichier_val_json);

		// System.out.println(content);

		LogScrap log = new LogScrap("BanqueBnp_dom.txt", LogConst.TYPE_LOG_FILE);
		num_ligneficval = ((new File(LogConst.STORE_LOG_PATH + LogConst.PREFIX_LOG_FILE + "_BanqueBnp_dom.txt"))
				.exists()) ? Integer.parseInt(log.readLog().getLastLineProcessed().split("\\t")[4]) : 1;
		

		String content = FileUtils.readFileToString(file_bnp, StandardCharsets.UTF_8);

		JSONArray agenciesJsonA = new JSONArray(content);

		String nomAgence = "", formatAgence = "", adresse = "", adresse_comp = "", CP = "", ville = "", tel = "",
				fax = "", horaires = "", GAB = "", particulier = "", professionnel = "", entreprise = "", change = "",
				coffre_fort = "", depot_permanent = "", automate_billet = "", latitude = "", longitude = "",
				automate_cheque = "";

		try {

			for (int i = 0; i < agenciesJsonA.length(); i++) {
				JSONObject agenciesJson = agenciesJsonA.getJSONObject(i);
				nomAgence = agenciesJson.getString("nom");
				nomAgence = this.normalize(nomAgence);
				System.out.println("nom: " + nomAgence);

				formatAgence = agenciesJson.getString("format");
				System.out.println("format: " + formatAgence);

				adresse = this.normalize(agenciesJson.getString("adresse"));
				System.out.println("adresse: " + adresse);
				adresse_comp = this.normalize(agenciesJson.getString("adresse2"));
				System.out.println("adresse_comp: " + adresse_comp);
				CP = this.normalize(agenciesJson.getString("cp"));
				System.out.println("cp: " + CP);
				ville = this.normalize(agenciesJson.getString("ville"));
				System.out.println("ville : " + ville);
				tel = agenciesJson.getString("telephone");
				System.out.println("tel : " + tel);
				fax = agenciesJson.getString("fax");
				System.out.println("fax : " + fax);
				horaires = Normalizer.normalize(agenciesJson.getString("text1").replaceAll("<br/>", " ").replaceAll(".:", " : "), Normalizer.Form.NFD).replaceAll("[^\\p{ASCII}]", "");
				System.out.println("ho : " + horaires);
				JSONObject services = agenciesJson.getJSONObject("services");
				GAB = services.getString("gab");
				System.out.println("GAB : " + GAB);
				particulier = this.normalize(services.getString("particulier"));
				System.out.println("particulier : " + particulier);
				professionnel = this.normalize(services.getString("professionnel"));
				System.out.println("professionnel : " + professionnel);
				entreprise = this.normalize(services.getString("entreprise"));
				System.out.println("entreprise : " + entreprise);
				change = this.normalize(services.getString("change"));
				System.out.println("change : " + change);
				coffre_fort = this.normalize(services.getString("coffre_fort"));
				System.out.println("coffre_fort : " + coffre_fort);
				depot_permanent = this.normalize(services.getString("depot_permanent"));
				System.out.println("depot_permanent : " + depot_permanent);
				automate_billet = this.normalize(services.getString("automate_billet"));
				System.out.println("automate_billet : " + automate_billet);
				automate_cheque = this.normalize(services.getString("automate_cheque"));
				System.out.println("automate_cheque : " + automate_cheque);
				latitude = agenciesJson.getString("latitude");
				System.out.println("lat : " + latitude);
				longitude = agenciesJson.getString("longitude");
				System.out.println("lon : " + longitude);

				nbRecuperes++;
				
				 this.setData(ValeurSoumise, String.valueOf(nbAgences), nomAgence,
				 formatAgence, adresse, adresse_comp, CP, ville, tel, fax, horaires, GAB,
				 particulier, professionnel, entreprise, change, coffre_fort, depot_permanent,
				 automate_billet, automate_cheque, latitude, longitude);
				 
				
				log.writeLog(new Date(), CP, "no_error", agenciesJsonA.length(), i+1, agenciesJsonA.length() - i);
				
				System.out.println("++++++++++++++");
			}

		} catch (Exception e) {
			System.out.println(e.getMessage());
		}

		sc.close();
	}


	@Override
	public void saveItem(String... args) {// TODO Auto-generated method stub

	}
	
	public void deleteAll() {// TODO Auto-generated method stub
		
		rep.deleteAll();
	}

}
