package com.app.service;

import java.io.IOException;

import java.net.SocketTimeoutException;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.PageLoadStrategy;
import org.openqa.selenium.chrome.ChromeDriver;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.app.model.BCFOI;
import com.app.scrap.RepBCFOI;
import com.app.utils.Parametres;

import fa.browserUtils.ChromeBrowser;
import fa.browserUtils.FactoryBrowser;

@Service
public class ServiceBcfoi extends Scraper {

	@Autowired
	private RepBCFOI rep;

	ChromeDriver chromeDriver;
	
	String path = "C:\\ChromeDriver\\";

	@Override
	public void saveItem(String... args) {
		BCFOI bcfoi = new BCFOI();
		bcfoi.setCodePostale(args[0]).setNomWeb(args[1]).setAdresse(args[2]).setHorairesTxt(args[3]).setTel(args[4])
				.setFax(args[5]).setEnseigne(args[6]).setDAB(args[7]).setNbAgences(args[8]);
		rep.save(bcfoi);

	}

	public void setup() {
		//chromeDriver = (ChromeDriver) this.setupCh(/*Parametres.CHROME_DRIVER_PATH*/path, false, false, "", "", "");

		FactoryBrowser factBrowser = new FactoryBrowser();
		ChromeBrowser b = (ChromeBrowser) factBrowser.create("chrome");
		final String binaryPath = "C:\\Program Files\\Google\\Chrome Beta\\Application\\chrome.exe";
		
		chromeDriver = b.setOptions(PageLoadStrategy.NORMAL, path, 60000, 60000, 60000, false, false, binaryPath)
				.initBrowserDriver();
	}

	public void scrap() throws IOException, InterruptedException {

		String url = "https://bfcoi.com/fr/votre-banque/nos-agences/";
		try {
			chromeDriver.get(url);
		} catch (Exception e) {
			if (e instanceof SocketTimeoutException) {
				this.tearsDowns();
				this.setup();
				chromeDriver.get(url);
			}

		}

		System.out.println("scrap mayotte");
		JavascriptExecutor js = (JavascriptExecutor) chromeDriver;
		String script = String.format(
				"let l=document.querySelectorAll(`#continent-121 > li:nth-child(1) > ul > li:nth-child(1) > ul > li`).length; return l", "");
		long l = (long) js.executeScript(script);
		System.out.println("Agences Mayotte : ");
		System.out.println(l);
		
		this.retrieveData(l, "976");

		System.out.println("scrap reunion");
		script = String.format(
				"let l=document.querySelectorAll(`#continent-121 > li:nth-child(2) > ul > li:nth-child(1) > ul > li`).length; return l",
				"");
		l = (long) js.executeScript(script);
		System.out.println("Agences reunion : ");
		System.out.println(l);
		this.retrieveData(l, "974");

		System.out.println("scrap paris");
		script = String.format("let l=document.querySelectorAll(`#continent-122 > li > ul > li>a`).length; return l",
				"");
		l = (long) js.executeScript(script);
		System.out.println(l);
		this.retrieveData(l, "75");

	}

	public void retrieveData(long l, String dep) throws InterruptedException {
		JavascriptExecutor js = (JavascriptExecutor) chromeDriver;
		String script = "";
		if (l > 0) {
			Thread.sleep(5000);
			for (int i = 1; i <= l; i++) {
				switch (dep) {

				case "976" -> script = String.format(
						"let l=document.querySelector(`#continent-121 > li:nth-child(1) > ul > li:nth-child(1) > ul > li:nth-child(%d)>a`).click()",
						i);

				case "974" -> script = String.format(
						"let l=document.querySelector(`#continent-121 > li:nth-child(2) > ul > li:nth-child(1) > ul > li:nth-child(%d) > a`).click()",
						i);
//#country784
				case "75" -> script=String.format("let l=document.querySelector(`#continent-122 > li > ul > li > a`).click()",
						"");

				}
				
				js.executeScript(script);
				Thread.sleep((4000));

				script = String.format(
						"let m=document.querySelector(`#mainGmap > div > div > div:nth-child(2) > div:nth-child(2) > "
								+ "div > div:nth-child(4) > div:nth-child(2) > div > div > div > div`).getInnerHTML(); return m",
						"");
				String html = (String) js.executeScript(script);
				//System.out.println(html);
				Document doc = Jsoup.parse(html);
				Element root = doc.selectFirst("#gMapInfo");
				if (root != null) {

					String nom = "";
					String adresse = "";
					String horaires = "";
					String enseigne = "BFC";
					String tel = "";
					String fax = "";
					int dab = 0;
					Element nomElement = root.selectFirst("div.name");
					nom = (nomElement != null) ? this.normalize(nomElement.text().toUpperCase()) : "";
					System.out.println("nom " + nom);

					//Element adresseElement = root.selectFirst("div:nth-child(5) > p:nth-child(1)");
					//adresse = (adresseElement != null) ? adresseElement.text() : "";
					Elements adresseElement = root.select("div.adresse");
					
					for(Element adEl : adresseElement) {
						
						//adresse = adEl.text() +" ";
						
						if(adEl.selectFirst("p:nth-child(1)") != null) {
							adresse = adEl.selectFirst("p:nth-child(1)").text() + " ";
						}else {
							adresse = adEl.text() + " ";
						}
						
					}
					
					adresse = this.normalize(adresse.replaceAll("\\s{2,}", " ").trim());
					
					if(dep=="75") {
						adresse = "16 place de la Madeleine 75008 Paris";
					}
					
					
					System.out.println("adresse " + adresse);

					//#gMapInfo > div:nth-child(5) > p:nth-child(2)
					String service = "";
					for(int j = 2;  j < 5 ; j++){
					    if(root.selectFirst("div.adresse > p:nth-child("+j+")") != null){
					        service += root.selectFirst("div.adresse > p:nth-child("+j+")").text() + " ";
					    }
					}
					
					/*Element serviceElement = root.selectFirst("div:nth-child(5) > p:nth-child(2)");
					if(serviceElement!=null) {
						String tmp=this.normalize(serviceElement.text().toLowerCase());
						dab = ( (tmp.contains("dab")) ||(tmp.contains("distributeur de billets"))) ? 1 : 0;
								
					}*/
					
					service = this.normalize(service.toLowerCase());
					
					dab = ( (service.contains("dab")) ||(service.contains("distributeur de billets"))) ? 1 : 0;
					
					System.out.println("dab " + dab);

					Element horairesElement = root.selectFirst("div.Horaire");
					horaires = (horairesElement != null) ? this.normalize(horairesElement.text().trim().replaceAll("\\s{2,}", " ")) : "";
					System.out.println("horaires " + horaires);

					Element telElement = root.selectFirst("div.tel");
					tel = (telElement != null) ? telElement.text().trim() : "";
					System.out.println("tel " + tel);

					Element faxElement = root.selectFirst("div.fax");
					fax = (faxElement != null) ? faxElement.text().trim() : "";
					System.out.println("fax " + fax);

					// bcfoi.setCodePostale(args[0]).setNomWeb(args[1]).setAdresse(args[2]).setHorairesTxt(args[3]).setTel(args[4])
					// .setFax(args[5]).setEnseigne(args[6]).setDAB(args[7]).setNbAgences(args[8]);
					this.saveItem(dep, nom, adresse, horaires, tel, fax, enseigne, String.valueOf(dab),
							String.valueOf(l));
				}

			}

		}
	}

	public void tearsDowns() {
		chromeDriver.quit();
	}

}
