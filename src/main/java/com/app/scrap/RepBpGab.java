package com.app.scrap;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.app.model.BP_GAB;

@Repository
public interface RepBpGab extends JpaRepository<BP_GAB, Integer>,RepositoryCustom<BP_GAB> {

}
