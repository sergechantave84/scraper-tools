package com.app.scrap;

import javax.persistence.EntityManagerFactory;

import org.eclipse.jetty.websocket.common.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class AppConfig {

	@Autowired
	private EntityManagerFactory emFact;
	
	@Bean
	public SessionFactory getSessionFactory() {
		return emFact.unwrap(SessionFactory.class);
	}
}
