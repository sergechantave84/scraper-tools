package com.app.model;

import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="assu2000_dom", schema="scrap")
public class Assu2000_DOM {
   
	@Id
	@SequenceGenerator(name="scrap.assu2000_dom_id_seq",
	                     sequenceName ="scrap.assu2000_dom_id_seq",
	                     allocationSize=1)
    @GeneratedValue( strategy=GenerationType.SEQUENCE,
                     generator="scrap.assu2000_dom_id_seq")
	  
	@Column(name="id",updatable=false)
	private int id;
	
	@Column(name="nbagences", columnDefinition="varchar")
	private String nbAgences;
	
	@Column(name="nb_pages", columnDefinition="varchar")
	private String nbPages;
	
	@Column(name="num_page", columnDefinition="varchar")
	private String numPage;
	
	@Column(name="nbagences_page", columnDefinition="varchar")
	private String nbAgencesPage;
	
	@Column(name="lien_agence", columnDefinition="varchar")
	private String lienAgence;
	
	@Column(name="nom_agence", columnDefinition="varchar")
	private String nomAgence;
	
	@Column(name="adresse", columnDefinition="varchar")
	private String adresse;
	
	@Column(name="tel", columnDefinition="varchar")
	private String tel;
	
	@Column(name="horaires", columnDefinition="varchar")
	private String horaires;
	
	@Column(name="agencehtm", columnDefinition="varchar")
	private String agenceHTM;
	
	@Column(name="annee_du_scrap" ,columnDefinition="varchar(4) DEFAULT date_part('year',CURRENT_DATE)" )
	private String anneeScrap;
	
	public Assu2000_DOM() {
		LocalDate ld= new Date().toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
		anneeScrap=String.valueOf(ld.getYear()+1);
	}
	
	public int getId() {
		return id;
	}
	public Assu2000_DOM setId(int id) {
		this.id = id;
		return this;
	}
	public String getNbAgences() {
		return nbAgences;
	}
	public Assu2000_DOM setNbAgences(String nbAgences) {
		this.nbAgences = nbAgences;
		return this;
	}
	public String getNbPages() {
		return nbPages;
	}
	public Assu2000_DOM setNbPages(String nbPages) {
		this.nbPages = nbPages;
		return this;
	}
	public String getNumPage() {
		return numPage;
	}
	public Assu2000_DOM setNumPage(String numPage) {
		this.numPage = numPage;
		return this;
	}
	public String getNbAgencesPage() {
		return nbAgencesPage;
	}
	public Assu2000_DOM setNbAgencesPage(String nbAgencesPage) {
		this.nbAgencesPage = nbAgencesPage;
		return this;
	}
	public String getLienAgence() {
		return lienAgence;
	}
	public Assu2000_DOM setLienAgence(String lienAgence) {
		this.lienAgence = lienAgence;
		return this;
	}
	public String getNomAgence() {
		return nomAgence;
	}
	public Assu2000_DOM setNomAgence(String nomAgence) {
		this.nomAgence = nomAgence;
		return this;
	}
	public String getAdresse() {
		return adresse;
	}
	public Assu2000_DOM setAdresse(String adresse) {
		this.adresse = adresse;
		return this;
	}
	public String getTel() {
		return tel;
	}
	public Assu2000_DOM setTel(String tel) {
		this.tel = tel;
		return this;
	}
	public String getHoraires() {
		return horaires;
	}
	public Assu2000_DOM setHoraires(String horaires) {
		this.horaires = horaires;
		return this;
	}
	public String getAgenceHTM() {
		return agenceHTM;
	}
	public Assu2000_DOM setAgenceHTM(String agenceHTM) {
		this.agenceHTM = agenceHTM;
		return this;
	}
	public String getAnneeScrap() {
		return anneeScrap;
	}
	public Assu2000_DOM setAnneeScrap(String anneeScrap) {
		this.anneeScrap = anneeScrap;
		return this;
	}
}
