package com.app.service;

import java.io.File;
import java.text.Normalizer;

import org.openqa.selenium.PageLoadStrategy;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;

import fa.browserUtils.ChromeBrowser;
import fa.browserUtils.FactoryBrowser;
import fa.browserUtils.HtmlUnitBrowser;

public abstract class Scraper {

	public WebDriver setupHU() {
		FactoryBrowser factB = new FactoryBrowser();
		HtmlUnitBrowser hb = (HtmlUnitBrowser) factB.create("htmlUnit");
		HtmlUnitDriver hu = hb.setOptions(true, false, false, 12000).initBrowserDriver();
		return hu;
	}

	public WebDriver setupCh(String driverPath, boolean useDataUser, boolean useExtension, String userDataPath,
			String extPath,String binaryPath) {
		FactoryBrowser factB = new FactoryBrowser();
		ChromeBrowser chB = (ChromeBrowser) factB.create("chrome");
		ChromeDriver ch = null;
		if (useExtension) {
			File[] f = { new File(extPath) };
			chB.setExtensionPath(f);
		}

		if (useDataUser)
		chB.setDataUserPath(userDataPath);
			
			
		ch=chB.setOptions(PageLoadStrategy.NORMAL, driverPath, 60000, 60000, 0, useDataUser, useExtension,binaryPath)
					.initBrowserDriver();

		return ch;
	}

	public void quitch(WebDriver wd) {
		if (wd instanceof ChromeDriver) {
			wd.quit();
		} else if (wd instanceof HtmlUnitDriver) {
			wd.quit();
		}

	}

	public String normalize(String str) {

		str = Normalizer.normalize(str, Normalizer.Form.NFKD).replaceAll("[^\\p{ASCII}]", "");
		return str;
	}

	public abstract void saveItem(String... args);

	public abstract void scrap() throws Exception;
}
