package com.app.model;

import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="swisslife", schema="scrap")
public class Swisslife {
	
	@Id
	@SequenceGenerator(name="scrap.swisslife_id_seq",
	                    sequenceName ="scrap.swisslife_id_seq",
	                     allocationSize=1)
    @GeneratedValue( strategy=GenerationType.SEQUENCE,
                     generator="scrap.swisslife_id_seq")
	  
	@Column(name="id",updatable=false)
	private int id;
	
	@Column(name="departement_soumis", columnDefinition="varchar")
	private String depSoumis;
	
	@Column(name="nbagences", columnDefinition="varchar")
	private String nbAgences;
	
	@Column(name="nbtrouves", columnDefinition="varchar")
	private String nbTrouves;
	
	
	@Column(name="tel_fax", columnDefinition="varchar")
	private String tel;
	
	@Column(name="nom_agent", columnDefinition="varchar")
	private String nomAgent;
	
	
	@Column(name="adresse", columnDefinition="varchar")
	private String adresse;
	
	@Column(name="orias", columnDefinition="varchar")
	private String orias;
	
	
	@Column(name="agencehtm", columnDefinition="varchar")
	private String agenceHtm;
	
	@Column(name="annee_du_scrap" ,columnDefinition="varchar(4) DEFAULT date_part('year',CURRENT_DATE)")
	private String anneeScrap;

	public Swisslife() {
		LocalDate ld = new Date().toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
		anneeScrap = String.valueOf(ld.getYear() + 1);
	}
	public int getId() {
		return id;
	}

	public Swisslife setId(int id) {
		this.id = id;
		return this;
	}

	public String getCantonSoumis() {
		return depSoumis;
	}

	public Swisslife setCantonSoumis(String cantonSoumis) {
		this.depSoumis = cantonSoumis;
		return this;
	}

	public String getNbAgences() {
		return nbAgences;
	}

	public Swisslife setNbAgences(String nbAgences) {
		this.nbAgences = nbAgences;
		return this;
	}

	public String getNbTrouves() {
		return nbTrouves;
	}

	public Swisslife setNbTrouves(String nbTrouves) {
		this.nbTrouves = nbTrouves;
		return this;
	}

	public String getTel() {
		return tel;
	}

	public Swisslife setTel(String tel) {
		this.tel = tel;
		return this;
	}

	public String getNomAgent() {
		return nomAgent;
	}

	public Swisslife setNomAgent(String nomAgent) {
		this.nomAgent = nomAgent;
		return this;
	}

	public String getAdresse() {
		return adresse;
	}

	public Swisslife setAdresse(String adresse) {
		this.adresse = adresse;
		return this;
	}

	public String getAgenceHtm() {
		return agenceHtm;
	}

	public Swisslife setAgenceHtm(String agenceHtm) {
		this.agenceHtm = agenceHtm;
		return this;
	}

	public String getAnneeScrap() {
		return anneeScrap;
	}

	public Swisslife setAnneeScrap(String anneeScrap) {
		this.anneeScrap = anneeScrap;
		return this;
	}
	public String getOrias() {
		return orias;
	}
	public Swisslife setOrias(String orias) {
		this.orias = orias;
		return this;
	}

}
