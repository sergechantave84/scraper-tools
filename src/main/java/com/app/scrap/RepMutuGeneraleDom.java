package com.app.scrap;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.app.model.Mutuelle_Generale;
import com.app.model.Mutuelle_Generale_DOM;

@Repository
public interface RepMutuGeneraleDom extends JpaRepository<Mutuelle_Generale_DOM,Integer>,RepositoryCustom<Mutuelle_Generale_DOM>{

}
