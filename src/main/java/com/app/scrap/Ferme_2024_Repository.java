package com.app.scrap;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import com.app.model.Ferme_2024;

@Repository
public interface Ferme_2024_Repository extends JpaRepository<Ferme_2024,Integer>, RepositoryCustom<Ferme_2024> {
	
	@Query(value = "SELECT * FROM scrap.ferme_2024 WHERE nom_ferme=?1 AND adresse_ferme=?2",nativeQuery = true)
    public Ferme_2024 findByNameAndAddress(String nom_ferme, String adresse_ferme);
	
	@Query(value = "SELECT * FROM scrap.ferme_2024 WHERE bafId=?",nativeQuery = true)
    public Ferme_2024 findByBafId(int bafId);
}