package com.app.utils;

import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class EntityManagerUtils {

	private EntityManagerFactory emf;
	public EntityManagerFactory createEMF(String m) {
		emf= Persistence.createEntityManagerFactory(m);
		return emf;
	}
			
}
