package com.app.model;

import java.text.SimpleDateFormat;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="adrea", schema="scrap")
public class Adrea {
    @Id
    
    @SequenceGenerator(name="scrap.adrea_id_seq",
                     sequenceName ="scrap.adrea_id_seq",
                    allocationSize=1)
   @GeneratedValue(strategy=GenerationType.SEQUENCE,
                   generator="scrap.adrea_id_seq")

    @Column(name="id",updatable=false)
    private int id;
    
    @Column(name="nbagences",length= 4)
    private String nb_agences; 
    
    @Column(name="nomweb",length=500)
    private String nom_web;
    
    @Column(name="adresse",length=500)
    private String adresse; 
    
    @Column(name="tel",length=20)
    private String tel;
    
    @Column(name="horaires",length=500)
    private String horaires;
    
    @Column(name="agences_htm", columnDefinition="varchar")
    private String agences_HTM;
    
    
    @Column(name="annee_du_scrap" ,columnDefinition="varchar(4) DEFAULT date_part('year',CURRENT_DATE)")
    private String anneeScrap;
    
	public Adrea() {
		SimpleDateFormat f=null;
		Date d=new Date();
		f=new SimpleDateFormat("yyyy");
		anneeScrap=f.format(d).toString();
	}
    public int getId() {
		return id;
	}
	public Adrea setId(int id) {
		this.id = id;
		
		return this;
	}
	public String getNb_agences() {
		return nb_agences;
	}
	public Adrea setNb_agences(String nb_agences) {
		this.nb_agences = nb_agences;
		
		return this;
	}
	public String getNom_web() {
		return nom_web;
	}
	public Adrea setNom_web(String nom_web) {
		this.nom_web = nom_web;
		return this;
	}
	public String getAdresse() {
		return adresse;
	}
	public Adrea setAdresse(String adresse) {
		this.adresse = adresse;
		return this;
	}
	public String getTel() {
		return tel;
	}
	public Adrea setTel(String tel) {
		this.tel = tel;
		return this;
	}
	public String getHoraires() {
		return horaires;
	}
	public Adrea setHoraires(String horaires) {
		this.horaires = horaires;
		return this;
	}
	public String getAgences_HTM() {
		return agences_HTM;
	}
	public Adrea setAgences_HTM(String agences_HTM) {
		this.agences_HTM = agences_HTM;
		return this;
	}
	public String getAnneeScrap() {
		return anneeScrap;
	}
	public Adrea setAnneeScrap(String anneeScrap) {
		this.anneeScrap = anneeScrap;
		return this;
	} 
    
}
