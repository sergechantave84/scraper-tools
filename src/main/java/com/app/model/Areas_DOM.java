package com.app.model;

import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="areas_dom", schema="scrap")
public class Areas_DOM {
    
	@Id
	@SequenceGenerator(name="scrap.areas_dom_id_seq",
	                   sequenceName ="scrap.areas_dom_id_seq",
	                   allocationSize=1)
    @GeneratedValue(  strategy=GenerationType.SEQUENCE,
                      generator="scrap.areas_dom_id_seq")
	  
	@Column(name="id",updatable=false)
	private int id;
	
	@Column(name="nbpages", columnDefinition="varchar")
	private String nbPages;
	
	@Column(name="num_page", columnDefinition="varchar")
	private String numPage;
	
	@Column(name="nbagences_page", columnDefinition="varchar")
	private String nbAgencePage;
	
	@Column(name="lien_agence", columnDefinition="varchar")
	private String lienAgence;
	
	@Column(name="nom_agence", columnDefinition="varchar")
	private String nomAgence;
	
	@Column(name="adresse", columnDefinition="varchar")
	private String adresse;
	
	@Column(name="tel", columnDefinition="varchar")
	private String tel;
	
	@Column(name="orias", columnDefinition="varchar")
	private String orias;
	
	@Column(name="horaires", columnDefinition="varchar")
	private String horaires;
	
	@Column(name="agencehtm", columnDefinition="varchar")
	private String agenceHTM;
	
	@Column(name="annee_du_scrap" ,columnDefinition="varchar(4) DEFAULT date_part('year',CURRENT_DATE)" )
	private String anneeScrap;
	
	public Areas_DOM() {
		LocalDate ld= new Date().toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
		anneeScrap=String.valueOf(ld.getYear()+1);
	}
	
	public int getId() {
		return id;
	}
	public Areas_DOM setId(int id) {
		this.id = id;
		return this;
	}
	public String getNbPages() {
		return nbPages;
	}
	public Areas_DOM setNbPages(String nbPages) {
		this.nbPages = nbPages;
		return this;
	}
	public String getNbAgencePage() {
		return nbAgencePage;
	}
	public Areas_DOM setNbAgencePage(String nbAgencePage) {
		this.nbAgencePage = nbAgencePage;
		return this;
	}
	public String getLienAgence() {
		return lienAgence;
	}
	public Areas_DOM setLienAgence(String lienAgence) {
		this.lienAgence = lienAgence;
		return this;
	}
	public String getNomAgence() {
		return nomAgence;
	}
	public Areas_DOM setNomAgence(String nomAgence) {
		this.nomAgence = nomAgence;
		return this;
	}
	public String getAdresse() {
		return adresse;
	}
	public Areas_DOM setAdresse(String adresse) {
		this.adresse = adresse;
		return this;
	}
	public String getTel() {
		return tel;
	}
	public Areas_DOM setTel(String tel) {
		this.tel = tel;
		return this;
	}
	public String getOrias() {
		return orias;
	}
	public Areas_DOM setOrias(String orias) {
		this.orias = orias;
		return this;
	}
	public String getHoraires() {
		return horaires;
	}
	public Areas_DOM setHoraires(String horaires) {
		this.horaires = horaires;
		return this;
	}
	public String getAgenceHTM() {
		return agenceHTM;
	}
	public Areas_DOM setAgenceHTM(String agenceHTM) {
		this.agenceHTM = agenceHTM;
		return this;
	}
	public String getAnneeScrap() {
		return anneeScrap;
	}
	public Areas_DOM setAnneeScrap(String anneeScrap) {
		this.anneeScrap = anneeScrap;
		return this;
	}

	public String getNumPage() {
		return numPage;
	}

	public Areas_DOM setNumPage(String numPage) {
		this.numPage = numPage;
		return this;
	}
}
