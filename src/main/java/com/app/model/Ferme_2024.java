package com.app.model;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="ferme_2024", schema="scrap")
public class Ferme_2024 {
   
	@Id
	@SequenceGenerator(name="scrap.ferme_id_seq",
	                     sequenceName ="scrap.ferme_id_seq",
	                     allocationSize=1)
    @GeneratedValue( strategy=GenerationType.SEQUENCE,
                     generator="scrap.ferme_id_seq")
	  
	@Column(name="id",updatable=false)
	private int id;
	
	@Column(name="bafId",columnDefinition="integer")
	private int bafId;
	
	
	@Column(name="activite",  columnDefinition="varchar")
    private String activite;
	
	@Column(name="adresse_ferme",  columnDefinition="varchar")
	private String address;
	
	@Column(name="code_postal",  columnDefinition="varchar")
    private String codePostale;
    
	@Column(name="departement",  columnDefinition="varchar")
	private String departement;
	
	@Column(name="departement_name",  columnDefinition="varchar")
    private String departementName;
	
	@Column(name="email", columnDefinition="varchar")
    private String email;
	
	@Column(name="engagement_prod",  columnDefinition="integer")
    private int engagementProd;
	
	@Column(name="fax", columnDefinition="varchar")
    private String fax;
	
	@Column(name="genre", columnDefinition="varchar")
    private String genre;
	
	@Column(name="horaires_vente_a_ferme", columnDefinition="varchar")
    private String horairesFerme;
	
	@Column(name="horaires_vente_magasin_prod", columnDefinition="varchar")
    private String horairesMagasin;
	
	@Column(name="horaires_vente_au_marche", columnDefinition="varchar")
    private String horairesMarche;
	
	@Column(name="acces_handicape", columnDefinition="integer")
    private int accesHandicape;
	
	@Column(name="acces_handicap_auditif", columnDefinition="integer")
    private int accesAuditif;
	
	@Column(name="acces_handicap_mental", columnDefinition="integer")
    private int accesMental;
	
	@Column(name="acces_handicap_motrice", columnDefinition="integer")
    private int accesMotrice;
	
	@Column(name="acces_handicap_visuel", columnDefinition="integer")
    private int accesVisuel;
	
	@Column(name="acces_voiture", columnDefinition="integer")
    private int accesVoiture;
	
	@Column(name="adherent_adeve", columnDefinition="integer")
    private int adherentAdeve;
	
	@Column(name="agriculture_bio", columnDefinition="integer")
    private int agricultureBio;
	
	@Column(name="animaux_autoriser", columnDefinition="integer")
    private int animauxAutoriser;
	
	@Column(name="atelier", columnDefinition="integer")
    private int atelier;
	
	@Column(name="carte_bancaire", columnDefinition="integer")
    private int carteBancaire;
	
	@Column(name="cheque_vacance", columnDefinition="integer")
    private int chequeVacance;
	
	@Column(name="degustation", columnDefinition="integer")
    private int degustation;
	
	@Column(name="marcher_produit", columnDefinition="integer")
    private int marcherProduit;
	
	@Column(name="site_web", columnDefinition="integer")
    private int siteWeb;
	
	@Column(name="lien_site_web", columnDefinition="varchar")
    private String siteUrls;
	
	@Column(name="station_verte", columnDefinition="integer")
    private int stationVerte;
	
	@Column(name="tickets_restaurant", columnDefinition="integer")
    private int ticketsRestaurant;
	
	@Column(name="vente_en_ligne", columnDefinition="integer")
    private int venteEnLigne;
	
	@Column(name="latitude", columnDefinition="float")
    private Float latitude;
	
	@Column(name="longitude", columnDefinition="float")
    private Float longitude;
	
	@Column(name="mot_du_fermier", columnDefinition="text")
    private String motFermier;
	
	@Column(name="nom_ferme", columnDefinition="varchar")
    private String nomFerme;
	
	@Column(name="nom_proprietaire", columnDefinition="varchar")
    private String nomProprietaire;
	
	@Column(name="produit1", columnDefinition="varchar")
    private String produit1;
	
	@Column(name="produit2", columnDefinition="varchar")
    private String produit2;
	
	@Column(name="produit3", columnDefinition="varchar")
    private String produit3;
	
	@Column(name="produit4", columnDefinition="varchar")
    private String produit4;
	
	@Column(name="produit5", columnDefinition="varchar")
    private String produit5;
	
	@Column(name="produit6", columnDefinition="varchar")
    private String produit6;
	
	@Column(name="produit7", columnDefinition="varchar")
    private String produit7;
	
	@Column(name="produit8", columnDefinition="varchar")
    private String produit8;
	
	@Column(name="produit_ferme", columnDefinition="text")
    private String produitFerme;
	
	@Column(name="telephone_domicile", columnDefinition="varchar")
    private String telephoneDomicile;
	
	@Column(name="telephone_mobile", columnDefinition="varchar")
    private String telephoneMobile;
	
	@Column(name="telephone_travail", columnDefinition="varchar")
    private String telephoneTravail;
	
	@Column(name="ville", columnDefinition="varchar")
    private String ville;
	
	@Column(name="note", columnDefinition="float NULL")
    private Float note;
	
	@Column(name="addBy", columnDefinition="varchar NULL")
    private String addBy;
	
	@Column(name="disabled", columnDefinition="int DEFAULT 0")
    private int disabled;
	
	@Column(name="annee_du_scrap" ,columnDefinition="varchar(4) DEFAULT date_part('year',CURRENT_DATE)" )
    private String anneeScrap;
	
	public Ferme_2024() {
		LocalDate ld= new Date().toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
		anneeScrap=String.valueOf(ld.getYear()+1);
	}
	public int getId() {
		return id;
	}
	public Ferme_2024 setId(int id) {
		this.id = id;
		return this;
	}
	
	public int getBafId() {
		return bafId;
	}
	public Ferme_2024 setBafId(int bafId) {
		this.bafId = bafId;
		return this;
	}
	public String getActivite() {
		return activite;
	}
	public Ferme_2024 setActivite(String activite) {
		this.activite = activite;
		return this;
	}
	public String getAddress() {
		return address;
	}
	public Ferme_2024 setAddress(String address) {
		this.address = address;
		return this;
	}
	public String getCodePostale() {
		return codePostale;
	}
	public Ferme_2024 setCodePostale(String codePostale) {
		this.codePostale = codePostale;
		return this;
	}
	public String getDepartement() {
		return departement;
	}
	public Ferme_2024 setDepartement(String departement) {
		this.departement = departement;
		return this;
	}
	public String getDepartementName() {
		return departementName;
	}
	public Ferme_2024 setDepartementName(String departementName) {
		this.departementName = departementName;
		return this;
	}
	public String getEmail() {
		return email;
	}
	public Ferme_2024 setEmail(String email) {
		this.email = email;
		return this;
	}
	public int getEngagementProd() {
		return engagementProd;
	}
	public Ferme_2024 setEngagementProd(int engagementProd) {
		this.engagementProd = engagementProd;
		return this;
	}
	public String getFax() {
		return fax;
	}
	public Ferme_2024 setFax(String fax) {
		this.fax = fax;
		return this;
	}
	public String getGenre() {
		return genre;
	}
	public Ferme_2024 setGenre(String genre) {
		this.genre = genre;
		return this;
	}
	public String getHorairesFerme() {
		return horairesFerme;
	}
	public Ferme_2024 setHorairesFerme(String horairesFerme) {
		this.horairesFerme = horairesFerme;
		return this;
	}
	public String getHorairesMagasin() {
		return horairesMagasin;
	}
	public Ferme_2024 setHorairesMagasin(String horairesMagasin) {
		this.horairesMagasin = horairesMagasin;
		return this;
	}
	public String getHorairesMarche() {
		return horairesMarche;
	}
	public Ferme_2024 setHorairesMarche(String horairesMarche) {
		this.horairesMarche = horairesMarche;
		return this;
	}
	public int getAccesHandicape() {
		return accesHandicape;
	}
	public Ferme_2024 setAccesHandicape(int accesHandicape) {
		this.accesHandicape = accesHandicape;
		return this;
	}
	public int getAccesAuditif() {
		return accesAuditif;
	}
	public Ferme_2024 setAccesAuditif(int accesAuditif) {
		this.accesAuditif = accesAuditif;
		return this;
	}
	public int getAccesMental() {
		return accesMental;
	}
	public Ferme_2024 setAccesMental(int accesMental) {
		this.accesMental = accesMental;
		return this;
	}
	public int getAccesMotrice() {
		return accesMotrice;
	}
	public Ferme_2024 setAccesMotrice(int accesMotrice) {
		this.accesMotrice = accesMotrice;
		return this;
	}
	public int getAccesVisuel() {
		return accesVisuel;
	}
	public Ferme_2024 setAccesVisuel(int accesVisuel) {
		this.accesVisuel = accesVisuel;
		return this;
	}
	public int getAccesVoiture() {
		return accesVoiture;
	}
	public Ferme_2024 setAccesVoiture(int accesVoiture) {
		this.accesVoiture = accesVoiture;
		return this;
	}
	public int getAdherentAdeve() {
		return adherentAdeve;
	}
	public Ferme_2024 setAdherentAdeve(int adherentAdeve) {
		this.adherentAdeve = adherentAdeve;
		return this;
	}
	public int getAgricultureBio() {
		return agricultureBio;
	}
	public Ferme_2024 setAgricultureBio(int agricultureBio) {
		this.agricultureBio = agricultureBio;
		return this;
	}
	public int getAnimauxAutoriser() {
		return animauxAutoriser;
	}
	public Ferme_2024 setAnimauxAutoriser(int animauxAutoriser) {
		this.animauxAutoriser = animauxAutoriser;
		return this;
	}
	public int getAtelier() {
		return atelier;
	}
	public Ferme_2024 setAtelier(int atelier) {
		this.atelier = atelier;
		return this;
	}
	public int getCarteBancaire() {
		return carteBancaire;
	}
	public Ferme_2024 setCarteBancaire(int carteBancaire) {
		this.carteBancaire = carteBancaire;
		return this;
	}
	public int getChequeVacance() {
		return chequeVacance;
	}
	public Ferme_2024 setChequeVacance(int chequeVacance) {
		this.chequeVacance = chequeVacance;
		return this;
	}
	public int getDegustation() {
		return degustation;
	}
	public Ferme_2024 setDegustation(int degustation) {
		this.degustation = degustation;
		return this;
	}
	public int getMarcherProduit() {
		return marcherProduit;
	}
	public Ferme_2024 setMarcherProduit(int marcherProduit) {
		this.marcherProduit = marcherProduit;
		return this;
	}
	public int getSiteWeb() {
		return siteWeb;
	}
	public Ferme_2024 setSiteWeb(int siteWeb) {
		this.siteWeb = siteWeb;
		return this;
	}
	public int getStationVerte() {
		return stationVerte;
	}
	public Ferme_2024 setStationVerte(int stationVerte) {
		this.stationVerte = stationVerte;
		return this;
	}
	public int getTicketsRestaurant() {
		return ticketsRestaurant;
	}
	public Ferme_2024 setTicketsRestaurant(int ticketsRestaurant) {
		this.ticketsRestaurant = ticketsRestaurant;
		return this;
	}
	public int getVenteEnLigne() {
		return venteEnLigne;
	}
	public Ferme_2024 setVenteEnLigne(int venteEnLigne) {
		this.venteEnLigne = venteEnLigne;
		return this;
	}
	public Float getLatitude() {
		return latitude;
	}
	public Ferme_2024 setLatitude(Float latitude) {
		this.latitude = latitude;
		return this;
	}
	public Float getLongitude() {
		return longitude;
	}
	public Ferme_2024 setLongitude(Float longitude) {
		this.longitude = longitude;
		return this;
	}
	public String getMotFermier() {
		return motFermier;
	}
	public Ferme_2024 setMotFermier(String motFermier) {
		this.motFermier = motFermier;
		return this;
	}
	public String getNomFerme() {
		return nomFerme;
	}
	public Ferme_2024 setNomFerme(String nomFerme) {
		this.nomFerme = nomFerme;
		return this;
	}
	public String getNomProprietaire() {
		return nomProprietaire;
	}
	public Ferme_2024 setNomProprietaire(String nomProprietaire) {
		this.nomProprietaire = nomProprietaire;
		return this;
	}
	public String getProduit1() {
		return produit1;
	}
	public Ferme_2024 setProduit1(String produit1) {
		this.produit1 = produit1;
		return this;
	}
	public String getProduit2() {
		return produit2;
	}
	public Ferme_2024 setProduit2(String produit2) {
		this.produit2 = produit2;
		return this;
	}
	public String getProduit3() {
		return produit3;
	}
	public Ferme_2024 setProduit3(String produit3) {
		this.produit3 = produit3;
		return this;
	}
	public String getProduit4() {
		return produit4;
	}
	public Ferme_2024 setProduit4(String produit4) {
		this.produit4 = produit4;
		return this;
	}
	public String getProduit5() {
		return produit5;
	}
	public Ferme_2024 setProduit5(String produit5) {
		this.produit5 = produit5;
		return this;
	}
	public String getProduit6() {
		return produit6;
	}
	public Ferme_2024 setProduit6(String produit6) {
		this.produit6 = produit6;
		return this;
	}
	public String getProduit7() {
		return produit7;
	}
	public Ferme_2024 setProduit7(String produit7) {
		this.produit7 = produit7;
		return this;
	}
	public String getProduit8() {
		return produit8;
	}
	public Ferme_2024 setProduit8(String produit8) {
		this.produit8 = produit8;
		return this;
	}
	public String getProduitFerme() {
		return produitFerme;
	}
	public Ferme_2024 setProduitFerme(String produitFerme) {
		this.produitFerme = produitFerme;
		return this;
	}
	public String getTelephoneDomicile() {
		return telephoneDomicile;
	}
	public Ferme_2024 setTelephoneDomicile(String telephoneDomicile) {
		this.telephoneDomicile = telephoneDomicile;
		return this;
	}
	public String getTelephoneMobile() {
		return telephoneMobile;
	}
	public Ferme_2024 setTelephoneMobile(String telephoneMobile) {
		this.telephoneMobile = telephoneMobile;
		return this;
	}
	public String getTelephoneTravail() {
		return telephoneTravail;
	}
	public Ferme_2024 setTelephoneTravail(String telephoneTravail) {
		this.telephoneTravail = telephoneTravail;
		return this;
	}
	public String getVille() {
		return ville;
	}
	public Ferme_2024 setVille(String ville) {
		this.ville = ville;
		return this;
	}
	public Float getNote() {
		return note;
	}
	public Ferme_2024 setNote(Float note) {
		this.note = note;
		return this;
	}
	public String getAddBy() {
		return addBy;
	}
	public Ferme_2024 setAddBy(String addBy) {
		this.addBy = addBy;
		return this;
	}
	public int getDisabled() {
		return disabled;
	}
	public Ferme_2024 setDisabled(int disabled) {
		this.disabled = disabled;
		return this;
	}
	
	public String getSiteUrls() {
		return siteUrls;
	}
	public Ferme_2024 setSiteUrls(String siteUrls) {
		this.siteUrls = siteUrls;
		return this;
	}
	
	public String getAnneeScrap() {
		return anneeScrap;
	}
	public Ferme_2024 setAnneeScrap(String anneeScrap) {
		this.anneeScrap = anneeScrap;
		return this;
	}
}
