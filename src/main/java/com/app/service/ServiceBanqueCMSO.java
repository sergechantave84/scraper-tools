package com.app.service;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.SocketTimeoutException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Scanner;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import org.openqa.selenium.htmlunit.HtmlUnitDriver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.app.model.BanqueCMB;
import com.app.model.BanqueCMSO;
import com.app.scrap.RepBanqueCMSO;
import com.app.utils.BrowserUtils;
import com.app.utils.Parametres;
import com.app.utils.Utilitaires;

import fa.browserUtils.FactoryBrowser;
import fa.browserUtils.HtmlUnitBrowser;
import fa.log.LogApp.LogScrap;
import fa.log.config.LogConst;

@Service
public class ServiceBanqueCMSO extends Utilitaires {

	String driverPath = "C:\\ChromeDriver\\";
	String nomSITE = "CMSO";
	String ficval = "dep_cmso";

	String url_debut = "https://www.cmso.com/reseau-bancaire-cooperatif/";

	int NbVilles;
	int NbAgences;

	int nbRecuperes;
	int num_ligneficval;
	int parcoursficval;

	HtmlUnitDriver htmlUnitDriver;
	BufferedWriter sortie;

	@Autowired
	RepBanqueCMSO rep;

	public BanqueCMSO saveData(BanqueCMSO t) {

		return rep.save(t);

	}

	public List<BanqueCMSO> saveDataAll(List<BanqueCMSO> t) {

		return rep.saveAll(t);

	}

	public List<BanqueCMSO> getAll() {

		return rep.findAll();
	}

	public BanqueCMSO getById(int id) {

		return rep.findById(id).orElse(null);
	}

	public String deleteProduct(int id) {
		try {
			rep.deleteById(id);

		} catch (Exception e) {
			e.printStackTrace();
		}

		return "removed";
	}

	public BanqueCMSO updateProduct(BanqueCMSO t) {
		// CMODO Auto-generated method stub
		return null;
	}

	public BanqueCMSO setData(String depSoumis, String nbVilles, String numVilles, String villeSoumise,
			String nbAgences, String nomWeb, String adresse, String horaire, String telephone, String lienAgence,
			String agenceHtm) {
		BanqueCMSO cmso = new BanqueCMSO();
		cmso.setDepSoumis(depSoumis).setNbVilles(nbVilles).setNumVilles(numVilles).setVilleSoumise(villeSoumise)
				.setNbAgences(nbAgences).setNomWeb(nomWeb).setAdresse(adresse).setHoraire(horaire)
				.setTelephone(telephone).setLienAgence(lienAgence).setAgenceHtm(agenceHtm);
		this.saveData(cmso);
		return cmso;
	}

	public List<BanqueCMSO> showFirstRows() {
		List<BanqueCMSO> a = rep.getFirstRow(100, BanqueCMSO.class);
		return a;
	}

	public List<BanqueCMSO> showLastRows() {
		List<BanqueCMSO> a = rep.getLastRow(100, BanqueCMSO.class);
		return a;
	}

	public void setup() {
		FactoryBrowser factB = new FactoryBrowser();
		HtmlUnitBrowser htmlB = (HtmlUnitBrowser) factB.create("htmlUnit");
		htmlUnitDriver = htmlB.setOptions(true, false, false, 12000).initBrowserDriver();
	}

	public void letScrap() throws IOException {

		String resultFolderName = Parametres.REPERTOIRE_RESULT + "\\BQ\\" + nomSITE;
		List<String> listVille = new ArrayList<>();
		List<String> listVilleSub = new ArrayList<>();
		Scanner sc = new Scanner(new InputStreamReader(
				new FileInputStream(new File(Parametres.REPERTOIRE_DATA_SOURCES + ficval + ".txt"))));

		if (!new File(resultFolderName).exists()) {
			new File(resultFolderName).mkdirs();

		}
		fichier = new File(
				resultFolderName + "\\" + "RESULTATS_" + nomSITE + "_" + System.currentTimeMillis() + ".txt");

		LogScrap log = new LogScrap("AssuranceAxaLog.txt", LogConst.TYPE_LOG_FILE);
		if ((new File(LogConst.STORE_LOG_PATH + LogConst.PREFIX_LOG_FILE + "_AssuranceAxaLog.txt")).exists()) {

			num_ligneficval = Integer.parseInt(log.readLog().getLastLineProcessed().split("\t")[4]);

		} else {
			num_ligneficval = 1;
		}

		parcoursficval = 1;
		while (sc.hasNextLine()) {
			String line = sc.nextLine();
			if (parcoursficval > num_ligneficval) {
				String[] arrayLine = line.split("\\t");
				String depSoumis = arrayLine[0];
				String path = arrayLine[1];
				String uri = url_debut + path;
				try {
					System.out.println("URI : \n"+uri);
					htmlUnitDriver.get(uri);
				} catch (Exception e) {
					if (e instanceof SocketTimeoutException) {
						System.out.println(uri);
						htmlUnitDriver.get(uri);
					}
				}
				String pageSource = htmlUnitDriver.getPageSource();
				Document doc = Jsoup.parse(pageSource);
				
				Elements rootLinks = doc.select("#wia_18702_0_wia_18703_1 > section > div.container > div > div:nth-child(2) > ul > li > a");
				NbVilles = rootLinks.size();
				System.out.println("Le nombre villes : " + NbVilles);
				if (!listVille.isEmpty()) {
					listVille.clear();
				}
				for (Element tmp : rootLinks) {
					String href = tmp.attr("href");
					listVille.add(href);
				}
				int numville = 0;
				for (String path1 : listVille) {

					uri = url_debut + path1;

					try {

						System.out.println(uri);
						htmlUnitDriver.get(uri);
					} catch (Exception e) {
						if (e instanceof SocketTimeoutException) {
							System.out.println(uri);
							htmlUnitDriver.get(uri);
						}
					}

					pageSource = htmlUnitDriver.getPageSource();
					doc = Jsoup.parse(pageSource);
					// #r_21566_0_r_21568_1 > section > div.space_top.space_bottom_lg >
					// div.container > div > div > div > ul > li:nth-child(1)
					Elements pathsSubCities = doc
							.select("#r_21566_0_r_21568_1 > section > " + "div.space_top.space_bottom_lg > "
									+ "div.container > div > div > div > " + "ul > li > div.content > a");

					if (!listVilleSub.isEmpty()) {
						listVilleSub.clear();
					}
					for (Element pathSubCity : pathsSubCities) {
						String href = pathSubCity.attr("href");
						listVilleSub.add(href);
					}
					
					if (!listVilleSub.isEmpty()) {
						NbAgences = listVilleSub.size();
						System.out.println("le nombre d'agence est " + NbAgences);
						for (String path2 : listVilleSub) {
							uri = url_debut + path2;
							numville++;
							try {

								System.out.println(uri);
								htmlUnitDriver.get(uri);
							} catch (Exception e) {
								if (e instanceof SocketTimeoutException) {
									System.out.println(uri);
									htmlUnitDriver.get(uri);
								}
							}
							pageSource = htmlUnitDriver.getPageSource();
							doc = Jsoup.parse(pageSource);

							this.retrieveInfo(pageSource, uri, depSoumis, numville);

						}
					} else {
						NbAgences = 1;
						System.out.println("le nombre d'agence est " + NbAgences);
						numville++;
						pageSource = htmlUnitDriver.getPageSource();
						this.retrieveInfo(pageSource, uri, depSoumis, numville);

					}

				}
				log.writeLog(new Date(), depSoumis, "no_error", NbAgences, parcoursficval, 0);
			}
			parcoursficval++;
		}
	}

	public void retrieveInfo(String pageSource, String uri, String depSoumis, int numVille) {
		String agences_HTM = "", nom_agence = "", adresse = "", LienAgence = "", tel = "", horaires = "",
				villeSoumise = "";
		Document doc = Jsoup.parse(pageSource);
		Element rootInfos = doc.selectFirst("#ficheAgence");
		if (rootInfos != null) {

			agences_HTM = "";
			nom_agence = "";
			adresse = "";
			LienAgence = "";
			tel = "";
			horaires = "";
			villeSoumise = "";

			agences_HTM = rootInfos.outerHtml();
			agences_HTM = agences_HTM.replaceAll("\\s{1,}", "");
			LienAgence = uri;

			Element bigBlocCoordinate = rootInfos.selectFirst("div.container > div.row >"
					+ "div.col-12.col-md-10.offset-md-1.mt_md_1.top > " + "div > div.has_r_border_md.first");
			if (bigBlocCoordinate != null) {
				Element agencyNameElement = bigBlocCoordinate.selectFirst("h2");
				nom_agence = (agencyNameElement != null) ? agencyNameElement.text() : "";
				nom_agence = "agence credit mutuel du sud ouest " + nom_agence;
				nom_agence = nom_agence.toUpperCase().replaceAll("[^a-zA-Z ]{1,}", "");
				nom_agence = this.normalize(nom_agence);
				System.out.println("Nom agence : " + nom_agence);

				villeSoumise = (agencyNameElement != null) ? agencyNameElement.text().replaceAll("[^a-zA-Z ]{1,}", "")
						: "";
				villeSoumise = this.normalize(villeSoumise);

				Element adresseElement = bigBlocCoordinate.selectFirst("div:nth-child(2)");
				adresse = (adresseElement != null) ? adresseElement.text() : "";
				adresse = adresse.toUpperCase().replaceAll("ADRESSE", "").toUpperCase();
				adresse = this.normalize(adresse);
				System.out.println("Adresse : " + adresse);

				Element telElement = bigBlocCoordinate.selectFirst("div:nth-child(3)");
				tel = (telElement != null) ? telElement.text() : "";
				tel = tel.replaceAll("[^0-9 ]{1,}", "");
				tel = tel.replaceAll("-", " ");
				tel = this.normalize(tel);
				System.out.println("Tel : " + tel);
			}
			Elements schedules = rootInfos.select("div>div>" + "div.col-12.col-md-10.offset-md-1.mt_md_1.top>"
					+ "div>div.col-12.col-md-5.offset-md-1.second >" + "div>div>div");
			for (Element schedule : schedules) {
				horaires += schedule.text() + " ";
			}
			horaires = this.normalize(horaires);
			System.out.println("Horaire : " + horaires);

			this.setData(depSoumis, String.valueOf(NbVilles), String.valueOf(numVille), villeSoumise,
					String.valueOf(NbAgences), nom_agence, adresse, horaires, tel, LienAgence, agences_HTM);
		}
	}

	public void tearsDown() {
		htmlUnitDriver.quit();
	}

	public void enregistrer(String agences_HTM, String nom_agence, String adresse, String LienAgence, int numville,
			String horaires, String Villesoumise, String depSoumis) {

		try {

			/*
			 * String depTXT = DepSoumis; depTXT = depTXT.replaceAll("SubdivisionId=FR-",
			 * "");
			 */
			sortie = new BufferedWriter(new FileWriter(fichier, true));
			sortie.write(depSoumis + "\t" + NbVilles + "\t" + numville + "\t" + Villesoumise + "\t" + NbAgences + "\t"
					+ nom_agence + "\t" + adresse + "\t" + horaires + "\t" + LienAgence + "\t" + agences_HTM + "\r\n");

			sortie.close();
		} catch (IOException e) {

			e.printStackTrace();
		}

	}

	/**
	 * La m�thode <code>initialiser()</code> permet d'ajouter la ligne d'ent�te dans
	 * le fichier r�sultat.
	 */

	public void initialiser() {

		try {
			// Attention pas d'accent dans les noms de colonnes !
			sortie = new BufferedWriter(new FileWriter(fichier, true));
			sortie.write("DepSoumis" + "\t" + "NbVilles" + "\t" + "Numville" + "\t" + "VilleSoumise" + "\t"
					+ "NbAgences" + "\t" + "NOMWEB" + "\t" + "ADRESSE" + "\t" + "HORAIRES" + "\t" + "LIEN_AGENCE" + "\t"
					+ "AGENCEHTM" + "\r\n");
			sortie.close();

			/*
			 * File toto = new File(dossierRESU + "\\"+  "LOG_" + nomSITE +  ".csv"); if(
			 * !toto.exists() ){ //if (num_ligneficval == num_ligne) { sortie_log = new
			 * BufferedWriter(new FileWriter(log,true)); sortie_log.write("NumLigne" + ";"
			 * +"FinValeur" + ";" + "DepSoumis" + ";" + "NbVilles" + ";" + "NumVILLE" + ";"+
			 * "NbAgences" + ";" + "Nbrecuperes" + ";" + "Reste" + ";" + "Heure_Fin"+ "\r\n"
			 * ); sortie_log.close(); }
			 */

		} catch (IOException e) {

			e.printStackTrace();
		}
	}

	public void logout() {
		htmlUnitDriver.quit();
	}
}
