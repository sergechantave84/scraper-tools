package com.app.model;

import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="gan_dom", schema="scrap")
public class GAN_DOM {

	@Id
	@SequenceGenerator(name="scrap.gan_dom_id_seq",
	                    sequenceName ="scrap.gan_dom_id_seq",
	                     allocationSize=1)
    @GeneratedValue( strategy=GenerationType.SEQUENCE,
                     generator="scrap.gan_dom_id_seq")
	  
	@Column(name="id",updatable=false)
	private int id;
	
	@Column(name="nbagences", columnDefinition="varchar")
	private String nbAgences;
	
	@Column(name="nb_pages", columnDefinition="varchar")
	private String nbPages;
	
	@Column(name="num_page", columnDefinition="varchar")
	private String numPage;
	
	@Column(name="nbagences_page", columnDefinition="varchar")
	private String nbAgencePage;
	
	@Column(name="lien_agence", columnDefinition="varchar")
	private String lienAgence;
	
	@Column(name="nom_agence", columnDefinition="varchar")
	private String nomAgence;
	
	@Column(name="adresse", columnDefinition="varchar")
	private String adresse;
	
	@Column(name="tel", columnDefinition="varchar")
	private String tel;
	
	@Column(name="nom_agents", columnDefinition="varchar")
	private String nomAgents;
	
	@Column(name="orias", columnDefinition="varchar")
	private String orias;
	
	@Column(name="horaires", columnDefinition="varchar")
	private String horaires;
	
	@Column(name="agencehtm", columnDefinition="varchar")
	private String agenceHtm;
	
	@Column(name="interlocuteurhtm",  columnDefinition="varchar")
	private String interlocuteurHtm;
	
	@Column(name="annee_du_scrap" ,columnDefinition="varchar(4) DEFAULT date_part('year',CURRENT_DATE)")
	private String anneeScrap;
	
	public GAN_DOM() {
		LocalDate ld= new Date().toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
		anneeScrap=String.valueOf(ld.getYear()+1);
	}
	
	public int getId() {
		return id;
	}
	public GAN_DOM setId(int id) {
		this.id = id;
		return this;
	}
	public String getNbAgences() {
		return nbAgences;
	}
	public GAN_DOM setNbAgences(String nbAgences) {
		this.nbAgences = nbAgences;
		return this;
	}
	public String getNbPages() {
		return nbPages;
	}
	public GAN_DOM setNbPages(String nbPages) {
		this.nbPages = nbPages;
		return this;
	}
	public String getNumPage() {
		return numPage;
	}
	public GAN_DOM setNumPage(String numPage) {
		this.numPage = numPage;
		return this;
	}
	public String getNbAgencePage() {
		return nbAgencePage;
	}
	public GAN_DOM setNbAgencePage(String nbAgencePage) {
		this.nbAgencePage = nbAgencePage;
		return this;
	}
	public String getLienAgence() {
		return lienAgence;
	}
	public GAN_DOM setLienAgence(String lienAgence) {
		this.lienAgence = lienAgence;
		return this;
	}
	public String getNomAgence() {
		return nomAgence;
	}
	public GAN_DOM setNomAgence(String nomAgence) {
		this.nomAgence = nomAgence;
		return this;
	}
	public String getAdresse() {
		return adresse;
	}
	public GAN_DOM setAdresse(String adresse) {
		this.adresse = adresse;
		return this;
	}
	public String getTel() {
		return tel;
	}
	public GAN_DOM setTel(String tel) {
		this.tel = tel;
		return this;
	}
	public String getNomAgents() {
		return nomAgents;
	}
	public GAN_DOM setNomAgents(String nomAgents) {
		this.nomAgents = nomAgents;
		return this;
	}
	public String getOrias() {
		return orias;
	}
	public GAN_DOM setOrias(String orias) {
		this.orias = orias;
		return this;
	}
	public String getHoraires() {
		return horaires;
	}
	public GAN_DOM setHoraires(String horaires) {
		this.horaires = horaires;
		return this;
	}
	public String getAgenbceHtm() {
		return agenceHtm;
	}
	public GAN_DOM setAgenbceHtm(String agenbceHtm) {
		this.agenceHtm = agenbceHtm;
		return this;
	}
	public String getInterlocuteurHtm() {
		return interlocuteurHtm;
	}
	public GAN_DOM setInterlocuteurHtm(String interlocuteurHtm) {
		this.interlocuteurHtm = interlocuteurHtm;
		return this;
	}
	public String getAnneeScrap() {
		return anneeScrap;
	}
	public GAN_DOM setAnneeScrap(String anneeScrap) {
		this.anneeScrap = anneeScrap;
		return this;
	}
	
}
