package com.app.model;

import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="banque_hsbc",schema="scrap")
public class HSBC {

	
	@Id
	@SequenceGenerator(name="scrap.banque_hsbc_id_seq",
	                    sequenceName ="scrap.banque_hsbc_id_seq",
	                     allocationSize=1)
    @GeneratedValue( strategy=GenerationType.SEQUENCE,
                     generator="scrap.banque_hsbc_id_seq")
	  
	@Column(name="id",updatable=false)
	private int id;
	
	@Column(name="Regsoumis",columnDefinition="varchar")
	private String regSoumise;
	
	@Column(name="nbagences",columnDefinition="varchar")
	private String nbAgences;
	
	@Column(name="nomweb",columnDefinition="varchar")
	private String nomWeb;
	
	@Column(name="adresse",columnDefinition="varchar")
	private String adresse;
	
	@Column(name="tel",columnDefinition="varchar")
	private String tel;
	
	@Column(name="fax",columnDefinition="varchar")
	private String fax;
	
	@Column(name="horaireshtm", columnDefinition="varchar")
	private String horairesHtm;
	
	@Column(name="horairestxt",columnDefinition="varchar")
	private String horairesTxt;
	
	
	
	@Column(name="agencehtm", columnDefinition="varchar")
	private String agenceHtm;
	
	@Column(name="annee_du_scrap" ,columnDefinition="varchar(4) DEFAULT date_part('year',CURRENT_DATE)")
	private String anneeScrap;
	
	public HSBC() {
		LocalDate ld = new Date().toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
		anneeScrap = String.valueOf(ld.getYear() + 1);
	}
	
	public int getId() {
		return id;
	}
	public HSBC setId(int id) {
		this.id = id;
		return this;
	}
	public String getDepSoumise() {
		return regSoumise;
	}
	public HSBC setRegSoumise(String regSoumise) {
		this.regSoumise = regSoumise;
		return this;
	}
	public String getNbAgences() {
		return nbAgences;
	}
	public HSBC setNbAgences(String nbAgences) {
		this.nbAgences = nbAgences;
		return this;
	}
	public String getNomWeb() {
		return nomWeb;
	}
	public HSBC setNomWeb(String nomWeb) {
		this.nomWeb = nomWeb;
		return this;
	}
	public String getAdresse() {
		return adresse;
	}
	public HSBC setAdresse(String adresse) {
		this.adresse = adresse;
		return this;
	}
	public String getTel() {
		return tel;
	}
	public HSBC setTel(String tel) {
		this.tel = tel;
		return this;
	}
	public String getFax() {
		return fax;
	}
	public HSBC setFax(String fax) {
		this.fax = fax;
		return this;
	}
	public String getHorairesHtm() {
		return horairesHtm;
	}
	public HSBC setHorairesHtm(String horairesHtm) {
		this.horairesHtm = horairesHtm;
		return this;
	}
	public String getHorairesTxt() {
		return horairesTxt;
	}
	public HSBC setHorairesTxt(String horairesTxt) {
		this.horairesTxt = horairesTxt;
		return this;
	}
	
	public String getAgenceHtm() {
		return agenceHtm;
	}
	public HSBC setAgenceHtm(String agenceHtm) {
		this.agenceHtm = agenceHtm;
		return this;
	}
	public String getAnneeScrap() {
		return anneeScrap;
	}
	public HSBC setAnneeScrap(String anneeScrap) {
		this.anneeScrap = anneeScrap;
		return this;
	}
}
