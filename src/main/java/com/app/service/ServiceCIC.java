package com.app.service;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.SocketTimeoutException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Scanner;
import java.util.regex.Pattern;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.app.model.CIC;
import com.app.scrap.RepCIC;
import com.app.utils.BrowserUtils;
import com.app.utils.Parametres;
import com.app.utils.Utilitaires;

import fa.browserUtils.FactoryBrowser;
import fa.browserUtils.HtmlUnitBrowser;
import fa.log.LogApp.LogScrap;
import fa.log.config.LogConst;

@Service
public class ServiceCIC extends Utilitaires {

	@Autowired
	private RepCIC rep;

	String driverPath = "C:\\ChromeDriver\\";

	private List<CIC> cicList = new ArrayList<>();
	String nomSITE = "CIC_Agences";
	// String ficval = "DEP_CMCIC";
	String ficval = "cic";
	String dossierRESU = Parametres.REPERTOIRE_RESULT + "\\BQ\\" + nomSITE;
	String url_debut = "https://www.cic.fr/fr/banques/particuliers/BrowseLocality.aspx?";

	String fichier_valeurs;
	String CPSoumis;
	String ligne;

	int num_ligne = 2;
	int NbVilles;
	int NbAgences;
	int numreponse = 1;
	int nbRecuperes;
	int num_ligneficval;
	int parcoursficval;
	int Resultats;

	HtmlUnitDriver htmlUnitDriver;
	BufferedWriter sortie;
	BufferedWriter sortie_log;

	Date Temps_Fin;

	public CIC saveData(CIC t) {

		return (CIC) rep.save((CIC) t);

	}

	public List<CIC> saveDataAll(List<CIC> t) {

		return (List<CIC>) rep.saveAll((List<CIC>) t);

	}

	public List<CIC> getElmnt() {

		return rep.findAll();
	}

	public CIC getById(int id) {

		return rep.findById(id).orElse(null);
	}

	public String deleteProduct(int id) {
		try {
			rep.deleteById(id);

		} catch (Exception e) {
			e.printStackTrace();
		}

		return "removed";
	}

	public CIC updateProduct(CIC t) {
		// CICODO Auto-generated method stub
		return null;
	}

	public CIC setData(String depSoumis, String nbVilles, String numVilles, String villeSoumise, String nbAgences,
			String nomWeb, String adresse, String horaire, String telephone, String lienAgence, String agenceHtm,
			String fax, String codeGuchet, int gab, int dab, int borneDepot, String latitude, String longitude) {
		CIC cic = new CIC();
		cic.setDepSoumis(depSoumis).setNbVilles(nbVilles).setNumVilles(numVilles).setVilleSoumise(villeSoumise)
				.setNbAgences(nbAgences).setNomWeb(nomWeb).setAdresse(adresse).setHoraire(horaire)
				.setTelephone(telephone).setLienAgence(lienAgence).setAgenceHtm(agenceHtm).setFax(fax)
				.setCodeGuchet(codeGuchet).setGab(gab).setDab(dab).setBorneDepot(borneDepot).setLatitude(latitude)
				.setLongitude(longitude);
		this.saveData(cic);
		return cic;
	}

	public List<CIC> showFirstRows() {
		List<CIC> a = rep.getFirstRow(100, CIC.class);
		return a;
	}

	public List<CIC> showLastRows() {
		List<CIC> a = rep.getLastRow(100, CIC.class);
		return a;
	}

	public void setup() {

		FactoryBrowser fact = new FactoryBrowser();
		HtmlUnitBrowser hB = (HtmlUnitBrowser) fact.create("htmlUnit");
		htmlUnitDriver = hB.setOptions(false, false, false, 60000).initBrowserDriver();
	}

	public List<CIC> scrapCIC() throws IOException {
		fichier_valeurs = Parametres.REPERTOIRE_DATA_SOURCES + ficval + ".txt";
		List<String> listVille = new ArrayList<>();
		Scanner sc = new Scanner(new BufferedInputStream(new FileInputStream(new File(fichier_valeurs))));
		Scanner sc1 = new Scanner(new BufferedInputStream(new FileInputStream(new File(fichier_valeurs))));

		if (!new File(dossierRESU).exists()) {
			new File(dossierRESU).mkdirs();

		}
		fichier = new File(dossierRESU + "\\" + "RESULTATS_" + nomSITE + "_" + System.currentTimeMillis() + ".txt");
		LogScrap log = new LogScrap("BanqueCIC.txt", LogConst.TYPE_LOG_FILE);
		num_ligneficval = ((new File(LogConst.STORE_LOG_PATH + LogConst.PREFIX_LOG_FILE + "_BanqueCIC.txt")).exists())
				? Integer.parseInt(log.readLog().getLastLineProcessed().split("\\t")[4])
				: 1;

		parcoursficval = 0;
		int numville = 0;
		int i = 1;
		while (sc1.hasNextLine()) {
			String line = sc1.nextLine();
			if (i > 1) {
				NbVilles++;
			}
			i++;
		}
		sc1.close();
		while (sc.hasNextLine()) {

			String line = sc.nextLine();
			String[] fic_valeurs = line.split("\\t");
			if (parcoursficval > num_ligneficval) {

				numville++;
				// CPSoumis = fic_valeurs[0];
				// https://www.creditmutuel.fr/cmag/fr/banques/contact/trouver-une-agence/SearchList.aspx?type=branch&osat.p=&omon.p=&loca=75000&Btn.Ok.x=53&Btn.Ok.y=23&sub=true&loadmap=False&adv=&selflat=&selflng=
				// String uri=url_debut+CPSoumis;
				String uri = fic_valeurs[0];
				try {
					System.out.println("uri_SourceFile : " + uri);
					htmlUnitDriver.get(uri);
				} catch (Exception e) {
					if (e instanceof SocketTimeoutException) {
						htmlUnitDriver.get(uri);
					}
				}
				/*
				 * if(!listVille.isEmpty()) { listVille.clear(); } String
				 * pageSource=htmlUnitDriver.getPageSource(); Document
				 * doc=Jsoup.parse(pageSource); //System.out.println(pageSource); Element
				 * villeRoot=doc.
				 * selectFirst("#ei_tpl_content > div > article > div.body > div > div.a_blocfctl.lister"
				 * );
				 * 
				 * if(villeRoot!=null) { Elements villeLinkElement=villeRoot.
				 * select("table > tbody > tr > td.a_actions.nowrap > a"); for(Element tmp :
				 * villeLinkElement ) { listVille.add(tmp.attr("href")); } }
				 * 
				 * System.out.println("Nombre de villeLinkElement : " + listVille.size());
				 * 
				 * if(!listVille.isEmpty()) { String
				 * url="https://www.cic.fr/fr/banques/particuliers/"; for(String link :
				 * listVille) {
				 * 
				 * String uri2=url+link; try { System.out.println(uri2);
				 * htmlUnitDriver.get(uri2); }catch(Exception e) { if(e instanceof
				 * SocketTimeoutException) { htmlUnitDriver.get(uri2); } }
				 */
				String pageSource = htmlUnitDriver.getPageSource();
				Document doc = Jsoup.parse(pageSource);
				Element root = doc.select("#rslt").first();

				String agences_HTM = "", nom_agence = "", adresse = "", LienAgence = "", tel = "", horaires = "",
						villeSoumise = "", fax, latitude, longitude, codeGuchet;
				int gab, dab, borneDepot;
				if (root != null) {

					Elements agenciesLinkJElement = root.select("#rslt_lst> li>div.body > span > em > a");
					NbAgences = agenciesLinkJElement.size();
					System.out.println("NbAgences : " + NbAgences);
					nbRecuperes = 0;
					List<String> listLink = new ArrayList<>();
					if (!listLink.isEmpty()) {
						listLink.clear();
					}
					for (Element tmp : agenciesLinkJElement) {

						listLink.add(tmp.attr("href"));

					}

					System.out.println("NbAgencesTab : " + listLink.size());

					// System.out.println(listLink);

					for (String link : listLink) {

						try {
							System.out.println("uri_Fin : " + link);
							htmlUnitDriver.get(link);
						} catch (Exception e) {
							if (e instanceof SocketTimeoutException) {
								htmlUnitDriver.get(link);
							}
						}
						
						if(link.equals("https://www.cic.fr:443/fr/agence/particuliers/30568/19913/00/000")) {
							System.out.println("\t\t TY LE TSY NETY");
						}

						pageSource = htmlUnitDriver.getPageSource();
						doc = Jsoup.parse(pageSource);

						Element rootAgence = doc.selectFirst("#ei_tpl_contener");

						agences_HTM = "";
						nom_agence = "";
						adresse = "";
						LienAgence = "";
						tel = "";
						horaires = "";
						villeSoumise = "";

						fax = "";
						latitude = "";
						longitude = "";
						codeGuchet = "";
						gab = 0;
						dab = 0;
						borneDepot = 0;

						// retrieve agency link
						Element agencyNamaElement = rootAgence.selectFirst("p[id$=\"title\"]");
						Element agencyNamaElement2 = rootAgence.selectFirst("p.a_titre2");
						LienAgence = link;
						System.out.println("Lien : " + LienAgence);

						// retrieve agency name
						
						if (agencyNamaElement != null) {
							agences_HTM = this.normalize(agencyNamaElement.outerHtml()
									.replaceAll("\\s{1,}", " "));
							nom_agence = this.normalize(
									agencyNamaElement.text().toUpperCase().replace("BIENVENUE DANS VOTRE", "").trim());
						} else if (agencyNamaElement2 != null) {
							agences_HTM = this.normalize(agencyNamaElement2.outerHtml()
									.replaceAll("\\s{1,}", " "));
							nom_agence = this.normalize(
									agencyNamaElement2.text().toUpperCase().replace("BIENVENUE DANS VOTRE", "").trim());
						} else {
							agences_HTM = "";
							nom_agence = "";
						}
						
						System.out.println("Nom : " + nom_agence);

						// retrieve agency tel
						Element agencyTelElement = rootAgence.selectFirst("span[itemprop=\"telephone\"]");
						tel = (agencyTelElement != null) ? agencyTelElement.text().trim() : "";
						System.out.println("tel : " + tel);

						// retrieve horaires
						Element scheduleElement = rootAgence
								.selectFirst("#page_details > div.ei_gpblock-redac.ei_gpblock > div.ei_gpblock_body > "
										+ " div.cpc.eir_xs_table > div > div:nth-child(3) > div > div.ei_gpblock_body > table.days > tbody");
						
						Element scheduleElement2 = rootAgence.selectFirst("#schdl_main");
						//document.querySelector("#schdl_main").textContent.replace(/\s{2,}/g, " ")
						horaires = scheduleElement != null ? scheduleElement.text().trim() : "";
						horaires = horaires.toLowerCase().contains("dimanche") ? this.normalize(horaires) : this.normalize(horaires) +" Dimanche Ferme";
						System.out.println("horaires : " + horaires);
						
						if (scheduleElement != null) {
							horaires = scheduleElement.text().trim().replaceAll("\\s{2,}", " ");
						} else if (scheduleElement2 != null) {
							horaires = scheduleElement2.text().trim().replaceAll("\\s{2,}", " ");
						} else {
							horaires = "";
						}

						// retrieve agency address
						Element agencyAddressElement = rootAgence.selectFirst("span[itemprop=\"address\"]");
						adresse = (agencyAddressElement != null)
								? this.normalize(agencyAddressElement.text().toUpperCase().trim())
								: "";
						System.out.println("adresse : " + adresse);

						String[] tab = adresse.split(" ");

						for (String t : tab) {
							if (Pattern.matches("[0-9]{5}", t)) {
								CPSoumis = t;
							}

						}

						System.out.println("CPSoumis : " + CPSoumis);

						nbRecuperes++;

						villeSoumise = uri.replace(
								"https://www.cic.fr/fr/banques/particuliers/SearchList.aspx?sub=true&type=branch&loca=",
								"");

						villeSoumise = this.normalize(villeSoumise.replace("%20", " "));

						System.out.println("villeSoumise : " + villeSoumise);

						// retrieve agency fax
						Element agencyFaxElement = rootAgence.selectFirst(
								"#page_details > div.ei_gpblock-redac.ei_gpblock > div.ei_gpblock_body > div.cpc.eir_xs_table > "
										+ "div > div:nth-child(1) > div:nth-child(2) > div.ei_gpblock_body > table > tbody > tr:nth-child(2) > td");
						
						Element agencyFaxElement2 = rootAgence.selectFirst("td[itemprop=\"faxNumber\"]");
						
						if (agencyFaxElement != null) {
							fax = agencyFaxElement.text().trim();
						} else if (agencyFaxElement2 != null) {
							fax = agencyFaxElement2.text().trim();
						} else {
							fax = "";
						}
						
						System.out.println("fax : " + fax);

						// retrieve agency codeGuchet
						Element codeGuchetElement = rootAgence.selectFirst("span[itemprop=\"branchCode\"]");
						codeGuchet = (codeGuchetElement != null) ? codeGuchetElement.text().trim() : "";
						System.out.println("codeGuchet : " + codeGuchet);

						// retrieve agency gab
						Element dabGabBorneDepotElement = rootAgence.selectFirst("div.ei_gpblock_body > ul");

						String dabGabBorneDepot = dabGabBorneDepotElement != null
								? this.normalize(dabGabBorneDepotElement.text().toLowerCase().trim())
								: "";

						gab = dabGabBorneDepot.contains("guichet automatique") || dabGabBorneDepot.contains("guichets automatiques") ? 1 : 0;
						System.out.println("gab : " + gab);
						dab = dabGabBorneDepot.contains("distributeur de billets") || dabGabBorneDepot.contains("distributeurs de billets") ? 1 : 0;
						System.out.println("dab : " + dab);
						borneDepot = dabGabBorneDepot.contains("borne de depot") || dabGabBorneDepot.contains("bornes de depot") ? 1 : 0;
						System.out.println("borneDepot : " + borneDepot);

						latitude = rootAgence.selectFirst("meta[itemprop=\"latitude\"]") != null
								? doc.selectFirst("meta[itemprop=\"latitude\"]").attr("content")
								: "";
						System.out.println("latitude : " + latitude);

						longitude = rootAgence.selectFirst("meta[itemprop=\"longitude\"]") != null
								? doc.selectFirst("meta[itemprop=\"longitude\"]").attr("content")
								: "";
						System.out.println("longitude : " + longitude);

						/*cicList.add(this.setData(CPSoumis, String.valueOf(NbVilles), String.valueOf(numville),
								villeSoumise, String.valueOf(NbAgences), nom_agence, adresse, horaires, tel, LienAgence,
								agences_HTM, fax, codeGuchet, gab, dab, borneDepot, latitude, longitude));*/
						
						System.out.println("\n------------------------------\n");
					}

				}
				// }
				// }
				//log.writeLog(new Date(), CPSoumis, "no_errors", NbAgences, parcoursficval, 0);
			}
			parcoursficval++;
		}
		sc.close();
		return cicList;
	}

	public void tearsDown() {
		htmlUnitDriver.close();
	}

	public void enregistrer(String agences_HTM, String nom_agence, String adresse, String LienAgence, int numville,
			String Villesoumise) {

		try {

			/*
			 * String depTXT = DepSoumis; depTXT = depTXT.replaceAll("SubdivisionId=FR-",
			 * "");
			 */
			sortie = new BufferedWriter(new FileWriter(fichier, true));
			sortie.write(CPSoumis + "\t" + NbVilles + "\t" + numville + "\t" + Villesoumise + "\t" + NbAgences + "\t"
					+ nom_agence + "\t" + adresse + "\t" + LienAgence + "\t" + agences_HTM + "\r\n");

			sortie.close();
		} catch (IOException e) {

			e.printStackTrace();
		}

	}

	public void enregistrer_journal(int numreponse, int numville) {

//	    	this.fichier = new File(Parametres.REPERTOIRE + "RESULTATS_BNP.");
		try {
			Temps_Fin = new Date();
			SimpleDateFormat formater = null;
			formater = new SimpleDateFormat("'le' dd/MM/yyyy '�' hh:mm:ss");
			String date = formater.format(Temps_Fin);
			date = formater.format(Temps_Fin);

			/*
			 * String depTXT = DepSoumis; depTXT = depTXT.replaceAll("SubdivisionId=FR-",
			 * "");
			 */

			sortie_log = new BufferedWriter(new FileWriter(log, true));
			sortie_log.write(parcoursficval + ";" + "FinValeur" + ";" + CPSoumis + ";" + this.NbVilles + ";" + "VILLE_"
					+ numville + ";" + this.NbAgences + ";" + nbRecuperes + ";" + (this.NbAgences - nbRecuperes) + ";"
					+ date + "\r\n");
			sortie_log.close();
			long taille_fichier = fichier.length() / 1000000; // la length est en octets, on divise par 1000 pour mettre
																// en Ko et par 1000000 pour mettre en Mo

			if (taille_fichier > 8) {
				// INITIALISATION FICHIER DE RESULTAT
				this.fichier = new File(
						dossierRESU + "\\" + "RESULTATS_" + nomSITE + "_" + System.currentTimeMillis() + ".txt");
				// Attention pas d'accent dans les noms de colonnes !
				sortie = new BufferedWriter(new FileWriter(fichier, true));
				sortie.write("DepSoumis" + "\t" + "NbVilles" + "\t" + "Numville" + "\t" + "VilleSoumise" + "\t"
						+ "NbAgences" + "\t" + "NOMWEB" + "\t" + "ADRESSE" + "\t" + "LIEN_AGENCE" + "\t" + "AGENCEHTM"
						+ "\r\n");
				sortie.close();
			}

		} catch (IOException e) {

			e.printStackTrace();
		}

	}

	/**
	 * La m�thode <code>initialiser()</code> permet d'ajouter la ligne d'ent�te dans
	 * le fichier r�sultat.
	 */

	public void initialiser() {

		try {
			// Attention pas d'accent dans les noms de colonnes !
			sortie = new BufferedWriter(new FileWriter(fichier, true));
			sortie.write(
					"DepSoumis" + "\t" + "NbVilles" + "\t" + "Numville" + "\t" + "VilleSoumise" + "\t" + "NbAgences"
							+ "\t" + "NOMWEB" + "\t" + "ADRESSE" + "\t" + "LIEN_AGENCE" + "\t" + "AGENCEHTM" + "\r\n");
			sortie.close();

			File toto = new File(dossierRESU + "\\" + "LOG_" + nomSITE + ".csv");
			if (!toto.exists()) {
				// if (num_ligneficval == num_ligne) {
				sortie_log = new BufferedWriter(new FileWriter(log, true));
				sortie_log
						.write("NumLigne" + ";" + "FinValeur" + ";" + "DepSoumis" + ";" + "NbVilles" + ";" + "NumVILLE"
								+ ";" + "NbAgences" + ";" + "Nbrecuperes" + ";" + "Reste" + ";" + "Heure_Fin" + "\r\n");
				sortie_log.close();
			}

		} catch (IOException e) {

			e.printStackTrace();
		}
	}

}
