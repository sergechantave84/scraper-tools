package com.app.service;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.net.SocketTimeoutException;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.text.Normalizer;
import java.util.Date;
import java.util.Scanner;

import org.jsoup.Jsoup;

import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.app.model.BECM_GAB;
import com.app.scrap.RepBecmGab;
import com.app.utils.Parametres;

import fa.log.LogApp.LogScrap;
import fa.log.config.LogConst;

@Service
public class ServiceBecmGab extends Scraper {

	@Autowired
	RepBecmGab rep;

	private  HtmlUnitDriver hu;
	private String url_debut = "https://www.becm.fr/fr/banques/entreprises/SearchList.aspx?osat.p=&omon.p=&type=atm&loca=";
	private String url_fin = "&Btn.Ok.x=0&Btn.Ok.y=0&sub=true&loadmap=False&adv=&selflat=&selflng=";
	private String ficval = "CP_Commune_CMCIC_tot_2020";

	public void setup() {
		hu=(HtmlUnitDriver) this.setupHU();
	}

	@Override
	public void saveItem(String... args) {
		BECM_GAB becm_gab = new BECM_GAB();
		becm_gab.setNomWeb(args[0]).setAdresse(args[1]).setCommuneSoumise(args[2]).setGabCIC(args[3]).setGabCM(args[4])
				.setNbAgences(args[5]);
		rep.save(becm_gab);

	}

	@Override
	public void scrap() throws Exception {
		String fichier_valeurs = Parametres.REPERTOIRE_DATA_SOURCES + ficval + ".txt";
		Scanner sc = new Scanner(new BufferedInputStream(new FileInputStream(new File(fichier_valeurs))));
		LogScrap log = new LogScrap("BanqueBECM_GAB.txt", LogConst.TYPE_LOG_FILE);
		int num_ligneficval = ((new File(LogConst.STORE_LOG_PATH + LogConst.PREFIX_LOG_FILE + "_BanqueBECM_GAB.txt"))
				.exists()) ? Integer.parseInt(log.readLog().getLastLineProcessed().split("\\t")[4]) : 1;
		int currentLine = 1;

		while (sc.hasNextLine()) {
			String[] lines = sc.nextLine().split("\\t");
			if (currentLine > num_ligneficval) {
				String communeSoumise = lines[1] + " " + lines[0];
				String uri = url_debut + URLEncoder.encode(communeSoumise, StandardCharsets.UTF_8) + url_fin;
				try {
					hu.get(uri);
				} catch (Exception e) {
					if (e instanceof SocketTimeoutException) {
						hu.get(uri);
					}
				}
				String pageSource = hu.getPageSource();
				Document doc = Jsoup.parse(pageSource);
				Element root = doc.selectFirst("#rslt");
				String nom_agence = "", adresse = "";
				int gabCIC = 0, gabCM = 0, nbAgences = 0;
				if (root != null) {
					Elements agenciesLinkJElement = root.select("#rslt_lst> li>div.body");
					nbAgences = agenciesLinkJElement.size();
					for (Element tmp : agenciesLinkJElement) {
						Element gabCategoriesElement = tmp.selectFirst("span.invisible");
						String str = Normalizer
								.normalize(gabCategoriesElement.text().toLowerCase(), Normalizer.Form.NFKD)
								.replaceAll("[^\\p{ASCII}]", "");
						gabCIC = (str.contains("cic")) ? 1 : 0;
						gabCM = (str.contains("credit mutuel")) ? 1 : 0;
						
						System.out.println("gabcic "+gabCIC+" gabcm "+gabCM);

						// retrieve agency name
						Element agencyNameElement = tmp.select("span.lbl.titre3 > em").first();
						nom_agence=nom_agence.replaceAll("\\d{1,} km","");
						nom_agence = (agencyNameElement != null)
								? agencyNameElement.text().replaceAll("[^a-zA-Z ]", " ")
								: "";
						
						System.out.println("le nom de l'agence est " + nom_agence);

						// retrieve agency address

						Element agencyAddressElement = tmp.select("span:nth-child(3) > em > span.invisible").first();
						Element agencyAddressElement2 = tmp.select("	span:nth-child(2) > em > span.invisible")
								.first();
						adresse = "";
						if (agencyAddressElement != null) {
							adresse = agencyAddressElement.text();
						} else if (agencyAddressElement2 != null) {
							adresse = agencyAddressElement2.text();
						} else {
							adresse = "";
						}
						System.out.println(adresse);
						this.saveItem(nom_agence, adresse, communeSoumise, String.valueOf(gabCIC),
								String.valueOf(gabCM), String.valueOf(nbAgences));

					}

				}
			    log.writeLog(new Date(),communeSoumise,"no_error",nbAgences, currentLine,0);
			}
			currentLine++;
		}

	}

}
