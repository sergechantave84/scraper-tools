package com.app.scrap;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.app.model.Swisslife;
import com.app.model.Swisslife_DOM;

@Repository
public interface RepSwisslifeDom extends JpaRepository<Swisslife_DOM,Integer>,RepositoryCustom<Swisslife_DOM>{

}
