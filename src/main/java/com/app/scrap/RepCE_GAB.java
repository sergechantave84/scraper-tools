package com.app.scrap;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.app.model.CE_GAB;

@Repository
public interface RepCE_GAB extends JpaRepository<CE_GAB,Integer>,RepositoryCustom<CE_GAB> {

}
