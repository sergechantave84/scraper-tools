package com.app.service;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Scanner;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.NoAlertPresentException;
import org.openqa.selenium.PageLoadStrategy;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.app.model.BP_GAB;
import com.app.model.Mutuelle_Generale;
import com.app.model.Palatine;
import com.app.scrap.RepPalatine;
import com.app.utils.Parametres;
import com.app.utils.Utilitaires;

import com.app.utils.BrowserUtils;


@Deprecated
@Service
public class ServicePalatine extends Utilitaires {

	@Autowired
	RepPalatine rep;
	
	String driverPath = "C:\\ChromeDriver\\";
	String nomSITE = "BQ_PALATINE"; 
	String ficval = "CP_cantons";
	String dossierRESU = Parametres.REPERTOIRE_RESULT+ "\\BQ\\" +nomSITE ;;
	String url_accueil = "https://www.palatine.fr/nos-agences.html";
	String fichier_valeurs; 
	String DepSoumis;
	String ligne;
	
	int Resultats ;
	int num_ligne = 2;
    int NbAgences=0;
	int numreponse = 1;
	int nbRecuperes;
	int num_ligneficval=1;
	int parcoursficval=1;
	
	ChromeDriver wd;
	BufferedWriter sortie;
	BufferedWriter sortie_log;	
	
	Date Temps_Fin;  
	Document doc =null;
	public Palatine saveData(Palatine t) {

		
		return rep.save( t);
	
}


 public List<Palatine> saveDataAll(List<Palatine> t) {
	
		return  rep.saveAll( t);
	
 }


 public List<Palatine> getElmnt() {

	return  rep.findAll();
 }


  public Palatine getById(int id) {

	return  rep.findById(id).orElse(null);
}


public String deleteProduct(int id) {
	try {
		rep.deleteById(id);

	} catch (Exception e) {
		e.printStackTrace();
	}

	return "removed";
  }

 public void setData(String depSoumis,String nom_agence,String adresse,String tel,String fax,
		             String horairesTXT,String nbAgences,String agences_id,String horairesHTM,String agences_HTM ) {
	 
	 Palatine palatine=new Palatine();
	 palatine.setDepSoumis(depSoumis).
	          setNbAgences(nbAgences).
	          setAgences_id(agences_id).
	          setNom_agence(nom_agence).
	          setAdresse(adresse).
	          setTel(tel).
	          setFax(fax).
	          setHorairesTXT(horairesTXT).
	          setHorairesHTM(horairesHTM).
	          setAgences_HTM(agences_HTM);
	 this.saveData(palatine);
	          
	 
 }
 
 public void setUp() throws Exception {
 	
 	
		wd = BrowserUtils.chromeConstruct(driverPath, 60000, 60000,10,PageLoadStrategy.NORMAL);
 }
 

 public void open_BQ_PALATINE_byselenium() throws IOException, InterruptedException 
 {
     System.out.println("test");
     boolean documentLoaded=false;
 	fichier_valeurs = Parametres.REPERTOIRE_DATA_SOURCES +  ficval + ".txt";
 	Scanner sc = new Scanner(new File(fichier_valeurs));
 	
 	if( !new File(dossierRESU).exists() )
 		 new File(dossierRESU).mkdirs();
 	
 	this.fichier = new File(dossierRESU+"\\"+"RESULTATS_"+nomSITE+"_"+System.currentTimeMillis()+".txt");
 	this.log = new File(dossierRESU + "\\"+ "LOG_" + nomSITE +  ".csv");
 	File toto = new File(dossierRESU + "\\"+ "LOG_" + nomSITE +  ".csv"); 
     
 	if( toto.exists() )
     {
 	    String fichierLog = dossierRESU + "\\"+  "LOG_" + nomSITE +  ".csv";
	        InputStream ips=new FileInputStream(fichierLog); 
			InputStreamReader ipsr=new InputStreamReader(ips);
			BufferedReader br=new BufferedReader(ipsr);
		    String ligne;
		    num_ligneficval = 0;
		    while ((ligne=br.readLine())!=null)
		    {
			    num_ligneficval = this.compter(ligne, "(\\d+);FinValeur;", true);
		    }
		    br.close(); 
		}
     else {
     	num_ligneficval = num_ligne;
     } 
     
 	this.initialiser();
 	String html="",
 	       js="",
 	       line="",
 	       href="";
 	WebElement iframe=null, 
 			searchbox2=null, 
 			loupe=null, 
 			agency=null, 
 			backToListPage=null,
 	        cookie=null;
     while ( sc.hasNextLine() ) 
 	{
     	 html="";
     	 js="";
     	 line="";
     	 href="";
     	 iframe=null; 
     	 searchbox2=null;
     	 loupe=null;
     	 agency=null; 
     	 backToListPage=null;
     	 cookie=null;
     	JavascriptExecutor myExecutor2=null;
     	line = sc.nextLine();
 		String[] fic_valeurs = line.split("\\t");
 		if (parcoursficval>=num_ligneficval) 
 		{
 	    	
 	         try {
 	        	 wd.get(url_accueil);
				} catch (Exception e) {
					 wd.get(url_accueil);
				}
 	        documentLoaded=this.listensIfPageFullyLoaded(wd);
 	        if(documentLoaded) {
 	        	Thread.sleep(5000);
 	        	 cookie=wd.findElementByCssSelector("#tarteaucitronPersonalize");
	                 JavascriptExecutor jsExecutor4=(JavascriptExecutor) wd;
      			 jsExecutor4.executeScript("arguments[0].click();", cookie);
      			
 	        	iframe= wd.findElementByCssSelector("#frame_evermaps");
 	        	if(BrowserUtils.switchToFrame(wd,iframe)) {
 	        		    Resultats = 1;
 	       	            DepSoumis = fic_valeurs[0];
 	                    System.out.println(DepSoumis);
 	                    js=String.format("arguments[0].value='%s';",DepSoumis);
 	                    System.out.println(js);
 	                    
 	                    searchbox2 = wd.findElement(By.cssSelector("#searchcity"));
 	                    System.out.println("l'element est: "+searchbox2.getAttribute("placeholder"));
 	                    myExecutor2 = (JavascriptExecutor) wd;
 	                    myExecutor2.executeScript(js, searchbox2);
 	                    System.out.println("la valeur soumise est: "+searchbox2.getAttribute("value"));
 	                   
 	                    
 	                    loupe=wd.findElementByCssSelector(".btn.submit");
 	                    js=String.format("arguments[0].click()", "");
 	                    myExecutor2.executeScript(js, loupe);
 	                    try{
	                    	WebElement tmpElmnt=wd.findElement(By.cssSelector("#dlg-popin-geo-city"));
	                        List<WebElement> lstAmbigous=wd.findElements(By.cssSelector("#listAddressAmbiguous > div > div > ul>li"));
   	                    	boolean isInListChoise=false;
	                        for(WebElement tmpElement:lstAmbigous) {
   	                    		String strTmp=tmpElement.getText().toLowerCase();
   	                    		
   	                    		String ville=fic_valeurs[2].toLowerCase();
   	                    		
   	                    		if(strTmp.contains(ville)) {
   	                    			System.out.println("click sur "+strTmp);
   	                    			JavascriptExecutor jsExecutor3=(JavascriptExecutor) wd;
   	                    			jsExecutor3.executeScript("arguments[0].click();", tmpElement);
   	                    			isInListChoise=true;
   	                    		}
   	                    	}
   	                    	if(!isInListChoise) {
   	                    		JavascriptExecutor jsExecutor3=(JavascriptExecutor) wd;
	                    			jsExecutor3.executeScript("arguments[0].click();",  lstAmbigous.get(0));
   	                    	}
   	                    
	                    }catch(Exception e) {
	                    	System.out.println(e.getMessage());
	                    }
 	                    
 	                    documentLoaded=this.listensIfPageFullyLoaded(wd);
 	                    if(documentLoaded) {
 	                    	System.out.println("here");
 	                    	Thread.sleep(5000);
 	                    	 html=wd.getPageSource();
 	    	                    doc=Jsoup.parse(html);
 	    	                    Element agencyInfoBlock= doc.select("#agencies").first();
 	    	                    if( agencyInfoBlock !=null) 
 	    	                    {
 	    	                   	 Elements agenciesList=  agencyInfoBlock.select("li[class^=\"agency\"]");
 	    	                   	 nbRecuperes=0;
 	    	                   	 if(agenciesList !=null)
 	    	                   	 {
 	    	                   		 NbAgences=agenciesList.size();
 	    	                   		 System.out.println("le nombre d'agence est: "+ NbAgences);
 	    	                   		 for(Element agencies: agenciesList) 
 	    	                   		 {   
 	    	                   			 nbRecuperes++;
 	    	                   			 String nom_agence = "",
 	    	                   					 type_agence = "",
 	    	                   					 adresse = "",
 	    	                   					 tel = "",
 	    	                   					 fax = "",
 	    	                   					 HorairesHTM = "", 
 	    	                   					 HorairesTXT = "",
 	    	                   					 agencyId="",
 	    	                   					 agences_HTM ="", 
 	    	                   					 agency_href="";
 	    	                   			 
 	    	                   			   agences_HTM = agencies.outerHtml().toString().replaceAll("\\s{1,}","").trim();
 	    	                   			   agences_HTM=this.normalize(agences_HTM);
 	    	                   			 //fetch agency id
 	    	                                agencyId=agencies.attr("id");
 	    	                                System.out.println("l'id de l'agence est: "+agencyId);
 	    	                                
 	    	                               
 	    	                                //fetch agency Name
 	    	                                Element agencyHtmlName= agencies.select("h2[ class=\"agencylabel\"]").first();
 	    	                                nom_agence=( agencyHtmlName!= null ) ? this.normalize(agencyHtmlName.text()): "";
 	    	                                System.out.println("le nom de l'agence est: "+nom_agence);
 	    	                                
 	    	                                //fetch agency link
 	    	                                Element agencyHtmlHref= agencies.select("h2[ class=\"agencylabel\"]>a").first();
 	    	                                agency_href=(agencyHtmlHref!=null) ? agencyHtmlHref.attr("href"):"";
 	    	                                
 	    	                                //fetch agency type
 	    	                                Element agencyHtmlType= agencies.select("div[class=\"agencytype\"]").first();
 	    	                                type_agence= (agencyHtmlType!= null) ? agencyHtmlType.text():"";
 	    	                                System.out.println("le type de l'agence est: "+type_agence);
 	    	                                
 	    	                                //fetch agency address
 	    	                                Element agencyAddressHtml= agencies.select("div[class=\"agencyaddress\"]").first();
 	    	                                adresse= (agencyAddressHtml!= null) ? this.normalize(agencyAddressHtml.text()):""; //.toString().replaceAll("<br>","\\t")
 	    	                                System.out.println("l'adresse de l'agence est: "+adresse);
 	    	                                
 	    	                                //fetch agency phone number
 	    	                                Element agencyPhoneNumberHtml= agencies.select("div[class=\"agencytel\"]>a").first();
 	    	                                tel=(agencyPhoneNumberHtml!=null)? agencyPhoneNumberHtml.attr("href").replaceAll("tel:",""): "";
 	    	                                System.out.println("le téléphone de  l'agence est: "+ tel);
 	    	                                
 	    	                                //fetch agency fax number
 	    	                                Elements agencyFaxNumberHtml= agencies.select("div[class=\"agencyfax\"]>span");
 	    	                                if(agencyFaxNumberHtml!=null) 
 	    	                                {
 	    	                               	 int i=0;
 	    	                               	 for(Element tmp: agencyFaxNumberHtml ) 
 	    	                               	 {
 	    	                               		 i++;
 	    	                               		 if(i ==2) 
 	    	                               		 {
 	    	                               			 fax=tmp.text();
 	    	                               			 System.out.println("le fax de l'agence est: "+fax);
 	    	                               		 }
 	    	                               	 }
 	    	                                }
 	    	                                
 	    	                                //fetch horaire
 	    	                                if(!agency_href.equals("")) {
 	    	                                	href=String.format("href=\"%s\"",agency_href);
 	 	    	                                System.out.println(href);
 	 	    	                                agency=wd.findElementByCssSelector("a["+href+"]");
 	 	    	                                js=String.format("arguments[0].click()", "");
 	 	    	                                myExecutor2.executeScript(js,agency);
 	 	    	                               documentLoaded=this.listensIfPageFullyLoaded(wd);
 	 	    	       	                       if(documentLoaded) {
 	 	    	       	                    	Thread.sleep(5000); 
 	 	    	       	                    	html=wd.getPageSource();
 	 	    	                                doc=Jsoup.parse(html);
 	 	    	                                Element blocMain=doc.select("div[id=\"main\"]").first();
 	 	    	                                if( blocMain!= null ) 
 	 	    	                                {
 	 	    	                               	  Elements agenciesScheduleHtmlElementList=blocMain.select("div[class=\"agencyschedule\"]>div[class^=\"day\"]"); 
 	 	    	                               	  if( agenciesScheduleHtmlElementList!=null ) 
 	 	    	                               	  {   
 	 	    	                               		  HorairesHTM=  agenciesScheduleHtmlElementList.outerHtml().toString().replaceAll("\\s{1,}","").trim();
 	 	    	                               		  for(Element agenciesScheduleHtmlElement: agenciesScheduleHtmlElementList ) 
 	 	    	                               		  {
 	 	    	                               			  HorairesTXT+=agenciesScheduleHtmlElement.text()+" ";
 	 	    	                               		  }
 	 	    	                               		  System.out.println("l'horaires est: "+HorairesTXT);
 	 	    	                               	  }
 	 	    	                                }
 	    	                                }
 	    	                                HorairesHTM=this.normalize(HorairesHTM);
 	    	                                HorairesTXT=this.normalize(HorairesTXT);
 	    	                                this.enregistrer(agences_HTM, nom_agence, adresse, tel, fax, HorairesHTM, HorairesTXT);
 	    	                                this.setData(DepSoumis, nom_agence, adresse, tel, fax, 
 	    	                                		     HorairesTXT, String.valueOf(NbAgences), agencyId, HorairesHTM, agences_HTM);
 	    	                                this.enregistrer_journal(nbRecuperes);
 	    	                                
 	    	                                backToListPage=wd.findElementByCssSelector("a.btn.back");
 	    	                                js= String.format("arguments[0].click()", "");
 	    	                                myExecutor2.executeScript(js, backToListPage);
 	    	                                documentLoaded=this.listensIfPageFullyLoaded(wd);
 	    	                               if(documentLoaded) {
 	    	                            	  Thread.sleep(5000);
 	    	                               }
 	    	                                
 	    	       	                      }
 	    	                                  
 	    	                                
 	    	                                
 	    	                                  
 	    	                   		 }
 	    	                   	 }else 
 	    	                   	 {
 	    	                   		 NbAgences=0;
 	    	                   		 this.enregistrer_journal(nbRecuperes);
 	    	                   	 }
 	    	                } 
 	                    }
 	                    
 	                      		
 	        	}
    	    	}
 	    	 
              
             
 		}
 		parcoursficval++; 
 		
     } //fin tant que fichier de valeurs non fini
     sc.close();
 }
 
 public List<Palatine> showFirstRows() {
	 List<Palatine> a=rep.getFirstRow(100, Palatine.class);
	 return a;
}
	 
public List<Palatine> showLastRows(){
	List<Palatine> a=rep.getLastRow(100, Palatine.class);
	return a;
}
 
 public void enregistrer (
 		String agences_HTM,
         String nom_agence,
         String adresse,
         String tel,
         String fax,
         String HorairesHTM,
         String HorairesTXT)  {

// 	this.fichier = new File(Parametres.REPERTOIRE + "RESULTATS_BNP.");
 	try {
 	
 		sortie = new BufferedWriter(new FileWriter(fichier,true));
 		sortie.write(	DepSoumis + "\t" 
 						+ NbAgences + "\t" 
 						+ nom_agence + "\t"
 						+ adresse + "\t"
 						+ tel + "\t"
 						+ fax + "\t"
 						+ HorairesHTM + "\t"
 						+ HorairesTXT + "\t"
 						+ agences_HTM + "\r\n");
     
 	
 		sortie.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
}
 
 public void enregistrer_journal ( 
          int numreponse)  {

// 	this.fichier = new File(Parametres.REPERTOIRE + "RESULTATS_BNP.");
 	try {
 		Temps_Fin = new Date();
 		SimpleDateFormat formater = null;
 		formater = new SimpleDateFormat("'le' dd/MM/yyyy '�' hh:mm:ss");
 		String date =  formater.format(Temps_Fin);
 		date =  formater.format(Temps_Fin);
 		
 		sortie_log = new BufferedWriter(new FileWriter(log,true));
 		sortie_log.write(parcoursficval + ";"
 						+"FinValeur" + ";"
							+ DepSoumis + ";" 
							+ this.NbAgences + ";" 
 						+ nbRecuperes + ";" 
 						+ (this.NbAgences - nbRecuperes) + ";" 
 						+ date  + "\r\n"   );
 		sortie_log.close();
 		long  taille_fichier = fichier.length()/1000000; //la length est en octets, on divise par 1000 pour mettre en Ko et par 1000000 pour mettre en Mo
 		
 		if(taille_fichier> 4) {
 			//INITIALISATION FICHIER  DE RESULTAT
 	    	this.fichier = new File(dossierRESU + "\\"+ "RESULTATS_" + nomSITE +   "_" + System.currentTimeMillis() + ".txt");
 	    	//Attention pas d'accent dans les noms de colonnes !
         	sortie = new BufferedWriter(new FileWriter(fichier,true));
         	sortie.write("DepSoumis" + "\t" 
 					+ "NbAgences" + "\t" 
 					+ "NOMWEB" + "\t"
 					+ "ADRESSE" + "\t"
 					+ "TEL" + "\t"
 					+ "FAX" + "\t"
 					+ "HORAIRESHTM" + "\t"
 					+ "HORAIRESTXT" + "\t"
 					+ "AGENCEHTM"+ "\r\n");
         	sortie.close();
 		}
 		
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
}
 
 /**
  * La m�thode <code>initialiser()</code> permet d'ajouter la ligne d'ent�te dans le fichier r�sultat.
  */
 

 public void initialiser() {
     
     try {
     	//Attention pas d'accent dans les noms de colonnes !
     	sortie = new BufferedWriter(new FileWriter(fichier,true));
     	sortie.write("CPSoumis" + "\t" 
					+ "NbAgences" + "\t" 
					+ "NOMWEB" + "\t"
					+ "ADRESSE" + "\t"
					+ "TEL" + "\t"
					+ "FAX" + "\t"
					+ "HORAIRESHTM" + "\t"
					+ "HORAIRESTXT" + "\t"
					+ "AGENCEHTM"+ "\r\n");
     	sortie.close();
     	
     	File toto = new File(dossierRESU + "\\"+  "LOG_" + nomSITE +  ".csv"); 
         if( !toto.exists() ){
     	//if (num_ligneficval == num_ligne) {
	        	sortie_log = new BufferedWriter(new FileWriter(log,true));
	        	sortie_log.write("NumLigne" + ";" +"FinValeur" + ";" + "DepSoumis" + ";" + "NbAgences" + ";"  + "Nbrecuperes" + ";" + "Reste" + ";" + "Heure_Fin"+ "\r\n" );
	        	sortie_log.close();
     	}
     	
		} catch (IOException e) {
			
			e.printStackTrace();
		}
 }
 

 public void tearDown() {
     wd.close();
 	wd.quit();
     
 }
 
 public static boolean isAlertPresent(FirefoxDriver wd) {
     try {
         wd.switchTo().alert();
         return true;
     } catch (NoAlertPresentException e) {
         return false;
     }
 }
 public void saveHtmlSource(String html,String name) throws IOException 
 {
	  SimpleDateFormat sdfDate = new SimpleDateFormat("yyyy-MM-dd_HH-mm_a");//dd/MM/yyyy
     Date now = new Date();
     String strDate = sdfDate.format(now);
	  File file=new File("C:\\Palatine");
	  if(!file.exists())file.mkdirs();
	  String uri=file.toString();
	  BufferedWriter html1=new BufferedWriter(new FileWriter(uri+"\\pjco"+"_"+name+strDate+".html"));
     html1.write(html); 
     html1.close();
 }




}
