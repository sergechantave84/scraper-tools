package com.app.service;

import java.io.BufferedInputStream;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;

import java.net.SocketTimeoutException;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.text.Normalizer;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Scanner;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.app.model.CM_GAB;

import com.app.scrap.RepCM_GAB;

import com.app.utils.Parametres;
import com.app.utils.Utilitaires;

import fa.browserUtils.FactoryBrowser;
import fa.browserUtils.HtmlUnitBrowser;
import fa.log.LogApp.LogScrap;
import fa.log.config.LogConst;

@Service
public class ServiceCM_GAB extends Utilitaires {

	@Autowired
	private RepCM_GAB rep;

	static String driverPath = "C:\\ChromeDriver\\";

	private List<com.app.model.CM_GAB> cmGabList = new ArrayList<>();
	String nomSITE = "CM_GAB";
	String ficval = "CP_Commune_CMCIC_tot_2020";

	String dossierRESU = Parametres.REPERTOIRE_RESULT + "\\BQ\\" + nomSITE;;
	String url_debut = "https://www.creditmutuel.fr/cmag/fr/banques/contact/trouver-une-agence/SearchList.aspx?osat.p=&omon.p=&type=atm&loca=";
	String url_fin = "&Btn.Ok.x=0&Btn.Ok.y=0&sub=true&loadmap=False&adv=&selflat=&selflng=";
	String fichier_valeurs;
	String CommuneSoumise;
	String VilleSoumise;
	String ligne;
	String patternCIC = "<span class=\"invisible\" itemprop=\"brand\">CIC</span>";
	String patternCM = "<span class=\"invisible\" itemprop=\"brand\">Crédit Mutuel</span>";
	int num_ligne = 2;
	int NbAgences;
	int numreponse = 1;
	int GABCIC;
	int GABCM;
	int nbRecuperes;
	int num_ligneficval;
	int parcoursficval;
	int Resultats;

	HtmlUnitDriver htmlUnitDriver;
	BufferedWriter sortie;
	BufferedWriter sortie_log;

	Date Temps_Fin;

	public CM_GAB saveData(CM_GAB t) {

		return rep.save(t);

	}

	public List<CM_GAB> saveDataAll(List<CM_GAB> t) {

		return rep.saveAll(t);

	}

	public List<CM_GAB> getElmnt() {

		return rep.findAll();
	}

	public CM_GAB getById(int id) {

		return rep.findById(id).orElse(null);
	}

	public String deleteProduct(int id) {
		try {
			rep.deleteById(id);

		} catch (Exception e) {
			e.printStackTrace();
		}

		return "removed";
	}

	public CM_GAB updateProduct(CM_GAB t) {
		// CM_GABODO Auto-generated method stub
		return null;
	}

	public CM_GAB setData(String communeSoumise, String nbAgences, String nomWeb, String adresse, String gabCIC,
			String gabCM, String agenceHtm) {
		CM_GAB cmGab = new CM_GAB();
		cmGab.setCommuneSoumise(communeSoumise).setNbAgences(nbAgences).setNomWeb(nomWeb).setAdresse(adresse)
				.setGabCIC(gabCIC).setGabCM(gabCM).setAgenceHtm(agenceHtm);
		this.saveData(cmGab);
		return cmGab;
	}

	public List<CM_GAB> showFirstRows() {
		List<CM_GAB> a = rep.getFirstRow(100, CM_GAB.class);
		return a;
	}

	public List<CM_GAB> showLastRows() {
		List<CM_GAB> a = rep.getLastRow(100, CM_GAB.class);
		return a;
	}

	public void setup() {
		FactoryBrowser fact = new FactoryBrowser();
		HtmlUnitBrowser hB = (HtmlUnitBrowser) fact.create("htmlUnit");
		htmlUnitDriver = hB.setOptions(false, false, false, 60000).initBrowserDriver();
	}

	public List<com.app.model.CM_GAB> scrap() throws IOException {
		fichier_valeurs = Parametres.REPERTOIRE_DATA_SOURCES + ficval + ".txt";
		Scanner sc = new Scanner(new BufferedInputStream(new FileInputStream(new File(fichier_valeurs))));

		if (!new File(dossierRESU).exists()) {
			new File(dossierRESU).mkdirs();

		}
		fichier = new File(dossierRESU + "\\" + "RESULTATS_" + nomSITE + "_" + System.currentTimeMillis() + ".txt");
		LogScrap log = new LogScrap("BanqueCM_GAB.txt", LogConst.TYPE_LOG_FILE);
		num_ligneficval = ((new File(LogConst.STORE_LOG_PATH + LogConst.PREFIX_LOG_FILE + "_BanqueCM_GAB.txt"))
				.exists()) ? Integer.parseInt(log.readLog().getLastLineProcessed().split("\\t")[4]) : 1;

		parcoursficval = 1;

		while (sc.hasNextLine()) {

			String line = sc.nextLine();
			String[] fic_valeurs = line.split("\\t");
			if (parcoursficval >= num_ligneficval) {

				CommuneSoumise = fic_valeurs[1] + " " + fic_valeurs[0];
				System.out.println(CommuneSoumise);

				String uri = url_debut + URLEncoder.encode(CommuneSoumise, StandardCharsets.UTF_8) + fic_valeurs[0]
						+ url_fin;
				System.out.println(uri);
				try {
					System.out.println(uri);
					htmlUnitDriver.get(uri);
				} catch (Exception e) {
					if (e instanceof SocketTimeoutException) {
						System.out.println(uri);
						htmlUnitDriver.get(uri);
					}
				}
				String pageSource = htmlUnitDriver.getPageSource();
				Document doc = Jsoup.parse(pageSource);
				Element root = doc.select("#rslt").first();
				String agences_HTM = "", nom_agence = "", adresse = "", LienAgence = "";
				if (root != null) {

					Elements agenciesLinkJElement = root.select("#rslt_lst> li>div.body");

					NbAgences = agenciesLinkJElement.size();
					nbRecuperes = 0;
					GABCM = 0;
					GABCIC = 0;

					for (Element tmp : agenciesLinkJElement) {
						GABCM = 0;
						GABCIC = 0;
						System.out.println(CommuneSoumise);
						agences_HTM = tmp.outerHtml();
						agences_HTM = Normalizer.normalize(agences_HTM, Normalizer.Form.NFD);
						agences_HTM = agences_HTM.replaceAll("[^\\p{ASCII}]", "");
						System.out.println(agences_HTM);
						GABCIC = (agences_HTM.contains("<span class=\"invisible\" itemprop=\"brand\"> CIC </span>")) ? 1
								: 0;
						GABCM = (agences_HTM
								.contains("<span class=\"invisible\" itemprop=\"brand\"> Credit Mutuel </span>")) ? 1
										: 0;
						System.out.println("cic=" + GABCIC + " cm =" + GABCM);
						agences_HTM = agences_HTM.replaceAll("\\s{1,}", " ");

						// retrieve agency link
						Element agencyNamaElement = tmp.select("span.lbl.titre3 > em > a").first();
						LienAgence = (agencyNamaElement != null)
								? "https://www.creditmutuel.fr/cmag/fr/banques/contact/trouver-une-agence/"
										+ agencyNamaElement.attr("href")
								: "";
						System.out.println("le lien de l'agences est " + LienAgence);

						// retrieve agency name
						Element agencyNameElement = tmp.select("span.lbl.titre3 > em").first();
						nom_agence = (agencyNameElement != null)
								? agencyNameElement.text().replaceAll("[^a-zA-Z ]", " ")
								: "";
						System.out.println("le nom de l'agence est " + nom_agence);

						// retrieve agency address

						Element agencyAddressElement = tmp.select("span:nth-child(3) > em > span.invisible").first();
						Element agencyAddressElement2 = tmp.select("	span:nth-child(2) > em > span.invisible")
								.first();
						adresse = "";
						if (agencyAddressElement != null) {
							adresse = agencyAddressElement.text();
						} else if (agencyAddressElement2 != null) {
							adresse = agencyAddressElement2.text();
						} else {
							adresse = "";
						}

						System.out.println(adresse);
						nbRecuperes++;
						this.setData(CommuneSoumise, String.valueOf(NbAgences), nom_agence, adresse,
								String.valueOf(GABCIC), String.valueOf(GABCM), agences_HTM);

						System.out.println("+++++++++++++++++++++++++++++++++");
					}
				}
				log.writeLog(new Date(), CommuneSoumise, "no_error", NbAgences, parcoursficval, 0);
			}
			parcoursficval++;
		}
		sc.close();
		return cmGabList;
	}

	public void tearsDown() {
		htmlUnitDriver.close();
	}

	public void enregistrer(String agences_HTM, String nom_agence, String adresse) {

//    	this.fichier = new File(Parametres.REPERTOIRE + "RESULTATS_BNP.");
		try {

			sortie = new BufferedWriter(new FileWriter(fichier, true));
			sortie.write(CommuneSoumise + "\t" + NbAgences + "\t" + nom_agence + "\t" + adresse + "\t" + GABCIC + "\t"
					+ GABCM + "\t" + agences_HTM + "\r\n");

			sortie.close();
		} catch (IOException e) {

			e.printStackTrace();
		}

	}

	public void enregistrer_journal(int numreponse) {

//    	this.fichier = new File(Parametres.REPERTOIRE + "RESULTATS_BNP.");
		try {
			Temps_Fin = new Date();
			SimpleDateFormat formater = null;
			formater = new SimpleDateFormat("'le' dd/MM/yyyy '�' hh:mm:ss");
			String date = formater.format(Temps_Fin);
			date = formater.format(Temps_Fin);

			sortie_log = new BufferedWriter(new FileWriter(log, true));
			sortie_log.write(parcoursficval + ";" + "FinValeur" + ";" + CommuneSoumise + ";" + this.NbAgences + ";"
					+ nbRecuperes + ";" + (this.NbAgences - nbRecuperes) + ";" + date + "\r\n");
			sortie_log.close();
			long taille_fichier = fichier.length() / 1000000; // la length est en octets, on divise par 1000 pour mettre
																// en Ko et par 1000000 pour mettre en Mo

			if (taille_fichier > 8) {
				// INITIALISATION FICHIER DE RESULTAT
				this.fichier = new File(
						dossierRESU + "\\" + "RESULTATS_" + nomSITE + "_" + System.currentTimeMillis() + ".txt");
				// Attention pas d'accent dans les noms de colonnes !
				sortie = new BufferedWriter(new FileWriter(fichier, true));
				sortie.write("CommuneSoumise" + "\t" + "NbAgences" + "\t" + "NOMWEB" + "\t" + "ADRESSE" + "\t"
						+ "GABCIC" + "\t" + "GABCM" + "\t" + "AGENCEHTM" + "\r\n");
				sortie.close();
			}

		} catch (IOException e) {

			e.printStackTrace();
		}

	}

	/**
	 * La m�thode <code>initialiser()</code> permet d'ajouter la ligne d'ent�te dans
	 * le fichier r�sultat.
	 */

	public void initialiser() {

		try {
			// Attention pas d'accent dans les noms de colonnes !
			sortie = new BufferedWriter(new FileWriter(fichier, true));
			sortie.write("CommuneSoumise" + "\t" + "NbAgences" + "\t" + "NOMWEB" + "\t" + "ADRESSE" + "\t" + "GABCIC"
					+ "\t" + "GABCM" + "\t" + "AGENCEHTM" + "\r\n");
			sortie.close();

			File toto = new File(dossierRESU + "\\" + "LOG_" + nomSITE + ".csv");
			if (!toto.exists()) {
				// if (num_ligneficval == num_ligne) {
				sortie_log = new BufferedWriter(new FileWriter(log, true));
				sortie_log.write("NumLigne" + ";" + "FinValeur" + ";" + "CommuneSoumise" + ";" + "NbAgences" + ";"
						+ "Nbrecuperes" + ";" + "Reste" + ";" + "Heure_Fin" + "\r\n");
				sortie_log.close();
			}

		} catch (IOException e) {

			e.printStackTrace();
		}
	}
}
