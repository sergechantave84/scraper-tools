package com.app.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.app.service.ServiceFerme;
import com.app.service.ServiceFerme_2024;
import com.app.service.ServiceOrias;
import com.app.service.ServicePJAssurance;
import com.app.service.ServicePJAssurance2;
import com.app.service.ServicePJBanque;
import com.app.service.ServicePageJauneRestauration;
import com.app.service.ServicePageJauneRestauration2;
import com.app.service.ServiceTripadvisor;

@RestController
@RequestMapping("/scrap")
public class AutreController {

	@Autowired 
	ServiceTripadvisor ser;
	
	@Autowired
	ServiceOrias srvOrias;
	
	@Autowired
	ServicePJBanque srvPJBanque;
	
	@Autowired
	ServicePJAssurance2 srvPJAssu;
	
	@Autowired
	ServicePageJauneRestauration2 srvPJRest;
	
	@Autowired
	//ServiceFerme ferme;
	ServiceFerme_2024 ferme;
	
	@RequestMapping("/TripadVisor")
	public void test() {
		
		try {
			ser.setup();
			ser.scrapTripadvisor();
			ser.logOut();
		}catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	@RequestMapping("/Orias")
	public void setOrias() throws Exception {
		srvOrias.setup();
		srvOrias.scrap();
		srvOrias.quit();
	}
	@RequestMapping("/PJ_BANQUE")
	public void setPJ_Banuqe() throws Exception {
		srvPJBanque.setup();
		srvPJBanque.scrap();
		srvPJBanque.quit();
	}
	
	@RequestMapping("/PJ_ASSURANCE")
	public void setPJ_Assu() throws Exception {
		//srvPJAssu.setDep(true);
		srvPJAssu.setup();
		srvPJAssu.scrap();
		srvPJAssu.quit();
		
	}
	
	@RequestMapping("/PJ_RESTAURATION")
	public void setPJ_REST() throws Exception {
		srvPJRest.setup();
		srvPJRest.scrap();
		srvPJRest.quit();
		
	}
	
	@RequestMapping("/ferme")
	public void setFerme() throws Exception {
		
		//ferme.scrap();
		ferme.initDriver();
		ferme.scrapeFromAPI();
		
	}
}
