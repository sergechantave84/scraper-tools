package com.app.model;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="page_jaune_banque_dep", schema="scrap")
public class PJBanqueDep extends PJBanque {

	@Id
	@SequenceGenerator(name="scrap.page_jaune_banque_dep_id_seq",
	                  sequenceName ="scrap.page_jaune_banque_dep_id_seq",
	                  allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE,
	generator = "scrap.page_jaune_banque_dep_id_seq")
	@Column(name="id", unique=true, nullable = false)
	private int id;
	
	@Column(name="dpt_soumise",columnDefinition = "varchar")
	private String dpt;
	
	
	@Column(name="annee_scrap", columnDefinition = "varchar")
	private String anneeScrap;
	
	@Column(name="activite_soumise",columnDefinition = "varchar")
	private String activiteSoumise;

	public PJBanqueDep() {
		LocalDate dt=new Date().toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
		anneeScrap=String.valueOf(dt.getYear()+1);
	}
	public String getDpt() {
		return dpt;
	}

	public PJBanqueDep setDpt(String dpt) {
		this.dpt = dpt;
		return this;
	}

	
	public String getAnneeScrap() {
		return anneeScrap;
	}

	public PJBanqueDep setAnneeScrap(String anneeScrap) {
		this.anneeScrap = anneeScrap;
		return this;
	}
	public String getActiviteSoumise() {
		return activiteSoumise;
	}
	public PJBanqueDep setActiviteSoumise(String activiteSoumise) {
		this.activiteSoumise = activiteSoumise;
		return this;
	}
	
	
}
