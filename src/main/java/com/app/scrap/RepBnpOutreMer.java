package com.app.scrap;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.app.model.BNP_Outre_Mer;

@Repository
public interface RepBnpOutreMer extends JpaRepository<BNP_Outre_Mer, Integer>, RepositoryCustom<BNP_Outre_Mer>{

}
