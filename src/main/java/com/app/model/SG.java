package com.app.model;

import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="banque_sg", schema="scrap")
public class SG {
	
	@Id
	@SequenceGenerator(name="scrap.banque_sg_id_seq",
	                    sequenceName ="scrap.banque_sg_id_seq",
	                     allocationSize=1)
    @GeneratedValue( strategy=GenerationType.SEQUENCE,
                     generator="scrap.banque_sg_id_seq")
	  
	@Column(name="id",updatable=false)
	private int id;
	
	@Column(name="depsoumis", columnDefinition="varchar")
	private String depSoumis;
	
	@Column(name="nbagences", columnDefinition="varchar")
	private String nbAgences;
	
	@Column(name="nomweb", columnDefinition="varchar")
	private String nomWeb;
	
	@Column(name="adresse", columnDefinition="varchar")
	private String adresse;
	
	@Column(name="tel", columnDefinition="varchar")
	private String tel;
	
	@Column(name="horaireshtm", columnDefinition="varchar")
	private String horairesHtm;
	
	@Column(name="horairestxt", columnDefinition="varchar")
	private String horairestxt;
	
	@Column(name="dab", columnDefinition="varchar")
	private String dab;
	
	@Column(name="code_guichet", columnDefinition="varchar")
	private String code_guichet;
	
	@Column(name="services", columnDefinition="varchar")
	private String services;
	
	@Column(name="agencehtm", columnDefinition="varchar")
	private String agenceHtm;
	
	@Column(name="annee_du_scrap" ,columnDefinition="varchar(4) DEFAULT date_part('year',CURRENT_DATE)")
	private String anneeScrap;

	public SG() {
		LocalDate ld = new Date().toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
		anneeScrap = String.valueOf(ld.getYear() + 1);
	}

	public int getId() {
		return id;
	}

	public SG setId(int id) {
		this.id = id;
		return this;
	}

	public String getDepSoumis() {
		return depSoumis;
	}

	public SG setDepSoumis(String depSoumis) {
		this.depSoumis = depSoumis;
		return this;
	}

	public String getNbAgences() {
		return nbAgences;
	}

	public SG setNbAgences(String nbAgences) {
		this.nbAgences = nbAgences;
		return this;
	}

	public String getNomWeb() {
		return nomWeb;
	}

	public SG setNomWeb(String nomWeb) {
		this.nomWeb = nomWeb;
		return this;
	}

	public String getAdresse() {
		return adresse;
	}

	public SG setAdresse(String adresse) {
		this.adresse = adresse;
		return this;
	}

	public String getTel() {
		return tel;
	}

	public SG setTel(String tel) {
		this.tel = tel;
		return this;
	}

	public String getHorairesHtm() {
		return horairesHtm;
	}

	public SG setHorairesHtm(String horairesHtm) {
		this.horairesHtm = horairesHtm;
		return this;
	}

	public String getHorairestxt() {
		return horairestxt;
	}

	public SG setHorairestxt(String horairestxt) {
		this.horairestxt = horairestxt;
		return this;
	}

	public String getDab() {
		return dab;
	}

	public SG setDab(String dab) {
		this.dab = dab;
		return this;
	}

	public String getServices() {
		return services;
	}

	public SG setServices(String services) {
		this.services = services;
		return this;
	}

	public String getAgenceHtm() {
		return agenceHtm;
	}

	public SG setAgenceHtm(String agenceHtm) {
		this.agenceHtm = agenceHtm;
		return this;
	}

	public String getAnneeScrap() {
		return anneeScrap;
	}

	public SG setAnneeScrap(String anneeScrap) {
		this.anneeScrap = anneeScrap;
		return this;
	}

	public String getCode_guichet() {
		return code_guichet;
	}

	public SG setCode_guichet(String code_guichet) {
		this.code_guichet = code_guichet;
		return this;
	}

}
