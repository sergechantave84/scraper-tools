package com.app.service;

import java.io.BufferedInputStream;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;

import java.io.FileWriter;
import java.io.IOException;
import java.net.SocketTimeoutException;
import java.text.Normalizer;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Scanner;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.openqa.selenium.PageLoadStrategy;
import org.openqa.selenium.chrome.ChromeDriver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.app.model.Aesio;
import com.app.scrap.RepAesio;
import com.app.utils.Parametres;
import com.app.utils.Utilitaires;

import fa.browserUtils.ChromeBrowser;
import fa.browserUtils.FactoryBrowser;
import fa.log.LogApp.LogScrap;
import fa.log.config.LogConst;

@Service
public class ServiceAesio extends Utilitaires {
	
	@Autowired 
	RepAesio rep;
	
	ChromeDriver ch;
	String siteName="Aesio";
	String driverPath = "C:\\ChromeDriver\\";
	String fileDataSourceName="Aesio_dep.txt";
	//example url https://agences.aesio.fr/fr/region_fr/dep_fr
	String urlAesio="https://agences.aesio.fr/fr/";
	String scrappingPathResult="";
	int currentLine;
	int lineTraveled;
	BufferedWriter sortie=null;
	
	public void saveData(Aesio aesio) {
		rep.save(aesio);
	}
	
	public Aesio setData(String depSoumis,
			             String nomWeb, 
			             String nombreAgence,
			             String adresse,
			             String horaires,
			             String telephone,
			             String lienAgence,
			             String agenceHTML) {
		Aesio aesio=new Aesio();
		aesio.setDepSoumis(depSoumis).
		      setNomWeb(nomWeb).
		      setNombreAgence(nombreAgence).
		      setAdresse(adresse).
		      setHoraires(horaires).
		      setTelephone(telephone).
		      setLienAgence(lienAgence).
		      setAgenceHTML(agenceHTML);
		return aesio;
	}
	
	public void setup() {
		FactoryBrowser factB=new FactoryBrowser();
		ChromeBrowser chB=(ChromeBrowser) factB.create("chrome");
		final String chromeBeta = "C:\\Program Files\\Google\\Chrome Beta\\Application\\chrome.exe";
		//ch=chB.setOptions(PageLoadStrategy.NORMAL,driverPath, 60000,60000,60000,false,false).initBrowserDriver();
		ch=chB.setOptions(PageLoadStrategy.NORMAL,driverPath, 60000,60000,60000,false,false,chromeBeta).initBrowserDriver();
	}
	
	
	public void scrapAesio() throws IOException {
		
		this.setup();
		
		scrappingPathResult=Parametres.REPERTOIRE_RESULT+"\\ASSU\\"+siteName;
		
		File f=new File(scrappingPathResult);
		if(!f.exists()) {
			f.mkdirs();
		}
		fichier = new File(scrappingPathResult + "\\"+ "RESULTATS_" +siteName+   "_" + System.currentTimeMillis() + ".txt");
		Scanner sc= new Scanner(new BufferedInputStream(new FileInputStream(new File(Parametres.REPERTOIRE_DATA_SOURCES+fileDataSourceName))));
		
		LogScrap log= new LogScrap("AssuranceAesio.txt",LogConst.TYPE_LOG_FILE);
		
		
		if((new File(LogConst.STORE_LOG_PATH+LogConst.PREFIX_LOG_FILE+"_AssuranceAesio.txt")).exists()) {
			 
	        	currentLine=Integer.parseInt(log.readLog().getLastLineProcessed().split("\\t")[4]);
		}else {
				currentLine=1;
	    }
		
		//https://agences.aesio.fr/fr/auvergne-rhône-alpes/ain
		lineTraveled=1;
		String region="";
		String departement="";
		String pageSource="";
		
		while(sc.hasNextLine()) {
			String line[]=sc.nextLine().split("\\t");
			region=line[2];
			departement=line[1];
			
			System.out.println(lineTraveled+" / "+currentLine);
			
			if(lineTraveled > currentLine) {
				int nombreAgence=0;
				String uri=urlAesio+region+"/"+departement;
				System.out.println(uri);
				
				 ch.get(uri);
				 
				pageSource=ch.getPageSource();
				Document doc=Jsoup.parse(pageSource);
				Elements agenciesElement=doc.select("#lf-content > "
						+ "div.lf-geo-divisions__main__content > "
						+ "div > "
						+ "div.lf-geo-divisions__main__content__wrapper__locations > ul > li");
				List<String> strPaths=new ArrayList<>();
				nombreAgence=agenciesElement.size();
				for(Element agency: agenciesElement ) 
					strPaths.add(agency.selectFirst("div > a").attr("href"));
				
				for(String strPath: strPaths ) {
				 
		             String nomWeb=""; 
		             String adresse="";
		             String horaires="";
		             String telephone="";
		             String agenceHTML="";
					try{
						ch.get("https://agences.aesio.fr"+strPath);
					}catch(Exception e) {
						if(e instanceof SocketTimeoutException) {
							ch.get("https://agences.aesio.fr"+strPath);
						}
					}
					
					pageSource=ch.getPageSource();
					doc=Jsoup.parse(pageSource);
					Element agencyRoot=doc.selectFirst("#lf-content");
					
					System.out.println("Lien : "+strPath);
					
					/* retrieve agency html*/
					agenceHTML=agencyRoot.outerHtml().replaceAll("\\s{1,}"," ");
					
					//retrieve agency name
					Element agencyNameElement=agencyRoot.selectFirst("#lf-location-wcag-titre");
					nomWeb=(agencyNameElement!=null)? agencyNameElement.text():"";
					nomWeb=Normalizer.normalize(nomWeb, Normalizer.Form.NFD).replaceAll("[^\\p{ASCII}]","");
					System.out.println("nom agence : "+nomWeb);
					
					
					//retrieve agency adresse
					Element agencyAdresseElement=agencyRoot.selectFirst("address[class^=\"lf-parts-address\"]");
					adresse=(agencyAdresseElement!=null)? agencyAdresseElement.text():"";
					adresse=Normalizer.normalize(adresse, Normalizer.Form.NFD).replaceAll("[^\\p{ASCII}]","").replaceAll("Adresse","");
					System.out.println("adresse agence : "+adresse);
					
					//retrieve agency phone number
					Element agencyPhoneElement=agencyRoot.selectFirst("main>"
							+ "section.lf-location__main__infos > "
							+ "div.lf-location__main__infos__left > "
							+ "div.lf-parts-phone.lf-location__main__infos__left__phone > "
							+ "a");
					telephone=(agencyPhoneElement!=null)? "0"+agencyPhoneElement.attr("href").replaceAll("[^0-9]",""):"";
					System.out.println("telephone agence : "+telephone);
					
					//retrieve agency schedules 
					Elements agencyShedulesElements=agencyRoot.select("#lf-parts-opening-hours-content > div");
					for(Element agencyScheduleElement: agencyShedulesElements) {
						horaires+=this.normalize(agencyScheduleElement.text())+" ";
					}
					horaires = horaires.replaceAll("Horaires d'ouverture d'aujourd'hui", "");
					
					System.out.println("horaires : "+horaires);
					
					System.out.println("----------------------------------------------------------------\n ");
					
					
					this.saveData(this.setData(line[0], 
							                   nomWeb,
							                   String.valueOf(nombreAgence), 
							                   adresse, 
							                   horaires, 
							                   telephone, 
							                   "https://agences.aesio.fr"+strPath,
							                   agenceHTML));
					
					/*this.enregistrer( departement, 
							          nomWeb, 
							          nombreAgence, 
							          adresse, 
							          horaires, 
							          telephone,
							          "https://agences.aesio.fr"+strPath, 
							          agenceHTML);*/
				}
				log.writeLog(new Date(),line[0],"no_error", nombreAgence, lineTraveled, nombreAgence);
			}
		
			lineTraveled++;
		}
		
	}
	 
	 public void initialiser() {
	        
	        try {
	        	//Attention pas d'accent dans les noms de colonnes !
	        	sortie = new BufferedWriter(new FileWriter(fichier,true));
	        	sortie.write("DepSoumis" + "\t" 
						     + "NOMWEB" + "\t"
						     + "NbAgences" + "\t" 
						     + "ADRESSE" + "\t"
						     + "HORIARE"+ "\t"
						     + "TEL" + "\t"
						     + "LIEN"+ "\t"
					         + "AGENCEHTM"+ "\r\n");
	        	sortie.close();
	        	
			 } catch (IOException e) {
				
				e.printStackTrace();
			 }  
	 }
	 public void enregistrer (
	    		String dep,
			    String nomWeb,
	            int nombreAgence,
	            String adresse,
	            String horaires,
	            String telephone,
	            String lien,
	            String agenceHTML)   {

	    	try {
	    	
	    		sortie = new BufferedWriter(new FileWriter(fichier,true));
	    		sortie.write(	dep + "\t"+ 
	    						nomWeb+ "\t" +
	    				        nombreAgence+"\t"+
	    						adresse + "\t"+
	    						horaires+ "\t"+
	    						telephone+ "\t"+
	    						lien+"\t"+
	    						agenceHTML + "\r\n");
	            sortie.close();
			} catch (IOException e) {
				
				e.printStackTrace();
			}
			
	}
	 public void logOut() {
		 ch.quit();
	 }

}
