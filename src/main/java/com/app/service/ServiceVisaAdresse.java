package com.app.service;


import java.io.BufferedWriter;
import java.io.File;

import java.io.FileWriter;
import java.io.IOException;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Scanner;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.NoAlertPresentException;
import org.openqa.selenium.PageLoadStrategy;
import org.openqa.selenium.UnhandledAlertException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.app.model.Visa_adresse;
import com.app.scrap.RepVisaAdresse;
import com.app.utils.BrowserUtils;
import com.app.utils.Parametres;
import com.app.utils.Utilitaires;

import fa.browserUtils.ChromeBrowser;
import fa.browserUtils.FactoryBrowser;
import fa.log.LogApp.LogScrap;
import fa.log.config.LogConst;

@Service
public class ServiceVisaAdresse extends Utilitaires {

	@Autowired
	RepVisaAdresse rep;

	static String driverPath = "C:\\ChromeDriver\\";
	String nomSITE = "Visa_adresse";
	String ficval = "VISA_ADRESSE_2021";

	private List<Visa_adresse> visaList = new ArrayList<>();
	String dossierRESU = Parametres.REPERTOIRE_RESULT + "\\BQ\\" + nomSITE;;
	String url_accueil = "https://www.visa.com/atmlocator/#(language:french,page:home)";
	String fichier_valeurs;
	String codpostSoumis;
	String communeSoumise;
	String ligne;
	int num_ligne = 2;
	int AgenciesNumberPerPage;
	int PageNumbers;
	int numreponse = 1;
	int numpage;
	int DAB = 1;
	int nbRecuperes_page;
	int num_ligneficval;
	int parcoursficval;
	int Resultats;
	ChromeDriver chromeDriver;
	BufferedWriter sortie;
	BufferedWriter sortie_log;
	Date Temps_Fin;
	Document doc = null;
	boolean bool = true;

	public Visa_adresse saveData(Visa_adresse t) {

		return rep.save(t);

	}

	public List<Visa_adresse> saveDataAll(List<Visa_adresse> t) {

		return rep.saveAll(t);

	}

	public List<Visa_adresse> getElmnt() {

		return rep.findAll();
	}

	public Visa_adresse getById(int id) {

		return rep.findById(id).orElse(null);
	}

	public String deleteProduct(int id) {
		try {
			rep.deleteById(id);

		} catch (Exception e) {
			e.printStackTrace();
		}

		return "removed";
	}

	public Visa_adresse updateProduct(Visa_adresse t) {
		// Visa_adresseODO Auto-generated method stub
		return null;
	}

	public Visa_adresse setData(String adresse, String nbPages, String numPage, String nbAgencesPage, String nomWeb,
			String voie, String cp, String ville, String dab, String agenceHtm) {
		Visa_adresse visa = new Visa_adresse();
		visa.setAdresseSoumis(adresse).setNbPages(nbPages).setNumPage(numPage).setNbAgencesPage(nbAgencesPage)
				.setNomWeb(nomWeb).setVoie(voie).setCp(cp).setVille(ville).setDab(dab).setAgenceHtm(agenceHtm);
		//this.saveData(visa);
		return visa;
	}

	public List<Visa_adresse> showFirstRows() {
		List<Visa_adresse> a = rep.getFirstRow(100, Visa_adresse.class);
		return a;
	}

	public List<Visa_adresse> showLastRows() {
		List<Visa_adresse> a = rep.getLastRow(100, Visa_adresse.class);
		return a;
	}

	public void setUp() throws Exception {
		FactoryBrowser factBrowser = new FactoryBrowser();
		ChromeBrowser b = (ChromeBrowser) factBrowser.create("chrome");
		final String chromeBeta = "C:\\Program Files\\Google\\Chrome Beta\\Application\\chrome.exe";
		chromeDriver = b.setOptions(PageLoadStrategy.NORMAL, driverPath, 60000, 60000, 60000, false, false,chromeBeta)
				.initBrowserDriver();
	}

	public List<Visa_adresse> scrap() throws Exception {
		// for(int j=1; j<96;j++)
		// {
		boolean msgAlert = false;
		fichier_valeurs = Parametres.REPERTOIRE_DATA_SOURCES + ficval + ".txt";
		System.out.println(fichier_valeurs);
		Scanner sc = new Scanner(new File(fichier_valeurs));
		
		
		LogScrap log = new LogScrap("VISA_Adresse.txt", LogConst.TYPE_LOG_FILE);
		num_ligneficval = ((new File(LogConst.STORE_LOG_PATH + LogConst.PREFIX_LOG_FILE + "_VISA_Adresse.txt"))
				.exists()) ? num_ligneficval = Integer.parseInt(log.readLog().getLastLineProcessed().split("\\t")[4])
						: 1;

	
		parcoursficval = 1;

		while (sc.hasNextLine()) {
			
			WebElement input = chromeDriver.findElement(By.id("#autosuggestinput_location_id"));
			System.out.println("is not null : "+input!=null);
			String line = "", js = "", html = "";
			line = sc.nextLine();
			String[] fic_valeurs = line.split("\\t");
			if (parcoursficval >= num_ligneficval) {
				WebElement searchBox2 = null;
				WebElement boutonOK = null;
				WebElement didYouMeanHtml = null;
				if (bool == false) {
					this.tearDown();
					this.setUp();
				}
				bool = true;
				JavascriptExecutor myExecutor2 = (JavascriptExecutor) chromeDriver;
				AgenciesNumberPerPage = 0;
				PageNumbers = 0;
				Resultats = 1;
				nbRecuperes_page = 0;
				codpostSoumis = fic_valeurs[1];
				System.out.println(codpostSoumis);
				communeSoumise = fic_valeurs[0];
				codpostSoumis = codpostSoumis.replaceAll(", FRANCE", "");
				String test = "";
				try {
					chromeDriver.get(url_accueil);
					Thread.sleep(2000);
					searchBox2 = chromeDriver.findElementByCssSelector("input.searchInput");
					
					//WebElement input = chromeDriver.findElement(By.id("#autosuggestinput_location_id"));
					//WebElement root_element = null;
					
					JavascriptExecutor jse = (JavascriptExecutor)chromeDriver;
					jse.executeScript("document.querySelector('#location_id').shadowRoot.querySelector('#autosuggestinput_location_id').value;");
					
					 WebElement shadow_root = null;
					 Object o = ((JavascriptExecutor) chromeDriver).executeScript("return arguments[0].shadowRoot.querySelector('#autosuggestinput_location_id')");
					 shadow_root = (WebElement) o;
					    
					//document.querySelector("#location_id").shadowRoot.querySelector("#autosuggestinput_location_id")
					js = String.format("arguments[0].value='%s,FRANCE'", codpostSoumis);
					System.out.println(js);
					myExecutor2.executeScript(js, searchBox2);
					boutonOK = chromeDriver.findElementByCssSelector(
							"form[class=\"visaATMSearchWrap\"]>button[class=\"visaATMSubmit\"]");
					if (boutonOK != null) {
						js = String.format("arguments[0].click()", "");
						myExecutor2.executeScript(js, boutonOK);
					}
					Thread.sleep(3000);

					msgAlert = this.isAlertPresent(chromeDriver);
					System.out.println(msgAlert);
					

					
					// chech if atm locator know the place or we just have to submit the zip code
					didYouMeanHtml = chromeDriver.findElementByCssSelector(
							"form[class=\"visaATMSearchWrap\"]> div[class=\"visaATMSearchDidYouMean\"]");
					test = didYouMeanHtml.getAttribute("outerHTML");
				} catch (Exception e) {
					if (e instanceof UnhandledAlertException) {
						try {
							String alertCibled = " Le serveur est momentanément indisponible";
							Alert alert = chromeDriver.switchTo().alert();
							String alertMessage = alert.getText();
							alert.accept();

							if (alertMessage.contains(alertCibled)) {
								this.tearDown();
								this.setUp();
								chromeDriver.get(url_accueil);
								Thread.sleep(2000);
								searchBox2 = chromeDriver.findElementByCssSelector("input.searchInput");
								js = String.format("arguments[0].value='%s,FRANCE'", codpostSoumis);
								System.out.println(js);
								myExecutor2.executeScript(js, searchBox2);
								boutonOK = chromeDriver.findElementByCssSelector(
										"form[class=\"visaATMSearchWrap\"]>button[class=\"visaATMSubmit\"]");
								if (boutonOK != null) {
									js = String.format("arguments[0].click()", "");
									myExecutor2.executeScript(js, boutonOK);
								}
								//msgAlert = this.isAlertPresent(chromeDriver);
								Thread.sleep(2000);
								// chech if atm locator know the place or we just have to submit the zip code
								didYouMeanHtml = chromeDriver.findElementByCssSelector(
										"form[class=\"visaATMSearchWrap\"]> div[class=\"visaATMSearchDidYouMean\"]");
								test = didYouMeanHtml.getAttribute("outerHTML");

							}
							System.out.println(alertMessage);

						} catch (Exception f) {
							if (f instanceof NoAlertPresentException) {
								f.printStackTrace();
							}
						} finally {
							System.out.println("on continue");
						}
					}
				}

				//System.out.println(test);
				if (test.contains("ul")) {
					JavascriptExecutor myExecutor3 = (JavascriptExecutor) chromeDriver;
					try {
						chromeDriver.get(url_accueil);
						Thread.sleep(3000);

						searchBox2 = chromeDriver.findElementByCssSelector("input.searchInput");
						js = String.format("arguments[0].value='%1$s %2$s,FRANCE'", codpostSoumis, communeSoumise);
						System.out.println(js);
						myExecutor3.executeScript(js, searchBox2);
						boutonOK = chromeDriver.findElementByCssSelector(
								"form[class=\"visaATMSearchWrap\"]>button[class=\"visaATMSubmit\"]");
						if (boutonOK != null) {
							js = String.format("arguments[0].click()", "");
							myExecutor2.executeScript(js, boutonOK);
						}
						Thread.sleep(3000);
						msgAlert = this.isAlertPresent(chromeDriver);
						bool = false;

					} catch (Exception e) {
						if (e instanceof UnhandledAlertException) {
							try {
								String alertCibled = " Le serveur est momentanément indisponible";
								Alert alert = chromeDriver.switchTo().alert();
								String alertMessage = alert.getText();
								alert.accept();
								if (alertMessage.contains(alertCibled)) {
									this.tearDown();
									this.setUp();
									chromeDriver.get(url_accueil);
									Thread.sleep(2000);
									msgAlert = this.isAlertPresent(chromeDriver);
									searchBox2 = chromeDriver.findElementByCssSelector("input.searchInput");
									js = String.format("arguments[0].value='%1$s %2$s,FRANCE'", codpostSoumis,
											communeSoumise);
									System.out.println(js);
									myExecutor3.executeScript(js, searchBox2);
									boutonOK = chromeDriver.findElementByCssSelector(
											"form[class=\"visaATMSearchWrap\"]>button[class=\"visaATMSubmit\"]");
									if (boutonOK != null) {
										js = String.format("arguments[0].click()", "");
										myExecutor2.executeScript(js, boutonOK);
									}
									BrowserUtils.closeAlertPopup(chromeDriver, false);
									bool = false;

								}
							} catch (Exception f) {
								if (f instanceof NoAlertPresentException) {
									f.printStackTrace();
								}
							}
						}
					}

				}
				Thread.sleep(4000);
				System.out.println("on scrap");
				System.out.println(msgAlert);
				if (!msgAlert) {
					html = chromeDriver.getPageSource();
					doc = Jsoup.parse(html);
					Element resultHtmlElement = doc.select("div[class=\"visaATMResultsInfo\"]").first();

					if (resultHtmlElement != null) {
						System.out.println("set");

						Elements listOfResult = null;
						Elements paginationHtmElement = resultHtmlElement
								.select("ol[class=\"visaATMPagination\"]>li>a[data-pagination]");
						PageNumbers = paginationHtmElement.size();
						System.out.println("le nombre de page est: " + PageNumbers);
						// fetch all information in page number one
						listOfResult = resultHtmlElement.select("ol>li[class*=\"visaATMResultListItem\"]");
						numpage = 1;
						this.fetchALLInfo(listOfResult);
						System.out.println("++++++++++++++++++++++++++++++");

						// fetch all information in the rest of page
						for (int i = 1; i < PageNumbers; i++) {
							numpage = i + 1;
							JavascriptExecutor myExecutor = (JavascriptExecutor) chromeDriver;
							try {
								js = String.format("arguments[0].click()", "");
								WebElement nextPageElement = chromeDriver
										.findElementByCssSelector("a[data-pagination=\"next\"]");
								myExecutor.executeScript(js, nextPageElement);

							} catch (Exception e) {
								if (e instanceof UnhandledAlertException) {
									try {
										String alertCibled = " Le serveur est momentanément indisponible";
										Alert alert = chromeDriver.switchTo().alert();
										String alertMessage = alert.getText();
										alert.accept();
										if (alertMessage.contains(alertCibled)) {
											js = String.format("arguments[0].click()", "");
											WebElement nextPageElement = chromeDriver
													.findElementByCssSelector("a[data-pagination=\"next\"]");
											myExecutor.executeScript(js, nextPageElement);

										}
										System.out.println(alertMessage);
									} catch (Exception f) {
										if (f instanceof NoAlertPresentException) {
											f.printStackTrace();
										}
									}
								}
							}
							Thread.sleep(4000);
							html = chromeDriver.getPageSource();
							doc = Jsoup.parse(html);
							resultHtmlElement = doc.select("div[class=\"visaATMResultsInfo\"]").first();
							listOfResult = resultHtmlElement.select("ol>li[class*=\"visaATMResultListItem\"]");
							this.fetchALLInfo(listOfResult);
							System.out.println("++++++++++++++++++++++++++++++");
						}
					}

				}

				log.writeLog(new Date(), communeSoumise, "no_error", AgenciesNumberPerPage, parcoursficval, 0);
			}
			parcoursficval++;

		}
		sc.close();
		return visaList;

		// }

	}

	public void fetchALLInfo(Elements listOfResult) {
		AgenciesNumberPerPage = listOfResult.size();
		int j = 0;
		String agencyHTM = "", agencyName = "", agencyVoie = "", agencyZIP = "", agencyCity = "";
		for (Element result : listOfResult) {
			j++;
			nbRecuperes_page = j;
			String agencyAddress = "";
			String[] agencyAddressArray = null, agencyAddressArray2 = null;
			// fetch agency Htm
			agencyHTM = result.html().replaceAll("\\s{1,}", "").trim();

			// fetch agency name
			Element agencyNameHtmlElement = result.select("p[class=\"visaATMPlaceLink\"]").first();
			agencyName = (agencyNameHtmlElement != null) ? agencyNameHtmlElement.text() : "";
			System.out.println("name: " + j + " " + agencyName);

			// fetch agency Addresse
			Element agencyAddresseHtmlElement = result.select("p[class=\"visaATMAddress\"]").first();
			agencyAddress = (agencyAddresseHtmlElement != null) ? agencyAddresseHtmlElement.html() : "";// replaceAll(";"
																										// , ","):"";
			agencyAddressArray = agencyAddress.split("<br>");
			agencyVoie = (agencyAddressArray[0] != null) ? agencyAddressArray[0].replaceAll("&nbsp;", "") : "";
			if (agencyAddressArray[1].contains("&nbsp;")) {
				agencyAddressArray[1] = agencyAddressArray[1].replaceAll("&nbsp;", "");
			}
			System.out.println("juste pour verifié l'adresse: " + agencyAddressArray[1]);
			agencyAddressArray2 = agencyAddressArray[1].split(",");
			agencyCity = (agencyAddressArray2[0] != null) ? agencyAddressArray2[0].replaceAll("<br>", "") : "";
			agencyZIP = (agencyAddressArray2[1] != null) ? agencyAddressArray2[1] : "";
			System.out.println(agencyVoie + " " + agencyCity + " " + agencyZIP);
			
			visaList.add(this.setData(codpostSoumis, String.valueOf(PageNumbers), String.valueOf(numpage),
					String.valueOf(AgenciesNumberPerPage), agencyName, agencyVoie, agencyZIP, agencyCity,
					String.valueOf(DAB), agencyHTM));
		}

	}

	
	public void tearDown() {
		chromeDriver.close();
		chromeDriver.quit();

	}

	public  boolean isAlertPresent(WebDriver wd) {
		try {
			Alert alert = wd.switchTo().alert();
			alert.accept();

			return true;
		} catch (NoAlertPresentException e) {
			return false;
		} finally {

		}
	}

}
