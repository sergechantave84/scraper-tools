package com.app.service;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;

import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Scanner;

import javax.net.ssl.HttpsURLConnection;

import org.apache.commons.text.StringEscapeUtils;
import org.json.JSONObject;

import org.jsoup.nodes.Element;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.app.model.BNP;

import com.app.scrap.RepBNP;

import com.app.utils.Parametres;
import com.app.utils.Utilitaires;

import fa.browserUtils.FactoryBrowser;
import fa.browserUtils.HtmlUnitBrowser;
import fa.log.LogApp.LogScrap;
import fa.log.config.LogConst;

@Service
public class ServiceBnp extends Utilitaires {

	@Autowired
	private RepBNP rep;

	private String nomSITE = "BNP";

	private List<BNP> bnpList = new ArrayList<>();
	private String ficval = "bnp_code";
	private String dossierRESU = Parametres.REPERTOIRE_RESULT + "\\BQ\\" + nomSITE;
	// https://agences.bnpparibas/engineV2/?media=mobile&bank=BNPP&lang=FR&enc=json&crit=agence&nbag=5&ag=allpoi
	//private String strURL = "https://agences.bnpparibas/engineV2/?media=mobile&bank=BNPP&lang=FR&enc=json&crit=agence&nbag=5&ag=";
	private String strURL ="https://agences.mabanque.bnpparibas/engineV2/?media=mobile&bank=BNPP&lang=FR&enc=json&crit=agence&nbag=5&ag=";
	//private String strURL = "https://mabanque.bnpparibas/fr/nous-contacter/nous-trouver/trouver-une-agence";
	private String fichier_valeurs;

	private String ValeurSoumise;
	// private String ligne;

	private static HtmlUnitDriver htmlUnitDriver;
	private BufferedWriter sortie;
	private BufferedWriter sortie_log;

	private int nbAgences;
	private int nbRecuperes;
	private int num_ligneficval;
	private int parcoursficval;

	public BNP saveData(BNP t) {

		return rep.save((BNP) t);

	}

	public List<BNP> saveDataAll(List<BNP> t) {

		return rep.saveAll(t);

	}

	public List<BNP> getElmnt() {

		return (List<BNP>) rep.findAll();
	}

	public BNP getById(int id) {

		return rep.findById(id).orElse(null);
	}

	public String deleteProduct(int id) {
		try {
			rep.deleteById(id);

		} catch (Exception e) {
			e.printStackTrace();
		}

		return "removed";
	}
	
	public String deleteAll() {
		try {
			
			rep.deleteAll();
			System.out.println("Supprimee avec success");

		} catch (Exception e) {
			e.printStackTrace();
		}

		return "removed";
	}

	public BNP updateProduct(BNP t) {
		// BNPODO Auto-generated method stub
		return null;
	}

	public BNP setData(String valeurSoumise, String nbAgences, String nomWeb, String formatAgence, String adresse,
			String adresseComp, String cp, String ville, String tel, String fax, String horaires, String gab,
			String particulier, String professionel, String entreprise, String change, String coffreFort,
			String depotPermanent, String automateBillet, String automateCheque, String latitude, String longitude, String horairesRDV) {
		BNP bnp = new BNP();
		bnp.setValeurSoumise(valeurSoumise).setNbAgences(nbAgences).setNomWeb(nomWeb).setFormatAgence(formatAgence)
				.setAdresse(adresse).setAdresseComp(adresseComp).setCp(cp).setVille(ville).setTel(tel).setFax(fax)
				.setHoraires(horaires).setGab(gab).setParticulier(particulier).setProfessionel(professionel)
				.setEntreprise(entreprise).setChange(change).setCoffreFort(coffreFort).setDepotPermanent(depotPermanent)
				.setAutomateBillet(automateBillet).setAutomateCheque(automateCheque).setLatitude(latitude)
				.setLongitude(longitude).setHoraires_rdv(horairesRDV);
		this.saveData(bnp);
		return bnp;
	}

	public List<BNP> showFirstRows() {
		List<BNP> a = rep.getFirstRow(100, BNP.class);
		return a;
	}

	public List<BNP> showLastRows() {
		List<BNP> a = rep.getLastRow(100, BNP.class);
		return a;
	}

	public void setup() {
		FactoryBrowser factB = new FactoryBrowser();
		HtmlUnitBrowser htmlB = (HtmlUnitBrowser) factB.create("htmlUnit");
		htmlUnitDriver = htmlB.setOptions(true, false, false, 12000).initBrowserDriver();
	}

	public List<BNP> scrapBNP() throws IOException {
		fichier_valeurs = Parametres.REPERTOIRE_DATA_SOURCES + ficval + ".txt";
		Scanner sc = new Scanner(new BufferedInputStream(new FileInputStream(new File(fichier_valeurs))));

		if (!new File(dossierRESU).exists()) {
			new File(dossierRESU).mkdirs();
		}
		fichier = new File(dossierRESU + "\\" + "RESULTATS_" + nomSITE + "_" + System.currentTimeMillis() + ".txt");
		LogScrap log = new LogScrap("BanqueBnp.txt", LogConst.TYPE_LOG_FILE);
		num_ligneficval = ((new File(LogConst.STORE_LOG_PATH + LogConst.PREFIX_LOG_FILE + "_BanqueBnp.txt")).exists())
				? Integer.parseInt(log.readLog().getLastLineProcessed().split("\\t")[4])
				: 1;

		parcoursficval = 1;

		Element jsonJElement = null;
		while (sc.hasNextLine()) {

			String[] lines = sc.nextLine().split(",");
			if (parcoursficval >= num_ligneficval) {
				URL url = new URL(strURL + lines[1]);
				HttpsURLConnection con = (HttpsURLConnection) url.openConnection();
				con.setRequestMethod("GET");
				con.setRequestProperty("Content-Type", "application/json; charset=utf-8");
				con.setRequestProperty("Content-Encoding", "gzip");
				con.setRequestProperty("Vary", "Accept-Encoding");
				con.setRequestProperty("Content-Length", "0");
				con.setConnectTimeout(60000);
				con.setReadTimeout(60000);
				con.setInstanceFollowRedirects(true);
				BufferedReader in = new BufferedReader(
						new InputStreamReader(con.getInputStream(), StandardCharsets.UTF_8));
				String pageSource;
				StringBuilder strB = new StringBuilder();
				while ((pageSource = in.readLine()) != null) {
					strB.append(pageSource);
				}

				String json = StringEscapeUtils.unescapeJava(strB.toString());
				JSONObject root = new JSONObject(json);
				if (root.has("agence")) {
					String nomAgence = "", formatAgence = "", adresse = "", adresse_comp = "", CP = "", ville = "",
							tel = "", fax = "", horaires = "", horairesRDV = "", GAB = "", particulier = "", professionnel = "",
							entreprise = "", change = "", coffre_fort = "", depot_permanent = "", automate_billet = "",
							latitude = "", longitude = "", automate_cheque = "";

					try {

						nbRecuperes = 0;
						nbAgences = 1;

						JSONObject jsonobject = root.getJSONObject("agence");

						nomAgence = jsonobject.getString("nom");
						nomAgence = this.normalize(nomAgence);
						System.out.println("nom : " + nomAgence);

						formatAgence = jsonobject.getString("format");
						System.out.println("format : " + formatAgence);

						adresse = this.normalize(jsonobject.getString("adresse"));
						System.out.println("adresse : " + adresse);
						adresse_comp = this.normalize(jsonobject.getString("adresse2"));
						System.out.println("adresse_comp : " + adresse_comp);
						CP = this.normalize(jsonobject.getString("cp"));
						System.out.println("cp : " + CP);
						ville = this.normalize(jsonobject.getString("ville"));
						System.out.println("ville : " + ville);
						tel = jsonobject.getString("telephone");
						System.out.println("tel : " + tel);
						fax = jsonobject.getString("fax");
						System.out.println("fax : " + fax);
						horaires = this.normalize(jsonobject.getString("text1").replaceAll("\\.", "").replaceAll("<br/>", " "));
						System.out.println("horaires : " + horaires);
						
						horairesRDV = this.normalize(jsonobject.getString("text3").replaceAll("\\.", "").replaceAll("<br/>", " "));
						System.out.println("horairesRDV : " + horairesRDV);
						
						JSONObject services = jsonobject.getJSONObject("services");
						GAB = services.getString("gab");
						System.out.println("GAB : " + GAB);
						particulier = this.normalize(services.getString("particulier"));
						System.out.println("particulier : " + particulier);
						professionnel = this.normalize(services.getString("professionnel"));
						System.out.println("professionnel : " + professionnel);
						entreprise = this.normalize(services.getString("entreprise"));
						System.out.println("  entreprise : " + entreprise);
						change = this.normalize(services.getString("change"));
						System.out.println("change : " + change);
						coffre_fort = this.normalize(services.getString("coffre_fort"));
						System.out.println("coffre_fort : " + coffre_fort);
						depot_permanent = this.normalize(services.getString("depot_permanent"));
						System.out.println("depot_permanent : " + depot_permanent);
						automate_billet = this.normalize(services.getString("automate_billet"));
						System.out.println("automate_billet : " + automate_billet);
						automate_cheque = this.normalize(services.getString("automate_cheque"));
						System.out.println("automate_cheque : " + automate_cheque);
						latitude = jsonobject.getString("latitude");
						System.out.println("lat : " + latitude);
						longitude = jsonobject.getString("longitude");
						System.out.println("lon : " + longitude);

						nbRecuperes++;
						BNP bnp = this.setData(ValeurSoumise, String.valueOf(nbAgences), nomAgence, formatAgence,
								adresse, adresse_comp, CP, ville, tel, fax, horaires, GAB, particulier, professionnel,
								entreprise, change, coffre_fort, depot_permanent, automate_billet, automate_cheque,
								latitude, longitude, horairesRDV);

						// }
						System.out.println("++++++++++++++");
						// }
					} catch (Exception e) {
						System.out.println(e.getMessage());
					}

				}
				log.writeLog(new Date(), ValeurSoumise, "no_error", nbAgences, parcoursficval, nbAgences - nbRecuperes);
			}
			parcoursficval++;
		}
		return bnpList;
	}

	public String cleanString(String str) {
		int firstIndex = str.indexOf(".", 0);
		int lastIndex = 0;
		StringBuilder sb = null;
		String result = "";
		while (firstIndex > 0) {
			int tmp = firstIndex;
			firstIndex = str.indexOf(".", tmp + 1);
			if (firstIndex > 0) {
				lastIndex = firstIndex;
				sb = new StringBuilder(str);
				sb.setCharAt(lastIndex, ' ');
			}

		}
		if (sb != null) {
			return result = sb.toString().replaceAll(" ", "");

		} else {
			return str;
		}

	}

	public void enregistrer(String nomAgence, String formatAgence, String adresse, String adresse_comp, String CP,
			String ville, String tel, String fax, String horaires, String GAB, String particulier, String professionnel,
			String entreprise, String change, String coffre_fort, String depot_permanent, String automate_billet,
			String automate_cheque, String latitude, String longitude) {

		try {

			sortie = new BufferedWriter(new FileWriter(fichier, true));
			sortie.write(ValeurSoumise + "\t" + this.nbAgences + "\t" + nomAgence + "\t" + formatAgence + "\t" + adresse
					+ "\t" + adresse_comp + "\t" + CP + "\t" + ville + "\t" + tel + "\t" + fax + "\t" + horaires + "\t"
					+ GAB + "\t" + particulier + "\t" + professionnel + "\t" + entreprise + "\t" + change + "\t"
					+ coffre_fort + "\t" + depot_permanent + "\t" + automate_billet + "\t" + automate_cheque + "\t"
					+ latitude + "\t" + longitude + "\r\n");

			sortie.close();
		} catch (IOException e) {

			e.printStackTrace();
		}

	}

	/**
	 * La methode <code>initialiser()</code> permet d'ajouter la ligne d'ent�te dans
	 * le fichier r�sultat.
	 */

	public void initialiser() {

		try {
			// Attention pas d'accent dans les noms de colonnes !
			sortie = new BufferedWriter(new FileWriter(fichier, true));
			sortie.write("ValeurSoumise" + "\t" + "NbAgences" + "\t" + "NOMWEB" + "\t" + "formatAgence" + "\t"
					+ "adresse" + "\t" + "adresse_comp" + "\t" + "CP" + "\t" + "ville" + "\t" + "tel" + "\t" + "fax"
					+ "\t" + "horaires" + "\t" + "GAB" + "\t" + "particulier" + "\t" + "professionnel" + "\t"
					+ "entreprise" + "\t" + "change" + "\t" + "coffre_fort" + "\t" + "depot_permanent" + "\t"
					+ "automate_billet" + "\t" + "automate_cheque" + "\t" + "latitude" + "\t" + "longitude" + "\r\n");
			sortie.close();

			File toto = new File(dossierRESU + "\\" + "LOG_" + nomSITE + ".csv");
			if (!toto.exists()) {
				// if (num_ligneficval == num_ligne) {
				sortie_log = new BufferedWriter(new FileWriter(log, true));
				sortie_log.write("NumLigne" + ";" + "FinValeur" + ";" + "ValeurSoumise" + ";" + "NbAgences" + ";"
						+ "Nbrecuperes" + ";" + "Reste" + ";" + "Heure_Fin" + "\r\n");
				sortie_log.close();
			}

		} catch (IOException e) {

			e.printStackTrace();
		}
	}

	public void tearsDown() {
		htmlUnitDriver.quit();
	}
	
}
