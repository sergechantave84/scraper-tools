package com.app.service;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.SocketTimeoutException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Scanner;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.PageLoadStrategy;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.app.model.LCL_Outre_Mer;

import com.app.scrap.RepLcl;
import com.app.scrap.RepLclOutreMer;
import com.app.utils.BrowserUtils;
import com.app.utils.Parametres;
import com.app.utils.Utilitaires;

import fa.browserUtils.ChromeBrowser;
import fa.browserUtils.FactoryBrowser;
import fa.log.LogApp.LogScrap;
import fa.log.config.LogConst;

@Service
public class ServiceLcl_Outre_Mer extends Utilitaires {

	@Autowired
	private RepLclOutreMer rep;
	
	//static String driverPath = "C:\\ChromeDriver\\";

	static String driverPath = "C:\\chromeDriver\\";

	private List<LCL_Outre_Mer> lclList = new ArrayList<LCL_Outre_Mer>();
	String nomSITE = "LCL_Outre_Mer";
	String ficval = Parametres.OUTRE_MER_NOM_FICHIER;
	String dossierRESU = Parametres.REPERTOIRE_RESULT + "\\BQ\\" + nomSITE;
	String url = "https://www.lcl.fr/agence-banque";
	String fichier_valeurs;
	StringBuffer resultat;
	String ValeurSoumise;
	String ligne;

	int nbAgences;
	int nbRecuperes;
	int num_ligneficval = 1;
	int parcoursficval;
	int num_ligne = 2;
	int nbLiens;

	static WebDriver chromeDriver;
	FileOutputStream fos;
	BufferedWriter sortie;
	BufferedWriter sortie_log;

	Date Temps_Fin;

	public LCL_Outre_Mer saveData(LCL_Outre_Mer t) {

		return rep.save(t);

	}

	public List<LCL_Outre_Mer> saveDataAll(List<LCL_Outre_Mer> t) {

		return rep.saveAll(t);

	}

	public List<LCL_Outre_Mer> getElmnt() {

		return rep.findAll();
	}

	public LCL_Outre_Mer getById(int id) {

		return rep.findById(id).orElse(null);
	}

	public String deleteProduct(int id) {
		try {
			rep.deleteById(id);

		} catch (Exception e) {
			e.printStackTrace();
		}

		return "removed";
	}

	public LCL_Outre_Mer updateProduct(LCL_Outre_Mer t) {
		// LCL_Outre_MerODO Auto-generated method stub
		return null;
	}

	public LCL_Outre_Mer setData(String valeurSoumise, String nbAgences, String nbLiensLCL_Outre_Merrouves,
			String numAgence, String nomWeb, String adresse, String tel, String fax, String horairesHtm,
			String horairesLCL_Outre_Merxt, String services, String agenceHtm) {
		LCL_Outre_Mer lcl = new LCL_Outre_Mer();
		lcl.setValeurSoumise(valeurSoumise).setNbAgences(nbAgences).setNbLiensTrouves(nbLiensLCL_Outre_Merrouves)
				.setNumAgence(numAgence).setNomWeb(nomWeb).setAdresse(adresse).setTel(tel).setFax(fax)
				.setHorairesHtm(horairesHtm).setHorairesTxt(horairesLCL_Outre_Merxt).setServices(services)
				.setAgenceHtm(agenceHtm);
		this.saveData(lcl);
		return lcl;
	}

	public List<LCL_Outre_Mer> showFirstRows() {
		List<LCL_Outre_Mer> a = rep.getFirstRow(100, LCL_Outre_Mer.class);
		return a;
	}

	public List<LCL_Outre_Mer> showLastRows() {
		List<LCL_Outre_Mer> a = rep.getLastRow(100, LCL_Outre_Mer.class);
		return a;
	}

	public void setup() {
		
		FactoryBrowser factBrowser = new FactoryBrowser();
		ChromeBrowser b = (ChromeBrowser) factBrowser.create("chrome");
		final String binaryPath = "C:\\Program Files\\Google\\Chrome Beta\\Application\\chrome.exe";
		
		chromeDriver = b.setOptions(PageLoadStrategy.NORMAL, driverPath, 60000, 60000, 60000, false, false, binaryPath)
				.initBrowserDriver();

		//chromeDriver = BrowserUtils.chromeConstruct(driverPath, 60000, 60000, 10, PageLoadStrategy.NORMAL);
		//chromeDriver = this.getDriver();
	}
	
	public WebDriver getDriver() {
		System.setProperty("webdriver.chrome.driver", driverPath+"chromedriver.exe");
		ChromeOptions options = new ChromeOptions();
		options.addArguments("start-maximized");
		options.addArguments("--remote-allow-origins=*");
		options.setPageLoadStrategy(PageLoadStrategy.NORMAL);

		WebDriver driver = new ChromeDriver(options);
		return driver;
	}

	public void letScrapLCL() throws IOException, InterruptedException {
		boolean documentReadyState = false;
		fichier_valeurs = Parametres.REPERTOIRE_DATA_SOURCES + ficval + ".txt";
		Scanner sc = new Scanner(new File(fichier_valeurs));
		File f = new File(dossierRESU);
		if (!f.exists()) {
			f.mkdirs();
		}
		fichier = new File(dossierRESU + "\\" + "RESULTATS_" + nomSITE + "_" + System.currentTimeMillis() + ".txt");
		//NANTE
		LogScrap log = new LogScrap("BanqueLCL_DOM.txt", LogConst.TYPE_LOG_FILE);
		//log = new File(dossierRESU + "\\" + "LOG_" + nomSITE + ".csv");
		File toto = new File(dossierRESU + "\\" + "LOG_" + nomSITE + ".csv");
		
		num_ligneficval = ((new File(LogConst.STORE_LOG_PATH + LogConst.PREFIX_LOG_FILE + "_BanqueLCL_DOM.txt")).exists())
				? Integer.parseInt(log.readLog().getLastLineProcessed().split("\\t")[4])
				: 1;

		parcoursficval = 0;

		while (sc.hasNextLine()) {
			String line = sc.nextLine();
			String[] fic_valeurs = line.split("\\t");
			if (parcoursficval >= num_ligneficval) {
				
				nbAgences = 0;
				
				ValeurSoumise = fic_valeurs[0];
				
				System.out.println("CP : " + ValeurSoumise);

				this.accesWebSite();
				documentReadyState = this.listensIfPageFullyLoaded(chromeDriver);
				if (documentReadyState) {
					
					Thread.sleep(2000);
					
					this.submitRequest();

					documentReadyState = this.listensIfPageFullyLoaded(chromeDriver);
					
					if (documentReadyState) {
						List<WebElement> resultList = chromeDriver.findElements(
								By.cssSelector("app-agency-list>div"));
						int listSize = resultList.size();
						if (listSize > 0) {
							nbAgences = listSize;
							nbLiens = listSize;
							System.out.println("nbAgences : " + nbAgences);
							boolean isPresent = false;
							
							if (listSize > 0) {
								Document doc = null;
								for (int i = 0; i < listSize; i++) {
									JavascriptExecutor js = (JavascriptExecutor) chromeDriver;

									int index = i + 1;

									/*
									 * String s = String.format(
									 * "let l=document.getElementsByClassName('result-content').lenght; let b=( l > 0)? true: false ; return b"
									 * , ""); isPresent = (boolean) js.executeScript(s); if (isPresent) {
									 */
									/*WebElement result = chromeDriver.findElement(By.cssSelector("app-agency-list > div:nth-child(" + index + ")"));
									if(result != null)
										result.click();*/
									
									String script = String.format(
											"const result=document.querySelector(\"app-agency-list > div:nth-child(" + index + ")\"); if(result != null){result.click()};",
											"");
									js.executeScript(script);
									
									Thread.sleep(5000);

									script = String.format(
											"let l=document.getElementsByClassName(`result ng-star-inserted`).length; let b=( l > 0)? true: false ; return b",
											"");
									isPresent = (boolean) js.executeScript(script);
									System.out.println(isPresent);
									if (isPresent) {
										WebElement resultContainer = chromeDriver
												.findElement(By.cssSelector("slice-agency-details"));
										if(resultContainer != null) {
											
											String html = resultContainer.getAttribute("innerHTML");
											doc = Jsoup.parse(html);
											if (doc != null) {
												String agences_HTM = "", nom_agence = "", adresse = "", tel = "", fax = "",
														HorairesHTM = "", HorairesTXT = "", Services = "";
												Element root = doc.selectFirst("div.result-infos");
												if (root != null) {
													agences_HTM = html.replaceAll("\\s{1,}", " ");
													
													// retrieve agency name
													Element nameElement = doc.selectFirst("cb-fullwidth-background > div > h2");
													nom_agence = (nameElement != null) ? this.normalize(nameElement.text().replaceAll("\\s{2,}", " ").trim()) : "";
													nom_agence = nom_agence.toUpperCase();
													System.out.println("le nom de l'agence est : " + nom_agence);
													Element blocCoordinateElmnt = root
															.selectFirst("div.infos-container>.agency-details");
													if (blocCoordinateElmnt != null) {
														Element adresseElement = blocCoordinateElmnt.selectFirst(".info-address");
														adresse = (adresseElement != null) ? this.normalize(adresseElement.text().replaceAll("\\s{2,}", " ").trim()) : "";
														adresse = adresse.toUpperCase().replaceAll(",", "");
														adresse = adresse.replaceAll("\\s{1,}", " ");
														System.out.println("l'adresse de l'agence est : " + adresse);
														Element telElement = blocCoordinateElmnt.selectFirst(".info-phone");
														tel = (telElement != null) ? telElement.text().replaceAll("[^0-9 ]", "")
																: "";
														System.out.println("le tel de l'agence est : " + tel);
														Element faxElement = blocCoordinateElmnt.selectFirst(".info-fax");
														fax = (faxElement != null) ? faxElement.text().replaceAll("[^0-9 ]", "")
																: "";
														System.out.println("le fax de l'agence est : " + fax);
													}
													Element scheduleElement = doc
															.selectFirst("app-agency-schedule > table.table");
													if (scheduleElement != null) {
														HorairesTXT = scheduleElement.text();
													}
													HorairesTXT = this.normalize(HorairesTXT.replaceAll("\\s{2,}", " ").trim());
													HorairesTXT = HorairesTXT.toLowerCase();
													System.out.println("les horaires de l'agence sont : " + HorairesTXT);
												}
												nbRecuperes++;
												
												this.setData(ValeurSoumise, String.valueOf(nbAgences), String.valueOf(nbLiens),
													String.valueOf(nbRecuperes), nom_agence, adresse, tel, fax, HorairesHTM,
													HorairesTXT, Services, agences_HTM);
												
												
											}
										}

									}
									this.accesWebSite();
									this.submitRequest();
									// }

								}

							}

						}else {
							System.out.println("Aucun résultat");
						}

					}else {
						System.out.println("Aucun list à scrapper");
					}

				}
				
				log.writeLog(new Date(), ValeurSoumise, "no_errors", nbAgences, parcoursficval, 0);

			}
			
			System.out.println("parcoursficval : " + parcoursficval);

			parcoursficval++;
		}
		sc.close();
	}

	public void accesWebSite() {
		try {

			chromeDriver.get(url);

		} catch (Exception e) {
			if (e instanceof SocketTimeoutException) {
				chromeDriver.get(url);

			}
		}
	}

	public void submitRequest() throws InterruptedException {
		
		WebElement input = chromeDriver.findElement(By.cssSelector(
				"input.form-field-input"));
		input.sendKeys(ValeurSoumise, Keys.ENTER);
		Thread.sleep(3000);
		/*JavascriptExecutor js = (JavascriptExecutor) chromeDriver;
		String script = String.format(
				"let l=document.getElementsByClassName(`result ng-star-inserted`).length; let b=( l > 0)? true: false ; return b",
				"");
		boolean isPresent = (boolean) js.executeScript(script);
		System.out.println("element choise " + isPresent);*/
		/*if (isPresent) {
			try {
				chromeDriver.findElement(By.cssSelector(
						"body > app-root > app-cms > cms-page > dynamic-page > cb-slices > slice-agency-map-hero > "
								+ "cb-fullwidth-background > div > div > app-agency-form > form > div > div > div:nth-child(3) > div > button"))
						.click();
			} catch (Exception e) {
				// TODO: handle exception
			}
		}*/
		/*try {
			WebElement btnSubmit = chromeDriver.findElement(By.cssSelector(
					"form cb-cta-btn-link > button"));
			btnSubmit.click();
		} catch (Exception e) {

		}*/
		/*WebElement btnSubmit = chromeDriver.findElement(By.cssSelector(
				"app-agency-form > form > div > cb-cta-btn-link > button"));
		btnSubmit.click();*/

	}

	public void tearDown() {
		chromeDriver.quit();
	}

	public void enregistrer(String agences_HTM, String nom_agence, String adresse, String tel, String fax,
			String HorairesHTM, String HorairesTXT, String Services, int numreponse) {

		try {

			sortie = new BufferedWriter(new FileWriter(fichier, true));
			sortie.write(ValeurSoumise + "\t" + this.nbAgences + "\t" + nbLiens + "\t" + numreponse + "\t" + nom_agence
					+ "\t" + adresse + "\t" + tel + "\t" + fax + "\t" + HorairesHTM + "\t" + HorairesTXT + "\t"
					+ Services + "\t" + agences_HTM + "\r\n");

			sortie.close();
		} catch (IOException e) {

			e.printStackTrace();
		}

	}

	public void enregistrer_journal(int numreponse) {

//    	this.fichier = new File(Parametres.REPERTOIRE + "RESULTATS_LCL_Outre_Mer.txt");
		try {
			Temps_Fin = new Date();
			SimpleDateFormat formater = null;
			formater = new SimpleDateFormat("'le' dd/MM/yyyy '�' hh:mm:ss");
			String dateTXT = formater.format(Temps_Fin);
			dateTXT = formater.format(Temps_Fin);

			sortie_log = new BufferedWriter(new FileWriter(log, true));
			sortie_log.write(parcoursficval + ";" + "FinValeur" + ";" + ValeurSoumise + ";" + this.nbAgences + ";"
					+ nbRecuperes + ";" + (this.nbAgences - nbRecuperes) + ";" + dateTXT + "\r\n");
			sortie_log.close();
			long taille_fichier = fichier.length() / 1000000; // la length est en octets, on divise par 1000 pour mettre
																// en Ko et par 1000000 pour mettre en Mo

			if (taille_fichier > 4) {
				// INITIALISATION FICHIER DE RESULTAT
				this.fichier = new File(
						dossierRESU + "\\" + "RESULTATS_" + nomSITE + "_" + System.currentTimeMillis() + ".txt");
				// Attention pas d'accent dans les noms de colonnes !
				sortie = new BufferedWriter(new FileWriter(fichier, true));
				sortie.write("ValeurSoumise" + "\t" + "NbAgences" + "\t" + "NbLiensTrouves" + "\t" + "numAgence" + "\t"
						+ "NOMWEB" + "\t" + "ADRESSE" + "\t" + "TEL" + "\t" + "FAX" + "\t" + "HORAIRESHTM" + "\t"
						+ "HORAIRESTXT" + "\t" + "SERVICES" + "\t" + "AGENCEHTM" + "\r\n");
				sortie.close();
			}

		} catch (IOException e) {

			e.printStackTrace();
		}

	}

	/**
	 * La m�thode <code>initialiser()</code> permet d'ajouter la ligne d'ent�te dans
	 * le fichier r�sultat.
	 */

	public void initialiser() {

		try {
			// Attention pas d'accent dans les noms de colonnes !
			sortie = new BufferedWriter(new FileWriter(fichier, true));
			sortie.write("ValeurSoumise" + "\t" + "NbAgences" + "\t" + "NbLiensTrouves" + "\t" + "numAgence" + "\t"
					+ "NOMWEB" + "\t" + "ADRESSE" + "\t" + "TEL" + "\t" + "FAX" + "\t" + "HORAIRESHTM" + "\t"
					+ "HORAIRESTXT" + "\t" + "SERVICES" + "\t" + "AGENCEHTM" + "\r\n");
			sortie.close();

			File toto = new File(dossierRESU + "\\" + "LOG_" + nomSITE + ".csv");
			if (!toto.exists()) {
				// if (num_ligneficval == num_ligne) {
				sortie_log = new BufferedWriter(new FileWriter(log, true));
				sortie_log.write("NumLigne" + ";" + "FinValeur" + ";" + "ValeurSoumise" + ";" + "NbAgences" + ";"
						+ "Nbrecuperes" + ";" + "Reste" + ";" + "Heure_Fin" + "\r\n");
				sortie_log.close();
			}

		} catch (IOException e) {

			e.printStackTrace();
		}
	}

}