package com.app.scrap;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.app.model.CDN;

@Repository
public interface RepCDN extends JpaRepository<CDN,Integer>,RepositoryCustom<CDN>{

}
