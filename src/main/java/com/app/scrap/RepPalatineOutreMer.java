package com.app.scrap;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.app.model.Palatine_Outre_Mer;

@Repository
public interface RepPalatineOutreMer extends JpaRepository<Palatine_Outre_Mer, Integer>,RepositoryCustom<Palatine_Outre_Mer>{

}
