package com.app.service;

import java.io.BufferedInputStream;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;

import java.net.SocketTimeoutException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Scanner;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.app.model.Mutuelle_Generale;

import com.app.scrap.RepMutuGenerale;

import com.app.utils.Parametres;
import com.app.utils.Utilitaires;

import fa.browserUtils.FactoryBrowser;
import fa.browserUtils.HtmlUnitBrowser;
import fa.log.LogApp.LogScrap;
import fa.log.config.LogConst;

@Service
public class ServiceMutuGenerale extends Utilitaires {

	@Autowired
	private RepMutuGenerale rep;

	private HtmlUnitDriver htmlUnitDriver;
	private List<Mutuelle_Generale> mutGeneraleList = new ArrayList<>();

	public Mutuelle_Generale setData(String lienAgence, String nbAgences, String nomAgence, String adresse, String tel,
			String horaires, String agenceHtm) {
		Mutuelle_Generale mg = new Mutuelle_Generale();
		mg.setLienAgence(lienAgence).setNbAgences(nbAgences).setNomAgence(nomAgence).setAdresse(adresse).setTel(tel)
				.setHoraires(horaires).setAgenceHtm(agenceHtm);
		rep.save(mg);
		return mg;
	}

	public void setup() {
		FactoryBrowser factBrowser = new FactoryBrowser();
		HtmlUnitBrowser hB = (HtmlUnitBrowser) factBrowser.create("htmlUnit");
		htmlUnitDriver = hB.setOptions(false, false, false, 60000).initBrowserDriver();
	}

	public void scrapMGeneral() throws IOException {

		int nombreAgences = 0;

		int counter = 1;
		int curentLine = 1;
		Document doc = null;
		Element root = null;
		Element agencyNameElmnt = null;
		Element addressElmnt = null;
		Elements scheduleElmnts = null;

		LogScrap log = new LogScrap("BanqueMutuelleG.txt", LogConst.TYPE_LOG_FILE);
		curentLine = ((new File(LogConst.STORE_LOG_PATH + LogConst.PREFIX_LOG_FILE + "_BanqueMutuelleG.txt")).exists())
				? Integer.parseInt(log.readLog().getLastLineProcessed().split("\\t")[4])
				: 1;
		System.out.println("On est là ");
		Scanner sc = new Scanner(new BufferedInputStream(new FileInputStream(new File(Parametres.REPERTOIRE_DATA_SOURCES + "lien_mutuG.txt"))));
		while (sc.hasNextLine()) {
			String lines[] = sc.nextLine().split("\\t");
			if (counter > curentLine) {
				String url = lines[1];
				try {
					htmlUnitDriver.get(url);
				} catch (Exception e) {
					if (e instanceof SocketTimeoutException) {
						htmlUnitDriver.get(url);
					}
				}
				System.out.println("url : " + url);
				String pageSource = htmlUnitDriver.getPageSource();
				doc = Jsoup.parse(pageSource);
				//#content > div
				root = doc.selectFirst("#content > div.section");
				if (root != null) {

					String lienAgence = "";
					String nomAgence = "";
					String adresse = "";
					String telephone = "";
					String horaires = "";
					String agenceHtml = "";

					agenceHtml = root.outerHtml();
					agenceHtml = this.normalize(agenceHtml.replaceAll("\\s{2,}", "").trim());

					// retrieve agency name
					agencyNameElmnt = doc.selectFirst("#block-lmg-corporate-page-title > div > h1 > span");
					nomAgence = (agencyNameElmnt != null)
							? this.normalize(agencyNameElmnt.ownText().replaceAll("\\s{2,}", " ").trim())
							: "";
					System.out.println("nom : " + nomAgence);

					// retrieve agency address
					addressElmnt = root.selectFirst(
							"article > div > div.col-xl-8.col-12 > div:nth-child(4) > div > div.field__item > p");
					adresse = (addressElmnt != null)
							? this.normalize(addressElmnt.text().replaceAll("\\s{2,}", " ").trim())
							: "";
					System.out.println("adresse : " + adresse);

					// retrieve agency telephone
					telephone = "3035";

					// retrieve agency schedule
					//#content > section > article > div > div.col-xl-8.col-12 > div:nth-child(6)
					scheduleElmnts = root.select("article > div > div.col-xl-8.col-12 > div:nth-child(6)");
					for (Element tmp : scheduleElmnts) {
						horaires += tmp.text().replaceAll("\\s{2,}", " ").trim() + " ";
					}
					horaires = this.normalize(horaires);
					System.out.println("horaires : " + horaires);
					System.out.println("--------------------------\n" );

					/*this.setData(lienAgence, String.valueOf(nombreAgences), nomAgence, adresse, telephone, horaires,
							agenceHtml);*/
				}

				//log.writeLog(new Date(), lines[1], "no_error", 1, counter, 0);

			}
			counter++;

		}
		sc.close();

	}

	public void tearsDown() {
		htmlUnitDriver.quit();
	}
}
