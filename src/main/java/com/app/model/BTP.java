package com.app.model;

import java.text.SimpleDateFormat;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="BTP_banque",schema="scrap")
public class BTP {
	@Id
	@SequenceGenerator(name="scrap.BTP_banque_id_seq",
	                   sequenceName ="scrap.BTP_banque_id_seq",
	                   allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator="scrap_BTP_banque_id_seq")
	
	@Column(name ="id",updatable = false)
	private int id;
	
	@Column(name="nom_agence", columnDefinition = "varchar")
	private String name;
	
	@Column(name="horaires", columnDefinition = "varchar")
	private String schedule;
	
	@Column(name="tel", columnDefinition = "varchar")
	private String tel;
	
	@Column(name="adresse", columnDefinition = "varchar")
	private String address;
	
	@Column(name="annee_du_scrap" ,columnDefinition="varchar(4)")
	private String anneeScrap;
	
	@Column(name="libre_service", columnDefinition = "varchar")
	private String freeService;
	
	@Column(name="agence_type",columnDefinition = "varchar")
	private String agenceType;
	
	@Column(name="valeur_soumis", columnDefinition = "varchar")
	private String valSoumise;
	
	public BTP() {
		SimpleDateFormat f=null;
		Date d=new Date();
		f=new SimpleDateFormat("yyyy");
		anneeScrap=f.format(d).toString();
	}

	public String getName() {
		return name;
	}

	public BTP setName(String name) {
		this.name = name;
		return this;
	}

	public String getSchedule() {
		return schedule;
	}

	public BTP setSchedule(String schedule) {
		this.schedule = schedule;
		return this;
	}

	public String getTel() {
		return tel;
	}

	public BTP setTel(String tel) {
		this.tel = tel;
		return this;
	}

	public String getAddress() {
		return address;
	}

	public BTP setAddress(String address) {
		this.address = address;
		return this;
	}

	public String getValSoumise() {
		return valSoumise;
	}

	public BTP setValSoumise(String valSoumise) {
		this.valSoumise = valSoumise;
		return this;
	}

	public String getFreeService() {
		return freeService;
	}

	public BTP setFreeService(String freeService) {
		this.freeService = freeService;
		return this;
	}

	public String getAgenceType() {
		return agenceType;
	}

	public BTP setAgenceType(String agenceType) {
		this.agenceType = agenceType;
		return this;
	}

}
