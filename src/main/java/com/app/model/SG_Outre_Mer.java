package com.app.model;

import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="banque_sg_antille", schema="scrap")
public class SG_Outre_Mer {

	@Id
	@SequenceGenerator(name="scrap.banque_sg_antille_id_seq",
	                    sequenceName ="scrap.banque_sg_antille_id_seq",
	                     allocationSize=1)
    @GeneratedValue( strategy=GenerationType.SEQUENCE,
                     generator="scrap.banque_sg_antille_id_seq")
	  
	@Column(name="id",updatable=false)
	private int id;
	
	@Column( name="lien_soumis",columnDefinition="varchar")
	private String lienSoumis;
	
	@Column( name="code_postale",columnDefinition="varchar")
	private String cp;
	
	@Column(name="nbagences",columnDefinition="varchar")
	private String nbAgences;
	
	@Column(name="nomweb",columnDefinition="varchar")
	private String nomWeb;
	
	@Column(name="adresse",columnDefinition="varchar")
	private String adresse;
	
	@Column(name="tel_commerciale",columnDefinition="varchar")
	private String telCom;
	
	@Column(name="tel_pros",columnDefinition="varchar")
	private String telPros;
	
	@Column(name="horairestxt",columnDefinition="varchar")
	private String horairesTxt;
	
	@Column(name="agencehtm",columnDefinition="varchar")
	private String agenceHtm;
	
	@Column(name="annee_du_scrap" ,columnDefinition="varchar(4) DEFAULT date_part('year',CURRENT_DATE)")
	private String anneeScrap;
	
	
	public SG_Outre_Mer() {
		
	}
	public int getId() {
		return id;
	}
	public SG_Outre_Mer setId(int id) {
		this.id = id;
		return this;
	}
	public String getLienSoumis() {
		return lienSoumis;
	}
	public SG_Outre_Mer setLienSoumis(String lienSoumis) {
		this.lienSoumis = lienSoumis;
		return this;
	}
	public String getNbAgences() {
		return nbAgences;
	}
	public SG_Outre_Mer setNbAgences(String nbAgences) {
		this.nbAgences = nbAgences;
		return this;
	}
	public String getNomWeb() {
		return nomWeb;
	}
	public SG_Outre_Mer setNomWeb(String nomWeb) {
		this.nomWeb = nomWeb;
		return this;
	}
	public String getAdresse() {
		return adresse;
	}
	public SG_Outre_Mer setAdresse(String adresse) {
		this.adresse = adresse;
		return this;
	}
	
	public String getAgenceHtm() {
		return agenceHtm;
	}
	public SG_Outre_Mer setAgenceHtm(String agenceHtm) {
		this.agenceHtm = agenceHtm;
		return this;
	}
	public String getAnneeScrap() {
		return anneeScrap;
	}
	public SG_Outre_Mer setAnneeScrap(String anneeScrap) {
		this.anneeScrap = anneeScrap;
		return this;
	}
	public String getTelPros() {
		return telPros;
	}
	public SG_Outre_Mer setTelPros(String telPros) {
		this.telPros = telPros;
		return this;
	}
	public String getTelCom() {
		return telCom;
	}
	public SG_Outre_Mer setTelCom(String telCom) {
		this.telCom = telCom;
		return this;
	}
	public String getHorairesTxt() {
		return horairesTxt;
	}
	public SG_Outre_Mer setHorairesTxt(String horairesTxt) {
		this.horairesTxt = horairesTxt;
		return this;
	}
	public String getCp() {
		return cp;
	}
	public  SG_Outre_Mer setCp(String cp) {
		this.cp = cp;
		return this;
	}
}
