package com.app.service;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.SocketTimeoutException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Scanner;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.PageLoadStrategy;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.app.model.CCOOP_Outre_Mer;

import com.app.scrap.RepCcoopOutreMer;
import com.app.utils.BrowserUtils;
import com.app.utils.Parametres;
import com.app.utils.Utilitaires;

import fa.browserUtils.ChromeBrowser;
import fa.browserUtils.FactoryBrowser;
import fa.log.LogApp.LogScrap;
import fa.log.config.LogConst;

@Service
public class ServiceCcoop_Outre_Mer extends Utilitaires {

	@Autowired
	private RepCcoopOutreMer rep;

	private List<CCOOP_Outre_Mer> ccoopList = new ArrayList<>();
	private static String driverPath = "C:\\ChromeDriver\\";
	private String nomSITE = "CCOOP_Outre_Mer";
	private String ficval = "CP_Communes_DOM_2021";
	private String dossierRESU = Parametres.REPERTOIRE_RESULT + "\\BQ\\" + nomSITE;;
	private String url_accueil = "https://agences.credit-cooperatif.coop/banque/agences/";
	private String fichier_valeurs;
	private String cp;
	private String ligne;

	private int num_ligne = 2;
	private int NbAgences;
	private int numreponse = 1;
	private int nbRecuperes;
	private int num_ligneficval;
	private int parcoursficval;

	private static ChromeDriver chromeDriver;
	private BufferedWriter sortie;
	private BufferedWriter sortie_log;

	private Date Temps_Fin;

	public CCOOP_Outre_Mer saveData(CCOOP_Outre_Mer t) {

		return rep.save(t);

	}

	public List<CCOOP_Outre_Mer> saveDataAll(List<CCOOP_Outre_Mer> t) {

		return rep.saveAll(t);

	}

	public List<CCOOP_Outre_Mer> getElmnt() {

		return rep.findAll();
	}

	public CCOOP_Outre_Mer getById(int id) {

		return rep.findById(id).orElse(null);
	}

	public String deleteProduct(int id) {
		try {
			rep.deleteById(id);

		} catch (Exception e) {
			e.printStackTrace();
		}

		return "removed";
	}

	public CCOOP_Outre_Mer updateProduct(CCOOP_Outre_Mer t) {

		return null;
	}

	public CCOOP_Outre_Mer setData(String depSoumis, String total, String nbAgences, String enseigne, String nomWeb,
			String adresse, String tel, String fax, String horaireshtm, String horairestxt, String services, String dab,
			String agenceHtm) {
		CCOOP_Outre_Mer ccoop = new CCOOP_Outre_Mer();
		ccoop.setDepSoumis(depSoumis).setTotal(total).setNbAgences(nbAgences).setEnseigne(enseigne).setNomWeb(nomWeb)
				.setAdresse(adresse).setTel(tel).setFax(fax).setHoraireshtm(horaireshtm).setHorairestxt(horairestxt)
				.setServices(services).setDab(dab).setAgenceHtm(agenceHtm);
		this.saveData(ccoop);
		return ccoop;
	}

	public List<CCOOP_Outre_Mer> showFirstRows() {
		List<CCOOP_Outre_Mer> a = rep.getFirstRow(100, CCOOP_Outre_Mer.class);
		return a;
	}

	public List<CCOOP_Outre_Mer> showLastRows() {
		List<CCOOP_Outre_Mer> a = rep.getLastRow(100, CCOOP_Outre_Mer.class);
		return a;
	}

	public void setup() {

		FactoryBrowser factBrowser = new FactoryBrowser();
		ChromeBrowser b = (ChromeBrowser) factBrowser.create("chrome");
		final String binaryPath = "C:\\Program Files\\Google\\Chrome Beta\\Application\\chrome.exe";

		chromeDriver = b.setOptions(PageLoadStrategy.NORMAL, driverPath, 60000, 60000, 60000, false, false, binaryPath)
				.initBrowserDriver();

		// chromeDriver= BrowserUtils.chromeConstruct(driverPath, 6000, 6000,10,
		// PageLoadStrategy.NORMAL);

	}

	public List<CCOOP_Outre_Mer> scrapCCOOP_Outre_Mer() throws IOException {
		boolean documentReadyState = false;
		JavascriptExecutor jsExecutor = (JavascriptExecutor) chromeDriver;
		String javaScript = "";
		fichier_valeurs = Parametres.REPERTOIRE_DATA_SOURCES + ficval + ".txt";
		Scanner sc = new Scanner(new BufferedInputStream(new FileInputStream(new File(fichier_valeurs))));

		if (!new File(dossierRESU).exists()) {
			new File(dossierRESU).mkdirs();
		}
		//fichier = new File(dossierRESU + "\\" + "RESULTATS_" + nomSITE + "_" + System.currentTimeMillis() + ".txt");
		LogScrap log = new LogScrap("BanqueCCOOP_DOM.txt", LogConst.TYPE_LOG_FILE);
		num_ligneficval = ((new File(LogConst.STORE_LOG_PATH + LogConst.PREFIX_LOG_FILE + "_BanqueCCOOP_DOM.txt")).exists())
				? Integer.parseInt(log.readLog().getLastLineProcessed().split("\\t")[4])
				: 1;

		parcoursficval = 0;
		while (sc.hasNextLine()) {
			String line = sc.nextLine();
			String[] arrayTmp = line.split("\\t");
			String comm = arrayTmp[1];

			String uri_acc = url_accueil;

			if (parcoursficval > num_ligneficval) {
				cp = arrayTmp[0];
				System.out.println("CP : " +cp);

				try {
					chromeDriver.get(uri_acc);

				} catch (Exception e) {
					if (e instanceof SocketTimeoutException) {
						chromeDriver.get(uri_acc);

					}
				}
				documentReadyState = this.listensIfPageFullyLoaded(chromeDriver);
				if (documentReadyState) {

					if (comm.toLowerCase() != "commune") {
						WebElement input = chromeDriver.findElement(By.cssSelector("#em-search-form__searchcity"));
						javaScript = String.format("arguments[0].value=\"%s\"", cp);
						jsExecutor.executeScript(javaScript, input);
						
						try {
							Thread.sleep(2000);
						} catch (InterruptedException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						
						input.sendKeys(Keys.ENTER);
						
						/*WebElement listAd = chromeDriver.findElement(By.cssSelector("#listAddressAmbiguous"));
						if(listAd != null) {
							
							WebElement listAdEl = chromeDriver.findElement(By.cssSelector("#listAddressAmbiguous > div > div > ul > li:nth-child(1)"));
							listAdEl.click();
						}*/
						
						
					}

					try {
						Thread.sleep(2000);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

					/*WebElement btnSearch = chromeDriver.findElement(By.cssSelector(
							"#em-search-form > div > div.em-search-form__fieldset-wrapper > fieldset.em-search-form__fieldset.em-search-form__buttons > div > button"));
					btnSearch.click();*/
					
					try {
						Thread.sleep(5000);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}


					documentReadyState = this.listensIfPageFullyLoaded(chromeDriver);
					
					//System.out.println("documentReadyState : " +documentReadyState);

					if (documentReadyState) {
						nbRecuperes = 0;
						String pageSource = chromeDriver.getPageSource();
						Document doc = null;
						doc = Jsoup.parse(pageSource);
						Element rootJElement = doc.select("main").first();
						
						Element resultElement = doc.selectFirst("body > div.em-page > header > div.em-header > h1 > span");
						
						//System.out.println(resultElement.text());
						
						if (!resultElement.text().contains("Aucune agence Crédit Coopératif à proximité de")) {
							Elements agencyLinksJElement = rootJElement.select(
									"div.em-results-wrapper > div > div.em-results__list-wrapper > div > div > div.jspPane > ul > li > h2 > a");
							List<String> agencyLinks = new ArrayList<>();
							if (!agencyLinks.isEmpty()) {
								agencyLinks.clear();
							}
							for (Element tmp : agencyLinksJElement) {
								agencyLinks.add(tmp.attr("href"));
							}
							NbAgences = agencyLinks.size();
							
							System.out.println("Misy : "+NbAgences);
							
							for (String path : agencyLinks) {
								String uri = "https://agences.credit-cooperatif.coop" + path;
								String agences_HTM = "", nom_agence = "", adresse = "", tel = "", fax = "",
										HorairesHTM = "", HorairesTXT = "", Services = "", enseigne = "";
								int total = 0, DAB = 0;
								try {
									chromeDriver.get(uri);

								} catch (Exception e) {
									if (e instanceof SocketTimeoutException) {
										chromeDriver.get(uri);

									}
								}
								documentReadyState = this.listensIfPageFullyLoaded(chromeDriver);
								if (documentReadyState) {
									pageSource = chromeDriver.getPageSource();
									doc = Jsoup.parse(pageSource);
									Element root2JElement = doc.select("main").first();

									agences_HTM = root2JElement.outerHtml().trim();
									agences_HTM = this.normalize(agences_HTM.replaceAll("\\s{1,}", ""));

									// retrieve agency name
									Element agencyNameJElement = root2JElement
											.select("div.em-details > div.em-details__poi-card > h2").first();
									nom_agence = (agencyNameJElement != null)
											? this.normalize(agencyNameJElement.text())
											: "";
									// retrieve agency address
									Element agencyAddressJElement = root2JElement.select(
											"div.em-details > div.em-details__poi-card > div > div:nth-child(1) > div")
											.first();
									adresse = (agencyAddressJElement != null)
											? this.normalize(agencyAddressJElement.text())
											: "";

									// retrieve agency tel
									Element agencyTelJElement = root2JElement.select(
											"div.em-details > div.em-details__poi-card > div > div:nth-child(2) > a")
											.first();
									tel = (agencyTelJElement != null)
											? this.normalize(
													agencyTelJElement.attr("href").replaceAll("[a-zA-Z]", "").trim())
											: "";

									// retrieve agency schedule
									Elements agencySheduleJElement = root2JElement.select(
											"div.em-details__horaires-bloc > div > div.em-graphical-schedules > div");
									HorairesHTM = agencySheduleJElement.outerHtml().trim();
									HorairesHTM = HorairesHTM.replaceAll("\\s{1,}", "");
									for (Element tmp : agencySheduleJElement) {
										HorairesTXT += tmp.text() + " ";
									}
									HorairesTXT = this.normalize(HorairesTXT.replaceAll("[^0-9a-zA-z ]", " "));
									// retrieve agency services
									Elements agencyServiceJElement = root2JElement
											.select("div.em-details__services-bloc.bloc-agenceco > ul > li");
									for (Element tmp : agencyServiceJElement) {
										Services += tmp.text() + " ";
									}
									Services = this.normalize(Services);

									// DAB
									if (Services.toLowerCase().contains("distributeur automatique de billet")) {
										DAB = 1;
									}
									total = NbAgences;

									// ENSEIGNE
									enseigne = "CREDIT COOPERATIF";

									nbRecuperes++;

									ccoopList.add(this.setData(cp, String.valueOf(total), String.valueOf(NbAgences),
											enseigne, nom_agence, adresse, tel, fax, HorairesHTM, HorairesTXT, Services,
											String.valueOf(DAB), agences_HTM));
									
									System.out.println("\n--------------------------------\n");

								}

							}
							// this.enregistrer_journal(nbRecuperes);
						} else {
							System.out.println("Aucun resultat");
							// this.enregistrer_journal(nbRecuperes);
							// this.enregistrer(0, "", "","","","","","","","",0);
						}
					}else {
						System.out.println("Oh non!!!!");
					}

				}
				log.writeLog(new Date(), chromeDriver.getCurrentUrl(), "no_error", NbAgences, parcoursficval, NbAgences - nbRecuperes);
			}

			parcoursficval++;
		}
		return ccoopList;

	}

	public void tearsDown() {
		chromeDriver.quit();
	}

	public void enregistrer(int total, String agences_HTM, String nom_agence, String adresse, String tel, String fax,
			String HorairesHTM, String HorairesTXT, String Services, String enseigne, int DAB) {

		try {

			sortie = new BufferedWriter(new FileWriter(fichier, true));
			sortie.write(cp + "\t" + NbAgences + "\t" + enseigne + "\t" + nom_agence + "\t" + adresse + "\t" + tel
					+ "\t" + fax + "\t" + HorairesHTM + "\t" + HorairesTXT + "\t" + Services + "\t" + DAB + "\t"
					+ agences_HTM + "\r\n");

			sortie.close();
		} catch (IOException e) {

			e.printStackTrace();
		}

	}

	public void enregistrer_journal(int numreponse) {

//	    	this.fichier = new File(Parametres.REPERTOIRE + "RESULTATS_BNP.");
		try {
			Temps_Fin = new Date();
			SimpleDateFormat formater = null;
			formater = new SimpleDateFormat("'le' dd/MM/yyyy '�' hh:mm:ss");
			String date = formater.format(Temps_Fin);
			date = formater.format(Temps_Fin);

			sortie_log = new BufferedWriter(new FileWriter(log, true));
			sortie_log.write(parcoursficval + ";" + "FinValeur" + ";" + cp + ";" + this.NbAgences + ";" + nbRecuperes
					+ ";" + (this.NbAgences - nbRecuperes) + ";" + date + "\r\n");
			sortie_log.close();
			long taille_fichier = fichier.length() / 1000000; // la length est en octets, on divise par 1000 pour mettre
																// en Ko et par 1000000 pour mettre en Mo

			if (taille_fichier > 8) {
				// INITIALISATION FICHIER DE RESULTAT
				this.fichier = new File(
						dossierRESU + "\\" + "RESULTATS_" + nomSITE + "_" + System.currentTimeMillis() + ".txt");
				// Attention pas d'accent dans les noms de colonnes !
				sortie = new BufferedWriter(new FileWriter(fichier, true));
				sortie.write("CodePostaleSoumise" + "\t" + "TOTAL" + "\t" + "NbAgences" + "\t" + "ENSEIGNE" + "\t"
						+ "NOMWEB" + "\t" + "ADRESSE" + "\t" + "TEL" + "\t" + "FAX" + "\t" + "HORAIRESHTM" + "\t"
						+ "HORAIRESTXT" + "\t" + "SERVICES" + "\t" + "DAB" + "\t" + "AGENCEHTM" + "\r\n");
				sortie.close();
			}

		} catch (IOException e) {

			e.printStackTrace();
		}

	}

	/**
	 * La m�thode <code>initialiser()</code> permet d'ajouter la ligne d'ent�te dans
	 * le fichier r�sultat.
	 */

	public void initialiser() {

		try {
			// Attention pas d'accent dans les noms de colonnes !
			sortie = new BufferedWriter(new FileWriter(fichier, true));
			sortie.write("DepSoumis" + "\t" + "TOTAL" + "\t" + "NbAgences" + "\t" + "ENSEIGNE" + "\t" + "NOMWEB" + "\t"
					+ "ADRESSE" + "\t" + "TEL" + "\t" + "FAX" + "\t" + "HORAIRESHTM" + "\t" + "HORAIRESTXT" + "\t"
					+ "SERVICES" + "\t" + "DAB" + "\t" + "AGENCEHTM" + "\r\n");
			sortie.close();

			File toto = new File(dossierRESU + "\\" + "LOG_" + nomSITE + ".csv");
			if (!toto.exists()) {
				// if (num_ligneficval == num_ligne) {
				sortie_log = new BufferedWriter(new FileWriter(log, true));
				sortie_log.write("NumLigne" + ";" + "Recherche" + ";" + "DepSoumis" + ";" + "NbAgences" + ";"
						+ "Nbrecuperes" + ";" + "Reste" + ";" + "Heure_Fin" + "\r\n");
				sortie_log.close();
			}

		} catch (IOException e) {

			e.printStackTrace();
		}
	}

}
