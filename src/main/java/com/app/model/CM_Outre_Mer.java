package com.app.model;

import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "banque_cm_dep_outre_mer", schema = "scrap")
public class CM_Outre_Mer {

	@Id
	@SequenceGenerator(name = "scrap.banque_cm_dep_outre_mer_id_seq", sequenceName = "scrap.banque_cm_dep_outre_mer_id_seq", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "scrap.banque_cm_dep_outre_mer_id_seq")

	@Column(name = "id", updatable = false)
	private int id;

	@Column(name = "depsoumis", columnDefinition = "varchar")
	private String depSoumis;

	@Column(name = "nbvilles", columnDefinition = "varchar")
	private String nbVilles;

	@Column(name = "numvilles", columnDefinition = "varchar")
	private String numVilles;

	@Column(name = "villesoumise", columnDefinition = "varchar")
	private String villeSoumise;

	@Column(name = "nbagences", columnDefinition = "varchar")
	private String nbAgences;

	@Column(name = "nomweb", columnDefinition = "varchar")
	private String nomWeb;

	@Column(name = "adresse", columnDefinition = "varchar")
	private String adresse;

	@Column(name = "horaire", columnDefinition = "varchar")
	private String horaire;

	@Column(name = "telephone", columnDefinition = "varchar")
	private String telephone;

	@Column(name = "lienagence", columnDefinition = "varchar")
	private String lienAgence;

	@Column(name = "agencehtm", columnDefinition = "varchar")
	private String agenceHtm;
	
	@Column(name = "fax", columnDefinition = "varchar")
	private String fax;
	
	@Column(name = "codeGuchet", columnDefinition = "varchar")
	private String codeGuchet;
	
	@Column(name = "gab")
	private int gab;
	
	@Column(name = "dab")
	private int dab;
	
	@Column(name = "borneDepot")
	private int borneDepot;
	
	@Column(name = "latitude", columnDefinition = "varchar")
	private String latitude;
	
	@Column(name = "longitude", columnDefinition = "varchar")
	private String longitude;

	@Column(name = "annee_du_scrap", columnDefinition = "varchar(4) DEFAULT date_part('year',CURRENT_DATE)")
	private String anneeScrap;

	public CM_Outre_Mer() {
		LocalDate date = new Date().toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
		anneeScrap = String.valueOf(date.getYear() + 1);
	}

	public int getId() {
		return id;
	}

	public CM_Outre_Mer setId(int id) {
		this.id = id;
		return this;
	}

	public String getDepSoumis() {
		return depSoumis;
	}

	public CM_Outre_Mer setDepSoumis(String depSoumis) {
		this.depSoumis = depSoumis;
		return this;
	}

	public String getNbVilles() {
		return nbVilles;
	}

	public CM_Outre_Mer setNbVilles(String nbVilles) {
		this.nbVilles = nbVilles;
		return this;
	}

	public String getNumVilles() {
		return numVilles;
	}

	public CM_Outre_Mer setNumVilles(String numVilles) {
		this.numVilles = numVilles;
		return this;
	}

	public String getVilleSoumise() {
		return villeSoumise;
	}

	public CM_Outre_Mer setVilleSoumise(String villeSoumise) {
		this.villeSoumise = villeSoumise;
		return this;
	}

	public String getNbAgences() {
		return nbAgences;
	}

	public CM_Outre_Mer setNbAgences(String nbAgences) {
		this.nbAgences = nbAgences;
		return this;
	}

	public String getNomWeb() {
		return nomWeb;
	}

	public CM_Outre_Mer setNomWeb(String nomWeb) {
		this.nomWeb = nomWeb;
		return this;
	}

	public String getAdresse() {
		return adresse;
	}

	public CM_Outre_Mer setAdresse(String adresse) {
		this.adresse = adresse;
		return this;
	}

	public String getLienAgence() {
		return lienAgence;
	}

	public CM_Outre_Mer setLienAgence(String lienAgence) {
		this.lienAgence = lienAgence;
		return this;
	}

	public String getAgenceHtm() {
		return agenceHtm;
	}

	public CM_Outre_Mer setAgenceHtm(String agenceHtm) {
		this.agenceHtm = agenceHtm;
		return this;
	}

	public String getAnneeScrap() {
		return anneeScrap;
	}

	public CM_Outre_Mer setAnneeScrap(String anneeScrap) {
		this.anneeScrap = anneeScrap;
		return this;
	}

	public String getHoraire() {
		return horaire;
	}

	public CM_Outre_Mer setHoraire(String horaire) {
		this.horaire = horaire;
		return this;
	}

	public String getTelephone() {
		return telephone;
	}

	public CM_Outre_Mer setTelephone(String telephone) {
		this.telephone = telephone;
		return this;
	}
	
	public String getFax() {
		return fax;
	}

	public CM_Outre_Mer setFax(String fax) {
		this.fax = fax;
		return this;
	}

	public String getCodeGuchet() {
		return codeGuchet;
	}

	public CM_Outre_Mer setCodeGuchet(String codeGuchet) {
		this.codeGuchet = codeGuchet;
		return this;
	}

	public int getGab() {
		return gab;
	}

	public CM_Outre_Mer setGab(int gab) {
		this.gab = gab;
		return this;
	}

	public int getDab() {
		return dab;
	}

	public CM_Outre_Mer setDab(int dab) {
		this.dab = dab;
		return this;
	}

	public int getBorneDepot() {
		return borneDepot;
	}

	public CM_Outre_Mer setBorneDepot(int borneDepot) {
		this.borneDepot = borneDepot;
		return this;
	}

	public String getLatitude() {
		return latitude;
	}

	public CM_Outre_Mer setLatitude(String latitude) {
		this.latitude = latitude;
		return this;
	}

	public String getLongitude() {
		return longitude;
	}

	public CM_Outre_Mer setLongitude(String longitude) {
		this.longitude = longitude;
		return this;
	}

}
