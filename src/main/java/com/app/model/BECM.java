package com.app.model;

import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="banque_becm", schema="scrap")
public class BECM {
	
	@Id
	@SequenceGenerator(name="scrap.banque_becm_id_seq",
	                   sequenceName = "scrap.banque_becm_id_seq",
	                   allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE,
	                 generator = "scrap.banque_becm_id_seq")
	@Column(name="id", unique = false)
	private int id;
	
	@Column(name="dep_soumis", columnDefinition = "varchar")
	private String dep;
	
	@Column(name="nbagences", columnDefinition = "varchar")
	private String nbAgences;
	
	@Column(name = "nom_agence", columnDefinition = "varchar")
	private String nomAgence;
	
	@Column(name="adresse", columnDefinition = "varchar")
	private String adresse;
	
	@Column(name="horaire", columnDefinition = "varchar")
	private String horaire;
	
	@Column(name="telephone", columnDefinition = "varchar")
	private String telephone;
	
	@Column(name="lienagence", columnDefinition = "varchar")
	private String lienAgence;
	
	@Column(name="GAB",columnDefinition = "varchar")
	private String gab;
	
	@Column(name="annee_du_scrap" ,columnDefinition="varchar(4) DEFAULT date_part('year',CURRENT_DATE)")
	private String anneeScrap;
	
	public  BECM() {
		LocalDate ld= new Date().toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
		anneeScrap=String.valueOf(ld.getYear()+1);
	}

	public String getDep() {
		return dep;
	}

	public BECM setDep(String dep) {
		this.dep = dep;
		return this;
	}

	public String getNbAgences() {
		return nbAgences;
	}

	public BECM setNbAgences(String nbAgences) {
		this.nbAgences = nbAgences;
		return this;
	}

	public String getNomAgence() {
		return nomAgence;
	}

	public BECM setNomAgence(String nomAgence) {
		this.nomAgence = nomAgence;
		return this;
	}

	public String getAdresse() {
		return adresse;
	}

	public BECM setAdresse(String adresse) {
		this.adresse = adresse;
		return this;
	}

	public String getHoraire() {
		return horaire;
	}

	public BECM setHoraire(String horaire) {
		this.horaire = horaire;
		return this;
	}

	public String getTelephone() {
		return telephone;
	}

	public BECM setTelephone(String telephone) {
		this.telephone = telephone;
		return this;
	}

	public String getLienAgence() {
		return lienAgence;
	}

	public BECM setLienAgence(String lienAgence) {
		this.lienAgence = lienAgence;
		return this;
	}

	public String getGab() {
		return gab;
	}

	public BECM setGab(String gab) {
		this.gab = gab;
		return this;
	}
	
	
	
	

}
