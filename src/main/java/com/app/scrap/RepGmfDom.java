package com.app.scrap;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.app.model.GMF;
import com.app.model.GMF_DOM;

@Repository
public interface RepGmfDom extends JpaRepository<GMF_DOM,Integer>,RepositoryCustom<GMF_DOM> {

}
