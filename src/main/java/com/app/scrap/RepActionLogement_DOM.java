package com.app.scrap;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.app.model.ActionLogement;
import com.app.model.ActionLogement_DOM;

@Repository
public interface RepActionLogement_DOM  extends JpaRepository<ActionLogement_DOM, Integer>{

}
