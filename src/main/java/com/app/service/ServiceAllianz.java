package com.app.service;

import java.io.BufferedWriter;
import java.io.File;

import java.io.FileWriter;
import java.io.IOException;

import java.net.SocketTimeoutException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Scanner;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.app.model.Allianz;
import com.app.scrap.RepAllianz;

import com.app.utils.Parametres;
import com.app.utils.Utilitaires;

import fa.browserUtils.FactoryBrowser;
import fa.browserUtils.HtmlUnitBrowser;
import fa.log.LogApp.LogScrap;
import fa.log.config.LogConst;

@Service
public class ServiceAllianz extends Utilitaires {

	@Autowired
	private RepAllianz rep;

	private List<Allianz> listAllianz = new ArrayList<>();
	private String nomSITE = "ALLIANZ";
	private String ficval = "DepALLIANZ";
	private String dossierRESU = Parametres.REPERTOIRE_RESULT + "\\ASSU\\" + nomSITE;;
	private String url_accueil = "https://agences.allianz.fr/";
	private String fichier_valeurs;
	private String DepSoumis;
	private String agentName = "";
	private String agentNames = "";
	private String agencySchedule = "";
	private String agencyName = "";
	private String agencyPhone = "";
	private String agencyAddress = "";
	private String agencyFax = "";
	private String agencyHtml = "";
	private String orias = "";

	private int NbAgences;
	private int nbRecuperes;
	private int num_ligneficval;
	private int parcoursficval;

	private ChromeDriver wd;
	private HtmlUnitDriver htmlUnitDriver;
	private BufferedWriter sortie;
	private BufferedWriter sortie_log;

	private Date Temps_Fin;

	public Allianz saveData(Allianz t) {

		return rep.save(t);

	}

	public List<Allianz> saveDataAll(List<Allianz> t) {

		return rep.saveAll(t);

	}

	public List<Allianz> getElmnt() {

		return rep.findAll();
	}

	public Allianz getById(int id) {

		return rep.findById(id).orElse(null);
	}

	public String deleteProduct(int id) {
		try {
			rep.deleteById(id);

		} catch (Exception e) {
			e.printStackTrace();
		}

		return "removed";
	}

	public Allianz updateProduct(Allianz t) {
		// AllianzODO Auto-generated method stub
		return null;
	}

	public Allianz setData(String depSoumis, String nbAgences, String villeSoumise, String nomAgence, String adresse,
			String tel, String fax, String orias, String horaires, String agenceHTM, String agentGenerale,
			String interlocuteurHTM) {
		Allianz allianz = new Allianz();
		allianz.setDepSoumis(depSoumis).setNbAgences(nbAgences).setVilleSoumise(villeSoumise).setNomAgence(nomAgence)
				.setAdresse(adresse).setTel(tel).setFax(fax).setOrias(orias).setHoraires(horaires)
				.setAgenceHTM(agenceHTM).setAgentGenerale(agentGenerale).setInterlocuteurHTM(interlocuteurHTM);
		this.saveData(allianz);
		return allianz;

	}

	public List<Allianz> showFirstRows() {
		List<Allianz> a = rep.getFirstRow(100, Allianz.class);
		return a;
	}

	public List<Allianz> showLastRows() {
		List<Allianz> a = rep.getLastRow(100, Allianz.class);
		return a;
	}

	public void setUp() throws Exception {
		FactoryBrowser factB = new FactoryBrowser();
		HtmlUnitBrowser hB = (HtmlUnitBrowser) factB.create("htmlUnit");
		htmlUnitDriver = hB.setOptions(true, false, false, 60000).initBrowserDriver();
	}

	public List<Allianz> scrapALLIANZ() throws IOException {

		fichier_valeurs = Parametres.REPERTOIRE_DATA_SOURCES + ficval + ".txt";
		Scanner sc = new Scanner(new File(fichier_valeurs));
		File f = new File(dossierRESU);
		if (!f.exists()) {
			f.mkdirs();
		}
		fichier = new File(dossierRESU + "\\" + "RESULTATS_" + nomSITE + "_" + System.currentTimeMillis() + ".txt");
		LogScrap log = new LogScrap("AssuranceAllianzLog.txt", LogConst.TYPE_LOG_FILE);

		if ((new File(LogConst.STORE_LOG_PATH + LogConst.PREFIX_LOG_FILE + "_AssuranceAllianzLog.txt")).exists()) {

			num_ligneficval = Integer.parseInt(log.readLog().getLastLineProcessed().split("\t")[4]);

		} else {
			num_ligneficval = 1;
		}

		parcoursficval = 1;
		while (sc.hasNextLine()) {
			String line = sc.nextLine();
			if (parcoursficval >= num_ligneficval) {

				nbRecuperes = 0;
				DepSoumis = line;
				// String readyState="";
				// JavascriptExecutor jsExecutor=(JavascriptExecutor)wd;
				List<String> agenciesListLinks = new ArrayList<>();
				try {
					htmlUnitDriver.get((url_accueil + DepSoumis));
					// readyState=(String)jsExecutor.executeScript("return document.readyState");
				} catch (Exception e) {

					if (e instanceof SocketTimeoutException) {
						wd.get((url_accueil + DepSoumis));
					}
				}
				try {
					String pageSource = "";
					Document doc = null;
					System.out.println("here");
					/*
					 * readyState=(String)jsExecutor.executeScript("return document.readyState");
					 * 
					 * while( !readyState.equals("complete") ) {
					 * readyState=(String)jsExecutor.executeScript("return document.readyState");
					 * System.out.println("le reday state :"+readyState); }
					 */
					// if( readyState.equals("complete") ){
					pageSource = htmlUnitDriver.getPageSource();
					// if( pageSource!=null ) {
					doc = Jsoup.parse(pageSource);
					Element bigBlockListJsoupElement = doc.select("#alist").first();
					if (bigBlockListJsoupElement != null) {
						Elements liJsoupElements = bigBlockListJsoupElement.select("#agencies > li > div > h2 > a");
						for (Element elementTmp : liJsoupElements) {
							agenciesListLinks.add((elementTmp.attr("href")));
						}
						NbAgences = agenciesListLinks.size();
						for (String strTmp : agenciesListLinks) {
							nbRecuperes++;
							agentName = "";
							agencySchedule = "";
							agencyAddress = "";
							agencyPhone = "";
							agencyFax = "";
							agencyName = "";
							agencyHtml = "";
							pageSource = "";
							try {
								htmlUnitDriver.get(strTmp);
							} catch (Exception e) {

								if (e instanceof SocketTimeoutException) {
									htmlUnitDriver.get(strTmp);
								}
							}
							pageSource = htmlUnitDriver.getPageSource();
							// retrieve agency info
							doc = Jsoup.parse(pageSource);
							
							System.out.println("strTmp : "+strTmp);
							
							// retrieve agency name

							Element agencyNameJsoupElement = doc.select(
									"div.l-container.l-container--full-width > div > div.l-grid.l-grid--max-width > div > div.c-agent-card__headline__container > h1")
									.first();

							agencyName = (agencyNameJsoupElement != null) ? agencyNameJsoupElement.text().trim() : "";
							agencyName = this.normalize(agencyName);
							
							System.out.println("agencyName : "+agencyName);
							
							Element bigBlockAgencyInfoJsoupElement = doc
									.select("div[class=\"c-agent-card__container\"]").first();

							if (bigBlockAgencyInfoJsoupElement != null) {
								agencyHtml = bigBlockAgencyInfoJsoupElement.outerHtml();
								agencyHtml = agencyHtml.replaceAll("\\s{1,}", "");
								agencyHtml = this.normalize(agencyHtml);

								Element wrapperHeaderJsoupElement = bigBlockAgencyInfoJsoupElement
										.select("div>div.c-agent-card__main-block").first();

								// retrievet allianz agent
								
								Element agentJsoupElement = wrapperHeaderJsoupElement
										.select("p.c-heading.c-heading--section.c-agent-card__name").first();
								agentName = (agentJsoupElement != null) ? agentJsoupElement.text().trim() : "";
								agentName = this.normalize(agentName);
								System.out.println("agent name : " + agentName);
								// retrieve allianz agency schedule

								Elements agencyScheduleJsoupElement = wrapperHeaderJsoupElement
										.select("div.u-flex-basis-auto.js-show-hide>table>tbody>tr");
								
								if (agencyScheduleJsoupElement != null) {
									for (Element elementTmp : agencyScheduleJsoupElement) {
										agencySchedule += elementTmp.text().trim() + " ";
									}
								}
								agencySchedule = this.normalize(agencySchedule);
								
								System.out.println("agencySchedule : " + agencySchedule);
								
								// retrieve allianz agency address
								Element agencyAddressJsoupElement = wrapperHeaderJsoupElement.select("strong").first();
								agencyAddress = (agencyAddressJsoupElement != null)
										? agencyAddressJsoupElement.text().replaceAll("\\W{1,}", " ")
										: "";
								agencyAddress = this.normalize(agencyAddress);
								
								System.out.println("agency adresse : " + agencyAddress);
								
								Elements wrapperFooterJsoupElement = bigBlockAgencyInfoJsoupElement
										.select("div>div.c-agent-card__footer > div > a");
								/*
								 * for(Element element: wrapperFooterJsoupElement) { agencyPhone="";
								 * agencyFax=""; String str=element.outerHtml();
								 * if(str.contains("c-icon--phone")) { agencyPhone+=element.attr("href")+"&"; }
								 * if(str.contains("c-icon--fax")) { agencyFax= element.attr("href"); }
								 * 
								 * }
								 */
								//c-link c-link--block c-link--negative c-agent-card__footer-link
								Element phoneElement = bigBlockAgencyInfoJsoupElement
										.selectFirst("div>div.c-agent-card__footer>"
												+ "div.c-agent-card__footer-links>a:nth-child(1)");
								agencyPhone = (phoneElement != null)
										? phoneElement.attr("href").replaceAll("[^0-9 ]{1,}", "")
										: "";
								agencyPhone = this.normalize(agencyPhone);
								
								System.out.println("agencyPhone : " + agencyPhone);

								Element oriasElement = doc.selectFirst("body > "
										+ "div.footer_navigation.aem-GridColumn.aem-GridColumn--default--12 > "
										+ "div > " + "div >" + " div > " + "div:nth-child(4) > " + "ul > "
										+ "li:nth-child(7) > " + "span.c-footer__navigation-link.orias-number");
								orias = (oriasElement != null) ? oriasElement.text() : "";
								orias = orias.replaceAll("\\s{2,}", "");
								orias = orias.replaceAll("[^0-9 ]{1,}", "");
								orias = this.normalize(orias);
								agentNames = (oriasElement != null) ? oriasElement.text() : "";
								agentNames = agentNames.replaceAll("N°ORIAS", "");
								agentNames = agentNames.replaceAll("[0-9:]", "");
								agentNames = this.normalize(agentNames);
								
								System.out.println("orias : " + orias);
								
								System.out.println("-------------------------------------\n");
								
							}

							listAllianz.add(this.setData(DepSoumis, String.valueOf(NbAgences), strTmp, agencyName,
									agencyAddress, agencyPhone, agencyFax, orias, agencySchedule, agencyHtml, agentName,
									agentNames));
						}

					}
					// }

					// }

				} catch (Exception e) {
					e.printStackTrace();
				}

				log.writeLog(new Date(), line, "no_error", NbAgences, parcoursficval, (NbAgences - nbRecuperes));
			}
			parcoursficval++;
		}
		sc.close();
		return listAllianz;
	}

	/**
	 * this method record all infos about allianz into File locato to
	 * C:\scraping\assu\maaf\
	 * 
	 * @param TetxArea txtArea, javaFx Object
	 * @param String   ville, agency allianz link
	 * @param String   nom_agence, agency allianz name
	 * @param String   adresse, agency allianz address
	 * @param String   tel, agency allianz phone number
	 * @param String   fax, agency allianz fax
	 * @param String   Horaires, agency allianz schedule
	 * @param String   agence, html code of info a bout allianz
	 * @param String   interlocuteur agent allianz
	 */
	public void enregistrer(String ville, String nom_agence, String adresse, String tel, String fax, String Horaires,
			String agence, String interlocuteur) {

		try {
			sortie = new BufferedWriter(new FileWriter(fichier, true));
			sortie.write(DepSoumis + "\t" + NbAgences + "\t" + ville + "\t" + nom_agence + "\t" + adresse + "\t" + tel
					+ "\t" + fax + "\t" + Horaires + "\t" + agence + "\t" + interlocuteur + "\r\n");
			sortie.close();
		} catch (IOException e) {

		}
	}

	/**
	 * this methode write an csv file log(locate to C:\scraping\assu\maaf\), the csv
	 * is use when the application meet an issue and we rerunning app, the app know
	 * where it should be start
	 * 
	 * @param String
	 */
	public void enregistrer_journal(int numreponse) {
		try {
			Temps_Fin = new Date();
			SimpleDateFormat formater = null;
			formater = new SimpleDateFormat("'le' dd/MM/yyyy '�' hh:mm:ss");
			String date = formater.format(Temps_Fin);
			date = formater.format(Temps_Fin);
			sortie_log = new BufferedWriter(new FileWriter(log, true));
			sortie_log.write(parcoursficval + ";" + "FinValeur" + ";" + DepSoumis + ";" + NbAgences + ";" + nbRecuperes
					+ ";" + (NbAgences - nbRecuperes) + ";" + date + "\r\n");
			sortie_log.close();
			long taille_fichier = fichier.length() / 1000000;
			if (taille_fichier > 8) {
				fichier = new File(
						dossierRESU + "\\" + "RESULTATS_" + nomSITE + "_" + System.currentTimeMillis() + ".txt");
				sortie = new BufferedWriter(new FileWriter(fichier, true));
				sortie.write("DepSoumis" + "\t" + "NbAgences" + "\t" + "VILLE_SOUMISE" + "\t" + "NOM_AGENCE" + "\t"
						+ "ADRESSE" + "\t" + "TEL" + "\t" + "FAX" + "\t" + "HORAIRES" + "\t" + "AGENCEHTM" + "\t"
						+ "INTERLOCUTEURHTM" + "\r\n");
				sortie.close();
			}
		} catch (IOException e) {

		}
	}

	/**
	 * this methode initialize the header of the result file
	 * 
	 * @param TextArea txtArea (javaFx object)
	 */
	public void initialiser() {

		try {

			sortie = new BufferedWriter(new FileWriter(fichier, true));
			sortie.write("DepSoumis" + "\t" + "NbAgences" + "\t" + "VILLE_SOUMISE" + "\t" + "NOM_AGENCE" + "\t"
					+ "ADRESSE" + "\t" + "TEL" + "\t" + "FAX" + "\t" + "HORAIRES" + "\t" + "AGENCEHTM" + "\t"
					+ "INTERLOCUTEURHTM" + "\r\n");
			sortie.close();
			File toto = new File(dossierRESU + "\\" + "LOG_" + nomSITE + ".csv");
			if (!toto.exists()) {
				sortie_log = new BufferedWriter(new FileWriter(log, true));
				sortie_log.write("NumLigne" + ";" + "FinValeur" + ";" + "DepSoumis" + ";" + "NbAgences" + ";"
						+ "Nbrecuperes" + ";" + "Reste" + ";" + "Heure_Fin" + "\r\n");
				sortie_log.close();
			}

		} catch (IOException e) {

		}
	}

	/**
	 * this methode close htmlunitdriver
	 * 
	 * @param TextArea txtArea (javaFx Object)
	 */

	public void tearDown() {

		try {
			wd.quit();
		} catch (Exception e) {
		}

	}
}
