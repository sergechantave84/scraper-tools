package com.app.scrap;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.app.model.CDN_GAB_Outre_Mer;

@Repository
public interface RepCdnGabOutreMer extends JpaRepository<CDN_GAB_Outre_Mer, Integer>, RepositoryCustom<CDN_GAB_Outre_Mer>{

}
