package com.app.controller;



import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;


@org.springframework.stereotype.Controller
@RequestMapping("/scrap")
public class Controller {
	@GetMapping(value="/")
	public String getIndex() {
		return "index";
	}
	@GetMapping(value="assurance")
	public String getAssu() {
		return"template";
	}
	@GetMapping(value="banque")
	public String getBank() {
		return"template";
	}
	
	@GetMapping(value="autre")
	public String getAutre() {
		return"template";
	}
	
	@GetMapping(value="bdom")
	public String getBankOutreMer() {
		return"template";
	}
	
	
}
