package com.app.scrap;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.app.model.April;

@Repository
public interface RepApril extends JpaRepository<April,Integer>, RepositoryCustom<April> {

}
