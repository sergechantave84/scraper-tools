package com.app.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="Tripadvisor", schema="scrap")
public class Tripadvisor {

	

	@Id
	@SequenceGenerator(name="scrap.Tripadvisor_id_seq",
	                   sequenceName="scrap.Tripadvisor_id_seq",
	                   allocationSize = 1)
	@GeneratedValue (strategy=GenerationType.SEQUENCE,
	                 generator="scrap.Tripadvisor_id_seq")
	@Column(name="id",updatable=false)
	private int id;
	
	@Column(name="departementSoumis",columnDefinition = "varchar")
	private String depSoumis;
	
	@Column(name="nomRestaurant", columnDefinition = "varchar")
	private String nom;
	
	@Column(name="telephone", columnDefinition = "varchar")
	private String tel;
	
	@Column(name="adresse", columnDefinition = "varchar")
	private String adresse;
	
	@Column(name="horaires",columnDefinition = "varchar")
	private String horaires;
	
	@Column(name="agenceHtml", columnDefinition = "varchar")
	private String agenceHtml;
	
	@Column(name="nombreAgence", columnDefinition = "varchar")
	private int nombreAgence;
	
	@Column(name="type_cuisine", columnDefinition = "varchar")
	private String typeCuisine;
	
	@Column(name="regime_speciaux", columnDefinition = "varchar")
	private String regimeSpeciaux;
	
	@Column(name="repas", columnDefinition = "varchar")
	private String repas;
	
	@Column(name="fonctionalite", columnDefinition = "varchar")
	private String fonctionalite;
	
	@Column(name="fourchette_prix", columnDefinition = "varchar")
	private String fourchette_prix;
	
	@Column(name="note", columnDefinition = "varchar")
	private String note;
	
	@Column(name="classement", columnDefinition = "varchar")
	private String classement;
	
	@Column(name="lienAgence", columnDefinition="varchar")
	private String uri;
	


	public Tripadvisor() {
		
	}
	public Tripadvisor(String depSoumis, 
			String nom, 
			String tel,
			String adresse, 
			String horaires,
			String uri,
			int nombreAgence,
			String agenceHtml) {
		super();
		this.depSoumis = depSoumis;
		this.nom = nom;
		this.tel = tel;
		this.adresse = adresse;
		this.horaires = horaires;
		this.agenceHtml = agenceHtml;
		this.uri=uri;
		this.nombreAgence=nombreAgence;
	}
	

	public String getDepSoumis() {
		return depSoumis;
	}

	public Tripadvisor setDepSoumis(String depSoumis) {
		this.depSoumis = depSoumis;
		return this;
	}

	public String getNom() {
		return nom;
	}

	public Tripadvisor setNom(String nom) {
		this.nom = nom;
		return this;
	}

	public String getAdresse() {
		return adresse;
	}

	public Tripadvisor setAdresse(String adresse) {
		this.adresse = adresse;
		return this;
	}

	public String getHoraires() {
		return horaires;
	}

	public Tripadvisor setHoraires(String horaires) {
		this.horaires = horaires;
		return this;
	}

	public String getAgenceHtml() {
		return agenceHtml;
	}

	public Tripadvisor setAgenceHtml(String agenceHtml) {
		this.agenceHtml = agenceHtml;
		return this;
	}
	public String getTel() {
		return tel;
	}
	public Tripadvisor setTel(String tel) {
		this.tel = tel;
		return this;
	}
	public int getNombreAgence() {
		return nombreAgence;
	}
	public Tripadvisor setNombreAgence(int nombreAgence) {
		this.nombreAgence = nombreAgence;
		return this;
	}
	public String getUri() {
		return uri;
	}
	public Tripadvisor setUri(String uri) {
		this.uri = uri;
		return this;
	}
	public int getId() {
		return id;
	}
	public void  setId(int id) {
		this.id = id;
		
		
	}
	public String getTypeCuisine() {
		return typeCuisine;
	}
	public Tripadvisor  setTypeCuisine(String typeCuisine) {
		this.typeCuisine = typeCuisine;
		return this;
	}
	public String getRegimeSpeciaux() {
		return regimeSpeciaux;
	}
	public Tripadvisor  setRegimeSpeciaux(String regimeSpeciaux) {
		this.regimeSpeciaux = regimeSpeciaux;
		return this;
	}
	public String getRepas() {
		return repas;
	}
	public Tripadvisor  setRepas(String repas) {
		this.repas = repas;
		return this;
	}
	public String getFonctionalite() {
		return fonctionalite;
	}
	public Tripadvisor  setFonctionalite(String fonctionalite) {
		this.fonctionalite = fonctionalite;
		return this;
	}
	public String getFourchette_prix() {
		return fourchette_prix;
	}
	public Tripadvisor  setFourchette_prix(String fourchette_prix) {
		this.fourchette_prix = fourchette_prix;
		return this;
	}
	public String getNote() {
		return note;
	}
	public Tripadvisor  setNote(String note) {
		this.note = note;
		return this;
	}
	public String getClassement() {
		return classement;
	}
	public Tripadvisor  setClassement(String classement) {
		this.classement = classement;
		return this;
	}
	
	
	
	
	
}
