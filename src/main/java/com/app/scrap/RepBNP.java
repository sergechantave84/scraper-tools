package com.app.scrap;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.app.model.BNP;

@Repository
public interface RepBNP extends JpaRepository<BNP,Integer>,RepositoryCustom<BNP> {

}
