package com.app.scrap;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.app.model.CDN_GAB;

@Repository
public interface RepCDN_GAB extends JpaRepository<CDN_GAB,Integer>,RepositoryCustom<CDN_GAB>{

}
