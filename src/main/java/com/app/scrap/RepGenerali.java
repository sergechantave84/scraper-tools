package com.app.scrap;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.app.model.Generali;

@Repository
public interface RepGenerali extends JpaRepository<Generali,Integer>,RepositoryCustom<Generali> {

}
