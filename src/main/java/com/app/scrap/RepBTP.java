package com.app.scrap;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.app.model.BTP;

@Repository
public interface RepBTP extends JpaRepository<BTP, Integer>, RepositoryCustom<BTP> {

}
