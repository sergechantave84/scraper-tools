package com.app.scrap;

import org.springframework.data.jpa.repository.JpaRepository;

import com.app.model.TableAssurance;

public interface RepTableAssurance extends JpaRepository<TableAssurance, Integer>, RepositoryCustom<TableAssurance>{

}
