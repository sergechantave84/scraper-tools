package com.app.scrap;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import com.app.model.BP_Outre_Mer;

@Repository
public interface RepBpOutreMer extends JpaRepository<BP_Outre_Mer,Integer>,RepositoryCustom<BP_Outre_Mer> {

}
