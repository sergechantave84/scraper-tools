
let a = document.querySelector(".navbar_dropdown")
let dropDown = document.querySelector(".dropdown")
dropDown.style.display = "none"
dropDown.style.transition = "0.5s"
a.onclick = (e) => {
	let iconAngle = document.querySelector(".fa.fa-angle-right")
	if (!iconAngle.classList.contains("rotation")) {
		iconAngle.classList.add("rotation");
		dropDown.style.display = "block";
	} else {
		iconAngle.classList.remove("rotation");
		dropDown.style.display = "none"
	}
}

/*canvas part */
let showCanvas = () => {
	let canvas = document.querySelector("canvas")
	wHeight = document.documentElement.clientHeight
	wWidth = document.documentElement.clientWidth
	console.log(wWidth)
	canvas.width = wWidth;
	canvas.height = wHeight;
	window.onresize = () => {
		wHeight = document.documentElement.clientHeight
		wWidth = document.documentElement.clientWidt;
		canvas.width = wWidth;
		canvas.height = wHeight;
	}

	let context = canvas.getContext("2d");
	
	let posX = 20
	let psY = 100
	let vx = 10
	let vy = -10
	let gravity = 1
	let pss = []
	let j = 0;
	let ps = new Particules(canvas)
	density = ps.setting.density
	setInterval(function() {
		context.clearRect(0, 0, canvas.width, canvas.height);
		for (let i = 0; i < density; i++) {
			if (Math.random() > 0.97) {
				pss[j] = new Particules(canvas).particlesInit()
				j++
			}
		}
		for (let n in pss) {
			pss[n].particuleDraw(context, canvas)
		}

	}, 1000 / 60)
}
let callScrap = (type) => {
	let a = document.getElementsByClassName("call_scrap")
	let loader = document.querySelector("#loader")
	Array.from(a).forEach((item) => {

		item.addEventListener('click', (e) => {
			loader.classList.remove("hidden")
			e.preventDefault();
			let named = e.target.textContent;
			let xhr = new XMLHttpRequest();
			xhr.onreadystatechange = () => {
				if (xhr.readyState == 4) {
					if (xhr.status==200) {
						loader.classList.add("hidden")
						let u = "http://localhost:8090/";
						switch (type) {

							case "banque": {
								u += "bigMan/upToJourB"
								break;
							}
							case "assurance": {
								u += "bigMan/upToJourA"
								break;
							}
							case "autre": {
								u += "bigMan/upToJourAu"
								break;
							}
							case "banqueDOM": {
								u += "bigMan/upToJourDom"
								break;
							}
						}
						console.log(u + " name" + named)
						let xhr2 = new XMLHttpRequest();
						xhr2.onreadystatechange = () => {
							if (xhr.readyState == 4) 
					           if (xhr.status==200) 
							          location.reload();
						}
						
						xhr2.open("POST", u, true)
						xhr2.setRequestHeader("Content-Type", "application/x-www-form-urlencoded")
						xhr2.send("name=" + encodeURI(named))
					}
				}
			}
			xhr.open("POST", e.target.href, true)
			xhr.send();

		});
	});
}
let currentUrl = window.location.href;
console.log(currentUrl)
let url = ""
let itemGenerator = new ItemGenerator(document.querySelector("body > div > div"))
if (currentUrl.includes("assurance")) {
	url = `http://localhost:8090/bigMan/getItemAssu`
	// render table Assulist

	itemGenerator.getAllItem(url).then((response) => {
		itemGenerator.renderItem(response)
		showCanvas();
		callScrap("assurance");
	}).catch((error) => {
		console.log(error)
	})
}
if (currentUrl.includes("banque")) {
	//render Table BanqueList
	url = `http://localhost:8090/bigMan/getItemBanque`
	itemGenerator.getAllItem(url).then((response) => {
		itemGenerator.renderItem(response)
		showCanvas();
		callScrap("banque");
	}).catch((error) => {
		console.log(error)
	})

}
if (currentUrl.includes("autre")) {
	//render Table BanqueList
	url = `http://localhost:8090/bigMan/getItemAutre`
	itemGenerator.getAllItem(url).then((response) => {
		itemGenerator.renderItem(response)
		showCanvas();
		callScrap("autre");
	}).catch((error) => {
		console.log(error)
	})
}
if (currentUrl.includes("bdom")) {
	url = `http://localhost:8090/bigMan/getItemDOM`
	itemGenerator.getAllItem(url).then((response) => {
		itemGenerator.renderItem(response)
		showCanvas();
		callScrap("banqueDOM");
	}).catch((error) => {
		console.log(error)
	})
}






