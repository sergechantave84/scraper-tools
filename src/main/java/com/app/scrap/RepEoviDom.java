package com.app.scrap;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.app.model.EOVI;
import com.app.model.EOVI_DOM;

@Repository
public interface RepEoviDom extends JpaRepository<EOVI_DOM,Integer>,RepositoryCustom<EOVI_DOM> {

}
