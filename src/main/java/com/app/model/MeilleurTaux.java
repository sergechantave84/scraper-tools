package com.app.model;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="meilleur_taux", schema = "scrap")
public class MeilleurTaux {
	 @Id
	 @SequenceGenerator(name="scrap.meilleur_taux_id_seq", allocationSize = 1)
	 @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "scrap.meilleur_taux_id_seq" )
	 private int id;
	 
	 @Column(name="annee_du_scrap" ,columnDefinition="varchar(4)")
		private String anneeScrap;
	 
	 @Column(name="ville_soumise", columnDefinition = "varchar")
	 private String villeS;
	 
	 @Column(name="nom_agence", columnDefinition = "varchar")
	 private String name;
	 
	 @Column(name="adresse", columnDefinition = "varchar")
	 private String adresse;
	 
	 @Column(name="tel", columnDefinition = "varchar")
	 private String tel;
	 
	 @Column(name="horaires", columnDefinition = "varchar")
	 private String horaires;
	 
	 public MeilleurTaux() {
		 LocalDate dt= new Date().toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
		 anneeScrap=String.valueOf(dt.getYear()+1);
	 }

	

	public String getName() {
		return name;
	}

	public MeilleurTaux setName(String name) {
		this.name = name;
		return this;
	}

	public String getTel() {
		return tel;
	}

	public MeilleurTaux setTel(String tel) {
		this.tel = tel;
		return this;
	}

	public String getHoraires() {
		return horaires;
	}

	public MeilleurTaux setHoraires(String horaires) {
		this.horaires = horaires;
		return this;
	}



	public String getAdresse() {
		return adresse;
	}


	public MeilleurTaux  setAdresse(String adresse) {
		this.adresse = adresse;
		return this;
	}



	public String getVilleS() {
		return villeS;
	}



	public MeilleurTaux setVilleS(String villeS) {
		this.villeS = villeS;
		return this;
	}

}
