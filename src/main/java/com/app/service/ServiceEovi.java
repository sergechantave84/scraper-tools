package com.app.service;

import java.io.BufferedReader;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.SocketTimeoutException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Scanner;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.app.model.EOVI;

import com.app.scrap.RepEovi;
import com.app.utils.BrowserUtils;
import com.app.utils.Parametres;
import com.app.utils.Utilitaires;

//devenu aesio
@Deprecated
@Service
public class ServiceEovi extends Utilitaires {

	@Autowired
	private RepEovi rep;

	
	private List<com.app.model.EOVI> eoviList=new ArrayList<>();
	private String nomSITE = "EOVI";
	private String ficval = "DepEOVI";
	private String dossierRESU = Parametres.REPERTOIRE_RESULT+ "\\ASSU\\" +nomSITE ;;
	private String url_accueil = "https://agences.eovi-mcd.fr/fr/";
	private String fichier_valeurs; 
    private String DepSoumis;
	
    private int num_ligne = 2;
	private int NbAgences;
    private int nbRecuperes;
	private int num_ligneficval;
	private int parcoursficval;

	private HtmlUnitDriver wd;
	private BufferedWriter sortie;
	private BufferedWriter sortie_log;	
	
	private Date Temps_Fin;
	
	
	
	public EOVI saveData(EOVI t) {

		
			return rep.save( t);
		
	}

	
	public List<EOVI> saveDataAll(List<EOVI> t) {
		
			return  rep.saveAll( t);
		
	}

	
	public List<EOVI> getElmnt() {

		return rep.findAll();
	}

	
	public EOVI getById(int id) {

		return  rep.findById(id).orElse(null);
	}

	
	public String deleteProduct(int id) {
		try {
			rep.deleteById(id);

		} catch (Exception e) {
			e.printStackTrace();
		}

		return "removed";
	}

	public EOVI updateProduct(EOVI t) {
		// EOVIODO Auto-generated method stub
		return null;
	}
    
	public EOVI setData(String depSoumis, String nbAgences, String villeSoumise,String nomAgence,String adresse,
			            String tel,String horaires,String agenceHtm) {
		EOVI eovi= new EOVI();
		eovi.setDepSoumis(depSoumis).
		     setNbAgences(nbAgences).
		     setVilleSoumise(villeSoumise).
		     setNomAgence(nomAgence).
		     setAdresse(adresse).
		     setTel(tel).
		     setHoraires(horaires).
		     setAgenceHtm(agenceHtm);
		this.saveData(eovi);
		return eovi;
	}
	public List<EOVI> showFirstRows() {
		 List<EOVI> a=rep.getFirstRow(100, EOVI.class);
			 return a;
	}
		 
	public List<EOVI> showLastRows(){
			 List<EOVI> a=rep.getLastRow(100, EOVI.class);
			 return a;
	}
	
	public void setUp() throws Exception {
		wd= BrowserUtils.HtmlUnitConstruct(true,false,false,12000);
	         
    }
	
	
    public List<com.app.model.EOVI> scrapEovi() throws IOException {
       
    	fichier_valeurs =Parametres.REPERTOIRE_DATA_SOURCES +  ficval + ".txt";
    	Scanner sc = new Scanner(new File(fichier_valeurs));
		File f=new File(dossierRESU);
   	    if(!f.exists())
        {
            f.mkdirs();
        }
        fichier = new File(dossierRESU + "\\"+ "RESULTATS_" + nomSITE +   "_" + System.currentTimeMillis() + ".txt");
    	log = new File(dossierRESU + "\\"+ "LOG_" + nomSITE +  ".csv");
    	File toto = new File(dossierRESU + "\\"+ "LOG_" + nomSITE +  ".csv"); 
        if( toto.exists() )
        {
    	    String fichierLog = dossierRESU + "\\"+  "LOG_" + nomSITE +  ".csv";
	        InputStream ips=new FileInputStream(fichierLog); 
			InputStreamReader ipsr=new InputStreamReader(ips);
			BufferedReader br=new BufferedReader(ipsr);
		    String ligne;
		    num_ligneficval = 0;
		    while ((ligne=br.readLine())!=null){
			       num_ligneficval = this.compter(ligne, "(\\d+);FinValeur;", true);
		    }
		    br.close(); 
	     }else {
	    	 num_ligneficval = num_ligne; 
	     }
        this.initialiser();
    	 parcoursficval = 1;
    	 while (sc.hasNextLine()) 
    	{
    		String line = sc.nextLine();
    		Document doc=null;
    		String pageSource="";
    		List<String> agenciesLinks=new ArrayList<>();
            if (parcoursficval>=num_ligneficval) 
            {
        	    
    	        nbRecuperes = 0;
                DepSoumis =line;
                try {
                	  wd.get(url_accueil+DepSoumis);
                }catch(Exception e) {
                	
        	    	if(e instanceof SocketTimeoutException) {
        	    		 wd.get(url_accueil+"/"+line);
            	   }
                }
                pageSource=wd.getPageSource();
                doc=Jsoup.parse(pageSource);
                Element rootJsoupElement=doc.select( "#lf-body>div.lf>div>div.lf-geo-divisions__results>div.lf-geo-divisions__results__content>"+
                		                             "div.lf-geo-divisions__results__content__locations").first();
               
                if(rootJsoupElement!=null) 
                {
                	Elements agencyLinkJsoupElement=rootJsoupElement.select("div.lf-geo-divisions__results__content__locations__list>div>h2>a");
                    NbAgences=agencyLinkJsoupElement.size();
                	if(!agenciesLinks.isEmpty()) 
                	{
                		agenciesLinks.clear();
                	}
                	for(Element elementTmp:agencyLinkJsoupElement) 
                	{
                		agenciesLinks.add((elementTmp.attr("href")));
                	}
                	for(String agencyLink:agenciesLinks) 
                	{
                	  String agencyName="",agencyAddress="",agencyPhone="",agencySchedule="",agencyHTML=""; 
                	  nbRecuperes++;
                	  try {
                       	  wd.get("https://agences.eovi-mcd.fr/"+agencyLink);
                       }catch(Exception e) {
                       	
               	    	    if(e instanceof SocketTimeoutException) {
               	    	    	wd.get("https://agences.eovi-mcd.fr/"+agencyLink);
                   	        }
                       }
                	   pageSource=wd.getPageSource();
                	   doc=Jsoup.parse(pageSource);
                	   Element agencyInfoJsoupElement=doc.select("div.lf>div.lf-location>div.lf-location__store>div.lf-location__store__infos").first();
                	   if(agencyInfoJsoupElement!=null) 
                	   {
                		   agencyHTML=agencyInfoJsoupElement.outerHtml().trim();   
                		   agencyHTML=this.normalize(agencyHTML.replaceAll("\\s{1,}",""));
                		   //retrieve agency name
                		   Element agencyNameJsoupElement= agencyInfoJsoupElement.select("div.lf-location__store__infos__shop>h1.lf-location__store__infos__shop__title"+
                		   		                                                         ">span.lf-location__store__infos__shop__contact__title__span").first();
                		   agencyName=(agencyNameJsoupElement!=null) ? this.normalize(agencyNameJsoupElement.text().trim()):"";
                		  
                		 
                		   //retrieve agency address
                		   Element agencyStreetAddressJsoupElement= agencyInfoJsoupElement.select( "div.lf-location__store__infos__shop>div.lf-location__store__infos__shop__content>"+
                		   		                                                                   "div.lf-location__store__infos__shop__content--left>#locations-address-default > "+
                		   		                                                                   "div.lf-locations-address-default__street").first();
                		   String agencyStreetAddress=(agencyStreetAddressJsoupElement!=null)? this.normalize(agencyStreetAddressJsoupElement.text()):"";
                		   Element agencyCityAddressJsoupElement= agencyInfoJsoupElement.select( "div.lf-location__store__infos__shop>div.lf-location__store__infos__shop__content>"+
                                                                                                 "div.lf-location__store__infos__shop__content--left> #locations-address-default >"+
                				                                                                 "div.lf-locations-address-default__city").first();
                		   String agencyCityAddress=(agencyCityAddressJsoupElement!=null)?this.normalize(agencyCityAddressJsoupElement.text()):"";
                		   agencyAddress=agencyStreetAddress+" "+agencyCityAddress;
                		   
                		   
                		   //retrieve agency schedule
                		   Elements agencyScheduleJsoupElement=agencyInfoJsoupElement.select( "div.lf-location__store__infos__shop>div.lf-location__store__infos__shop__content>"+
                		   		                                                              "div.lf-location__store__infos__shop__content--right>div#lf-menu-hours>"+
                		   		                                                               "div.lf-location__store__infos__shop__content--right__hours__content>"+
                		   		                                                               "div#lf-openinghours>div.lf-location-opening-hours-default__row>div.lf-location-opening-hours-default__row__column");
                		   for(Element elementTmp2: agencyScheduleJsoupElement) 
                		   {
                			   agencySchedule+=elementTmp2.text().trim()+" ";
                		   }
                		  agencySchedule=this.normalize(agencySchedule);
                		   //retrieve agency phone Number
                		   Element agencyPhoneJsoupElement=agencyInfoJsoupElement.select( "div.lf-location__store__infos__ctas>div#lf-location-phone>"+
                				                                                          "a[class^=\"lf-location-phone-default__phone\"]").first();
                		   agencyPhone=(agencyPhoneJsoupElement!=null)?agencyPhoneJsoupElement.text():"";
                		  
                		   
                		   this.enregistrer(agencyLink, agencyName, agencyAddress, agencyPhone, agencySchedule, agencyHTML);
                		   eoviList.add(this.setData(DepSoumis, String.valueOf(NbAgences),agencyLink, 
                				                     agencyName, agencyAddress, agencyPhone, agencySchedule, agencyHTML));
                	   }
                	   this.enregistrer_journal(nbRecuperes);
                	}
                }
           } 
           parcoursficval++; 
    		
        }
        sc.close();
        return eoviList;
    }
   
	
    public void enregistrer ( String ville, String nom_agence, String adresse,
                              String tel, String Horaires,String agence)  
    {
       try {
    	
    		sortie = new BufferedWriter(new FileWriter(fichier,true));
    		sortie.write(DepSoumis+"\t"+NbAgences+"\t"+ville+"\t"+ 
    				     nom_agence+"\t"+adresse+"\t"+tel+"\t"+ 
    				     Horaires + "\t"+ agence + "\r\n");	
    		sortie.close();
		} catch (IOException e) {
			
		}			
    }
    
    public void enregistrer_journal (int numreponse)  
    {
    	 try {
   		       Temps_Fin = new Date();
   		       SimpleDateFormat formater = null;
   		       formater = new SimpleDateFormat("'le' dd/MM/yyyy '�' hh:mm:ss");
   		       String date =  formater.format(Temps_Fin);
   		       date =  formater.format(Temps_Fin);
   		       sortie_log = new BufferedWriter(new FileWriter(log,true));
   	   		   sortie_log.write(parcoursficval+";"+"FinValeur"+";"+
   								DepSoumis+";"+NbAgences+";"+nbRecuperes+";"+ 
   								(NbAgences - nbRecuperes)+";"+date+"\r\n"); 
   	   		    sortie_log.close();
   		        long  taille_fichier = fichier.length()/1000000; 
   	            if(taille_fichier> 8) {
   		             fichier = new File(dossierRESU + "\\"+ "RESULTATS_" + nomSITE +   "_" + System.currentTimeMillis() + ".txt");
   	    	         sortie = new BufferedWriter(new FileWriter(fichier,true));
           	         sortie.write("DepSoumis"+"\t"+"NbAgences"+"\t"+"Lien_agence"+"\t"+   
           	        		      "NOM_AGENCE"+"\t"+"ADRESSE"+"\t"+"TEL"+"\t"+"HORAIRES"+"\t"+"AGENCEHTM"+ "\r\n");
           	  	     sortie.close(); 
   				}
   		
		} catch (IOException e) {
			 
		}
	}
    
    
    public void initialiser() {
        try{
        	sortie = new BufferedWriter(new FileWriter(fichier,true));
        	sortie.write( "DepSoumis"+"\t"+"NbAgences"+"\t"+"VILLE_SOUMISE"+"\t"+ 
        			      "NOM_AGENCE"+"\t"+"ADRESSE"+"\t"+"TEL"+"\t"+"HORAIRES"+
      					   "\t"+"AGENCEHTM"+"\r\n");
            sortie.close();
        	File toto = new File(dossierRESU + "\\"+  "LOG_" + nomSITE +  ".csv"); 
            if( !toto.exists() ){
        	  sortie_log = new BufferedWriter(new FileWriter(log,true));
	        	sortie_log.write( "NumLigne"+";"+"FinValeur"+";"+"DepSoumis"+";"+"NbAgences"+ 
        	                      ";"+"Nbrecuperes"+";"+"Reste"+";"+"Heure_Fin"+"\r\n" );
	        	sortie_log.close();
        	}
        } catch (IOException e) {
        	 
		}
    }
    
    
	public void tearDown() {
         try{
        	 wd.quit();
         }catch(Exception e) {
        	
         }
        
    }
}
