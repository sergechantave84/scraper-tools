package com.app.scrap;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.app.model.MATMUT;

@Repository
public interface RepMatmut extends JpaRepository<MATMUT,Integer>,RepositoryCustom<MATMUT>{

}
