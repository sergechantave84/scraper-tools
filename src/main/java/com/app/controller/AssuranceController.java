package com.app.controller;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.app.service.AbeilleService;
import com.app.service.ServiceAbeille2;
import com.app.service.ServiceAesio;
import com.app.service.ServiceAesio2;
import com.app.service.ServiceAg2r;
import com.app.service.ServiceAllianz;
import com.app.service.ServiceAllianz2;
import com.app.service.ServiceAreas;
import com.app.service.ServiceAssu2000;
import com.app.service.ServiceAviva;
import com.app.service.ServiceAxa;
import com.app.service.ServiceGan;
import com.app.service.ServiceGenerali;
import com.app.service.ServiceGenerali2;
import com.app.service.ServiceGmf;
import com.app.service.ServiceGroupama;
import com.app.service.ServiceGroupama2;
import com.app.service.ServiceGroupama2;
import com.app.service.ServiceHarmonie;
import com.app.service.ServiceMAAF;
import com.app.service.ServiceMACIF;
import com.app.service.ServiceMAIF;
import com.app.service.ServiceMMA;
import com.app.service.ServiceMatmut;
import com.app.service.ServiceMutuGenerale;
import com.app.service.ServiceMutuPoitier;
import com.app.service.ServiceMutualia;
import com.app.service.ServiceSwisslife;
import com.app.service.ServiceThelem;
import com.app.service.ServiceThelem2;

@RestController
@RequestMapping("/scrap")
public class AssuranceController {

	@Autowired
	private ServiceAesio srvAESIO;
	
	@Autowired
	private ServiceAg2r srvAG2R;
	
	@Autowired
	private ServiceAllianz srvAllianz;
	
	@Autowired
	private ServiceAreas srvAreas;
	
	@Autowired
	private ServiceAssu2000 srvAssu2000;
	
	@Autowired
	private ServiceAviva srvAviva;
	
	@Autowired
	private ServiceAxa srvAxa;
	
	@Autowired
	private ServiceGan srvGan;
	
	@Autowired
	private ServiceGenerali2 srvGenerali;
	
	@Autowired
	private ServiceGmf srvGmf;
	
	@Autowired
	private ServiceGroupama srvGroupama;
	
	@Autowired
	private ServiceHarmonie srvHarmonie;
	
	@Autowired
	private ServiceMAAF srvMAAF;
	
	@Autowired
	private ServiceMACIF srvMACIF;
	
	@Autowired
	private ServiceMAIF srvMaif;
	
	@Autowired
	private ServiceMatmut srvMatmut;
	
	@Autowired
	private ServiceMMA srvMMA;
	
	@Autowired
	private ServiceMutualia srvMutualia;
	
	@Autowired
	private ServiceMutuGenerale srvMUGenerale;
	
	@Autowired
	private ServiceMutuPoitier srvMutuPoitier;
	
	@Autowired
	private ServiceSwisslife srvSwisslife;
	
	@Autowired
	private ServiceThelem srvThelem;
	
	@Autowired
	private AbeilleService srvAbeille;
	
	@Autowired
	private ServiceAbeille2 srvAb;
	
	
	@RequestMapping("/AESIO")
	public void setAesioData() throws Exception {
		//srvAESIO.setUp();
		//srvAESIO.scrap();
		srvAESIO.setup();
		srvAESIO.scrapAesio();
		srvAESIO.logOut();
		//srvAESIO.tearDown();
		
	}
	
	@RequestMapping("/AG2R")
	public void setAG2RData() throws Exception {
		//srvAG2R.setUp();
		srvAG2R.scrap();
		//srvAG2R.tearDown();
		
	}
	@RequestMapping("/ALLIANZ")
	public void setAllianz() throws Exception {
		srvAllianz.setUp();
		srvAllianz.scrapALLIANZ();
		//srvAllianz.scrap();
		srvAllianz.tearDown();
	}
	@RequestMapping("/AREAS")
	public void setAreas() throws Exception {
		srvAreas.setup();
		srvAreas.scrap();
		srvAreas.tearDown();
	}
	@RequestMapping("/ASSU2000")
	public void setAssu2000() throws Exception {
		//srvAssu2000.setUp();
		srvAssu2000.scrap();
		//srvAssu2000.tearDown();
	}
	@RequestMapping("/AVIVA")
	public void setAviva() throws Exception {
		/*srvAviva.setUpHtmlUintDriver();
		srvAviva.scrapAVIVA();
		srvAviva.tearDown();
		*/
		srvAb.setUpHtmlUintDriver();
		srvAb.scrap();
		
		
	}
	@RequestMapping("/AXA")
	public void setAXA() throws Exception {
		srvAxa.setUp();
		srvAxa.scrap();
		srvAxa.tearDown();
		//srvAxa.show2();
	}
	@RequestMapping("/GAN")
	public void setGAN() throws Exception {
		srvGan.setUp();
		srvGan.open_GAN_byselenium();
		srvGan.tearDown();
	}
	@RequestMapping("/GENERALI")
	public void setGenerali() throws Exception {
		srvGenerali.setUp();
		srvGenerali.open_GENERALI_byselenium();
		srvGenerali.tearDown();
	}
	@RequestMapping("/GMF")
	public void setGMF() throws Exception {
		srvGmf.setUp();
		srvGmf.open_GMF_byselenium();
		srvGmf.tearDown();
	}
	@RequestMapping("/GROUPAMA")
	public void setGroupama() throws Exception {
		srvGroupama.setUp();
		//srvGroupama.scrap();
		srvGroupama.open_GROUPAMA_byselenium();
		srvGroupama.tearDown();
	}
	@RequestMapping("/HARMONIE")
	public void setHarmonie() throws Exception {
		srvHarmonie.setUp();
		srvHarmonie.scrapHarmonie();
		srvHarmonie.tearDown();
	}
	@RequestMapping("/MAAF")
	public void setMAAF() throws Exception {
		srvMAAF.setUp();
		srvMAAF.scrap();
		
	}
	@RequestMapping("/MACIF")
	public void setMACIF() throws Exception {
		
		srvMACIF.scrap();
				
	}
	@RequestMapping("/MAIF")
	public void setMAIF() throws Exception {
		
		srvMaif.scrap();
		
		
	}
	@RequestMapping("/MATMUT")
	public void setMATMUT() throws Exception {
		srvMatmut.setUp();
		srvMatmut.scrap();
		srvMatmut.tearDown();
    }
	@RequestMapping("/MMA")
	public void setMMA() throws Exception {
		srvMMA.setUp();
		srvMMA.scrap();
		srvMMA.tearDown();
	}
	@RequestMapping("/MUTUALIA")
	public void setMutua() throws IOException {
		srvMutualia.setup();
		srvMutualia.scrapMutualia();
		srvMutualia.tearsDown();
	}
	@RequestMapping("/MUTUEL_GENERALE")
	public void setMutuelle_GENE() throws IOException {
		srvMUGenerale.setup();
		srvMUGenerale.scrapMGeneral();
		srvMUGenerale.tearsDown();
	}
	@RequestMapping("/MUTUEL_POITIER")
	public void setMutuellePoitier() throws IOException, InterruptedException {
		srvMutuPoitier.setup();
		srvMutuPoitier.scrapMutuellePoitier();
		srvMutuPoitier.tearsDown();
	}
	@RequestMapping("/SWISSLIFE")
	public void setSwisslife() throws Exception {
		srvSwisslife.setUp();
		srvSwisslife.open_SWISSLIFE_byselenium();
		srvSwisslife.tearDown();
	}
	@RequestMapping("/THELEM")
	public void setThelem() throws IOException, InterruptedException {
		srvThelem.setup();
		srvThelem.scrap();
		srvThelem.tearsDown();
	}
	
}
