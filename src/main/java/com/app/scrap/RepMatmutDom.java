package com.app.scrap;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.app.model.MATMUT;
import com.app.model.MATMUT_DOM;

@Repository
public interface RepMatmutDom extends JpaRepository<MATMUT_DOM,Integer>,RepositoryCustom<MATMUT_DOM>{

}
