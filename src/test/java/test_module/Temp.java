package test_module;

import java.io.BufferedInputStream;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.math.BigInteger;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

import org.hibernate.internal.build.AllowSysOut;
import org.junit.Ignore;
import org.junit.Test;

public class Temp {

	@Ignore
	@Test
	public void test() throws IOException {
		Path p = Paths.get("C:\\Datasources");
		DirectoryStream<Path> stream = Files.newDirectoryStream(p, "*.txt");
		for (Path p2 : stream) {

			if (p2.getFileName().toString().contains("VISA_CPCANTON")) {
				System.out.println(p2.getFileName());
				Scanner sc = new Scanner(new BufferedInputStream(new FileInputStream(new File(p2.toString()))));
				BufferedWriter bw = new BufferedWriter(new FileWriter(new File("VISA_CPCANTON.txt"), true));
				int i = 0;
				while (sc.hasNextLine()) {
					String line = sc.nextLine();
					if (i >= 1) {
						bw.write(line + "\r\n");
					}
					i++;
				}
				sc.close();
				bw.close();
			}
		}
		System.out.println("finis");
	}

	@Test
	public void tes() {
		System.out.println(shading(pascalTriangleH(930887),930887,new BigInteger("5")));
	}

	public BigInteger[][] pascalTriangle(int r) {

		BigInteger[][] pT = new BigInteger[r][r];
		for (int i = 0; i < r; i++) {
			for (int j = 0; j < r; j++) {
				pT[i][j] = new BigInteger("0");
			}
		}

		pT[0][0] = new BigInteger("1");
		for (int i = 1; i < r; i++) {
			for (int j = 0; j < r; j++) {
				if (j != 0)
					pT[i][j] = pT[i - 1][j - 1].add(pT[i - 1][j]);
				else
					pT[i][j] = new BigInteger("1");
			}
		}
		printTab(pT, r);
		return pT;
	}

	public List<BigInteger> pascaltriangle2(int r) {

		List<BigInteger> ls = new ArrayList<>();

		for (int i = 0; i < (r * r); i++) {
			System.out.println(i);
			ls.add(i, new BigInteger("0"));
		}
		ls.set(0, new BigInteger("1"));
		for (int i = 1; i < r; i++) {
			for (int j = 0; j < r; j++) {
				int index = j + (i * r);
				if (j != 0) {
					int tmpIndex0 = (j - 1) + ((i - 1) * r);
					int tmpIndex1 = j + ((i - 1) * r);

					ls.set(index, (ls.get(tmpIndex0).add(ls.get(tmpIndex1))));

				} else
					ls.set(index, new BigInteger("1"));
			}
		}
		printTab(ls, r);
		return ls;

	}

	public Map<BigInteger, BigInteger> pascaltriangle3(BigInteger bI) {

		Map<BigInteger, BigInteger> m = new HashMap<>();
		BigInteger mult = bI.multiply(bI);
		for (BigInteger i = BigInteger.ZERO; i.compareTo(mult) < 0; i = i.add(BigInteger.ONE)) {
			m.put(i, BigInteger.ZERO);
		}

		m.put(BigInteger.ZERO, BigInteger.ONE);

		for (BigInteger i = BigInteger.ONE; i.compareTo(bI) < 0; i = i.add(BigInteger.ONE)) {
			for (BigInteger j = BigInteger.ZERO; j.compareTo(bI) < 0; j = j.add(BigInteger.ONE)) {

				BigInteger index = j.add((i.multiply(bI)));

				if (j.compareTo(BigInteger.ZERO) != 0) {

					BigInteger tmp = (i.subtract(BigInteger.ONE)).multiply(bI);
					BigInteger tmpIndex0 = (j.subtract(BigInteger.ONE)).add(tmp);
					BigInteger tmpIndex1 = j.add(tmp);

					BigInteger tmp2 = m.get(tmpIndex0).add(m.get(tmpIndex1));

					m.put(index, tmp2);
				} else {
					m.put(index, BigInteger.ONE);
				}
			}

		}
		printTab(m, bI);
		return m;

	}

	public HashMap<Long, HashMap<Long, BigInteger>> pascalTriangleH(long r) {
		HashMap<Long, HashMap<Long, BigInteger>> map = new HashMap<>();
		for (long i = 0; i < r; i++) {
			HashMap<Long, BigInteger> resTmp = new HashMap<>();
			HashMap<Long, BigInteger> pre = new HashMap<>();
			for (long j = 0; j <= i; j++) {

				if (j == i || j == 0) {
					resTmp.put(j, BigInteger.ONE);
					map.put(i, resTmp);

				} else {

					pre = map.get((i - 1));
					
					resTmp.put(j, (pre.get(j - 1).add(pre.get(j))));
					map.put(i, resTmp);
				}
				
			}
			
		}
		System.out.println(map.get(r-1));
        return map;
	}
	
	public long shading(HashMap<Long,HashMap<Long, BigInteger>> map, long c, BigInteger p) {
		long compteur=0l;
		for(long key: map.keySet()) {
			HashMap<Long,BigInteger> v=new HashMap<>();
			v=map.get(key);
			for(long i=0; i<v.size(); i++) {
				if(i <= (c-1)) {
					if(!v.get(i).mod(p).equals(BigInteger.ZERO)) {
						compteur++;
					}
				}
			}
		}
		return compteur % 1000000007;
	}

	public void printTab(Map<Long, Long> pT, long bI) {
		long index = 1;
		for (long i = 1; i <= bI; i++) {

			for (long k = 1; k <= i; k++) {

				if (i > 1)
					index++;// = index.add(BigInteger.ONE);

				System.out.print(pT.get(index) + " ");
			}
			;
			System.out.println("");
		}
	}

	public Map<BigInteger, BigInteger> pascalTriangle4(BigInteger bI) {
		BigInteger c = BigInteger.ONE;
		Map<BigInteger, BigInteger> map = new HashMap<>();

		BigInteger index = BigInteger.ONE;
		for (BigInteger i = BigInteger.ONE; i.compareTo(bI) <= 0; i = i.add(BigInteger.ONE)) {
			c = BigInteger.ONE;
			for (BigInteger k = BigInteger.ONE; k.compareTo(i) <= 0; k = k.add(BigInteger.ONE)) {

				if (i.compareTo(BigInteger.ONE) > 0)
					index = index.add(BigInteger.ONE);

				// System.out.println(index);
				map.put(index, c);
				c = (c.multiply((i.subtract(k)))).divide(k);

			}
			// System.out.println("last index "+index);

		}

		return map;

	}

	public void printTab(Map<BigInteger, BigInteger> pT, BigInteger bI) {
		BigInteger index = BigInteger.ONE;
		for (BigInteger i = BigInteger.ONE; i.compareTo(bI) <= 0; i = i.add(BigInteger.ONE)) {

			for (BigInteger k = BigInteger.ONE; k.compareTo(i) <= 0; k = k.add(BigInteger.ONE)) {

				if (i.compareTo(BigInteger.ONE) > 0)
					index = index.add(BigInteger.ONE);

				System.out.print(pT.get(index) + " ");
			}
			;
			System.out.println("");
		}
	}

	public BigInteger shading(Map<BigInteger, BigInteger> pT, BigInteger bI, BigInteger c, BigInteger p) {
		BigInteger numberShading = BigInteger.ZERO;
		BigInteger index = BigInteger.ONE;
		BigInteger sum = BigInteger.ZERO;
		for (BigInteger i = BigInteger.ONE; i.compareTo(bI) <= 0; i = i.add(BigInteger.ONE)) {
			numberShading = BigInteger.ZERO;
			for (BigInteger k = BigInteger.ONE; k.compareTo(i) <= 0; k = k.add(BigInteger.ONE)) {

				if (i.compareTo(BigInteger.ONE) > 0)
					index = index.add(BigInteger.ONE);

				if (k.compareTo(c) <= 0) {
					if (!pT.get(index).mod(p).equals(BigInteger.ZERO))
						numberShading = numberShading.add(BigInteger.ONE);
				}

			}
			System.out.println(i + " " + numberShading);
			sum = sum.add(numberShading);

		}
		return sum;
	}

	public int shading(BigInteger[][] pT, int r, int c, long p) {
		int numberShadingPerRow = 0;
		int sum = 0;
		for (int i = 0; i < r; i++) {
			numberShadingPerRow = 0;
			for (int j = 0; j < (i + 1); j++) {
				if (j < c)
					if (!(pT[i][j].mod(new BigInteger(String.valueOf(p)))).equals(BigInteger.ZERO))
						numberShadingPerRow++;
			}
			System.out.println(i + " " + numberShadingPerRow);
			sum += numberShadingPerRow;
		}
		return (sum);
	}

	public static int shading(List<BigInteger> pT, int r, int c, long p) {
		int numberShadingPerRow = 0;
		int sum = 0;
		for (int i = 0; i < r; i++) {
			numberShadingPerRow = 0;
			for (int j = 0; j < (i + 1); j++) {
				int index = j + (i * r);
				if (j < c)
					if (!(pT.get(index).mod(new BigInteger(String.valueOf(p)))).equals(BigInteger.ZERO))
						numberShadingPerRow++;
			}

			sum += numberShadingPerRow;
		}
		return (sum);
	}

	public void printTab(List<BigInteger> ls, int r) {
		for (int i = 0; i < r; i++) {
			for (int j = 0; j < r; j++) {
				int index = j + (i * r);
				System.out.print(ls.get(index) + " ");
			}
			System.out.println();
		}
	}

	public void printTab(BigInteger[][] pT, int r) {
		for (int i = 0; i < r; i++) {
			for (int j = 0; j < r; j++) {
				System.out.print(pT[i][j] + " ");
			}
			System.out.println();
		}
	}
}
