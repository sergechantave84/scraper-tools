package com.app.scrap;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.data.repository.RepositoryDefinition;


@SpringBootApplication

@ComponentScan({"com.app.controller","com.app.controller.admin","com.app.service","com.app.model","com.app.libs","com.app.service.admin"})
@EntityScan("com.app.model")

public class Scrapper1Application {

	public static void main(String[] args) {
		SpringApplication.run(Scrapper1Application.class, args);
	}

}
