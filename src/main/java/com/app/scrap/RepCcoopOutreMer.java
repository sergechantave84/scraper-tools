package com.app.scrap;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.app.model.CCOOP_Outre_Mer;

@Repository
public interface RepCcoopOutreMer extends JpaRepository<CCOOP_Outre_Mer, Integer>, RepositoryCustom<CCOOP_Outre_Mer>{

}
