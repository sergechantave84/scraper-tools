package com.app.scrap;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.app.model.BCFOI;

@Repository
public interface RepBCFOI extends JpaRepository<BCFOI,Integer>,RepositoryCustom<BCFOI> {

}
