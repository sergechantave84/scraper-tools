package com.app.service;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.text.Normalizer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Scanner;
import java.util.function.Function;

import org.hibernate.mapping.Array;
import org.json.JSONArray;
import org.json.JSONObject;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.app.model.AbeilleAssurance;
import com.app.model.Aviva;
import com.app.model.PJAssuranceDep;
import com.app.scrap.AbeilleRepository;
import com.app.utils.Parametres;
import com.app.utils.Utilitaires;
import com.shapesecurity.salvation.data.URI;

import fa.browserUtils.FactoryBrowser;
import fa.browserUtils.HtmlUnitBrowser;
import fa.log.LogApp.LogScrap;
import fa.log.config.LogConst;

@Service
public class ServiceAbeille2 extends Scraper {

	@Autowired
	AbeilleRepository rep;

	//private String nomSITE = "ABEILLE";
	private String ficval = "abeille";

	//private String url_accueil = "https://www.aviva.fr/";
	private String url_accueil = "https://www.abeille-assurances.fr";

	private int lastLine;

	private HtmlUnitDriver htmlUnitDriver;
	private ChromeDriver ch;

	public AbeilleAssurance saveData(AbeilleAssurance t) {
		return rep.save(t);
	}

	public List<AbeilleAssurance> saveDataAll(List<AbeilleAssurance> t) {
		return rep.saveAll(t);
	}

	public List<AbeilleAssurance> getElmnt() {

		return rep.findAll();
	}

	public AbeilleAssurance getById(int id) {

		return (AbeilleAssurance) rep.findById(id).orElse(null);
	}
	
	public void show() {
		List<AbeilleAssurance> res= rep.getFirstRow(10, AbeilleAssurance.class);
		for(AbeilleAssurance ab : res) {
			System.out.println(ab.getId());
		}
	}

	public String deleteProduct(int id) {
		try {
			rep.deleteById(id);

		} catch (Exception e) {
			e.printStackTrace();
		}

		return "removed";
	}
	
	public void deleteAll() {
        rep.deleteAll();
       System.out.println("Deleted all");
    }
	
	public List<AbeilleAssurance> showAll() {
		System.out.println(rep.findAll());
		return rep.findAll();
	}

	public AbeilleAssurance updateProduct(AbeilleAssurance t) {
		// AvivaODO Auto-generated method stub
		return null;
	}

	public void setUpHtmlUintDriver() throws Exception {
		
		  FactoryBrowser factoryB = new FactoryBrowser(); 
		  HtmlUnitBrowser htmlB = (HtmlUnitBrowser) factoryB.create("htmlUnit"); 
		  htmlUnitDriver = htmlB.setOptions(true, false, false, 60000).initBrowserDriver();
		
		final String chromeBeta = "C:\\Program Files\\Google\\Chrome Beta\\Application\\chrome.exe";
		ch = (ChromeDriver) this.setupCh(Parametres.CHROME_DRIVER_PATH, true, false,"C:\\Users\\GEOMADA PC4\\AppData\\Local\\Google\\Chrome Beta\\User Data", "", chromeBeta);
		
		
		/*DesiredCapabilities caps = DesiredCapabilities.chrome();
		caps.setCapability("chrome.switches", Arrays.asList("--disable-javascript"));*/
		
	}

	public void scrap() throws IOException, InterruptedException {

		Scanner sc = new Scanner(new BufferedInputStream(
				new FileInputStream(new File(Parametres.REPERTOIRE_DATA_SOURCES + ficval + ".txt"))));

		LogScrap log = new LogScrap("AssuranceAbeille.txt", LogConst.TYPE_LOG_FILE);

		if ((new File(LogConst.STORE_LOG_PATH + LogConst.PREFIX_LOG_FILE + "_AssuranceAbeille.txt").exists())) {
			Scanner sc3 = new Scanner(new BufferedInputStream(new FileInputStream(
					new File(LogConst.STORE_LOG_PATH + LogConst.PREFIX_LOG_FILE + "_AssuranceAbeille.txt"))));
			while (sc3.hasNextLine()) {
				lastLine = Integer.parseInt(sc3.nextLine().split("\t")[4]);
			}
		}

		int currentLine = 1;
		
		String nom_agents = "";
		String nom_agence = "";
		String adresse = "";
		String tel = "";
		String fax = "";
		String horaires = "";
		String ORIAS = "";
		String lien ="";
		
		String pageSource ="";
		Document doc = null;

		while (sc.hasNextLine()) {
			String line[] = sc.nextLine().split(";");
			String uri = line[1];
			
			String depSoumise = uri.split("/")[4].replace(".html", "");

			if (currentLine > lastLine) {

				System.out.println("Uri : " + uri);

				ch.get(uri);

				pageSource = ch.getPageSource();

				doc = Jsoup.parse(pageSource);
				
				//System.out.println(doc);
				
				Elements linksDep = doc.select("#main > div:nth-child(2) > div:nth-child(2) > div.podContent > ul > li > a");
				
				if(linksDep.size() > 0) {
					
					for(Element linkDep : linksDep) {
						String link = "https://www.abeille-assurances.fr" +linkDep.attr("href").replaceAll(" ","%20");
						
						System.out.println("Lien dep : "+link);
						
						ch.get(link);
						
						pageSource =  ch.getPageSource();
						doc = Jsoup.parse(pageSource);
						
						Element rootAgence = doc.selectFirst("#main > div:nth-child(3) > div:nth-child(2)");
						
						Elements linksAgence = rootAgence.select("div.podContent > ul > li > a");
						
						if(linksAgence.size() > 0) {

							for(Element linkAgence : linksAgence) {
								String url = linkAgence.attr("href");
								
								System.out.println("url : "+url);
								
								if(url != "") {
									htmlUnitDriver.get(url);
									
									nom_agents="";
									
									pageSource =  htmlUnitDriver.getPageSource();
									pageSource = Normalizer.normalize(htmlUnitDriver.getPageSource().toLowerCase(), Normalizer.Form.NFD)
											.replaceAll("[^\\p{ASCII}]", "");
									doc = Jsoup.parse(pageSource);
									
									Element nomElem = doc.selectFirst("#map_aname");
									Element adresseElem = doc.selectFirst("#map_loc");
									Element telElem = doc.selectFirst("#map_phone");
									Element horElem = doc.selectFirst("#nous-trouver > div > div.map-section-text > div.map-timing.info-text-style > div:nth-child(2) > table > tbody");
									Elements agentsElem = doc.select("#notre-equipe > div > div");
									
									if(agentsElem.size() > 0) {
										for(Element agentElem : agentsElem) {
											if(agentElem.text().contains("agent general")) {
												nom_agents+=agentElem.text().replaceAll("agent general", "").trim()+" & ";
												
											}
										}
									}
									
									nom_agence = nomElem != null? this.normalize(nomElem.text().toUpperCase()).trim() :"";
									adresse = adresseElem !=null? this.normalize(adresseElem.text().toUpperCase()).trim() :"";
									tel = telElem !=null ? this.normalize(telElem.text()).trim() :"";
									horaires = horElem !=null ? this.normalize(horElem.text()).trim() :"";
									
									if(nom_agence != "" && adresse!= "") {
										this.saveItem(depSoumise,nom_agence,adresse,ORIAS,nom_agents,tel,horaires,lien, fax);
									}
									
									System.out.println("nom_agence : "+nom_agence);
									System.out.println("adresse : "+adresse);
									System.out.println("tel : "+tel);
									System.out.println("horaires : "+horaires);
									System.out.println("nom_agents : "+nom_agents);
									System.out.println("-----------------------------------------------\n");
								}
								
							}
						}
						
					}
				}

				log.writeLog(new Date(), line[0], "no_error", 1, currentLine, 0);
				
				System.out.println("**********************************************\n");

			}

			currentLine++;
		}
	}
	

	public void waitForPageLoad(WebDriver driver_) {
		new WebDriverWait(driver_, 2).until(
			      webDriver -> ((JavascriptExecutor) webDriver).executeScript("return document.readyState").equals("complete"));
	}

	@Override
	public void saveItem(String... args) {
		
		AbeilleAssurance abAss = new AbeilleAssurance();
		
		abAss.setDepSoumise(args[0]).setNomAgence(args[1]).setAdresse(args[2])
				.setOrias(args[3]).setNomAgents(args[4]).setTel(args[5]).setHoraires(args[6]).setLienAgence(args[7]).setFax(args[8]);
		rep.save(abAss);
		
	}

}
