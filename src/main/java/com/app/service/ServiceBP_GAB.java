package com.app.service;

import java.io.BufferedInputStream;
import java.io.BufferedReader;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;

import java.net.URL;


import java.util.Date;

import java.util.Scanner;

import javax.net.ssl.HttpsURLConnection;

import org.apache.commons.text.StringEscapeUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;




import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.app.model.BP_GAB;
import com.app.scrap.RepBpGab;

import com.app.utils.Parametres;

import fa.log.LogApp.LogScrap;
import fa.log.config.LogConst;

@Service
public class ServiceBP_GAB extends Scraper {

	@Autowired
	private RepBpGab rep;

	private String ficval = "lien_bp_gab";

	private String cp;
	private String fichier_valeurs;

	private int NbAgences;
	private int nbRecuperes;
	private int num_ligneficval;
	private int parcoursficval;
	private int HORS_SITE;
	private int DAB = 1;

	/*
	 * public BP_GAB setData(String depSoumis, String total, String nbAgences,
	 * String enseigne, String nomWeb, String adresse, String dab, String horsSite,
	 * String agenceHtm) { BP_GAB bpGab = new BP_GAB();
	 * bpGab.setDepSoumis(depSoumis).setTotal(total).setNbAgences(nbAgences).
	 * setEnseigne(enseigne).setNomWeb(nomWeb)
	 * .setAdresse(adresse).setDab(dab).setHorsSite(horsSite).setAgenceHtm(agenceHtm
	 * ); this.saveData(bpGab); return bpGab; }
	 */

	@Override
	public void saveItem(String... args) {
		BP_GAB bpGab = new BP_GAB();
		bpGab.setDepSoumis(args[0]).setTotal(args[1]).setNbAgences(args[2]).setEnseigne(args[3]).setNomWeb(args[4])
				.setAdresse(args[5]).setDab(args[6]).setHorsSite(args[7]).setAgenceHtm(args[8]);
		rep.save(bpGab);

	}

	@Override
	public void scrap() throws IOException, InterruptedException {

		fichier_valeurs = Parametres.REPERTOIRE_DATA_SOURCES + ficval + ".txt";
		Scanner sc = new Scanner(new BufferedInputStream(new FileInputStream(new File(fichier_valeurs))));

		LogScrap log = new LogScrap("BanqueBPGab.txt", LogConst.TYPE_LOG_FILE);
		num_ligneficval = ((new File(LogConst.STORE_LOG_PATH + LogConst.PREFIX_LOG_FILE + "_BanqueBPGab.txt")).exists())
				? Integer.parseInt(log.readLog().getLastLineProcessed().split("\\t")[4])
				: 1;

		parcoursficval = 1;
		while (sc.hasNextLine()) {
			String line = sc.nextLine();
			//String[] fic_valeurs = line.split("\\t");
			if (parcoursficval >= num_ligneficval) {
				
				cp = line;
				
				System.out.println("url : " + cp);

				URL url = new URL(cp);
				HttpsURLConnection con = (HttpsURLConnection) url.openConnection();
				con.setRequestMethod("GET");
				con.setConnectTimeout(60000);
				con.setReadTimeout(60000);
				con.setInstanceFollowRedirects(true);
				BufferedReader br = new BufferedReader(new InputStreamReader(con.getInputStream(), "ISO-8859-1"));
				String pageSource;
				StringBuilder strB = new StringBuilder();
				while ((pageSource = br.readLine()) != null) {
					strB.append(pageSource);
				}
				pageSource = StringEscapeUtils.unescapeHtml4(strB.toString());

				Document doc = null;
				doc = Jsoup.parse(pageSource);
				Element rootJElement = doc.select(
						"body > div.em-page > main > div.em-results-wrapper > div > div.em-results__list-wrapper > div")
						.first();
				Elements agencyLinksJElement = rootJElement.select("ul > li");
				NbAgences = agencyLinksJElement.size();
				System.out.println("NbAgences : " + NbAgences);
				nbRecuperes = 0;

				for (Element elmntTmp : agencyLinksJElement) {
					String nom_agence = "";
					String adresse = "";
					String enseigne = "", agences_HTM = "";

					agences_HTM = elmntTmp.outerHtml().trim();
					agences_HTM = this.normalize(agences_HTM.replaceAll("\\s{1,}", ""));
					// retrieve agency name
					Element agencyNameJElement = elmntTmp.select("h2 >  a").first();
					nom_agence = (agencyNameJElement != null) ? this.normalize(agencyNameJElement.text().toUpperCase()) : "";

					System.out.println("nom agence : " + nom_agence);
					// retrieve agency address

					Element agencyAddressJElement = elmntTmp.select("div.em-poi-card__address").first();
					adresse = (agencyAddressJElement != null) ? this.normalize(agencyAddressJElement.text().replaceAll(",", "").toUpperCase()) : "";
					System.out.println("adresse : " + adresse);

					// retrieve agency anseigne
					Element agencyEnseignJElement = elmntTmp.select("h2>span.em-poi-card__network").first();
					enseigne = (agencyEnseignJElement != null) ? this.normalize(agencyEnseignJElement.text()) : "";
					System.out.println("enseigne : " + enseigne);

					HORS_SITE = (nom_agence.toLowerCase().contains("agence")) ? 0 : 1;
					
					System.out.println("\n-----------------------\n");

					nbRecuperes++;
					this.saveItem(cp, String.valueOf(NbAgences), String.valueOf(NbAgences), enseigne, nom_agence,
							adresse, String.valueOf(DAB), String.valueOf(HORS_SITE), agences_HTM);
				}

				log.writeLog(new Date(), cp, "no_errors", NbAgences, parcoursficval, NbAgences - nbRecuperes);
				con.disconnect();
			}
			parcoursficval++;
		}
		sc.close();

	}

}
