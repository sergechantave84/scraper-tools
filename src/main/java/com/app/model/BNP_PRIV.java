package com.app.model;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="banque_bnp_privee", schema="scrap")
public class BNP_PRIV {
	
	@Id
	@SequenceGenerator(name = "scrap.banque_bnp_privee_id_seq",
	sequenceName = "scrap.banque_bnp_privee_id_seq",
	allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE,
	                 generator = "scrap.banque_bnp_privee_id_seq")
	@Column(name="id",unique = true, nullable = false)
	private int id;
	
	@Column(name="nom_agence", columnDefinition = "varchar")
	private String agencyName;
	
	@Column(name="type_agence",columnDefinition = "varchar")
	private String agencyType;
	
	@Column(name="departement", columnDefinition = "varchar")
	private String dep;
	
	@Column(name="region",columnDefinition = "varchar")
	private String region;
	
	@Column(name="adresse",columnDefinition="varchar")
	private String adresse;
	
	@Column(name="telephone",columnDefinition = "varchar")
	private String tel;
	
	@Column(name="fax",columnDefinition = "varchar")
	private String fax;
	
	@Column(name="horaires", columnDefinition = "varchar")
	private String svhedule;
	
	@Column(name="gab", columnDefinition ="varchar")
	private String gab;
	
	@Column(name="zone_express", columnDefinition = "varchar")
	private String expressZone;
	
	@Column(name="access_expertise", columnDefinition = "varchar")
	private String expertiseAccess;
	
	@Column(name="particulier", columnDefinition = "varchar")
	private String particulier;
	
	@Column(name="professionel", columnDefinition = "varchar")
	private String professional;
	
	@Column(name="entreprise", columnDefinition = "varchar")
	private String entreprise;
	
	@Column(name="change", columnDefinition = "varchar")
	private String change;
	
	@Column(name="coffre_fort", columnDefinition = "varchar")
	private String coffreFort;
	
	@Column(name="automate_billet", columnDefinition = "varchar")
	private String automateBillet;
	
	@Column(name="automate_cheque", columnDefinition = "varchar")
	private String automateCheque;
	
	@Column(name="gab_vocale", columnDefinition = "varchar")
	private String gabVocal;
	
	@Column(name="imprimante_libre_service", columnDefinition = "varchar")
	private String imprLibreServ;
	
	@Column(name="espace_libre_service", columnDefinition = "varchar")
	private String espaceLibreService;
	
	@Column(name="annee_scrap", columnDefinition = "varchar")
	private String anneeScrap;
	
	public BNP_PRIV() {
		LocalDate dt=new Date().toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
		anneeScrap=String.valueOf(dt.getYear()+1);
	}

	public String getAgencyName() {
		return agencyName;
	}

	public BNP_PRIV setAgencyName(String agencyName) {
		this.agencyName = agencyName;
		return this;
	}

	public String getAgencyType() {
		return agencyType;
	}

	public BNP_PRIV setAgencyType(String agencyType) {
		this.agencyType = agencyType;
		return this;
	}

	public String getDep() {
		return dep;
	}

	public BNP_PRIV setDep(String dep) {
		this.dep = dep;
		return this;
	}

	public String getRegion() {
		return region;
	}

	public BNP_PRIV setRegion(String region) {
		this.region = region;
		return this;
	}

	public String getAdresse() {
		return adresse;
	}

	public BNP_PRIV setAdresse(String adresse) {
		this.adresse = adresse;
		return this;
	}

	public String getTel() {
		return tel;
	}

	public BNP_PRIV setTel(String tel) {
		this.tel = tel;
		return this;
	}

	public String getFax() {
		return fax;
	}

	public BNP_PRIV setFax(String fax) {
		this.fax = fax;
		return this;
	}

	public String getSvhedule() {
		return svhedule;
	}

	public BNP_PRIV setSvhedule(String svhedule) {
		this.svhedule = svhedule;
		return this;
	}

	public String getGab() {
		return gab;
	}

	public BNP_PRIV setGab(String gab) {
		this.gab = gab;
		return this;
	}

	public String getExpressZone() {
		return expressZone;
	}

	public BNP_PRIV setExpressZone(String expressZone) {
		this.expressZone = expressZone;
		return this;
	}

	public String getExpertiseAccess() {
		return expertiseAccess;
	}

	public BNP_PRIV setExpertiseAccess(String expertiseAccess) {
		this.expertiseAccess = expertiseAccess;
		return this;
	}

	public String getParticulier() {
		return particulier;
	}

	public BNP_PRIV setParticulier(String particulier) {
		this.particulier = particulier;
		return this;
	}

	public String getProfessional() {
		return professional;
	}

	public BNP_PRIV setProfessional(String professional) {
		this.professional = professional;
		return this;
	}

	public String getEntreprise() {
		return entreprise;
	}

	public BNP_PRIV setEntreprise(String entreprise) {
		this.entreprise = entreprise;
		return this;
	}

	public String getChange() {
		return change;
	}

	public BNP_PRIV setChange(String change) {
		this.change = change;
		return this;
	}

	public String getCoffreFort() {
		return coffreFort;
	}

	public BNP_PRIV setCoffreFort(String coffreFort) {
		this.coffreFort = coffreFort;
		return this;
	}

	public String getAutomateBillet() {
		return automateBillet;
	}

	public BNP_PRIV setAutomateBillet(String automateBillet) {
		this.automateBillet = automateBillet;
		return this;
	}

	public String getAutomateCheque() {
		return automateCheque;
	}

	public BNP_PRIV setAutomateCheque(String automateCheque) {
		this.automateCheque = automateCheque;
		return this;
	}

	public String getGabVocal() {
		return gabVocal;
	}

	public BNP_PRIV setGabVocal(String gabVocal) {
		this.gabVocal = gabVocal;
		return this;
	}

	public String getImprLibreServ() {
		return imprLibreServ;
	}

	public BNP_PRIV setImprLibreServ(String imprLibreServ) {
		this.imprLibreServ = imprLibreServ;
		return this;
	}

	public String getEspaceLibreService() {
		return espaceLibreService;
	}

	public BNP_PRIV setEspaceLibreService(String espaceLibreService) {
		this.espaceLibreService = espaceLibreService;
		return this;
	}
	
	

}
