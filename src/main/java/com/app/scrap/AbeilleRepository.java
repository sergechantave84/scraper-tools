package com.app.scrap;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.app.model.AbeilleAssurance;

@Repository
public interface AbeilleRepository extends JpaRepository<AbeilleAssurance, Integer>, RepositoryCustom<AbeilleAssurance>{

}
