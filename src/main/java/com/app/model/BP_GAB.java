package com.app.model;

import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "banque_bp_gab", schema = "scrap")
public class BP_GAB {
	@Id
	@SequenceGenerator(name = "scrap.banque_bp_gab_id_seq", sequenceName = "scrap.banque_bp_gab_id_seq", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "scrap.banque_bp_gab_id_seq")

	@Column(name = "id", updatable = false)
	private int id;

	@Column(name = "depsoumis", columnDefinition = "varchar")
	private String depSoumis;

	@Column(name = "total", columnDefinition = "varchar")
	private String total;

	@Column(name = "nbagences", columnDefinition = "varchar")
	private String nbAgences;

	@Column(name = "enseigne", columnDefinition = "varchar")
	private String enseigne;

	@Column(name = "nomweb", columnDefinition = "varchar")
	private String nomWeb;

	@Column(name = "adresse", columnDefinition = "varchar")
	private String adresse;

	@Column(name = "dab", columnDefinition = "varchar")
	private String dab;

	@Column(name = "hors_site", columnDefinition = "varchar")
	private String horsSite;

	@Column(name = "agencehtm", columnDefinition = "varchar")
	private String agenceHtm;

	@Column(name = "annee_du_scrap", columnDefinition = "varchar(4) DEFAULT date_part('year',CURRENT_DATE)")
	private String anneeScrap;

	public BP_GAB() {
		LocalDate ld= new Date().toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
		anneeScrap=String.valueOf(ld.getYear()+1);
	}

	public int getId() {
		return id;
	}

	public BP_GAB setId(int id) {
		this.id = id;
		return this;
	}

	public String getDepSoumis() {
		return depSoumis;
	}

	public BP_GAB setDepSoumis(String depSoumis) {
		this.depSoumis = depSoumis;
		return this;
	}

	public String getTotal() {
		return total;
	}

	public BP_GAB setTotal(String total) {
		this.total = total;
		return this;
	}

	public String getNbAgences() {
		return nbAgences;
	}

	public BP_GAB setNbAgences(String nbAgences) {
		this.nbAgences = nbAgences;
		return this;
	}

	public String getEnseigne() {
		return enseigne;
	}

	public BP_GAB setEnseigne(String enseigne) {
		this.enseigne = enseigne;
		return this;
	}

	public String getNomWeb() {
		return nomWeb;
	}

	public BP_GAB setNomWeb(String nomWeb) {
		this.nomWeb = nomWeb;
		return this;
	}

	public String getAdresse() {
		return adresse;
	}

	public BP_GAB setAdresse(String adresse) {
		this.adresse = adresse;
		return this;
	}

	public String getDab() {
		return dab;
	}

	public BP_GAB setDab(String dab) {
		this.dab = dab;
		return this;
	}

	public String getHorsSite() {
		return horsSite;
	}

	public BP_GAB setHorsSite(String horsSite) {
		this.horsSite = horsSite;
		return this;
	}

	public String getAgenceHtm() {
		return agenceHtm;
	}

	public BP_GAB setAgenceHtm(String agenceHtm) {
		this.agenceHtm = agenceHtm;
		return this;
	}

	public String getAnneeScrap() {
		return anneeScrap;
	}

	public BP_GAB setAnneeScrap(String anneeScrap) {
		this.anneeScrap = anneeScrap;
		return this;
	}

}
