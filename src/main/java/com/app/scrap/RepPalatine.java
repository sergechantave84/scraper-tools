package com.app.scrap;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


import com.app.model.Palatine;

@Repository
public interface RepPalatine extends JpaRepository<Palatine, Integer>,RepositoryCustom<Palatine> {

}
