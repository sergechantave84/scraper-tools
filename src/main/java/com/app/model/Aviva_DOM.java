package com.app.model;

import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="aviva", schema="scrap")
public class Aviva_DOM { 

	@Id
	@SequenceGenerator(name="scrap.aviva_dom_id_seq",
	                     sequenceName ="scrap.aviva_dom_id_seq",
	                     allocationSize=1)
    @GeneratedValue( strategy=GenerationType.SEQUENCE,
                     generator="scrap.aviva_dom_id_seq")
	  
	@Column(name="id",updatable=false)
	private int id;
	
	@Column(name="villesoumise", columnDefinition="varchar")
	private String villeSoumise;
	
	@Column(name="nbagences_ville", columnDefinition="varchar")
	private String nbAgencesVille;
	
	@Column(name="lien_agence", columnDefinition="varchar")
	private String lienAgence;
	
	@Column(name="type_site", columnDefinition="varchar")
	private String typeSite;
	
	@Column(name="nom_agence", columnDefinition="varchar")
	private String nomAgence;
	
	@Column(name="nom_agents", columnDefinition="varchar")
	private String nomAgents;
	
	@Column(name="adresse", columnDefinition="varchar")
	private String adresse;
	
	@Column(name="tel", columnDefinition="varchar")
	private String tel;
	
	@Column(name="fax", columnDefinition="varchar")
	private String fax;
	
	@Column(name="orias", columnDefinition="varchar")
	private String orias;
	
	@Column(name="horaires", columnDefinition="varchar")
	private String horaires;
	
	@Column(name="agencehtm", columnDefinition="varchar")
	private String agenceHTM;
	
	@Column(name="annee_du_scrap" ,columnDefinition="varchar(4) DEFAULT date_part('year',CURRENT_DATE)" )
	private String anneeScrap;
	
	public Aviva_DOM() {
		LocalDate ld= new Date().toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
		anneeScrap=String.valueOf(ld.getYear()+1);
	}
	
	public int getId() {
		return id;
	}
	public Aviva_DOM setId(int id) {
		this.id = id;
		return this;
	}
	public String getVilleSoumise() {
		return villeSoumise;
	}
	public Aviva_DOM setVilleSoumise(String villeSoumise) {
		this.villeSoumise = villeSoumise;
		return this;
	}
	public String getNbAgencesVille() {
		return nbAgencesVille;
	}
	public Aviva_DOM setNbAgencesVille(String nbAgencesVille) {
		this.nbAgencesVille = nbAgencesVille;
		return this;
	}
	public String getLienAgence() {
		return lienAgence;
	}
	public Aviva_DOM setLienAgence(String lienAgence) {
		this.lienAgence = lienAgence;
		return this;
	}
	public String getTypeSite() {
		return typeSite;
	}
	public Aviva_DOM setTypeSite(String typeSite) {
		this.typeSite = typeSite;
		return this;
	}
	public String getNomAgence() {
		return nomAgence;
	}
	public Aviva_DOM setNomAgence(String nomAgence) {
		this.nomAgence = nomAgence;
		return this;
	}
	public String getNomAgents() {
		return nomAgents;
	}
	public Aviva_DOM setNomAgents(String nomAgents) {
		this.nomAgents = nomAgents;
		return this;
	}
	public String getAdresse() {
		return adresse;
	}
	public Aviva_DOM setAdresse(String adresse) {
		this.adresse = adresse;
		return this;
	}
	public String getTel() {
		return tel;
	}
	public Aviva_DOM setTel(String tel) {
		this.tel = tel;
		return this;
	}
	public String getFax() {
		return fax;
	}
	public Aviva_DOM setFax(String fax) {
		this.fax = fax;
		return this;
	}
	public String getOrias() {
		return orias;
	}
	public Aviva_DOM setOrias(String orias) {
		this.orias = orias;
		return this;
	}
	public String getHoraires() {
		return horaires;
	}
	public Aviva_DOM setHoraires(String horaires) {
		this.horaires = horaires;
		return this;
	}
	public String getAgenceHTM() {
		return agenceHTM;
	}
	public Aviva_DOM setAgenceHTM(String agenceHTM) {
		this.agenceHTM = agenceHTM;
		return this;
	}
	public String getAnneeScrap() {
		return anneeScrap;
	}
	public Aviva_DOM setAnneeScrap(String anneeScrap) {
		this.anneeScrap = anneeScrap;
		return this;
	}
}
