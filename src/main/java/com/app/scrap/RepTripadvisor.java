package com.app.scrap;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.app.model.Tripadvisor;

@Repository
public interface RepTripadvisor extends JpaRepository<Tripadvisor, Integer>,RepositoryCustom<Tripadvisor> {

}
