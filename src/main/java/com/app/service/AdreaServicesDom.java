package com.app.service;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.net.SocketTimeoutException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.openqa.selenium.PageLoadStrategy;
import org.openqa.selenium.chrome.ChromeDriver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import com.app.model.Adrea;
import com.app.scrap.Rep;
import com.app.utils.BrowserUtils;
import com.app.utils.Parametres;
import com.app.utils.Utilitaires;



@Service
public class AdreaServicesDom extends Utilitaires{

	
	
	
	@Autowired
	private Rep repository;
	
	
		private List<Adrea> adrea= new ArrayList<>();
		String driverPath = "C:\\ChromeDriver\\";
		String nomSITE = "ADREA";
		String dossierRESU =Parametres.REPERTOIRE_RESULT + "\\ASSU\\" +nomSITE ;
		String url_accueil = "https://www.adrea.fr/nos-agences"; //https://www.ADREA.fr/nos-agences
		String fichier_valeurs; 
	    String DepSoumis;
		String ligne;
		
		int num_ligne = 2;
		int nbAgences;
		int NbAgences_trouvees;
		int numreponse = 1;	
		int nbRecuperes;
		int num_ligneficval;
		int parcoursficval;
		int Resultats ; 
	    
		ChromeDriver chromeDriver;
		BufferedWriter sortie;
		BufferedWriter sortie_log;	
	    Date Temps_Fin;
		
	    boolean b=false;
	   
    
	 public Adrea saveData(Adrea adrea) {
		 
		 return repository.save(adrea);
	 }
	 
	
	
	 public List<Adrea> saveDataAll(List<Adrea> adreas) {
		 
		 return repository.saveAll(adreas);
	 }
	 
	 
	 public  List<Adrea> getAdreaAll(){
		 return repository.findAll();
	 }
	 
	 public Adrea getAdreaById(int id) {
		 
		 return repository.findById(id).orElse(null);
	 }
	 
	 public String deleteProduct(int id) {
		 
		 try{
			 repository.deleteById(id);
			 
		 }catch(Exception e){
			 e.printStackTrace();
		 }
		 
		 return "removed";
		 
	 }
	 
	 
	 public Adrea updateProduct(Adrea adrea) {
		 Adrea existingData= repository.findById(adrea.getId()).orElse(null);
		 if(existingData!=null) {
			 existingData.setAdresse(adrea.getAdresse());
			 existingData.setNom_web(adrea.getNom_web());
			 existingData.setTel(adrea.getTel());
			 existingData.setHoraires(adrea.getHoraires());
			 existingData.setNb_agences(adrea.getNb_agences());
			 existingData.setAgences_HTM(adrea.getAgences_HTM());
			 return repository.save(existingData);
		 }else {
			 return null;
		 }
	 }
	 
	 public Adrea setData(String nom_web,String adresse, String horaires,String nb_agences,String agences_HTM,String tel) {
		 Adrea adrea= new Adrea();
		 adrea.setNom_web(nom_web).
		      setAdresse(adresse).
		      setTel(tel).
		      setHoraires(horaires).
		      setNb_agences(nb_agences).
		      setAgences_HTM(agences_HTM);
		 this.saveData(adrea)   ;
		 return adrea;
	 }
	 
	 public List<Adrea> showFirstRows() {
		 List<Adrea> a=repository.getFirstRow(100, Adrea.class);
		 return a;
	 }
	 
	 public List<Adrea> showLastRows(){
		 List<Adrea> a=repository.getLastRow(100, Adrea.class);
		 return a;
	 }
	
	    public void setUp() throws Exception 
	    {
	    	
			chromeDriver =BrowserUtils.chromeConstruct(driverPath, 60000,60000,60000, PageLoadStrategy.NORMAL);
		}
			
	    
	    
	    public List<Adrea> open_ADREA_byselenium() throws Exception 
	    {
	        File f= new File(dossierRESU);
	        Elements blocInfo=null;
	        if( !f.exists() )
	              f.mkdirs();
	        fichier = new File(dossierRESU + "\\"+ "RESULTATS_" + nomSITE +   "_" + System.currentTimeMillis() + ".txt");
	    	log = new File(dossierRESU + "\\"+ "LOG_" + nomSITE +  ".csv");
	    	String html="";
	    	Document doc=null;
	    	try 
	        {
	       	   chromeDriver.get(url_accueil);
	       	    System.out.println("on c'est connecté");
	        }
	    	catch(Exception e) 
	        {
	       	   if(e instanceof SocketTimeoutException) 
	       	  {
	       		 this.tearDown();
	       		 this.setUp();
	       		 chromeDriver.get(url_accueil);
	       		 
	       	  }
	       	}
	    	this.initialiser();
	    	b=this.listensIfPageFullyLoaded(chromeDriver);
	    	
	    	if(b) {
	    		Thread.sleep(20000);
	    		html=chromeDriver.getPageSource();
	        	doc=Jsoup.parse(html);
	        	//body > main > section > div > div > div.sidebar > div.result > ul
	        	blocInfo= doc.select("body > main > section > div > div > div.sidebar >div[class=\"result\"]> ul> li");
	        	System.out.println("ggg"+blocInfo);
	        	nbAgences=blocInfo.size();
	        	nbRecuperes=0;
	        	int i=0;
	        	for( Element tmp: blocInfo ) 
	        	{
	        		 String agencyName="",agencyAddress="",agencyPhone="",agencySchedule="",agencyHtm="";
	        		 
	        		 agencyHtm=tmp.outerHtml().replaceAll("\\s{1,}"," ").trim();
	        		 agencyHtm=this.normalize(agencyHtm);
	        		//fetch agency name
	        		 Element agencyElmentName= tmp.select("div[class=\"town\"]").first();
	        		 agencyName=(agencyElmentName!=null) ? agencyElmentName.text().replaceAll("\\s{1,}|\\d{1,}Km|\\(|\\)"," ").trim():"";
	        		 agencyName=this.normalize(agencyName);
	        		 System.out.println("le nom de l'agence est: "+ agencyName);
	        		
	        		//fetch agency address
	        		Element agencyElementAddress=tmp.select("address").first();
	        		agencyAddress=(agencyElementAddress!=null)? agencyElementAddress.text().replaceAll("\\s{1,}"," "):"";
	        		agencyAddress=this.normalize(agencyAddress);
	        		System.out.println("agence adresse: "+ agencyAddress);
	        		
	        		//fetch agency tel
	        		Element agencyElementPhone=tmp.select("div[class=\"tel\"]").first();
	        		agencyPhone= (agencyElementPhone!=null) ?agencyElementPhone.text().replaceAll("\\s{1,}"," "):"";
	        		agencyPhone=this.normalize(agencyPhone);
	        		System.out.println("agency phone: "+ agencyPhone);
	        		
	        		//fetch agency schedule
	        		Element agencyElementSchedule=tmp.select("div[class=\"schedule\"]").first();
	        		agencySchedule= (agencyElementSchedule!=null) ? agencyElementSchedule.text().replaceAll("\\s{1,}"," "):"";
	        		agencySchedule=this.normalize(agencySchedule);
	        		System.out.println("agency horaire: "+ agencySchedule);
	        		nbRecuperes++;
	        		
	        		
	        		this.enregistrer(agencyHtm,  agencyName,agencyAddress, agencyPhone, agencySchedule);
	        		this.enregistrer_journal(nbRecuperes);
	        		adrea.add(this.setData( agencyName,agencyAddress,agencySchedule,String.valueOf(this.nbAgences),agencyHtm, agencyPhone));
	        		
	        	}
	    	}
	    	
	    	return adrea;
	       
	    }
	    
	    
	    
	    public void enregistrer (
	    		String agenceHTM,
	            
	            String nomAgence,
	            String adresse,
	            String tel,
	            String horairesHTM)  {


	    	try {
	    	
	    		sortie = new BufferedWriter(new FileWriter(fichier,true));
	    		sortie.write(	this.nbAgences + "\t" 
	    						+ nomAgence + "\t" 
	    						+ adresse + "\t" 
	    						+ tel + "\t" 
	    						+ horairesHTM  + "\t"
	    						+ agenceHTM  + "\r\n");
	        
	    	
	    		sortie.close();
			} catch (IOException e) {
				
				e.printStackTrace();
			}
			
	}
	    
	    public void enregistrer_journal ( 
	             int numreponse)  {

//	    	this.fichier = new File(Parametres.REPERTOIRE + "RESULTATS_BDP.");
	    	try {
	    		Temps_Fin = new Date();
	    		SimpleDateFormat formater = null;
	    		formater = new SimpleDateFormat("'le' dd/MM/yyyy 'à' hh:mm:ss");
	    		String date =  formater.format(Temps_Fin);
	    		date =  formater.format(Temps_Fin);
	    		
	    		sortie_log = new BufferedWriter(new FileWriter(log,true));
	    		sortie_log.write(
	    						+ this.nbAgences + ";" 
	    						+ nbRecuperes + ";" 
	    						+ (this.nbAgences - nbRecuperes) + ";" 
	    						+ date  + "\r\n"   );
	    		sortie_log.close();
	    		
			} catch (IOException e) {
				
				e.printStackTrace();
			}
			
	}
	    
	    /**
	     * La méthode <code>initialiser()</code> permet d'ajouter la ligne d'entête dans le fichier résultat.
	     */

	   
	    public void initialiser() {
	        
	        try {
	        	//Attention pas d'accent dans les noms de colonnes !
	        	sortie = new BufferedWriter(new FileWriter(fichier,true));
	        	sortie.write("NbAgences" + "\t" + "NOMWEB"+ "\t" + "ADRESSE"+ "\t"  + "TEL"+ "\t" + "HORAIRESHTM"+ "\t"  
	        			 + "AGENCES_HTM"+ "\r\n");
	        	sortie.close();
	        	
	        	File toto = new File(dossierRESU + "\\"+ "LOG_" + nomSITE + ".csv"); 
	            if( !toto.exists() ){
	        	//if (num_ligneficval == num_ligne) {
		        	sortie_log = new BufferedWriter(new FileWriter(log,true));
		        	sortie_log.write("NbAgences" + ";"  + "Nbrecuperes" + ";" + "Reste" + ";" + "Heure_Fin"+ "\r\n" );
		        	sortie_log.close();
	        	}
	        	
			} catch (IOException e) {
				
				e.printStackTrace();
			}
	    }
	   
	    public void tearDown() {
	        chromeDriver.quit();
	        
	    }
	    
	   
	    



}
