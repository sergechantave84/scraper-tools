package com.app.service;

import java.io.BufferedInputStream;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;

import java.net.SocketTimeoutException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Scanner;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.PageLoadStrategy;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.app.model.CE_Outre_Mer;
import com.app.scrap.RepCeOutreMer;

import com.app.utils.Parametres;
import com.app.utils.Utilitaires;

import fa.browserUtils.ChromeBrowser;
import fa.browserUtils.FactoryBrowser;
import fa.log.LogApp.LogScrap;
import fa.log.config.LogConst;

@Service
public class ServiceCE_Outre_Mer extends Scraper {

	@Autowired
	private RepCeOutreMer rep;

	String driverPath = "C:\\ChromeDriver\\";
	private List<CE_Outre_Mer> ceList = new ArrayList<>();

	String nomSITE = "CE_Outre_Mer";
	String ficval = "liens_ce_outre_mer";
	String dossierRESU = Parametres.REPERTOIRE_RESULT + "\\BQ\\" + nomSITE;;
	String url_accueil = "https://www.agences.caisse-epargne.fr";
	String fichier_valeurs;
	String cp;
	String VilleSoumise;
	String ligne;

	int num_ligne = 2;
	int NbAgences;
	int numreponse = 1;
	int DAB = 1;
	int HORS_SITE;
	int nbRecuperes;
	int num_ligneficval;
	int parcoursficval;
	int Resultats;

	ChromeDriver chromeDriver;
	BufferedWriter sortie;
	BufferedWriter sortie_log;

	Date Temps_Fin;

	
	/*public void setData(String depSoumis, String nbAgences, String nomWeb, String adresse, String tel,
			String fax, String horairesHtm, String horairestxt, String dab, String agenceHtm) {
		CE_Outre_Mer ce = new CE_Outre_Mer();
		ce.setDepSoumis(depSoumis).setNbAgences(nbAgences).setNomWeb(nomWeb).setAdresse(adresse).setTel(tel).setFax(fax)
				.setHorairesHtm(horairesHtm).setHorairestxt(horairestxt).setDab(dab).setAgenceHtm(agenceHtm);
		
		
	}*/
	
	@Override
	public void saveItem(String... args) {
		CE_Outre_Mer ce = new CE_Outre_Mer();
		ce.setDepSoumis(args[0]).setNbAgences(args[1]).setNomWeb(args[2]).setAdresse(args[3]).setTel(args[4]).setFax(args[5])
				.setHorairesHtm(args[6]).setHorairestxt(args[7]).setDab(args[8]).setServices(args[9]).setAgenceHtm(args[10]);
		rep.save(ce);
	}

	
	public void setup() {
		
		FactoryBrowser factBrowser = new FactoryBrowser();
		ChromeBrowser b = (ChromeBrowser) factBrowser.create("chrome");

		// final String binaryPath = "C:\\Program Files\\Google\\Chrome
		// Beta\\Application\\chrome.exe";
		final String binaryPath = "C:\\Program Files\\Google\\Chrome Beta\\Application\\chrome.exe";
		File[] f = { new File("ext/scrap_utils_ext.crx") };
		chromeDriver = b.setDataUserPath("").setExtensionPath(f)
				.setOptions(PageLoadStrategy.NORMAL, driverPath, 60000, 60000, 60000, false, true, binaryPath)
				.initBrowserDriver();
		
		//chromeDriver = (ChromeDriver) this.setupCh(driverPath, false, true, "", "ext/scrap_utils_ext.crx", "");
	}

	@Override
	public void scrap() throws IOException, InterruptedException {
		
		
		List<String> lienAgences=new ArrayList<>();
		fichier_valeurs = Parametres.REPERTOIRE_DATA_SOURCES + ficval + ".txt";
		Scanner sc = new Scanner(new BufferedInputStream(new FileInputStream(new File(fichier_valeurs))));

		LogScrap log = new LogScrap("BanqueCE_OUTRE_MER.txt", LogConst.TYPE_LOG_FILE);
		num_ligneficval = ((new File(LogConst.STORE_LOG_PATH + LogConst.PREFIX_LOG_FILE + "_BanqueCE_OUTRE_MER.txt"))
				.exists()) ? Integer.parseInt(log.readLog().getLastLineProcessed().split("\\t")[4]) : 1;

		parcoursficval = 1;
		while (sc.hasNextLine()) {
			
			String[] lines = sc.nextLine().split("\\t");
			if (parcoursficval > num_ligneficval) {
				cp = lines[0];
				String finalUrl=url_accueil+lines[1];
				System.out.println(finalUrl);
				try {
					chromeDriver.get(finalUrl);

				} catch (Exception e) {
					if (e instanceof SocketTimeoutException) {
						chromeDriver.get(finalUrl);

					}
				}
				Thread.sleep(3000);
				String pageSource = chromeDriver.getPageSource();
				Document doc = Jsoup.parse(pageSource);
				Elements listResults = doc.select("body > div.em-page > main > div.em-results-wrapper > div > div.em-results__list-wrapper > div > div > div.jspPane > ul>li");
				
				if (listResults != null) {
					NbAgences = listResults.size();
					if (!lienAgences.isEmpty())
						lienAgences.clear();
					for (Element listResult : listResults) {
						Element href = listResult.selectFirst("li> h2 > a.em-poi-card__link");
						if (href != null) {
							System.out.println(href.attr("href"));
							lienAgences.add(href.attr("href"));
						}
					}
					System.out.println("NbAgences : " + NbAgences);
				}

				for (String lienAgence : lienAgences) {

					String agenceHtm = "";
					String nomWeb = "";
					String adresse = "";
					String telephone = "";
					String fax = "";
					String horaires = "";
					String horairesHTM = "";
					String services = "";
					String dab = "0";

					url_accueil = "https://www.agences.caisse-epargne.fr" + lienAgence;
					//url_accueil = lienAgence;
					System.out.println(url_accueil);
					try {
						chromeDriver.get(url_accueil);
					} catch (Exception e) {
						if (e instanceof SocketTimeoutException) {
							chromeDriver.get(url_accueil);
						}
					}

					Thread.sleep(3000);
					pageSource = chromeDriver.getPageSource();
					agenceHtm = pageSource.replaceAll("\\s{1,}", " ");
					//System.out.println(agenceHtm);
					doc = Jsoup.parse(pageSource);

					Element nomAgenceJSEelement = doc.selectFirst("#titre_faniry");
					nomWeb = (nomAgenceJSEelement != null) ? nomAgenceJSEelement.text() : "";
					System.out.println("nom : " + nomWeb);

					Element adresseJSElement = doc.selectFirst("#adresse_faniry");
					adresse = (adresseJSElement != null) ? adresseJSElement.text() : "";
					System.out.println("adresse : " + adresse);

					Element telJSElement = doc.selectFirst("#tel_faniry");
					telephone = (telJSElement != null) ? telJSElement.attr("href").replaceAll("[^0-9 ]", "") : "";
					System.out.println("tel " + telephone);

					Element faxJSElement = doc.selectFirst("#fax_faniry");
					fax = (faxJSElement != null) ? faxJSElement.text().replaceAll("[^0-9]", " ") : "";
					System.out.println("fax " + fax);

					Elements horaireJSElements = doc.select("#horaire_faniry >div");
					if (horaireJSElements.size() > 0) {
						for (Element horaireJSElement : horaireJSElements) {
							horaires += horaireJSElement.text() + " ";
						}
						horairesHTM = doc.selectFirst("body > div.em-page > " + "main > div.em-details__horaires-bloc")
								.html().replaceAll("\\s{1,}", " ");
					} else {
						Element horaireJSElement = doc.selectFirst("#no_horaire_faniry");
						if (horaireJSElement != null) {
							horaires = horaireJSElement.text();
							horairesHTM = doc
									.selectFirst("body > " + "div.em-page > " + "main > "
											+ "div.em-details__horaires-text-info > " + "p")
									.html().replaceAll("\\s{1,}", " ");
						}
					}
					//System.out.println(horairesHTM);
					System.out.println("horaire " + horaires);

					Element serviceJSElement = doc.selectFirst("#dan_faniry");
					if (serviceJSElement != null) {
						services = serviceJSElement.text();
						dab = serviceJSElement.attr("data-dab-statut");
					} else
						dab = "0";
					System.out.println("dab " + dab);
					System.out.println("service " + services);

					/*ce.setDepSoumis(args[0]).setNbAgences(args[1]).setNomWeb(args[2]).setAdresse(args[3]).setTel(args[4]).setFax(args[5])
					.setHorairesHtm(args[6]).setHorairestxt(args[7]).setDab(args[8]).setServices(args[9]).setAgenceHtm(args[10]);*/
				   this.saveItem(cp, String.valueOf(NbAgences), nomWeb, adresse, telephone, fax,
							horairesHTM, horaires, dab,  services, agenceHtm);
					nbRecuperes++;
					System.out.println("\n------------------------------------\n");

					
				}

				log.writeLog(new Date(), cp, "no_error", NbAgences, parcoursficval, (NbAgences - nbRecuperes));
			}

			parcoursficval++;
		}
		sc.close();

	}

	public void tearDown() {
		chromeDriver.quit();

	}

	

	
}
