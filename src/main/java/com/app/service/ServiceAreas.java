package com.app.service;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;

import java.io.InputStreamReader;
import java.net.SocketTimeoutException;
import java.net.URL;

import java.util.Date;

import java.util.Scanner;

import javax.net.ssl.HttpsURLConnection;

import org.apache.commons.text.StringEscapeUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.openqa.selenium.JavascriptExecutor;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.app.model.Areas;
import com.app.scrap.RepArea;

import com.app.utils.Parametres;

import fa.log.LogApp.LogScrap;
import fa.log.config.LogConst;

@Service
public class ServiceAreas extends Scraper {

	@Autowired
	private RepArea rep;

	String driverPath = "C:\\ChromeDriver\\";
	String dataSourceFileName = "Areas_dep.txt";
	String nomSITE = "AREAS";
	String dossierRESU = Parametres.REPERTOIRE_RESULT + "\\ASSU\\" + nomSITE;
	String url_accueil = "https://www.areas.fr/trouver-une-agence";
	String ligne;

	int nb_pages = 48;
	int numreponse = 1;
	int num_page = 0;
	int NbAgences_page;
	int nbRecuperes;
	int parcoursficval;
	int Resultats;

	ChromeDriver chromeDriver = null;

	BufferedWriter sortie;
	BufferedWriter sortie_log;
	Date Temps_Fin;

	public void setData(String nbPages, String numPage, String nbAgencePage, String lienAgence, String nomAgence,
			String adresse, String tel, String orias, String horaires, String agenceHAreasM) {
		Areas areas = new Areas();
		areas.setNbPages(nbPages).setNumPage(numPage).setNbAgencePage(nbAgencePage).setLienAgence(lienAgence)
				.setNomAgence(nomAgence).setAdresse(adresse).setTel(tel).setOrias(orias).setHoraires(horaires)
				.setAgenceHTM(agenceHAreasM);
		rep.save(areas);

	}

	public void setup() throws Exception {
		
		final String chromeBeta = "C:\\Program Files\\Google\\Chrome Beta\\Application\\chrome.exe";
		
		/*ch = (ChromeDriver) this.setupCh(Parametres.CHROME_DRIVER_PATH, true, false,
				"C:\\Users\\GEOMADA PC4\\AppData\\Local\\Google\\Chrome Beta\\User Data", "",chromeBeta);*/

		this.chromeDriver = (ChromeDriver) this.setupCh(Parametres.CHROME_DRIVER_PATH, false, false, "","", chromeBeta);
	}

	public void scrap() throws Exception {

		Scanner sc = new Scanner(new BufferedInputStream(
				new FileInputStream(new File(Parametres.REPERTOIRE_DATA_SOURCES + dataSourceFileName))));

		LogScrap log = new LogScrap("AssuranceAreasLog.txt", LogConst.TYPE_LOG_FILE);
		int currentLine = (new File(LogConst.STORE_LOG_PATH + LogConst.PREFIX_LOG_FILE + "_AssuranceAreasLog.txt"))
				.exists() ? currentLine = Integer.parseInt(log.readLog().getLastLineProcessed().split("\t")[4]) : 1;

		parcoursficval = 1;
		JavascriptExecutor jsExecutor = null;
		String jsScript = "", html = "";
		Document doc = null;

		while (sc.hasNextLine()) {
			String line = sc.nextLine();

			if (parcoursficval > currentLine) {

				try {
					this.chromeDriver.get(url_accueil);
				} catch (Exception e) {
					if (e instanceof SocketTimeoutException) {
						chromeDriver.get(url_accueil);
					}
				}
				html = "";
				nbRecuperes = 0;
				WebElement input = this.chromeDriver.findElementByCssSelector("input[name=\"ASA_champAdresse\"]");
				jsScript = String.format("arguments[0].value='%s'", line);
				jsExecutor = (JavascriptExecutor) this.chromeDriver;
				jsExecutor.executeScript(jsScript, input);

				WebElement btnOk = chromeDriver.findElementByCssSelector("input[value=\"OK\"]");
				jsScript = String.format("arguments[0].click()", "");
				jsExecutor = (JavascriptExecutor) chromeDriver;
				jsExecutor.executeScript(jsScript, btnOk);

				// dReadyState=this.listensIfPageFullyLoaded(chromeDriver);
				// if(dReadyState) {
				Thread.sleep(3000);
				html = chromeDriver.getPageSource();
				doc = Jsoup.parse(html);

				Elements agenciesList = doc.select("div[id=\"carte_google_resultats\"] > div[class=\"node_adresse\"]");
				NbAgences_page = agenciesList.size();
				num_page++;

				for (Element elmntTmp : agenciesList) {
					nbRecuperes++;
					String ORIAS = "", nom_agence = "", adresse = "", tel = "", horaires = "", agence = "";
					String href = elmntTmp.attr("attr-l");
					
					String url = "https://www.areas.fr" + href;
					URL urlC = new URL(url);
					
					System.out.println(urlC);
					
					HttpsURLConnection con = (HttpsURLConnection) urlC.openConnection();
					con.setRequestMethod("GET");
					con.setReadTimeout(600000);
					con.setConnectTimeout(60000);
					BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream(), "UTF-8"));
					String pageSource;
					StringBuilder strB = new StringBuilder();
					while ((pageSource = in.readLine()) != null) {
						strB.append(pageSource);
					}
					html = StringEscapeUtils.unescapeHtml4(strB.toString());
					doc = Jsoup.parse(html);
					Element bigBlockElement = doc.select("#colonne").first();

					if (bigBlockElement != null) {
						agence = bigBlockElement.outerHtml().replaceAll("\\s{1,}", " ").trim();
						agence = this.normalize(agence);
						Element bigBlockForCoordinate = bigBlockElement.select("div:nth-child(2)>.bandeauAgent")
								.first();

						if (bigBlockForCoordinate != null) {
							// fetch agency name
							Element agencyName = bigBlockElement.select(".zoneInfo>h1.h1-titre").first();
							nom_agence = (agencyName != null) ? agencyName.text() : "";
							nom_agence = nom_agence.replaceAll("Votre agence Aréas Assurances -", "");
							nom_agence = nom_agence.trim();
							nom_agence = this.normalize(nom_agence);
							System.out.println("nom agence : " + nom_agence);

							// fetch agency address and fetch agency tel

							Element addresseElement = bigBlockForCoordinate.select(".zoneInfo>.h2-accroche").first();
							adresse = (addresseElement != null) ? addresseElement.text() : "";
							adresse = adresse.replaceAll("\\s{2,}", "");
							adresse = this.normalize(adresse);
							Element telElement = bigBlockForCoordinate
									.selectFirst(".zoneInfo>.contact>a.btn_telFixe.p3-bouton");
							tel = (telElement != null) ? telElement.attr("href") : "";
							tel = tel.replaceAll("[a-zA-Z:]", "");
							tel = tel.replaceAll("\\.", " ");
							tel = this.normalize(tel);
							System.out.println("telephone agence : " + tel);
							System.out.println("adresse agence : " + adresse);

							// fetch agency orias
							Elements agencyOriasElements = bigBlockForCoordinate
									.select(".zoneAgent>.agent>.agentNumeroOrias");
							if (agencyOriasElements != null) {
								ORIAS = "";
								for (Element agencyOriasElement : agencyOriasElements) {
									ORIAS += agencyOriasElement.text() + " ";
								}
							}
							ORIAS = ORIAS.replaceAll("[^0-9 ]", "").trim();
							System.out.println("l'orias de l'agence est : " + ORIAS);
						}
						ORIAS = this.normalize(ORIAS);
						// fetch agency horaires
						Elements scheduleElements = bigBlockElement.select("div:nth-child(7) > div");
						horaires = (scheduleElements != null) ? scheduleElements.text().replaceAll("\\s{1,}", " ") : "";

						horaires = horaires.toLowerCase();
						horaires = horaires.replaceAll("ouvert jusqu'à", "");
						horaires = horaires.replaceAll("puis de", "");
						horaires = horaires.replaceAll("Fermé, ouvre de", "");
						horaires = this.normalize(horaires);
						System.out.println("les horaire est : " + horaires);
						System.out.println("-------------------------------------\n");

						this.setData(String.valueOf(nb_pages), String.valueOf(num_page), String.valueOf(NbAgences_page),
								url, nom_agence, adresse, tel, ORIAS, horaires, agence);
					}

				}
				// }
				log.writeLog(new Date(), line, "no_error", NbAgences_page, parcoursficval,
						(NbAgences_page - nbRecuperes));
			}
			parcoursficval++;
		}

	}

	public void tearDown() {
		chromeDriver.quit();

	}

	@Override
	public void saveItem(String... args) {
		// TODO Auto-generated method stub

	}

}
