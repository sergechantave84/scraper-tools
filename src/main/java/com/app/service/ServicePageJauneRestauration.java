package com.app.service;

import java.io.BufferedInputStream;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.net.URLEncoder;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.Normalizer;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Stream;
import org.json.JSONException;
import org.json.JSONObject;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.openqa.selenium.PageLoadStrategy;
import org.openqa.selenium.chrome.ChromeDriver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.app.model.PJRestauration;
import com.app.scrap.RepPJRestauration;
import com.app.utils.Parametres;

import fa.browserUtils.ChromeBrowser;
import fa.browserUtils.FactoryBrowser;
import fa.log.LogApp.LogScrap;
import fa.log.config.LogConst;

@Service
public class ServicePageJauneRestauration extends Scraper implements SuperClassPJ {

	ChromeDriver ch;
	int nombreParPage = 0;
	static String driverPath = "C:\\ChromeDriver\\";

	@Autowired
	RepPJRestauration rep;

	@Override
	public void saveItem(String... args) {
		PJRestauration pjRest = new PJRestauration();
		pjRest.setActivite_soumis(args[0]).setCommune(args[1]).setCp(args[2]).setNb_res(Integer.parseInt(args[3]))
				.setNb_page(Integer.parseInt(args[4])).setNb_par_page(Integer.parseInt(args[5])).setActivite(args[6])
				.setTel(args[7]).setDenomination(args[8]).setAdresse(args[9]).setPrestation(args[10])
				.setTypeCuisine(args[11]).setBudget(args[12]).setModePaiement(args[13]).setSite(args[14])
				.setPasse_sanitaire(args[15]).setSiret(args[16]).setSiren(args[17]).setAmbiance(args[18]);

		//rep.save(pjRest);
	}

	public void setup() {
		/*//final String binaryPath="C:\\Program Files\\Google\\Chrome\\Application\\chrome.exe";
		final String binaryPathBeta="C:\\Program Files\\Google\\Chrome Beta\\Application\\chrome.exe";
		//final String dataDirChrome="C:\\Users\\GEOMADA PC5\\AppData\\Local\\Google\\Chrome\\User Data";
		final String dataDirChromeBeta="C:\\Users\\GEOMADA PC5\\AppData\\Local\\Google\\Chrome Beta\\User Data";
		ch = (ChromeDriver) this.setupCh(Parametres.CHROME_DRIVER_PATH, true, false,dataDirChromeBeta, "",binaryPathBeta);
		*/
		
		
		final String chromeBeta = "C:\\Program Files\\Google\\Chrome Beta\\Application\\chrome.exe";
		ch = (ChromeDriver) this.setupCh(Parametres.CHROME_DRIVER_PATH, true, false,"C:\\Users\\GEOMADA PC4\\AppData\\Local\\Google\\Chrome Beta\\User Data", "", chromeBeta);
	}

	@Override
	public void scrap() throws Exception {
		String url = "https://www.pagesjaunes.fr/annuaire/chercherlespros?quoiqui=";
		String ficVal = "Liste_CP_rubriques_20220920.txt";
		Scanner sc = new Scanner(
				new BufferedInputStream(new FileInputStream(new File(Parametres.REPERTOIRE_DATA_SOURCES + ficVal))));
		LogScrap log = new LogScrap("PJRestauration.txt", LogConst.TYPE_LOG_FILE);
		int lastLine = (new File(LogConst.STORE_LOG_PATH + LogConst.PREFIX_LOG_FILE + "_PJRestauration.txt").exists())
				? Integer.parseInt(log.readLog().getLastLineProcessed().split("\t")[4])
				: 1;
		int currentLine = 1;

		while (sc.hasNextLine()) {
			String[] lines = sc.nextLine().split("\\t");
			if (currentLine > lastLine) {
				int b = 0;
				String fileNameStore = String.format("PJRestaurationStore/linkStore_%s_%s_%s.txt", lines[1], lines[0],
						lines[2]);
				System.out.println(fileNameStore);
				if (new File(fileNameStore).exists()) {
					Scanner sc1 = new Scanner(new BufferedInputStream(new FileInputStream(new File(fileNameStore))));
					int j = 1;
					while (sc1.hasNextLine()) {
						String l = sc1.nextLine().split(",")[5];
						if (j > 1)
							b = Integer.parseInt(l);
						j++;
					}
				}
				String commune = lines[0];
				String cp = lines[1];
				String activiteSoumis = lines[2];
				String finalURL = url + activiteSoumis + "&ou=" + URLEncoder.encode(commune + "(" + cp + ")", "UTF-8")
						+ "&univers=pagesjaunes&idOu=";
				ch.get(finalURL);
				String pageSource = Normalizer.normalize(ch.getPageSource().toLowerCase(), Normalizer.Form.NFD)
						.replaceAll("[^\\p{ASCII}]", "");
				if (this.noResponse(pageSource)) {
					finalURL = "https://www.pagesjaunes.fr/annuaire/chercherlespros?quoiqui=" + activiteSoumis + "&ou="
							+ commune + "&univers=pagesjaunes&idOu=";
					ch.get(finalURL);
					pageSource = Normalizer.normalize(ch.getPageSource().toLowerCase(), Normalizer.Form.NFD)
							.replaceAll("[^\\p{ASCII}]", "");
				}
				pageSource = this.iscaptcha(pageSource, ch);
				pageSource = Normalizer.normalize(ch.getPageSource().toLowerCase(), Normalizer.Form.NFD)
						.replaceAll("[^\\p{ASCII}]", "");
				int stat = 0;
				if (!this.noResponse(pageSource)) {
					Document doc = Jsoup.parse(pageSource);
					int pageNumber = this.retrievePageNumber(doc);
					if (b == 0) {
						if (!new File(fileNameStore).exists())
							this.intFile("PJRestaurationStore/",
									"linkStore_" + lines[1] + "_" + lines[0] + "_" + lines[2] + ".txt");

						int i = 2;
						
						activiteSoumis = Normalizer.normalize(activiteSoumis, Normalizer.Form.NFD).replaceAll("[^\\p{ASCII}]", "");
						String depSoumise=cp+" "+((commune.trim()).split("\\s")[0]).toLowerCase();
						if (pageNumber > 1) {
							
							this.retrieveLinks(doc, activiteSoumis, depSoumise, stat, fileNameStore,lines[1]+" "+lines[0].toLowerCase());
							while (i <= Integer.valueOf(pageNumber)) {

								Thread.sleep(3000);
								if (i <= pageNumber)
									ch.findElementById("pagination-next").click();

								if (i == Integer.valueOf(pageNumber))
									stat = 1;
								
								Thread.sleep(3000);
								pageSource = Normalizer.normalize(ch.getPageSource().toLowerCase(), Normalizer.Form.NFD)
										.replaceAll("[^\\p{ASCII}]", "");
								pageSource = this.iscaptcha(pageSource, ch);
								doc = Jsoup.parse(pageSource);
								this.retrieveLinks(doc, activiteSoumis, depSoumise, stat, fileNameStore,lines[1]+" "+lines[0].toLowerCase());
								pageNumber = this.retrievePageNumber(doc);

								System.out.println("num page " + i + " re-page number " + pageNumber);
								i++;
							}
						} else {
							
							stat = 1;
							
							this.retrieveLinks(doc, activiteSoumis,depSoumise, stat, fileNameStore,lines[1]+" "+lines[0].toLowerCase());
						}

					}
					// make scrap

					String logStr = "_PJRestaurationLog2" + lines[1] + "_" + lines[0] + "_" + lines[2] + ".txt";

					LogScrap log2 = new LogScrap(
							"PJRestaurationLog2" + lines[1] + "_" + lines[0] + "_" + lines[2] + ".txt",
							LogConst.TYPE_LOG_FILE);
					int secondLastLine = 1;
					if ((new File(LogConst.STORE_LOG_PATH + LogConst.PREFIX_LOG_FILE + logStr)).exists()) {
						Scanner sc3 = new Scanner(new BufferedInputStream(new FileInputStream(
								new File(LogConst.STORE_LOG_PATH + LogConst.PREFIX_LOG_FILE + logStr))));
						while (sc3.hasNextLine()) {
							secondLastLine = Integer.parseInt(sc3.nextLine().split("\t")[4]);
						}
					}
					System.out.println(secondLastLine);
					int secondCurrentLine = 1;
					Scanner sc1 = new Scanner(new BufferedInputStream(new FileInputStream(new File(fileNameStore))));

					Stream<String> fileStream = Files.lines(Paths.get(fileNameStore));
					int nombreResultat = ((int) fileStream.count()) - 1;

					while (sc1.hasNextLine()) {
						String secondLines[] = sc1.nextLine().split(",");

						if (secondCurrentLine > secondLastLine) {
							String finalUrl = "";
							if (secondLines[2].contains("/pros/")) {
								finalUrl = "https://www.pagesjaunes.fr" + secondLines[2];
							} else {
								finalUrl = "https://www.pagesjaunes.fr/pros/detail?bloc_id="
										+ secondLines[2].toUpperCase() + "&no_sequence=0&code_rubrique="
										+ secondLines[3];
							}
							Pattern p = Pattern.compile("[a-zA-Z]{1,}");
							Matcher match = p.matcher(secondLines[2]);

							if (!match.find()) {
								finalUrl = "https://www.pagesjaunes.fr/pros/detail?code_etablissement=" + secondLines[2]
										+ "&code_localite=" + secondLines[4] + "&code_rubrique=" + secondLines[3];
							}
							System.out.println(finalUrl);
							ch.get(finalUrl);
							Thread.sleep(2000);
							pageSource = Normalizer.normalize(ch.getPageSource().toLowerCase(), Normalizer.Form.NFD)
									.replaceAll("[^\\p{ASCII}]", "");

							pageSource = this.iscaptcha(pageSource, ch);

							if (!this.noResponse(pageSource)) {
								doc = Jsoup.parse(pageSource);
								this.retrieveData(doc, cp, activiteSoumis, commune, nombreResultat, pageNumber,
										Integer.parseInt(secondLines[6]));
							}
							log2.writeLog(new Date(), secondLines[0], "no_error", 0, secondCurrentLine, 0);
						}
						secondCurrentLine++;
					}
				}
				// write log
				log.writeLog(new Date(), lines[2] + " " + lines[0], "no_error", 0, currentLine, 0);
			}
			currentLine++;
		}

	}

	private void retrieveData(Document doc, String cp, String activiteSoumis, String commune, int nombreResultat,
			int pageNumber, int nombreResultatParPage) {

		Element agencyNameElement = doc.selectFirst("#teaser-header > div.row > div > div > div.denom > h1");
		String nomAgence = (agencyNameElement != null) ? agencyNameElement.text() : "";
		nomAgence=nomAgence.replaceAll("[^a-z0-9 ]"," ").toUpperCase();
		System.out.println("nom : " + nomAgence);

		Elements adresseElements = doc.select(
				"#teaser-footer > div > div > div.address-container.marg-btm-s > a.teaser-item.black-icon.address.streetaddress.clearfix.map-click-zone.pj-lb.pj-link > span");
		String adresse = "";
		if (!adresseElements.isEmpty()) {
			for (Element adresseElement : adresseElements)
				adresse += adresseElement.text() + " ";
		}
        adresse=adresse.replaceAll("[^a-z0-9 ]"," ").toUpperCase();
		System.out.println("adresse : " + adresse);

		Element telElement = doc
				.selectFirst("#teaser-footer > div > div > div.zone-fantomas > div > span > span.coord-numero.noTrad");
		String tel = (telElement != null) ? telElement.text() : "";
		System.out.println("tel : " + tel);

		Elements prestationsElements = doc.select(
				"#zone-info > div.zone-produits-presta-services-marques.fd-bloc > div.ligne.prestations.marg-btm-m > ul > li");
		String prestations = "";
		if (!prestationsElements.isEmpty()) {
			for (Element prestationElement : prestationsElements) {
				prestations += prestationElement.text() + " ";
			}
		} else {
			prestationsElements = doc.select("#zone-info > div.zone-produits-presta-services-marques.fd-bloc > "
					+ "div.ligne.cris > div.cri-prestations > ul > li");
			if (!prestationsElements.isEmpty())
				for (Element prestationElement : prestationsElements) {
					prestations += prestationElement.text() + " ";
				}
			prestationsElements = doc
					.select("#zone-info > div.zone-produits-presta-services-marques.fd-bloc > div.ligne.cris > "
							+ "div.cri-produits--prestations-et-services > ul > li");
			if (!prestationsElements.isEmpty())
				for (Element prestationElement : prestationsElements) {
					prestations += prestationElement.text() + " ";
				}
		}
		System.out.println("prestations : " + prestations);

		Elements activitiesElements = doc.select("#zonemultiactivite > div > ul >li ");
		String activite = "";
		if (!activitiesElements.isEmpty()) {
			for (Element activityElement : activitiesElements) {
				activite += activityElement.text() + " ";
			}
		}
		System.out.println("activite : " + activite);

		String siret = "";
		Element blocEtabElement = doc.selectFirst("#zoneb2b > dl.info-etablissement.marg-btm-s.zone-b2b.txt_sm");
		if (blocEtabElement != null) {
			Element siretElement = blocEtabElement.selectFirst("dd:nth-child(2)");
			siret = (siretElement.text().matches("[0-9]{14,}")) ? siretElement.text() : "";
		}
		System.out.println("siret : " + siret);
		String siren = "";
		Element blocEntreprise = doc.selectFirst("#zoneb2b > dl.info-entreprise.marg-btm-s.zone-b2b.txt_sm");
		if (blocEntreprise != null) {
			Element sirenElement = blocEntreprise.selectFirst(" dd:nth-child(2) > strong");
			siren = (sirenElement.text().matches("[0-9]{6,}")) ? sirenElement.text() : "";
		}
		System.out.println("siren : " + siren);

		String typeCuisine = "";
		Elements cuisineElements = doc
				.select("#teaser-description > div.bloc-infos-teaser.marg-btm-m.row > div.bloc-info-cuisine > ul> li");
		if (!cuisineElements.isEmpty())
			for (Element cuisineElement : cuisineElements) {
				typeCuisine += Normalizer.normalize(cuisineElement.text(), Normalizer.Form.NFKD)
						.replaceAll("[^\\p{ASCII}]", "") + " ";
			}

		System.out.println("type cuisine : " + typeCuisine);

		Elements budgetElements = doc.select("#tarif-generique > ul >li");
		String budget = "";
		if (!budgetElements.isEmpty()) {
			for (Element budgetElement : budgetElements) {
				budget += budgetElement.text().replaceAll("[^0-9-]", "") + " ";
			}
			budget += "euros";
		}
		System.out.println("budget : " + budget);

		Elements ambianceElements = doc
				.select("#teaser-description > div.bloc-infos-teaser.marg-btm-m.row > div.bloc-info-ambiance > ul> li");
		String ambiance = "";
		if (!ambianceElements.isEmpty())
			for (Element ambianceElement : ambianceElements) {
				ambiance += ambianceElement.text() + " ";
			}
		System.out.println("ambiance : " + ambiance);

		Element siteWebElement = doc
				.selectFirst("#teaser-footer > div > div > div.lvs-container.marg-btm-s > a > span.value");
		String siteWeb = (siteWebElement != null) ? siteWebElement.text() : "";
		System.out.println("site web : " + siteWeb);

		Element passeSanitaireElement = doc.selectFirst("#zone-informations-pratiques");
		int passeSanitaire = (passeSanitaireElement.text().contains("pass sanitaire requis")) ? 1 : 0;
		System.out.println("passe sanitaire" + passeSanitaire);

		Elements modePaiementElements = doc
				.select("#zone-informations-pratiques > div.row > div.col-sm-12.col-md-6.zone-a-propos >"
						+ " div.zone-info-moyen-paiement > div > ul > li> figure > img");
		String modePaiement = "";
		if (!modePaiementElements.isEmpty()) {
			for (Element modePaiementElement : modePaiementElements) {
				modePaiement += modePaiementElement.attr("alt") + " ";
			}
		}
		System.out.println("mode paiement : " + modePaiement);

		this.saveItem(activiteSoumis, commune, cp, String.valueOf(nombreResultat), String.valueOf(pageNumber),
				String.valueOf(nombreResultatParPage), activite, tel, nomAgence, adresse, prestations, typeCuisine,
				budget, modePaiement, siteWeb, String.valueOf(passeSanitaire), siret, siren, ambiance);
		
		System.out.println("-----------------------------------------------------------------------------\n");
	}

	private void intFile(String path, String str) throws IOException {
		File dir = new File(path);
		if (!dir.exists())
			dir.mkdirs();
		File f = new File(dir, str);
		f.createNewFile();
		BufferedWriter bw = new BufferedWriter(new FileWriter(f, true));
		bw.write("actV,place,blocid,rubrique,localite,stat,nombreResultatParPage\r\n");
		bw.close();
	}

	private void writeLink(String path, String content) throws IOException {

		BufferedWriter bw = new BufferedWriter(new FileWriter(new File(path), true));
		bw.write(content);
		bw.close();
	}

	private void retrieveLinks(Document doc, String activiteSoumise, String depSoumise, int stat, String path,String dep)
			throws IOException {

		Map<String, String> bloc_ids = this.retrieveBlocId(doc, depSoumise);
		String code_rubrique = this.retrieveCodeRubrique(doc);

		for (String bloc_id : bloc_ids.keySet()) {
			String content = activiteSoumise + "," + dep + "," + bloc_id + "," + code_rubrique + ","
					+ bloc_ids.get(bloc_id) + "," + stat + "," + nombreParPage + "\r\n";
			this.writeLink(path, content);
		}

	}

	private Map<String, String> retrieveBlocId(Document doc, String depSoumise) {
		Map<String, String> codeIds = new HashMap<>();
		Elements blocIdElements = doc.select("#listresults > div > ul > li ");
		nombreParPage = blocIdElements.size();

		for (Element blocIdElement : blocIdElements) {
			Element address = blocIdElement.selectFirst("div.bi-address.small > a");
			//#epj-06967954 > div.bi-clic-mobile > div > div > div.bi-address.small > a
			System.out.println("retrieve bloc id for addres "+depSoumise+ " " + address.text());
			if (address.text().toLowerCase().contains(depSoumise)) {
				System.out.println("can be scrapped ");
				Element tmp = null;
				Elements oriasElement = blocIdElement.select("div.bi-clic-mobile > div > div > p.bi-mentions.tag-full");

				if (blocIdElement.selectFirst("div.bi-ctas > button") != null) {
					tmp = blocIdElement.selectFirst("div.bi-ctas > button");
					System.out.println(tmp.attr("data-pjsethisto"));

					JSONObject json = null;
					String str = "";
					String codeLocal = "";
					try {
						json = new JSONObject(tmp.attr("data-pjsethisto"));
						if (json.getJSONObject("data").getJSONObject("fd").has("id_bloc"))
							str = json.getJSONObject("data").getJSONObject("fd").getString("id_bloc");
						else if (json.getJSONObject("data").getJSONObject("fd").has("code_etab"))
							str = json.getJSONObject("data").getJSONObject("fd").getString("code_etab");
						if (json.getJSONObject("data").getJSONObject("fd").has("code_localite")) {
							codeLocal = json.getJSONObject("data").getJSONObject("fd").getString("code_localite");
                            
						}
					} catch (Exception e) {
						if (e instanceof JSONException) {
							json = new JSONObject(tmp.attr("data-pjhistofantomas"));
							str = json.getString("data");
						}
					}

					codeIds.put(str, codeLocal);
				} else {
					Element h = blocIdElement.selectFirst(" div > div:nth-child(2) > div > a");
					if (h != null) {
						System.out.println(h.attr("href"));
						// codeIds.add(h.attr("href"));
						codeIds.put(h.attr("href"), "");
					}

				}
			}

		}

		return codeIds;
	}

	private String retrieveCodeRubrique(Document doc) {
		Elements scripts = doc.select("script");
		String str = "";
		for (Element script : scripts) {
			str = script.html();
			if (str.contains("coderubrique")) {
				str = str.split("var _")[3].replaceAll("paramsstatpage = ", "");
				str = str.replaceAll("[^a-zA-Z0-9:,]", "").split(",")[6].replaceAll("[^0-9]", "").replaceAll("\\s{1,}",
						"");
				return str;

			}
		}
		return "";

	}

	public void quit() {
		ch.close();
	}

}
