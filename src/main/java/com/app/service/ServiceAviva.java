package com.app.service;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;

import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

import java.net.SocketTimeoutException;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Scanner;

import org.json.JSONArray;
import org.json.JSONObject;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.app.model.Aviva;

import com.app.scrap.RepAviva;

import com.app.utils.Parametres;
import com.app.utils.Utilitaires;

import fa.browserUtils.FactoryBrowser;
import fa.browserUtils.HtmlUnitBrowser;
import fa.log.LogApp.LogScrap;
import fa.log.config.LogConst;

@Service
public class ServiceAviva extends Utilitaires {

	@Autowired
	private RepAviva rep;

	private List<Aviva> avivaList = new ArrayList<>();
	private String nomSITE = "AVIVA";
	private String ficval = "AVIVA_LienAgences_New";

	//private String url_accueil = "https://www.aviva.fr/";
	private String url_accueil = "https://www.abeille-assurances.fr";

	private int NbAgences_ville;
	private int nbRecuperes;
	private int num_ligneficval;
	private int parcoursficval;

	private HtmlUnitDriver htmlUnitDriver;
	private BufferedWriter sortie;

	public Aviva saveData(Aviva t) {
		return rep.save(t);
	}

	public List<Aviva> saveDataAll(List<Aviva> t) {
		return rep.saveAll(t);
	}

	public List<Aviva> getElmnt() {

		return rep.findAll();
	}

	public Aviva getById(int id) {

		return (Aviva) rep.findById(id).orElse(null);
	}

	public String deleteProduct(int id) {
		try {
			rep.deleteById(id);

		} catch (Exception e) {
			e.printStackTrace();
		}

		return "removed";
	}

	public Aviva updateProduct(Aviva t) {
		// AvivaODO Auto-generated method stub
		return null;
	}

	public Aviva setData(String villeSoumise, String nbAgencesVille, String lienAgence, String typeSite,
			String nomAgence, String nomAgents, String adresse, String tel, String fax, String orias, String horaires,
			String agenceHAvivaM) {
		Aviva aviva = new Aviva();
		aviva.setVilleSoumise(villeSoumise).setNbAgencesVille(nbAgencesVille).setLienAgence(lienAgence)
				.setTypeSite(typeSite).setNomAgence(nomAgence).setNomAgents(nomAgents).setAdresse(adresse).setTel(tel)
				.setFax(fax).setOrias(orias).setHoraires(horaires).setAgenceHTM(agenceHAvivaM);
		this.saveData(aviva);
		return aviva;
	}

	public List<Aviva> showFirstRows() {
		List<Aviva> a = rep.getFirstRow(100, Aviva.class);
		return a;
	}

	public List<Aviva> showLastRows() {
		List<Aviva> a = rep.getLastRow(100, Aviva.class);
		return a;
	}

	public void setUpHtmlUintDriver() throws Exception {
		FactoryBrowser factoryB = new FactoryBrowser();
		HtmlUnitBrowser htmlB = (HtmlUnitBrowser) factoryB.create("htmlUnit");
		htmlUnitDriver = htmlB.setOptions(true, false, false, 60000).initBrowserDriver();
	}

	public List<Aviva> scrapAVIVA() throws IOException {
		String dossierRESU = Parametres.REPERTOIRE_RESULT + "\\ASSU\\" + nomSITE;
		Scanner sc = new Scanner(
				new BufferedReader(new FileReader(new File(Parametres.REPERTOIRE_DATA_SOURCES + ficval + ".txt"))));
		File f = new File(dossierRESU);
		if (!f.exists()) {
			f.mkdirs();
		}
		fichier = new File(dossierRESU + "\\" + "RESULTATS_" + nomSITE + "_" + System.currentTimeMillis() + ".txt");
		LogScrap log = new LogScrap("AssuranceAvivaLog.txt", LogConst.TYPE_LOG_FILE);
		if ((new File(LogConst.STORE_LOG_PATH + LogConst.PREFIX_LOG_FILE + "_AssuranceAvivaLog.txt")).exists()) {

			num_ligneficval = Integer.parseInt(log.readLog().getLastLineProcessed().split("\t")[4]);

		} else {
			num_ligneficval = 1;
		}

		parcoursficval = 0;
		String nom_agents = "";
		String agenceHTM = "";
		String nom_agence = "";
		String adresse = "";
		String tel = "";
		String fax = "";
		String horaires = "";
		String ORIAS = "";
		String typeSiteAgence = "";
		while (sc.hasNextLine()) {
			String line[] = sc.nextLine().split("\\t");
			String submitedCity = line[3];
			if (parcoursficval >= num_ligneficval) {
				//String submitedCity = line[3];
				String uri = url_accueil + submitedCity;
				System.out.println("Uri : "+uri);
				try {

					htmlUnitDriver.get(uri);

				} catch (Exception e) {

					if (e instanceof SocketTimeoutException) {
						htmlUnitDriver.get(uri);
					}
				}

				String pageSource = htmlUnitDriver.getPageSource();
				Document doc = Jsoup.parse(pageSource);
				Element rootJsoupElement = doc.select("#main").first();
				Elements agenciesLinkJsoupElement = rootJsoupElement
						.select("div:nth-child(3) > div:nth-child(2) > div.podContent > ul > li > a");
				NbAgences_ville = agenciesLinkJsoupElement.size();

				for (Element elementTmp : agenciesLinkJsoupElement) {
					nom_agents = "";
					agenceHTM = "";
					nom_agence = "";
					adresse = "";
					tel = "";
					fax = "";
					horaires = "";
					ORIAS = "";
					typeSiteAgence = "";

					String agencyLink = elementTmp.attr("href");

					if (agencyLink.contains("agences.aviva.fr")) {
						typeSiteAgence = "NV";
					}
					System.out.println(agencyLink);

					try {
						htmlUnitDriver.get(agencyLink);
					} catch (Exception e) {

						if (e instanceof SocketTimeoutException) {
							htmlUnitDriver.get(agencyLink);
						}
					}
					pageSource = htmlUnitDriver.getPageSource();
					doc = Jsoup.parse(pageSource);

					// retrieve agency infos
					Element agencyInfoJsoupElement = doc.select("script[type=\"application/ld+json\"]").first();
					if (agencyInfoJsoupElement != null) {
						String agencyInfo = agencyInfoJsoupElement.html();
						agencyInfo = agencyInfo.replaceAll("\\s{1,}", " ").trim();
						// agenceHTM=agencyInfo.replaceAll(regex, replacement);
						agenceHTM = this.normalize(agencyInfo);
						try {
							agencyInfo = agencyInfo.replaceAll("//\\<\\!\\[CDATA\\[", "");
							JSONObject jsonAgencyInfo = new JSONObject(agencyInfo);
							JSONArray jsonAgencyArray = jsonAgencyInfo.getJSONArray("@graph");
							for (int i = 0; i < jsonAgencyArray.length(); i++) {
								if (i == 0) {
									nom_agence = "";
									adresse = "";
									tel = "";
									horaires = "";

									JSONObject jsonAgencyCoordinate = jsonAgencyArray.getJSONObject(i);

									// retrieve agency name
									nom_agence = jsonAgencyCoordinate.getString("name");
									nom_agence = this.normalize(nom_agence);
									System.out.println("nom agence : " + nom_agence);

									// retrieve agency address
									JSONObject jsonAgencyAddress = jsonAgencyCoordinate.getJSONObject("address");
									String streetAddress = "", addressLocality = "", postalCode = "";
									streetAddress = jsonAgencyAddress.getString("streetAddress");
									addressLocality = jsonAgencyAddress.getString("addressLocality");
									postalCode = jsonAgencyAddress.getString("postalCode");
									adresse = streetAddress + " " + postalCode + " " + addressLocality;
									adresse = this.normalize(adresse);
									System.out.println("adresse : " + adresse);
									// retrieve agency phone
									tel = jsonAgencyCoordinate.getString("telephone");
									tel = tel.replaceAll("[^0-9 ]{1,}", "");
									tel = this.normalize(tel);
									System.out.println("tel : " + tel);
									// retrieve agency schedule
									JSONArray jsonScheduleArray = jsonAgencyCoordinate.getJSONArray("openingHours");
									for (int j = 0; j < jsonScheduleArray.length(); j++) {
										horaires += jsonScheduleArray.getString(j) + ",";
									}
									horaires = this.normalize(horaires);
								} else {
									// retrieve agent name
									JSONObject jsonAgentName = jsonAgencyArray.getJSONObject(i);
									String jobTitle = jsonAgentName.getString("jobTitle");
									System.out.println(jobTitle);
									if (jobTitle.contains("Agent Général")) {
										nom_agents += jsonAgentName.getString("name") + "&";
									}

								}
							}
							System.out.println("nom agent : " + nom_agents);
							// retrieve agency orias
							Elements jsonElemetJsoupElement = doc.select("script[type=\"text/javascript\"]");
							for (Element elementTmp2 : jsonElemetJsoupElement) {
								String jsonOrias = elementTmp2.html();
								if (jsonOrias.contains("window.JSContext")) {
									jsonOrias = jsonOrias.replaceAll("window.JSContext =", "").trim();
									jsonOrias = jsonOrias.replaceAll("//\\<\\!\\[CDATA\\[", "");
									JSONObject jo2 = new JSONObject(jsonOrias);
									JSONObject jsonProfile = jo2.getJSONObject("profile");
									JSONArray oriasArray = jsonProfile.getJSONArray("footer_numeros_orias");
									for (int i = 0; i < oriasArray.length(); i++) {
										JSONObject jsonTmp = oriasArray.getJSONObject(i);
										ORIAS += jsonTmp.getString("footer_numeros_orias") + " ";
									}

								}
							}
							ORIAS = this.normalize(ORIAS);
							System.out.println("orias : " + ORIAS);
							
							System.out.println("---------------------\n");
							
						} catch (Exception e) {

						}

						//this.setData(submitedCity, String.valueOf(NbAgences_ville), agencyLink, typeSiteAgence,
								//nom_agence, nom_agents, adresse, tel, fax, ORIAS, horaires, agenceHTM);
					}

				}

				//log.writeLog(new Date(), submitedCity, "no_error", NbAgences_ville, parcoursficval,
						//(NbAgences_ville - nbRecuperes));

			}
			parcoursficval++;

		}

		sc.close();
		return avivaList;
	}

	public void enregistrer(String lien_agence, String submitedCity, String typeSiteAgence, String nom_agence,
			String nom_agents, String adresse, String tel, String fax, String ORIAS, String horaires,
			String agenceHTM) {
		try {
			sortie = new BufferedWriter(new FileWriter(fichier, true));
			/*sortie.write(submitedCity + "\t" + NbAgences_ville + "\t" + lien_agence + "\t" + typeSiteAgence + "\t"
					+ nom_agence + "\t" + nom_agents + "\t" + adresse + "\t" + tel + "\t" + fax + "\t" + ORIAS + "\t"
					+ horaires + "\t" + agenceHTM + "\r\n");
			sortie.close();*/
		} catch (IOException e) {

		}

	}

	public void initialiser(String dossierRESU) throws IOException {

		sortie = new BufferedWriter(new FileWriter(fichier, true));
		/*sortie.write("VilleSoumise" + "\t" + "NbAgences_ville" + "\t" + "LIEN_AGENCE" + "\t" + "TYPE_SITE" + "\t"
				+ "NOM_AGENCE" + "\t" + "NOM_AGENTS" + "\t" + "ADRESSE" + "\t" + "TEL" + "\t" + "FAX" + "\t" + "ORIAS"
				+ "\t" + "HORAIRESHTM" + "\t" + "AGENCEHTM" + "\r\n");
		sortie.close();*/

	}

	public void tearDown() {

		try {
			htmlUnitDriver.quit();
		} catch (Exception e) {

		}

	}

}
