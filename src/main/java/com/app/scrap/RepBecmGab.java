package com.app.scrap;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.app.model.BECM_GAB;

@Repository
public interface RepBecmGab extends JpaRepository<BECM_GAB, Integer> {

}
