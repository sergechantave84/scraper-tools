package com.app.service;

import java.util.List;

public interface CRUD<T> {
	  T saveData(T t); 
	  List<T> saveDataAll(List<T> t) ;
	  List<T> getElmnt();
	  T getById(int id) ;
	  String deleteProduct(int id) ;
	  T updateProduct(T t) ;
}
