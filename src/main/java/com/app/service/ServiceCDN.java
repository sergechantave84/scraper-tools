package com.app.service;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.SocketTimeoutException;
import java.text.Normalizer;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Scanner;

import org.json.JSONObject;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.openqa.selenium.PageLoadStrategy;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.app.model.CDN;

import com.app.scrap.RepCDN;

import com.app.utils.Parametres;
import com.app.utils.Utilitaires;

import fa.browserUtils.ChromeBrowser;
import fa.browserUtils.FactoryBrowser;
import fa.browserUtils.HtmlUnitBrowser;
import fa.log.LogApp.LogScrap;
import fa.log.config.LogConst;

@Service
public class ServiceCDN extends Utilitaires {

	@Autowired
	private RepCDN rep;

	private static String driverPath = "C:\\ChromeDriver\\";

	private List<CDN> cdnList = new ArrayList<>();
	private String nomSITE = "CDN";
	private String ficval = "lien_cdn";
	private String dossierRESU = Parametres.REPERTOIRE_RESULT + "\\BQ\\" + nomSITE;
	private String url_accueil ="https://agences.sg.fr/banque-assurance/particulier/";
	// "https://agences.groupe-credit-du-nord.com/banque-assurance/credit-du-nord/";
	private String fichier_valeurs;
	private String cp;

	private int num_ligne = 2;
	private int NbAgences;
	private int nbRecuperes;
	private int num_ligneficval;
	private int parcoursficval;
	ChromeDriver chromeDriver;

	private HtmlUnitDriver htmlUnitDriver;
	private BufferedWriter sortie;
	private BufferedWriter sortie_log;

	private Date Temps_Fin;

	public CDN saveData(CDN t) {

		return rep.save((CDN) t);

	}

	public List<CDN> saveDataAll(List<CDN> t) {

		return rep.saveAll(t);

	}

	public List<CDN> getElmnt() {

		return rep.findAll();
	}

	public CDN getById(int id) {

		return rep.findById(id).orElse(null);
	}

	public String deleteProduct(int id) {
		try {
			rep.deleteById(id);

		} catch (Exception e) {
			e.printStackTrace();
		}

		return "removed";
	}

	public CDN updateProduct(CDN t) {
		// CDNODO Auto-generated method stub
		return null;
	}

	public CDN setData(String cpSoumise, String nbAgences, String enseigne, String nomWeb, String adresse, String tel,
			String fax, String codeGuichet, String horaires, String dab, String services, String agenceHtm) {
		CDN cdn = new CDN();
		cdn.setCpSoumise(cpSoumise).setNbAgences(nbAgences).setEnseigne(enseigne).setNomWeb(nomWeb).setAdresse(adresse)
				.setTel(tel).setFax(fax).setCodeGuichet(codeGuichet).setHoraires(horaires).setDab(dab)
				.setServices(services).setAgenceHtm(agenceHtm);
		this.saveData(cdn);
		return cdn;
	}

	public List<CDN> showFirstRows() {
		List<CDN> a = rep.getFirstRow(100, CDN.class);
		return a;
	}

	public List<CDN> showLastRows() {
		List<CDN> a = rep.getLastRow(100, CDN.class);
		return a;
	}

	public void setup() {
		FactoryBrowser fact = new FactoryBrowser();
		HtmlUnitBrowser hB = (HtmlUnitBrowser) fact.create("htmlUnit");
		htmlUnitDriver = hB.setOptions(false, false, false, 60000).initBrowserDriver();
		
		FactoryBrowser factBrowser = new FactoryBrowser();
		ChromeBrowser b = (ChromeBrowser) factBrowser.create("chrome");
		/*chromeDriver = b.setOptions(PageLoadStrategy.NORMAL, driverPath, 60000, 60000, 60000, false, false)
				.initBrowserDriver();*/ 
		
		final String binaryPath = "C:\\Program Files\\Google\\Chrome Beta\\Application\\chrome.exe";
		
		chromeDriver = b.setOptions(PageLoadStrategy.NORMAL, driverPath, 60000, 60000, 60000, false, false, binaryPath)
				.initBrowserDriver();
	}

	public List<CDN> scrapCDN() throws IOException, InterruptedException {
		Document doc = null;
		fichier_valeurs = Parametres.REPERTOIRE_DATA_SOURCES + ficval + ".txt";
		Scanner sc = new Scanner(new BufferedInputStream(new FileInputStream(new File(fichier_valeurs))));

		if (!new File(dossierRESU).exists()) {
			new File(dossierRESU).mkdirs();
		}
		fichier = new File(dossierRESU + "\\" + "RESULTATS_" + nomSITE + "_" + System.currentTimeMillis() + ".txt");
		LogScrap log = new LogScrap("BanqueCDN.txt", LogConst.TYPE_LOG_FILE);
		num_ligneficval = ((new File(LogConst.STORE_LOG_PATH + LogConst.PREFIX_LOG_FILE + "_BanqueCDN.txt")).exists())
				? Integer.parseInt(log.readLog().getLastLineProcessed().split("\\t")[4])
				: 1;

		parcoursficval = 1;

		while (sc.hasNextLine()) {

			String line = sc.nextLine();
			String[] arrayTmp = line.split("\\t");
			String url = "";
			if (parcoursficval > num_ligneficval) {
				cp = arrayTmp[0];
				url = arrayTmp[1];
				try {
					htmlUnitDriver.get(url);
					System.out.println("URL : "+url);

				} catch (Exception e) {
					if (e instanceof SocketTimeoutException) {
						htmlUnitDriver.get(url);

					}
				}

				Thread.sleep(5000);
				//System.out.println(cp);
				String pageSource = htmlUnitDriver.getPageSource();
				doc = Jsoup.parse(pageSource);
				
				//#agencies > div > div.jspPane > li.agency.agencesg.odd.selected > div.agency-inner > div.agencylabel > a
				
				Element rootJElement = doc.select("#wrapper").first();
				
				//System.out.println(rootJElement);
				
				if (!rootJElement.outerHtml().contains("Nous ne disposons pas d'agence proche du secteur recherché")) {
					System.out.println("Reponse pour : " + cp);
					List<String> agenciesLinkList = new ArrayList<>();
					if (!agenciesLinkList.isEmpty()) {
						agenciesLinkList.clear();
					}
					Elements agenciesLinksElement = rootJElement.select("#agencies > li > div.agency-inner > div.agencylabel > a");
					for (Element tmp : agenciesLinksElement) {
						agenciesLinkList.add(tmp.attr("href"));
					}
					//System.out.println("List link : " + agenciesLinkList);
					NbAgences = agenciesLinkList.size();
					nbRecuperes = 0;
					for (String link : agenciesLinkList) {
						try {
							
							chromeDriver.get("https://agences.sg.fr"+link);
							System.out.println("Link : " + "https://agences.sg.fr"+link);

						} catch (Exception e) {
							if (e instanceof SocketTimeoutException) {
								chromeDriver.get(link);

							}
						}

						String agences_HTM = "", nom_agence = "", adresse = "", tel = "", fax = "", CodeGuichet = "",
								HorairesHTM = "", services = "", enseigne = "", code = "";
						int DAB = 0;
						pageSource = chromeDriver.getPageSource();
						
						//System.out.println(pageSource);
						
						doc = Jsoup.parse(pageSource);
						Element root2JElement = doc.select("#content").first();

						agences_HTM = root2JElement.outerHtml().trim();
						agences_HTM = this.normalize(agences_HTM.replaceAll("\\s{1,}", ""));
						// retrieve agency name
						Element agencyNameJElement = root2JElement.select("#aside > div > div.agency.blocinfo.selected > div:nth-child(1) > div.info > h3").first();

						nom_agence = (agencyNameJElement != null)
								? Normalizer.normalize(agencyNameJElement.text(), Normalizer.Form.NFKD).replaceAll(
										"[^\\p{ASCII}]", "")
								: "";
						System.out.println("Nom : " + nom_agence);
						// retrieve agency address
						Element agencyAddressJElement = root2JElement
								.select("#aside > div > div.agency.blocinfo.selected > div:nth-child(1) > div:nth-child(3) > div")
								.first();
						adresse = (agencyAddressJElement != null)
								? Normalizer.normalize(agencyAddressJElement.text().trim(), Normalizer.Form.NFKD)
										.replaceAll("[^\\p{ASCII}]", "")
								: "";
						System.out.println("Adresse : " + adresse);
						// retrieve agency tel
						Element agencyTelJElement = root2JElement.select("#aside > div > div.agency.blocinfo.selected > div.links > a.btntel.btn.btn2.cta-phone").first();
						tel = (agencyTelJElement != null) ? agencyTelJElement.attr("href").replaceAll("[^0-9 ]","") : "";
						System.out.println("Tel : " + tel);
						// retrieve agency fax
						/*Element agencyFaxJElement = root2JElement
								.select("#addressbox > div.content > div > div.agencyfax").first();
						fax = (agencyFaxJElement != null) ? agencyFaxJElement.text().replaceAll("[^0-9 ]","")  : "";
						System.out.println("Fax : " + fax);*/
						
						// reterieve guichet code
						Element agencyCGJElement = root2JElement
								.select("#aside > div > div.agency.blocinfo > div.guichet-bloc").first();
						CodeGuichet = (agencyCGJElement != null) ? agencyCGJElement.text().replaceAll("[^0-9]", "")
								: "";
						System.out.println("CodeGuichet : " + CodeGuichet);

						Elements agencyScheduleJElement = root2JElement
								.select("#agencyschedule-content > div.agencyschedule_graphical > div > ul.graphicalSchedules__days > li");
						for (Element tmp : agencyScheduleJElement) {
							
							String day ="";
							String hours = "";
									
							if(tmp.hasClass("isClosed")) {
								day = tmp.text()+" : Fermee ";
							}else {
								day = tmp.select("div.graphicalSchedules__dayName").first().text();
								Elements hr = tmp.select("div.graphicalSchedules__dayTrack > div");
								String h_temp ="";
								for(Element hrr : hr) {
									if(hrr.hasClass("graphicalSchedules__range--rdv")) {
										h_temp += hrr.text()+"(Sur RDV) ";
									}else {
										h_temp += hrr.text()+" ";
									}
								}
								hours = h_temp;
							}
							
							HorairesHTM += day + " " + hours + " ";
						}
						//HorairesHTM = hrr;
						
						if(!HorairesHTM.toLowerCase().contains("dimanche")) {
							HorairesHTM+=" Dimanche : Fermee ";
						}
						
						HorairesHTM = Normalizer
								.normalize((HorairesHTM), Normalizer.Form.NFKD)
								.replaceAll("[^\\p{ASCII}]", "");
						System.out.println("Horaires : " +HorairesHTM);
						// retrieve service
						
						Elements agencyServicesJElement = root2JElement.select("#wrapper > div.services-agency-bloc.blocinfo > div");
						for (Element tmp : agencyServicesJElement) {
							services += tmp.text() + " & ";
						}
						if (services.toLowerCase().contains("distributeur automatique de billets")) {
							DAB = 1;
						}
						System.out.println("DAB : " + DAB);
						services = Normalizer.normalize(services, Normalizer.Form.NFKD).replaceAll("[^\\p{ASCII}]", "");
						System.out.println("Service : " + services);
						// retrieve enseigne
						enseigne = this.recuperer(url, enseigne, "/banque-assurance/", "/agence-");

						nbRecuperes++;
						this.setData(cp, String.valueOf(NbAgences), enseigne, nom_agence, adresse, tel, fax,
								CodeGuichet, HorairesHTM, String.valueOf(DAB), services, agences_HTM);
						System.out.println("\n--------------------------------------\n");

					}
					
				}

				//log.writeLog(new Date(), url, "no error", NbAgences, parcoursficval, NbAgences - nbRecuperes);
			}

			parcoursficval++;
		}
		sc.close();
		return cdnList;
	}

	public void enregistrer(String agences_HTM, String nom_agence, String adresse, String tel, String fax,
			String CodeGuichet, String HorairesHTM, String Services, String enseigne, int dab) {

//		this.fichier = new File(Parametres.REPERTOIRE + "RESULTATS_BNP.");
		try {

			sortie = new BufferedWriter(new FileWriter(fichier, true));
			sortie.write(cp + "\t" + NbAgences + "\t" + enseigne + "\t" + nom_agence + "\t" + adresse + "\t" + tel
					+ "\t" + fax + "\t" + CodeGuichet + "\t" + HorairesHTM + "\t" + dab + "\t" + Services + "\t"
					+ agences_HTM + "\r\n");

			sortie.close();
		} catch (IOException e) {

			e.printStackTrace();
		}

	}

	public void enregistrer_journal(int numreponse) {

//	    	this.fichier = new File(Parametres.REPERTOIRE + "RESULTATS_BNP.");
		try {
			Temps_Fin = new Date();
			SimpleDateFormat formater = null;
			formater = new SimpleDateFormat("'le' dd/MM/yyyy '�' hh:mm:ss");
			String date = formater.format(Temps_Fin);
			date = formater.format(Temps_Fin);

			sortie_log = new BufferedWriter(new FileWriter(log, true));
			sortie_log.write(parcoursficval + ";" + "FinValeur" + ";" + cp + ";" + this.NbAgences + ";" + nbRecuperes
					+ ";" + (this.NbAgences - nbRecuperes) + ";" + date + "\r\n");
			sortie_log.close();
			long taille_fichier = fichier.length() / 1000000; // la length est en octets, on divise par 1000 pour mettre
																// en Ko et par 1000000 pour mettre en Mo

			if (taille_fichier > 4) {
				// INITIALISATION FICHIER DE RESULTAT
				this.fichier = new File(
						dossierRESU + "\\" + "RESULTATS_" + nomSITE + "_" + System.currentTimeMillis() + ".txt");
				// Attention pas d'accent dans les noms de colonnes !
				sortie = new BufferedWriter(new FileWriter(fichier, true));
				sortie.write("CodePostaleSoumise" + "\t" + "NbAgences" + "\t" + "ENSEIGNE" + "\t" + "NOMWEB" + "\t"
						+ "ADRESSE" + "\t" + "TEL" + "\t" + "FAX" + "\t" + "CODE_GUICHET" + "\t" + "HORAIRESHTM" + "\t"
						+ "DAB" + "\t" + "SERVICES" + "\t" + "AGENCEHTM" + "\r\n");
				sortie.close();
			}

		} catch (IOException e) {

			e.printStackTrace();
		}

	}

	/**
	 * La m�thode <code>initialiser()</code> permet d'ajouter la ligne d'ent�te dans
	 * le fichier r�sultat.
	 */

	public void initialiser() {

		try {
			// Attention pas d'accent dans les noms de colonnes !
			sortie = new BufferedWriter(new FileWriter(fichier, true));
			sortie.write("CPSoumise" + "\t" + "NbAgences" + "\t" + "ENSEIGNE" + "\t" + "NOMWEB" + "\t" + "ADRESSE"
					+ "\t" + "TEL" + "\t" + "FAX" + "\t" + "CODE_GUICHET" + "\t" + "HORAIRESHTM" + "\t" + "DAB" + "\t"
					+ "SERVICES" + "\t" + "AGENCEHTM" + "\r\n");
			sortie.close();

			File toto = new File(dossierRESU + "\\" + "LOG_" + nomSITE + ".csv");
			if (!toto.exists()) {
				// if (num_ligneficval == num_ligne) {
				sortie_log = new BufferedWriter(new FileWriter(log, true));
				sortie_log.write("NumLigne" + ";" + "FinValeur" + ";" + "DepSoumis" + ";" + "NbAgences" + ";"
						+ "Nbrecuperes" + ";" + "Reste" + ";" + "Heure_Fin" + "\r\n");
				sortie_log.close();
			}

		} catch (IOException e) {

			e.printStackTrace();
		}
	}

	public void tearsDown() {
		htmlUnitDriver.close();

	}
}
