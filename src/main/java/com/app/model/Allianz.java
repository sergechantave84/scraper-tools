package com.app.model;

import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="allianz", schema="scrap")
public class Allianz {
     
	@Id
	@SequenceGenerator(name="scrap.allianz_id_seq",
	                     sequenceName ="scrap.allianz_id_seq",
	                     allocationSize=1)
    @GeneratedValue( strategy=GenerationType.SEQUENCE,
                     generator="scrap.allianz_id_seq")
	  
	@Column(name="id",updatable=false)
	private int id;
	
	@Column(name="depsoumis", columnDefinition="varchar")
	private String depSoumis;
	
	@Column(name="nbagences", columnDefinition="varchar")
	private String nbAgences;
	
	@Column(name="ville_soumise", columnDefinition="varchar")
	private String villeSoumise;
	
	@Column(name="nom_agence", columnDefinition="varchar")
    private String nomAgence;
	
	@Column(name="adresse", columnDefinition="varchar")
    private String adresse;
	
	@Column(name="tel", columnDefinition="varchar")
    private String tel;
	
	@Column(name="fax", columnDefinition="varchar")
    private String fax;
	
	@Column(name="orias", columnDefinition="varchar")
	private String orias;
	
	@Column(name="horaires", columnDefinition="varchar")
    private String horaires;
	
	@Column(name="agencehtm", columnDefinition="varchar")
    private String agenceHTM;
	
	@Column(name="interlocuteurhtm",columnDefinition="varchar")
    private String interlocuteurHTM;
	
	@Column(name="agent_generale",columnDefinition="varchar")
    private String agentGenerale;
    
	@Column(name="annee_du_scrap" ,
			columnDefinition="varchar(4) DEFAULT date_part('year',CURRENT_DATE)")
	private String anneeScrap;
	
	public Allianz() {
		LocalDate ld= new Date().toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
		anneeScrap=String.valueOf(ld.getYear()+1);
	}
	public int getId() {
		return id;
	}
	public Allianz setId(int id) {
		this.id = id;
		return this;
	}
	public String getDepSoumis() {
		return depSoumis;
	}
	public Allianz setDepSoumis(String depSoumis) {
		this.depSoumis = depSoumis;
		return this;
	}
	public String getNbAgences() {
		return nbAgences;
		
	}
	public Allianz setNbAgences(String nbAgences) {
		this.nbAgences = nbAgences;
		return this;
	}
	public String getVilleSoumise() {
		return villeSoumise;
	}
	public Allianz setVilleSoumise(String villeSoumise) {
		this.villeSoumise = villeSoumise;
		return this;
	}
	public String getNomAgence() {
		return nomAgence;
	}
	public Allianz setNomAgence(String nomAgence) {
		this.nomAgence = nomAgence;
		return this;
	}
	public String getAdresse() {
		return adresse;
	}
	public Allianz setAdresse(String adresse) {
		this.adresse = adresse;
		return this;
	}
	public String getTel() {
		return tel;
	}
	public Allianz setTel(String tel) {
		this.tel = tel;
		return this;
	}
	public String getFax() {
		return fax;
	}
	public Allianz setFax(String fax) {
		this.fax = fax;
		return this;
	}
	public String getHoraires() {
		return horaires;
	}
	public Allianz setHoraires(String horaires) {
		this.horaires = horaires;
		return this;
	}
	public String getAgenceHTM() {
		return agenceHTM;
	}
	public Allianz setAgenceHTM(String agenceHTM) {
		this.agenceHTM = agenceHTM;
		return this;
	}
	public String getInterlocuteurHTM() {
		return interlocuteurHTM;
	}
	public Allianz setInterlocuteurHTM(String interlocuteurHTM) {
		this.interlocuteurHTM = interlocuteurHTM;
		return this;
	}
	public String getAnneeScrap() {
		return anneeScrap;
	}
	public Allianz setAnneeScrap(String anneeScrap) {
		this.anneeScrap = anneeScrap;
		return this;
	}
	public String getOrias() {
		return orias;
	}
	public Allianz setOrias(String orias) {
		this.orias = orias;
		return this;
	}
	public String getAgentGenerale() {
		return agentGenerale;
	}
	public Allianz setAgentGenerale(String agentGenerale) {
		this.agentGenerale = agentGenerale;
		return this;
	}
}
