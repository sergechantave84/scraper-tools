package com.app.scrap;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.app.model.CM;

@Repository
public interface RepCM extends JpaRepository<CM,Integer>,RepositoryCustom<CM> {

}
