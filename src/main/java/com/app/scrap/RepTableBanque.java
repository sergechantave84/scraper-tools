package com.app.scrap;

import org.springframework.data.jpa.repository.JpaRepository;

import com.app.model.TableBanque;

public interface RepTableBanque extends JpaRepository<TableBanque, Integer>, RepositoryCustom<TableBanque>{

}
