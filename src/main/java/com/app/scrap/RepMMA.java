package com.app.scrap;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.app.model.MMA;

@Repository
public interface RepMMA extends JpaRepository<MMA,Integer>,RepositoryCustom<MMA> {

}
