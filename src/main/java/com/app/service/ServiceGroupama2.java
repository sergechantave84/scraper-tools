package com.app.service;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.net.SocketTimeoutException;
import java.text.Normalizer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Scanner;

import org.hibernate.mapping.Array;
import org.json.JSONArray;
import org.json.JSONObject;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.openqa.selenium.chrome.ChromeDriver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.app.model.Aviva;
import com.app.model.Groupama;
import com.app.model.PJAssuranceDep;
import com.app.scrap.AbeilleRepository;
import com.app.scrap.RepGroupama;
import com.app.utils.Parametres;
import fa.log.LogApp.LogScrap;
import fa.log.config.LogConst;

@Service
public class ServiceGroupama2 extends Scraper {

	@Autowired
	RepGroupama rep;

	//private String nomSITE = "ABEILLE";
	private String ficval = "lien_groupama";

	// private String url_accueil = "https://www.aviva.fr/";
	//private String url_accueil = "https://www.abeille-assurances.fr";

	private int lastLine =1;

	// private ch ch;
	private ChromeDriver ch;

	public Groupama saveData(Groupama t) {
		return rep.save(t);
	}

	public List<Groupama> saveDataAll(List<Groupama> t) {
		return rep.saveAll(t);
	}

	public List<Groupama> getElmnt() {

		return rep.findAll();
	}

	public Groupama getById(int id) {

		return (Groupama) rep.findById(id).orElse(null);
	}

	public String deleteProduct(int id) {
		try {
			rep.deleteById(id);

		} catch (Exception e) {
			e.printStackTrace();
		}

		return "removed";
	}

	public Groupama updateProduct(Groupama t) {
		// AvivaODO Auto-generated method stub
		return null;
	}

	public void setUp() throws Exception {
		/*
		 * FactoryBrowser factoryB = new FactoryBrowser(); HtmlUnitBrowser htmlB =
		 * (HtmlUnitBrowser) factoryB.create("htmlUnit"); ch =
		 * htmlB.setOptions(true, false, false, 60000).initBrowserDriver();
		 */
		final String chromeBeta = "C:\\Program Files\\Google\\Chrome Beta\\Application\\chrome.exe";
		ch = (ChromeDriver) this.setupCh(Parametres.CHROME_DRIVER_PATH, true, false,
				"C:\\Users\\GEOMADA PC4\\AppData\\Local\\Google\\Chrome Beta\\User Data", "", chromeBeta);
	}

	public void scrap() throws IOException, InterruptedException {

		Scanner sc = new Scanner(new BufferedInputStream(
				new FileInputStream(new File(Parametres.REPERTOIRE_DATA_SOURCES + ficval + ".txt"))));

		LogScrap log = new LogScrap("AssuranceGroupama.txt", LogConst.TYPE_LOG_FILE);

		if ((new File(LogConst.STORE_LOG_PATH + LogConst.PREFIX_LOG_FILE + "_AssuranceGroupama.txt").exists())) {
			Scanner sc3 = new Scanner(new BufferedInputStream(new FileInputStream(
					new File(LogConst.STORE_LOG_PATH + LogConst.PREFIX_LOG_FILE + "_AssuranceGroupama.txt"))));
			while (sc3.hasNextLine()) {
				lastLine = Integer.parseInt(sc3.nextLine().split("\t")[4]);
			}
		}

		int currentLine = 1;

		JSONArray nom_agentsArray = null;
		
		String nom_agents = "";
		String nom_agence = "";
		String adresse = "";
		String tel = "";
		String fax = "";
		String horaires = "";
		String ORIAS = "";
		String lien ="";

		while (sc.hasNextLine()) {
			String line[] = sc.nextLine().split("\\t");
			//String uri = line[1]
			String url = line[2];
			
			String regionSoumise = line[0];

			if (currentLine > lastLine) {

				System.out.println("Uri : " + url);

				ch.get(url);
				
				Thread.sleep(2000);
				
				String pageSource = ch.getPageSource();

				pageSource = Normalizer.normalize(ch.getPageSource().toLowerCase(), Normalizer.Form.NFD)
						.replaceAll("[^\\p{ASCII}]", "");

				Document doc = Jsoup.parse(pageSource);

			

				Element root = doc.selectFirst("#ito_ny_resultat");

				String result = root != null ? root.attr("data-result") : null;

				JSONArray jsonArrayRes = null;
				JSONObject json = null;

				// System.out.println(result);

				if (result != null) {
					jsonArrayRes = new JSONArray(result);

					System.out.println("Nombre de json : " + jsonArrayRes.length());
					
					int nbAgence = jsonArrayRes.length();

					for (int i = 0; i < jsonArrayRes.length(); ++i) {

						json = jsonArrayRes.getJSONObject(i);

						//System.out.println("json : " + json);

						nom_agence = (json.has("nom")) ? json.getString("nom").toUpperCase() : "";
						adresse = (json.has("adresse")) ? json.getString("adresse").toUpperCase() : "";

						tel = (json.has("tel")) ? json.getString("tel") : "";
						
						lien = (json.has("lien")) ? json.getString("lien") : "";

						horaires = (json.has("horaires")) ? json.getString("horaires") : "";
						
						if(json.has("nom") && json.has("adresse")) {
							this.saveItem(regionSoumise,nom_agence,adresse,tel,fax,horaires,lien, String.valueOf(nbAgence));
						}
						
						System.out.println("Nom : "+nom_agence);
						System.out.println("region : "+regionSoumise);
						System.out.println("adresse : "+adresse);
						System.out.println("telephone : "+tel);
						System.out.println("lien : "+lien);
						System.out.println("horaires : "+horaires);
						System.out.println("\n- - - - - - - - - - - - - - - - - - - - - - - - - - - - \n");

					}
				} else {
					System.err.println("Aucun resultat!");
				}

				log.writeLog(new Date(), line[0], "no_error", 1, currentLine, 0);

			}

			currentLine++;
		}
	}

	@Override
	public void saveItem(String... args) {
		
		Groupama groupama = new Groupama();
		
		groupama.setRegionSoumise(args[0]).setNomAgence(args[1]).setAdresse(args[2])
				.setTel(args[3]).setFax(args[4]).setHoraires(args[6]).setLienAgence(args[7]).setNbAgences(args[8]);
		rep.save(groupama);
		
	}
	

	public void tearDown() {
		ch.quit();

	}

}
