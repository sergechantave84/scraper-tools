package com.app.model;


import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="Banque_BECM_GAB", schema="scrap")
public class BECM_GAB {
	
	@Id
	@SequenceGenerator(name="scrap.Banque_BECM_GAB_id_seq",
	                   sequenceName = "scrap.Banque_BECM_GAB_id_seq",
	                   allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE,
	                generator = "scrap.Banque_BECM_GAB_id_seq")
	@Column(name="id", unique = true,nullable = false)
	int id;
	
	@Column(name="communesoumise", columnDefinition="varchar")
	private String communeSoumise;
	
	@Column(name="nbagences", columnDefinition="varchar")
	private String nbAgences;
	
	@Column(name="nomweb",columnDefinition="varchar")
	private String nomWeb;
	
	@Column(name="adresse", columnDefinition="varchar")
	private String adresse;
	
	@Column(name="gabcic",columnDefinition="varchar")
	private String gabCIC; 
	
	@Column(name="gabcm", columnDefinition="varchar")
	private String gabCM;
	 
    @Column(name="annee_du_scrap" ,columnDefinition="varchar(4) DEFAULT date_part('year',CURRENT_DATE)")
	private String anneeScrap;
	
	public BECM_GAB() {
		LocalDate ld= new Date().toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
		anneeScrap=String.valueOf(ld.getYear()+1);
		
	}

	public String getCommuneSoumise() {
		return communeSoumise;
	}

	public BECM_GAB setCommuneSoumise(String communeSoumise) {
		this.communeSoumise = communeSoumise;
		return this;
	}

	public String getNbAgences() {
		return nbAgences;
	}

	public BECM_GAB setNbAgences(String nbAgences) {
		this.nbAgences = nbAgences;
		return this;
	}

	public String getNomWeb() {
		return nomWeb;
	}

	public BECM_GAB setNomWeb(String nomWeb) {
		this.nomWeb = nomWeb;
		return this;
	}

	public String getAdresse() {
		return adresse;
	}

	public BECM_GAB setAdresse(String adresse) {
		this.adresse = adresse;
		return this;
	}

	public String getGabCIC() {
		return gabCIC;
	}

	public BECM_GAB setGabCIC(String gabCIC) {
		this.gabCIC = gabCIC;
		return this;
	}

	public String getGabCM() {
		return gabCM;
	}

	public BECM_GAB setGabCM(String gabCM) {
		this.gabCM = gabCM;
		return this;
	}

	

}
