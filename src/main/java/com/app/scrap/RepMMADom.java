package com.app.scrap;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.app.model.MMA;
import com.app.model.MMA_DOM;

@Repository
public interface RepMMADom extends JpaRepository<MMA_DOM,Integer>,RepositoryCustom<MMA_DOM> {

}
