package com.app.service;

import java.io.BufferedInputStream;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;

import java.net.SocketTimeoutException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Scanner;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.PageLoadStrategy;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.app.model.BP_GAB_Outre_Mer;

import com.app.scrap.RepBpGabOutreMer;
import com.app.utils.BrowserUtils;
import com.app.utils.Parametres;
import com.app.utils.Utilitaires;

import fa.browserUtils.ChromeBrowser;
import fa.browserUtils.FactoryBrowser;
import fa.log.LogApp.LogScrap;
import fa.log.config.LogConst;

@Service
public class ServiceBP_GAB_Outre_Mer extends Utilitaires {

	@Autowired
	private RepBpGabOutreMer rep;

	private String driverPath = "C:\\chromeDriver\\";

	private List<com.app.model.BP_GAB_Outre_Mer> bpGabList = new ArrayList<>();
	private String nomSITE = "BP_GAB_Outre_Mer";
	private String ficval = Parametres.OUTRE_MER_NOM_FICHIER;
	private String dossierRESU = Parametres.REPERTOIRE_RESULT + "\\BQ\\" + nomSITE;;
	private String url_accueil = "https://agences.banquepopulaire.fr/banque-assurance/distributeur/";
	private String cp;
	private String fichier_valeurs;

	private int NbAgences;
	private int nbRecuperes;
	private int num_ligneficval;
	private int parcoursficval;
	private int HORS_SITE;
	private int DAB = 1;

	private static ChromeDriver chromeDriver;
	private BufferedWriter sortie;
	private BufferedWriter sortie_log;

	private Date Temps_Fin;

	public BP_GAB_Outre_Mer saveData(BP_GAB_Outre_Mer t) {

		return rep.save(t);

	}

	public List<BP_GAB_Outre_Mer> saveDataAll(List<BP_GAB_Outre_Mer> t) {
		return rep.saveAll(t);
	}

	public List<BP_GAB_Outre_Mer> getElmnt() {

		return rep.findAll();
	}

	public BP_GAB_Outre_Mer getById(int id) {

		return rep.findById(id).orElse(null);
	}

	public String deleteProduct(int id) {
		try {
			rep.deleteById(id);

		} catch (Exception e) {
			e.printStackTrace();
		}

		return "removed";
	}

	public BP_GAB_Outre_Mer updateProduct(BP_GAB_Outre_Mer t) {
		// BP_GAB_Outre_MerODO Auto-generated method stub
		return null;
	}

	public BP_GAB_Outre_Mer setData(String depSoumis, String total, String nbAgences, String enseigne, String nomWeb,
			String adresse, String dab, String horsSite, String agenceHtm) {
		BP_GAB_Outre_Mer bpGab = new BP_GAB_Outre_Mer();
		bpGab.setDepSoumis(depSoumis).setTotal(total).setNbAgences(nbAgences).setEnseigne(enseigne).setNomWeb(nomWeb)
				.setAdresse(adresse).setDab(dab).setHorsSite(horsSite).setAgenceHtm(agenceHtm);
		this.saveData(bpGab);
		return bpGab;
	}

	public List<BP_GAB_Outre_Mer> showFirstRows() {
		List<BP_GAB_Outre_Mer> a = rep.getFirstRow(100, BP_GAB_Outre_Mer.class);
		return a;
	}

	public List<BP_GAB_Outre_Mer> showLastRows() {
		List<BP_GAB_Outre_Mer> a = rep.getLastRow(100, BP_GAB_Outre_Mer.class);
		return a;
	}

	public void setup() {
		FactoryBrowser factBrowser = new FactoryBrowser();
		ChromeBrowser b = (ChromeBrowser) factBrowser.create("chrome");
		final String binaryPath = "C:\\Program Files\\Google\\Chrome Beta\\Application\\chrome.exe";
		chromeDriver = b.setOptions(PageLoadStrategy.NORMAL, driverPath, 60000, 60000, 60000, false, false, binaryPath)
				.initBrowserDriver();
		//chromeDriver = BrowserUtils.chromeConstruct(driverPath, 60000, 60000, 60000, PageLoadStrategy.NORMAL);
	}

	public List<com.app.model.BP_GAB_Outre_Mer> scrapBPGAB() throws IOException, InterruptedException {
		boolean documentReadyState = false;
		JavascriptExecutor jsExecutor = (JavascriptExecutor) chromeDriver;
		String javaScript = "";
		fichier_valeurs = Parametres.REPERTOIRE_DATA_SOURCES + ficval + ".txt";
		Scanner sc = new Scanner(new BufferedInputStream(new FileInputStream(new File(fichier_valeurs))));

		if (!new File(dossierRESU).exists()) {
			new File(dossierRESU).mkdirs();
		}
		fichier = new File(dossierRESU + "\\" + "RESULTATS_" + nomSITE + "_" + System.currentTimeMillis() + ".txt");
		LogScrap log = new LogScrap("BanqueBPGabOutreMer.txt", LogConst.TYPE_LOG_FILE);
		num_ligneficval = ((new File(LogConst.STORE_LOG_PATH + LogConst.PREFIX_LOG_FILE + "_BanqueBPGabOutreMer.txt"))
				.exists()) ? Integer.parseInt(log.readLog().getLastLineProcessed().split("\\t")[4]) : 1;

		parcoursficval = 0;
		while (sc.hasNextLine()) {
			String line = sc.nextLine();
			String[] fic_valeurs = line.split("\\t");
			if (parcoursficval > num_ligneficval) {
				cp = fic_valeurs[0];
				System.out.println("url_accueil " + url_accueil);
				try {
					chromeDriver.get(url_accueil);

				} catch (Exception e) {
					if (e instanceof SocketTimeoutException) {
						chromeDriver.get(url_accueil);

					}
				}
				documentReadyState = this.listensIfPageFullyLoaded(chromeDriver);
				if (documentReadyState) {
					Thread.sleep(5000);
					WebElement btn_cab = chromeDriver.findElement(By.cssSelector("#mdc-tab-1"));
					btn_cab.click();

					WebElement input = chromeDriver.findElement(By.cssSelector("#em-search-form__searchcity"));
					/*javaScript = String.format("arguments[0].value=\"%s\"", cp);
					jsExecutor.executeScript(javaScript, input);*/
					input.sendKeys(cp);
					
					Thread.sleep(2000);

					WebElement btnSearch = chromeDriver.findElement(
							By.cssSelector("#em-search-form > div > div.em-search-form__fieldset-wrapper > "
									+ "fieldset.em-search-form__fieldset.em-search-form__buttons > div:nth-child(2) > button"));
					btnSearch.click();
					/*
					 * javaScript=String.format("arguments[0].click()","");
					 * jsExecutor.executeScript(javaScript,btnSearch);
					 */

					documentReadyState = this.listensIfPageFullyLoaded(chromeDriver);
					if (documentReadyState) {

						Thread.sleep(2000);
						String pageSource = "";
				
						Document doc = null;
						pageSource = chromeDriver.getPageSource();
						doc = Jsoup.parse(pageSource);
						Element rootJElement = doc.select(
								"body > div.em-page > main > div.em-results-wrapper > div > div.em-results__list-wrapper > div")
								.first();
						Elements agencyLinksJElement = (rootJElement != null) ? rootJElement.select("ul > li") : null;
						if (agencyLinksJElement != null) {
							NbAgences = agencyLinksJElement.size();
							System.out.println("NbAgences " + NbAgences);
							nbRecuperes = 0;
							for (Element elmntTmp : agencyLinksJElement) {
								String nom_agence = "";
								String adresse = "";
								String enseigne = "", agences_HTM = "";

								agences_HTM = elmntTmp.outerHtml().trim();
								agences_HTM = this.normalize(agences_HTM.replaceAll("\\s{1,}", ""));
								// retrieve agency name
								Element agencyNameJElement = elmntTmp.select("h2 >  a").first();
								nom_agence = (agencyNameJElement != null) ? this.normalize(agencyNameJElement.text())
										: "";

								System.out.println("nom agence " + nom_agence);
								// retrieve agency address

								Element agencyAddressJElement = elmntTmp.select("div.em-poi-card__address").first();
								adresse = (agencyAddressJElement != null) ? this.normalize(agencyAddressJElement.text())
										: "";
								System.out.println("adresse " + adresse);

								// retrieve agency anseigne
								Element agencyEnseignJElement = elmntTmp.select("h2>span.em-poi-card__network").first();
								enseigne = (agencyEnseignJElement != null)
										? this.normalize(agencyEnseignJElement.text())
										: "";
								System.out.println("enseigne " + enseigne);

								HORS_SITE = (nom_agence.toLowerCase().contains("agence")) ? 0 : 1;
								
								System.out.println("HORS_SITE " + HORS_SITE);

								nbRecuperes++;
								this.setData(cp, String.valueOf(NbAgences), String.valueOf(NbAgences), enseigne,
										nom_agence, adresse, String.valueOf(DAB), String.valueOf(HORS_SITE),
										agences_HTM);
							}
						} else {
							System.out.println("No result");
							//this.enregistrer(0, "", "", "", "");
						}
						// this.enregistrer_journal(nbRecuperes);
					}
				}
				log.writeLog(new Date(), cp, "no_error", NbAgences, parcoursficval, NbAgences - nbRecuperes);
			}
			parcoursficval++;
		}
		sc.close();
		return bpGabList;
	}

	public void enregistrer(int total, String agences_HTM, String nom_agence, String adresse, String enseigne) {

		try {

			sortie = new BufferedWriter(new FileWriter(fichier, true));
			sortie.write(cp + "\t" + total + "\t" + NbAgences + "\t" + enseigne + "\t" + nom_agence + "\t" + adresse
					+ "\t" + DAB + "\t" + HORS_SITE + "\t" + agences_HTM + "\r\n");

			sortie.close();
		} catch (IOException e) {

			e.printStackTrace();
		}

	}

	public void enregistrer_journal(int numreponse) {

//	    	this.fichier = new File(Parametres.REPERTOIRE + "RESULTATS_BNP.");
		try {
			Temps_Fin = new Date();
			SimpleDateFormat formater = null;
			formater = new SimpleDateFormat("'le' dd/MM/yyyy '�' hh:mm:ss");
			String date = formater.format(Temps_Fin);
			date = formater.format(Temps_Fin);

			sortie_log = new BufferedWriter(new FileWriter(log, true));
			sortie_log.write(parcoursficval + ";" + "FinValeur" + ";" + cp + ";" + this.NbAgences + ";" + nbRecuperes
					+ ";" + (this.NbAgences - nbRecuperes) + ";" + date + "\r\n");
			sortie_log.close();
			long taille_fichier = fichier.length() / 1000000; // la length est en octets, on divise par 1000 pour mettre
																// en Ko et par 1000000 pour mettre en Mo

			if (taille_fichier > 4) {
				// INITIALISATION FICHIER DE RESULTAT
				this.fichier = new File(
						dossierRESU + "\\" + "RESULTATS_" + nomSITE + "_" + System.currentTimeMillis() + ".txt");
				// Attention pas d'accent dans les noms de colonnes !
				sortie = new BufferedWriter(new FileWriter(fichier, true));
				sortie.write("CPSoumis" + "\t" + "TOTAL" + "\t" + "NbAgences" + "\t" + "ENSEIGNE" + "\t" + "NOMWEB"
						+ "\t" + "ADRESSE" + "\t" + "DAB" + "\t" + "HORS_SITE" + "\t" + "AGENCEHTM" + "\r\n");
				sortie.close();
			}

		} catch (IOException e) {

			e.printStackTrace();
		}

	}

	public void initialiser() {

		try {
			// Attention pas d'accent dans les noms de colonnes !
			sortie = new BufferedWriter(new FileWriter(fichier, true));
			sortie.write("DepSoumis" + "\t" + "TOTAL" + "\t" + "NbAgences" + "\t" + "ENSEIGNE" + "\t" + "NOMWEB" + "\t"
					+ "ADRESSE" + "\t" + "DAB" + "\t" + "HORS_SITE" + "\t" + "AGENCEHTM" + "\r\n");
			sortie.close();

			File toto = new File(dossierRESU + "\\" + "LOG_" + nomSITE + ".csv");
			if (!toto.exists()) {
				// if (num_ligneficval == num_ligne) {
				sortie_log = new BufferedWriter(new FileWriter(log, true));
				sortie_log.write("NumLigne" + ";" + "BIDON" + ";" + "DepSoumis" + ";" + "NbAgences" + ";"
						+ "Nbrecuperes" + ";" + "Reste" + ";" + "Heure_Fin" + "\r\n");
				sortie_log.close();
			}

		} catch (IOException e) {

			e.printStackTrace();
		}
	}

	public void tearDown() {
		chromeDriver.quit();

	}
}
