package com.app.model;

import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="maif_dom", schema="scrap")
public class MAIF_DOM {
	
	@Id
	@SequenceGenerator(name="scrap.maif_dom_id_seq",
	                    sequenceName ="scrap.maif_dom_id_seq",
	                     allocationSize=1)
    @GeneratedValue( strategy=GenerationType.SEQUENCE,
                     generator="scrap.maif_dom_id_seq")
	  
	@Column(name="id",updatable=false)
	private int id;
	
    @Column(name="nbagences",  columnDefinition="varchar")
	private String nbAgences;
	
	@Column(name="lienagence",  columnDefinition="varchar")
	private String lienAgence;
	
	@Column(name="nom_agence",  columnDefinition="varchar")
	private String nomAgence;
	
	@Column(name="adresse",  columnDefinition="varchar")
	private String adresse;
	
	@Column(name="tel",  columnDefinition="varchar")
	private String tel;
	
	@Column(name="horaires",  columnDefinition="varchar")
	private String horaires;
	
	@Column(name="agencehtm", columnDefinition="varchar")
	private String agenceHtm;
	
	@Column(name="annee_du_scrap" ,columnDefinition="varchar(4) DEFAULT date_part('year',CURRENT_DATE)")
	private String anneeScrap;

	public MAIF_DOM() {
		LocalDate ld = new Date().toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
		anneeScrap = String.valueOf(ld.getYear() + 1);
	}
	public int getId() {
		return id;
	}

	public MAIF_DOM setId(int id) {
		this.id = id;
		return this;
	}

	public String getNbAgences() {
		return nbAgences;
	}

	public MAIF_DOM setNbAgences(String nbAgences) {
		this.nbAgences = nbAgences;
		return this;
	}

	public String getLienAgence() {
		return lienAgence;
	}

	public MAIF_DOM setLienAgence(String lienAgence) {
		this.lienAgence = lienAgence;
		return this;
	}

	public String getNomAgence() {
		return nomAgence;
	}

	public MAIF_DOM setNomAgence(String nomAgence) {
		this.nomAgence = nomAgence;
		return this;
	}

	public String getAdresse() {
		return adresse;
	}

	public MAIF_DOM setAdresse(String adresse) {
		this.adresse = adresse;
		return this;
	}

	public String getTel() {
		return tel;
	}

	public MAIF_DOM setTel(String tel) {
		this.tel = tel;
		return this;
	}

	public String getHoraires() {
		return horaires;
	}

	public MAIF_DOM setHoraires(String horaires) {
		this.horaires = horaires;
		return this;
	}

	public String getAgenceHtm() {
		return agenceHtm;
	}

	public MAIF_DOM setAgenceHtm(String agenceHtm) {
		this.agenceHtm = agenceHtm;
		return this;
	}

	public String getAnneeScrap() {
		return anneeScrap;
	}

	public MAIF_DOM setAnneeScrap(String anneeScrap) {
		this.anneeScrap = anneeScrap;
		return this;
		
	}


}
