package com.app.scrap;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.app.model.Aviva;

@Repository
public interface RepAviva extends JpaRepository<Aviva,Integer>, RepositoryCustom<Aviva> {

}
