package com.app.model;

import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="banque_lcl", schema="scrap")
public class LCL {

	
	@Id
	@SequenceGenerator(name="scrap.banque_lcl_id_seq",
	                    sequenceName ="scrap.banque_lcl_id_seq",
	                     allocationSize=1)
    @GeneratedValue( strategy=GenerationType.SEQUENCE,
                     generator="scrap.banque_lcl_id_seq")
	  
	@Column(name="id",updatable=false)
	private int id;
	
	@Column(name="valeursoumise",  columnDefinition="varchar")
	private String valeurSoumise;
	
	@Column(name="nbagences",  columnDefinition="varchar")
	private String nbAgences;
	
	@Column(name="nblienstrouves",  columnDefinition="varchar")
	private String nbLiensTrouves;
	
	@Column(name="numagence",  columnDefinition="varchar")
	private String numAgence;
	
	@Column(name="nomweb",  columnDefinition="varchar")
	private String nomWeb;
	
	@Column(name="adresse",  columnDefinition="varchar")
	private String adresse;
	
	@Column(name="tel",  columnDefinition="varchar")
	private String tel;
	
	@Column(name="fax",  columnDefinition="varchar")
	private String fax;
	
	@Column(name="horaireshtm", columnDefinition="varchar")
	private String horairesHtm;
	
	@Column(name="horairestxt",  columnDefinition="varchar")
	private String horairesTxt;
	
	@Column(name="services",  columnDefinition="varchar")
	private String services;
	
	@Column(name="agencehtm", columnDefinition="varchar")
	private String agenceHtm;
	
	@Column(name="annee_du_scrap" ,columnDefinition="varchar(4) DEFAULT date_part('year',CURRENT_DATE)")
	private String anneeScrap;
	
	public LCL() {
		LocalDate ld = new Date().toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
		anneeScrap = String.valueOf(ld.getYear() + 1);
	}
	public int getId() {
		return id;
	}
	public LCL setId(int id) {
		this.id = id;
		return this;
	}
	public String getValeurSoumise() {
		return valeurSoumise;
	}
	public LCL setValeurSoumise(String valeurSoumise) {
		this.valeurSoumise = valeurSoumise;
		return this;
	}
	public String getNbAgences() {
		return nbAgences;
	}
	public LCL setNbAgences(String nbAgences) {
		this.nbAgences = nbAgences;
		return this;
	}
	public String getNbLiensTrouves() {
		return nbLiensTrouves;
	}
	public LCL setNbLiensTrouves(String nbLiensTrouves) {
		this.nbLiensTrouves = nbLiensTrouves;
		return this;
	}
	public String getNumAgence() {
		return numAgence;
	}
	public LCL setNumAgence(String numAgence) {
		this.numAgence = numAgence;
		return this;
	}
	public String getNomWeb() {
		return nomWeb;
	}
	public LCL setNomWeb(String nomWeb) {
		this.nomWeb = nomWeb;
		return this;
	}
	public String getAdresse() {
		return adresse;
	}
	public LCL setAdresse(String adresse) {
		this.adresse = adresse;
		return this;
	}
	public String getTel() {
		return tel;
	}
	public LCL setTel(String tel) {
		this.tel = tel;
		return this;
	}
	public String getFax() {
		return fax;
	}
	public LCL setFax(String fax) {
		this.fax = fax;
		return this;
	}
	public String getHorairesHtm() {
		return horairesHtm;
	}
	public LCL setHorairesHtm(String horairesHtm) {
		this.horairesHtm = horairesHtm;
		return this;
	}
	public String getHorairesTxt() {
		return horairesTxt;
	}
	public LCL setHorairesTxt(String horairesTxt) {
		this.horairesTxt = horairesTxt;
		return this;
	}
	public String getServices() {
		return services;
	}
	public LCL setServices(String services) {
		this.services = services;
		return this;
	}
	public String getAgenceHtm() {
		return agenceHtm;
	}
	public LCL setAgenceHtm(String agenceHtm) {
		this.agenceHtm = agenceHtm;
		return this;
	}
	public String getAnneeScrap() {
		return anneeScrap;
	}
	public LCL setAnneeScrap(String anneeScrap) {
		this.anneeScrap = anneeScrap;
		return this;
	}
}
