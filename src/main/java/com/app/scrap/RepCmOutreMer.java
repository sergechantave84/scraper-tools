package com.app.scrap;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.app.model.CM_Outre_Mer;

@Repository
public interface RepCmOutreMer extends JpaRepository<CM_Outre_Mer, Integer>, RepositoryCustom<CM_Outre_Mer> {

}
