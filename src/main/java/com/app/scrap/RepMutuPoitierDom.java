package com.app.scrap;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.app.model.Mutuelle_Poitier;
import com.app.model.Mutuelle_Poitier_DOM;

@Repository
public interface RepMutuPoitierDom extends JpaRepository<Mutuelle_Poitier_DOM,Integer>,RepositoryCustom<Mutuelle_Poitier_DOM> {

}
