package com.app.scrap;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.app.model.TableAutre;

@Repository
public interface RepTableAutre extends JpaRepository<TableAutre, Integer>{

}
