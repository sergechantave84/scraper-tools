package com.app.controller.admin;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.app.model.TableAssurance;
import com.app.model.TableAutre;
import com.app.model.TableBanque;
import com.app.model.TableBanqueDOM;
import com.app.service.admin.AdminService;


@RestController
@RequestMapping("/bigMan")

public class AdminControler {

	@Autowired
	AdminService as;
	
	@RequestMapping("/popBanque")
	public void populateTableBanque() {
		as.populateBanqueTable();
		
	}
	
	@RequestMapping("/popAssurance")
	public void populateTableAssurance() {
		as.populateAssuranceTable();
		
	}
	
	@RequestMapping("/popAutre")
	public void populateTableAutre() {
		as.populateAutreTable();
		
	}
	
	@RequestMapping("/popDOM")
	public void populateTableDOM() {
		as.populateDOMTable();
		
	}
	
	@RequestMapping("/addCB")
	public void addCB(@RequestParam(value="name")String name) {
		as.addColumn(name,new TableBanque() );
		
	}
	
	@RequestMapping("/addCA")
	public void addCA(@RequestParam(value="name")String name) {
		as.addColumn(name,new TableAssurance() );
		
	}
	
	@RequestMapping("/addDOM")
	public void addDOM(@RequestParam(value="name")String name) {
		as.addColumn(name,new TableBanqueDOM() );
		
	}
	
	@RequestMapping("/addAu")
	public void addAu(@RequestParam(value="name")String name) {
		as.addColumn(name,new TableAutre() );
		
	}
	
	@CrossOrigin(origins = "http://127.0.0.1:5500", methods = {RequestMethod.POST})
	@RequestMapping("/upToJourB")
	public void updateStatusBanque(@RequestParam(value="name")String name) {
		as.updateStatusBanque(name);
	}
	
	@CrossOrigin(origins = "*", methods = {RequestMethod.POST})
	@RequestMapping("/upToJourA")
	public void updateStatusAssurance(@RequestParam(value="name")String name) {
		as.updateStatusAssurance(name);
	}
	
	@CrossOrigin(origins = "*", methods = {RequestMethod.POST})
	@RequestMapping("/upToJourAu")
	public void updateStatusAutre(@RequestParam(value="name")String name) {
		as.updateStatusAutre(name);
	}
	
	@CrossOrigin(origins = "*", methods = {RequestMethod.POST})
	@RequestMapping("/upToJourDom")
	public void updateStatusDom(@RequestParam(value="name")String name) {
		as.updateStatusDOM(name);
	}
	
	@CrossOrigin(origins = "*", methods = {RequestMethod.GET})
	@RequestMapping("/getItemBanque")
	public List<TableBanque>  getAssuranceItems() {
		List<TableBanque> l=as.getItem(TableBanque.class);
		return l;
	}
	
	@CrossOrigin(origins = "*", methods = {RequestMethod.GET})
	@RequestMapping("/getItemAssu")
	public List<TableAssurance>  getBanqueItems() {
		List<TableAssurance> l=as.getItem(TableAssurance.class);
		return l;
	}
	
	@CrossOrigin(origins = "*", methods = {RequestMethod.GET})
	@RequestMapping("/getItemAutre")
	public List<TableAutre>  getAutreItems() {
		List<TableAutre> l=as.getItem(TableAutre.class);
		return l;
	}
	
	@CrossOrigin(origins = "*", methods = {RequestMethod.GET})
	@RequestMapping("/getItemDOM")
	public List<TableBanqueDOM>  getDOMItems() {
		List<TableBanqueDOM> l=as.getItem(TableBanqueDOM.class);
		return l;
	}
}
