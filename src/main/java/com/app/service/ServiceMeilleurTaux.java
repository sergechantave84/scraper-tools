package com.app.service;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.Date;
import java.util.Scanner;

import javax.net.ssl.HttpsURLConnection;

import org.apache.commons.text.StringEscapeUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.app.model.MeilleurTaux;
import com.app.scrap.RepMeilleurTaux;
import com.app.utils.Parametres;

import fa.log.LogApp.LogScrap;
import fa.log.config.LogConst;

@Service
public class ServiceMeilleurTaux extends Scraper{

	@Autowired
	RepMeilleurTaux rep;
	
	private String ficval="lien_meilleur_taux.txt";
	
	@Override
	public void saveItem(String... args) {
		MeilleurTaux mt=new MeilleurTaux();
		mt.setName(args[0]).setAdresse(args[1]).setTel(args[2]).setHoraires(args[3]).setVilleS(args[4]);
		rep.save(mt);
		
	}

	@Override
	public void scrap() throws Exception {
		Scanner sc=new Scanner(new BufferedInputStream(new FileInputStream(new File(Parametres.REPERTOIRE_DATA_SOURCES+ficval))));
		LogScrap log = new LogScrap("BanqueMeilleurtauxLog.txt", LogConst.TYPE_LOG_FILE);
		int currentLine = ((new File(LogConst.STORE_LOG_PATH + LogConst.PREFIX_LOG_FILE + "_BanqueMeilleurtauxLog.txt"))
				.exists()) ? Integer.parseInt(log.readLog().getLastLineProcessed().split("\t")[4]) : 1;

		int counter = 1;
		
		while(sc.hasNextLine()) {
			String[] lines=sc.nextLine().split("\t");
			
			if( counter > currentLine ) {
				System.out.println("url : " + lines[1]);
				URL url= new URL(lines[1]);
				HttpsURLConnection con=(HttpsURLConnection) url.openConnection();
				con.setRequestMethod("GET");
				con.setReadTimeout(600000);
				con.setConnectTimeout(600000);
				con.setUseCaches(false);
				con.setAllowUserInteraction(false);
				con.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
				con.setRequestProperty("Cache-Control","no-cache,no-store,must-revalidate");
				int responceCode = con.getResponseCode();
				BufferedReader br=null;
				String pageSource="";
				StringBuilder strB=new StringBuilder();
				if (responceCode == HttpsURLConnection.HTTP_OK) {
				     br= new BufferedReader(new InputStreamReader(con.getInputStream(),"UTF-8"));
				     while((pageSource=br.readLine())!=null) {
							strB.append(pageSource);
						}
				}
				
				pageSource=StringEscapeUtils.unescapeHtml4(strB.toString());
				
				Document doc=Jsoup.parse(pageSource);
				//Element root=doc.selectFirst("#content-wrapper > div > div > div:nth-child(2) > div > div > div.item-page > div.row.centrer.marges.stretch.row-map.grid-complet > div");
				Element root=doc.selectFirst("div.rub-apropos.agence");
				if(root!=null) {
					String name="";
					String adresse="";
					String tel="";
					String horaires="";
					
					Element agentNameElement=root.selectFirst("div.ap-banner > div.ap-big-title > div > div > h1");
					name=(agentNameElement!=null)? agentNameElement.text() :"";
					System.out.println("name : "+name);
					
					Element adresseElement =root.selectFirst("div.our-agence-coordonnees-info > span.adresse");
					adresse=(adresseElement!=null) ? adresseElement.text().toUpperCase().replaceAll(",","") :"";
					System.out.println("adresse : "+adresse);
					
					Element telElement= root.selectFirst("div.our-agence-coordonnees-info > span.telephone > span > a");
					tel=(telElement!=null) ?telElement.text() :"";
					System.out.println("tel : "+tel);
					
					Elements horairesElements= root.select("#horaires > span > ul.liste >li");
					if(!horairesElements.isEmpty()) {
						for(Element horaireElement: horairesElements) {
							horaires+=horaireElement.text()+" ";
						}
					}
					if(!horaires.toLowerCase().contains("samedi")) {
						horaires +=" Samedi : Ferme";
					}
					if(!horaires.toLowerCase().contains("dimanche")) {
						horaires +=" Dimanche : Ferme";
					}
					
					System.out.println("horaires : " + horaires);
					//	mt.setName(args[0]).setAdresse(args[1]).setTel(args[2]).setHoraires(args[3]).setVilleS(args[4]);
					this.saveItem(name,adresse,tel,horaires,lines[0]);
				}
				
				log.writeLog(new Date(), lines[0], "no_error", 0, counter,0);
			}
			counter++;
			
		}
		
		
		
	}

}
