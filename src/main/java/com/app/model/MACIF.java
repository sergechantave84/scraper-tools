package com.app.model;

import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "macif", schema = "scrap")
public class MACIF {

	@Id
	@SequenceGenerator(name = "scrap.macif_id_seq", sequenceName = "scrap.macif_id_seq", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "scrap.macif_id_seq")

	@Column(name = "id", updatable = false)
	private int id;

	@Column(name = "depsoumise", columnDefinition = "varchar")
	private String depSoumise;

	@Column(name = "nbagences", columnDefinition = "varchar")
	private String nbAgences;

	@Column(name = "nbagences_trouvees", columnDefinition = "varchar")
	private String nbAgencesTrouvees;

	@Column(name = "lien_agence", columnDefinition = "varchar")
	private String lienAgence;

	@Column(name = "nom_agence", columnDefinition = "varchar")
	private String nomAgence;

	@Column(name = "adresse", columnDefinition = "varchar")
	private String adresse;

	@Column(name = "tel", columnDefinition = "varchar")
	private String tel;

	@Column(name = "horaires", columnDefinition = "varchar")
	private String horaires;

	@Column(name = "agencehtm", columnDefinition = "varchar")
	private String agenceHtm;

	@Column(name = "annee_du_scrap", columnDefinition = "varchar(4) DEFAULT date_part('year',CURRENT_DATE)")
	private String anneeScrap;

	public MACIF() {
		LocalDate ld = new Date().toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
		anneeScrap = String.valueOf(ld.getYear() + 1);
	}

	public int getId() {
		return id;
	}

	public MACIF setId(int id) {
		this.id = id;
		return this;
	}

	public String getDepSoumise() {
		return depSoumise;
	}

	public MACIF setDepSoumise(String depSoumise) {
		this.depSoumise = depSoumise;
		return this;
	}

	public String getNbAgences() {
		return nbAgences;
	}

	public MACIF setNbAgences(String nbAgences) {
		this.nbAgences = nbAgences;
		return this;
	}

	public String getNbAgencesTrouvees() {
		return nbAgencesTrouvees;
	}

	public MACIF setNbAgencesTrouvees(String nbAgencesTrouvees) {
		this.nbAgencesTrouvees = nbAgencesTrouvees;
		return this;
	}

	public String getLienAgence() {
		return lienAgence;
	}

	public MACIF setLienAgence(String lienAgence) {
		this.lienAgence = lienAgence;
		return this;
	}

	public String getNomAgence() {
		return nomAgence;
	}

	public MACIF setNomAgence(String nomAgence) {
		this.nomAgence = nomAgence;
		return this;
	}

	public String getAdresse() {
		return adresse;

	}

	public MACIF setAdresse(String adresse) {
		this.adresse = adresse;
		return this;
	}

	public String getTel() {
		return tel;
	}

	public MACIF setTel(String tel) {
		this.tel = tel;
		return this;
	}

	public String getHoraires() {
		return horaires;
	}

	public MACIF setHoraires(String horaires) {
		this.horaires = horaires;
		return this;
	}

	public String getAgenceHtm() {
		return agenceHtm;
	}

	public MACIF setAgenceHtm(String agenceHtm) {
		this.agenceHtm = agenceHtm;
		return this;
	}

	public String getAnneeScrap() {
		return anneeScrap;
	}

	public MACIF setAnneeScrap(String anneeScrap) {
		this.anneeScrap = anneeScrap;
		return this;
	}

}
