package com.app.scrap;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import com.app.model.Adrea;
import com.app.model.Adrea_DOM;

@Repository
public interface RepAdreaDom extends JpaRepository<Adrea_DOM,Integer>, RepositoryCustom<Adrea_DOM> {
}