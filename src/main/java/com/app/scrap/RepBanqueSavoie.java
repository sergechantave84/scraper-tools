package com.app.scrap;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.app.model.BanqueSavoie;

@Repository
public interface RepBanqueSavoie extends JpaRepository<BanqueSavoie,Integer> ,RepositoryCustom<BanqueSavoie>{

}
