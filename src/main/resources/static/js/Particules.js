class Particules{
   
    constructor(canvas){
        
        this.blurry=true
        this.man=false
        this.colors=['255, 255, 255',
        '249, 102, 43',
        '19, 167, 107',
        '255, 228, 1']
        this.life=0;
        this.setting = {density: 20,
        particuleSize:1.02,
        startingX: canvas.width,
        startingY: canvas.height,
        gravity:.03,
        maxLife:100}
    }

    particlesInit(){
        //init origin
        this.x=(Math.random()*(this.setting.startingX-0)+0)
        this.y=(Math.random()*(this.setting.startingY-0)+0)
        let color=this.colors[~~(Math.random()*(this.colors.length))]
        this.rgbColor='rgb('+color+', .5'+')'
        //randomize x and y velocity
        this.vx=(Math.random()*(.5 -.05)+.05)
        this.vy=(Math.random()*(.5 -.05)+.05)
        return this;
    }

    particuleDraw(context,canvas){
        
        this.x += this.vx ;
        this.y -=this.vy; 
        this.life++
        if (this.life >= this.setting.maxLife) {
            delete this
        }
        context.beginPath();
        context.fillStyle=this.rgbColor
        context.arc(this.x, this.y, this.setting.particuleSize, 0, Math.PI*2, false); 
        context.closePath();
        context.fill();
    }
}