package com.app.model;

import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="action_loge_banque", schema="scrap")
public class ActionLogement {

	@Id
	@SequenceGenerator( name="scrap.action_koge_banque_id_seq",
	                    sequenceName = "scrap.action_koge_banque_id_seq",
	                    allocationSize = 1)
	@GeneratedValue( strategy = GenerationType.SEQUENCE,
	                 generator = "scrap.action_koge_banque_id_seq")
	@Column(name="id", updatable = false)
	private int id;
	
	@Column(name="nom_agence", columnDefinition = "varchar")
	private String nomAgence;
	
	@Column(name="adresse", columnDefinition = "varchar")
	private String adresse;
	
	@Column(name="horaies", columnDefinition = "varchar")
	private String horaires;
	
	@Column(name="tel", columnDefinition = "varchar")
	private String tel;
	
	@Column(name="depart_soumise", columnDefinition = "varchar")
	private String val;
	
	@Column(name="annee_scrap", columnDefinition = "varchar(4)")
	private String anneeScrap;
	
	public ActionLogement() {
		LocalDate ld= new Date().toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
		anneeScrap=String.valueOf(ld.getYear()+1);
	}

	public String getNomAgence() {
		return nomAgence;
	}

	public ActionLogement setNomAgence(String nomAgence) {
		this.nomAgence = nomAgence;
		return this;
	}

	public String getAdresse() {
		return adresse;
	}

	public ActionLogement setAdresse(String adresse) {
		this.adresse = adresse;
		return this;
	}

	public String getHoraires() {
		return horaires;
	}

	public ActionLogement setHoraires(String horaires) {
		this.horaires = horaires;
		return this;
	}

	public String getTel() {
		return tel;
	}

	public ActionLogement setTel(String tel) {
		this.tel = tel;
		return this;
	}

	public String getVal() {
		return val;
	}

	public ActionLogement setVal(String val) {
		this.val = val;
		return this;
	}
	
}
