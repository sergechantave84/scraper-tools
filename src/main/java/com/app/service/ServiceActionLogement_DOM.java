package com.app.service;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.SocketTimeoutException;
import java.text.Normalizer;
import java.util.Date;
import java.util.Scanner;

import org.hibernate.internal.build.AllowSysOut;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.app.model.ActionLogement;
import com.app.model.ActionLogement_DOM;
import com.app.scrap.RepActionLogement;
import com.app.scrap.RepActionLogement_DOM;
import com.app.utils.Parametres;
import com.app.utils.Utilitaires;

import fa.browserUtils.FactoryBrowser;
import fa.browserUtils.HtmlUnitBrowser;
import fa.log.LogApp.LogScrap;
import fa.log.config.LogConst;

@Service
public class ServiceActionLogement_DOM extends Utilitaires {

	@Autowired
	RepActionLogement_DOM rep;
	
	private HtmlUnitDriver hu;
	private String siteWeb="action_logement";
	private String ficVal="lien_actionLogement_dom.txt";
	
	
	private void saveItem(String adresse,String horaires, String nomAgence,String tel,String dep) {
		ActionLogement_DOM al= new ActionLogement_DOM();
		al.setAdresse(adresse).
		 setHoraires(horaires).
		 setNomAgence(nomAgence).
		 setTel(tel).
		 setVal(dep);
		rep.save(al);
	}
	
	public void setup() {
		FactoryBrowser factB = new FactoryBrowser();
		HtmlUnitBrowser hb = (HtmlUnitBrowser) factB.create("htmlUnit");
		hu = hb.setOptions(true, false, false, 12000).initBrowserDriver();
	}
	
	public void logout() {
		hu.quit();
	}
	
	public void scrapActionlogement() throws IOException {
		Scanner sc= new Scanner(new BufferedInputStream(new FileInputStream(new File(Parametres.REPERTOIRE_DATA_SOURCES+ficVal))));
		LogScrap log = new LogScrap("BanqueActLogeDomLog.txt", LogConst.TYPE_LOG_FILE);
		int currentLine = ((new File(LogConst.STORE_LOG_PATH + LogConst.PREFIX_LOG_FILE + "_BanqueActeLogeDomLog.txt"))
				.exists()) ? Integer.parseInt(log.readLog().getLastLineProcessed().split("\t")[4]) : 1;
		
		int counter=1;
		while(sc.hasNextLine()) {
			String[] lines=sc.nextLine().split("\t");
			if(counter>currentLine) {
				System.out.println(lines[1]);
				try {
					hu.get(lines[1]);
				} catch (Exception e) {
					if (e instanceof SocketTimeoutException) {

						this.setup();
						hu.get(lines[1]);
					}
				}
				
				String pageSource=hu.getPageSource();
				Document doc=Jsoup.parse(pageSource);
				Element root=doc.selectFirst("section[id^=\"node\"] > div > div.row");
				if(root!=null) {
					
					String nomAgence="";
					String adresse="";
					String horaires="";
					String tel="";
					
					Element nomAgenceElement =doc.selectFirst("body > div.main-container.container > div > section > h1");
					nomAgence=(nomAgenceElement!=null)? nomAgenceElement.text() :"";
					System.out.println("nomAgence "+ nomAgence);
					
					Element adresseElement= root.selectFirst("div:nth-child(1) > div.field.field-name-field-address");
					adresse=(adresseElement!=null)? adresseElement.text() :"";
					System.out.println("adresse "+adresse);
					
					Element telElement=root.selectFirst("div:nth-child(2) > a");
					tel=(telElement!=null) ? telElement.attr("href") :"";
					System.out.println("tel "+tel );

					Element horairesElement= root.selectFirst("div:nth-child(3) > div.field.field-name-field-horaire");
					horaires=(horairesElement!=null) ? horairesElement.text() :"";
					horaires=Normalizer.normalize(horaires, Normalizer.Form.NFD).replaceAll("[^\\p{ASCII}]","");
					System.out.println("horaires "+ horaires);

					this.saveItem(adresse, horaires, nomAgence,tel,lines[0]);
				}
	
				log.writeLog(new Date(), lines[0], "no_error", 1, counter,0);
			}
			counter++;
		}
	}
}
