package com.app.model;

import java.text.SimpleDateFormat;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="banque_nickel", schema="scrap")
public class Nickel {
	
	@Id
	@SequenceGenerator(name="scrap.banque_nickel_id_seq",
	                    sequenceName ="scrap.banque_nickel_id_seq",
	                     allocationSize=1)
    @GeneratedValue( strategy=GenerationType.SEQUENCE,
                     generator="scrap.banque_nickel_id_seq")
	  
	@Column(name="id",updatable=false)
	private int id;
	
	@Column(name="valeursoumis", length=500)
	private String valeurSoumis;
	
	@Column(name="nb_point_nickel", length=4)
	private String nbPointNickel;
	
	@Column(name="nomweb", length=500)
	private String nomWeb;
	
	@Column(name="adresse", length=500)
	private String adresse;
	
	@Column(name="tel", length=20)
	private String tel;
	
	@Column(name="lien_point_nickel", length=500)
	private String lienPointNickel;
	
	@Column(name="horaireshtm", columnDefinition="varchar")
	private String horairesHtm;
	
	@Column(name="horairestxt", length=500)
	private String horairesTxt;
	
	@Column(name="dab", length=4)
	private String dab;
	
	@Column(name="services", length=500)
	private String services;
	
	@Column(name="agencehtm", columnDefinition="varchar")
	private String pointHtm;
	
	@Column(name="annee_du_scrap" ,columnDefinition="varchar(4) DEFAULT date_part('year',CURRENT_DATE)")
	private String anneeScrap;

	public Nickel() {
		SimpleDateFormat f=null;
		Date d=new Date();
		f=new SimpleDateFormat("yyyy");
		anneeScrap=f.format(d).toString();
	}

	public int getId() {
		return id;
	}

	public Nickel setId(int id) {
		this.id = id;
		return this;
	}

	public String getValeurSoumis() {
		return valeurSoumis;
	}

	public Nickel setValeurSoumis(String valeurSoumis) {
		this.valeurSoumis = valeurSoumis;
		return this;
	}

	public String getNbPointNickel() {
		return nbPointNickel;
	}

	public Nickel setNbPointNickel(String nbPointNickel) {
		this.nbPointNickel = nbPointNickel;
		return this;
	}

	public String getNomWeb() {
		return nomWeb;
	}

	public Nickel setNomWeb(String nomWeb) {
		this.nomWeb = nomWeb;
		return this;
	}

	public String getAdresse() {
		return adresse;
	}

	public Nickel setAdresse(String adresse) {
		this.adresse = adresse;
		return this;
	}

	public String getTel() {
		return tel;
	}

	public Nickel setTel(String tel) {
		this.tel = tel;
		return this;
	}

	public String getLienPointNickel() {
		return lienPointNickel;
	}

	public Nickel setLienPointNickel(String lienPointNickel) {
		this.lienPointNickel = lienPointNickel;
		return this;
	}

	public String getHorairesHtm() {
		return horairesHtm;
	}

	public Nickel setHorairesHtm(String horairesHtm) {
		this.horairesHtm = horairesHtm;
		return this;
	}

	public String getHorairesTxt() {
		return horairesTxt;
	}

	public Nickel setHorairesTxt(String horairesTxt) {
		this.horairesTxt = horairesTxt;
		return this;
	}

	public String getDab() {
		return dab;
	}

	public Nickel setDab(String dab) {
		this.dab = dab;
		return this;
	}

	public String getServices() {
		return services;
	}

	public Nickel setServices(String services) {
		this.services = services;
		return this;
	}

	public String getPointHtm() {
		return pointHtm;
	}

	public Nickel setPointHtm(String pointHtm) {
		this.pointHtm = pointHtm;
		return this;
	}

	public String getAnneeScrap() {
		return anneeScrap;
	}

	public Nickel setAnneeScrap(String anneeScrap) {
		this.anneeScrap = anneeScrap;
		return this;
	}

}
