package com.app.scrap;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.app.model.EOVI;

@Repository
public interface RepEovi extends JpaRepository<EOVI,Integer>,RepositoryCustom<EOVI> {

}
