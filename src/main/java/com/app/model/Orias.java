package com.app.model;


import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "orias", schema = "scrap")
public class Orias {

	@Id
	@SequenceGenerator(name = "scrap.orias_id_seq", sequenceName = "scrap.orias_id_seq", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "scrap.orias_id_seq")
	@Column(name = "id", nullable = false, unique = true)
	private int id;

	@Column(name = "agent", columnDefinition = "varchar")
	private String agent;

	@Column(name = "mandant_IA", columnDefinition = "varchar")
	private String mandatIA;

	@Column(name = "ia_encaissement_fond", columnDefinition = "varchar")
	private String iaEnciassementFond;

	@Column(name = "IA_inscrit_Ou_suppr", columnDefinition = "varchar")
	private String iaInscritOrSuppr;

	@Column(name = "nature_Activite_IA", columnDefinition = "varchar")
	private String natureActiviteIA;

	@Column(name = "nomAgent_IA", columnDefinition = "varchar")
	private String nomAgentIA;

	@Column(name = "agent_date_suppr", columnDefinition = "varchar")
	private String agentDateSuppr;

	@Column(name = "agent_date_inscription", columnDefinition = "varchar")
	private String agentDateInscription;

	@Column(name = "AGTPSI", columnDefinition = "varchar")
	private String AGTPSI;

	@Column(name = "AGTPSI_Date_Suppr", columnDefinition = "varchar")
	private String AGTPSIDateSuppr;

	@Column(name = "AGTPSI_Date_Insript", columnDefinition = "varchar")
	private String AGTPSIDateInsript;

	@Column(name = "ALPSI", columnDefinition = "varchar")
	private String ALPSI;

	@Column(name = "alpsi_Date_Suppr", columnDefinition = "varchar")
	private String alpsiDateSuppr;

	@Column(name = "alpsi_Date_Insript", columnDefinition = "varchar")
	private String alpsiDateInsript;

	@Column(name = "CIF", columnDefinition = "varchar")
	private String CIF;

	@Column(name = "cif_Date_Suppr", columnDefinition = "varchar")
	private String cifDateSuppr;

	@Column(name = "cif_Date_Insript", columnDefinition = "varchar")
	private String cifDateInsript;

	@Column(name = "CIP", columnDefinition = "varchar")
	private String CIP;

	@Column(name = "cip_date_suppr", columnDefinition = "varchar")
	private String cipDateSuppr;

	@Column(name = "cip_Date_Insript", columnDefinition = "varchar")
	private String cipDateInsript;

	@Column(name = "COBSP", columnDefinition = "varchar")
	private String COBSP;

	@Column(name = "cobsp_Date_Suppr", columnDefinition = "varchar")
	private String cobspDateSuppr;

	@Column(name = "cobsp_Date_Insript", columnDefinition = "varchar")
	private String cobspDateInsript;

	@Column(name = "coutier", columnDefinition = "varchar")
	private String coutier;

	@Column(name = "courtier_Date_Suppr", columnDefinition = "varchar")
	private String courtierDateSuppr;

	@Column(name = "courtier_Date_Insript", columnDefinition = "varchar")
	private String courtierDateInsript;

	@Column(name = "COA_encaissement_Fond", columnDefinition = "varchar")
	private String COA_encaissementFond;

	@Column(name = "COA_inscrit_ou_suppr", columnDefinition = "varchar")
	private String COA_inscritOrsuppr;

	@Column(name = "IEA", columnDefinition = "varchar")
	private String IEA;
	
	@Column(name = "IEA_Date_Supprr", columnDefinition = "varchar")
	private String IEADateSuppr;
	
	@Column(name = "IEA_Date_Insriptr", columnDefinition = "varchar")
	private String IEADateInsript;
	
	@Column(name = "IEA_encaissement_Fond", columnDefinition = "varchar")
	private String IEA_encaissementFond; 
	
	@Column(name = "IEA_inscrit_Or_suppr", columnDefinition = "varchar")
	private String IEAinscritOrsuppr;
	
	@Column(name = "IFP", columnDefinition = "varchar")
	private String IFP;
	
	@Column(name = "ifp_Date_Suppr", columnDefinition = "varchar")
    private String  ifpDateSuppr;
	
	@Column(name = "ifp_Date_Insript", columnDefinition = "varchar")
    private String ifpDateInsript;
	
	@Column(name = "MAL", columnDefinition = "varchar")
	private String  MAL;
	
	@Column(name = "mal_Date_Suppr", columnDefinition = "varchar")
    private String malDateSuppr; 
    
	@Column(name = "mal_Date_Insript", columnDefinition = "varchar")
    private String malDateInsript;
	
	@Column(name = "mandataire", columnDefinition = "varchar")
	private String mandataire;
	
	@Column(name = "mandataire_Date_Supp", columnDefinition = "varchar")
    private String mandataire_Date_Suppr;
    
	@Column(name = "mandataire_Date_Insriptt", columnDefinition = "varchar")
    private String mandataireDateInsript;
	
	@Column(name = "mandataire_encaissementFond", columnDefinition = "varchar")
    private String mandataire_encaissementFond;
	
	@Column(name = "mandataire_inscrit_ou_suppr", columnDefinition = "varchar")
    private String mandataireinscritOrsuppr;
	
	@Column(name = "MIA", columnDefinition = "varchar")
	private String  MIA;
	
	@Column(name = "mia_Date_Suppr", columnDefinition = "varchar")
    private String  miaDateSuppr;
    
	@Column(name = "mia_Date_Insript", columnDefinition = "varchar")
    private String miaDateInsript;
    
	@Column(name = "MIA_encaissement_Fond", columnDefinition = "varchar")
    private String MIA_encaissementFond;
    
	@Column(name = "MIA_inscrit_ou_suppr", columnDefinition = "varchar")
    private String MIAinscritOrsuppr;
	
	@Column(name = "MIOBSP", columnDefinition = "varchar")
	private String  MIOBSP;
	
	@Column(name = "miobsp_Date_Suppr", columnDefinition = "varchar")
	private String  miobspDateSuppr;
	
	@Column(name = "miobsp_Date_Insript", columnDefinition = "varchar")
	private String miobspDateInsript;
	
	@Column(name = "MOBSP", columnDefinition = "varchar")
	private String MOBSP;
	
	@Column(name = "mobsp_Date_Suppr", columnDefinition = "varchar")
    private String mobspDateSuppr; 
	
	@Column(name = "mobsp_Date_Insript", columnDefinition = "varchar")
	private String mobspDateInsript;
	
	@Column(name = "MOBSPL", columnDefinition = "varchar")
	private String  MOBSPL;
	
	@Column(name = "mobspl_Date_Suppr", columnDefinition = "varchar")
    private String mobsplDateSuppr; 
	
	@Column(name = "mobspl_Date_Insript", columnDefinition = "varchar")
    private String mobsplDateInsript;
	
	@Column(name="annee_scrap",columnDefinition = "varchar")
	private String anneeScrap;

	@Column(name="denomination_social",columnDefinition = "varchar")
	private String denominationSocial;
	
	@Column(name="nom_commercial",columnDefinition = "varchar")
	private String nomCommercial; 
	
	@Column(name="RCS",columnDefinition = "varchar")
	private String RCS;
	
	@Column(name="siren",columnDefinition = "varchar")
	private String siren;
	
	@Column(name="code_naf",columnDefinition = "varchar")
	private String codeNAF;
	
	@Column(name="num_immatr",columnDefinition = "varchar")
	private String numImmatr;
	
	@Column(name="statut",columnDefinition = "varchar")
	private String statut;
	
	@Column(name="forme_juridique",columnDefinition = "varchar")
	private String formeJuridique;
	
	@Column(name="adresse",columnDefinition = "varchar")
	private String adresse;
	
	@Column(name="reseauORIAS1",columnDefinition = "varchar")
	private String reseauORIAS1;
	
	@Column(name="reseauORIAS2",columnDefinition = "varchar")
	private String reseauORIAS2;
	
	@Column(name="reseauORIAS3",columnDefinition = "varchar")
	private String reseauORIAS3;
	
	@Column(name="reseauORIAS4",columnDefinition = "varchar")
	private String reseauORIAS4;
	
	@Column(name="reseauORIAS5",columnDefinition = "varchar")
	private String reseauORIAS5; 
	
	@Column(name="reseauORIAS6",columnDefinition = "varchar")
	private String reseauORIAS6;
	
	@Column(name="reseauORIAS7",columnDefinition = "varchar")
	private String reseauORIAS7;
	
	@Column(name="nom_ia",columnDefinition = "varchar")
	private String nomIA;
	public Orias() {
		LocalDate dt=new Date().toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
		anneeScrap=String.valueOf(dt.getYear()+1);
	}
	public String getAgent() {
		return agent;
	}

	public Orias setAgent(String agent) {
		this.agent = agent;
		return this;
	}

	public String getMandatIA() {
		return mandatIA;
	}

	public Orias setMandatIA(String mandatIA) {
		this.mandatIA = mandatIA;
		return this;
	}

	public String getIaEnciassementFond() {
		return iaEnciassementFond;
	}

	public Orias setIaEnciassementFond(String iaEnciassementFond) {
		this.iaEnciassementFond = iaEnciassementFond;
		return this;
	}

	public String getIaInscritOrSuppr() {
		return iaInscritOrSuppr;
	}

	public Orias setIaInscritOrSuppr(String iaInscritOrSuppr) {
		this.iaInscritOrSuppr = iaInscritOrSuppr;
		return this;
	}

	public String getNatureActiviteIA() {
		return natureActiviteIA;
	}

	public Orias setNatureActiviteIA(String natureActiviteIA) {
		this.natureActiviteIA = natureActiviteIA;
		return this;
	}

	public String getNomAgentIA() {
		return nomAgentIA;
	}

	public Orias setNomAgentIA(String nomAgentIA) {
		this.nomAgentIA = nomAgentIA;
		return this;
	}

	public String getAgentDateSuppr() {
		return agentDateSuppr;
	}

	public Orias setAgentDateSuppr(String agentDateSuppr) {
		this.agentDateSuppr = agentDateSuppr;
		return this;
	}

	public String getAgentDateInscription() {
		return agentDateInscription;
	}

	public Orias setAgentDateInscription(String agentDateInscription) {
		this.agentDateInscription = agentDateInscription;
		return this;
	}

	public String getAGTPSI() {
		return AGTPSI;
	}

	public Orias setAGTPSI(String aGTPSI) {
		AGTPSI = aGTPSI;
		return this;
	}

	public String getAGTPSIDateSuppr() {
		return AGTPSIDateSuppr;
	}

	public Orias setAGTPSIDateSuppr(String aGTPSIDateSuppr) {
		AGTPSIDateSuppr = aGTPSIDateSuppr;
		return this;
	}

	public String getAGTPSIDateInsript() {
		return AGTPSIDateInsript;
	}

	public Orias setAGTPSIDateInsript(String aGTPSIDateInsript) {
		AGTPSIDateInsript = aGTPSIDateInsript;
		return this;
	}

	public String getALPSI() {
		return ALPSI;
	}

	public Orias setALPSI(String aLPSI) {
		ALPSI = aLPSI;
		return this;
	}

	public String getAlpsiDateSuppr() {
		return alpsiDateSuppr;
	}

	public Orias setAlpsiDateSuppr(String alpsiDateSuppr) {
		this.alpsiDateSuppr = alpsiDateSuppr;
		return this;
	}

	public String getAlpsiDateInsript() {
		return alpsiDateInsript;
	}

	public Orias setAlpsiDateInsript(String alpsiDateInsript) {
		this.alpsiDateInsript = alpsiDateInsript;
		return this;
	}

	public String getCIF() {
		return CIF;
	}

	public Orias setCIF(String cIF) {
		CIF = cIF;
		return this;
	}

	public String getCifDateSuppr() {
		return cifDateSuppr;
	}

	public Orias setCifDateSuppr(String cifDateSuppr) {
		this.cifDateSuppr = cifDateSuppr;
		return this;
	}

	public String getCifDateInsript() {
		return cifDateInsript;
	}

	public Orias setCifDateInsript(String cifDateInsript) {
		this.cifDateInsript = cifDateInsript;
		return this;
	}

	public String getCIP() {
		return CIP;
	}

	public Orias setCIP(String cIP) {
		CIP = cIP;
		return this;
	}

	public String getCipDateSuppr() {
		return cipDateSuppr;
	}

	public Orias setCipDateSuppr(String cipDateSuppr) {
		this.cipDateSuppr = cipDateSuppr;
		return this;
	}

	public String getCipDateInsript() {
		return cipDateInsript;
	}

	public Orias setCipDateInsript(String cipDateInsript) {
		this.cipDateInsript = cipDateInsript;
		return this;
	}

	public String getCOBSP() {
		return COBSP;
	}

	public Orias setCOBSP(String cOBSP) {
		COBSP = cOBSP;
		return this;
	}

	public String getCobspDateSuppr() {
		return cobspDateSuppr;
	}

	public Orias setCobspDateSuppr(String cobspDateSuppr) {
		this.cobspDateSuppr = cobspDateSuppr;
		return this;
	}

	public String getCobspDateInsript() {
		return cobspDateInsript;
	}

	public Orias setCobspDateInsript(String cobspDateInsript) {
		this.cobspDateInsript = cobspDateInsript;
		return this;
	}

	public String getCoutier() {
		return coutier;
	}

	public Orias setCoutier(String coutier) {
		this.coutier = coutier;
		return this;
	}

	public String getCourtierDateSuppr() {
		return courtierDateSuppr;
	}

	public Orias setCourtierDateSuppr(String courtierDateSuppr) {
		this.courtierDateSuppr = courtierDateSuppr;
		return this;
	}

	public String getCourtierDateInsript() {
		return courtierDateInsript;
	}

	public Orias setCourtierDateInsript(String courtierDateInsript) {
		this.courtierDateInsript = courtierDateInsript;
		return this;
	}

	public String getCOA_encaissementFond() {
		return COA_encaissementFond;
	}

	public Orias setCOA_encaissementFond(String cOA_encaissementFond) {
		COA_encaissementFond = cOA_encaissementFond;
		return this;
	}

	public String getCOA_inscritOrsuppr() {
		return COA_inscritOrsuppr;
	}

	public Orias setCOA_inscritOrsuppr(String cOA_inscritOrsuppr) {
		COA_inscritOrsuppr = cOA_inscritOrsuppr;
		return this;
	}

	public String getIEA() {
		return IEA;
	}

	public Orias setIEA(String iEA) {
		IEA = iEA;
		return this;
	}

	public String getIEADateSuppr() {
		return IEADateSuppr;
	}

	public Orias setIEADateSuppr(String iEADateSuppr) {
		IEADateSuppr = iEADateSuppr;
		return this;
	}

	public String getIEADateInsript() {
		return IEADateInsript;
	}

	public Orias setIEADateInsript(String iEADateInsript) {
		IEADateInsript = iEADateInsript;
		return this;
	}

	public String getIEA_encaissementFond() {
		return IEA_encaissementFond;
	}

	public Orias setIEA_encaissementFond(String iEA_encaissementFond) {
		IEA_encaissementFond = iEA_encaissementFond;
		return this;
	}

	public String getIEAinscritOrsuppr() {
		return IEAinscritOrsuppr;
	}

	public Orias setIEAinscritOrsuppr(String iEAinscritOrsuppr) {
		IEAinscritOrsuppr = iEAinscritOrsuppr;
		return this;
	}

	public String getIFP() {
		return IFP;
	}

	public Orias setIFP(String iFP) {
		IFP = iFP;
		return this;
	}

	public String getIfpDateSuppr() {
		return ifpDateSuppr;
	}

	public Orias setIfpDateSuppr(String ifpDateSuppr) {
		this.ifpDateSuppr = ifpDateSuppr;
		return this;
	}

	public String getIfpDateInsript() {
		return ifpDateInsript;
	}

	public Orias setIfpDateInsript(String ifpDateInsript) {
		this.ifpDateInsript = ifpDateInsript;
		return this;
	}

	public String getMAL() {
		return MAL;
	}

	public Orias setMAL(String mAL) {
		MAL = mAL;
		return this;
	}

	public String getMalDateSuppr() {
		return malDateSuppr;
	}

	public Orias setMalDateSuppr(String malDateSuppr) {
		this.malDateSuppr = malDateSuppr;
		return this;
	}

	public String getMalDateInsript() {
		return malDateInsript;
	}

	public Orias setMalDateInsript(String malDateInsript) {
		this.malDateInsript = malDateInsript;
		return this;
	}

	public String getMandataire() {
		return mandataire;
	}

	public Orias setMandataire(String mandataire) {
		this.mandataire = mandataire;
		return this;
	}

	public String getMandataire_Date_Suppr() {
		return mandataire_Date_Suppr;
	}

	public Orias setMandataire_Date_Suppr(String mandataire_Date_Suppr) {
		this.mandataire_Date_Suppr = mandataire_Date_Suppr;
		return this;
	}

	public String getMandataireDateInsript() {
		return mandataireDateInsript;
	}

	public Orias setMandataireDateInsript(String mandataireDateInsript) {
		this.mandataireDateInsript = mandataireDateInsript;
		return this;
	}

	public String getMandataire_encaissementFond() {
		return mandataire_encaissementFond;
	}

	public Orias setMandataire_encaissementFond(String mandataire_encaissementFond) {
		this.mandataire_encaissementFond = mandataire_encaissementFond;
		return this;
	}

	public String getMandataireinscritOrsuppr() {
		return mandataireinscritOrsuppr;
	}

	public Orias setMandataireinscritOrsuppr(String mandataireinscritOrsuppr) {
		this.mandataireinscritOrsuppr = mandataireinscritOrsuppr;
		return this;
	}

	public String getMIA() {
		return MIA;
	}

	public Orias setMIA(String mIA) {
		MIA = mIA;
		return this;
	}

	public String getMiaDateSuppr() {
		return miaDateSuppr;
	}

	public Orias setMiaDateSuppr(String miaDateSuppr) {
		this.miaDateSuppr = miaDateSuppr;
		return this;
	}

	public String getMiaDateInsript() {
		return miaDateInsript;
	}

	public Orias setMiaDateInsript(String miaDateInsript) {
		this.miaDateInsript = miaDateInsript;
		return this;
	}

	public String getMIA_encaissementFond() {
		return MIA_encaissementFond;
	}

	public Orias setMIA_encaissementFond(String mIA_encaissementFond) {
		MIA_encaissementFond = mIA_encaissementFond;
		return this;
	}

	public String getMIAinscritOrsuppr() {
		return MIAinscritOrsuppr;
	}

	public Orias setMIAinscritOrsuppr(String mIAinscritOrsuppr) {
		MIAinscritOrsuppr = mIAinscritOrsuppr;
		return this;
	}

	public String getMIOBSP() {
		return MIOBSP;
	}

	public Orias setMIOBSP(String mIOBSP) {
		MIOBSP = mIOBSP;
		return this;
	}

	public String getMiobspDateSuppr() {
		return miobspDateSuppr;
	}

	public Orias setMiobspDateSuppr(String miobspDateSuppr) {
		this.miobspDateSuppr = miobspDateSuppr;
		return this;
	}

	public String getMiobspDateInsript() {
		return miobspDateInsript;
	}

	public Orias setMiobspDateInsript(String miobspDateInsript) {
		this.miobspDateInsript = miobspDateInsript;
		return this;
	}

	public String getMOBSP() {
		return MOBSP;
	}

	public Orias setMOBSP(String mOBSP) {
		MOBSP = mOBSP;
		return this;
	}

	public String getMobspDateSuppr() {
		return mobspDateSuppr;
	}

	public Orias setMobspDateSuppr(String mobspDateSuppr) {
		this.mobspDateSuppr = mobspDateSuppr;
		return this;
	}

	public String getMobspDateInsript() {
		return mobspDateInsript;
	}

	public Orias setMobspDateInsript(String mobspDateInsript) {
		this.mobspDateInsript = mobspDateInsript;
		return this;
	}

	public String getMOBSPL() {
		return MOBSPL;
	}

	public Orias setMOBSPL(String mOBSPL) {
		MOBSPL = mOBSPL;
		return this;
	}

	public String getMobsplDateSuppr() {
		return mobsplDateSuppr;
	}

	public Orias setMobsplDateSuppr(String mobsplDateSuppr) {
		this.mobsplDateSuppr = mobsplDateSuppr;
		return this;
	}

	public String getMobsplDateInsript() {
		return mobsplDateInsript;
	}

	public Orias setMobsplDateInsript(String mobsplDateInsript) {
		this.mobsplDateInsript = mobsplDateInsript;
		return this;
	}

	public String getAnneeScrap() {
		return anneeScrap;
	}

	public Orias setAnneeScrap(String anneeScrap) {
		this.anneeScrap = anneeScrap;
		return this;
	}
	public String getDenominationSocial() {
		return denominationSocial;
	}
	public Orias setDenominationSocial(String denominationSocial) {
		this.denominationSocial = denominationSocial;
		return this;
	}
	public String getNomCommercial() {
		return nomCommercial;
	}
	public Orias setNomCommercial(String nomCommercial) {
		this.nomCommercial = nomCommercial;
		return this;
	}
	public String getRCS() {
		return RCS;
	}
	public Orias setRCS(String rCS) {
		RCS = rCS;
		return this;
	}
	public String getSiren() {
		return siren;
	}
	public Orias setSiren(String siren) {
		this.siren = siren;
		return this;
	}
	public String getCodeNAF() {
		return codeNAF;
	}
	public Orias setCodeNAF(String codeNAF) {
		this.codeNAF = codeNAF;
		return this;
	}
	public String getNumImmatr() {
		return numImmatr;
	}
	public Orias setNumImmatr(String numImmatr) {
		this.numImmatr = numImmatr;
		return this;
	}
	public String getStatut() {
		return statut;
	}
	public Orias setStatut(String statut) {
		this.statut = statut;
		return this;
	}
	public String getFormeJuridique() {
		return formeJuridique;
	}
	public Orias setFormeJuridique(String formeJuridique) {
		this.formeJuridique = formeJuridique;
		return this;
	}
	public String getAdresse() {
		return adresse;
	}
	public Orias setAdresse(String adresse) {
		this.adresse = adresse;
		return this;
	}
	public String getReseauORIAS1() {
		return reseauORIAS1;
	}
	public Orias setReseauORIAS1(String reseauORIAS1) {
		this.reseauORIAS1 = reseauORIAS1;
		return this;
	}
	public String getReseauORIAS2() {
		return reseauORIAS2;
	}
	public Orias setReseauORIAS2(String reseauORIAS2) {
		this.reseauORIAS2 = reseauORIAS2;
		return this;
	}
	public String getReseauORIAS3() {
		return reseauORIAS3;
	}
	public Orias setReseauORIAS3(String reseauORIAS3) {
		this.reseauORIAS3 = reseauORIAS3;
		return this;
	}
	public String getReseauORIAS4() {
		return reseauORIAS4;
	}
	public Orias setReseauORIAS4(String reseauORIAS4) {
		this.reseauORIAS4 = reseauORIAS4;
		return this;
	}
	public String getReseauORIAS5() {
		return reseauORIAS5;
	}
	public Orias setReseauORIAS5(String reseauORIAS5) {
		this.reseauORIAS5 = reseauORIAS5;
		return this;
	}
	public String getReseauORIAS6() {
		return reseauORIAS6;
	}
	public Orias setReseauORIAS6(String reseauORIAS6) {
		this.reseauORIAS6 = reseauORIAS6;
		return this;
	}
	public String getReseauORIAS7() {
		return reseauORIAS7;
	}
	public Orias setReseauORIAS7(String reseauORIAS7) {
		this.reseauORIAS7 = reseauORIAS7;
		return this;
	}
	public String getNomIA() {
		return nomIA;
	}
	public Orias setNomIA(String nomIA) {
		this.nomIA = nomIA;
		return this;
	}

}
