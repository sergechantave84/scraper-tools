package com.app.service;

import java.io.BufferedInputStream;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;

import java.net.SocketTimeoutException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Scanner;

import org.json.JSONArray;
import org.json.JSONObject;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.openqa.selenium.By;
import org.openqa.selenium.NoAlertPresentException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.app.model.BP;
import com.app.scrap.RepBP;

import com.app.utils.Parametres;
import com.app.utils.Utilitaires;

import fa.browserUtils.FactoryBrowser;
import fa.browserUtils.HtmlUnitBrowser;
import fa.log.LogApp.LogScrap;
import fa.log.config.LogConst;

@Service
public class ServiceBP extends Utilitaires {

	// private static String driverPath = "C:\\ChromeDriver\\";
	private List<BP> bpList = new ArrayList<>();

	private String nomSITE = "BP";
	private String ficval = "lien_bp";
	private String dossierRESU = Parametres.REPERTOIRE_RESULT + "\\BQ\\" + nomSITE;
	private String url_accueil = "https://agences.banquepopulaire.fr";
	private String fichier_valeurs;
	private String codePostale;

	// private String ligne;

	private int NbAgences;
	// private int numreponse = 1;
	private int nbRecuperes;
	private int num_ligneficval;
	private int parcoursficval;

	private static HtmlUnitDriver htmlUnitDriver;
	private BufferedWriter sortie;
	private BufferedWriter sortie_log;

	private Date Temps_Fin;

	@Autowired
	private RepBP rep;

	public BP saveData(BP t) {

		return rep.save(t);

	}

	public List<BP> saveDataAll(List<BP> t) {

		return rep.saveAll(t);

	}

	public List<BP> getElmnt() {

		return rep.findAll();
	}

	public BP getById(int id) {

		return rep.findById(id).orElse(null);
	}

	public String deleteProduct(int id) {
		try {
			rep.deleteById(id);

		} catch (Exception e) {
			e.printStackTrace();
		}

		return "removed";
	}

	public BP updateProduct(BP t) {
		// BPODO Auto-generated method stub
		return null;
	}

	public BP setData(String codePostale, String total, String nbAgences, String enseigne, String nomWeb,
			String adresse, String tel, String fax, String horairesHtm, String horairesBPxt, String services,
			String dab, String agenceHtm) {
		BP bp = new BP();
		bp.setCodePostale(codePostale).setTotal(total).setNbAgences(nbAgences).setEnseigne(enseigne).setNomWeb(nomWeb)
				.setAdresse(adresse).setTel(tel).setFax(fax).setHorairesHtm(horairesHtm).setHorairesTxt(horairesBPxt)
				.setServices(services).setDab(dab).setAgenceHtm(agenceHtm);
		this.saveData(bp);
		return bp;
	}

	public List<BP> showFirstRows() {
		List<BP> a = rep.getFirstRow(100, BP.class);
		return a;
	}

	public List<BP> showLastRows() {
		List<BP> a = rep.getLastRow(100, BP.class);
		return a;
	}

	public void setUp() throws Exception {

		FactoryBrowser fact = new FactoryBrowser();
		HtmlUnitBrowser hB = (HtmlUnitBrowser) fact.create("htmlUnit");
		htmlUnitDriver = hB.setOptions(false, false, false, 60000).initBrowserDriver();

	}

	public List<BP> open_BP_byselenium() throws IOException, InterruptedException {

		fichier_valeurs = Parametres.REPERTOIRE_DATA_SOURCES + ficval + ".txt";
		Scanner sc = new Scanner(new BufferedInputStream(new FileInputStream(new File(fichier_valeurs))));

		if (!new File(dossierRESU).exists()) {
			new File(dossierRESU).mkdirs();
		}
		fichier = new File(dossierRESU + "\\" + "RESULTATS_" + nomSITE + "_" + System.currentTimeMillis() + ".txt");
		LogScrap log = new LogScrap("BanqueBP.txt", LogConst.TYPE_LOG_FILE);
		num_ligneficval = ((new File(LogConst.STORE_LOG_PATH + LogConst.PREFIX_LOG_FILE + "_BanqueBP.txt")).exists())
				? Integer.parseInt(log.readLog().getLastLineProcessed().split("\\t")[4])
				: 1;
		
		
		parcoursficval = 1;
		while (sc.hasNextLine()) {
			String line = sc.nextLine();
			String[] fic_valeurs = line.split("\\t");
			if (parcoursficval >= num_ligneficval) {
				nbRecuperes = 0;
				codePostale = fic_valeurs[0];

				String url = url_accueil + fic_valeurs[1];
				System.out.println("url_accueil : " + url_accueil);
				try {
					htmlUnitDriver.get(url);

				} catch (Exception e) {
					if (e instanceof SocketTimeoutException) {
						htmlUnitDriver.get(url);

					}
				}

				Thread.sleep(10000);
				String pageSource = "";
				Document doc = null;
				List<String> agencyLinks = new ArrayList<>();
				pageSource = htmlUnitDriver.getPageSource();
				doc = Jsoup.parse(pageSource);

				Element rootJElement = doc.select(
						"body > div.em-page > main > div.em-results-wrapper > div > div.em-results__list-wrapper > div")
						.first();
				Elements agencyLinksJElement = rootJElement.select("ul > li");
				
				if (!agencyLinks.isEmpty()) agencyLinks.clear();
				
				for (Element elmntTmp : agencyLinksJElement) {
					Element hrefElement = elmntTmp.selectFirst("h2>a");
					agencyLinks.add(hrefElement.attr("href"));
				}
				
				NbAgences = agencyLinks.size();
				System.out.println("NbAgences : " + NbAgences);
				nbRecuperes = 0;

				for (String strTmp : agencyLinks) {
					try {
						htmlUnitDriver.get("https://agences.banquepopulaire.fr" + strTmp);

					} catch (Exception e) {
						if (e instanceof SocketTimeoutException) {
							htmlUnitDriver.get("https://agences.banquepopulaire.fr" + strTmp);

						}
					}

					Thread.sleep(1000);
					String agences_HTM = "", nom_agence = "", adresse = "", tel = "", fax = "", HorairesHTM = "",
							HorairesTXT = "", Services = "", enseigne = "";
					int total = 0, DAB = 0;
					total = NbAgences;
					pageSource = htmlUnitDriver.getPageSource();
					doc = Jsoup.parse(pageSource);

					// TODO change
					Element root2JElement = doc.select("div.em-page > main > div.em-details > div.em-details__poi-card")
							.first();
					agences_HTM = root2JElement.outerHtml().trim().replaceAll("\\s{1,}", "");
					agences_HTM = this.normalize(agences_HTM);

					// retrieve agency name
					Element agencyNameJElement = root2JElement.select("h2.em-details__label").first();
					nom_agence = (agencyNameJElement != null) ? this.normalize(agencyNameJElement.text()) : "";
					System.out.println("nom agence : " + nom_agence);

					// retrieve agency address
					Element agencyAddressJElement = root2JElement
							.select("div.em-details__info>div:nth-child(1)>div.em-details__address>div").first();
					adresse = (agencyAddressJElement != null) ? this.normalize(agencyAddressJElement.text().replaceAll(",","").toUpperCase()) : "";
					System.out.println("adresse : " + adresse);

					// retrieve agency tel
					Element agencyTelJElement = root2JElement.select("div.em-details__info>div:nth-child(2)>a").first();
					tel = (agencyTelJElement != null) ? agencyTelJElement.attr("href").replaceAll("[^0-9]", "") : "";
					System.out.println("telephone : " + tel);

					// retrieve agency fax
					Element agencyFaxJElement = root2JElement.select("div.em-details__info>div:nth-child(2)>div>span")
							.first();
					fax = (agencyFaxJElement != null) ? agencyFaxJElement.text().replaceAll("[^0-9]", "") : "";
					System.out.println("fax : " + fax);

					// retrieve agency schedule
					/*Element agencyScheduleJElement = doc.selectFirst("head > script[type=\"application/ld+json\"]");
					System.out.println(agencyScheduleJElement);
					String agencyInfo = agencyScheduleJElement.html();*/
					try {
						/*agencyInfo = agencyInfo.replaceAll("//\\<\\!\\[CDATA\\[", "");
						agencyInfo = agencyInfo.replaceAll("//\\]{1,}>", "");
						agencyInfo = agencyInfo.replaceAll("\\s{2,}", "");
						HorairesHTM = agencyInfo;
						JSONObject jsonRoot = new JSONObject(agencyInfo);
						JSONArray jsons = jsonRoot.getJSONArray("openingHoursSpecification");
						int jl = jsons.length();
						for (int i = 0; i < jl; i++) {
							JSONObject json = jsons.getJSONObject(i);

							String day = json.getString("dayOfWeek");
							day = day.replaceAll("http://schema.org/", "").trim();
							switch (day) {
							case "Monday": {
								day = "lundi";
								break;
							}
							case "Tuesday": {
								day = "mardi";
								break;
							}
							case "Wednesday": {
								day = "mercredi";
								break;
							}
							case "Thursday": {
								day = "jeudi";
								break;
							}
							case "Friday": {
								day = "vendredi";
								break;
							}
							case "Saturday": {
								day = "samedi";
								break;
							}
							case "Sunday": {
								day = "dimanche";
								break;
							}

							}
							String hours = json.getString("opens") + "-" + json.getString("closes");
							HorairesTXT += day + " " + hours + " ";

						}*/
						
						
						// ADD BY NANTE
						Element emPage = doc.selectFirst("div.em-page");
						Element script = emPage.nextElementSibling();
						/*Elements agencySheduleJElement = root2JElement.select(
								"div.em-details__horaires-bloc > div > div.em-graphical-schedules > div > ul.graphicalSchedules__days > li");*/
						
						String[] splitScripts = (script.html().toString()).split("day1");
						
						String days = splitScripts[1].replaceAll("/", "").replaceAll("\"", "").replace("day2:", "").replace("day3:", "").replace("day4:", "").replace("day5:", "").replace("day6:", "");
					
						//System.out.println("day1 : " + days);
						
						String[] listHoraire = days.split(",");
						
						String lundi = listHoraire[0].contains("h")?listHoraire[0].replaceAll("day1 : :", "").replaceAll(":", "") : "Fermee";
						String mardi = listHoraire[1].contains("h")?listHoraire[1] : "Fermee";
						String mercredi = listHoraire[2].contains("h")?listHoraire[2] : "Fermee";
						String jeudi = listHoraire[3].contains("h")?listHoraire[3] : "Fermee";
						String vendredi = listHoraire[4].contains("h")?listHoraire[4] : "Fermee";
						String samedi = listHoraire[5].contains("h")?listHoraire[5] : "Fermee";
						
						lundi = "Lundi : " + lundi;
						mardi = "Mardi : " + mardi;
						mercredi = "Mercredi : " + mercredi;
						jeudi = "Jeudi : " + jeudi;
						vendredi = "Vendredi : " + vendredi;
						samedi = "Samedi : " + samedi;
						samedi+=" Dimanche : Fermee";
						
						/*for (Element tmp : agencySheduleJElement) {
							HorairesTXT += tmp.text() + " ";
						}
						HorairesTXT = this.normalize(HorairesTXT.replaceAll("[^0-9a-zA-z ]", " "));*/
						
						HorairesTXT = lundi + " " + mardi + " " + mercredi + " " + jeudi + " " + vendredi + " " + samedi;
						HorairesTXT = this.normalize(HorairesTXT);
						System.out.println("Horaires : " + HorairesTXT);
						
						// END NANTE
						
					} catch (Exception e) {
						e.printStackTrace();
					}

					// retrieve agency services

					Elements agencyServiceJElement = doc
							.select("div.em-page > main > div.em-details__services-bloc.bloc-bp>ul>li");
					if (agencyServiceJElement != null) {
						for (Element elementTmp : agencyServiceJElement) {
							Services += elementTmp.text() + " ";
						}
						if (Services.contains("Distributeur automatique de billets")) {
							DAB = 1;
						}
					} else {
						Services = "";
						DAB = 0;
					}
					
					System.out.println("DAB : " + DAB);
					
					Services = this.normalize(Services);
					System.out.println("services : " + Services);
					// retrieve agency tag
					Element agencyTagJElement = root2JElement.select("#aside > div > div > a.agencyreseau").first();
					enseigne = (agencyTagJElement != null) ? agencyTagJElement.text() : "";
					
					System.out.println("\n---------------------\n");

					nbRecuperes++;
					bpList.add(this.setData(codePostale, String.valueOf(total), String.valueOf(NbAgences), enseigne,
							nom_agence, adresse, tel, fax, HorairesHTM, HorairesTXT, Services, String.valueOf(DAB),
							agences_HTM));
					log.writeLog(new Date(), url, "no_eeror", NbAgences, parcoursficval, NbAgences - nbRecuperes);
				}
			}
			parcoursficval++;
		}

		sc.close();
		return bpList;
	}

	public void enregistrer(int total, String agences_HTM, String nom_agence, String adresse, String tel, String fax,
			String HorairesHTM, String HorairesTXT, String Services, String enseigne, int DAB) {

		try {

			sortie = new BufferedWriter(new FileWriter(fichier, true));
			sortie.write(codePostale + "\t" + total + "\t" + NbAgences + "\t" + enseigne + "\t" + nom_agence + "\t"
					+ adresse + "\t" + tel + "\t" + fax + "\t" + HorairesHTM + "\t" + HorairesTXT + "\t" + Services
					+ "\t" + DAB + "\t" + agences_HTM + "\r\n");

			sortie.close();
		} catch (IOException e) {

			e.printStackTrace();
		}

	}

	public void enregistrer_journal(int numreponse) {

//	    	this.fichier = new File(Parametres.REPERTOIRE + "RESULTATS_BNP.");
		try {
			Temps_Fin = new Date();
			SimpleDateFormat formater = null;
			formater = new SimpleDateFormat("'le' dd/MM/yyyy '�' hh:mm:ss");
			String date = formater.format(Temps_Fin);
			date = formater.format(Temps_Fin);

			sortie_log = new BufferedWriter(new FileWriter(log, true));
			sortie_log.write(parcoursficval + ";" + "FinValeur" + ";" + codePostale + ";" + this.NbAgences + ";"
					+ nbRecuperes + ";" + (this.NbAgences - nbRecuperes) + ";" + date + "\r\n");
			sortie_log.close();
			long taille_fichier = fichier.length() / 1000000; // la length est en octets, on divise par 1000 pour mettre
																// en Ko et par 1000000 pour mettre en Mo

			if (taille_fichier > 8) {
				// INITIALISATION FICHIER DE RESULTAT
				this.fichier = new File(
						dossierRESU + "\\" + "RESULTATS_" + nomSITE + "_" + System.currentTimeMillis() + ".txt");
				// Attention pas d'accent dans les noms de colonnes !
				sortie = new BufferedWriter(new FileWriter(fichier, true));
				sortie.write("CodePostale" + "\t" + "TOTAL" + "\t" + "NbAgences" + "\t" + "ENSEIGNE" + "\t" + "NOMWEB"
						+ "\t" + "ADRESSE" + "\t" + "TEL" + "\t" + "FAX" + "\t" + "HORAIRESHTM" + "\t" + "HORAIRESTXT"
						+ "\t" + "SERVICES" + "\t" + "DAB" + "\t" + "AGENCEHTM" + "\r\n");
				sortie.close();
			}

		} catch (IOException e) {

			e.printStackTrace();
		}

	}

	/**
	 * La m�thode <code>initialiser()</code> permet d'ajouter la ligne d'ent�te dans
	 * le fichier r�sultat.
	 */

	public void initialiser() {

		try {
			// Attention pas d'accent dans les noms de colonnes !
			sortie = new BufferedWriter(new FileWriter(fichier, true));
			sortie.write("CodePostale" + "\t" + "TOTAL" + "\t" + "NbAgences" + "\t" + "ENSEIGNE" + "\t" + "NOMWEB"
					+ "\t" + "ADRESSE" + "\t" + "TEL" + "\t" + "FAX" + "\t" + "HORAIRESHTM" + "\t" + "HORAIRESTXT"
					+ "\t" + "SERVICES" + "\t" + "DAB" + "\t" + "AGENCEHTM" + "\r\n");
			sortie.close();

			File toto = new File(dossierRESU + "\\" + "LOG_" + nomSITE + ".csv");
			if (!toto.exists()) {
				// if (num_ligneficval == num_ligne) {
				sortie_log = new BufferedWriter(new FileWriter(log, true));
				sortie_log.write("NumLigne" + ";" + "FinValeur" + ";" + "DepSoumis" + ";" + "NbAgences" + ";"
						+ "Nbrecuperes" + ";" + "Reste" + ";" + "Heure_Fin" + "\r\n");
				sortie_log.close();
			}

		} catch (IOException e) {

			e.printStackTrace();
		}
	}

	public void tearDown() {
		htmlUnitDriver.quit();

	}

	public static boolean isAlertPresent(FirefoxDriver chromeDriver) {
		try {
			chromeDriver.switchTo().alert();
			return true;
		} catch (NoAlertPresentException e) {
			return false;
		}
	}

}
