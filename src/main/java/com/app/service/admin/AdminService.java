package com.app.service.admin;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.CriteriaUpdate;
import javax.persistence.criteria.Root;
import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.app.model.TableAssurance;
import com.app.model.TableAutre;
import com.app.model.TableBanque;
import com.app.model.TableBanqueDOM;
import com.app.scrap.RepTableAssurance;
import com.app.scrap.RepTableAutre;
import com.app.scrap.RepTableBanque;
import com.app.scrap.RepTableBanqueDOM;

@Service
@Transactional
public class AdminService {

	private final String[] banques = { "CMB", "CMSO", "SAVOIE", "BCP", "BNP", "BP", "BP_GAB", "CA", "CASDEN", "CDN_GAB",
			"CDN", "CE_GAB", "CE", "CCOOP", "CIC_GAB", "CIC", "CM_GAB", "CM", "HSBC", "LCL", "PALATINE", "SG_GAB", "SG",
			"VISA", "VISA_ADRESSE", "BTP", "BECM", "ACTION_LOGEMENTALL", "BNP_PRIVEE","MEILLEUR_TAUX" };
	
	private final String[] banquesDOM = { "BCFOI", "BCP_DOM", "BNP_DOM", "BP_DOM", "BP_GAB_DOM", "CA_DOM", "CASDEN_DOM", "CDN_GAB_DOM", "CDN_DOM",
			"CE_GAB_DOM", "CE_DOM", "CCOOP_DOM", "CIC_GAB_DOM", "CIC_DOM", "CM_GAB_DOM", "CM_DOM", "HSBC_DOM", "LCL_DOM", "PALATINE_DOM", "SAVOIE_DOM","ACTION_LOGEMENT_DOM","BC" };
	
	private final String[] assurances = { "AESIO", "AG2R", "ALLIANZ", "AREAS", "ASSU2000", "AVIVA", "AXA", "GAN",
			"GENERALI", "GMF", "GROUPAMA", "HARMONIE", "MAAF", "MACIF", "MAIF", "MATMUT", "MMA", "MUTUALIA",
			"MUTUEL_GENERALE", "MUTUEL_POITIER", "SWISSLIFE", "THELEM" };
	private String[] autres = { "TripadVisor", "Orias", "PJ_BANQUE", "PJ_ASSURANCE", "PJ_RESTAURATION", "ferme" };

	@Autowired
	RepTableAssurance repAssu;

	@Autowired
	RepTableBanque repBanque;

	@Autowired
	RepTableAutre repAutre;

	@Autowired
	RepTableBanqueDOM repDom;

	@Autowired
	EntityManager em;

	public <T> boolean save(T t) {
		boolean b = false;
		if (t instanceof TableBanque) {
			TableBanque tb = repBanque.save((TableBanque) t);
			b = (tb != null) ? true : false;
		}
		if (t instanceof TableAssurance) {
			TableAssurance ta = repAssu.save((TableAssurance) t);
			b = (ta != null) ? true : false;
		}

		if (t instanceof TableAutre) {
			TableAutre tAu = repAutre.save((TableAutre) t);
			b = (tAu != null) ? true : false;
		}

		if (t instanceof TableBanqueDOM) {
			TableBanqueDOM tAu = repDom.save((TableBanqueDOM) t);
			b = (tAu != null) ? true : false;
		}
		return b;
	}
	
	public <T> void criteriaBuild(Class<T> type, String name) {
		
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaUpdate<T> cu = cb.createCriteriaUpdate(type);
		Root<T> root = cu.from(type);
		cu.set("status", true).where(cb.equal(root.get("name"), name));
		
		em.createQuery(cu).executeUpdate();
		//em.getTransaction().begin();
		//em.getTransaction().commit();

	}

	public boolean populateBanqueTable() {
		boolean b = false;
		for (String banque : banques) {
			TableBanque tb = new TableBanque();
			tb.setName(banque);
			tb.setStatus(false);
			b = this.save(tb);
		}
		return b;
	}
	public <T> void addColumn(String name, T t) {
		
		if(t instanceof TableBanque) {
			t= (T) new TableBanque();
			((TableBanque) t).setName(name);
			((TableBanque) t).setStatus(false);
			repBanque.save((TableBanque) t);
			
		}
		if(t instanceof TableAssurance) {
			t= (T) new TableAssurance();
			((TableAssurance) t).setName(name);
			((TableAssurance) t).setStatus(false);
			repAssu.save((TableAssurance)t);
			
		}
		if(t instanceof TableBanqueDOM) {
			t= (T) new TableBanqueDOM();
			((TableBanqueDOM) t).setName(name);
			((TableBanqueDOM) t).setStatus(false);
			repDom.save((TableBanqueDOM)t);
			
		}
		if(t instanceof TableAutre) {
			t=(T) new TableAutre();
			((TableAutre) t).setName(name);
			((TableAutre) t).setStatus(false);
			repAutre.save((TableAutre)t);
		}
		
		
		
		
	}

	public boolean populateAssuranceTable() {
		boolean b = false;
		for (String assurance : assurances) {
			TableAssurance ta = new TableAssurance();
			ta.setName(assurance);
			ta.setStatus(false);
			b = this.save(ta);
		}
		return b;
	}

	public boolean populateAutreTable() {
		boolean b = false;
		for (String autre : autres) {
			TableAutre ta = new TableAutre();
			ta.setName(autre);
			ta.setStatus(false);
			b = this.save(ta);
		}
		return b;
	}

	public boolean populateDOMTable() {
		boolean b = false;
		for (String banqueDOM : banquesDOM) {
			TableBanqueDOM ta = new TableBanqueDOM();
			ta.setName(banqueDOM);
			ta.setStatus(false);
			b = this.save(ta);
		}
		return b;
	}

	public void addNewAssurance(String name) {
		TableAssurance ta = new TableAssurance();
		ta.setName(name);
		this.save(ta);
	}

	public void updateStatusBanque(String name) {

		this.criteriaBuild(TableBanque.class, name);
	}

	public void updateStatusAssurance(String name) {

		this.criteriaBuild(TableAssurance.class, name);
	}

	public void updateStatusAutre(String name) {

		this.criteriaBuild(TableAutre.class, name);
	}

	public void updateStatusDOM(String name) {

		this.criteriaBuild(TableBanqueDOM.class, name);
	}

	public <T> List<T> getItem(Class<T> type) {

		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<T> cq = cb.createQuery(type);
		Root<T> root = cq.from(type);
		cq.select(root);
		Query q = em.createQuery(cq);
		List<T> l = q.getResultList();
		return l;
	}

}
