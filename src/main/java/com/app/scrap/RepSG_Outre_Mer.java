package com.app.scrap;

import org.springframework.stereotype.Repository;
import org.springframework.data.jpa.repository.JpaRepository;
import com.app.model.SG_Outre_Mer;

@Repository
public interface RepSG_Outre_Mer extends JpaRepository<SG_Outre_Mer,Integer>,RepositoryCustom<SG_Outre_Mer> {

}
