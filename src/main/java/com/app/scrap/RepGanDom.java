package com.app.scrap;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.app.model.GAN;
import com.app.model.GAN_DOM;

@Repository
public interface RepGanDom extends JpaRepository<GAN_DOM,Integer>,RepositoryCustom<GAN_DOM> {

}
