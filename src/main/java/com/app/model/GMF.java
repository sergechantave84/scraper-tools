package com.app.model;

import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="gmf", schema="scrap")
public class GMF {


	@Id
	@SequenceGenerator(name="scrap.gmf_id_seq",
	                    sequenceName ="scrap.gmf_id_seq",
	                     allocationSize=1)
    @GeneratedValue( strategy=GenerationType.SEQUENCE,
                     generator="scrap.gmf_id_seq")
	  
	@Column(name="id",updatable=false)
	private int id;
	
	@Column(name="depsoumis", columnDefinition="varchar")
	private String depSoumis;
	
	@Column(name="nbagences", columnDefinition="varchar")
	private String nbAgences;
	
	@Column(name="villesoumise", columnDefinition="varchar")
	private String villeSoumise;
	
	@Column(name="nomagence", columnDefinition="varchar")
	private String nomAgence;
	
	@Column(name="adresse", columnDefinition="varchar")
	private String adresse;
	
	@Column(name="tel", columnDefinition="varchar")
	private String tel;
	
	@Column(name="fax", columnDefinition="varchar")
	private String fax;
	
	@Column(name="horaires", columnDefinition="varchar")
	private String horaires;
	
	@Column(name="agencehtm", columnDefinition="varchar")
	private String agenceHtm;
	
	@Column(name="annee_du_scrap" ,columnDefinition="varchar(4) DEFAULT date_part('year',CURRENT_DATE)")
	private String anneeScrap;

	public GMF() {
		LocalDate ld= new Date().toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
		anneeScrap=String.valueOf(ld.getYear()+1);
	}
	public int getId() {
		return id;
	}

	public GMF setId(int id) {
		this.id = id;
		return this;
	}

	public String getDepSoumis() {
		return depSoumis;
	}

	public GMF setDepSoumis(String depSoumis) {
		this.depSoumis = depSoumis;
		return this;
	}

	public String getNbAgences() {
		return nbAgences;
	}

	public GMF setNbAgences(String nbAgences) {
		this.nbAgences = nbAgences;
		return this;
	}

	public String getVilleSoumise() {
		return villeSoumise;
	}

	public GMF setVilleSoumise(String villeSoumise) {
		this.villeSoumise = villeSoumise;
		return this;
	}

	public String getNomAgence() {
		return nomAgence;
	}

	public GMF setNomAgence(String nomAgence) {
		this.nomAgence = nomAgence;
		return this;
	}

	public String getAdresse() {
		return adresse;
	}

	public GMF setAdresse(String adresse) {
		this.adresse = adresse;
		return this;
	}

	public String getTel() {
		return tel;
	}

	public GMF setTel(String tel) {
		this.tel = tel;
		return this;
	}

	public String getFax() {
		return fax;
	}

	public GMF setFax(String fax) {
		this.fax = fax;
		return this;
	}

	public String getHoraires() {
		return horaires;
	}

	public GMF setHoraires(String horaires) {
		this.horaires = horaires;
		return this;
	}

	public String getAgenceHtm() {
		return agenceHtm;
	}

	public GMF setAgenceHtm(String agenceHtm) {
		this.agenceHtm = agenceHtm;
		return this;
	}

	public String getAnneeScrap() {
		return anneeScrap;
	}

	public GMF setAnneeScrap(String anneeScrap) {
		this.anneeScrap = anneeScrap;
		return this;
	}
}
