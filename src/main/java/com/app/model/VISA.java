package com.app.model;

import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="banque_visa", schema="scrap")
public class VISA {

	@Id
	@SequenceGenerator(name="scrap.banque_visa_id_seq",
	                    sequenceName ="scrap.banque_visa_id_seq",
	                     allocationSize=1)
    @GeneratedValue( strategy=GenerationType.SEQUENCE,
                     generator="scrap.banque_visa_id_seq")
	  
	@Column(name="id",updatable=false)
	private int id;
	
	@Column(name="codpostsoumis", columnDefinition="varchar")
	private String cpSoumis;
	
	@Column(name="nbpages", columnDefinition="varchar")
	private String nbPages;
	
	@Column(name="numpage", columnDefinition="varchar")
	private String numPage;
	
	@Column(name="nbagences_pages", columnDefinition="varchar")
	private String nbAgencesPage;
	
	@Column(name="nomweb", columnDefinition="varchar")
	private String nomWeb;
	
	@Column(name="voie", columnDefinition="varchar")
	private String voie;
	
	@Column(name="cp", columnDefinition="varchar")
	private String cp;
	
	@Column(name="ville", columnDefinition="varchar")
	private String ville;
	
	@Column(name="dab", columnDefinition="varchar")
	private String dab;
	
	@Column(name="agencehtm", columnDefinition="varchar")
	private String agenceHtm;
	
	@Column(name="annee_du_scrap" ,columnDefinition="varchar(4) DEFAULT date_part('year',CURRENT_DATE)")
	private String anneeScrap;
	
	public VISA() {
		LocalDate ld = new Date().toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
		anneeScrap = String.valueOf(ld.getYear() + 1);
	}
	public int getId() {
		return id;
	}
	public VISA setId(int id) {
		this.id = id;
		return this;
	}
	public String getCpSoumis() {
		return cpSoumis;
	}
	public VISA setCpSoumis(String cpSoumis) {
		this.cpSoumis = cpSoumis;
		return this;
	}
	public String getNbPages() {
		return nbPages;
	}
	public VISA setNbPages(String nbPages) {
		this.nbPages = nbPages;
		return this;
	}
	public String getNumPage() {
		return numPage;
	}
	public VISA setNumPage(String numPage) {
		this.numPage = numPage;
		return this;
	}
	public String getNbAgencesPage() {
		return nbAgencesPage;
	}
	public VISA setNbAgencesPage(String nbAgencesPage) {
		this.nbAgencesPage = nbAgencesPage;
		return this;
	}
	public String getNomWeb() {
		return nomWeb;
	}
	public VISA setNomWeb(String nomWeb) {
		this.nomWeb = nomWeb;
		return this;
	}
	public String getVoie() {
		return voie;
	}
	public VISA setVoie(String voie) {
		this.voie = voie;
		return this;
	}
	public String getCp() {
		return cp;
	}
	public VISA setCp(String cp) {
		this.cp = cp;
		return this;
	}
	public String getVille() {
		return ville;
	}
	public VISA setVille(String ville) {
		this.ville = ville;
		return this;
	}
	public String getDab() {
		return dab;
	}
	public VISA setDab(String dab) {
		this.dab = dab;
		return this;
	}
	public String getAgenceHtm() {
		return agenceHtm;
	}
	public VISA setAgenceHtm(String agenceHtm) {
		this.agenceHtm = agenceHtm;
		return this;
	}
	public String getAnneeScrap() {
		return anneeScrap;
	}
	public VISA setAnneeScrap(String anneeScrap) {
		this.anneeScrap = anneeScrap;
		return this;
	}
	
}
