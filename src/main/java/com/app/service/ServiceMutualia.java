package com.app.service;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.net.SocketTimeoutException;
import java.util.ArrayList;
import java.util.List;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.app.model.Mutualia;

import com.app.scrap.RepMutuallia;
import com.app.utils.BrowserUtils;
import com.app.utils.Parametres;
import com.app.utils.Utilitaires;

@Service
public class ServiceMutualia extends Utilitaires {

	@Autowired
	private RepMutuallia rep;

	private HtmlUnitDriver htmlUnitDriver = null;
	private List<com.app.model.Mutualia> mutualiaList = new ArrayList<>();

	public Mutualia saveData(Mutualia t) {

		return rep.save(t);

	}

	public List<Mutualia> saveDataAll(List<Mutualia> t) {

		return rep.saveAll(t);

	}

	public List<Mutualia> getElmnt() {

		return rep.findAll();
	}

	public Mutualia getById(int id) {

		return rep.findById(id).orElse(null);
	}

	public String deleteProduct(int id) {
		try {
			rep.deleteById(id);

		} catch (Exception e) {
			e.printStackTrace();
		}

		return "removed";
	}

	public Mutualia updateProduct(Mutualia t) {

		return null;
	}

	public Mutualia setData(String nbAgences, String lienAgence, String nomAgence, String adresse, String tel,
			String horaires, String horairesHtm, String agenceHtm) {
		Mutualia mutualia = new Mutualia();
		mutualia.setNbAgences(nbAgences).setLienAgence(lienAgence).setNomAgence(nomAgence).setAdresse(adresse)
				.setTel(tel).setHoraires(horaires).setHorairesHtm(horairesHtm).setAgenceHtm(agenceHtm);
		this.saveData(mutualia);
		return mutualia;
	}

	public List<Mutualia> showFirstRows() {
		List<Mutualia> a = rep.getFirstRow(100, Mutualia.class);
		return a;
	}

	public List<Mutualia> showLastRows() {
		List<Mutualia> a = rep.getLastRow(100, Mutualia.class);
		return a;
	}

	public void setup() {
		htmlUnitDriver = BrowserUtils.HtmlUnitConstruct(true, false, false, 12000);
	}

	public List<com.app.model.Mutualia> scrapMutualia() throws IOException {

		final String urlMutualia = "https://agence.mutualia.fr/fr";
		final String urlMutualia1 = "https://agence.mutualia.fr";
		final String uriDir = Parametres.REPERTOIRE_RESULT + "\\ASSU\\Mutualia";
		int agencyNumbers = 0;
		String pageSource = "";
		String lienAgence = "";
		String nomAgence = "";
		String telephone = "";
		String adresse = "";
		String horaires = "";
		String agenceHtml = "";
		String horairesHtml = "";
		List<String> agenciesPerReg = new ArrayList<>();
		List<String> agenciesPerDep = new ArrayList<>();
		List<String> agenciesPercity = new ArrayList<>();
		long timeMillis = System.currentTimeMillis();
		Document doc = null;
		File dir = new File(uriDir);
		File resultFile = new File(uriDir + "\\Mutualia_" + timeMillis + ".txt");

		if (!(dir.exists()))
			dir.mkdirs();

		BufferedWriter bf = new BufferedWriter(new FileWriter(resultFile, true));
		this.initialiser(bf);

		pageSource = this.getPageSource(urlMutualia);
		doc = Jsoup.parse(pageSource);
		Elements agenciesPerRegElmnt = doc.select("#lf-geo-divisions-facets > ul > li > a");
		Elements agenciesPerDepElmnt = null;
		Elements agenciesPerCityElmnt = null;
		Element agencyElmnt = null;
		Element root = null;
		Element agencyInfoElmnt = null;
		Element agencyNameElmnt = null;
		Element agencyAddressElmnt = null;
		Element agencyScheduleElmnt = null;
		Element agencyTelephoneElmnt = null;
		for (Element tmp : agenciesPerRegElmnt) {
			agenciesPerReg.add(urlMutualia1 + tmp.attr("href"));
		}

		for (String url0 : agenciesPerReg) {

			if (!agenciesPerDep.isEmpty())
				agenciesPerDep.clear();

			pageSource = this.getPageSource(url0);
			doc = Jsoup.parse(pageSource);
			agenciesPerDepElmnt = doc.select("#lf-geo-divisions-facets > ul > li > a");
			for (Element tmp : agenciesPerDepElmnt) {
				agenciesPerDep.add(urlMutualia1 + tmp.attr("href"));
			}
			
			System.out.println("url0 : "+url0);
			
			for (String url1 : agenciesPerDep) {
				
				System.out.println("url1 : "+url1);

				pageSource = this.getPageSource(url1);
				doc = Jsoup.parse(pageSource);
				agenciesPerCityElmnt = doc.select("#lf-geo-divisions-facets > ul > li > a");
				agencyNumbers += agenciesPerCityElmnt.size();

				if (!agenciesPercity.isEmpty())
					agenciesPercity.clear();

				for (Element tmp : agenciesPerCityElmnt) {
					agenciesPercity.add(urlMutualia1 + tmp.attr("href"));
				}
				for (String strTmp : agenciesPercity) {
					lienAgence = "";
					nomAgence = "";
					telephone = "";
					adresse = "";
					horaires = "";
					agenceHtml = "";
					horairesHtml = "";
					pageSource = this.getPageSource(strTmp);
					doc = Jsoup.parse(pageSource);
					agencyElmnt = doc
							.selectFirst("#lf-body > div.lf > div.lf-geo-divisions > div.lf-geo-divisions__results >"
									+ "div.lf-geo-divisions__results__content > div.lf-geo-divisions__results__content__locations >"
									+ "div.lf-geo-divisions__results__content__locations__list > div > h2 > a");
					lienAgence = urlMutualia1 + agencyElmnt.attr("href");
					pageSource = this.getPageSource(lienAgence);
					doc = Jsoup.parse(pageSource);
					root = doc.selectFirst(
							"#lf-body > div.lf > div.lf-location > div.container > div.row > div.col-xs-12.col-md-4 > div");
					if (root != null) {
						agenceHtml = root.outerHtml();
						agenceHtml = this.normalize(agenceHtml.replaceAll("\\s{1,}", ""));
						agencyInfoElmnt = root.selectFirst("div:nth-child(1)> div.lf-location__infos");
						if (agencyInfoElmnt != null) {
							agencyNameElmnt = agencyInfoElmnt.selectFirst("div.lf-location__infos__title");

							// retrieve agency name
							nomAgence = (agencyNameElmnt != null)
									? this.normalize(agencyNameElmnt.text().replaceAll("\\s ", " ").trim())
									: "";
							System.out.println("nomAgence : "+nomAgence);
							
							// retrieve agency telephone
							agencyTelephoneElmnt = agencyInfoElmnt.selectFirst("#lf-location-phone > div > span");
							telephone = (agencyTelephoneElmnt != null)
									? (agencyTelephoneElmnt.text().replaceAll("\\s{2,}", " ").trim())
									: "";

							System.out.println("telephone : "+telephone);
							// retrieve agency adresse
							agencyAddressElmnt = agencyInfoElmnt.selectFirst("#locations-address-default");
							adresse = (agencyAddressElmnt != null)
									? this.normalize(agencyAddressElmnt.text().replaceAll("\\s{2,}", " ").trim())
									: "";
							
							System.out.println("adresse : "+adresse);
						}
						// retrieve schedule
						agencyScheduleElmnt = root.selectFirst("#lf-menu-hours");
						if (agencyScheduleElmnt != null) {
							horairesHtml = agencyScheduleElmnt.outerHtml();
							horairesHtml = horairesHtml.replaceAll("\\s{2,}", " ").trim();
							horaires = agencyScheduleElmnt.text();
							horaires = horaires.replaceAll("\\s{1,}", " ").trim();
						} else {
							horairesHtml = "";
							horaires = "";
						}
					}
					horairesHtml = this.normalize(horairesHtml);
					horaires = this.normalize(horaires);
					System.out.println("horaires : "+horaires);
					System.out.println("----------------------------\n");

					this.setData(String.valueOf(agencyNumbers), lienAgence, nomAgence, adresse, telephone, horaires,
							horairesHtml, agenceHtml);
				}
			}
		}
		return mutualiaList;
	}

	public String getPageSource(String url) {

		try {
			htmlUnitDriver.get(url);
		} catch (Exception e) {
			if (e instanceof SocketTimeoutException) {
				htmlUnitDriver.get(url);
			}
		}

		return htmlUnitDriver.getPageSource();
	}

	public void enregistrer(BufferedWriter bf, int agencyNumbers, String lienAgence, String nomAgence, String adresse,
			String telephone, String horaires, String horairesHtml, String agenceHtml) {
		
		try {

			bf.write(agencyNumbers + "\t" + lienAgence + "\t" + nomAgence + "\t" + adresse + "\t" + telephone + "\t"
					+ horaires + "\t" + horairesHtml + "\t" + agenceHtml + "\r\n");
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	public void initialiser(BufferedWriter bf) {

		try {

			bf.write("NbAgences" + "\t" + "LIEN_AGENCE" + "\t" + "NOM_AGENCE" + "\t" + "ADRESSE" + "\t" + "TEL" + "\t"
					+ "HORAIRESTXT" + "\t" + "HORAIRESHTM" + "\t" + "AGENCEHTM" + "\r\n");
		} catch (IOException e) {

			e.printStackTrace();
		}
	}

	public void tearsDown() {
		System.out.println("scraper mutualia finis");
		htmlUnitDriver.quit();
	}
}
