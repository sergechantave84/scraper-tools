package com.app.service;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.SocketTimeoutException;
import java.text.Normalizer;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Scanner;

import org.json.JSONObject;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.NoAlertPresentException;
import org.openqa.selenium.PageLoadStrategy;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.app.model.CASDEN;
import com.app.scrap.RepCasden;
import com.app.utils.BrowserUtils;
import com.app.utils.Parametres;
import com.app.utils.Utilitaires;

import fa.browserUtils.ChromeBrowser;
import fa.browserUtils.FactoryBrowser;
import fa.browserUtils.HtmlUnitBrowser;
import fa.log.LogApp.LogScrap;
import fa.log.config.LogConst;

@Service
public class ServiceCasden extends Utilitaires {

	@Autowired
	private RepCasden rep;

	private List<CASDEN> casdenList = new ArrayList<>();
	static String driverPath = "C:\\ChromeDriver\\";
	String nomSITE = "CASDEN";
	String ficval = "lien_casden";
	String dossierRESU = Parametres.REPERTOIRE_RESULT + "\\BQ\\" + nomSITE;;
	String url_accueil = "https://agences.banquepopulaire.fr";
	
	// "https://www.casden.fr/Votre-banque-cooperative/Annuaires/Annuaire-des-agences-Banque-Populaire";
	String fichier_valeurs;
	String CPSoumis;
	String ville;
	String ligne;

	int num_ligne = 2;
	int NbAgences;
	int numreponse = 1;
	int nbRecuperes;
	int num_ligneficval;
	int parcoursficval;
	int Resultats;

	HtmlUnitDriver htmlUnitDriver;
	BufferedWriter sortie;
	BufferedWriter sortie_log;

	Date Temps_Fin;

	public CASDEN saveData(CASDEN t) {

		return rep.save(t);

	}

	public List<CASDEN> saveDataAll(List<CASDEN> t) {

		return rep.saveAll(t);

	}

	public List<CASDEN> getElmnt() {

		return rep.findAll();
	}

	public CASDEN getById(int id) {

		return rep.findById(id).orElse(null);
	}

	public String deleteProduct(int id) {
		try {
			rep.deleteById(id);

		} catch (Exception e) {
			e.printStackTrace();
		}

		return "removed";
	}
	
	public String deleteAll() {
		try {
			
			rep.deleteAll();
			System.out.println("Supprimee avec success");

		} catch (Exception e) {
			e.printStackTrace();
		}

		return "removed";
	}

	public CASDEN updateProduct(CASDEN t) {

		return null;
	}

	public CASDEN setData(String codePostale, String total, String nbAgences, String enseigne, String nomWeb,
			String adresse, String tel, String fax, String horairesHtm, String horairesCASDENxt, String services,
			String dab, String agenceHtm) {
		CASDEN casden = new CASDEN();
		casden.setCodePostale(codePostale).setTotal(total).setNbAgences(nbAgences).setEnseigne(enseigne)
				.setNomWeb(nomWeb).setAdresse(adresse).setTel(tel).setFax(fax).setHorairesHtm(horairesHtm)
				.setHorairesTxt(horairesCASDENxt).setServices(services).setDAB(dab).setAgenceHtm(agenceHtm);
		this.saveData(casden);
		return casden;
	}

	public List<CASDEN> showFirstRows() {
		List<CASDEN> a = rep.getFirstRow(100, CASDEN.class);
		return a;
	}

	public List<CASDEN> showLastRows() {
		List<CASDEN> a = rep.getLastRow(100, CASDEN.class);
		return a;
	}

	public void setup() {
		FactoryBrowser fact = new FactoryBrowser();
		HtmlUnitBrowser hB = (HtmlUnitBrowser) fact.create("htmlUnit");
		htmlUnitDriver = hB.setOptions(false, false, false, 60000).initBrowserDriver();
	}

	public List<CASDEN> open_CASDEN_byselenium() throws IOException, InterruptedException {
		String url = "";
		List<String> agencyLinks = new ArrayList<>();

		fichier_valeurs = Parametres.REPERTOIRE_DATA_SOURCES + ficval + ".txt";
		Scanner sc = new Scanner(new FileInputStream(new File(fichier_valeurs)));

		if (!new File(dossierRESU).exists()) {
			new File(dossierRESU).mkdirs();
		}
		fichier = new File(dossierRESU + "\\" + "RESULTATS_" + nomSITE + "_" + System.currentTimeMillis() + ".txt");
		LogScrap log = new LogScrap("BanqueCasden.txt", LogConst.TYPE_LOG_FILE);
		num_ligneficval = ((new File(LogConst.STORE_LOG_PATH + LogConst.PREFIX_LOG_FILE + "_BanqueCasden.txt"))
				.exists()) ? Integer.parseInt(log.readLog().getLastLineProcessed().split("\\t")[4]) : 1;

		parcoursficval = 1;
		while (sc.hasNextLine()) {
			String line = sc.nextLine();
			String[] fic_valeurs = line.split("\\t");
			if (parcoursficval > num_ligneficval) {

				nbRecuperes = 0;
				CPSoumis = fic_valeurs[0];
				// ville=fic_valeurs[1];
				url = url_accueil + fic_valeurs[1];
				System.out.println(url);
				try {
					htmlUnitDriver.get(url);

				} catch (Exception e) {
					if (e instanceof SocketTimeoutException) {
						htmlUnitDriver.get(url);

					}
				}

				String pageSource = "";
				Document doc = null;
				if (!agencyLinks.isEmpty()) {
					agencyLinks.clear();
				}
				pageSource = htmlUnitDriver.getPageSource();
				doc = Jsoup.parse(pageSource);
				Element rootJElement = doc.select("main").first();
				
				//System.out.println(rootJElement);
				//div.em-results-wrapper > div > div.em-results__list-wrapper > div > div > div > ul > li > h2 > a
				//body > main > div.em-results-wrapper > div > div.em-results__list-wrapper > div > ul > li > h2 > a
				Elements agencyLinksJElement = rootJElement
						.select("div.em-results-wrapper > div > div.em-results__list-wrapper > div > ul > li > h2 > a");
				for (Element elmntTmp : agencyLinksJElement) {
					String tmp = "";
					tmp = elmntTmp.attr("href");

					agencyLinks.add(tmp);
				}
				NbAgences = agencyLinks.size();
				System.out.println(NbAgences);

				nbRecuperes = 0;
				for (String strTmp : agencyLinks) {
					
					String temp_uri = "https://delegations.casden.fr";
					
					//url = url_accueil + strTmp;
					url = temp_uri + strTmp;
					System.out.println(url);
					try {
						htmlUnitDriver.get(url);

					} catch (Exception e) {
						if (e instanceof SocketTimeoutException) {
							htmlUnitDriver.get(url);

						}
					}

					String agences_HTM = "", nom_agence = "", adresse = "", tel = "", fax = "", HorairesHTM = "",
							HorairesTXT = "", services = "", enseigne = "";
					int total = 0, DAB = 0;
					total = NbAgences;
					pageSource = htmlUnitDriver.getPageSource();
					doc = Jsoup.parse(pageSource);

					Element root2JElement = doc.select("main").first();
					agences_HTM = root2JElement.outerHtml().trim().replaceAll("\\s{1,}", "");
					agences_HTM = this.normalize(agences_HTM);
					// retrieve agency name
					Element agencyNameJElement = root2JElement.select("div.em-details > div.em-details__poi-card > h2").first();
					nom_agence = (agencyNameJElement != null) ? Normalizer
							.normalize(agencyNameJElement.text(), Normalizer.Form.NFKD).replaceAll("[^\\p{ASCII}]", "")
							: "";
					System.out.println("Nom : " + nom_agence);
					// retrieve agency address
					Element agencyAddressJElement = root2JElement.select("div.em-details > div.em-details__poi-card > div > div:nth-child(1) > div")
							.first();
					adresse = (agencyAddressJElement != null)
							? Normalizer.normalize(agencyAddressJElement.text(), Normalizer.Form.NFKD).replaceAll(
									"[^\\p{ASCII}]", "").replaceAll(",", "")
							: "";
					System.out.println("Adresse : " + adresse);

					// retrieve agency tel
					Element agencyTelJElement = root2JElement.select("div.em-details > div.em-details__poi-card > div > div:nth-child(2) > a.em-details__tel").first();
					tel = (agencyTelJElement != null)
							? this.normalize(agencyTelJElement.text().replaceAll("[^0-9 ]", ""))
							: "";
					System.out.println("Tel : " + tel);

					// retrieve agency fax
					/*Element agencyFaxJElement = root2JElement.select("#aside > div > div > div.agencyfax > span")
							.first();
					fax = (agencyFaxJElement != null)
							? this.normalize(agencyFaxJElement.text().replaceAll("[^0-9 ]", ""))
							: "";
					System.out.println("Fax : " + fax);*/

					// retrieve agency schedule
					//Elements agencyScheduleJElement = root2JElement.select("div.em-details__horaires-bloc > div > div.em-graphical-schedules > div > ul.graphicalSchedules__days > li");
					
					//System.out.println(root2JElement);
					Element o = doc.select("body > script:nth-child(3)").first();
					
					String hrr ="";
					
					JSONObject obj = null;
					
					if(o!= null) {
						String o_text = o.toString();
						String shedule = o_text.split("schedule\": ")[1];
						String object = shedule.split("\\}\\}\\);")[0];
						
						//System.out.println(object);
						obj = new JSONObject(object);
						
						hrr += "Lundi : "+ (obj.get("day1")!="" ? (String) obj.get("day1") : "Fermee ");
						hrr +=" Mardi : "+ (obj.get("day2")!="" ? (String) obj.get("day2") : "Fermee ");
						hrr +=" Mercredi : "+ (obj.get("day3")!="" ? (String) obj.get("day3") : "Fermee ");
						hrr +=" Jeudi : "+ (obj.get("day4")!="" ? (String) obj.get("day4") : "Fermee ");
						hrr +=" Vendredi : "+ (obj.get("day5")!="" ? (String) obj.get("day5") : "Fermee ");
						hrr +=" Samedi : "+ (obj.get("day6")!="" ? (String) obj.get("day6") : "Fermee ");
						hrr +=" Dimanche : "+ (obj.get("day7")!="" ? (String) obj.get("day7") : "Fermee ");
						
						// Recup FAX
						
						fax = o_text.split("fax\":\"")[1].split("\",")[0];
						
						System.out.println("Fax : " + fax);
					}
					
					
					/*if (agencyScheduleJElement != null) {
						for (Element elementTmp : agencyScheduleJElement) {
							HorairesHTM += elementTmp.outerHtml().trim() + " ";
							Element dayNameElem = elementTmp.selectFirst("div.graphicalSchedules__dayName");
							Elements dayTrackElem = elementTmp.select("div.graphicalSchedules__dayTrack > div");
							String dayTrack0 = (dayTrackElem.get(0) != null) ? dayTrackElem.get(0).text() : "";
							String dayTrack1 = (dayTrackElem.get(1) != null) ? dayTrackElem.get(1).text() : "";
							HorairesTXT += dayNameElem.text() +" : " + dayTrack0 + " "+dayTrack1 +" ";
						}
						HorairesHTM = HorairesHTM.replaceAll("\\s{1,}", "");
					} else {
						HorairesTXT = "";
						HorairesHTM = "";
					}
					HorairesHTM = this.normalize(HorairesHTM);
					HorairesTXT = Normalizer.normalize((HorairesTXT), Normalizer.Form.NFKD).replaceAll("[^\\p{ASCII}]",
							"");*/
					HorairesHTM = obj.toString();
					HorairesTXT = hrr;
					
					System.out.println("Horaires : " + HorairesTXT);
					// retrieve agency services
					/*Elements agencyServiceJElement = root2JElement
							.select("div.services-bloc.bloc-bp > div > ul > li.service-cell > span.text");
					
					if (agencyServiceJElement != null) {
						for (Element elementTmp : agencyServiceJElement) {
							services += elementTmp.text() + " ";
						}
						if (services.contains("Distributeur automatique de billets")) {
							DAB = 1;
						}
					} else {
						services = "";
						DAB = 0;
					}*/
					
					// retrieve agency tag
					Element agencyTagJElement = root2JElement.select("div.em-details > div.em-details__poi-card > a.em-details__reseau").first();
					enseigne = (agencyTagJElement != null) ? agencyTagJElement.text() : "";
					System.out.println("Reseau : " + enseigne);
					
					// ADD BY NANTE
					Elements offres = root2JElement.select("div.em-details__services-bloc.bloc-casden > ul > li");
					String offreText = "";
					if(offres != null)for(Element offre : offres) {
						offreText += offre.text() + " ";
					}
					
					offreText = Normalizer.normalize(offreText, Normalizer.Form.NFKD).replaceAll("[^\\p{ASCII}]", "");
					
					System.out.println("Offres : " + offreText);
					
					services = offreText.trim();
					
					if (services.contains("Distributeur automatique de billets") || services.toLowerCase().contains("distributeur automatique")) {
						DAB = 1;
					}else {
						DAB = 0;
					}
					
					System.out.println("DAB : " + DAB);
					services = Normalizer.normalize(services, Normalizer.Form.NFKD).replaceAll("[^\\p{ASCII}]", "");
					System.out.println("Services : " + services);
					
					// FIN NANTE

					nbRecuperes++;
					this.setData(CPSoumis, String.valueOf(total), String.valueOf(NbAgences), enseigne, nom_agence,
							adresse, tel, fax, HorairesHTM, HorairesTXT, services, String.valueOf(DAB), agences_HTM);
					
					System.out.println("\n----------------------\n");

				}

				log.writeLog(new Date(), CPSoumis, "no_error", NbAgences, parcoursficval, NbAgences - nbRecuperes);
			}
			parcoursficval++;
		}

		sc.close();
		return casdenList;
	}

	public void enregistrer(int total, String agences_HTM, String nom_agence, String adresse, String tel, String fax,
			String HorairesHTM, String HorairesTXT, String Services, String enseigne, int DAB) {

		try {

			sortie = new BufferedWriter(new FileWriter(fichier, true));
			sortie.write(CPSoumis + "\t" + total + "\t" + NbAgences + "\t" + enseigne + "\t" + nom_agence + "\t"
					+ adresse + "\t" + tel + "\t" + fax + "\t" + HorairesHTM + "\t" + HorairesTXT + "\t" + Services
					+ "\t" + DAB + "\t" + agences_HTM + "\r\n");

			sortie.close();
		} catch (IOException e) {

			e.printStackTrace();
		}

	}

	public void enregistrer_journal(int numreponse) {

//    	this.fichier = new File(Parametres.REPERTOIRE + "RESULTATS_BNP.");
		try {
			Temps_Fin = new Date();
			SimpleDateFormat formater = null;
			formater = new SimpleDateFormat("'le' dd/MM/yyyy '�' hh:mm:ss");
			String date = formater.format(Temps_Fin);
			date = formater.format(Temps_Fin);

			sortie_log = new BufferedWriter(new FileWriter(log, true));
			sortie_log.write(parcoursficval + ";" + "FinValeur" + ";" + CPSoumis + ";" + this.NbAgences + ";"
					+ nbRecuperes + ";" + (this.NbAgences - nbRecuperes) + ";" + date + "\r\n");
			sortie_log.close();
			long taille_fichier = fichier.length() / 1000000; // la length est en octets, on divise par 1000 pour mettre
																// en Ko et par 1000000 pour mettre en Mo

			if (taille_fichier > 8) {
				// INITIALISATION FICHIER DE RESULTAT
				this.fichier = new File(
						dossierRESU + "\\" + "RESULTATS_" + nomSITE + "_" + System.currentTimeMillis() + ".txt");
				// Attention pas d'accent dans les noms de colonnes !
				sortie = new BufferedWriter(new FileWriter(fichier, true));
				sortie.write("CodePostale" + "\t" + "TOTAL" + "\t" + "NbAgences" + "\t" + "ENSEIGNE" + "\t" + "NOMWEB"
						+ "\t" + "ADRESSE" + "\t" + "TEL" + "\t" + "FAX" + "\t" + "HORAIRESHTM" + "\t" + "HORAIRESTXT"
						+ "\t" + "SERVICES" + "\t" + "DAB" + "\t" + "AGENCEHTM" + "\r\n");
				sortie.close();
			}

		} catch (IOException e) {

			e.printStackTrace();
		}

	}

	/**
	 * La m�thode <code>initialiser()</code> permet d'ajouter la ligne d'ent�te dans
	 * le fichier r�sultat.
	 */

	public void initialiser() {

		try {
			// Attention pas d'accent dans les noms de colonnes !
			sortie = new BufferedWriter(new FileWriter(fichier, true));
			sortie.write("CodePostale" + "\t" + "TOTAL" + "\t" + "NbAgences" + "\t" + "ENSEIGNE" + "\t" + "NOMWEB"
					+ "\t" + "ADRESSE" + "\t" + "TEL" + "\t" + "FAX" + "\t" + "HORAIRESHTM" + "\t" + "HORAIRESTXT"
					+ "\t" + "SERVICES" + "\t" + "DAB" + "\t" + "AGENCEHTM" + "\r\n");
			sortie.close();

			File toto = new File(dossierRESU + "\\" + "LOG_" + nomSITE + ".csv");
			if (!toto.exists()) {
				// if (num_ligneficval == num_ligne) {
				sortie_log = new BufferedWriter(new FileWriter(log, true));
				sortie_log.write("NumLigne" + ";" + "FinValeur" + ";" + "DepSoumis" + ";" + "NbAgences" + ";"
						+ "Nbrecuperes" + ";" + "Reste" + ";" + "Heure_Fin" + "\r\n");
				sortie_log.close();
			}

		} catch (IOException e) {

			e.printStackTrace();
		}
	}

	public void tearDown() {
		htmlUnitDriver.quit();

	}

	public static boolean isAlertPresent(FirefoxDriver chromeDriver) {
		try {
			chromeDriver.switchTo().alert();
			return true;
		} catch (NoAlertPresentException e) {
			return false;
		}
	}

}
