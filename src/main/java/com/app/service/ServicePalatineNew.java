package com.app.service;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;

import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;

import java.net.URL;

import java.text.Normalizer;
import java.util.Date;
import java.util.Scanner;

import org.json.JSONObject;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.app.model.Palatine;
import com.app.scrap.RepPalatine;
import com.app.utils.Parametres;
import com.app.utils.Utilitaires;

import fa.log.LogApp.LogScrap;
import fa.log.config.LogConst;

@Service
public class ServicePalatineNew extends Utilitaires {
	// Listes d'agences : https://agences.palatine.fr/toutes-nos-agences
	private String fichier_valeurs;
	private String nomSite = "banque_palatine";
	private String ficval = "lien_palatine";
	private String dossierRESU = Parametres.REPERTOIRE_RESULT + "\\BQ\\" + nomSite;
	private int num_ligneficval = 1;
	private int currentLine = 1;

	@Autowired
	private RepPalatine rep;

	public Palatine saveData(Palatine t) {

		return rep.save(t);

	}

	public void setData(String depSoumis, String nomAgence, String adresse, String tel, String fax, String horairesTXT,
			String nbAgences, String agencesId, String horairesHTM, String agencesHTML) {

		Palatine palatine = new Palatine();
		palatine.setDepSoumis(depSoumis).setNbAgences(nbAgences).setAgences_id(agencesId).setNom_agence(nomAgence)
				.setAdresse(adresse).setTel(tel).setFax(fax).setHorairesTXT(horairesTXT).setHorairesHTM(horairesHTM)
				.setAgences_HTM(agencesHTML);

		this.saveData(palatine);
	}

	public void scrapPalatine() throws IOException {
		fichier_valeurs = Parametres.REPERTOIRE_DATA_SOURCES + ficval + ".txt";
		Scanner sc = new Scanner(new BufferedInputStream(new FileInputStream(new File(fichier_valeurs))));

		if (!new File(dossierRESU).exists())
			new File(dossierRESU).mkdirs();
		this.fichier = new File(
				dossierRESU + "\\" + "RESULTATS_" + nomSite + "_" + System.currentTimeMillis() + ".txt");
		LogScrap log = new LogScrap("BanquePalatine.txt", LogConst.TYPE_LOG_FILE);
		num_ligneficval = ((new File(LogConst.STORE_LOG_PATH + LogConst.PREFIX_LOG_FILE + "_BanquePalatine.txt"))
				.exists()) ? Integer.parseInt(log.readLog().getLastLineProcessed().split("\\t")[4]) : 1;

		while (sc.hasNextLine()) {
			String lines[] = sc.nextLine().split("\\t");

			if (currentLine > num_ligneficval) {
				System.out.println(lines[1]);
				URL url = new URL(lines[1]);
				HttpURLConnection con = (HttpURLConnection) url.openConnection();
				con.setRequestMethod("GET");
				con.setRequestProperty("Content-Type", "text/html");
				con.setRequestProperty("Content-Encoding", "gzip");
				con.setRequestProperty("Vary", "Accept-Encoding");
				con.setRequestProperty("Content-Length", "0");
				con.setConnectTimeout(60000);
				con.setReadTimeout(60000);
				con.setInstanceFollowRedirects(true);

				BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream(), "ISO-8859-1"));
				String pageSource;
				StringBuilder strB = new StringBuilder();
				while ((pageSource = in.readLine()) != null) {
					strB.append(pageSource);
				}

				in.close();
				Document doc = Jsoup.parse(strB.toString());
				Element root = doc.selectFirst("body > script");

				String agencys[] = {};

				if (root.html().toString().contains("AddPoi")) {

					agencys = Normalizer.normalize(root.html().toString(), Normalizer.Form.NFD)
							.replaceAll("[^\\p{ASCII}]", "").split("AddPoi");
				} else {
					System.out.println("Aucun agence trouve");
				}

				int l = agencys.length;

				JSONObject scheduleJSON = null;
				JSONObject jsonDatas = null;
				JSONObject json = null;

				int nbAgences = l;

				if (l > 0) {

					String nomAgence = "", zipCode = "", city = "", adresse = "", postalAdress = "", tel = "", fax = "",
							horairesHTM = "", horairesTXT = "", agencyId = "", agencesHTML = "";

					for (int i = 0; i < l - 1; i++) {
						nomAgence = "";
						zipCode = "";
						city = "";
						adresse = "";
						postalAdress = "";
						tel = "";
						fax = "";
						horairesHTM = "";
						horairesTXT = "";
						agencyId = "";
						agencesHTML = "";

						String strJSON = agencys[i + 1].replaceAll("[\\(\\)]", "")
								.replaceAll("^['0-9,]{1,}\\{\"datas\"", "");
						strJSON = "{\"datas\"" + strJSON;

						json = new JSONObject(strJSON);
						jsonDatas = json.getJSONObject("datas");

						agencesHTML = jsonDatas.toString();

						nomAgence = (jsonDatas.has("label")) ? jsonDatas.getString("label") : "";
						System.out.println("le nom de l'agence " + nomAgence);

						zipCode = (jsonDatas.has("zip")) ? jsonDatas.getString("zip") : "";
						city = (jsonDatas.has("city")) ? jsonDatas.getString("city") : "";
						adresse = (jsonDatas.has("address1"))
								? jsonDatas.getString("address1") + " " + zipCode + " " + city
								: "";
						adresse = adresse.replaceAll(",", "").toUpperCase();
						System.out.println("l'adresse est " + adresse);

						agencyId = (jsonDatas.has("id")) ? jsonDatas.getString("id") : "";
						System.out.println("id " + agencyId);

						postalAdress = (jsonDatas.has("postalAddress")) ? jsonDatas.getString("postalAddress") : "";
						System.out.println("postalAdress " + postalAdress);

						tel = (jsonDatas.has("tel")) ? jsonDatas.getString("tel") : "";
						System.out.println("tel " + tel);

						fax = (jsonDatas.has("fax")) ? jsonDatas.getString("fax") : "";
						System.out.println("fax" + fax);

						scheduleJSON = (jsonDatas.has("schedule")) ? jsonDatas.getJSONObject("schedule") : null;
						if (scheduleJSON != null) {
							horairesHTM = scheduleJSON.toString();
							horairesTXT = "lundi " + scheduleJSON.getString("day1") + " mardi "
									+ scheduleJSON.getString("day2") + " mercredi " + scheduleJSON.getString("day3")
									+ " jeudi " + scheduleJSON.getString("day4") + " vendredi "
									+ scheduleJSON.getString("day5") + " samedi " + scheduleJSON.getString("day6");
							if(!horairesTXT.toLowerCase().contains("dimanche")) {
								horairesTXT+=" Dimanche : Ferme ";
							}
							System.out.println(horairesTXT);
						}
						
						this.setData(lines[0], nomAgence, adresse, tel, fax, horairesTXT, String.valueOf(nbAgences),
								agencyId, horairesHTM, agencesHTML);
					}
				}

				log.writeLog(new Date(), lines[1], "no_errors", nbAgences, currentLine, 0);
				con.disconnect();
			}
			currentLine++;

		}
	}

}
