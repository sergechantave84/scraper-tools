package com.app.scrap;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.app.model.CASDEN;

@Repository
public interface RepCasden extends JpaRepository<CASDEN,Integer>,RepositoryCustom<CASDEN> {

}
