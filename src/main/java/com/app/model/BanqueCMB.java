package com.app.model;

import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="banque_cmb", schema="scrap")
public class BanqueCMB {


	@Id
	@SequenceGenerator(name="scrap.banque_cmb_id_seq",
	                    sequenceName ="scrap.banque_cmb_id_seq",
	                     allocationSize=1)
    @GeneratedValue( strategy=GenerationType.SEQUENCE,
                     generator="scrap.banque_cmb_id_seq")
	  
	@Column(name="id",updatable=false)
	private int id;
	
	@Column(name="depsoumis", columnDefinition="varchar")
	private String depSoumis;
	
	@Column(name="nbvilles", columnDefinition="varchar")
	private String nbVilles;
	
	@Column(name="numvilles", columnDefinition="varchar")
	private String numVilles;
	
	@Column(name="villesoumise", columnDefinition="varchar")
	private String villeSoumise;
	
	@Column(name="nbagences", columnDefinition="varchar")
	private String nbAgences;
	
	@Column(name="nomweb", columnDefinition="varchar")
	private String nomWeb;
	
	@Column(name="adresse", columnDefinition="varchar")
	private String adresse;
	
	@Column(name="horaire", columnDefinition="varchar")
	private String horaire;
	
	@Column(name="telephone", columnDefinition="varchar")
	private String telephone;
	
	@Column(name="lienagence", columnDefinition="varchar")
	private String lienAgence;
	
	@Column(name="agencehtm", columnDefinition="varchar")
	private String agenceHtm;
	
	@Column(name="annee_du_scrap" ,columnDefinition="varchar(4) DEFAULT date_part('year',CURRENT_DATE)")
	private String anneeScrap;

	public BanqueCMB() {
		LocalDate ld= new Date().toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
		anneeScrap=String.valueOf(ld.getYear()+1);
	}

	public int getId() {
		return id;
	}

	public BanqueCMB setId(int id) {
		this.id = id;
		return this;
	}

	public String getDepSoumis() {
		return depSoumis;
	}

	public BanqueCMB setDepSoumis(String depSoumis) {
		this.depSoumis = depSoumis;
		return this;
	}

	public String getNbVilles() {
		return nbVilles;
	}

	public BanqueCMB setNbVilles(String nbVilles) {
		this.nbVilles = nbVilles;
		return this;
	}

	public String getNumVilles() {
		return numVilles;
	}

	public BanqueCMB setNumVilles(String numVilles) {
		this.numVilles = numVilles;
		return this;
	}

	public String getVilleSoumise() {
		return villeSoumise;
	}

	public BanqueCMB setVilleSoumise(String villeSoumise) {
		this.villeSoumise = villeSoumise;
		return this;
	}

	public String getNbAgences() {
		return nbAgences;
	}

	public BanqueCMB setNbAgences(String nbAgences) {
		this.nbAgences = nbAgences;
		return this;
	}

	public String getNomWeb() {
		return nomWeb;
	}

	public BanqueCMB setNomWeb(String nomWeb) {
		this.nomWeb = nomWeb;
		return this;
	}

	public String getAdresse() {
		return adresse;
	}

	public BanqueCMB setAdresse(String adresse) {
		this.adresse = adresse;
		return this;
	}

	public String getLienAgence() {
		return lienAgence;
	}

	public BanqueCMB setLienAgence(String lienAgence) {
		this.lienAgence = lienAgence;
		return this;
	}

	public String getAgenceHtm() {
		return agenceHtm;
	}

	public BanqueCMB setAgenceHtm(String agenceHtm) {
		this.agenceHtm = agenceHtm;
		return this;
	}

	public String getAnneeScrap() {
		return anneeScrap;
	}

	public BanqueCMB setAnneeScrap(String anneeScrap) {
		this.anneeScrap = anneeScrap;
		return this;
	}

	public String getHoraire() {
		return horaire;
	}

	public BanqueCMB setHoraire(String horaire) {
		this.horaire = horaire;
		return this;
	}

	public String getTelephone() {
		return telephone;
	}

	public BanqueCMB setTelephone(String telephone) {
		this.telephone = telephone;
		return this;
	}
	
}
