package com.app.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="pj_restauration", schema="scrap")
public class PJRestauration {

	@Id
	@SequenceGenerator(name="scrap.pj_restauration_id_seq", allocationSize = 1)
	@GeneratedValue(generator = "scrap.pj_restauration_id_seq",strategy = GenerationType.SEQUENCE)
	@Column(name="id")
	int id;
	
	@Column(name="activite_soumise", columnDefinition = "varchar(500) COLLATE pg_catalog.default")
	private String activite_soumis;
	
	@Column(name="COMMUNE" , columnDefinition = "varchar(500) COLLATE pg_catalog.default")
	private String commune;
	
	@Column(name="CP", columnDefinition = "varchar(10) COLLATE pg_catalog.default")
	private String cp;
	
	@Column(name="nb_res", columnDefinition = "integer")
	private int nb_res;
	
	@Column(name="nb_page", columnDefinition="integer")
	private int nb_page;
	
	@Column(name="nb_res_par_page", columnDefinition = "integer")
	private int nb_par_page;
	
	@Column(name="activite", columnDefinition = "varchar(500) COLLATE pg_catalog.default")
	private String activite;
	
	@Column(name="tel", columnDefinition = "varchar(500) COLLATE pg_catalog.default")
	private String tel;
	
	@Column(name="denomination", columnDefinition = "varchar(500) COLLATE pg_catalog.default")
	private String denomination;
	
	@Column(name="adresse",columnDefinition = "varchar(1000) COLLATE pg_catalog.default")
	private String adresse;
	
	@Column(name="prestation", columnDefinition="varchar(500) COLLATE pg_catalog.default")
	private String prestation;
	
	@Column(name="type_cuisine",  columnDefinition="varchar(500) COLLATE pg_catalog.default" )
	private String typeCuisine;
	
	@Column(name="budget", columnDefinition="varchar(500) COLLATE pg_catalog.default" )
	private String budget;
	
	@Column(name="mode_paiement", columnDefinition="varchar(500) COLLATE pg_catalog.default")
	private String modePaiement;
	
	@Column(name="site",columnDefinition="varchar(500) COLLATE pg_catalog.default" )
	private String site;
	
	@Column(name="passe_sanitaire",columnDefinition="varchar(500) COLLATE pg_catalog.default" )
	private String passe_sanitaire;
	
	@Column(name="siret",columnDefinition="varchar(500) COLLATE pg_catalog.default" )
	private String siret;
	
	@Column(name="siren", columnDefinition="varchar(500) COLLATE pg_catalog.default")
    private String siren;
	
	@Column(name="ambiance", columnDefinition="varchar(500) COLLATE pg_catalog.default")
	private String ambiance;
	
	@Column(name="adrbano", columnDefinition="varchar(1000) COLLATE pg_catalog.default")
	private String adrbano;
	
	@Column(name="dep44", columnDefinition="boolean")
	private boolean dep44;
	
	@Column(name="departement", columnDefinition="varchar(500) COLLATE pg_catalog.default")
	private String departement;
	
	public String getDepartement() {
		return departement;
	}

	public PJRestauration setDepartement(String departement) {
		this.departement = departement;
		return this;
	}

	public String getActivite_soumis() {
		return activite_soumis;
	}

	public PJRestauration setActivite_soumis(String activite_soumis) {
		this.activite_soumis = activite_soumis;
		return this;
	}

	public String getCommune() {
		return commune;
	}

	public PJRestauration setCommune(String commune) {
		this.commune = commune;
		return this;
	}

	public String getCp() {
		return cp;
	}

	public PJRestauration setCp(String cp) {
		this.cp = cp;
		return this;
	}

	public int getNb_res() {
		return nb_res;
	}

	public PJRestauration setNb_res(int nb_res) {
		this.nb_res = nb_res;
		return this;
	}

	public int getNb_page() {
		return nb_page;
	}

	public PJRestauration setNb_page(int nb_page) {
		this.nb_page = nb_page;
		return this;
	}

	public int getNb_par_page() {
		return nb_par_page;
	}

	public PJRestauration setNb_par_page(int nb_par_page) {
		this.nb_par_page = nb_par_page;
		return this;
	}

	public String getActivite() {
		return activite;
	}

	public PJRestauration setActivite(String activite) {
		this.activite = activite;
		return this;
	}

	public String getTel() {
		return tel;
	}

	public PJRestauration setTel(String tel) {
		this.tel = tel;
		return this;
	}

	public String getDenomination() {
		return denomination;
	}

	public PJRestauration setDenomination(String denomination) {
		this.denomination = denomination;
		return this;
	}

	public String getAdresse() {
		return adresse;
	}

	public PJRestauration setAdresse(String adresse) {
		this.adresse = adresse;
		return this;
	}

	public String getPrestation() {
		return prestation;
	}

	public PJRestauration setPrestation(String prestation) {
		this.prestation = prestation;
		return this;
	}

	public String getTypeCuisine() {
		return typeCuisine;
	}

	public PJRestauration setTypeCuisine(String typeCuisine) {
		this.typeCuisine = typeCuisine;
		return this;
	}

	public String getBudget() {
		return budget;
	}

	public PJRestauration setBudget(String budget) {
		this.budget = budget;
		return this;
	}

	public String getModePaiement() {
		return modePaiement;
	}

	public PJRestauration setModePaiement(String modePaiement) {
		this.modePaiement = modePaiement;
		return this;
	}

	public String getSite() {
		return site;
	}

	public PJRestauration setSite(String site) {
		this.site = site;
		return this;
	}

	public String getPasse_sanitaire() {
		return passe_sanitaire;
	}

	public PJRestauration setPasse_sanitaire(String passe_sanitaire) {
		this.passe_sanitaire = passe_sanitaire;
		return this;
	}

	public String getSiret() {
		return siret;
	}

	public PJRestauration setSiret(String siret) {
		this.siret = siret;
		return this;
	}

	public String getSiren() {
		return siren;
	}

	public PJRestauration setSiren(String siren) {
		this.siren = siren;
		return this;
	}

	public String getAmbiance() {
		return ambiance;
	}

	public PJRestauration setAmbiance(String ambiance) {
		this.ambiance = ambiance;
		return this;
	}
	
	
}
