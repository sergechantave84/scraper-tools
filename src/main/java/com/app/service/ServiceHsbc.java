package com.app.service;

import java.io.BufferedInputStream;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;

import java.net.SocketTimeoutException;
import java.text.Normalizer;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import java.util.Scanner;

import org.jsoup.Jsoup;

import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import org.openqa.selenium.htmlunit.HtmlUnitDriver;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.app.model.HSBC;

import com.app.scrap.RepHSBC;

import com.app.utils.Parametres;
import com.app.utils.Utilitaires;

import fa.browserUtils.FactoryBrowser;
import fa.browserUtils.HtmlUnitBrowser;
import fa.log.LogApp.LogScrap;
import fa.log.config.LogConst;

@Service
public class ServiceHsbc extends Utilitaires {

	@Autowired
	private RepHSBC rep;

	String driverPath = "C:\\ChromeDriver\\";

	private List<HSBC> hsbcList = new ArrayList<>();
	String nomSITE = "HSBC";
	String ficval = "lien_hsbc";
	String dossierRESU = Parametres.REPERTOIRE_RESULT + "\\BQ\\" + nomSITE;;
	// TODO use page number
	String url_accueil = "https://agences.hsbc.fr";
	// String newUrl_accueil="https://agences.hsbc.fr/search?geo=&lat=&lon=&page=";
	String fichier_valeurs;
	String cp;
	String VilleSoumise;
	String ligne;

	int num_ligne = 2;
	int NbAgences;
	int numreponse = 1;
	int nbRecuperes;
	int num_ligneficval;
	int parcoursficval;
	int Resultats;
	int numPage = 100;

	static HtmlUnitDriver htmlUnitDriver;

	BufferedWriter sortie;
	BufferedWriter sortie_log;

	Date Temps_Fin;

	public HSBC saveData(HSBC t) {

		return rep.save(t);

	}

	public List<HSBC> saveDataAll(List<HSBC> t) {

		return rep.saveAll(t);

	}

	public List<HSBC> getElmnt() {

		return rep.findAll();
	}

	public HSBC getById(int id) {

		return rep.findById(id).orElse(null);
	}

	public String deleteProduct(int id) {
		try {
			rep.deleteById(id);

		} catch (Exception e) {
			e.printStackTrace();
		}

		return "removed";
	}

	public HSBC updateProduct(HSBC t) {
		// HSBCODO Auto-generated method stub
		return null;
	}

	public HSBC setData(String RegSoumise, String nbAgences, String nomWeb, String adresse, String tel, String fax,
			String horairesHtm, String horairestxt, String agenceHtm) {
		HSBC hsbc = new HSBC();
		hsbc.setRegSoumise(RegSoumise).setNbAgences(nbAgences).setNomWeb(nomWeb).setAdresse(adresse).setTel(tel)
				.setFax(fax).setHorairesHtm(horairesHtm).setHorairesTxt(horairestxt).setAgenceHtm(agenceHtm);
		this.saveData(hsbc);
		return hsbc;
	}

	public List<HSBC> showFirstRows() {
		List<HSBC> a = rep.getFirstRow(100, HSBC.class);
		return a;
	}

	public List<HSBC> showLastRows() {
		List<HSBC> a = rep.getLastRow(100, HSBC.class);
		return a;
	}

	public void setup() {
		FactoryBrowser fact = new FactoryBrowser();
		HtmlUnitBrowser hB = (HtmlUnitBrowser) fact.create("htmlUnit");
		htmlUnitDriver = hB.setOptions(false, false, false, 60000).initBrowserDriver();
	}

	public List<HSBC> scrapHSBC() throws IOException, InterruptedException {

		List<String> agenciesLinks = new ArrayList<>();

		fichier_valeurs = Parametres.REPERTOIRE_DATA_SOURCES + ficval + ".txt";
		Scanner sc = new Scanner(new BufferedInputStream(new FileInputStream(new File(fichier_valeurs))));

		if (!new File(dossierRESU).exists()) {
			new File(dossierRESU).mkdirs();
		}
		fichier = new File(dossierRESU + "\\" + "RESULTATS_" + nomSITE + "_" + System.currentTimeMillis() + ".txt");
		LogScrap log = new LogScrap("BanqueHSBC.txt", LogConst.TYPE_LOG_FILE);
		num_ligneficval = ((new File(LogConst.STORE_LOG_PATH + LogConst.PREFIX_LOG_FILE + "_BanqueHSBC.txt")).exists())
				? Integer.parseInt(log.readLog().getLastLineProcessed().split("\\t")[4])
				: 1;

		parcoursficval = 1;
		while (sc.hasNextLine()) {

			String lines[] = sc.nextLine().split("\\t");
			if (parcoursficval > num_ligneficval) {

				try {
					htmlUnitDriver.get(lines[1]);

				} catch (Exception e) {
					if (e instanceof SocketTimeoutException) {
						htmlUnitDriver.get(url_accueil);

					}
				}
				String pageSource = htmlUnitDriver.getPageSource();
				Document doc = Jsoup.parse(pageSource);
				Elements agenciesLinksElmnts = doc.select("#lf-body-wrapper > div.lf > div > "
						+ "div.lf-geo-divisions__results > div.lf-geo-divisions__results__content > "
						+ "div.lf-geo-divisions__results__content__locations > "
						+ "div.lf-geo-divisions__results__content__locations__list > div > h2 > a");
				if (!agenciesLinksElmnts.isEmpty()) {
					if (!agenciesLinks.isEmpty())
						agenciesLinks.clear();
					for (Element agencyLinkElmnt : agenciesLinksElmnts)
						agenciesLinks.add(agencyLinkElmnt.attr("href"));
				}
				NbAgences = agenciesLinks.size();
				for (String agencyLink : agenciesLinks) {
					try {
						htmlUnitDriver.get(url_accueil + agencyLink);

					} catch (Exception e) {
						if (e instanceof SocketTimeoutException) {
							htmlUnitDriver.get(url_accueil);

						}
					}
					pageSource = htmlUnitDriver.getPageSource();
					doc = Jsoup.parse(pageSource);
					String agencesHTML = "", nomAgence = "", adresse = "", tel = "", fax = "", horairesHTM = "",
							horairesTXT = "";

					agencesHTML = pageSource.replaceAll("\\s{1,}", "");
					Element agencyCoordinateElement = doc.selectFirst("#lf-body-wrapper > div.lf > div > "
							+ "div.lf-location__cover > div > div.lf-location__cover__background__text");
					if (agencyCoordinateElement != null) {
						Element agencyNameElmnt = agencyCoordinateElement.selectFirst("h1");
						nomAgence = (agencyNameElmnt != null) ? Normalizer.normalize(agencyNameElmnt.text(),Normalizer.Form.NFKD).replaceAll("[^\\p{ASCII}]","") : "";
						System.out.println("nom " + nomAgence);

						Element adresseElement = agencyCoordinateElement.selectFirst("#locations-address-default");
						adresse = (adresseElement != null) ? Normalizer.normalize(adresseElement.text(),Normalizer.Form.NFKD).replaceAll("[^\\p{ASCII}]","") : "";
						System.out.println("adresse " + adresse);

						Element telElement = agencyCoordinateElement.selectFirst("#lf-location-phone > a");
						tel = (telElement != null) ? telElement.attr("href").replaceAll("[^0-9 ]","") : "";
						System.out.println("tel " + tel);

					}

					Element agencyOpeningHoursElement = doc.selectFirst("#lf-openinghours > div");
					horairesHTM = agencyOpeningHoursElement.outerHtml().replaceAll("\\s{1,}", "");
					horairesTXT = (agencyOpeningHoursElement != null) ? Normalizer.normalize(agencyOpeningHoursElement.text(),Normalizer.Form.NFKD).replaceAll("[^\\p{ASCII}]","") : "";
					System.out.println("horaires " + horairesTXT);

					this.setData(lines[0], String.valueOf(NbAgences), nomAgence, adresse, tel, fax, horairesHTM,
							horairesTXT, agencesHTML);

				}
				log.writeLog(new Date(), lines[0], "no_error", NbAgences, parcoursficval, 0);

			}
			parcoursficval++;
		}
		sc.close();
		return hsbcList;
	}

	public void enregistrer(String agences_HTM, String nom_agence, String adresse, String tel, String fax,
			String HorairesHTM, String HorairesTXT, String cadre_infos) {

//	    	this.fichier = new File(Parametres.REPERTOIRE + "RESULTATS_BNP.");
		try {

			sortie = new BufferedWriter(new FileWriter(fichier, true));
			sortie.write(cp + "\t" + NbAgences + "\t" + nom_agence + "\t" + adresse + "\t" + tel + "\t" + fax + "\t"
					+ HorairesHTM + "\t" + HorairesTXT + "\t" + cadre_infos + "\t" + agences_HTM + "\r\n");

			sortie.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	/**
	 * La m�thode <code>initialiser()</code> permet d'ajouter la ligne d'ent�te dans
	 * le fichier r�sultat.
	 */

	public void initialiser() {

		try {
			// Attention pas d'accent dans les noms de colonnes !
			sortie = new BufferedWriter(new FileWriter(fichier, true));
			sortie.write("DepSoumis" + "\t" + "NbAgences" + "\t" + "NOMWEB" + "\t" + "ADRESSE" + "\t" + "TEL" + "\t"
					+ "FAX" + "\t" + "HORAIRESHTM" + "\t" + "HORAIRESTXT" + "\t" + "CADRE_INFOS" + "\t" + "AGENCEHTM"
					+ "\r\n");
			sortie.close();

			File toto = new File(dossierRESU + "\\" + "LOG_" + nomSITE + ".csv");
			if (!toto.exists()) {
				// if (num_ligneficval == num_ligne) {
				sortie_log = new BufferedWriter(new FileWriter(log, true));
				sortie_log.write("NumLigne" + ";" + "FinValeur" + ";" + "DepSoumis" + ";" + "NbAgences" + ";"
						+ "Nbrecuperes" + ";" + "Reste" + ";" + "Heure_Fin" + "\r\n");
				sortie_log.close();
			}

		} catch (IOException e) {

			e.printStackTrace();
		}
	}

	public void tearDown() {
		htmlUnitDriver.quit();

	}
}
