package com.app.scrap;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.app.model.CCOOP;

@Repository
public interface RepCcoop extends JpaRepository<CCOOP,Integer>,RepositoryCustom<CCOOP> {

}
