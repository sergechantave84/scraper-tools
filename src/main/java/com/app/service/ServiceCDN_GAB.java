package com.app.service;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.SocketTimeoutException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Scanner;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.PageLoadStrategy;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.app.model.CDN_GAB;

import com.app.scrap.RepCDN_GAB;
import com.app.utils.BrowserUtils;
import com.app.utils.Parametres;
import com.app.utils.Utilitaires;

import fa.browserUtils.ChromeBrowser;
import fa.browserUtils.FactoryBrowser;
import fa.browserUtils.HtmlUnitBrowser;
import fa.log.LogApp.LogScrap;
import fa.log.config.LogConst;

@Service
public class ServiceCDN_GAB extends Utilitaires {

	@Autowired
	private RepCDN_GAB rep;

	private static String driverPath = "C:\\ChromeDriver\\";

	private List<com.app.model.CDN_GAB> cdnGabList = new ArrayList<>();
	String nomSITE = "CDN_GAB";
	String ficval = "Departement_CE";
	String dossierRESU = Parametres.REPERTOIRE_RESULT + "\\BQ\\" + nomSITE;;
	String url_accueil = "https://agences.groupe-credit-du-nord.com/banque-assurance/credit-du-nord/";
	String fichier_valeurs;
	String cp;
	String ligne;

	int num_ligne = 2;
	int NbAgences;
	int numreponse = 1;
	int GABSG;
	int GABCDN;
	int DAB = 1;
	int HORS_SITE;
	int nbRecuperes;
	int num_ligneficval;
	int parcoursficval;
	int Resultats;

	static ChromeDriver chromeDriver;
	BufferedWriter sortie;
	BufferedWriter sortie_log;
	
	private HtmlUnitDriver htmlUnitDriver;

	Date Temps_Fin;

	public CDN_GAB saveData(CDN_GAB t) {

		return rep.save(t);

	}

	public List<CDN_GAB> saveDataAll(List<CDN_GAB> t) {

		return rep.saveAll(t);

	}

	public List<CDN_GAB> getElmnt() {

		return rep.findAll();
	}

	public CDN_GAB getById(int id) {

		return rep.findById(id).orElse(null);
	}

	public String deleteProduct(int id) {
		try {
			rep.deleteById(id);

		} catch (Exception e) {
			e.printStackTrace();
		}

		return "removed";
	}

	public CDN_GAB updateProduct(CDN_GAB t) {
		// CDN_GABODO Auto-generated method stub
		return null;
	}

	public CDN_GAB setData(String depSoumis, String nbAgences, String nomWeb, String adresse, String gabCDN,
			String gabSG, String dab, String horsSite, String agenceHtm) {
		CDN_GAB cdn = new CDN_GAB();
		cdn.setDepSoumis(depSoumis).setNbAgences(nbAgences).setNomWeb(nomWeb).setAdresse(adresse).setGabCDN(gabCDN)
				.setGabSG(gabSG).setDab(dab).setHorsSite(horsSite).setAgenceHtm(agenceHtm);
		this.saveData(cdn);
		return cdn;
	}

	public List<CDN_GAB> showFirstRows() {
		List<CDN_GAB> a = rep.getFirstRow(100, CDN_GAB.class);
		return a;
	}

	public List<CDN_GAB> showLastRows() {
		List<CDN_GAB> a = rep.getLastRow(100, CDN_GAB.class);
		return a;
	}

	public void setup() {
		
		FactoryBrowser fact = new FactoryBrowser();
		HtmlUnitBrowser hB = (HtmlUnitBrowser) fact.create("htmlUnit");
		htmlUnitDriver = hB.setOptions(false, false, false, 60000).initBrowserDriver();

		/*FactoryBrowser factBrowser = new FactoryBrowser();
		ChromeBrowser b = (ChromeBrowser) factBrowser.create("chrome");
		
		final String binaryPath = "C:\\Program Files\\Google\\Chrome Beta\\Application\\chrome.exe";
		
		chromeDriver = b.setOptions(PageLoadStrategy.NORMAL, driverPath, 60000, 60000, 60000, false, false, binaryPath)
				.initBrowserDriver();*/
	}

	public List<com.app.model.CDN_GAB> scrapCDNGAb() throws IOException, InterruptedException {
		boolean documentReadyState = false;
		JavascriptExecutor jsExecutor = (JavascriptExecutor) chromeDriver;
		Document doc = null;
		fichier_valeurs = Parametres.REPERTOIRE_DATA_SOURCES + ficval + ".txt";
		Scanner sc = new Scanner(new BufferedInputStream(new FileInputStream(new File(fichier_valeurs))));

		if (!new File(dossierRESU).exists()) {
			new File(dossierRESU).mkdirs();
		}
		fichier = new File(dossierRESU + "\\" + "RESULTATS_" + nomSITE + "_" + System.currentTimeMillis() + ".txt");
		LogScrap log = new LogScrap("BanqueCDN_GAB.txt", LogConst.TYPE_LOG_FILE);
		num_ligneficval = ((new File(LogConst.STORE_LOG_PATH + LogConst.PREFIX_LOG_FILE + "_BanqueCDN_GAB.txt"))
				.exists()) ? Integer.parseInt(log.readLog().getLastLineProcessed().split("\\t")[4]) : 1;

		parcoursficval = 1;
		while (sc.hasNextLine()) {

			String line = sc.nextLine();
			String[] fic_valeurs = line.split("\\t");
			
			String path = "https://agences.sg.fr/banque-assurance/distributeur/";

			if (parcoursficval > num_ligneficval) {
				cp = fic_valeurs[1].split("/")[2];
				String code_postal = fic_valeurs[0].split(" ")[0];
				System.out.println("CP :"+cp);
				String final_url = path+cp;
				try {
					htmlUnitDriver.get(final_url);

				} catch (Exception e) {
					if (e instanceof SocketTimeoutException) {
						htmlUnitDriver.get(final_url);

					}
				}
				//documentReadyState = this.listensIfPageFullyLoaded(htmlUnitDriver);
				//Thread.sleep(2000);
				//if (documentReadyState) {
					/*String jsScript = "";
					WebElement radioBtn = chromeDriver
							.findElement(By.cssSelector("#searchform > fieldset:nth-child(4) > div.field.checkbox.searchdab > label"));
					jsScript = String.format("arguments[0].click()", "");
					jsExecutor.executeScript(jsScript, radioBtn);

					WebElement input = chromeDriver.findElement(By.cssSelector("#searchcity"));
					jsScript = String.format("arguments[0].value=\"%s\"", cp);
					jsExecutor.executeScript(jsScript, input);
					
					if(chromeDriver.findElement(By.cssSelector("#dlg-popin-0 > button")) != null) {
						chromeDriver.findElement(By.cssSelector("#dlg-popin-0 > button")).click();
					}
					
					if(chromeDriver.findElement(By.cssSelector("#popin_tc_privacy_button_3")) != null) {
						chromeDriver.findElement(By.cssSelector("#popin_tc_privacy_button_3")).click();
					}
					
					WebElement btnSearch = chromeDriver
							.findElement(By.cssSelector("#searchform > fieldset.bloc.bloc-search > button"));
					btnSearch.click();*/

					//documentReadyState = this.listensIfPageFullyLoaded(chromeDriver);
					//if (documentReadyState) {
						//Thread.sleep(9000);
						String pageSource = htmlUnitDriver.getPageSource();
						
						doc = Jsoup.parse(pageSource);
						Element rootJElement = doc.select("#wrapper").first();
						if (!rootJElement.outerHtml()
								.contains("Nous ne disposons pas d'agence proche du secteur recherché")) {
							Elements agencyListJElement = rootJElement.select("#agencies > li");
							nbRecuperes = 0;
							NbAgences = agencyListJElement.size();
							for (Element tmp : agencyListJElement) {
								String agences_HTM = "", nom_agence = "", adresse = "";
								agences_HTM = tmp.outerHtml();
								agences_HTM = agences_HTM.replaceAll("\\s{1,}", "");
								// MISE EN EVIDENCE DES GAB SG ET CDN
								
								System.out.println(agences_HTM);
								
								GABSG = 0;
								GABCDN = 0;
								// retrieve agency name
								Element agencyNameJElement = tmp.select("div > div.agencylabel > a").first();
								nom_agence = (agencyNameJElement != null) ? this.normalize(agencyNameJElement.text())
										: "";
								if (nom_agence.toLowerCase().contains("distributeur")) {
									GABSG = 1;
								} else {
									GABCDN = 1;
								}
								// retrieve agency Addresse
								Element agencyAddressJElement = tmp.select("div > div.agencyaddress").first();
								adresse = (agencyAddressJElement != null) ? this.normalize(agencyAddressJElement.text())
										: "";
								if (GABCDN == 1) {
									if (agences_HTM.contains("<divclass=\"links\">")) {
										HORS_SITE = 0;
									} else {
										HORS_SITE = 1;
									}
								}
								
								System.out.println("Nom : "+nom_agence);
								System.out.println("Adresse :"+adresse);
								System.out.println("GABSG : "+GABSG);
								System.out.println("GABCDN : "+GABCDN);
								System.out.println("HORS_SITE : "+HORS_SITE);

								nbRecuperes++;
								if(nom_agence!="") {
									
									cdnGabList.add(this.setData(code_postal, String.valueOf(NbAgences), nom_agence, adresse,
											String.valueOf(GABCDN), String.valueOf(GABSG), String.valueOf(DAB),
											String.valueOf(HORS_SITE), agences_HTM));
								}
								
								System.out.println("\n-----------------------\n");
							}

						}
					//}
				//}
				log.writeLog(new Date(), cp, "no error", NbAgences, parcoursficval, NbAgences - nbRecuperes);
			}
			parcoursficval++;
		}
		sc.close();
		return cdnGabList;
	}

	public void enregistrer(String agences_HTM, String nom_agence, String adresse) {

//	    	this.fichier = new File(Parametres.REPERTOIRE + "RESULTATS_BNP.");
		try {

			sortie = new BufferedWriter(new FileWriter(fichier, true));
			sortie.write(cp + "\t" + NbAgences + "\t" + nom_agence + "\t" + adresse + "\t" + GABCDN + "\t" + GABSG
					+ "\t" + DAB + "\t" + HORS_SITE + "\t" + agences_HTM + "\r\n");

			sortie.close();
		} catch (IOException e) {

			e.printStackTrace();
		}

	}

	public void enregistrer_journal(int numreponse) {

//	    	this.fichier = new File(Parametres.REPERTOIRE + "RESULTATS_BNP.");
		try {
			Temps_Fin = new Date();
			SimpleDateFormat formater = null;
			formater = new SimpleDateFormat("'le' dd/MM/yyyy '�' hh:mm:ss");
			String date = formater.format(Temps_Fin);
			date = formater.format(Temps_Fin);

			sortie_log = new BufferedWriter(new FileWriter(log, true));
			sortie_log.write(parcoursficval + ";" + "FinValeur" + ";" + cp + ";" + this.NbAgences + ";" + nbRecuperes
					+ ";" + (this.NbAgences - nbRecuperes) + ";" + date + "\r\n");
			sortie_log.close();
			long taille_fichier = fichier.length() / 1000000; // la length est en octets, on divise par 1000 pour mettre
																// en Ko et par 1000000 pour mettre en Mo

			if (taille_fichier > 8) {
				// INITIALISATION FICHIER DE RESULTAT
				this.fichier = new File(
						dossierRESU + "\\" + "RESULTATS_" + nomSITE + "_" + System.currentTimeMillis() + ".txt");
				// Attention pas d'accent dans les noms de colonnes !
				sortie = new BufferedWriter(new FileWriter(fichier, true));
				sortie.write("CPSoumis" + "\t" + "NbAgences" + "\t" + "NOMWEB" + "\t" + "ADRESSE" + "\t" + "GABCDN"
						+ "\t" + "GABSG" + "\t" + "DAB" + "\t" + "HORS_SITE" + "\t" + "AGENCEHTM" + "\r\n");
				sortie.close();
			}

		} catch (IOException e) {

			e.printStackTrace();
		}

	}

	/**
	 * La m�thode <code>initialiser()</code> permet d'ajouter la ligne d'ent�te dans
	 * le fichier r�sultat.
	 */

	public void initialiser() {

		try {
			// Attention pas d'accent dans les noms de colonnes !
			sortie = new BufferedWriter(new FileWriter(fichier, true));
			sortie.write("DepSoumis" + "\t" + "NbAgences" + "\t" + "NOMWEB" + "\t" + "ADRESSE" + "\t" + "GABCDN" + "\t"
					+ "GABSG" + "\t" + "DAB" + "\t" + "HORS_SITE" + "\t" + "AGENCEHTM" + "\r\n");
			sortie.close();

			File toto = new File(dossierRESU + "\\" + "LOG_" + nomSITE + ".csv");
			if (!toto.exists()) {
				// if (num_ligneficval == num_ligne) {
				sortie_log = new BufferedWriter(new FileWriter(log, true));
				sortie_log.write("NumLigne" + ";" + "BIDON" + ";" + "DepSoumis" + ";" + "NbAgences" + ";"
						+ "Nbrecuperes" + ";" + "Reste" + ";" + "Heure_Fin" + "\r\n");
				sortie_log.close();
			}

		} catch (IOException e) {

			e.printStackTrace();
		}
	}

	public void tearDown() {
		chromeDriver.quit();

	}
}
