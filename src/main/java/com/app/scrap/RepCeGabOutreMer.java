package com.app.scrap;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.app.model.CE_GAB_Outre_Mer;

@Repository
public interface RepCeGabOutreMer extends JpaRepository<CE_GAB_Outre_Mer, Integer>,RepositoryCustom<CE_GAB_Outre_Mer>{

}
