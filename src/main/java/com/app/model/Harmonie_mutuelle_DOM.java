package com.app.model;

import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="harmonie_mutelle_dom", schema="scrap")
public class Harmonie_mutuelle_DOM {

	
	@Id
	@SequenceGenerator(name="scrap.harmonie_mutelle_dom_id_seq",
	                    sequenceName ="scrap.harmonie_dom_mutelle_id_seq",
	                     allocationSize=1)
    @GeneratedValue( strategy=GenerationType.SEQUENCE,
                     generator="scrap.harmonie_mutelle_dom_id_seq")
	  
	@Column(name="id",updatable=false)
	private int id;
	
	@Column(name="nb_pages", columnDefinition="varchar")
	private String nbPages;
	
	@Column(name="num_page", columnDefinition="varchar")
	private String numPage;
	
	@Column(name="nbagences_page", columnDefinition="varchar")
	private String nbAgencePage;
	
	@Column(name="nom_agence", columnDefinition="varchar")
	private String nomAgence;
	
	@Column(name="adresse", columnDefinition="varchar")
	private String adresse;
	
	@Column(name="tel", columnDefinition="varchar")
	private String tel;
	
	@Column(name="fax", columnDefinition="varchar")
	private String fax;
	
	@Column(name="horaires", columnDefinition="varchar")
	private String horaires;
	
	@Column(name="agencehtm", columnDefinition="varchar")
	private String agenceHtm;
	
	@Column(name="annee_du_scrap" ,columnDefinition="varchar(4) DEFAULT date_part('year',CURRENT_DATE)")
	private String anneeScrap;

	public Harmonie_mutuelle_DOM() {
		LocalDate ld= new Date().toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
		anneeScrap=String.valueOf(ld.getYear()+1);
	}
	public int getId() {
		return id;
	}

	public Harmonie_mutuelle_DOM setId(int id) {
		this.id = id;
		return this;
	}

	public String getNbPages() {
		return nbPages;
	}

	public Harmonie_mutuelle_DOM setNbPages(String nbPages) {
		this.nbPages = nbPages;
		return this;
	}

	public String getNumPage() {
		return numPage;
	}

	public Harmonie_mutuelle_DOM setNumPage(String numPage) {
		this.numPage = numPage;
		return this;
	}

	public String getNbAgencePage() {
		return nbAgencePage;
	}

	public Harmonie_mutuelle_DOM setNbAgencePage(String nbAgencePage) {
		this.nbAgencePage = nbAgencePage;
		return this;
	}

	public String getNomAgence() {
		return nomAgence;
	}

	public Harmonie_mutuelle_DOM setNomAgence(String nomAgence) {
		this.nomAgence = nomAgence;
		return this;
	}

	public String getAdresse() {
		return adresse;
	}

	public Harmonie_mutuelle_DOM setAdresse(String adresse) {
		this.adresse = adresse;
		return this;
	}

	public String getTel() {
		return tel;
	}

	public Harmonie_mutuelle_DOM setTel(String tel) {
		this.tel = tel;
		return this;
	}

	public String getFax() {
		return fax;
	}

	public Harmonie_mutuelle_DOM setFax(String fax) {
		this.fax = fax;
		return this;
	}

	public String getHoraires() {
		return horaires;
	}

	public Harmonie_mutuelle_DOM setHoraires(String horaires) {
		this.horaires = horaires;
		return this;
	}

	public String getAgenceHtm() {
		return agenceHtm;
	}

	public Harmonie_mutuelle_DOM setAgenceHtm(String agenceHtm) {
		this.agenceHtm = agenceHtm;
		return this;
	}

	public String getAnneeScrap() {
		return anneeScrap;
	}

	public Harmonie_mutuelle_DOM setAnneeScrap(String anneeScrap) {
		this.anneeScrap = anneeScrap;
		return this;
	}
}
