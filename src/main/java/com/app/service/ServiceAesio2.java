package com.app.service;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.net.SocketTimeoutException;
import java.text.Normalizer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Scanner;

import org.hibernate.mapping.Array;
import org.json.JSONArray;
import org.json.JSONObject;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.openqa.selenium.chrome.ChromeDriver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.app.model.Aviva;
import com.app.model.Aesio;
import com.app.model.PJAssuranceDep;
import com.app.scrap.AbeilleRepository;
import com.app.scrap.RepAesio;
import com.app.scrap.RepAesio;
import com.app.scrap.RepAesio;
import com.app.utils.Parametres;
import fa.log.LogApp.LogScrap;
import fa.log.config.LogConst;

@Service
public class ServiceAesio2 extends Scraper {

	@Autowired
	RepAesio rep;

	//private String nomSITE = "ABEILLE";
	private String ficval = "Aesio_dep";
	private String url_accueil = "https://agences.aesio.fr/fr/";

	private int lastLine =1;

	// private ch ch;
	private ChromeDriver ch;

	public Aesio saveData(Aesio t) {
		return rep.save(t);
	}

	public List<Aesio> saveDataAll(List<Aesio> t) {
		return rep.saveAll(t);
	}

	public List<Aesio> getElmnt() {

		return rep.findAll();
	}

	public Aesio getById(int id) {

		return (Aesio) rep.findById(id).orElse(null);
	}

	public String deleteProduct(int id) {
		try {
			rep.deleteById(id);

		} catch (Exception e) {
			e.printStackTrace();
		}

		return "removed";
	}

	public Aesio updateProduct(Aesio t) {
		// AvivaODO Auto-generated method stub
		return null;
	}

	public void setUp() throws Exception {
		/*
		 * FactoryBrowser factoryB = new FactoryBrowser(); HtmlUnitBrowser htmlB =
		 * (HtmlUnitBrowser) factoryB.create("htmlUnit"); ch =
		 * htmlB.setOptions(true, false, false, 60000).initBrowserDriver();
		 */
		final String chromeBeta = "C:\\Program Files\\Google\\Chrome Beta\\Application\\chrome.exe";
		ch = (ChromeDriver) this.setupCh(Parametres.CHROME_DRIVER_PATH, true, false,
				"C:\\Users\\GEOMADA PC4\\AppData\\Local\\Google\\Chrome Beta\\User Data", "", chromeBeta);
	}

	public void scrap() throws IOException, InterruptedException {

		Scanner sc = new Scanner(new BufferedInputStream(
				new FileInputStream(new File(Parametres.REPERTOIRE_DATA_SOURCES + ficval + ".txt"))));

		LogScrap log = new LogScrap("AssuranceAesio.txt", LogConst.TYPE_LOG_FILE);

		if ((new File(LogConst.STORE_LOG_PATH + LogConst.PREFIX_LOG_FILE + "_AssuranceAesio.txt").exists())) {
			Scanner sc3 = new Scanner(new BufferedInputStream(new FileInputStream(
					new File(LogConst.STORE_LOG_PATH + LogConst.PREFIX_LOG_FILE + "_AssuranceAesio.txt"))));
			while (sc3.hasNextLine()) {
				lastLine = Integer.parseInt(sc3.nextLine().split("\t")[4]);
			}
		}

		int currentLine = 1;

		JSONArray nom_agentsArray = null;
		
		

		while (sc.hasNextLine()) {
			
			String line = sc.nextLine();
			
			String [] lines = line.split("\\t");
			
			String depSoumis = lines[1];
			String region = lines[2];
			//String uri = line[1]
			String url = url_accueil + region +"/"+ depSoumis;
			
			//String regionSoumise = line[0];

			if (currentLine > lastLine) {

				System.out.println("Uri : " + url);

				ch.get(url);
				
				
				
				String pageSource = ch.getPageSource();

				pageSource = Normalizer.normalize(ch.getPageSource().toLowerCase(), Normalizer.Form.NFD)
						.replaceAll("[^\\p{ASCII}]", "");

				Document doc = Jsoup.parse(pageSource);

				Thread.sleep(3000);

				Element root = doc.selectFirst("#ito_ny_resultat");
				
				Element pagination = doc.selectFirst("#lf-pagination > nav > ul");
				
				if(pagination == null) {
					
					this.retrieveData(root, depSoumis);
					
				}else {
					Elements newHref = pagination.select("li");
					
					Element next = doc.selectFirst("#lf-pagination > nav > ul > li.lf-pagination-default__nav__list__item.lf-pagination-default__nav__list__item--next.lf-geo-divisions__main__content__wrapper__locations__pagination__nav__list__item > a");
					
					while(next != null) {
						
						String link = next.attr("href");
						
						ch.get(link);
						pageSource = ch.getPageSource();

						pageSource = Normalizer.normalize(ch.getPageSource().toLowerCase(), Normalizer.Form.NFD)
								.replaceAll("[^\\p{ASCII}]", "");
						doc = Jsoup.parse(pageSource);

						Thread.sleep(3000);

						root = doc.selectFirst("#ito_ny_resultat");
						
						//this.retrieveData(root, depSoumis);
						
						next = doc.selectFirst("#lf-pagination > nav > ul > li.lf-pagination-default__nav__list__item.lf-pagination-default__nav__list__item--next.lf-geo-divisions__main__content__wrapper__locations__pagination__nav__list__item > a");
						
					}
					
				}

				//log.writeLog(new Date(), depSoumis, "no_error", 1, currentLine, 0);

			}

			currentLine++;
		}
	}
	
	private void retrieveData(Element root, String depSoumis) {
		
		String nom_agents = "";
		String nom_agence = "";
		String adresse = "";
		String tel = "";
		String fax = "";
		String horaires = "";
		String ORIAS = "";
		String lien ="";
		
		String result = root != null ? root.attr("data-result") : null;

		JSONArray jsonArrayRes = null;
		JSONObject json = null;

		// System.out.println(result);

		if (result != null) {
			jsonArrayRes = new JSONArray(result);

			System.out.println("Nombre de json : " + jsonArrayRes.length());
			
			int nbAgence = jsonArrayRes.length();

			for (int i = 0; i < jsonArrayRes.length(); ++i) {

				json = jsonArrayRes.getJSONObject(i);

				//System.out.println("json : " + json);

				nom_agence = (json.has("nom")) ? json.getString("nom").toUpperCase() : "";
				adresse = (json.has("adresse")) ? json.getString("adresse").toUpperCase() : "";

				tel = (json.has("tel")) ? json.getString("tel") : "";
				
				lien = (json.has("lien")) ? json.getString("lien") : "";

				horaires = (json.has("horaires")) ? json.getString("horaires") : "";
				
				if(json.has("nom") && json.has("adresse")) {
					//this.saveItem(depSoumis,nom_agence,adresse,tel, lien,horaires, String.valueOf(nbAgence));
				}
				
				System.out.println("Nom : "+nom_agence);
				System.out.println("dep : "+depSoumis);
				System.out.println("adresse : "+adresse);
				System.out.println("telephone : "+tel);
				System.out.println("lien : "+lien);
				System.out.println("horaires : "+horaires);
				System.out.println("\n- - - - - - - - - - - - - - - - - - - - - - - - - - - - \n");

			}
		} else {
			System.err.println("Aucun resultat!");
		}
	}

	@Override
	public void saveItem(String... args) {

		Aesio aesio = new Aesio();
		
		aesio.setDepSoumis(args[0]).setNomWeb(args[1]).setAdresse(args[2])
				.setTelephone(args[3]).setLienAgence(args[4]).setHoraires(args[5]).setNombreAgence(args[6]);
		rep.save(aesio);
		
		
	}
	

	public void tearDown() {
		ch.quit();

	}

}
