package com.app.service;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.text.Normalizer;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Scanner;

import org.json.JSONObject;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.app.model.CCOOP;

import com.app.scrap.RepCcoop;

import com.app.utils.Parametres;
import com.app.utils.Utilitaires;

import fa.browserUtils.FactoryBrowser;
import fa.browserUtils.HtmlUnitBrowser;
import fa.log.LogApp.LogScrap;
import fa.log.config.LogConst;

@Service
public class ServiceCcoop extends Utilitaires {
	//https://agences.credit-cooperatif.coop/banque/agences/toutes-nos-agences
	@Autowired
	private RepCcoop rep;

	private List<CCOOP> ccoopList = new ArrayList<>();
	// private static String driverPath= "C:\\ChromeDriver\\";
	private String nomSITE = "CCOOP";
	private String ficval = "depBP";
	private String dossierRESU = Parametres.REPERTOIRE_RESULT + "\\BQ\\" + nomSITE;;
	// private String url_accueil =
	// "https://agences.credit-cooperatif.coop/banque/agences/";
	private String url_accueil = "https://agences.credit-cooperatif.coop/banque/agences/agences-";
	private String fichier_valeurs;
	private String cp;

	private int NbAgences;

	private int nbRecuperes;
	private int num_ligneficval;
	private int parcoursficval;

	private static HtmlUnitDriver htmlUnitDriver;
	private BufferedWriter sortie;
	private BufferedWriter sortie_log;

	private Date Temps_Fin;

	public CCOOP saveData(CCOOP t) {

		return rep.save(t);

	}

	public List<CCOOP> saveDataAll(List<CCOOP> t) {

		return rep.saveAll(t);

	}

	public List<CCOOP> getElmnt() {

		return rep.findAll();
	}

	public CCOOP getById(int id) {

		return rep.findById(id).orElse(null);
	}

	public String deleteProduct(int id) {
		try {
			rep.deleteById(id);

		} catch (Exception e) {
			e.printStackTrace();
		}

		return "removed";
	}
	
	public String deleteAll() {
		try {
			
			rep.deleteAll();
			System.out.println("Supprimee avec success");

		} catch (Exception e) {
			e.printStackTrace();
		}

		return "removed";
	}

	public CCOOP updateProduct(CCOOP t) {

		return null;
	}

	public CCOOP setData(String depSoumis, String total, String nbAgences, String enseigne, String nomWeb,
			String adresse, String tel, String fax, String horaireshtm, String horairestxt, String services, String dab,
			String agenceHtm) {
		CCOOP ccoop = new CCOOP();
		ccoop.setDepSoumis(depSoumis).setTotal(total).setNbAgences(nbAgences).setEnseigne(enseigne).setNomWeb(nomWeb)
				.setAdresse(adresse).setTel(tel).setFax(fax).setHoraireshtm(horaireshtm).setHorairestxt(horairestxt)
				.setServices(services).setDab(dab).setAgenceHtm(agenceHtm);
		this.saveData(ccoop);
		return ccoop;
	}

	public List<CCOOP> showFirstRows() {
		List<CCOOP> a = rep.getFirstRow(100, CCOOP.class);
		return a;
	}

	public List<CCOOP> showLastRows() {
		List<CCOOP> a = rep.getLastRow(100, CCOOP.class);
		return a;
	}

	public void setup() {

		FactoryBrowser fact = new FactoryBrowser();
		HtmlUnitBrowser hB = (HtmlUnitBrowser) fact.create("htmlUnit");
		htmlUnitDriver = hB.setOptions(false, false, false, 60000).initBrowserDriver();

	}

	public List<CCOOP> scrapCCOOP() throws IOException {

		fichier_valeurs = Parametres.REPERTOIRE_DATA_SOURCES + ficval + ".txt";
		Scanner sc = new Scanner(new BufferedInputStream(new FileInputStream(new File(fichier_valeurs))));

		if (!new File(dossierRESU).exists()) {
			new File(dossierRESU).mkdirs();
		}
		fichier = new File(dossierRESU + "\\" + "RESULTATS_" + nomSITE + "_" + System.currentTimeMillis() + ".txt");
		LogScrap log = new LogScrap("BanqueCCOOP.txt", LogConst.TYPE_LOG_FILE);
		num_ligneficval = ((new File(LogConst.STORE_LOG_PATH + LogConst.PREFIX_LOG_FILE + "_BanqueCCOOP.txt")).exists())
				? Integer.parseInt(log.readLog().getLastLineProcessed().split("\\t")[4])
				: 1;

		parcoursficval = 0;
		while (sc.hasNextLine()) {
			String line = sc.nextLine();
			String[] arrayTmp = line.split("\\t");
			if (parcoursficval >= num_ligneficval) {
				cp = arrayTmp[0];
				String uri_acc = url_accueil + cp;
				System.out.println("uri_acc : " + uri_acc);

				URL url = new URL(uri_acc);
				HttpURLConnection con = (HttpURLConnection) url.openConnection();
				con.setRequestMethod("GET");
				con.setRequestProperty("Content-Type", "text/html");
				con.setRequestProperty("Content-Encoding", "gzip");
				con.setRequestProperty("Vary", "Accept-Encoding");
				con.setRequestProperty("Content-Length", "0");
				con.setConnectTimeout(60000);
				con.setReadTimeout(60000);
				con.setInstanceFollowRedirects(true);

				BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream(), "ISO-8859-1"));
				String pageSource;
				StringBuilder strB = new StringBuilder();
				while ((pageSource = in.readLine()) != null) {
					strB.append(pageSource);
				}

				in.close();
				Document doc = Jsoup.parse(strB.toString());
				Element root = doc.selectFirst("body > script");

				String agencys[] = {};

				if (root.html().toString().contains("AddPoi")) {

					agencys = Normalizer.normalize(root.html().toString(), Normalizer.Form.NFD)
							.replaceAll("[^\\p{ASCII}]", "").split("AddPoi");
				} else {
					System.out.println("\n---------------------\nAucun agence trouve\n-------------------------------------------\n");
				}

				nbRecuperes = 0;

				int l = agencys.length;

				JSONObject scheduleJSON = null;
				JSONObject rdvScheduleJSON = null;
				JSONObject jsonDatas = null;
				JSONObject json = null;

				NbAgences = l;

				if (l > 0) {

					String agences_HTM = "", nom_agence = "", adresse = "", tel = "", fax = "", email = "",
							HorairesHTM = "", HorairesTXT = "", Services = "", enseigne = "", DAB = "";
					
					int total = 0;

					String zipCode = "", city = "", postalAdress = "", agencyId = "";

					for (int i = 0; i < l - 1; i++) {
						nom_agence = "";
						zipCode = "";
						city = "";
						adresse = "";
						postalAdress = "";
						tel = "";
						fax = "";
						HorairesHTM = "";
						HorairesTXT = "";
						agencyId = "";
						agences_HTM = "";

						String strJSON = agencys[i + 1].replaceAll("[\\(\\)]", "")
								.replaceAll("^['0-9,]{1,}\\{\"datas\"", "");
						strJSON = "{\"datas\"" + strJSON;

						json = new JSONObject(strJSON);
						jsonDatas = json.getJSONObject("datas");

						agences_HTM = jsonDatas.toString();

						nom_agence = (jsonDatas.has("label")) ? jsonDatas.getString("label") : "";
						nom_agence= this.normalize(nom_agence);
						System.out.println("le nom de l'agence : " + nom_agence);

						zipCode = (jsonDatas.has("zip")) ? jsonDatas.getString("zip") : "";
						city = (jsonDatas.has("city")) ? jsonDatas.getString("city") : "";
						adresse = (jsonDatas.has("address1"))
								? jsonDatas.getString("address1") + " " + zipCode + " " + city
								: "";
						adresse= this.normalize(adresse).toUpperCase().replaceAll(",", "");
						System.out.println("Adresse : " + adresse);

						agencyId = (jsonDatas.has("id")) ? jsonDatas.getString("id") : "";
						System.out.println("id : " + agencyId);

						postalAdress = (jsonDatas.has("postalAddress")) ? jsonDatas.getString("postalAddress") : "";
						System.out.println("postalAdress : " + postalAdress);

						tel = (jsonDatas.has("tel")) ? jsonDatas.getString("tel") : "";
						System.out.println("Tel : " + tel);

						fax = (jsonDatas.has("fax")) ? jsonDatas.getString("fax") : "";
						System.out.println("Fax : " + fax);
						
						DAB = (jsonDatas.has("dab")) ? jsonDatas.getString("dab") : "0";
						if(DAB == "") DAB = "0";
						System.out.println("DAB : " + DAB);

						scheduleJSON = (jsonDatas.has("schedule")) ? jsonDatas.getJSONObject("schedule") : null;
						rdvScheduleJSON = (jsonDatas.has("rdvSchedules")) ? jsonDatas.getJSONObject("rdvSchedules") : null;
						
						String lundi = scheduleJSON.getString("day1").toLowerCase().contains("h") ? scheduleJSON.getString("day1") : "";
						String mardi = scheduleJSON.getString("day2").toLowerCase().contains("h") ? scheduleJSON.getString("day2") : "";
						String mercredi = scheduleJSON.getString("day3").toLowerCase().contains("h") ? scheduleJSON.getString("day3") : "";
						String jeudi = scheduleJSON.getString("day4").toLowerCase().contains("h") ? scheduleJSON.getString("day4") : "";
						String vendredi = scheduleJSON.getString("day5").toLowerCase().contains("h") ? scheduleJSON.getString("day5") : "";
						String samedi = scheduleJSON.getString("day6").toLowerCase().contains("h") ? scheduleJSON.getString("day6") : "";
						String dimanche = scheduleJSON.getString("day7").toLowerCase().contains("h") ? scheduleJSON.getString("day7") : "";
						
						String lundiRdv = rdvScheduleJSON.getString("day1").toLowerCase().contains("h") ? " " + rdvScheduleJSON.getString("day1") + " (sur RDV)" : "";
						String mardiRdv = rdvScheduleJSON.getString("day2").toLowerCase().contains("h") ? " " + rdvScheduleJSON.getString("day2") + " (sur RDV)" : "";
						String mercrediRdv = rdvScheduleJSON.getString("day3").toLowerCase().contains("h") ? " " + rdvScheduleJSON.getString("day3") + " (sur RDV)" : "";
						String jeudiRdv = rdvScheduleJSON.getString("day4").toLowerCase().contains("h") ? " " + rdvScheduleJSON.getString("day4") + " (sur RDV)" : "";
						String vendrediRdv = rdvScheduleJSON.getString("day5").toLowerCase().contains("h") ? " " + rdvScheduleJSON.getString("day5") + " (sur RDV)" : "";
						String samediRdv = rdvScheduleJSON.getString("day6").toLowerCase().contains("h") ? " " + rdvScheduleJSON.getString("day6") + " (sur RDV)" : "";
						String dimancheRdv = rdvScheduleJSON.getString("day7").toLowerCase().contains("h") ? " " + rdvScheduleJSON.getString("day7") + " (sur RDV)" : "";
						
						lundi = (lundi + lundiRdv).length() > 0 ? lundi + lundiRdv:"Fermee";
						mardi = (mardi + mardiRdv).length() > 0 ? mardi + mardiRdv:"Fermee";
						mercredi = (mercredi + mercrediRdv).length() > 0 ? mercredi + mercrediRdv:"Fermee";
						jeudi = (jeudi + jeudiRdv).length() > 0 ? jeudi + jeudiRdv:"Fermee";
						vendredi = (vendredi + vendrediRdv).length() > 0 ? vendredi + vendrediRdv:"Fermee";
						samedi = (samedi + samediRdv).length() > 0 ? samedi + samediRdv:"Fermee";
						dimanche = (dimanche + dimancheRdv).length() > 0 ? dimanche + dimancheRdv:"Fermee";
						
						if (scheduleJSON != null) {
							HorairesHTM = scheduleJSON.toString() + " " + rdvScheduleJSON.toString();
							HorairesTXT = "Lundi : " + lundi + " Mardi : "
									+ mardi + " Mercredi : " + mercredi
									+ " Jeudi : " + jeudi + " Vendredi : "
									+ vendredi + " Samedi : " + samedi + " Dimanche : " + dimanche;
						}
						
						HorairesTXT= this.normalize(HorairesTXT);
						
						System.out.println("HorairesTXT : " + HorairesTXT);
						
						total = NbAgences;
						
						// ENSEIGNE
						enseigne = "CREDIT COOPERATIF";

						/*ccoopList.add(this.setData(cp, String.valueOf(total), String.valueOf(NbAgences), enseigne,
								nom_agence, adresse, tel, fax, HorairesHTM, HorairesTXT, Services, DAB,
								agences_HTM));*/
						
						System.out.println("\n----------------------------------\n ");

						nbRecuperes++;
					}
				}

				//log.writeLog(new Date(), uri_acc, "no_error", NbAgences, parcoursficval, NbAgences - nbRecuperes);
			}
			parcoursficval++;
		}
		return ccoopList;

	}

	public void tearsDown() {
		htmlUnitDriver.quit();
	}

	public void enregistrer(int total, String agences_HTM, String nom_agence, String adresse, String tel, String fax,
			String HorairesHTM, String HorairesTXT, String Services, String enseigne, int DAB) {

		try {

			sortie = new BufferedWriter(new FileWriter(fichier, true));
			sortie.write(cp + "\t" + NbAgences + "\t" + enseigne + "\t" + nom_agence + "\t" + adresse + "\t" + tel
					+ "\t" + fax + "\t" + HorairesHTM + "\t" + HorairesTXT + "\t" + Services + "\t" + DAB + "\t"
					+ agences_HTM + "\r\n");

			sortie.close();
		} catch (IOException e) {

			e.printStackTrace();
		}

	}

	public void enregistrer_journal(int numreponse) {

//	    	this.fichier = new File(Parametres.REPERTOIRE + "RESULTATS_BNP.");
		try {
			Temps_Fin = new Date();
			SimpleDateFormat formater = null;
			formater = new SimpleDateFormat("'le' dd/MM/yyyy '�' hh:mm:ss");
			String date = formater.format(Temps_Fin);
			date = formater.format(Temps_Fin);

			sortie_log = new BufferedWriter(new FileWriter(log, true));
			sortie_log.write(parcoursficval + ";" + "FinValeur" + ";" + cp + ";" + this.NbAgences + ";" + nbRecuperes
					+ ";" + (this.NbAgences - nbRecuperes) + ";" + date + "\r\n");
			sortie_log.close();
			long taille_fichier = fichier.length() / 1000000; // la length est en octets, on divise par 1000 pour mettre
																// en Ko et par 1000000 pour mettre en Mo

			if (taille_fichier > 8) {
				// INITIALISATION FICHIER DE RESULTAT
				this.fichier = new File(
						dossierRESU + "\\" + "RESULTATS_" + nomSITE + "_" + System.currentTimeMillis() + ".txt");
				// Attention pas d'accent dans les noms de colonnes !
				sortie = new BufferedWriter(new FileWriter(fichier, true));
				sortie.write("CodePostaleSoumise" + "\t" + "TOTAL" + "\t" + "NbAgences" + "\t" + "ENSEIGNE" + "\t"
						+ "NOMWEB" + "\t" + "ADRESSE" + "\t" + "TEL" + "\t" + "FAX" + "\t" + "HORAIRESHTM" + "\t"
						+ "HORAIRESTXT" + "\t" + "SERVICES" + "\t" + "DAB" + "\t" + "AGENCEHTM" + "\r\n");
				sortie.close();
			}

		} catch (IOException e) {

			e.printStackTrace();
		}

	}

	/**
	 * La m�thode <code>initialiser()</code> permet d'ajouter la ligne d'ent�te dans
	 * le fichier r�sultat.
	 */

	public void initialiser() {

		try {
			// Attention pas d'accent dans les noms de colonnes !
			sortie = new BufferedWriter(new FileWriter(fichier, true));
			sortie.write("DepSoumis" + "\t" + "TOTAL" + "\t" + "NbAgences" + "\t" + "ENSEIGNE" + "\t" + "NOMWEB" + "\t"
					+ "ADRESSE" + "\t" + "TEL" + "\t" + "FAX" + "\t" + "HORAIRESHTM" + "\t" + "HORAIRESTXT" + "\t"
					+ "SERVICES" + "\t" + "DAB" + "\t" + "AGENCEHTM" + "\r\n");
			sortie.close();

			File toto = new File(dossierRESU + "\\" + "LOG_" + nomSITE + ".csv");
			if (!toto.exists()) {
				// if (num_ligneficval == num_ligne) {
				sortie_log = new BufferedWriter(new FileWriter(log, true));
				sortie_log.write("NumLigne" + ";" + "Recherche" + ";" + "DepSoumis" + ";" + "NbAgences" + ";"
						+ "Nbrecuperes" + ";" + "Reste" + ";" + "Heure_Fin" + "\r\n");
				sortie_log.close();
			}

		} catch (IOException e) {

			e.printStackTrace();
		}
	}

}
