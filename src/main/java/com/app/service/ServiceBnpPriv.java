package com.app.service;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;

import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.Date;
import java.util.Scanner;

import javax.net.ssl.HttpsURLConnection;
import javax.persistence.Column;

import org.apache.commons.text.StringEscapeUtils;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.app.model.BNP_PRIV;
import com.app.scrap.RepBnpPriv;
import com.app.utils.Parametres;

import fa.log.LogApp.LogScrap;
import fa.log.config.LogConst;

@Service
public class ServiceBnpPriv extends Scraper {

	@Autowired
	RepBnpPriv rep;

	//https://agences.mabanque.bnpparibas/engineV2/?media=mobile&bank=BNPP&lang=FR&enc=json&crit=agence&nbag=5&ag=allpoi
	private String strURL = "https://agences.mabanque.bnpparibas/engineV2/?media=mobile&bank=BNPBPF&lang=FR&enc=json&crit=agencebpf&nbag=5&ag=";
	//private String strURL = "https://agences.bnpparibas/engineV2/?media=mobile&bank=BNPBPF&lang=FR&enc=json&crit=agencebpf&nbag=5&ag=";
	private String ficval = "bnp_prive.txt";

	@Override
	public void saveItem(String... args) {
		BNP_PRIV bnpP = new BNP_PRIV();
		bnpP.setAgencyName(args[0]).setAdresse(args[1]).setSvhedule(args[2]).setAgencyType(args[3])
				.setAutomateBillet(args[4]).setAutomateCheque(args[5]).setChange(args[6]).setCoffreFort(args[7])
				.setDep(args[8]).setEntreprise(args[9]).setEspaceLibreService(args[10]).setExpertiseAccess(args[11])
				.setExpressZone(args[12]).setFax(args[13]).setGab(args[14]).setGabVocal(args[15])
				.setImprLibreServ(args[16]).setParticulier(args[17]).setProfessional(args[18]).setRegion(args[19])
				.setTel(args[20]);
		rep.save(bnpP);

	}

	@Override
	public void scrap() throws Exception {
		String fichier_valeurs = Parametres.REPERTOIRE_DATA_SOURCES + ficval;
		Scanner sc = new Scanner(new BufferedInputStream(new FileInputStream(new File(fichier_valeurs))));
		LogScrap log = new LogScrap("BanqueBNP_PRIVEE.txt", LogConst.TYPE_LOG_FILE);
		int num_ligneficval = ((new File(LogConst.STORE_LOG_PATH + LogConst.PREFIX_LOG_FILE + "_BanqueBNP_PRIVEE.txt"))
				.exists()) ? Integer.parseInt(log.readLog().getLastLineProcessed().split("\\t")[4]) : 1;
		int currentLine = 1;

		while (sc.hasNextLine()) {
			String[] lines = sc.nextLine().split("\\t");
			if (currentLine > num_ligneficval) {
				
				URL url = new URL(strURL + lines[1]);
				HttpsURLConnection con = (HttpsURLConnection) url.openConnection();
				con.setRequestMethod("GET");
				con.setRequestProperty("Content-Type", "application/json; charset=utf-8");
				con.setRequestProperty("Content-Encoding", "gzip");
				con.setRequestProperty("Vary", "Accept-Encoding");
				con.setRequestProperty("Content-Length", "0");
				con.setConnectTimeout(60000);
				con.setReadTimeout(60000);
				con.setInstanceFollowRedirects(true);
				BufferedReader in = new BufferedReader(
						new InputStreamReader(con.getInputStream(), StandardCharsets.UTF_8));
				String pageSource;
				StringBuilder strB = new StringBuilder();
				while ((pageSource = in.readLine()) != null) {
					strB.append(pageSource);
				}

				String json = StringEscapeUtils.unescapeJava(strB.toString());
				
				//System.out.println(json);
				
				JSONObject jsonOb = new JSONObject(json);
				if (jsonOb.has("agence")) {
					JSONObject jsonAgency = jsonOb.getJSONObject("agence");
					String expertiseAccess = "";
					String particulier = "";
					String professional = "";
					String entreprise = "";
					String change = "";
					String coffreFort = "";
					String automateBillet = "";
					String automateCheque = "";
					String gabVocal = "";
					String imprLibreServ = "";
					String expressZone = "";
					String gab = "";
					String espaceLibreService = "";
					String agencyName = (jsonAgency.has("nom")) ? this.normalize(jsonAgency.getString("nom")) : "";
					System.out.println("agence nom : " + agencyName);

					String agencyType = (jsonAgency.has("type_agence")) ? this.normalize(jsonAgency.getString("type_agence")) : "";
					System.out.println("agence type : " + agencyType);

					String dep = (jsonAgency.has("dpt")) ? this.normalize(jsonAgency.getString("dpt")) : "";
					System.out.println("agence dpt : " + dep);

					String region = (jsonAgency.has("region")) ? this.normalize(jsonAgency.getString("region")) : "";
					System.out.println("agence region : " + region);

					String address = (jsonAgency.has("adresse")) ? String.format("%s %s %s", jsonAgency.get("adresse"),
							jsonAgency.get("cp"), jsonAgency.get("ville")) : "";
					address = this.normalize(address);
					System.out.println("agence adresse : " + address);

					String schedule = (jsonAgency.has("text1")) ? this.normalize(jsonAgency.getString("text1").replaceAll("\\.", "").replaceAll("<br/>", " ")) : "";
					System.out.println("agence horaires : " + schedule);

					String tel = (jsonAgency.has("telephone")) ? jsonAgency.getString("telephone") : "";
					System.out.println("tel : " + tel);

					String fax = (jsonAgency.has("fax")) ? jsonAgency.getString("fax") : "";
					System.out.println("fax : " + fax);

					if (jsonAgency.has("services")) {
						JSONObject jsonService = jsonAgency.getJSONObject("services");

						gab = (jsonService.has("gab")) ? jsonService.getString("gab") : "";
						System.out.println("gab : " + gab);

						expressZone = (jsonService.has("zone_express")) ? jsonService.getString("zone_express") : "";
						System.out.println("zone_express : " + expressZone);

						expertiseAccess = (jsonService.has("acces_expertise"))
								? jsonService.getString("acces_expertise")
								: "";
						System.out.println("axpertice : " + expertiseAccess);

						particulier = (jsonService.has("particulier")) ? jsonService.getString("particulier") : "";
						System.out.println("particulier " + particulier);

						professional = (jsonService.has("professionnel")) ? jsonService.getString("professionnel") : "";
						System.out.println("professional : " + professional);

						entreprise = (jsonService.has("entreprise")) ? jsonService.getString("entreprise") : "";
						System.out.println("entreprise : " + entreprise);

						change = (jsonService.has("change")) ? jsonService.getString("change") : "";
						System.out.println("change : " + change);

						coffreFort = (jsonService.has("coffre_fort")) ? jsonService.getString("coffre_fort") : "";
						System.out.println("coffreFort : " + coffreFort);

						automateBillet = (jsonService.has("automate_billet")) ? jsonService.getString("automate_billet")
								: "";
						System.out.println("automate billet : " + automateBillet);

						automateCheque = (jsonService.has("automate_cheque")) ? jsonService.getString("automate_cheque")
								: "";
						System.out.println("automate cheque : " + automateCheque);

						gabVocal = (jsonService.has("gab_vocale")) ? jsonService.getString("gab_vocale") : "";
						System.out.println("geb vocal : " + gabVocal);

						imprLibreServ = (jsonService.has("imprimante_libre_service"))
								? jsonService.getString("imprimante_libre_service")
								: "";
						System.out.println("imprim libre service : " + imprLibreServ);

						espaceLibreService = (jsonService.has("espace_libre_service"))
								? jsonService.getString("espace_libre_service")
								: "";
						System.out.println("espace libre service : " + espaceLibreService);

					}
					this.saveItem(agencyName, address, schedule, agencyType, automateBillet, automateCheque, change,
							coffreFort, dep, entreprise, espaceLibreService, expertiseAccess, expressZone, fax, gab,
							gabVocal, imprLibreServ, particulier, professional, region, tel);
				}

				log.writeLog(new Date(),lines[1],"no_error", 1, currentLine, 0);
			}
			currentLine++;

		}
		sc.close();

	}

	
}
