package com.app.controller;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.app.service.ServiceActionLogement;
import com.app.service.ServiceBCP;
import com.app.service.ServiceBECM;
import com.app.service.ServiceBP;
import com.app.service.ServiceBP_GAB;
import com.app.service.ServiceBTP;
import com.app.service.ServiceBanqueCMB;
import com.app.service.ServiceBanqueCMSO;
import com.app.service.ServiceBanqueCaraibes;
import com.app.service.ServiceBanqueSavoie;
import com.app.service.ServiceBnp;
import com.app.service.ServiceBnpPriv;
import com.app.service.ServiceCA;
import com.app.service.ServiceCDN;
import com.app.service.ServiceCDN_GAB;
import com.app.service.ServiceCE;
import com.app.service.ServiceCE_modif;
import com.app.service.ServiceCE_GAB;
import com.app.service.ServiceCIC;
import com.app.service.ServiceCIC_GAB;
import com.app.service.ServiceCM;
import com.app.service.ServiceCM_GAB;
import com.app.service.ServiceCasden;
import com.app.service.ServiceCcoop;
import com.app.service.ServiceHsbc;
import com.app.service.ServiceInAndFi;
import com.app.service.ServiceLcl;
import com.app.service.ServiceMeilleurTaux;
import com.app.service.ServicePalatineNew;
import com.app.service.ServiceSG;
import com.app.service.ServiceSG_GAB;
import com.app.service.ServiceVisa;
import com.app.service.ServiceVisaAdresse;

@RestController
@RequestMapping("/scrap")
public class BanqueController {

	@Autowired
	private ServiceBanqueCMB srvCMB;
	
	@Autowired
	private ServiceInAndFi srvInAndFi;

	@Autowired
	private ServiceBanqueCMSO srvCMSO;

	@Autowired
	private ServiceBanqueSavoie srvSavoie;

	@Autowired
	private ServiceBCP srvBCP;

	@Autowired
	private ServiceBnp srvBNP;

	@Autowired
	private ServiceBP srvBP;

	@Autowired
	private ServiceBP_GAB srvBP_GAB;

	@Autowired
	private ServiceCA srvCA;

	@Autowired
	private ServiceCasden srvCasden;

	@Autowired
	private ServiceCDN srvCDN;

	@Autowired
	private ServiceCDN_GAB srvCDN_GAB;

	@Autowired
	private ServiceCE_GAB srvCE_GAB;

	@Autowired
	private ServiceCE srvCE;

	@Autowired
	private ServiceCcoop srvCcoop;

	@Autowired
	private ServiceCIC_GAB srvCIC_GAB;

	@Autowired
	private ServiceCIC srvCIC;

	@Autowired
	private ServiceCM_GAB srvCM_GAB;

	@Autowired
	private ServiceCM srvCM;

	@Autowired
	private ServiceHsbc srvHsbc;

	@Autowired
	private ServiceLcl srvLCL;

	@Autowired
	private ServicePalatineNew srvPalatine;

	@Autowired
	private ServiceSG_GAB srvSG_GAB;

	@Autowired
	private ServiceSG srvSG;

	@Autowired
	private ServiceVisa srvVisa;

	@Autowired
	private ServiceVisaAdresse srvVisaAdd;

	@Autowired
	private ServiceBTP srvBTP;

	@Autowired
	private ServiceBECM srvBECM;

	@Autowired
	private ServiceActionLogement srvActLog;

	@Autowired
	private ServiceBnpPriv srvBNP_Priv;
	
	@Autowired
	private ServiceMeilleurTaux srvMT;
	
	

	@RequestMapping("/CMB")
	public void setCMB() throws IOException, InterruptedException {
		srvCMB.setup();
		srvCMB.letScrap();
		srvCMB.logout();
		
		/*srvInAndFi.setup();
		srvInAndFi.scrap();
		srvInAndFi.tearDown();*/
	}

	@RequestMapping("/CMSO")
	public void setCMSO() throws IOException {
		srvCMSO.setup();
		srvCMSO.letScrap();
		srvCMSO.logout();
	}

	@RequestMapping("/SAVOIE")
	public void setSavoie() throws Exception {
		srvSavoie.setUp();
		srvSavoie.scrapBqSavoie();
		srvSavoie.tearDowns();

	}

	@RequestMapping("/BCP")
	public void setBCP() throws IOException {
		srvBCP.setup();
		srvBCP.scrapBCP();
		srvBCP.tearDown();
	}

	@RequestMapping("/BNP")
	public void setBNP() throws Exception {
		srvBNP.scrapBNP();
		//srvBNP.deleteAll();
	}

	@RequestMapping("/BP")
	public void setBP() throws Exception {
		srvBP.setUp();
		srvBP.open_BP_byselenium();
		srvBP.tearDown();
	}

	@RequestMapping("/BP_GAB")
	public void setBP_GAB() throws IOException, InterruptedException {

		srvBP_GAB.scrap();

	}

	@RequestMapping("/CA")
	public void setCA() throws IOException, InterruptedException {
		srvCA.setup();
		srvCA.scrap();
		srvCA.tearDown();
	}

	@RequestMapping("/CASDEN")
	public void setCAsden() throws IOException, InterruptedException {
		srvCasden.setup();
		srvCasden.open_CASDEN_byselenium();
		srvCasden.tearDown();

	}

	@RequestMapping("/CDN_GAB")
	public void setCDN_GAB() throws IOException, InterruptedException {
		srvCDN_GAB.setup();
		srvCDN_GAB.scrapCDNGAb();
		srvCDN_GAB.tearDown();
	}

	@RequestMapping("/CDN")
	public void setCDN() throws IOException, InterruptedException {
		srvCDN.setup();
		srvCDN.scrapCDN();
		srvCDN.tearsDown();
	}

	@RequestMapping("/CE_GAB")
	public void setCE_GAB() throws IOException, InterruptedException {
		srvCE_GAB.setup();
		srvCE_GAB.scrapCEGAB();
		srvCE_GAB.tearDown();
	}

	@RequestMapping("/CE")
	public void setCE() throws IOException, InterruptedException {
		srvCE.setup();
		srvCE.scrapCE();
		srvCE.tearDown();
	}

	@RequestMapping("/CCOOP")
	public void setCCOOP() throws IOException {
		srvCcoop.setup();
		srvCcoop.scrapCCOOP();
		srvCcoop.tearsDown();
	}

	@RequestMapping("/CIC_GAb")
	public void setCIC_GAB() throws IOException {
		srvCIC_GAB.setup();
		srvCIC_GAB.scrap();
		srvCIC_GAB.tearsDown();
	}

	@RequestMapping("/CIC")
	public void setCIC() throws IOException {
		srvCIC.setup();
		srvCIC.scrapCIC();
		srvCIC.tearsDown();
	}

	@RequestMapping("/CM_GAB")
	public void setCM_GAB() throws IOException {
		srvCM_GAB.setup();
		srvCM_GAB.scrap();
		srvCM_GAB.tearsDown();
	}

	@RequestMapping("/CM")
	public void setCM() throws IOException {
		srvCM.setup();
		srvCM.scrapCM();
		srvCM.tearsDown();
	}

	@RequestMapping("/HSBC")
	public void setHSBC() throws IOException, InterruptedException {
		srvHsbc.setup();
		srvHsbc.scrapHSBC();
		srvHsbc.tearDown();
	}

	@RequestMapping("/LCL")
	public void setLCL() throws IOException, InterruptedException {
		srvLCL.setup();
		srvLCL.letScrapLCL();
		srvLCL.tearDown();
	}

	@RequestMapping("/PALATINE")
	public void setPalatine() throws IOException {
		srvPalatine.scrapPalatine();
	}

	@RequestMapping("/SG_GAB")
	public void setSG_GAB() throws IOException, InterruptedException {
		srvSG_GAB.setup();
		srvSG_GAB.scrapSG_GAB();
		srvSG_GAB.tearDowns();
	}

	@RequestMapping("/SG")
	public void setSG() throws IOException, InterruptedException {
		srvSG.setup();
		srvSG.scrapSG();
		srvSG.tearDowns();
	}

	@RequestMapping("/VISA")
	public void setVisa() throws Exception {
		srvVisa.setUp();
		srvVisa.open_VISA_byselenium();
		srvVisa.tearDown();
	}

	@RequestMapping("/VISA_ADRESSE")
	public void setVISA_ADD() throws Exception {
		srvVisaAdd.setUp();
		srvVisaAdd.scrap();
		srvVisaAdd.tearDown();
	}

	@RequestMapping("/BTP")
	public void setBTP() throws IOException {
		srvBTP.setup();
		srvBTP.scrpBTP();
		srvBTP.logout();
	}

	@RequestMapping("/BECM")
	public void setBECM() throws Exception {
		srvBECM.setup();
		srvBECM.scrap();
		srvBECM.logout();
	}

	@RequestMapping("/ACTION_LOGEMENTALL")
	public void setActionLog() throws IOException {
		srvActLog.setup();
		srvActLog.scrapActionlogement();
		srvActLog.logout();
	}

	@RequestMapping("/BNP_PRIVEE")
	public void setBNP_PRIV() throws Exception {
		srvBNP_Priv.scrap();
	}
	
	@RequestMapping("/MEILLEUR_TAUX")
	public void setMt() throws Exception {
		srvMT.scrap();
	}

}