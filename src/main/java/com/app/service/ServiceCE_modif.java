package com.app.service;

import java.io.BufferedInputStream;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;

import java.net.SocketTimeoutException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Scanner;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import org.openqa.selenium.PageLoadStrategy;

import org.openqa.selenium.chrome.ChromeDriver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.app.model.CE;

import com.app.scrap.RepCE;

import com.app.utils.Parametres;
import com.app.utils.Utilitaires;

import fa.browserUtils.ChromeBrowser;
import fa.browserUtils.FactoryBrowser;
import fa.browserUtils.HtmlUnitBrowser;
import fa.log.LogApp.LogScrap;
import fa.log.config.LogConst;

@Service
public class ServiceCE_modif extends Utilitaires {

	@Autowired
	private RepCE rep;

	String driverPath = "C:\\ChromeDriver\\";
	// private List<CE> ceList=new ArrayList<>();

	String nomSITE = "CE";
	//String ficval = "Departement_CE";
	String ficval = "liens_ce_2023";
	String dossierRESU = Parametres.REPERTOIRE_RESULT + "\\BQ\\" + nomSITE;;
	// String url_accueil =
	// "https://www.caisse-epargne.fr/bretagne-pays-de-loire/particuliers/rechercher-une-agence";
	String url_accueil = "https://www.agences.caisse-epargne.fr";
	String fichier_valeurs;
	String cp;
	String VilleSoumise;
	String ligne;

	int num_ligne = 2;
	int NbAgences;
	int numreponse = 1;
	int DAB = 1;
	int HORS_SITE;
	int nbRecuperes;
	int num_ligneficval;
	int parcoursficval;
	int Resultats;

	ChromeDriver chromeDriver;
	HtmlUnitDriver htmlUnitDriver;
	BufferedWriter sortie;
	BufferedWriter sortie_log;

	Date Temps_Fin;

	public CE saveData(CE t) {

		return rep.save(t);

	}

	public List<CE> saveDataAll(List<CE> t) {

		return rep.saveAll(t);

	}

	public List<CE> getElmnt() {

		return rep.findAll();
	}

	public CE getById(int id) {

		return rep.findById(id).orElse(null);
	}

	public String deleteProduct(int id) {
		try {
			rep.deleteById(id);

		} catch (Exception e) {
			e.printStackTrace();
		}

		return "removed";
	}

	public CE updateProduct(CE t) {

		return null;
	}

	public CE setData(String depSoumis, String nbAgences, String nomWeb, String adresse, String tel, String fax,
			String horairesHtm, String horairestxt, String dab, String agenceHtm, String services, String lienAgence) {
		CE ce = new CE();
		ce.setDepSoumis(depSoumis).setNbAgences(nbAgences).setNomWeb(nomWeb).setAdresse(adresse).setTel(tel).setFax(fax)
				.setHorairesHtm(horairesHtm).setHorairestxt(horairestxt).setDab(dab).setAgenceHtm(agenceHtm)
				.setService(services).setLien_agence(lienAgence);

		return ce;
	}

	public List<CE> showFirstRows() {
		List<CE> a = rep.getFirstRow(100, CE.class);
		return a;
	}

	public List<CE> showLastRows() {
		List<CE> a = rep.getLastRow(100, CE.class);
		return a;
	}

	public void setup() {
		
		FactoryBrowser factB = new FactoryBrowser();
		HtmlUnitBrowser htmlB = (HtmlUnitBrowser) factB.create("htmlUnit");
		htmlUnitDriver = htmlB.setOptions(true, false, false, 12000).initBrowserDriver();
		
		/*FactoryBrowser factBrowser = new FactoryBrowser();
		ChromeBrowser b = (ChromeBrowser) factBrowser.create("chrome");
		ChromeOptions opt= new ChromeOptions();
		opt.addExtensions(new File("D:\\scrapper-3\\ext\\scrap_utils_ext.crx"));
		chromeDriver = new ChromeDriver(opt);*/
	      //set path of .crx file of extension
	   //opt.addExtensions(new File("C:\Users\Momentum_v0.92.2.crx"));
		//chromeDriver = b.setDataUserPath("D:\\scrapper-1\\ext\\scrap_utils_ext.crx")
			//	.setOptions(PageLoadStrategy.NORMAL, driverPath, 60000, 60000, 60000, false, true,"").initBrowserDriver();
	}
	
	public void scrapCE() throws IOException, InterruptedException {
		
		boolean documentReadyState = false;
		String pageSource = "";
		Document doc = null;
		List<String> lienAgences = new ArrayList<>();
		fichier_valeurs = Parametres.REPERTOIRE_DATA_SOURCES + ficval + ".txt";
		Scanner sc = new Scanner(new BufferedInputStream(new FileInputStream(new File(fichier_valeurs))));

		if (!new File(dossierRESU).exists()) {
			new File(dossierRESU).mkdirs();
		}

		fichier = new File(dossierRESU + "\\" + "RESULTATS_" + nomSITE + "_" + System.currentTimeMillis() + ".txt");
		LogScrap log = new LogScrap("BanqueCELog.txt", LogConst.TYPE_LOG_FILE);
		if ((new File(LogConst.STORE_LOG_PATH + LogConst.PREFIX_LOG_FILE + "_BanqueCELog.txt")).exists()) {

			num_ligneficval = Integer.parseInt(log.readLog().getLastLineProcessed().split("\t")[4]);

		} else {
			num_ligneficval = 1;
		}

		parcoursficval = 0;
		while (sc.hasNextLine()) {
			String line = sc.nextLine();
			String[] fic_valeurs = line.split("\\t");
			if (parcoursficval >= num_ligneficval) {
				cp = fic_valeurs[0];
				//url_accueil = "https://www.agences.caisse-epargne.fr" + fic_valeurs[1];
				url_accueil = "https://www.agences.caisse-epargne.fr/banque-assurance" + line;
				
				System.out.println("Url : " +url_accueil);
				try {
					htmlUnitDriver.get(url_accueil);

				} catch (Exception e) {
					if (e instanceof SocketTimeoutException) {
						htmlUnitDriver.get(url_accueil);

					}
				}
				
				pageSource = htmlUnitDriver.getPageSource();
				doc = Jsoup.parse(pageSource);
				
				String agenceHtm = "";
				String nomWeb = "";
				String adresse = "";
				String telephone = "";
				String fax = "";
				String horaires = "";
				String horairesHTM = "";
				String services = "";
				String dab = "0";
				
				Element main = doc.select("body > div.em-page > main").first();
				
				agenceHtm = pageSource.replaceAll("\\s{1,}", " ");
				
				Element nom = main.selectFirst("div.em-details > div.em-details__poi-card > h2");
				
				nomWeb = (nom != null) ? nom.text() : "";
				
				Element add =  main.selectFirst("div.em-details > div.em-details__poi-card > div > div:nth-child(1) > div");
				
				adresse = (add!= null) ? add.text() : "";
				
				Element tel = main.selectFirst("div.em-details > div.em-details__poi-card > div > div:nth-child(2) > a");
				
				telephone = (tel != null) ? tel.attr("href").replaceAll("[^0-9 ]", "") : "";
				
				Element fx = main.selectFirst("div.em-details > div.em-details__poi-card > div > div:nth-child(2) > div");
				
				fax = (fx != null) ? fx.text().replaceAll("[^0-9 ]", "") : "";
				
				Element h = main.selectFirst("div.em-details__horaires-bloc > div > div.em-graphical-schedules > div.graphicalSchedules");
				
				horaires = (h!=null)?h.text() : "";
				
				System.out.println("nomWeb : "+nomWeb);
				System.out.println("adresse : "+adresse);
				System.out.println("telephone : "+telephone);
				System.out.println("fax : "+fax);
				System.out.println("horaires : "+horaires);
				
				
				/*Elements listResults = doc.select("body > div.em-page > " + "main > div.em-results-wrapper > div > "
							+ "div.em-results__list-wrapper > div > div > div.jspPane > ul>li");
					if (listResults != null) {
						NbAgences = listResults.size();
						if (!lienAgences.isEmpty())
							lienAgences.clear();
						for (Element listResult : listResults) {
							Element href = listResult.selectFirst("li> h2 > a.em-poi-card__link");
							if (href != null) {
								System.out.println(href.attr("href"));
								lienAgences.add(href.attr("href"));
							}
						}
						System.out.println(NbAgences);
					}*/

					/*for (String lienAgence : lienAgences) {

						String agenceHtm = "";
						String nomWeb = "";
						String adresse = "";
						String telephone = "";
						String fax = "";
						String horaires = "";
						String horairesHTM = "";
						String services = "";
						String dab = "0";

						url_accueil = "https://www.agences.caisse-epargne.fr" + lienAgence;
						System.out.println(url_accueil);
						try {
							chromeDriver.get(url_accueil);
						} catch (Exception e) {
							if (e instanceof SocketTimeoutException) {
								chromeDriver.get(url_accueil);
							}
						}

						Thread.sleep(3000);
						pageSource = chromeDriver.getPageSource();
						agenceHtm = pageSource.replaceAll("\\s{1,}", " ");
						System.out.println(agenceHtm);
						doc = Jsoup.parse(pageSource);

						Element nomAgenceJSEelement = doc.selectFirst("#titre_faniry");
						nomWeb = (nomAgenceJSEelement != null) ? nomAgenceJSEelement.text() : "";
						System.out.println("nom " + nomWeb);

						Element adresseJSElement = doc.selectFirst("#adresse_faniry");
						adresse = (adresseJSElement != null) ? adresseJSElement.text() : "";
						System.out.println("adresse " + adresse);

						Element telJSElement = doc.selectFirst("#tel_faniry");
						telephone = (telJSElement != null) ? telJSElement.attr("href").replaceAll("[^0-9 ]", "") : "";
						System.out.println("tel " + telephone);

						Element faxJSElement = doc.selectFirst("#fax_faniry");
						fax = (faxJSElement != null) ? faxJSElement.text().replaceAll("[^0-9]", " ") : "";
						System.out.println("fax " + fax);

						Elements horaireJSElements = doc.select("#horaire_faniry >div");
						if (horaireJSElements.size() > 0) {
							for (Element horaireJSElement : horaireJSElements) {
								horaires += horaireJSElement.text() + " ";
							}
							horairesHTM = doc
									.selectFirst("body > div.em-page > " + "main > div.em-details__horaires-bloc")
									.html().replaceAll("\\s{1,}", " ");
						} else {
							Element horaireJSElement = doc.selectFirst("#no_horaire_faniry");
							if (horaireJSElement != null) {
								horaires = horaireJSElement.text();
								horairesHTM = doc
										.selectFirst("body > " + "div.em-page > " + "main > "
												+ "div.em-details__horaires-text-info > " + "p")
										.html().replaceAll("\\s{1,}", " ");
							}
						}
						System.out.println(horairesHTM);
						System.out.println("horaire " + horaires);

						Element serviceJSElement = doc.selectFirst("#dan_faniry");
						if (serviceJSElement != null) {
							services = serviceJSElement.text();
							dab = serviceJSElement.attr("data-dab-statut");
						} else
							dab = "0";
						System.out.println("dab " + dab);
						System.out.println("service " + services);

						//this.saveData(this.setData(cp, String.valueOf(NbAgences), nomWeb, adresse, telephone, fax,
								//horairesHTM, horaires, dab, agenceHtm, services, lienAgence));
						nbRecuperes++;

						//log.writeLog(new Date(), cp, "no_error", NbAgences, parcoursficval, (NbAgences - nbRecuperes));
					}*/

				//}
			}

			parcoursficval++;
		}
		//sc.close();
		
	}

	/*public void scrapCE() throws IOException, InterruptedException {
		
		boolean documentReadyState = false;
		String pageSource = "";
		Document doc = null;
		List<String> lienAgences = new ArrayList<>();
		fichier_valeurs = Parametres.REPERTOIRE_DATA_SOURCES + ficval + ".txt";
		Scanner sc = new Scanner(new BufferedInputStream(new FileInputStream(new File(fichier_valeurs))));

		if (!new File(dossierRESU).exists()) {
			new File(dossierRESU).mkdirs();
		}

		fichier = new File(dossierRESU + "\\" + "RESULTATS_" + nomSITE + "_" + System.currentTimeMillis() + ".txt");
		LogScrap log = new LogScrap("BanqueCELog.txt", LogConst.TYPE_LOG_FILE);
		if ((new File(LogConst.STORE_LOG_PATH + LogConst.PREFIX_LOG_FILE + "_BanqueCELog.txt")).exists()) {

			num_ligneficval = Integer.parseInt(log.readLog().getLastLineProcessed().split("\t")[4]);

		} else {
			num_ligneficval = 1;
		}

		parcoursficval = 0;
		while (sc.hasNextLine()) {
			String line = sc.nextLine();
			String[] fic_valeurs = line.split("\\t");
			if (parcoursficval >= num_ligneficval) {
				cp = fic_valeurs[0];
				//url_accueil = "https://www.agences.caisse-epargne.fr" + fic_valeurs[1];
				url_accueil = "https://www.agences.caisse-epargne.fr/banque-assurance" + line;
				
				System.out.println("Url : " +url_accueil);
				try {
					htmlUnitDriver.get(url_accueil);

				} catch (Exception e) {
					if (e instanceof SocketTimeoutException) {
						htmlUnitDriver.get(url_accueil);

					}
				}
				//documentReadyState = this.listensIfPageFullyLoaded(chromeDriver);

				//if (documentReadyState) {
					//Thread.sleep(3000);
					pageSource = htmlUnitDriver.getPageSource();
					doc = Jsoup.parse(pageSource);
					Elements listResults = doc.select("body > div.em-page > " + "main > div.em-results-wrapper > div > "
							+ "div.em-results__list-wrapper > div > div > div.jspPane > ul>li");
					if (listResults != null) {
						NbAgences = listResults.size();
						if (!lienAgences.isEmpty())
							lienAgences.clear();
						for (Element listResult : listResults) {
							Element href = listResult.selectFirst("li> h2 > a.em-poi-card__link");
							if (href != null) {
								System.out.println(href.attr("href"));
								lienAgences.add(href.attr("href"));
							}
						}
						System.out.println(NbAgences);
					}

					for (String lienAgence : lienAgences) {

						String agenceHtm = "";
						String nomWeb = "";
						String adresse = "";
						String telephone = "";
						String fax = "";
						String horaires = "";
						String horairesHTM = "";
						String services = "";
						String dab = "0";

						url_accueil = "https://www.agences.caisse-epargne.fr" + lienAgence;
						System.out.println(url_accueil);
						try {
							chromeDriver.get(url_accueil);
						} catch (Exception e) {
							if (e instanceof SocketTimeoutException) {
								chromeDriver.get(url_accueil);
							}
						}

						Thread.sleep(3000);
						pageSource = chromeDriver.getPageSource();
						agenceHtm = pageSource.replaceAll("\\s{1,}", " ");
						System.out.println(agenceHtm);
						doc = Jsoup.parse(pageSource);

						Element nomAgenceJSEelement = doc.selectFirst("#titre_faniry");
						nomWeb = (nomAgenceJSEelement != null) ? nomAgenceJSEelement.text() : "";
						System.out.println("nom " + nomWeb);

						Element adresseJSElement = doc.selectFirst("#adresse_faniry");
						adresse = (adresseJSElement != null) ? adresseJSElement.text() : "";
						System.out.println("adresse " + adresse);

						Element telJSElement = doc.selectFirst("#tel_faniry");
						telephone = (telJSElement != null) ? telJSElement.attr("href").replaceAll("[^0-9 ]", "") : "";
						System.out.println("tel " + telephone);

						Element faxJSElement = doc.selectFirst("#fax_faniry");
						fax = (faxJSElement != null) ? faxJSElement.text().replaceAll("[^0-9]", " ") : "";
						System.out.println("fax " + fax);

						Elements horaireJSElements = doc.select("#horaire_faniry >div");
						if (horaireJSElements.size() > 0) {
							for (Element horaireJSElement : horaireJSElements) {
								horaires += horaireJSElement.text() + " ";
							}
							horairesHTM = doc
									.selectFirst("body > div.em-page > " + "main > div.em-details__horaires-bloc")
									.html().replaceAll("\\s{1,}", " ");
						} else {
							Element horaireJSElement = doc.selectFirst("#no_horaire_faniry");
							if (horaireJSElement != null) {
								horaires = horaireJSElement.text();
								horairesHTM = doc
										.selectFirst("body > " + "div.em-page > " + "main > "
												+ "div.em-details__horaires-text-info > " + "p")
										.html().replaceAll("\\s{1,}", " ");
							}
						}
						System.out.println(horairesHTM);
						System.out.println("horaire " + horaires);

						Element serviceJSElement = doc.selectFirst("#dan_faniry");
						if (serviceJSElement != null) {
							services = serviceJSElement.text();
							dab = serviceJSElement.attr("data-dab-statut");
						} else
							dab = "0";
						System.out.println("dab " + dab);
						System.out.println("service " + services);

						//this.saveData(this.setData(cp, String.valueOf(NbAgences), nomWeb, adresse, telephone, fax,
								//horairesHTM, horaires, dab, agenceHtm, services, lienAgence));
						nbRecuperes++;

						//log.writeLog(new Date(), cp, "no_error", NbAgences, parcoursficval, (NbAgences - nbRecuperes));
					}

				//}
			}

			parcoursficval++;
		}
		//sc.close();
		
	}*/

	public void enregistrer(String agences_HTM, String nom_agence, String adresse, String tel, String fax, String dab,
			String HorairesHTM, String HorairesTXT, String services, String lienAgence) {

//    	this.fichier = new File(Parametres.REPERTOIRE + "RESULTATS_BNP.");
		try {

			sortie = new BufferedWriter(new FileWriter(fichier, true));
			sortie.write(cp + "\t" + NbAgences + "\t" + lienAgence + "\t" + nom_agence + "\t" + adresse + "\t" + tel
					+ "\t" + fax + "\t" + services + "\t" + HorairesTXT + "\t" + dab + "\t" + agences_HTM + "\t"
					+ HorairesHTM + "\r\n");

			sortie.close();
		} catch (IOException e) {

			e.printStackTrace();
		}

	}

	public void enregistrer_journal(int numreponse) {

//    	this.fichier = new File(Parametres.REPERTOIRE + "RESULTATS_BNP.");
		try {
			Temps_Fin = new Date();
			SimpleDateFormat formater = null;
			formater = new SimpleDateFormat("'le' dd/MM/yyyy  hh:mm:ss");
			String date = formater.format(Temps_Fin);
			date = formater.format(Temps_Fin);

			sortie_log = new BufferedWriter(new FileWriter(log, true));
			sortie_log.write(parcoursficval + ";" + "FinValeur" + ";" + cp + ";" + this.NbAgences + ";" + nbRecuperes
					+ ";" + (this.NbAgences - nbRecuperes) + ";" + date + "\r\n");
			sortie_log.close();
			long taille_fichier = fichier.length() / 1000000; // la length est en octets, on divise par 1000 pour mettre
																// en Ko et par 1000000 pour mettre en Mo

			if (taille_fichier > 4) {
				// INITIALISATION FICHIER DE RESULTAT
				this.fichier = new File(
						dossierRESU + "\\" + "RESULTATS_" + nomSITE + "_" + System.currentTimeMillis() + ".txt");
				// Attention pas d'accent dans les noms de colonnes !
				sortie = new BufferedWriter(new FileWriter(fichier, true));
				sortie.write("CPSoumis" + "\t" + "NbAgences" + "\t" + "NOMWEB" + "\t" + "ADRESSE" + "\t" + "TEL" + "\t"
						+ "FAX" + "\t" + "HORAIRESHTM" + "\t" + "HORAIRESTXT" + "\t" + "DAB" + "\t" + "AGENCEHTM"
						+ "\r\n");
				sortie.close();
			}

		} catch (IOException e) {

			e.printStackTrace();
		}

	}

	/**
	 * La m�thode <code>initialiser()</code> permet d'ajouter la ligne d'ent�te dans
	 * le fichier r�sultat.
	 */

	public void initialiser() {

		try {
			// Attention pas d'accent dans les noms de colonnes !
			sortie = new BufferedWriter(new FileWriter(fichier, true));
			sortie.write("DepSoumis" + "\t" + "NbAgences" + "\t" + "NOMWEB" + "\t" + "ADRESSE" + "\t" + "TEL" + "\t"
					+ "FAX" + "\t" + "HORAIRESHTM" + "\t" + "HORAIRESTXT" + "\t" + "DAB" + "\t" + "AGENCEHTM" + "\r\n");
			sortie.close();

		} catch (IOException e) {

			e.printStackTrace();
		}
	}

	public void tearDown() {
		//chromeDriver.quit();

	}
}
