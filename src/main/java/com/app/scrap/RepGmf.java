package com.app.scrap;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.app.model.GMF;

@Repository
public interface RepGmf extends JpaRepository<GMF,Integer>,RepositoryCustom<GMF> {

}
