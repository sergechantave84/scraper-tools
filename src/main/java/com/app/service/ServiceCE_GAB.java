package com.app.service;

import java.io.BufferedInputStream;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;

import java.net.SocketTimeoutException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Scanner;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import org.openqa.selenium.htmlunit.HtmlUnitDriver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.app.model.CE_GAB;

import com.app.scrap.RepCE_GAB;

import com.app.utils.Parametres;
import com.app.utils.Utilitaires;

import fa.browserUtils.FactoryBrowser;
import fa.browserUtils.HtmlUnitBrowser;
import fa.log.LogApp.LogScrap;
import fa.log.config.LogConst;

@Service
public class ServiceCE_GAB extends Utilitaires {

	@Autowired
	private RepCE_GAB rep;

	private List<CE_GAB> ceGAbLis = new ArrayList<>();

	static String driverPath = "C:\\htmlUnitDriver\\";

	String nomSITE = "CE_GAB";
	String ficval = "lien_ce_gab";
	String dossierRESU = Parametres.REPERTOIRE_RESULT + "\\BQ\\" + nomSITE;;
	String url_accueil = "https://www.agences.caisse-epargne.fr";
	String fichier_valeurs;
	String cp;
	String VilleSoumise;
	String ligne;

	int Resultats;
	int num_ligne = 2;
	int NbAgences;
	int numreponse = 1;
	int DAB = 1;
	int HORS_SITE;
	int nbRecuperes;
	int num_ligneficval;
	int parcoursficval;

	static HtmlUnitDriver htmlUnitDriver;
	BufferedWriter sortie;
	BufferedWriter sortie_log;

	Date Temps_Fin;

	public CE_GAB saveData(CE_GAB t) {

		return rep.save(t);

	}

	public List<CE_GAB> saveDataAll(List<CE_GAB> t) {

		return rep.saveAll(t);

	}

	public List<CE_GAB> getElmnt() {

		return rep.findAll();
	}

	public CE_GAB getById(int id) {

		return rep.findById(id).orElse(null);
	}

	public String deleteProduct(int id) {
		try {
			rep.deleteById(id);

		} catch (Exception e) {
			e.printStackTrace();
		}

		return "removed";
	}

	public CE_GAB updateProduct(CE_GAB t) {

		return null;
	}

	public CE_GAB setData(String depSoumis, String nbAgences, String nomWeb, String adresse, String dab,
			String horsSite, String agenceHtm) {
		CE_GAB ceGab = new CE_GAB();
		ceGab.setDepSoumis(depSoumis).setNbAgences(nbAgences).setNomWeb(nomWeb).setAdresse(adresse).setDab(dab)
				.setHorsSite(horsSite).setAgenceHtm(agenceHtm);
		this.saveData(ceGab);
		return ceGab;
	}

	public List<CE_GAB> showFirstRows() {
		List<CE_GAB> a = rep.getFirstRow(100, CE_GAB.class);
		return a;
	}

	public List<CE_GAB> showLastRows() {
		List<CE_GAB> a = rep.getLastRow(100, CE_GAB.class);
		return a;
	}

	public void setup() {
		FactoryBrowser fact = new FactoryBrowser();
		HtmlUnitBrowser hB = (HtmlUnitBrowser) fact.create("htmlUnit");
		htmlUnitDriver = hB.setOptions(false, false, false, 60000).initBrowserDriver();
	}

	public List<CE_GAB> scrapCEGAB() throws IOException, InterruptedException {

		fichier_valeurs = Parametres.REPERTOIRE_DATA_SOURCES + ficval + ".txt";
		Scanner sc = new Scanner(new BufferedInputStream(new FileInputStream(new File(fichier_valeurs))));

		if (!new File(dossierRESU).exists()) {
			new File(dossierRESU).mkdirs();
		}
		fichier = new File(dossierRESU + "\\" + "RESULTATS_" + nomSITE + "_" + System.currentTimeMillis() + ".txt");
		LogScrap log = new LogScrap("BanqueBP.txt", LogConst.TYPE_LOG_FILE);
		num_ligneficval = ((new File(LogConst.STORE_LOG_PATH + LogConst.PREFIX_LOG_FILE + "_Banque_CE_GAB.txt")).exists())
				? Integer.parseInt(log.readLog().getLastLineProcessed().split("\\t")[4])
				: 1;

		parcoursficval = 0;
		while (sc.hasNextLine()) {
			String line = sc.nextLine();
			String[] fic_valeurs = line.split("\\t");
			String url = "";
			if (parcoursficval >= num_ligneficval) {
				cp = fic_valeurs[0];
				url = url_accueil + fic_valeurs[1];
				System.out.println("url : " + url);
				try {
					htmlUnitDriver.get(url);

				} catch (Exception e) {
					if (e instanceof SocketTimeoutException) {
						htmlUnitDriver.get(url);

					}
				}

				Document doc = null;
				String pageSource = htmlUnitDriver.getPageSource();
				doc = Jsoup.parse(pageSource);
				Element rootJElement = null;
				//rootJElement = doc.select("#agencies").first();
				rootJElement = doc.select("ul.em-results__items").first();
				if (rootJElement != null) {
					//Elements agencyListJElement = rootJElement.select("div>div.jspPane>li[class^=\"agency\"]");
					Elements agencyListJElement = rootJElement.select("li");
					NbAgences = agencyListJElement.size();
					System.out.println("NbAgences : " + NbAgences);
					nbRecuperes = 0;
					/*for (Element tmp : agencyListJElement)
						NbAgences++;*/

					for (Element tmp : agencyListJElement) {
						String agences_HTM = "", nom_agence = "", adresse = "";
						agences_HTM = tmp.outerHtml().trim().replaceAll("\\s{1,}", " ");
						agences_HTM = this.normalize(agences_HTM);
						if (agences_HTM.contains("agencytel")) {
							HORS_SITE = 0;
						} else {
							HORS_SITE = 1;
						}
						System.out.println("HORS_SITE : " + HORS_SITE);
						// retrieve agency name
						Element agencyNameJElement = tmp.select("h2 > a.em-poi-card__link").first();
						nom_agence = (agencyNameJElement != null) ? this.normalize(agencyNameJElement.text()) : "";
						System.out.println("nom_agence : " + nom_agence);
						
						// retrieve agency adresse
						Element agencyAddressJElement = tmp.select("div.em-poi-card__address").first();
						adresse = (agencyAddressJElement != null) ? agencyAddressJElement.text().replaceAll(",","") : "";
						System.out.println("adresse : " + adresse);

						nbRecuperes++;
						/*this.setData(cp, String.valueOf(NbAgences), nom_agence, adresse, String.valueOf(DAB),
								String.valueOf(HORS_SITE), agences_HTM);*/

					}

				}

				//log.writeLog(new Date(), url, "no_error", NbAgences, parcoursficval, NbAgences - nbRecuperes);
			}
			parcoursficval++;
		}

		sc.close();
		return ceGAbLis;
	}

	public void enregistrer(String agences_HTM, String nom_agence, String adresse) {

//    	this.fichier = new File(Parametres.REPERTOIRE + "RESULTATS_BNP.");
		try {

			sortie = new BufferedWriter(new FileWriter(fichier, true));
			sortie.write(cp + "\t" + NbAgences + "\t" + nom_agence + "\t" + adresse + "\t" + DAB + "\t" + HORS_SITE
					+ "\t" + agences_HTM + "\r\n");

			sortie.close();
		} catch (IOException e) {

			e.printStackTrace();
		}

	}

	public void enregistrer_journal(int numreponse) {

//    	this.fichier = new File(Parametres.REPERTOIRE + "RESULTATS_BNP.");
		try {
			Temps_Fin = new Date();
			SimpleDateFormat formater = null;
			formater = new SimpleDateFormat("'le' dd/MM/yyyy '�' hh:mm:ss");
			String date = formater.format(Temps_Fin);
			date = formater.format(Temps_Fin);

			sortie_log = new BufferedWriter(new FileWriter(log, true));
			sortie_log.write(parcoursficval + ";" + "FinValeur" + ";" + cp + ";" + this.NbAgences + ";" + nbRecuperes
					+ ";" + (this.NbAgences - nbRecuperes) + ";" + date + "\r\n");
			sortie_log.close();
			long taille_fichier = fichier.length() / 1000000; // la length est en octets, on divise par 1000 pour mettre
																// en Ko et par 1000000 pour mettre en Mo

			if (taille_fichier > 8) {
				// INITIALISATION FICHIER DE RESULTAT
				this.fichier = new File(
						dossierRESU + "\\" + "RESULTATS_" + nomSITE + "_" + System.currentTimeMillis() + ".txt");
				// Attention pas d'accent dans les noms de colonnes !
				sortie = new BufferedWriter(new FileWriter(fichier, true));
				sortie.write("CPSoumis" + "\t" + "NbAgences" + "\t" + "NOMWEB" + "\t" + "ADRESSE" + "\t" + "DAB" + "\t"
						+ "HORS_SITE" + "\t" + "AGENCEHTM" + "\r\n");
				sortie.close();
			}

		} catch (IOException e) {

			e.printStackTrace();
		}

	}

	/**
	 * La m�thode <code>initialiser()</code> permet d'ajouter la ligne d'ent�te dans
	 * le fichier r�sultat.
	 */

	public void initialiser() {

		try {
			// Attention pas d'accent dans les noms de colonnes !
			sortie = new BufferedWriter(new FileWriter(fichier, true));
			sortie.write("DepSoumis" + "\t" + "NbAgences" + "\t" + "NOMWEB" + "\t" + "ADRESSE" + "\t" + "DAB" + "\t"
					+ "HORS_SITE" + "\t" + "AGENCEHTM" + "\r\n");
			sortie.close();

			File toto = new File(dossierRESU + "\\" + "LOG_" + nomSITE + ".csv");
			if (!toto.exists()) {
				// if (num_ligneficval == num_ligne) {
				sortie_log = new BufferedWriter(new FileWriter(log, true));
				sortie_log.write("NumLigne" + ";" + "BIDON" + ";" + "DepSoumis" + ";" + "NbAgences" + ";"
						+ "Nbrecuperes" + ";" + "Reste" + ";" + "Heure_Fin" + "\r\n");
				sortie_log.close();
			}

		} catch (IOException e) {

			e.printStackTrace();
		}
	}

	public void tearDown() {
		htmlUnitDriver.quit();

	}

}
