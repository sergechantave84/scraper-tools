package com.app.model;

import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="groupama", schema="scrap")
public class Groupama {

	@Id
	@SequenceGenerator(name="scrap.groupama_id_seq",
	                    sequenceName ="scrap.groupama_id_seq",
	                     allocationSize=1)
    @GeneratedValue( strategy=GenerationType.SEQUENCE,
                     generator="scrap.groupama_id_seq")
	  
	@Column(name="id",updatable=false)
	private int id;
	
	@Column(name="regionsoumise", columnDefinition="varchar")
	private String regionSoumise;
	
	@Column(name="nbagences", columnDefinition="varchar")
	private String nbAgences;
	
	@Column(name="nbagences_trouvees", columnDefinition="varchar")
	private String nbAgencesTrouvees;
	
	@Column(name="lien_agence", columnDefinition="varchar")
	private String lienAgence;
	
	@Column(name="nom_agence", columnDefinition="varchar")
	private String nomAgence;
	
	@Column(name="adresse", columnDefinition="varchar")
	private String adresse;
	
	@Column(name="tel", columnDefinition="varchar")
	private String tel;
	
	@Column(name="fax", columnDefinition="varchar")
	private String fax;
	
	@Column(name="horaires", columnDefinition="varchar")
	private String horaires;
	
	@Column(name="agencehtm", columnDefinition="varchar")
	private String agenceHtm;
	
	@Column(name="annee_du_scrap" ,columnDefinition="varchar(4) DEFAULT date_part('year',CURRENT_DATE)")
	private String anneeScrap;

	public Groupama() {
		LocalDate ld= new Date().toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
		anneeScrap=String.valueOf(ld.getYear()+1);
	}
	
	public int getId() {
		return id;
	}

	public Groupama setId(int id) {
		this.id = id;
		return this;
	}

	public String getRegionSoumise() {
		return regionSoumise;
	}

	public Groupama setRegionSoumise(String regionSoumise) {
		this.regionSoumise = regionSoumise;
		return this;
	}

	public String getNbAgences() {
		return nbAgences;
	}

	public Groupama setNbAgences(String nbAgences) {
		this.nbAgences = nbAgences;
		return this;
	}

	public String getNbAgencesTrouvees() {
		return nbAgencesTrouvees;
	}

	public Groupama setNbAgencesTrouvees(String nbAgencesTrouvees) {
		this.nbAgencesTrouvees = nbAgencesTrouvees;
		return this;
	}

	public String getLienAgence() {
		return lienAgence;
	}

	public Groupama setLienAgence(String lienAgence) {
		this.lienAgence = lienAgence;
		return this;
	}

	public String getNomAgence() {
		return nomAgence;
	}

	public Groupama setNomAgence(String nomAgence) {
		this.nomAgence = nomAgence;
		return this;
	}

	public String getAdresse() {
		return adresse;
	}

	public Groupama setAdresse(String adresse) {
		this.adresse = adresse;
		return this;
	}

	public String getTel() {
		return tel;
	}

	public Groupama setTel(String tel) {
		this.tel = tel;
		return this;
	}

	public String getFax() {
		return fax;
	}

	public Groupama setFax(String fax) {
		this.fax = fax;
		return this;
	}

	public String getHoraires() {
		return horaires;
	}

	public Groupama setHoraires(String horaires) {
		this.horaires = horaires;
		return this;
	}

	public String getAgenceHtm() {
		return agenceHtm;
	}

	public Groupama setAgenceHtm(String agenceHtm) {
		this.agenceHtm = agenceHtm;
		return this;
	}

	public String getAnneeScrap() {
		return anneeScrap;
	}

	public Groupama setAnneeScrap(String anneeScrap) {
		this.anneeScrap = anneeScrap;
		return this;
	}
	
}
