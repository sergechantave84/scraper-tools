package com.app.scrap;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


import com.app.model.Assu2000;

@Repository
public interface RepAssu2000 extends JpaRepository<Assu2000,Integer>,RepositoryCustom<Assu2000> {

}
