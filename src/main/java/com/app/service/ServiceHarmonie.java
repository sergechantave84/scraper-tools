package com.app.service;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.SocketTimeoutException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Scanner;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.openqa.selenium.By;
import org.openqa.selenium.PageLoadStrategy;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.app.model.Harmonie_mutuelle;
import com.app.scrap.RepHarmonieMutu;
import com.app.utils.BrowserUtils;
import com.app.utils.Parametres;
import com.app.utils.Utilitaires;

import fa.browserUtils.FactoryBrowser;
import fa.browserUtils.HtmlUnitBrowser;
import fa.log.LogApp.LogScrap;
import fa.log.config.LogConst;

@Service
public class ServiceHarmonie extends Utilitaires {

	@Autowired
	private RepHarmonieMutu rep;

	private List<Harmonie_mutuelle> harmonieList = new ArrayList<>();
	private String driverPath = "C:\\ChromeDriver\\";
	private String nomSITE = "HARMONIE";
	private String dossierRESU = Parametres.REPERTOIRE_RESULT + "\\ASSU\\" + nomSITE;;

	private int num_ligneficval = 1;
	private int NbAgences_page;
	private int currentLine = 1;
	private int nbRecuperes;

	private HtmlUnitDriver htmlUnitDriver;
	private BufferedWriter sortie;

	public Harmonie_mutuelle saveData(Harmonie_mutuelle t) {

		return rep.save(t);

	}

	public List<Harmonie_mutuelle> saveDataAll(List<Harmonie_mutuelle> t) {

		return rep.saveAll(t);

	}

	public List<Harmonie_mutuelle> getElmnt() {

		return rep.findAll();
	}

	public Harmonie_mutuelle getById(int id) {

		return rep.findById(id).orElse(null);
	}

	public String deleteProduct(int id) {
		try {
			rep.deleteById(id);

		} catch (Exception e) {
			e.printStackTrace();
		}

		return "removed";
	}

	public Harmonie_mutuelle updateProduct(Harmonie_mutuelle t) {
		// Harmonie_mutuelleODO Auto-generated method stub
		return null;
	}

	public Harmonie_mutuelle setData(String nomAgence, String adresse, String tel, String fax, String horaires,
			String agenceHtm) {
		Harmonie_mutuelle hm = new Harmonie_mutuelle();
		hm.setNomAgence(nomAgence).setAdresse(adresse).setTel(tel).setFax(fax).setHoraires(horaires)
				.setAgenceHtm(agenceHtm);
		//this.saveData(hm);
		return hm;
	}

	public List<Harmonie_mutuelle> showFirstRows() {
		List<Harmonie_mutuelle> a = rep.getFirstRow(100, Harmonie_mutuelle.class);
		return a;
	}

	public List<Harmonie_mutuelle> showLastRows() {
		List<Harmonie_mutuelle> a = rep.getLastRow(100, Harmonie_mutuelle.class);
		return a;
	}

	public void setUp() throws Exception {
		FactoryBrowser fact = new FactoryBrowser();
		HtmlUnitBrowser hB = (HtmlUnitBrowser) fact.create("htmlUnit");
		htmlUnitDriver = hB.setOptions(false, false, false, 60000).initBrowserDriver();
	}

	public List<Harmonie_mutuelle> scrapHarmonie() throws IOException {

		String url = "https://agences.harmonie-mutuelle.fr";
		/*Scanner sc = new Scanner(new BufferedInputStream(
				new FileInputStream(new File(Parametres.REPERTOIRE_DATA_SOURCES + "lien_harmonie_test.txt"))));*/
		Scanner sc = new Scanner(new BufferedInputStream(
				new FileInputStream(new File(Parametres.REPERTOIRE_DATA_SOURCES + "lien_harmonie.txt"))));
		
		if (!new File(dossierRESU).exists())
			new File(dossierRESU).mkdirs();

		fichier = new File(dossierRESU + "\\" + "RESULTATS_" + nomSITE + "_" + System.currentTimeMillis() + ".txt");
		LogScrap log = new LogScrap("BanqueGAN.txt", LogConst.TYPE_LOG_FILE);
		num_ligneficval = ((new File(LogConst.STORE_LOG_PATH + LogConst.PREFIX_LOG_FILE + "_BanqueGAN.txt")).exists())
				? Integer.parseInt(log.readLog().getLastLineProcessed().split("\\t")[4])
				: 1;

		Document doc = null;
		String pageSource = "";
		while (sc.hasNextLine()) {
			String lines = sc.nextLine();
			if (currentLine > num_ligneficval) {
				System.out.println(url + lines);
				try {
					htmlUnitDriver.get(url + lines);
					Thread.sleep(2000);
				} catch (Exception e) {
					htmlUnitDriver.get(url + lines);
					
				}
				pageSource = htmlUnitDriver.getPageSource();
				doc = Jsoup.parse(pageSource);
				String nomAgence = "";
				String adresse = "";
				String tel = "";
				String fax = "";
				String horaires = "";
				String agenceHtm = "";
				Element agencyNameContainer = doc.selectFirst("body > " + "div > " + "div > " + "main > " + "div > "
						+ "div > " + "article > " + "div > " + "div.AgencyPage-anchors > " + "div > "
						+ "div.AgencyPage-anchors-agencyWrapper > " + "div.mobile-left > " + "h2");
				nomAgence = (agencyNameContainer != null) ? agencyNameContainer.text() : "";
				System.out.println("nom agence : " + nomAgence);
				Element agencyCoordinateBlock = doc.selectFirst("body > div > div > "
						+ "main > div > div > article > div > div.AgencyPage-HourBlock.container > div.AgencyPage-CoordinatesBlock");
				if (agencyCoordinateBlock != null) {
					Element adresseElement = agencyCoordinateBlock.selectFirst("div.AgencyPage-CoordinatesBlock-left > "
							+ "div.AgencyPage-CoordinatesBlock-content > div.info-group.mail > div");
					adresse = (adresseElement != null) ? adresseElement.text() : "";
					adresse = adresse.replaceAll("Adresse postale", "").trim();
					adresse = adresse.replaceAll("Voir l'itinéraire", "");

					System.out.println("adresse : " + adresse);
					Element phoneElement = agencyCoordinateBlock.selectFirst("#agencyPhoneLink");
					tel = (phoneElement != null) ? phoneElement.text() : "";
					System.out.println("tel : " + tel);

					Element faxElement = agencyCoordinateBlock.selectFirst("div.AgencyPage-CoordinatesBlock-left > "
							+ "div.AgencyPage-CoordinatesBlock-content > div.info-group.fax");
					fax = (faxElement != null) ? faxElement.text() : "";
					System.out.println("fax : " + fax);
				}

				Element agencyOpeningHoursElement = doc.selectFirst("body > div > div > main > div > div > "
						+ "article > div > div.AgencyPage-HourBlock.container > div.AgencyPage-OpeningHoursBlock");
				if (agencyOpeningHoursElement != null) {
					Element scheduleElement = agencyOpeningHoursElement
							.selectFirst("div.AgencyPage-OpeningHoursBlock-hours");
					horaires = (scheduleElement != null) ? scheduleElement.text() : "";
					System.out.println("horaires : " + horaires);
				}
				System.out.println("-------------------------------------\n");
				this.setData(nomAgence, adresse, tel, fax, horaires, agenceHtm);
				log.writeLog(new Date(), lines, "no_error", 1, currentLine, 0);
			}
			currentLine++;

		}
		return harmonieList;
	}

	public void enregistrer(String nom_agence, String adresse, String tel, String fax, String HorairesHTM,
			String agence) {
		try {
			sortie = new BufferedWriter(new FileWriter(fichier, true));
			sortie.write(NbAgences_page + "\t" + nom_agence + "\t" + adresse + "\t" + tel + "\t" + fax + "\t"
					+ HorairesHTM + "\t" + agence + "\r\n");
			sortie.close();
		} catch (IOException e) {

		}

	}

	public void initialiser() {

		try {
			sortie = new BufferedWriter(new FileWriter(fichier, true));
			sortie.write("NB_PAGES" + "\t" + "NUM_PAGE" + "\t" + "NbAgences_page" + "\t" + "NOM_AGENCE" + "\t"
					+ "ADRESSE" + "\t" + "TEL" + "\t" + "FAX" + "\t" + "Horaires" + "\t" + "AGENCEHTM" + "\r\n");
			sortie.close();

		} catch (IOException e) {

		}
	}

	public void tearDown() {
		try {
			htmlUnitDriver.quit();
			// chromeDriver.quit();
		} catch (Exception e) {

		}

	}

}
