package com.app.scrap;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.app.model.BECM;

@Repository
public interface RepBECM extends JpaRepository<BECM, Integer> {

}
