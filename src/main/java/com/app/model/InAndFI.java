package com.app.model;

import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="inandfi", schema="scrap")
public class InAndFI {
 
	@Id
	@SequenceGenerator( name="scrap.inandfi_id_seq",
	                    sequenceName ="scrap.inandfi_id_seq",
	                    allocationSize=1)
    @GeneratedValue( strategy=GenerationType.SEQUENCE,
                     generator="scrap.inandfi_id_seq")
	  
	@Column(name="id",updatable=false)
	private int id;
	
	@Column(name="depsoumis", columnDefinition="varchar")
	private String depSoumis;
	
	@Column(name="nbagences", columnDefinition="varchar")
	private String nbAgence;
	
	@Column(name="nom_agents", columnDefinition="varchar")
	private String nomAgents;
	
	@Column(name="nom_agence", columnDefinition="varchar")
	private String nomAgence;
	
	@Column(name="adresse", columnDefinition="varchar")
	private String adresse;
	
	@Column(name="telephone", columnDefinition="varchar")
	private String tel;

	@Column(name="horaires", columnDefinition="varchar")
	private String horaire;
	
	@Column(name="agencehtml", columnDefinition="varchar")
	private String agenceHTM;
	
	@Column(name = "lien_agence", columnDefinition = "varchar")
	private String lienAgence;
	
	@Column(name = "services", columnDefinition = "varchar")
	private String services;
	

	public String getServices() {
		return services;
	}

	public InAndFI setServices(String services) {
		this.services = services;
		return this;
	}

	public String getLienAgence() {
		return lienAgence;
	}

	public InAndFI setLienAgence(String lienAgence) {
		this.lienAgence = lienAgence;
		return this;
	}

	public String getHorairehtml() {
		return horairehtml;
	}

	public InAndFI setHorairehtml(String horairehtml) {
		this.horairehtml = horairehtml;
		return this;
	}

	@Column(name="horairehtml", columnDefinition="varchar")
	private String horairehtml;
	
	
	@Column(name="annee_du_scrap" ,columnDefinition="varchar(4) DEFAULT date_part('year',CURRENT_DATE)" )
	private String anneeScrap;
	
	public InAndFI() {
		LocalDate ld= new Date().toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
		anneeScrap=String.valueOf(ld.getYear()+1);
	}
	
	public int getId() {
		return id;
	}
	public InAndFI setId(int id) {
		this.id = id;
		return this;
	}
	public String getDepSoumis() {
		return depSoumis;
	}
	public InAndFI setDepSoumis(String depSoumis) {
		this.depSoumis = depSoumis;
		return this;
	}

	public String getNbAgence() {
		return nbAgence;
	}
	public InAndFI setNbAgence(String nbAgenceVille) {
		this.nbAgence = nbAgenceVille;
		return this;
	}
	public String getAgenceHTM() {
		return agenceHTM;
	}
	public InAndFI setAgenceHTM(String agenceHTM) {
		this.agenceHTM = agenceHTM;
		return this;
	}
	public String getAnneeScrap() {
		return anneeScrap;
	}
	public InAndFI setAnneeScrap(String anneeScrap) {
		this.anneeScrap = anneeScrap;
		return this;
	}

	public String getNomAgents() {
		return nomAgents;
	}

	public InAndFI setNomAgents(String nomAgent) {
		this.nomAgents = nomAgent;
		return this;
	}

	public String getNomAgence() {
		return nomAgence;
	}

	public InAndFI setNomAgence(String nomAgence) {
		this.nomAgence = nomAgence;
		return this;
	}

	public String getAdresse() {
		return adresse;
	}

	public InAndFI setAdresse(String adresse) {
		this.adresse = adresse;
		return this;
	}

	public String getTel() {
		return tel;
	}

	public InAndFI setTel(String tel) {
		this.tel = tel;
		return this;
	}

	public String getHoraire() {
		return horaire;
	}

	public InAndFI setHoraire(String horaire) {
		this.horaire = horaire;
		return this;
	}
}
