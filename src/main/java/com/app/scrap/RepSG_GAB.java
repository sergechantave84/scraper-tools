package com.app.scrap;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.app.model.SG_GAB;

@Repository
public interface RepSG_GAB extends JpaRepository<SG_GAB, Integer>,RepositoryCustom<SG_GAB> {

}
