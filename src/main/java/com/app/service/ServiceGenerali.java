package com.app.service;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.SocketTimeoutException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Scanner;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.PageLoadStrategy;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.app.model.Generali;

import com.app.scrap.RepGenerali;
import com.app.utils.BrowserUtils;
import com.app.utils.Parametres;
import com.app.utils.Utilitaires;

import fa.browserUtils.ChromeBrowser;
import fa.browserUtils.FactoryBrowser;
import fa.log.LogApp.LogScrap;
import fa.log.config.LogConst;

@Service
public class ServiceGenerali extends Utilitaires {

	@Autowired
	private RepGenerali rep;

	private List<Generali> geneList = new ArrayList<>();

	String driverPath = "C:\\ChromeDriver\\";
	String nomSITE = "GENERALI";
	String ficval = "CP_cantons";
	String dossierRESU = Parametres.REPERTOIRE_RESULT + "\\ASSU\\" + nomSITE;
	String url_accueil = "https://www.generali.fr/trouver-mon-agence-generali/";
	String fichier_valeurs;
	String CantonSoumis;
	String NomRegion;
	String ligne;
	int num_ligne = 2;
	int NbAgences;
	int NbAgences_Ville;
	int numreponse = 1;
	int nbRecuperes;
	int num_ligneficval;
	int parcoursficval;
	int Resultats;
	ChromeDriver chromeDriver;
	BufferedWriter sortie;
	BufferedWriter sortie_log;
	Date Temps_Fin;

	public Generali saveData(Generali t) {

		return rep.save(t);

	}

	public List<Generali> saveDataAll(List<Generali> t) {

		return rep.saveAll(t);

	}

	public List<Generali> getElmnt() {

		return rep.findAll();
	}

	public Generali getById(int id) {

		return rep.findById(id).orElse(null);
	}

	public String deleteProduct(int id) {
		try {
			rep.deleteById(id);

		} catch (Exception e) {
			e.printStackTrace();
		}

		return "removed";
	}

	public Generali updateProduct(Generali t) {
		// GeneraliODO Auto-generated method stub
		return null;
	}

	public Generali setData(String cantonSoumis, String nbAgences, String nbAgencesGeneralirouvees, String lienAgence,
			String nomAgence, String adresse, String tel, String agenceHtm) {
		Generali generali = new Generali();
		generali.setCantonSoumis(cantonSoumis).setNbAgences(nbAgences).setNbAgencesTrouvees(nbAgencesGeneralirouvees)
				.setLienAgence(lienAgence).setNomAgence(nomAgence).setAdresse(adresse).setTel(tel)
				.setAgenceHtm(agenceHtm);
		this.saveData(generali);
		return generali;
	}

	public List<Generali> showFirstRows() {
		List<Generali> a = rep.getFirstRow(100, Generali.class);
		return a;
	}

	public List<Generali> showLastRows() {
		List<Generali> a = rep.getLastRow(100, Generali.class);
		return a;
	}

	public void setUp() throws Exception {
		FactoryBrowser factB = new FactoryBrowser();
		ChromeBrowser chB = (ChromeBrowser) factB.create("chrome");
		final String chromeBeta = "C:\\Program Files\\Google\\Chrome Beta\\Application\\chrome.exe";
		chromeDriver = chB.setOptions(PageLoadStrategy.NORMAL, driverPath, 60000, 60000, 60000, false, false,chromeBeta)
				.initBrowserDriver();
	}

	public List<Generali> open_GENERALI_byselenium() throws Exception {

		boolean dReadyState = false;
		fichier_valeurs = Parametres.REPERTOIRE_DATA_SOURCES + ficval + ".txt";
		Scanner sc = new Scanner(new BufferedInputStream(new FileInputStream(new File(fichier_valeurs))));

		if (!new File(dossierRESU).exists())
			new File(dossierRESU).mkdirs();
		fichier = new File(dossierRESU + "\\" + "RESULTATS_" + nomSITE + "_" + System.currentTimeMillis() + ".txt");
		LogScrap log = new LogScrap("BanqueGENERALI.txt", LogConst.TYPE_LOG_FILE);
		num_ligneficval = ((new File(LogConst.STORE_LOG_PATH + LogConst.PREFIX_LOG_FILE + "_BanqueGENERALI.txt"))
				.exists()) ? Integer.parseInt(log.readLog().getLastLineProcessed().split("\\t")[4]) : 1;

		parcoursficval = 1;
		Document doc = null;
		while (sc.hasNextLine()) {
			String line = sc.nextLine();
			String[] fic_valeurs = line.split("\\t");
			if (parcoursficval >= num_ligneficval) {
				JavascriptExecutor jsExecutor = null;
				String jsScript = "", html = "";
				Resultats = 1;
				nbRecuperes = 0;
				CantonSoumis = fic_valeurs[0];
				NbAgences = 0;
				try {
					System.out.println("run");
					chromeDriver.get(url_accueil);
					System.out.println("on c'est connecté");
				} catch (Exception e) {
					if (e instanceof SocketTimeoutException) {
						this.tearDown();
						this.setUp();
						chromeDriver.get(url_accueil + url_accueil);

					}
				}
				dReadyState = this.listensIfPageFullyLoaded(chromeDriver);
				if (dReadyState) {
					Thread.sleep(2000);
					WebElement input = chromeDriver.findElementByCssSelector("#city");
					WebElement clickBtn = chromeDriver.findElementByCssSelector(".btn-rouge.btn-submit");

					jsScript = String.format("arguments[0].value='%s'", CantonSoumis);
					jsExecutor = (JavascriptExecutor) chromeDriver;
					jsExecutor.executeScript(jsScript, input);

					jsScript = String.format("arguments[0].click()", "");
					jsExecutor = (JavascriptExecutor) chromeDriver;
					jsExecutor.executeScript(jsScript, clickBtn);

					dReadyState = this.listensIfPageFullyLoaded(chromeDriver);
					if (dReadyState) {
						Thread.sleep(2000);
						html = chromeDriver.getPageSource().toLowerCase();
						if (html.contains("choisissez une proposition dans la liste ci-dessous")) {
							WebElement multipleList = chromeDriver.findElementByCssSelector("#cgeocoder29_city_0");
							jsScript = String.format("arguments[0].click()", "");
							jsExecutor = (JavascriptExecutor) chromeDriver;
							jsExecutor.executeScript(jsScript, multipleList);
						}
						dReadyState = this.listensIfPageFullyLoaded(chromeDriver);
						if (dReadyState) {
							Thread.sleep(2000);
							WebElement iframe = chromeDriver.findElementByCssSelector("#iframeResultsfindagence");
							chromeDriver.switchTo().frame(iframe);
							dReadyState = this.listensIfPageFullyLoaded(chromeDriver);
							if (dReadyState) {
								Thread.sleep(2000);
								html = chromeDriver.getPageSource();
								doc = Jsoup.parse(html);
								//System.out.println(html);
								Element bigBlockListAgency = doc.select("div[id=\"listes-agences\"]").first();
								if (bigBlockListAgency != null) {
									String lien_agence = "", nom_agence = "", adresse = "", tel = "", agence = "";
									int NbAgences_verif = 0;
									Elements agenciesList = bigBlockListAgency.select("div[class^=\"bloc-agence\"]");
									if (agenciesList != null) {
										NbAgences = agenciesList.size();
										NbAgences_verif = NbAgences;
										for (Element elmntTmp : agenciesList) {
											lien_agence = "";
											nom_agence = "";
											adresse = "";
											tel = "";
											agence = "";

											agence = elmntTmp.outerHtml().replaceAll("\\s{1,}", " ").trim();
											agence = this.normalize(agence);
											// fetch agency link
											Element agencyLinkElement = elmntTmp
													.select("a[class=\"btn-rouge link-agence-single\"]").first();
											lien_agence = (agencyLinkElement != null) ? agencyLinkElement.attr("href")
													: "";
											System.out.println("le lien de l'agence est : " + lien_agence);

											// fetch agency address
											Element addressElement = elmntTmp.select("p[class=\"agence-adresse\"]")
													.first();
											Element zipElement = elmntTmp.select("p[class=\"agence-zipcity\"]").first();
											String voie = (addressElement != null) ? addressElement.text() : "";
											String zipCode = (zipElement != null) ? zipElement.text() : "";
											adresse = this.normalize(voie + " " + zipCode);
											System.out.println("adresse : " + adresse);

											// fetch agency tel
											Element phoneElement = elmntTmp.select("a[title=\"Appeler l'agence\"]")
													.first();
											tel = (phoneElement != null)
													? phoneElement.attr("href").replaceAll("tel:", "")
													: "";
											System.out.println("le numero de telephone : " + tel);

											// fetch agency name
											Element agencyNameElement = elmntTmp.select("p[class=\"agence-name\"]")
													.first();
											nom_agence = (agencyNameElement != null)
													? this.normalize(agencyNameElement.text())
													: "";
											System.out.println("le nom de l'agence est : " + nom_agence);
											
											System.out.println("-----------------------------------\n");

											nbRecuperes++;

											/*geneList.add(this.setData(CantonSoumis, String.valueOf(NbAgences),
													String.valueOf(NbAgences_verif), lien_agence, nom_agence, adresse,
													tel, agence));*/
										}

									}
								}

							}

						}

					}

				}
				//log.writeLog(new Date(), CantonSoumis, "no_error", NbAgences, parcoursficval, 0);
			}
			parcoursficval++;
		}
		sc.close();
		return geneList;
	}

	public void enregistrer(int NbAgences_verif, String lien_agence, String nom_agence, String adresse, String tel,
			String agence) {

//	    	this.fichier = new File(Parametres.REPERTOIRE + "RESULTATS_BNP.");
		try {

			sortie = new BufferedWriter(new FileWriter(fichier, true));
			sortie.write(CantonSoumis + "\t" + NbAgences + "\t" + NbAgences_verif + "\t" + lien_agence + "\t"
					+ nom_agence + "\t" + adresse + "\t" + tel + "\t" + agence + "\r\n");

			sortie.close();
		} catch (IOException e) {

			e.printStackTrace();
		}

	}

	public void enregistrer_journal(int numreponse) {

//	    	this.fichier = new File(Parametres.REPERTOIRE + "RESULTATS_BNP.");
		try {
			Temps_Fin = new Date();
			SimpleDateFormat formater = null;
			formater = new SimpleDateFormat("'le' dd/MM/yyyy '�' hh:mm:ss");
			String date = formater.format(Temps_Fin);
			date = formater.format(Temps_Fin);

			sortie_log = new BufferedWriter(new FileWriter(log, true));
			sortie_log.write(parcoursficval + ";" + "FinValeur" + ";" + CantonSoumis + ";" + this.NbAgences + ";"
					+ nbRecuperes + ";" + (this.NbAgences - nbRecuperes) + ";" + date + "\r\n");
			sortie_log.close();
			long taille_fichier = fichier.length() / 1000000; // la length est en octets, on divise par 1000 pour mettre
																// en Ko et par 1000000 pour mettre en Mo

			if (taille_fichier > 8) {
				// INITIALISATION FICHIER DE RESULTAT
				this.fichier = new File(
						dossierRESU + "\\" + "RESULTATS_" + nomSITE + "_" + System.currentTimeMillis() + ".txt");
				// Attention pas d'accent dans les noms de colonnes !
				sortie = new BufferedWriter(new FileWriter(fichier, true));
				sortie.write("CantonSoumis" + "\t" + "NbAgences" + "\t" + "NbAgences_trouvees" + "\t" + "LIEN_AGENCE"
						+ "\t" + "NOM_AGENCE" + "\t" + "ADRESSE" + "\t" + "TEL" + "\t" + "AGENCEHTM" + "\r\n");
				sortie.close();
			}

		} catch (IOException e) {

			e.printStackTrace();
		}

	}

	/**
	 * La m�thode <code>initialiser()</code> permet d'ajouter la ligne d'ent�te dans
	 * le fichier r�sultat.
	 */

	public void initialiser() {

		try {
			// Attention pas d'accent dans les noms de colonnes !
			sortie = new BufferedWriter(new FileWriter(fichier, true));
			sortie.write("CantonSoumis" + "\t" + "NbAgences" + "\t" + "NbAgences_trouvees" + "\t" + "LIEN_AGENCE" + "\t"
					+ "NOM_AGENCE" + "\t" + "ADRESSE" + "\t" + "TEL" + "\t" + "AGENCEHTM" + "\r\n");
			sortie.close();

			File toto = new File(dossierRESU + "\\" + "LOG_" + nomSITE + ".csv");
			if (!toto.exists()) {
				// if (num_ligneficval == num_ligne) {
				sortie_log = new BufferedWriter(new FileWriter(log, true));
				sortie_log.write("NumLigne" + ";" + "FinValeur" + ";" + "CantonSoumis" + ";" + "NbAgences" + ";"
						+ "Nbrecuperes" + ";" + "Reste" + ";" + "Heure_Fin" + "\r\n");
				sortie_log.close();
			}

		} catch (IOException e) {

			e.printStackTrace();
		}
	}

	public void tearDown() {
		chromeDriver.quit();

	}

}
