package com.app.model;

import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="generali", schema="scrap")
public class Generali {
	
	@Id
	@SequenceGenerator(name="scrap.generali_id_seq",
	                    sequenceName ="scrap.generali_id_seq",
	                     allocationSize=1)
    @GeneratedValue( strategy=GenerationType.SEQUENCE,
                     generator="scrap.generali_id_seq")
	  
	@Column(name="id",updatable=false)
	private int id;
	
	@Column(name="cantonsoumis", columnDefinition="varchar")
	private String cantonSoumis;
	
	@Column(name="nbagences", columnDefinition="varchar")
	private String nbAgences;
	
	@Column(name="nbagences_trouvees", columnDefinition="varchar")
	private String nbAgencesTrouvees;
	
	@Column(name="lien_agence", columnDefinition="varchar")
	private String lienAgence;
	
	@Column(name="nom_agence", columnDefinition="varchar")
	private String nomAgence;
	
	@Column(name="adresse", columnDefinition="varchar")
	private String adresse;
	
	@Column(name="tel", columnDefinition="varchar")
	private String tel;
	
	@Column(name="agencehtm", columnDefinition="varchar")
	private String agenceHtm;
	
	@Column(name="annee_du_scrap" ,columnDefinition="varchar(4) DEFAULT date_part('year',CURRENT_DATE)")
	private String anneeScrap;
	
	public Generali() {
		LocalDate ld= new Date().toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
		anneeScrap=String.valueOf(ld.getYear()+1);
	}
	public int getId() {
		return id;
	}
	public Generali setId(int id) {
		this.id = id;
		return this;
	}
	public String getCantonSoumis() {
		return cantonSoumis;
	}
	public Generali setCantonSoumis(String cantonSoumis) {
		this.cantonSoumis = cantonSoumis;
		return this;
	}
	public String getNbAgences() {
		return nbAgences;
	}
	public Generali setNbAgences(String nbAgences) {
		this.nbAgences = nbAgences;
		return this;
	}
	public String getNbAgencesTrouvees() {
		return nbAgencesTrouvees;
	}
	public Generali setNbAgencesTrouvees(String nbAgencesTrouvees) {
		this.nbAgencesTrouvees = nbAgencesTrouvees;
		return this;
	}
	public String getLienAgence() {
		return lienAgence;
	}
	public Generali setLienAgence(String lienAgence) {
		this.lienAgence = lienAgence;
		return this;
	}
	public String getNomAgence() {
		return nomAgence;
	}
	public Generali setNomAgence(String nomAgence) {
		this.nomAgence = nomAgence;
		return this;
	}
	public String getAdresse() {
		return adresse;
	}
	public Generali setAdresse(String adresse) {
		this.adresse = adresse;
		return this;
	}
	public String getTel() {
		return tel;
	}
	public Generali setTel(String tel) {
		this.tel = tel;
		return this;
	}
	public String getAgenceHtm() {
		return agenceHtm;
	}
	public Generali setAgenceHtm(String agenceHtm) {
		this.agenceHtm = agenceHtm;
		return this;
	}
	public String getAnneeScrap() {
		return anneeScrap;
	}
	public Generali setAnneeScrap(String anneeScrap) {
		this.anneeScrap = anneeScrap;
		return this;
	}

}
