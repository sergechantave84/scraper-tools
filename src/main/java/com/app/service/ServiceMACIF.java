package com.app.service;

import java.io.BufferedInputStream;
import java.io.BufferedReader;

import java.io.File;
import java.io.FileInputStream;

import java.io.InputStreamReader;

import java.net.URL;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Scanner;

import javax.net.ssl.HttpsURLConnection;

import org.apache.commons.text.StringEscapeUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.app.model.MACIF;

import com.app.scrap.RepMacif;

import com.app.utils.Parametres;

import fa.log.LogApp.LogScrap;
import fa.log.config.LogConst;

@Service
public class ServiceMACIF extends Scraper {

	@Autowired
	private RepMacif rep;

	String ficval = "lien_macif.txt";

	String url_accueil = "https://agence.macif.fr";
	String fichier_valeurs;

	int NbAgences;
	int NbAgences_trouvees;

	int num_ligneficval;
	int parcoursficval;

	@Override
	public void saveItem(String... args) {
		MACIF macif = new MACIF();
		macif.setDepSoumise(args[0]).setNbAgences(args[1]).setNbAgencesTrouvees(args[2]).setLienAgence(args[3])
				.setNomAgence(args[4]).setAdresse(args[5]).setTel(args[6]).setHoraires(args[7]).setAgenceHtm(args[8]);
		rep.save(macif);

	}
	
	public void show() {
		List<MACIF> res = rep.findAll();
		for(MACIF m : res) {
			System.out.println(m.getId()+"\t"+m.getNomAgence()+"\t"+m.getAdresse());
		}
	}
	
	public void show2() {
		List<MACIF> res = rep.getFirstRow(1, MACIF.class);
		for(MACIF m : res) {
			System.out.println(m.getId()+"\t"+m.getNomAgence()+"\t"+m.getAdresse());
		}
		List<MACIF> res2 = rep.getLastRow(1, MACIF.class);
		for(MACIF m : res2) {
			System.out.println(m.getId()+"\t"+m.getNomAgence()+"\t"+m.getAdresse());
		}
	}
	
	public void deleteAll() {
		rep.deleteAll();
		System.out.println("Supp. avec succees!");
	}

	@Override
	public void scrap() throws Exception {

		List<String> agencyLink = new ArrayList<>();

		Scanner sc = new Scanner(
				new BufferedInputStream(new FileInputStream(new File(Parametres.REPERTOIRE_DATA_SOURCES + ficval))));

		LogScrap log = new LogScrap("AssuranceMACIF.txt", LogConst.TYPE_LOG_FILE);
		num_ligneficval = ((new File(LogConst.STORE_LOG_PATH + LogConst.PREFIX_LOG_FILE + "_AssuranceMACIF.txt")).exists())
				? Integer.parseInt(log.readLog().getLastLineProcessed().split("\\t")[4])
				: 1;

		parcoursficval = 1;
		while (sc.hasNextLine()) {

			String[] lines = sc.nextLine().split("\\t");

			if (parcoursficval > num_ligneficval) {
				String finalUrl = url_accueil + lines[1];
				URL url = new URL(finalUrl);
				HttpsURLConnection con = (HttpsURLConnection) url.openConnection();
				con.setRequestMethod("GET");
				con.setConnectTimeout(600000);
				con.setReadTimeout(60000);
				BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream(), "UTF-8"));
				String pageSource;
				StringBuilder strB = new StringBuilder();
				while ((pageSource = in.readLine()) != null) {
					strB.append(pageSource);
				}
				pageSource = StringEscapeUtils.unescapeHtml4(strB.toString());
				Document doc = Jsoup.parse(pageSource);

				Elements agencyBigListElement = doc
						.select("#em-agencies > li > div > div.agencycontent > div.agencydetail > div.agencylabel > a");
				if (!agencyBigListElement.isEmpty()) {

					for (Element tmp : agencyBigListElement) {
						String str = tmp.attr("href");
						agencyLink.add(str);
						//System.out.println(str);
					}
					NbAgences = agencyLink.size();
					NbAgences_trouvees = NbAgences;
					System.out.println("+++++++++++++++++++++");
					for (String str : agencyLink) {

						finalUrl = url_accueil + str;
						url = new URL(finalUrl);
						con = (HttpsURLConnection) url.openConnection();
						con.setRequestMethod("GET");
						con.setConnectTimeout(600000);
						con.setReadTimeout(60000);
						in = new BufferedReader(new InputStreamReader(con.getInputStream(), "UTF-8"));

						strB = new StringBuilder();
						while ((pageSource = in.readLine()) != null) {
							strB.append(pageSource);
						}
						pageSource = StringEscapeUtils.unescapeHtml4(strB.toString());
						doc = Jsoup.parse(pageSource);
						
						System.out.println("url : " + url);
						
						Element bigBlockInfoElement = doc.select("div[class=\"agencycontent\"]").first();
						String agencyName = "", agencyAddress = "", agencySchedule = "", agencyPhone = "",
								agencyHtm = "";

						if (bigBlockInfoElement != null) {
							agencyHtm = bigBlockInfoElement.html().replaceAll("\\s{1,}", "").trim();
							agencyHtm = this.normalize(agencyHtm);
							// fetch agency coordinate
							Element agencyCoordinateElement = bigBlockInfoElement.select("div[class=\"agencydetail\"]")
									.first();
							if (agencyCoordinateElement != null) {
								// fetch agency Name
								Element agencyNameElement = agencyCoordinateElement.select("div[itemprop=\"name\"]")
										.first();
								agencyName = (agencyNameElement != null)
										? this.normalize(agencyNameElement.text().trim())
										: "";
								System.out.println("agency name : " + agencyName);

								// fetch agency Address
								Element agencyAddressElement = agencyCoordinateElement
										.select("div[class=\"agencyaddress\"]").first();
								agencyAddress = (agencyAddressElement != null)
										? this.normalize(agencyAddressElement.text().trim())
										: "";
								System.out.println("agency adresse : " + agencyAddress);
							}

							// fetch agency schedule
							Elements scheduleElements = bigBlockInfoElement.select(
									"div[class=\"agencyschedules\"]>div[class=\"agencyschtable\"] > span[class*=\"day\"]");
							if (scheduleElements != null) {
								String day = "", hours = "";
								for (Element scheduleElement : scheduleElements) {
									Element dayElmnt = scheduleElement.select("span[class=\"schedule-label\"]").first();
									Element hourElmnt = scheduleElement.select("span[class=\"schedule-value\"]")
											.first();
									day = dayElmnt.text();
									hours = hourElmnt.text();
									agencySchedule += day + ": " + hours + " ";
								}
							}
							agencySchedule = this.normalize(agencySchedule);
							System.out.println("agency schedule : " + agencySchedule);

							// fetch agency Tel
							Element agencyPhoneElement = bigBlockInfoElement.select("div[itemprop=\"telephone\"]")
									.first();
							agencyPhone = (agencyPhoneElement != null) ? agencyPhoneElement.text().trim() : "";
							System.out.println("agency phone : " + agencyPhone);
							System.out.println("-------------------------------------------\n ");
						}
						//macif.setDepSoumise(args[0]).setNbAgences(args[1]).setNbAgencesTrouvees(args[2]).setLienAgence(args[3])
						//.setNomAgence(args[4]).setAdresse(args[5]).setTel(args[6]).setHoraires(args[7]).setAgenceHtm(args[8]);

						this.saveItem(lines[0], String.valueOf(NbAgences), String.valueOf(NbAgences_trouvees), str,
								agencyName, agencyAddress, agencyPhone, agencySchedule, agencyHtm);
						

					}

				}
				

				log.writeLog(new Date(), lines[0], "no_error", NbAgences, parcoursficval, 0);

			}
			parcoursficval++;

		}
		sc.close();

	}
	

}
