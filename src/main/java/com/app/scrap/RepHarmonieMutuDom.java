package com.app.scrap;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.app.model.Harmonie_mutuelle;
import com.app.model.Harmonie_mutuelle_DOM;

@Repository
public interface RepHarmonieMutuDom extends JpaRepository<Harmonie_mutuelle_DOM,Integer>,RepositoryCustom<Harmonie_mutuelle_DOM> {

}
