package com.app.scrap;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.app.model.CIC_GAB;

@Repository
public interface RepCIC_GAB extends JpaRepository<CIC_GAB, Integer>,RepositoryCustom<CIC_GAB> {

}
