package com.app.model;

import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "banque_bfcoi_outre_mer", schema = "scrap")
public class BCFOI {

	@Id
	@SequenceGenerator(name = "scrap.banque_bfcoi_outre_mer_id_seq", sequenceName = "scrap.banque_bfcoi_outre_mer_id_seq", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "scrap.banque_bfcoi_outre_mer_id_seq")

	@Column(name = "id", updatable = false)
	private int id;

	@Column(name = "dep", columnDefinition = "varchar")
	private String dep;

	@Column(name = "nbagences", columnDefinition = "varchar")
	private String nbAgences;

	@Column(name = "enseigne", columnDefinition = "varchar")
	private String enseigne;

	@Column(name = "nomweb", columnDefinition = "varchar")
	private String nomWeb;

	@Column(name = "adresse", columnDefinition = "varchar")
	private String adresse;

	@Column(name = "tel", columnDefinition = "varchar")
	private String tel;

	@Column(name = "fax", columnDefinition = "varchar")
	private String fax;

	@Column(name = "horaires", columnDefinition = "varchar")
	private String horaires;

	@Column(name = "DAB", columnDefinition = "varchar")
	private String DAB;

	
	@Column(name = "annee_du_scrap", columnDefinition = "varchar(4)")
	private String anneeScrap;

	public BCFOI() {
		LocalDate ld = new Date().toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
		anneeScrap = String.valueOf(ld.getYear() + 1);
	}

	public int getId() {
		return id;
	}

	public BCFOI setId(int id) {
		this.id = id;
		return this;
	}

	public String getCodePostale() {
		return dep;
	}

	public BCFOI setCodePostale(String codePostale) {
		this.dep = codePostale;
		return this;
	}

	public String getNbAgences() {
		return nbAgences;
	}

	public BCFOI setNbAgences(String nbAgences) {
		this.nbAgences = nbAgences;
		return this;
	}

	public String getEnseigne() {
		return enseigne;
	}

	public BCFOI setEnseigne(String enseigne) {
		this.enseigne = enseigne;
		return this;
	}

	public String getNomWeb() {
		return nomWeb;
	}

	public BCFOI setNomWeb(String nomWeb) {
		this.nomWeb = nomWeb;
		return this;
	}

	public String getAdresse() {
		return adresse;
	}

	public BCFOI setAdresse(String adresse) {
		this.adresse = adresse;
		return this;
	}

	public String getTel() {
		return tel;
	}

	public BCFOI setTel(String tel) {
		this.tel = tel;
		return this;
	}

	

	

	public String getHorairesTxt() {
		return horaires;
	}

	public BCFOI setHorairesTxt(String horairesTxt) {
		this.horaires = horairesTxt;
		return this;
	}

	public String getDAB() {
		return DAB;
	}

	public BCFOI setDAB(String dAB) {
		DAB = dAB;
		return this;
	}



	public String getAnneeScrap() {
		return anneeScrap;
	}

	public BCFOI setAnneeScrap(String anneeScrap) {
		this.anneeScrap = anneeScrap;
		return this;
	}

	public String getFax() {
		return fax;
	}

	public BCFOI setFax(String fax) {
		this.fax = fax;
		return this;
	}
}
