package com.app.model;

import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="mma", schema="scrap")
public class MMA {

	
	@Id
	@SequenceGenerator(name="scrap.mma_id_seq",
	                    sequenceName ="scrap.mma_id_seq",
	                     allocationSize=1)
    @GeneratedValue( strategy=GenerationType.SEQUENCE,
                     generator="scrap.mma_id_seq")
	  
	@Column(name="id",updatable=false)
	private int id;
	
	@Column(name="depsoumise", columnDefinition="varchar")
	private String depSoumise;
	
	@Column(name="nbagences_ville", columnDefinition="varchar")
	private String nbAgencesVille;
	
	@Column(name="nom_agence", columnDefinition="varchar")
	private String nomAgence;
	
	@Column(name="adresse", columnDefinition="varchar")
	private String adresse;
	
	@Column(name="tel", columnDefinition="varchar")
	private String tel;
	
	@Column(name="noms_agents", columnDefinition="varchar")
	private String nomsAgents;
	
	@Column(name="orias", columnDefinition="varchar")
	private String orias;
	
	@Column(name="horaires", columnDefinition="varchar")
	private String horaires;
	
	@Column(name="agencehtm", columnDefinition="varchar")
	private String agenceHtm;
	
	@Column(name="annee_du_scrap" ,columnDefinition="varchar(4) DEFAULT date_part('year',CURRENT_DATE)")
	private String anneeScrap;

	public MMA() {
		LocalDate ld = new Date().toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
		anneeScrap = String.valueOf(ld.getYear() + 1);
	}
	public int getId() {
		return id;
	}

	public MMA setId(int id) {
		this.id = id;
		return this;
	}

	

	public String getNbAgencesVille() {
		return nbAgencesVille;
	}

	public MMA setNbAgencesVille(String nbAgencesVille) {
		this.nbAgencesVille = nbAgencesVille;
		return this;
	}

	public String getNomAgence() {
		return nomAgence;
	}

	public MMA setNomAgence(String nomAgence) {
		this.nomAgence = nomAgence;
		return this;
	}

	public String getAdresse() {
		return adresse;
	}

	public MMA setAdresse(String adresse) {
		this.adresse = adresse;
		return this;
	}

	public String getTel() {
		return tel;
	}

	public MMA setTel(String tel) {
		this.tel = tel;
		return this; 
	}

	public String getNomsAgents() {
		return nomsAgents;
	}

	public MMA setNomsAgents(String nomsAgents) {
		this.nomsAgents = nomsAgents;
		return this;
	}

	public String getOrias() {
		return orias;
	}

	public MMA setOrias(String orias) {
		this.orias = orias;
		return this;
	}

	public String getHoraires() {
		return horaires;
	}

	public MMA setHoraires(String horaires) {
		this.horaires = horaires;
		return this;
	}

	public String getAgenceHtm() {
		return agenceHtm;
	}

	public MMA setAgenceHtm(String agenceHtm) {
		this.agenceHtm = agenceHtm;
		return this;
	}

	public String getAnneeScrap() {
		return anneeScrap;
	}

	public MMA setAnneeScrap(String anneeScrap) {
		this.anneeScrap = anneeScrap;
		return this;
	}
	public String getDepSoumise() {
		return depSoumise;
	}
	public MMA setDepSoumise(String depSoumise) {
		this.depSoumise = depSoumise;
		return this;
	}
}
