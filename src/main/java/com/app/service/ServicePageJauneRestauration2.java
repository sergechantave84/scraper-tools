package com.app.service;

import java.io.BufferedInputStream;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.net.URLEncoder;
import java.text.Normalizer;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.json.JSONException;
import org.json.JSONObject;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.openqa.selenium.By;
import org.openqa.selenium.PageLoadStrategy;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.app.model.PJRestauration;
import com.app.scrap.RepPJRestauration;
import com.app.utils.Parametres;

import fa.log.LogApp.LogScrap;
import fa.log.config.LogConst;

@Service
public class ServicePageJauneRestauration2 extends Scraper implements SuperClassPJ {

	ChromeDriver ch;
	int nombreParPage = 0;
	static String driverPath = "C:\\chromeDriver\\";
	private HtmlUnitDriver htmlUnitDriver;

	@Autowired
	RepPJRestauration rep;

	@Override
	public void saveItem(String... args) {
		PJRestauration pjRest = new PJRestauration();
		pjRest.setActivite_soumis(args[0]).setCommune(args[1]).setCp(args[2]).setNb_res(Integer.parseInt(args[3]))
				.setNb_page(Integer.parseInt(args[4])).setNb_par_page(Integer.parseInt(args[5])).setActivite(args[6])
				.setTel(args[7]).setDenomination(args[8]).setAdresse(args[9]).setPrestation(args[10])
				.setTypeCuisine(args[11]).setBudget(args[12]).setModePaiement(args[13]).setSite(args[14])
				.setPasse_sanitaire(args[15]).setSiret(args[16]).setSiren(args[17]).setAmbiance(args[18])
				.setDepartement(args[19]);

		rep.save(pjRest);
	}

	/*
	 * public void setup() { FactoryBrowser factoryB = new FactoryBrowser();
	 * HtmlUnitBrowser htmlB = (HtmlUnitBrowser) factoryB.create("htmlUnit");
	 * htmlUnitDriver = htmlB.setOptions(true, false, false,
	 * 60000).initBrowserDriver();
	 * 
	 * final String chromeBeta =
	 * "C:\\Program Files\\Google\\Chrome Beta\\Application\\chrome.exe"; ch =
	 * (ChromeDriver) this.setupCh(Parametres.CHROME_DRIVER_PATH, true,
	 * false,"C:\\Users\\GEOMADA PC3\\AppData\\Local\\Google\\Chrome Beta\\User Data"
	 * , "", chromeBeta); }
	 */
//	public void setup() {
//		final String binaryPath = "C:\\Program Files\\Google\\Chrome Beta\\Application\\chrome.exe";
//		final String dataDirChrome = "C:\\Users\\GEOMADA PC3\\AppData\\Local\\Google\\Chrome\\User Data";
//		final String dataDirChromeBeta = "C:\\Users\\GEOMADA PC3\\AppData\\Local\\Google\\Chrome Beta\\User Data";
//		ChromeOptions chromeOptions = new ChromeOptions();
//		
//		ch = (ChromeDriver) this.setupCh(driverPath, true, false, dataDirChromeBeta, "", binaryPath);
//	
//	}

	public void setup() {

		System.setProperty("webdriver.chrome.driver", "C:\\ChromeDriver2\\chromedriver.exe");

		final String userProfile = "C:\\Users\\GEOMADA PC4\\AppData\\Local\\Google\\Chrome Beta\\User Data";
		final String binaryPath = "C:\\Program Files\\Google\\Chrome Beta\\Application\\chrome.exe";
		// final String dataDirChromeBeta = "C:\\Users\\GEOMADA
		// PC3\\AppData\\Local\\Google\\Chrome Beta\\User Data";
		ChromeOptions chromeOptions = new ChromeOptions();

		chromeOptions.addArguments("user-data-dir=" + userProfile);

		chromeOptions.addArguments("--remote-allow-origins=*");

		chromeOptions.addArguments("--start-maximized");

		chromeOptions.setPageLoadStrategy(PageLoadStrategy.NORMAL);

		chromeOptions.setBinary(binaryPath);

		ch = new ChromeDriver(chromeOptions);

	}

	@Override
	public void scrap() throws Exception {

		String url = "https://www.pagesjaunes.fr/annuaire/chercherlespros?quoiqui=";
		String ficVal = "Liste_CP_rubriques_20220920_dep_20.txt";
		Scanner sc = new Scanner(
				new BufferedInputStream(new FileInputStream(new File(Parametres.REPERTOIRE_DATA_SOURCES + ficVal))));
		LogScrap log = new LogScrap("PJRestauration_dep_20.txt", LogConst.TYPE_LOG_FILE);
		int lastLine = (new File(LogConst.STORE_LOG_PATH + LogConst.PREFIX_LOG_FILE + "_PJRestauration_dep_20.txt")
				.exists()) ? Integer.parseInt(log.readLog().getLastLineProcessed().split("\t")[4]) : 1;
		int currentLine = 1;

		while (sc.hasNextLine()) {
			String[] lines = sc.nextLine().split("\\t");
			if (currentLine > lastLine) {

				String commune = lines[0];
				String cp = lines[1];
				String activiteSoumis = lines[2];
				String path = "_link_" + cp + "_" + commune + "_" + activiteSoumis + ".txt";
				if (!new File("link/link_" + cp + "_" + commune + "_" + activiteSoumis + ".txt").exists()) {
					String finalURL = url + activiteSoumis + "&ou="
							+ URLEncoder.encode(commune + "(" + cp + ")", "UTF-8") + "&univers=pagesjaunes&idOu=";
					ch.get(finalURL);

					System.out.println("URL : " + finalURL);

					System.out.println("Département : " + cp);

					ch.get(finalURL);

					String pageSource = Normalizer.normalize(ch.getPageSource().toLowerCase(), Normalizer.Form.NFD)
							.replaceAll("[^\\p{ASCII}]", "");

					pageSource = this.iscaptcha(pageSource, ch);
					String pageNumber = "";
					Document doc = Jsoup.parse(pageSource);
					boolean hasResult = doc.selectFirst(".manohy_tsika") != null;
					System.out.println("has result " + hasResult);
					if (hasResult) {
						Element elementPageNumber = doc.selectFirst("#sel-compteur");
						String pageNumberTmp = elementPageNumber.text();
						Pattern pattern = Pattern.compile("/ \\d{1,}");
						Matcher match = pattern.matcher(pageNumberTmp);
						while (match.find())
							pageNumber = match.group().replaceAll("/", " ").trim();

						System.out.println("Scan page 1 ...");

						List<String> list_link = this.getLinks(doc, cp);
						for (String str : list_link) {
							writeLink(str, "link_" + cp + "_" + commune + "_" + activiteSoumis + ".txt");
						}

						// System.out.println("--------------------------------------------\n");

						int i = 2;
						if (Integer.parseInt(pageNumber) > 1) {
							while (i <= Integer.valueOf(pageNumber)) {

								if (i <= Integer.valueOf(pageNumber))
									ch.findElement(By.id("pagination-next")).click();

								Thread.sleep(200);

								pageSource = Normalizer.normalize(ch.getPageSource().toLowerCase(), Normalizer.Form.NFD)
										.replaceAll("[^\\p{ASCII}]", "");
								pageSource = this.iscaptcha(pageSource, ch);

								doc = Jsoup.parse(pageSource);

								System.out.println("Scan page " + i + " sur " + pageNumber + "...");

								list_link.addAll(this.getLinks(doc, cp));
								for (String str : list_link) {
									writeLink(str, "link_" + cp + "_" + commune + "_" + activiteSoumis + ".txt");
								}
								// System.out.println("--------------------------------------------\n");

								elementPageNumber = doc.selectFirst("#sel-compteur");
								pageNumberTmp = (elementPageNumber != null) ? elementPageNumber.text() : "";
								pattern = Pattern.compile("/ \\d{1,}");
								match = pattern.matcher(pageNumberTmp);
								while (match.find())
									pageNumber = match.group().replaceAll("/", " ").trim();
								if (this.noResponse(pageSource))
									break;

								i++;
							}
						}
						
						if(new File("link/link_" + cp + "_" + commune + "_" + activiteSoumis + ".txt").exists()) {
							Scanner sc2 = new Scanner(new BufferedInputStream(new FileInputStream(
									new File("link/link_" + cp + "_" + commune + "_" + activiteSoumis + ".txt"))));
							LogScrap logLink = new LogScrap("link_" + cp + "_" + commune + "_" + activiteSoumis + ".txt",
									LogConst.TYPE_LOG_FILE);

							int cursor = (new File(LogConst.STORE_LOG_PATH + LogConst.PREFIX_LOG_FILE + "_link_" + cp + "_"
									+ commune + "_" + activiteSoumis + ".txt").exists())
											? Integer.parseInt(logLink.readLog().getLastLineProcessed().split("\t")[4])
											: 1;
							int k = 1;
							while (sc2.hasNextLine()) {
								String link = sc2.nextLine();
								if (k >= cursor) {
									System.out.println("uri : " + link);

									ch.get(link);

									pageSource = Normalizer.normalize(ch.getPageSource().toLowerCase(), Normalizer.Form.NFD)
											.replaceAll("[^\\p{ASCII}]", "");
									pageSource = this.iscaptcha(pageSource, ch);

									doc = Jsoup.parse(pageSource);

									this.retrieveData(doc, cp, activiteSoumis, commune, lastLine, currentLine, i);

									logLink.writeLog(new Date(), link, "no_error", 1, k, 0);

								}
								k++;
							}

							sc2.close();
						}else {
							System.err.println("\t PAS DE LIEN A SCRAPER DONC PAS DE FICHIER");
						}

						log.writeLog(new Date(), lines[1], "no_error", 1, currentLine, 0);
						
					} else {
						System.out.println("skip coz no result");

						boolean isRecaptcha = doc.selectFirst("#challenge-running") != null;
						if (isRecaptcha) {
							System.out.println("<<<<<<<< RECAPTCHA DETECTED BROWSER CLOSED AUTOMATICLY >>>>>>>");
							ch.close();
						} else {
							log.writeLog(new Date(), lines[1], "no_error", 1, currentLine, 0);
						}

					}
				} else {
					Scanner sc2 = new Scanner(new BufferedInputStream(new FileInputStream(
							new File("link/link_" + cp + "_" + commune + "_" + activiteSoumis + ".txt"))));
					LogScrap logLink = new LogScrap("link_" + cp + "_" + commune + "_" + activiteSoumis + ".txt",
							LogConst.TYPE_LOG_FILE);

					int cursor = (new File(LogConst.STORE_LOG_PATH + LogConst.PREFIX_LOG_FILE + "_link_" + cp + "_"
							+ commune + "_" + activiteSoumis + ".txt").exists())
									? Integer.parseInt(logLink.readLog().getLastLineProcessed().split("\t")[4])
									: 1;
					int k = 1;
					while (sc2.hasNextLine()) {
						String link = sc2.nextLine();
						if (k >= cursor) {
							System.out.println("uri : " + link);

							ch.get(link);

							String pageSource = Normalizer.normalize(ch.getPageSource().toLowerCase(), Normalizer.Form.NFD)
									.replaceAll("[^\\p{ASCII}]", "");
							pageSource = this.iscaptcha(pageSource, ch);

							Document doc = Jsoup.parse(pageSource);

							this.retrieveData(doc, cp, activiteSoumis, commune, lastLine, currentLine, k);

							logLink.writeLog(new Date(), link, "no_error", 1, k, 0);

						}
						k++;
					}

					sc2.close();
					log.writeLog(new Date(), lines[1], "no_error", 1, currentLine, 0);
				}

			}
			currentLine++;
		}

	}

	private void retrieveData(Document doc, String cp, String activiteSoumis, String commune, int nombreResultat,
			int pageNumber, int nombreResultatParPage) {
		
		boolean isRecaptcha = doc.selectFirst("#challenge-running") != null;
		if (isRecaptcha) {
			System.out.println("<<<<<<<< RECAPTCHA DETECTED BROWSER CLOSED AUTOMATICLY >>>>>>>");
			ch.close();
		}

		Element agencyNameElement = doc.selectFirst("#teaser-header > div.row > div > div > div.denom > h1");
		String nomAgence = (agencyNameElement != null) ? agencyNameElement.text() : "";
		nomAgence = nomAgence.replaceAll("[^a-z0-9 ]", " ").toUpperCase();
		System.out.println("nom : " + nomAgence);

		Elements adresseElements = doc.select(
				"#teaser-footer > div > div > div.address-container.marg-btm-s > a.teaser-item.black-icon.address.streetaddress.clearfix.map-click-zone.pj-lb.pj-link > span");
		String adresse = "";
		if (!adresseElements.isEmpty()) {
			for (Element adresseElement : adresseElements)
				adresse += adresseElement.text() + " ";
		}
		adresse = adresse.replaceAll("[^a-z0-9 ]", " ").toUpperCase();
		System.out.println("adresse : " + adresse);

		Element telElement = doc
				.selectFirst("#teaser-footer > div > div > div.zone-fantomas > div > span > span.coord-numero.noTrad");
		String tel = (telElement != null) ? telElement.text() : "";
		System.out.println("tel : " + tel);

		Elements prestationsElements = doc.select(
				"#zone-info > div.zone-produits-presta-services-marques.fd-bloc > div.ligne.prestations.marg-btm-m > ul > li");
		String prestations = "";
		if (!prestationsElements.isEmpty()) {
			for (Element prestationElement : prestationsElements) {
				prestations += prestationElement.text() + " & ";
			}
		} else {
			prestationsElements = doc.select("#zone-info > div.zone-produits-presta-services-marques.fd-bloc > "
					+ "div.ligne.cris > div.cri-prestations > ul > li");
			if (!prestationsElements.isEmpty())
				for (Element prestationElement : prestationsElements) {
					prestations += prestationElement.text() + " ";
				}
			prestationsElements = doc
					.select("#zone-info > div.zone-produits-presta-services-marques.fd-bloc > div.ligne.cris > "
							+ "div.cri-produits--prestations-et-services > ul > li");
			if (!prestationsElements.isEmpty())
				for (Element prestationElement : prestationsElements) {
					prestations += prestationElement.text() + " & ";
				}
		}
		System.out.println("prestations : " + prestations);

		Elements activitiesElements = doc.select("#zonemultiactivite > div > ul >li ");
		String activite = "";
		if (!activitiesElements.isEmpty()) {
			for (Element activityElement : activitiesElements) {
				activite += activityElement.text() + " & ";
			}
		}
		System.out.println("activite : " + activite);

		String siret = "";
		Element blocEtabElement = doc.selectFirst("#zoneb2b > dl.info-etablissement.marg-btm-s.zone-b2b.txt_sm");
		if (blocEtabElement != null) {
			Element siretElement = blocEtabElement.selectFirst("dd:nth-child(2)");
			siret = (siretElement.text().matches("[0-9]{14,}")) ? siretElement.text() : "";
		}
		System.out.println("siret : " + siret);
		String siren = "";
		Element blocEntreprise = doc.selectFirst("#zoneb2b > dl.info-entreprise.marg-btm-s.zone-b2b.txt_sm");
		if (blocEntreprise != null) {
			Element sirenElement = blocEntreprise.selectFirst(" dd:nth-child(2) > strong");
			siren = (sirenElement.text().matches("[0-9]{6,}")) ? sirenElement.text() : "";
		}
		System.out.println("siren : " + siren);

		String typeCuisine = "";
		Elements cuisineElements = doc
				.select("#teaser-description > div.bloc-infos-teaser.marg-btm-m.row > div.bloc-info-cuisine > ul> li");
		if (!cuisineElements.isEmpty())
			for (Element cuisineElement : cuisineElements) {
				typeCuisine += Normalizer.normalize(cuisineElement.text(), Normalizer.Form.NFKD)
						.replaceAll("[^\\p{ASCII}]", "") + " & ";
			}

		System.out.println("type cuisine : " + typeCuisine);

		Elements budgetElements = doc.select("#tarif-generique > ul >li");
		String budget = "";
		if (!budgetElements.isEmpty()) {
			for (Element budgetElement : budgetElements) {
				budget += budgetElement.text().replaceAll("[^0-9-]", "") + " ";
			}
			budget += "euros";
		}
		System.out.println("budget : " + budget);

		Elements ambianceElements = doc
				.select("#teaser-description > div.bloc-infos-teaser.marg-btm-m.row > div.bloc-info-ambiance > ul> li");
		String ambiance = "";
		if (!ambianceElements.isEmpty())
			for (Element ambianceElement : ambianceElements) {
				ambiance += ambianceElement.text() + " ";
			}
		System.out.println("ambiance : " + ambiance);

		Element siteWebElement = doc
				.selectFirst("#teaser-footer > div > div > div.lvs-container.marg-btm-s > a > span.value");
		String siteWeb = (siteWebElement != null) ? siteWebElement.text() : "";
		System.out.println("site web : " + siteWeb);

		Element passeSanitaireElement = doc.selectFirst("#zone-informations-pratiques");
		int passeSanitaire = 0;
		if(passeSanitaireElement != null) {
			passeSanitaire = (passeSanitaireElement.text().contains("pass sanitaire requis")) ? 1 : 0;
			System.out.println("passe sanitaire : " + passeSanitaire);
		}

		Elements modePaiementElements = doc
				.select("#zone-informations-pratiques > div.row > div.col-sm-12.col-md-6.zone-a-propos >"
						+ " div.zone-info-moyen-paiement > div > ul > li> figure > img");
		String modePaiement = "";
		if (!modePaiementElements.isEmpty()) {
			for (Element modePaiementElement : modePaiementElements) {
				modePaiement += modePaiementElement.attr("alt") + " & ";
			}
		}
		System.out.println("mode paiement : " + modePaiement);

		if(nomAgence != "") {
			this.saveItem(activiteSoumis, commune, cp, String.valueOf(nombreResultat), String.valueOf(pageNumber),
					String.valueOf(nombreResultatParPage), activite, tel, nomAgence, adresse, prestations, typeCuisine,
					budget, modePaiement, siteWeb, String.valueOf(passeSanitaire), siret, siren, ambiance, cp.substring(0, 2));
		}
		System.out.println("-----------------------------------------------------------------------------\n");
	}

	private List<String> getLinks(Document doc, String cp) {

		Elements li_links = doc.select("li.bi-generic.misy_ao_e");

		List<String> list_links = new ArrayList<String>();

		for (Element link : li_links) {
			String id = link.attr("id").replace("epj-", "");
			// System.out.println("ID : "+id);

			Element adresse = link.selectFirst("div.bi-clic-mobile > div > div > div.bi-address.small > a");

			// System.out.println("Adresse : "+adresse.text());

			if (adresse.text().contains(cp)) {

				Element bloc_data = link.selectFirst("div.bi-ctas > button");

				if (bloc_data != null) {
					String data = bloc_data.attr("data-pjsethisto");

					JSONObject json = null;
					String code_id = "";
					String code_etab = "";
					String code_localite = "";
					String code_rubrique = "";

					try {
						json = new JSONObject(data);

						if (json.getJSONObject("data").getJSONObject("fd").has("id_bloc"))
							code_id = json.getJSONObject("data").getJSONObject("fd").getString("id_bloc");
						if (json.getJSONObject("data").getJSONObject("fd").has("code_etab"))
							code_etab = json.getJSONObject("data").getJSONObject("fd").getString("code_etab");
						if (json.getJSONObject("data").getJSONObject("fd").has("code_localite")) {
							code_localite = json.getJSONObject("data").getJSONObject("fd").getString("code_localite");
						}
						if (json.getJSONObject("data").getJSONObject("fd").has("code_rubrique")) {
							code_rubrique = json.getJSONObject("data").getJSONObject("fd").getString("code_rubrique");
						}

					} catch (Exception e) {
						if (e instanceof JSONException) {
							json = new JSONObject(bloc_data.attr("data-pjhistofantomas"));
							code_id = json.getString("data");
						}
					}
					if (code_etab != "" && code_localite != "" && code_rubrique != "") {
						list_links.add("https://www.pagesjaunes.fr/pros/detail?code_etablissement=" + code_etab
								+ "&code_localite=L" + code_localite + "&code_rubrique=" + code_rubrique);
					} else if (code_id != "") {
						list_links.add("https://www.pagesjaunes.fr/pros/" + code_id);

					} else {
						list_links.add("https://www.pagesjaunes.fr/pros/" + id);
					}
				}
			}

		}
		return list_links;

	}

	public void quit() {
		ch.close();
	}

	public void writeLink(String link, String fileName) throws IOException {
		if(!new File("link/").exists())
			new File("link/").mkdirs();
		BufferedWriter bw = new BufferedWriter(new FileWriter(new File("link/"+fileName), true));
		bw.write(link+"\r\n");
		bw.close();
	}
	
}
