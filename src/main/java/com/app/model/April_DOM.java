package com.app.model;

import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="april_dom", schema="scrap")
public class April_DOM {
   
	@Id
	@SequenceGenerator(name="scrap.april_dom_id_seq",
	                     sequenceName ="scrap.april_dom_id_seq",
	                     allocationSize=1)
    @GeneratedValue( strategy=GenerationType.SEQUENCE,
                     generator="scrap.april_dom_id_seq")
	  
	@Column(name="id",updatable=false)
	private int id;
	
	@Column(name="nbagences",columnDefinition="varchar")
    private String nbAgences;
	
	@Column(name="region_soumise", columnDefinition="varchar")
	private String regionSoumise;
	
	@Column(name="lien_agence", columnDefinition="varchar")
    private String lienAgence;
    
	@Column(name="nom_agence", columnDefinition="varchar")
	private String nomAgence;
	
	@Column(name="adresse", columnDefinition="varchar")
    private String adresse;
	
	@Column(name="tel", columnDefinition="varchar")
    private String tel;
	
	@Column(name="horaires", columnDefinition="varchar")
    private String horaires;
	
	@Column(name="agencehtm", columnDefinition="varchar")
    private String agenceHTM;
	
	@Column(name="annee_du_scrap" ,columnDefinition="varchar(4) DEFAULT date_part('year',CURRENT_DATE)" )
    private String anneeScrap;
	
	public April_DOM() {
		LocalDate ld= new Date().toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
		anneeScrap=String.valueOf(ld.getYear()+1);
	}
	public int getId() {
		return id;
	}
	public April_DOM setId(int id) {
		this.id = id;
		return this;
	}
	
	public String getNbAgences() {
		return nbAgences;
	}
	public April_DOM setNbAgences(String nbAgences) {
		this.nbAgences = nbAgences;
		return this;
	}
	
	public String getLienAgence() {
		return lienAgence;
	}
	public April_DOM setLienAgence(String lienAgence) {
		this.lienAgence = lienAgence;
		return this;
	}
	public String getNomAgence() {
		return nomAgence;
	}
	public April_DOM setNomAgence(String nomAgence) {
		this.nomAgence = nomAgence;
		return this;
	}
	public String getAdresse() {
		return adresse;
	}
	public April_DOM setAdresse(String adresse) {
		this.adresse = adresse;
		return this;
	}
	public String getTel() {
		return tel;
	}
	public April_DOM setTel(String tel) {
		this.tel = tel;
		return this;
	}
	public String getHoraires() {
		return horaires;
	}
	public April_DOM setHoraires(String horaires) {
		this.horaires = horaires;
		return this;
	}
	public String getAgenceHTM() {
		return agenceHTM;
	}
	public April_DOM setAgenceHTM(String agenceHTM) {
		this.agenceHTM = agenceHTM;
		return this;
	}
	public String getAnneeScrap() {
		return anneeScrap;
	}
	public April_DOM setAnneeScrap(String anneeScrap) {
		this.anneeScrap = anneeScrap;
		return this;
	}
	public String getRegionSoumise() {
		return regionSoumise;
	}
	public April_DOM setRegionSoumise(String regionSoumise) {
		this.regionSoumise = regionSoumise;
		return this;
	}
}
