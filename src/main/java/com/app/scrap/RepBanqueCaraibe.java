package com.app.scrap;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.app.model.BanqueCaraibe;

@Repository
public interface RepBanqueCaraibe extends JpaRepository<BanqueCaraibe, Integer> {

}
