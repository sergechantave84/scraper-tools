import com.app.scrap.Scrapper1Application;
import com.app.service.ServiceActionLogement;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.FileNotFoundException;
import java.io.IOException;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = Scrapper1Application.class)
@TestPropertySource(locations = "classpath:test_application.properties")
public class ActionLogementTest {

    @Autowired
    ServiceActionLogement serv;

    @Test
    public void test() throws IOException {
        serv.setup();
        serv.scrapActionlogement();
    }
}
