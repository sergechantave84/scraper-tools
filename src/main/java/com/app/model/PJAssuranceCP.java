package com.app.model;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="page_jaune_assurance_cp", schema="scrap")
public class PJAssuranceCP extends PJAssurance {

	@Id
	@SequenceGenerator(name="scrap.page_jaune_assurance_cp_id_seq",
	                  sequenceName ="scrap.page_jaune_assurance_cp_id_seq",
	                  allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE,
	generator = "scrap.page_jaune_assurance_cp_id_seq")
	@Column(name="id", unique=true, nullable = false)
	private int id;
	
	@Column(name="cp_soumise",columnDefinition = "varchar")
	private String cp;
	

	
	@Column(name="annee_scrap", columnDefinition = "varchar")
	private String anneeScrap;
	
	@Column(name="activite_soumise",columnDefinition = "varchar")
	private String activiteSoumise;

	public PJAssuranceCP() {
		LocalDate dt=new Date().toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
		anneeScrap=String.valueOf(dt.getYear()+1);
	}
	public String getCp() {
		return cp;
	}

	public PJAssuranceCP setCp(String cp) {
		this.cp = cp;
		return this;
	}
	public String getActiviteSoumise() {
		return activiteSoumise;
	}
	public PJAssuranceCP setActiviteSoumise(String activiteSoumise) {
		this.activiteSoumise = activiteSoumise;
		return this;
	}

	
	
	
}
