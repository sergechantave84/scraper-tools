import java.io.IOException;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import com.app.scrap.Scrapper1Application;
import com.app.service.ServiceTripadvisor;

@RunWith(SpringRunner.class)
@SpringBootTest(classes=Scrapper1Application.class)
@TestPropertySource(locations = "classpath:test_application.properties")
public class TripadvisorTest {
	

	@Autowired
	ServiceTripadvisor srv;
	
	@Test
	public void test() throws IOException, InterruptedException {
		srv.setup();
		srv.scrapTripadvisor();
	}
}
