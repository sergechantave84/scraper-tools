package com.app.service;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.SocketTimeoutException;
import java.text.Normalizer;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Scanner;

import org.json.JSONArray;
import org.json.JSONObject;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.PageLoadStrategy;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.app.model.Generali;

import com.app.scrap.RepGenerali;
import com.app.utils.BrowserUtils;
import com.app.utils.Parametres;
import com.app.utils.Utilitaires;

import fa.browserUtils.ChromeBrowser;
import fa.browserUtils.FactoryBrowser;
import fa.browserUtils.HtmlUnitBrowser;
import fa.log.LogApp.LogScrap;
import fa.log.config.LogConst;

@Service
public class ServiceGenerali2 extends Scraper {

	@Autowired
	private RepGenerali rep;

	private List<Generali> geneList = new ArrayList<>();

	String driverPath = "C:\\ChromeDriver\\";
	String nomSITE = "GENERALI";
	String ficval = "CP_cantons";
	String dossierRESU = Parametres.REPERTOIRE_RESULT + "\\ASSU\\" + nomSITE;
	String url_accueil = "https://www.generali.fr/trouver-mon-agence-generali/";
	String fichier_valeurs;
	String CantonSoumis;
	String NomRegion;
	String ligne;
	int num_ligne = 2;
	int NbAgences;
	int NbAgences_Ville;
	int numreponse = 1;
	int nbRecuperes;
	int num_ligneficval;
	int parcoursficval;
	int Resultats;
	ChromeDriver chromeDriver;
	BufferedWriter sortie;
	BufferedWriter sortie_log;
	Date Temps_Fin;
	
	HtmlUnitDriver unitDriver;

	public Generali saveData(Generali t) {

		return rep.save(t);

	}

	public List<Generali> saveDataAll(List<Generali> t) {

		return rep.saveAll(t);

	}

	public List<Generali> getElmnt() {

		return rep.findAll();
	}

	public Generali getById(int id) {

		return rep.findById(id).orElse(null);
	}

	public String deleteProduct(int id) {
		try {
			rep.deleteById(id);

		} catch (Exception e) {
			e.printStackTrace();
		}

		return "removed";
	}

	public Generali updateProduct(Generali t) {
		// GeneraliODO Auto-generated method stub
		return null;
	}

	public Generali setData(String cantonSoumis, String nbAgences, String nbAgencesGeneralirouvees, String lienAgence,
			String nomAgence, String adresse, String tel, String agenceHtm) {
		Generali generali = new Generali();
		generali.setCantonSoumis(cantonSoumis).setNbAgences(nbAgences).setNbAgencesTrouvees(nbAgencesGeneralirouvees)
				.setLienAgence(lienAgence).setNomAgence(nomAgence).setAdresse(adresse).setTel(tel)
				.setAgenceHtm(agenceHtm);
		this.saveData(generali);
		return generali;
	}

	public List<Generali> showFirstRows() {
		List<Generali> a = rep.getFirstRow(100, Generali.class);
		return a;
	}

	public List<Generali> showLastRows() {
		List<Generali> a = rep.getLastRow(100, Generali.class);
		return a;
	}

	public void setUp() throws Exception {
		
		final String chromeBeta = "C:\\Program Files\\Google\\Chrome Beta\\Application\\chrome.exe";
		chromeDriver = (ChromeDriver) this.setupCh(Parametres.CHROME_DRIVER_PATH, true, false,
				"C:\\Users\\GEOMADA PC4\\AppData\\Local\\Google\\Chrome Beta\\User Data", "", chromeBeta);
		
		FactoryBrowser fact = new FactoryBrowser();
		HtmlUnitBrowser hB = (HtmlUnitBrowser) fact.create("htmlUnit");
		unitDriver = hB.setOptions(false, false, false, 60000).initBrowserDriver();
	}

	public List<Generali> open_GENERALI_byselenium() throws Exception {

		boolean dReadyState = false;
		fichier_valeurs = Parametres.REPERTOIRE_DATA_SOURCES + ficval + ".txt";
		Scanner sc = new Scanner(new BufferedInputStream(new FileInputStream(new File(fichier_valeurs))));

		if (!new File(dossierRESU).exists())
			new File(dossierRESU).mkdirs();
		File fichier = new File(dossierRESU + "\\" + "RESULTATS_" + nomSITE + "_" + System.currentTimeMillis() + ".txt");
		LogScrap log = new LogScrap("AssuranceGENERALI.txt", LogConst.TYPE_LOG_FILE);
		num_ligneficval = ((new File(LogConst.STORE_LOG_PATH + LogConst.PREFIX_LOG_FILE + "_AssuranceGENERALI.txt"))
				.exists()) ? Integer.parseInt(log.readLog().getLastLineProcessed().split("\\t")[4]) : 1;

		parcoursficval = 1;
		Document doc = null;
		String nom_agence ="";
		String tel ="";
		String adresse ="";
		String horaires ="";
		String agents ="";
		String lien ="";
		String agenceHtml ="";
		
		while (sc.hasNextLine()) {
			
			String line = sc.nextLine();
			
			String[] fic_valeurs = line.split("\\t");
			
			if (parcoursficval > num_ligneficval) {
				
				CantonSoumis = fic_valeurs[0];
				
				chromeDriver.get(url_accueil);
				
				System.out.println("CantonSoumis : "+CantonSoumis);
				
				Thread.sleep(2000);
				
				WebElement input = chromeDriver.findElement(By.xpath("//*[@id=\"__next\"]/div/div/div[2]/div/div[2]/div[2]/div[2]/div/div/div/input"));
				
				WebElement clickBtn = chromeDriver.findElement(By.xpath("//*[@id=\"search-button\"]"));
				
				input.sendKeys(CantonSoumis);
				Thread.sleep(1000);
				//input.sendKeys(Keys.ENTER);
				clickBtn.click();
				
				Thread.sleep(3000);
				
				boolean isLoaded = isLoaded(chromeDriver);
				
				if(isLoaded) {
					//ystem.out.println("Loaded......... ");
					String currentUrl = chromeDriver.getCurrentUrl();
					
					System.out.println("Current url : " +currentUrl);
					
					chromeDriver.get(currentUrl);

					String pageSource = chromeDriver.getPageSource();

					pageSource = Normalizer.normalize(chromeDriver.getPageSource().toLowerCase(), Normalizer.Form.NFD)
							.replaceAll("[^\\p{ASCII}]", "");

					doc = Jsoup.parse(pageSource);

					Thread.sleep(2000);
					
					/* Modification du 18-11-2022 */
					
					Elements roots = doc.select("#__next > div > div > div.em-layout__container > div.em-results__container > div.dual-frame__container > div.dual-frame__frames-container > div > div > div.em-frame__container.em-frame__container--secondary.em-frame__container--small.poi-list-element");
					
					
					if(roots.size() > 0) {
						for(Element root : roots) {
							
							agents ="";
							
							Element nomElem =root.selectFirst("div > div > div.pois-list-element__second-column > h2 > a");
							//#__next > div > div > div.em-layout__container > div.em-results__container > div.dual-frame__container > div.dual-frame__frames-container > div > div > div > 
							Element horaire = root.selectFirst("div > div.pois-list-element__first-line > div.pois-list-element__second-column > div > div.em-schedules-info__opening-info__schedules.em-schedules-info__opening-info__schedules--collapsed");
							
							Element add1 = doc.selectFirst("div > div > div.pois-list-element__second-column > p.pois-list-element__address-line");
							Element add2 = doc.selectFirst("div > div > div.pois-list-element__second-column > p.pois-list-element__address-info");
							
							nom_agence = nomElem!= null ? this.normalize(nomElem.text().toUpperCase().trim()) :"";
							adresse = (add1 != null && add2 != null)  ? this.normalize((add1.text()+" "+add2.text()).toUpperCase().trim()) :"";
							
							horaires = horaire != null ? this.normalize(horaire.text().trim()) : "";
							
							lien  = nomElem!= null ? nomElem.attr("href") :"";
							
							System.out.println("Nom : "+nom_agence);
							System.out.println("Lien : "+lien);
							System.out.println("Horaire : "+horaires);
							System.out.println("Adresse : "+adresse);
							
							if(lien != "") {
								String newUrl = lien + "qui-sommes-nous";
								chromeDriver.get(newUrl);
								
								Thread.sleep(2000);
								
								pageSource = unitDriver.getPageSource();
								
								pageSource = Normalizer.normalize(chromeDriver.getPageSource().toLowerCase(), Normalizer.Form.NFD)
										.replaceAll("[^\\p{ASCII}]", "");
								
								doc = Jsoup.parse(pageSource);
								
								
								
								Element telElem = doc.selectFirst("#js-header-banner > div.col-12.col-md-4.col-lg-3.px-md-0.mb-4.mb-md-0 > div.wrap-map > section > ul > li:nth-child(3) > a > p");
								Elements agentsElem = doc.select("#block-sqli-content > section:nth-child(5) > div > div");
							
								tel = telElem != null ? telElem.text() :"";
								
								System.out.println("Telephone : "+tel);
								
								for(Element agentElem : agentsElem) {
									
									if(agentElem.text().contains("agent general")) {
										
										Element agentsElems =agentElem.selectFirst("figure > figcaption > h3");
										agents +=agentsElems.text().toUpperCase()+" & ";
									}
									
								}
								
								System.out.println("agents : "+agents);
								
								geneList.add(this.setData(CantonSoumis, String.valueOf(NbAgences),
										String.valueOf(roots.size()), lien, nom_agence, adresse,
										tel, agenceHtml));
								
								System.out.println("---------------------------------------\n");
								
							}
							
						}
					}else {
						System.err.println("Aucun resultat!");
					}
					
					log.writeLog(new Date(), CantonSoumis, "no_error", 1, parcoursficval, 0);
					

				}
			}
			parcoursficval++;
		}
		sc.close();
		return geneList;
	}

	public  boolean isLoaded(WebDriver driver) 
	 {
		
		 String jsScript="return document.readyState", dReadyState="";
		 JavascriptExecutor jsExecutor=(JavascriptExecutor)driver;
		 try 
		   {
			   dReadyState=(String) jsExecutor.executeScript(jsScript);
			   while( !dReadyState.equals("complete")) 
			   {
				   dReadyState=(String) jsExecutor.executeScript(jsScript);
			   }
		   }
		   catch(Exception e) 
		   {
			  return(false);
		   }
		   return (dReadyState.equals("complete"));
		   
	 }
	public void tearDown() {
		chromeDriver.quit();

	}

	@Override
	public void saveItem(String... args) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void scrap() throws Exception {
		// TODO Auto-generated method stub
		
	}

}
