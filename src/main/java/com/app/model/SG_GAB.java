package com.app.model;

import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "banque_sg_gab", schema = "scrap")
public class SG_GAB {

	@Id
	@SequenceGenerator(name = "scrap.banque_sg_gab_id_seq", sequenceName = "scrap.banque_sg_gab_id_seq", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "scrap.banque_sg_gab_id_seq")

	@Column(name = "id", updatable = false)
	private int id;

	@Column(name = "cpsoumis", columnDefinition = "varchar")
	private String cpSoumis;

	@Column(name = "nbagences", columnDefinition = "varchar")
	private String nbAgences;

	@Column(name = "nomweb", length = 50)
	private String nomWeb;

	@Column(name = "adresse", columnDefinition = "varchar")
	private String adresse;

	@Column(name = "dab", columnDefinition = "varchar")
	private String dab;

	@Column(name = "hors_site", columnDefinition = "varchar")
	private String hors_site;

	@Column(name = "agencehtm", columnDefinition = "varchar")
	private String agenceHtm;
	
	@Column(name = "codeguichet", columnDefinition = "varchar")
	private String codeGuichet;

	@Column(name = "annee_du_scrap", columnDefinition = "varchar(4) DEFAULT date_part('year',CURRENT_DATE)")
	private String anneeScrap;

	public SG_GAB() {
		LocalDate ld = new Date().toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
		anneeScrap = String.valueOf(ld.getYear() + 1);
	}

	public int getId() {
		return id;
	}

	public SG_GAB setId(int id) {
		this.id = id;
		return this;
	}

	public String getCpSoumis() {
		return cpSoumis;
	}

	public SG_GAB setCpSoumis(String cpSoumis) {
		this.cpSoumis = cpSoumis;
		return this;
	}

	public String getNbAgences() {
		return nbAgences;
	}

	public SG_GAB setNbAgences(String nbAgences) {
		this.nbAgences = nbAgences;
		return this;
	}

	public String getNomWeb() {
		return nomWeb;
	}

	public SG_GAB setNomWeb(String nomWeb) {
		this.nomWeb = nomWeb;
		return this;
	}

	public String getAdresse() {
		return adresse;
	}

	public SG_GAB setAdresse(String adresse) {
		this.adresse = adresse;
		return this;
	}

	public String getDab() {
		return dab;
	}

	public SG_GAB setDab(String dab) {
		this.dab = dab;
		return this;
	}

	public String getHors_site() {
		return hors_site;
	}

	public SG_GAB setHors_site(String hors_site) {
		this.hors_site = hors_site;
		return this;
	}

	public String getAgenceHtm() {
		return agenceHtm;
	}

	public SG_GAB setAgenceHtm(String agenceHtm) {
		this.agenceHtm = agenceHtm;
		return this;
	}

	public String getAnneeScrap() {
		return anneeScrap;
	}

	public SG_GAB setAnneeScrap(String anneeScrap) {
		this.anneeScrap = anneeScrap;
		return this;
	}

	public String getCodeGuichet() {
		return codeGuichet;
	}

	public SG_GAB setCodeGuichet(String codeGuichet) {
		this.codeGuichet = codeGuichet;
		return this;
	}
}
