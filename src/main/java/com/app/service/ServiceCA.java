package com.app.service;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.SocketTimeoutException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Scanner;

import org.json.JSONObject;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import org.openqa.selenium.PageLoadStrategy;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.app.model.CA;
import com.app.scrap.RepCA;
import com.app.utils.BrowserUtils;
import com.app.utils.Parametres;
import com.app.utils.Utilitaires;

import fa.browserUtils.ChromeBrowser;
import fa.browserUtils.FactoryBrowser;
import fa.browserUtils.HtmlUnitBrowser;
import fa.log.LogApp.LogScrap;
import fa.log.config.LogConst;

@Service
public class ServiceCA extends Scraper {

	@Autowired
	private RepCA rep;

	private static String driverPath = "C:\\ChromeDriver\\";
	String nomSITE = "CA";
	// TODO change
	String ficval = "liens_CA"; // liste des enseignes du CA

	String url = "";
	// old_url
	// "https://www.credit-agricole.fr/particulier/acces-cr-et-agence.html?origin=/content/ca/national/npc/fr/particulier/acces-cr-et-agence/localisation-agence.html";
	String cp; // enseigne en clair
	String ville; // enseigne ds adr url (nom du site)
	String fichier_valeurs;

	int num_ligne = 2;
	int NbVilles;
	int NbAgences;
	int numreponse = 1;

	int indice_ville;

	int num_ligneficval;
	int parcoursficval;

	HtmlUnitDriver htmlUnitDriver;
	// htmlUnitDriver htmlUnitDriver;
	BufferedWriter sortie;
	BufferedWriter sortie_log;
	Date Temps_Fin;
	
	ChromeDriver chromeDriver;

	@Override
	public void saveItem(String... args) {
		CA ca = new CA();
		ca.setLienEnseigne(args[0]).setNbVilles(args[1]).setNumVilles(args[2]).setVilleSoumise(args[3])
				.setNbAgences(args[4]).setLienAgence(args[5]).setNomWeb(args[6]).setAdresse(args[7]).setTel(args[8])
				.setFax(args[9]).setHorairesHtm(args[10]).setHorairesTxt(args[11]).setServices(args[12])
				.setCoordonnees(args[13]).setAgenceHtm(args[14]).setRelais(args[15]);
		rep.save(ca);

	}

	public void setup() {

		htmlUnitDriver = (HtmlUnitDriver) this.setupHU();
		
		FactoryBrowser factBrowser = new FactoryBrowser();
		ChromeBrowser b = (ChromeBrowser) factBrowser.create("chrome");
		final String binaryPath = "C:\\Program Files\\Google\\Chrome Beta\\Application\\chrome.exe";
		
		chromeDriver = b.setOptions(PageLoadStrategy.NORMAL, driverPath, 60000, 60000, 60000, false, false, binaryPath)
				.initBrowserDriver();
	}
	
	public String deleteAll() {
		try {
			
			rep.deleteAll();
			System.out.println("Supprimee avec success");
	
		} catch (Exception e) {
			e.printStackTrace();
		}
	
		return "removed";
	}

	@Override
	public void scrap() throws IOException, InterruptedException {
		// boolean documentReadyState = false;
		fichier_valeurs = Parametres.REPERTOIRE_DATA_SOURCES + ficval + ".txt";
		Scanner sc = new Scanner(new BufferedInputStream(new FileInputStream(new File(fichier_valeurs))));
		Scanner sc1 = new Scanner(new File(fichier_valeurs));

		LogScrap log = new LogScrap("BanqueCA.txt", LogConst.TYPE_LOG_FILE);
		num_ligneficval = ((new File(LogConst.STORE_LOG_PATH + LogConst.PREFIX_LOG_FILE + "_BanqueCA.txt")).exists())
				? Integer.parseInt(log.readLog().getLastLineProcessed().split("\\t")[4])
				: 1;

		parcoursficval = 1;
		int i = 1;

		while (sc1.hasNextLine()) {
			String l = sc1.nextLine();
			if (i > 1) {
				NbVilles++;
			}
			i++;
		}
		sc1.close();
		while (sc.hasNextLine()) {
			String line = sc.nextLine();
			String[] fic_valeurs = line.split("\\t");

			if (parcoursficval > num_ligneficval) {

				cp = fic_valeurs[0];
				ville = fic_valeurs[1];
				// TODO change
				String uri1 = ville;
				try {
					System.out.println("first uri : " + uri1);
					htmlUnitDriver.get(uri1);

				} catch (Exception e) {
					if (e instanceof SocketTimeoutException) {
						htmlUnitDriver.get(uri1);

					}
				}
				
				String pageSource = htmlUnitDriver.getPageSource();
				Document doc = null;
				doc = Jsoup.parse(pageSource);

				Element root = doc.selectFirst(
						"body > main > div > div > div > div.container > div.indexTown-Content > div > div");
				List<String> agenciesLinkList = new ArrayList<>();
				List<String> subAgenciesLinks = new ArrayList<>();
				if (root != null) {

					if (!agenciesLinkList.isEmpty()) {
						agenciesLinkList.clear();
					}

					Elements agenciesLinkElement = root.select("ul");

					for (Element tmp4 : agenciesLinkElement) {

						Elements hrefs = tmp4.select("li>a");
						for (Element href : hrefs) {
							String strHref = href.attr("href");
							agenciesLinkList.add(strHref);
							//System.out.println("Url agence par departement : " +strHref);
						}
					}
				}

				if (!agenciesLinkList.isEmpty()) {
					NbAgences = agenciesLinkList.size();
					for (String path : agenciesLinkList) {

						String uri = "https://www.credit-agricole.fr" + path;
						try {
							System.out.println("alphabetic : " + uri);
							htmlUnitDriver.get(uri);

						} catch (Exception e) {
							if (e instanceof SocketTimeoutException) {
								System.out.println(uri);
								htmlUnitDriver.get(uri);

							}
						}
						
						pageSource = htmlUnitDriver.getPageSource();
						doc = Jsoup.parse(pageSource);

						//#main > div > div > div > div.container-fluid > div > div.container > div > div.StoreLocatorMap-content.js-StoreLocatorMap-content.has-mapLeft > div.StoreLocatorMap-Agencies.js-StoreLocatorMap-Agencies > ul
						Element main = doc.select(
								"body > main > div > div > div > div.container-fluid > div > div.container > div > "
										+ "div.StoreLocatorMap-content.js-StoreLocatorMap-content > div.StoreLocatorMap-Agencies.js-StoreLocatorMap-Agencies > ul")
								.first();
					
						int numVille = 0;

						if (main != null) {
							System.out.println("\n**************** not null *****************\n");
							
							//System.out.println(main);

							Elements pathsElements = main.select("li > script");
							
							//System.out.println(pathsElements);

							if (!subAgenciesLinks.isEmpty())
								subAgenciesLinks.clear();

							for (Element pathElement : pathsElements) {
								
								String href = pathElement.toString().split("a href=\"")[1].split("data-agencyid=")[0].trim().replaceAll("\"", "");
								
								//System.out.println(pathElement.toString().split("a href=\"")[1].split("data-agencyid=")[0]);

								//String href = pathElement.attr("href");
								subAgenciesLinks.add(href);

							}
							
							for (String path1 : subAgenciesLinks) {
								String uriVille = "https://www.credit-agricole.fr" + path1;
								//villeSoumise = path1;
								try {
									System.out.println("uriVille : " + uriVille);
									chromeDriver.get(uriVille);

								} catch (Exception e) {
									if (e instanceof SocketTimeoutException) {
										System.out.println(uriVille);
										chromeDriver.get(uriVille);

									}
								}
								
								pageSource = chromeDriver.getPageSource();
								doc = Jsoup.parse(pageSource);
								
								this.retrieveData(doc, path1, numVille, uriVille);
								
								
								// }

							}

						}else {
							//retrieve data
							
							System.out.println("\n ************** Tonga dia scrap*****************\n");
							
							this.retrieveData(doc, uri.replace("https://www.credit-agricole.fr", ""), numVille, uri);
						}

						// }

					}

					// enregistrer_journal(countAgencyNumber, 1);
				} else {
					// this.enregistrer("", "", "","",0, ville,"","","","","","");
				}

				// }

				// }
				//log.writeLog(new Date(), ville, "no_error", NbVilles, parcoursficval, 0);
			}
			parcoursficval++;
		}
		sc.close();

	}
	
	public void retrieveData(Document doc, String villeSoumise, int numVille, String uriVille) {
		
		String nom_agence = "";
		String adresse = "";
		String tel = "";
		String fax = "";
		String Services = "";
		String HorairesHTM = "";
		String HorairesTXT = "";
		String coord = "";
		String agences_HTM = "";
		//String villeSoumise = "";
		int relais = 0;
		
		
		Element infosRoot = doc.selectFirst(
				"#main > div > div > div");
		if (infosRoot != null) {
			nom_agence = "";
			adresse = "";
			tel = "";
			fax = "";
			Services = "";
			HorairesHTM = "";
			HorairesTXT = "";
			coord = "";
			agences_HTM = "";
			relais = 0;

			agences_HTM = infosRoot.outerHtml();
			agences_HTM = this.normalize(agences_HTM.replaceAll("\\s{1,}", ""));
			
			//System.out.println(infosRoot);
			
			Element agencyNameElmnt = infosRoot.selectFirst("div > div.npc-sl-strct-coordonnee > div > div > h1");
			
			nom_agence = (agencyNameElmnt != null) ? this.normalize(agencyNameElmnt.text().replaceAll("\\s{2,}", " ").trim())
					: "";
			
			Element agencyAddress = infosRoot.selectFirst("div > div.npc-sl-strct-coordonnee > div > div > div.npc-sl-strct-infos-ctct-adresse");
			
			adresse = (agencyAddress != null) ? this.normalize(agencyAddress.text().replaceAll("\\s{2,}", " ").trim().toUpperCase()) : "";
			
			
			System.out.println("nom agence : " + nom_agence);
			
			Element agencyCoordinate = infosRoot.selectFirst("div > div.npc-sl-strct-coordonnee > div > div > div.npc-sl-strct-infos-ctct-actions");
			
			System.out.println("agency address : " + adresse);
			
			// RootElement for phone // Fax
			Elements rootTelFaxElems = agencyCoordinate.select("a");
			
			for(Element rootTelFaxElem : rootTelFaxElems) {
				//Element label = rootTelFaxElem.selectFirst("span.CardAgencyFunc-label");
				String labelText = this.normalize(rootTelFaxElem.text().toLowerCase());
				if(labelText.contains("appeler")) {
					tel = rootTelFaxElem.attr("href");
				}else if(labelText.contains("fax")){
					fax = labelText;
				}
			}
			
			tel = tel.replaceAll("[^0-9]", "");
			System.out.println("tel : " + tel);
			
			fax = fax.replaceAll("[^0-9]", "");
			System.out.println("fax : " + fax);

			// retrieve agency schedule
			
			Element agencyScheduleElement = infosRoot.selectFirst("div > div.npc-sl-strct-horairesMap > div.npc-sl-strct-horaires > ul");
			
			Elements agencyScheduleElements = infosRoot.select("div > div.npc-sl-strct-horairesMap > div.npc-sl-strct-horaires > ul > li");
			
			String hh ="";
			for(Element shedule : agencyScheduleElements) {
				//String jour_name = shedule.select(".ficheAgence-jourNom").first()!= null ? shedule.select(".ficheAgence-jourNom").first().text() : "";
				
				Element jour_ouvert_name = shedule.select("span.ficheAgence-jourNom").first();
				String jour_name = jour_ouvert_name!= null ? jour_ouvert_name.text() : "";
				
				//Element horaires= jour_ouvert_elem.get(1);
				Elements horairesList = shedule.select("span.ficheAgence-jourHorairesOuverture > span");
				String h_str = "";
				for(Element h : horairesList) {
					if(h.hasClass("ficheAgence-sur-rendez-vous")) {
						h_str+=h.text()+"(Sur RDV) ";
					}else {
						h_str+=h.text()+" ";
					}
				}
				hh+=jour_name+" : "+h_str;
				
				//String jour_ouvert = shedule.select(".ficheAgence-jourHorairesOuverture").first()!= null ? shedule.select(".ficheAgence-jourHorairesOuverture").first().text() : "";
			}
			
			//System.out.println("hh : " + hh);
			Element vqh = infosRoot.selectFirst("div > div.npc-sl-strct-horairesMap > div.npc-sl-strct-horaires > div.npc-sl-strct-horaires--24h");
			if(agencyScheduleElement!= null) {
				HorairesHTM = agencyScheduleElement.outerHtml();
				HorairesHTM = HorairesHTM.replaceAll("\\s{1,}", " ").trim();
				HorairesHTM = this.normalize(HorairesHTM);
				
				//HorairesTXT = agencyScheduleElement.text().replaceAll("\\s{2,}", " ").trim();
				HorairesTXT = hh.replaceAll("\\s{2,}", " ").trim();
		
				HorairesTXT = this.normalize(HorairesTXT);
				
			}else if(vqh != null){
				HorairesHTM = vqh.outerHtml();
				HorairesHTM = HorairesHTM.replaceAll("\\s{1,}", " ").trim();
				HorairesHTM = this.normalize(HorairesHTM);
				
				//HorairesTXT = agencyScheduleElement.text().replaceAll("\\s{2,}", " ").trim();
				HorairesTXT = vqh.text().replaceAll("\\s{2,}", " ").trim();
		
				HorairesTXT = this.normalize(HorairesTXT);
				
			}
			
			//HorairesTXT = HorairesTXT.replaceAll("\\*", " Sur RDV");
			
			System.out.println("horaires : " + HorairesTXT);
		

			// retrieve services

			Element servicesElement = infosRoot.selectFirst("div > div.npc-sl-strct-services > div > div");
			
			Services = (servicesElement != null )?servicesElement.text().replaceAll("\\s{2,}", " ").trim() : "";

			Services = this.normalize(Services);
			
			System.out.println("Services : " + Services);
			
			// retrieve coord
			
			Element geoCoord = infosRoot.selectFirst("div > div.npc-sl-strct-horairesMap > div.npc-sl-strct-mapInfosLegales > div.npc-sl-strct-map.Card.js-CardAgencyMap");
			
			if (geoCoord != null) {
				String strGeoCoord = geoCoord.attr("data-value");
				JSONObject json = new JSONObject(strGeoCoord);
				String lat = "", longt = "";
				lat = json.getString("latitude");
				longt = json.getString("longitude");
				coord = "Lat:" + " " + lat + " " + "Long:" + " " + longt;
				System.out.println("Lat :" + " " + lat + " " + "Long :" + " " + longt);
			}

			// body > main > div > div > div > div:nth-child(5) > div > div > div >
			// div:nth-child(2) > p:nth-child(1)
			Element relaisStatusElement = doc.selectFirst(
					"body > main > div > div > div > div:nth-child(5) > div > div > div > div:nth-child(2) > p:nth-child(1)");
			String tmp = (relaisStatusElement != null)
					? this.normalize(relaisStatusElement.text().toLowerCase())
					: "";
			System.out.println("tmp : " + tmp);

			Element relaisStatusElement2 = doc.selectFirst(
					"body > main > div > div > div > div:nth-child(6) > div > div > div > div:nth-child(2) > ul");
			String tmp2 = (relaisStatusElement2 != null) ? this.normalize(
					relaisStatusElement2.text().toLowerCase().replaceAll("\\*", "Sur RDV"))
					: "";
			//System.out.println("tmp2 " + tmp2);

			if (nom_agence.toLowerCase().contains("relais ca")) {
				relais = 1;
			} else if ((tmp.contains("relais ca")) || (tmp2.contains("relais ca"))) {
				relais = 1;
			} else {
				relais = 0;
			}
			
			numVille++;
			
			System.out.println("relais : " + relais);
			if(nom_agence != "") {
				
				this.saveItem(cp, String.valueOf(NbVilles), String.valueOf(numVille), villeSoumise,
						String.valueOf(NbAgences), uriVille, nom_agence, adresse, tel, fax,
						HorairesHTM, HorairesTXT, Services, coord, agences_HTM,
						String.valueOf(relais));
			}
			
			System.out.println("\n---------------------\n");
		}
		
	}

	public void tearDown() {
		htmlUnitDriver.quit();

	}

}
