package com.app.model;

import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="banque_palatine_dep_outre_mer", schema="scrap")
public class Palatine_Outre_Mer {

    @Id
    @SequenceGenerator(name="scrap.banque_palatine_dep_outre_mer_id_seq",
                     sequenceName ="scrap.banque_palatine_dep_outre_mer_id_seq",
                    allocationSize=1)
    @GeneratedValue(strategy=GenerationType.SEQUENCE,
                   generator="scrap.banque_palatine_dep_outre_mer_id_seq")

    @Column(name="id",updatable=false)
    private int id;

    @Column(name="dep_soumis",columnDefinition="varchar")
    private String depSoumis;
    
    @Column(name="nombre_agences",columnDefinition="varchar")
    private String nbAgences;   
    
    @Column(name="nom_agence",columnDefinition="varchar")
    private String nom_agence; 
    
    @Column(name="adresse",columnDefinition="varchar")
    private String adresse; 
    
    @Column(name="telephone",columnDefinition="varchar")
    private String tel;
    
    @Column(name="fax",columnDefinition="varchar")
    private String fax; 
    
    @Column(name="horaires_htm",columnDefinition="varchar")
    private String HorairesHTM;
    
    @Column(name="horaire_txt",columnDefinition="varchar")
    private String HorairesTXT;
    
    @Column(name="agence_htm", columnDefinition="varchar")
    private String agences_HTM;
    
    @Column(name="annee_du_scrap" ,columnDefinition="varchar(4) DEFAULT date_part('year',CURRENT_DATE)")
    private String anneeScrap;
    
    @Column(name="agence_id", columnDefinition="varchar")
    private String agences_id;
    
    public Palatine_Outre_Mer() {
    	LocalDate ld = new Date().toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
		anneeScrap = String.valueOf(ld.getYear() + 1);
	}

	public int getId() {
		return id;
	}

	public Palatine_Outre_Mer setId(int id) {
		this.id = id;
		return this;
	}

	public String getDepSoumis() {
		return depSoumis;
	}

	public Palatine_Outre_Mer setDepSoumis(String depSoumis) {
		this.depSoumis = depSoumis;
		return this;
		
	}

	public String getNbAgences() {
		return nbAgences;
	}

	public Palatine_Outre_Mer setNbAgences(String nbAgences) {
		this.nbAgences = nbAgences;
		return this;
	}

	public String getNom_agence() {
		return nom_agence;
	}

	public Palatine_Outre_Mer setNom_agence(String nom_agence) {
		this.nom_agence = nom_agence;
		return this;
	}

	public String getAdresse() {
		return adresse;
	}

	public Palatine_Outre_Mer setAdresse(String adresse) {
		this.adresse = adresse;
		return this;
	}

	public String getTel() {
		return tel;
	}

	public Palatine_Outre_Mer setTel(String tel) {
		this.tel = tel;
		return this;
	}

	public String getFax() {
		return fax;
	}

	public Palatine_Outre_Mer setFax(String fax) {
		this.fax = fax;
		return this;
	}

	public String getHorairesHTM() {
		return HorairesHTM;
	}

	public Palatine_Outre_Mer setHorairesHTM(String horairesHTM) {
		HorairesHTM = horairesHTM;
		return this;
	}

	public String getHorairesTXT() {
		return HorairesTXT;
	}

	public Palatine_Outre_Mer setHorairesTXT(String horairesTXT) {
		HorairesTXT = horairesTXT;
		return this;
	}

	public String getAgences_HTM() {
		return agences_HTM;
	}

	public Palatine_Outre_Mer setAgences_HTM(String agences_HTM) {
		this.agences_HTM = agences_HTM;
		return this;
	}

	public String getAnneeScrap() {
		return anneeScrap;
	}

	public Palatine_Outre_Mer setAnneeScrap(String anneeScrap) {
		this.anneeScrap = anneeScrap;
		return this;
	}

	public String getAgences_id() {
		return agences_id;
	}

	public Palatine_Outre_Mer setAgences_id(String agences_id) {
		this.agences_id = agences_id;
		return this;
	}
}
