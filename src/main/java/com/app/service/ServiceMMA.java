package com.app.service;

import java.io.BufferedInputStream;

import java.io.File;
import java.io.FileInputStream;

import java.net.SocketTimeoutException;

import java.text.Normalizer;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Scanner;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import org.openqa.selenium.chrome.ChromeDriver;

import org.openqa.selenium.htmlunit.HtmlUnitDriver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.app.model.MMA;

import com.app.scrap.RepMMA;

import com.app.utils.Parametres;

import fa.log.LogApp.LogScrap;
import fa.log.config.LogConst;

@Service
public class ServiceMMA extends Scraper {

	@Autowired
	private RepMMA rep;

	String nomSITE = "MMA";
	String ficval = "lien_mma";
	String dossierRESU = Parametres.REPERTOIRE_RESULT + "\\ASSU\\" + nomSITE;;
	String url_accueil = "http://agence.mma.fr";

	String depSoumise;

	int NbVillesREG;
	int numville_REG;
	int NbAgences_ville;

	int nbRecuperes;
	int num_ligneficval;
	int parcoursficval;

	HtmlUnitDriver htmlUnitDriver;
	ChromeDriver chDrive;

	/*
	 * public void setData(String depSoumise, String nbAgencesVille, String
	 * nomAgence, String adresse, String tel, String nomsAgents, String orias,
	 * String horaires, String agenceHtm) { MMA mma = new MMA();
	 * mma.setDepSoumise(depSoumise).setNbAgencesVille(nbAgencesVille).setNomAgence(
	 * nomAgence).setAdresse(adresse)
	 * .setTel(tel).setNomsAgents(nomsAgents).setOrias(orias).setHoraires(horaires).
	 * setAgenceHtm(agenceHtm); this.saveData(mma);
	 * 
	 * }
	 */

	public void setUp() throws Exception {
		htmlUnitDriver = (HtmlUnitDriver) this.setupHU();

	}

	@Override
	public void saveItem(String... args) {
		MMA mma = new MMA();
		mma.setDepSoumise(args[0]).setNbAgencesVille(args[1]).setNomAgence(args[2]).setAdresse(args[3]).setTel(args[4])
				.setNomsAgents(args[5]).setOrias(args[6]).setHoraires(args[7]).setAgenceHtm(args[8]);
		rep.save(mma);

	}

	public void scrap() throws Exception {

		Scanner sc = new Scanner(new BufferedInputStream(
				new FileInputStream(new File(Parametres.REPERTOIRE_DATA_SOURCES + ficval + ".txt"))));

		LogScrap log = new LogScrap("AssuranceMMA.txt", LogConst.TYPE_LOG_FILE);
		num_ligneficval = ((new File(LogConst.STORE_LOG_PATH + LogConst.PREFIX_LOG_FILE + "_AssuranceMMA.txt")).exists())
				? Integer.parseInt(log.readLog().getLastLineProcessed().split("\\t")[4])
				: 1;

		parcoursficval = 1;

		List<String> agenciesLinks = new ArrayList<>();

		while (sc.hasNextLine()) {

			String lines[] = sc.nextLine().split("\\t");

			if (parcoursficval > num_ligneficval) {
				depSoumise = lines[0];
				String uri = lines[1];
				System.out.println(uri);
				/*
				 * URL url = new URL(uri); HttpsURLConnection con = (HttpsURLConnection)
				 * url.openConnection(); con.setRequestMethod("POST");
				 * con.setConnectTimeout(600000); con.setReadTimeout(600000); BufferedReader br
				 * = new BufferedReader(new InputStreamReader(con.getInputStream(), "UTF-8"));
				 * String pageSource = ""; StringBuilder strB = new StringBuilder(); while
				 * ((pageSource = br.readLine()) != null) { strB.append(pageSource); }
				 * System.out.println(pageSource); br.close();
				 */
				htmlUnitDriver.get(uri);
				String pageSource = htmlUnitDriver.getPageSource();
				
				//System.out.println("pageSource : " + pageSource);
				Document doc = Jsoup.parse(pageSource);
				
				Element blockResult = doc.selectFirst("#page-content > div > div > div > div:nth-child(2)");
				
				//Elements listRes = doc.select("#page-content > div > div > div > div:nth-child(2) > div > div > ul > li");
				
				//System.out.println("listRes length : " + listRes.size());
				
				if (!agenciesLinks.isEmpty())
					agenciesLinks.clear();

				if (blockResult != null) {
					if (blockResult.text().contains("Les agences MMA dans les grandes villes")) {
						Elements agenciesLinksElement = blockResult.select("div > div > ul > li > a");
						if (!agenciesLinksElement.isEmpty())
							for (Element agencyLinkElement : agenciesLinksElement) {
								//System.out.println(agencyLinkElement.attr("href"));
								agenciesLinks.add(agencyLinkElement.attr("href"));
							}
					}

				}

				blockResult = doc.selectFirst("#page-content > div > div > div > div:nth-child(3)");

				if (blockResult != null) {
					if (blockResult.text().contains("Les agences MMA dans les autres villes")) {
						Elements agenciesLinksElement = blockResult.select("div > div > ul > li > a");
						if (!agenciesLinksElement.isEmpty())
							for (Element agencyLinkElement : agenciesLinksElement) {
								//System.out.println(agencyLinkElement.attr("href"));
								agenciesLinks.add(agencyLinkElement.attr("href"));
							}
					}

				}

				System.out.println("agenciesLinks : " + agenciesLinks);
				
				NbAgences_ville = agenciesLinks.size();

				for (String agencyLink : agenciesLinks) {

					String agenceHtm = "";
					System.out.println(agencyLink);
					htmlUnitDriver.get(agencyLink);
					
					Thread.sleep(2000);
					
					pageSource = htmlUnitDriver.getPageSource();

					doc = Jsoup.parse(pageSource);
					
				
					Element root = doc.selectFirst("#page-content > div:nth-child(3) > div > div");
					
					Element root2 = doc.selectFirst("#page-content > div > div > div > div:nth-child(1)");

					
					if (root != null) {
						
						agenceHtm = root.outerHtml().replaceAll("\\s{1,}", "");
						
						Element agencyCoordinate = root.selectFirst("div.insurance-agency > div.agency-card");
					

						if (agencyCoordinate != null) {
							
							this.retrieveInfo(agenceHtm, agencyCoordinate, root, doc);
							
						} else {
							
							//System.out.println("One est ici.......");
							
							List<String> secondLinks = new ArrayList<String>();
							Elements agenciesSecondLinks = doc.select(
									"#page-content > div > div > div > div:nth-child(1) > div > div > div > ul > li > a");
							if (!agenciesSecondLinks.isEmpty()) {
								for (Element agencySecondLink : agenciesSecondLinks) {
									secondLinks.add(agencySecondLink.attr("href"));
								}
							}
							for (String secondLink : secondLinks) {
								System.out.println("https://agence.mma.fr" + secondLink);
								htmlUnitDriver.get("https://agence.mma.fr" + secondLink);
								pageSource = htmlUnitDriver.getPageSource();
                                doc = Jsoup.parse(pageSource);
                                root = doc.selectFirst("#page-content > div:nth-child(3) > div > div ");
                                agenceHtm = root.outerHtml().replaceAll("\\s{1,}", "");
								agencyCoordinate = root.selectFirst("div.insurance-agency > div.agency-card");

								if (agencyCoordinate != null) {
									this.retrieveInfo(agenceHtm, agencyCoordinate, root, doc);
								}

							}
						}

					}
				}
				log.writeLog(new Date(), depSoumise, "no_error", 0, parcoursficval, 0);
			}

			parcoursficval++;
		}
		sc.close();

	}

	public void retrieveInfo(String agenceHtm, Element agencyCoordinate, Element root, Document doc) {
		String nomAgence = "";
		String adresse = "";
		String tel = "";
		String nomsAgents = "";
		String orias = "";
		String horaires = "";
		Element agencyNameElement = agencyCoordinate.selectFirst("h2");
		nomAgence = (agencyNameElement != null) ? this.normalize(agencyNameElement.text()) : "";
		System.out.println("nom agence : " + nomAgence);

		Element agencyAddressElement = agencyCoordinate.selectFirst("p.address");
		adresse = (agencyAddressElement != null) ? this.normalize(agencyAddressElement.text()) : "";
		System.out.println("nom adresse : " + adresse);

		Elements agenciesOpeningsHours = agencyCoordinate.select("div.info > div > ul > li");
		if (!agenciesOpeningsHours.isEmpty()) {

			for (Element agencyOpeningHour : agenciesOpeningsHours) {
				horaires += this.normalize(agencyOpeningHour.text()) + " ";
			}

		}
		System.out.println("horaires : " + horaires);

		// #page-content > div:nth-child(4) > div > div > div.insurance-agency >
		// div.agency-card >
		Element agencyPhoneElement = agencyCoordinate.selectFirst("div > ul > li:nth-child(1) > div");
		tel = (agencyPhoneElement != null) ? agencyPhoneElement.attr("data-phone") : "";
		System.out.println("telephone : " + tel);

		// #page-content > div:nth-child(4) > div > div >
		
		//#page-content > div:nth-child(3) > div > div > div.block.block-text > div > div > div > div
		
		
		Elements agenciesAgentsElement = root.select(" div.block.block-text > div > div > div > div > div");

		if (!agenciesAgentsElement.isEmpty()) {
			for (Element agencyAgentElement : agenciesAgentsElement) {
				if (Normalizer.normalize(agencyAgentElement.text(), Normalizer.Form.NFD).replaceAll("[^\\p{ASCII}]", "")
						.toLowerCase().contains("agent general")) {
					Element agentNameElement = agencyAgentElement.selectFirst("div > h3");
					nomsAgents = agentNameElement != null ? this.normalize(nomsAgents + agentNameElement.text()) + "&" : "";
				}
			}
		}
		System.out.println("agent name : " + nomsAgents);
		
		Element oriasElement = doc.selectFirst(
				"#page-content > div:nth-child(4) > div > div.sm-flex-row > address > span.orias.seo-detail");

		orias = (oriasElement != null) ? oriasElement.text().replaceAll("[^0-9-]+", "") : "";
		System.out.println("orias : " + orias);
		saveItem(depSoumise, String.valueOf(NbAgences_ville), nomAgence, adresse, tel, nomsAgents, orias, horaires,
				agenceHtm);
		System.out.println("-----------------------\n");
	}

	public void tearDown() {
		htmlUnitDriver.quit();

	}

}
