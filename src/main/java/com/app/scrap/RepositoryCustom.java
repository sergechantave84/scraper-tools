package com.app.scrap;

import java.util.List;

public interface RepositoryCustom<E> {

	List<E> getFirstRow(int limits, Class<E> type);
	List<E> getLastRow(int limits, Class<E> type);
	
}
