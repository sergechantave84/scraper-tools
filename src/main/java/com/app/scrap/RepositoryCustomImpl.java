package com.app.scrap;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.app.model.Allianz;

@Repository
public class RepositoryCustomImpl<E> implements RepositoryCustom<E>{

	private CriteriaBuilder cb;
	private CriteriaQuery<E> cq;
	private Root<E> root;
	
	
	@Autowired
	EntityManager em;
	
	@Override
	public List<E> getFirstRow(int limits,Class<E> type) {
	
		this.criteriaBuild(type);
		this.cq.select(this.root).orderBy(this.cb.asc(this.root.get("id")));
		TypedQuery<E> query= em.createQuery(this.cq).
				                setMaxResults(limits);
		return query.getResultList();
	}

	@Override
	public List<E> getLastRow(int limits, Class<E> type) {
		this.criteriaBuild(type);
		this.cq.select(this.root).orderBy(this.cb.desc(this.root.get("id")));
		TypedQuery<E> query= em.createQuery(this.cq).
				                setMaxResults(limits);
		return query.getResultList();
		
	}

	public void criteriaBuild(Class<E> type) {
		
		this.cb=em.getCriteriaBuilder();
		this.cq= cb.createQuery(type);
		this.root= cq.from(type);
	}

}
