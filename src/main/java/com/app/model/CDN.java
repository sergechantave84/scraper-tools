package com.app.model;

import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="banque_cdn", schema="scrap")
public class CDN {

	@Id
	@SequenceGenerator(name="scrap.banque_cdn_id_seq",
	                    sequenceName ="scrap.banque_cdn_id_seq",
	                     allocationSize=1)
    @GeneratedValue( strategy=GenerationType.SEQUENCE,
                     generator="scrap.banque_cdn_id_seq")
	  
	@Column(name="id",updatable=false)
	private int id;
	
	@Column(name="cpsoumise", columnDefinition="varchar")
	private String cpSoumise;
	
	@Column(name="nbagences", columnDefinition="varchar")
	private String nbAgences;
	
	@Column(name="enseigne", columnDefinition="varchar")
	private String enseigne;
	
	@Column(name="nomweb", columnDefinition="varchar")
	private String nomWeb;
	
	@Column(name="adresse", columnDefinition="varchar")
	private String adresse;
	
	@Column(name="tel", columnDefinition="varchar")
	private String tel;
	
	@Column(name="fax", columnDefinition="varchar")
	private String fax;
	
	@Column(name="code_guichet", columnDefinition="varchar")
	private String codeGuichet;
	
	@Column(name="horaireshtm", columnDefinition="varchar")
	private String horaires;
	
	@Column(name="dab", columnDefinition="varchar")
	private String dab;
	
	@Column(name="services", columnDefinition="varchar")
	private String services;
	
	@Column(name="agencehtm", columnDefinition="varchar")
	private String agenceHtm;
	
	
	
	@Column(name="annee_du_scrap" ,columnDefinition="varchar(4) DEFAULT date_part('year',CURRENT_DATE)")
	private String anneeScrap;

     public CDN() {
    	 LocalDate ld = new Date().toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
 		anneeScrap = String.valueOf(ld.getYear() + 1);
	}
     
	public int getId() {
		return id;
	}

	public CDN setId(int id) {
		this.id = id;
		return this;
	}

	public String getCpSoumise() {
		return cpSoumise;
	}

	public CDN setCpSoumise(String cpSoumise) {
		this.cpSoumise = cpSoumise;
		return this;
	}

	public String getNbAgences() {
		return nbAgences;
	}

	public CDN setNbAgences(String nbAgences) {
		this.nbAgences = nbAgences;
		return this;
	}

	public String getEnseigne() {
		return enseigne;
	}

	public CDN setEnseigne(String enseigne) {
		this.enseigne = enseigne;
		return this;
	}

	public String getNomWeb() {
		return nomWeb;
	}

	public CDN setNomWeb(String nomWeb) {
		this.nomWeb = nomWeb;
		return this;
	}

	public String getAdresse() {
		return adresse;
	}

	public CDN setAdresse(String adresse) {
		this.adresse = adresse;
		return this;
	}

	public String getTel() {
		return tel;
	}

	public CDN setTel(String tel) {
		this.tel = tel;
		return this;
	}

	public String getFax() {
		return fax;
	}

	public CDN setFax(String fax) {
		this.fax = fax;
		return this;
	}

	public String getCodeGuichet() {
		return codeGuichet;
	}

	public CDN setCodeGuichet(String codeGuichet) {
		this.codeGuichet = codeGuichet;
		return this;
	}

	public String getDab() {
		return dab;
	}

	public CDN setDab(String dab) {
		this.dab = dab;
		return this;
	}

	public String getServices() {
		return services;
	}

	public CDN setServices(String services) {
		this.services = services;
		return this;
	}

	public String getAgenceHtm() {
		return agenceHtm;
	}

	public CDN setAgenceHtm(String agenceHtm) {
		this.agenceHtm = agenceHtm;
		return this;
	}

	public String getAnneeScrap() {
		return anneeScrap;
	}

	public CDN setAnneeScrap(String anneeScrap) {
		this.anneeScrap = anneeScrap;
		return this;
	}

	public String getHoraires() {
		return horaires;
	}

	public CDN setHoraires(String horaires) {
		this.horaires = horaires;
		return this;
	}

	
}
