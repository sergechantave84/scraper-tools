package com.app.service;

import java.io.BufferedInputStream;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;

import java.net.SocketTimeoutException;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Scanner;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.openqa.selenium.NoAlertPresentException;

import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.app.model.Groupama;

import com.app.scrap.RepGroupama;

import com.app.utils.Parametres;
import com.app.utils.Utilitaires;

import fa.browserUtils.FactoryBrowser;
import fa.browserUtils.HtmlUnitBrowser;
import fa.log.LogApp.LogScrap;
import fa.log.config.LogConst;

@Service
public class ServiceGroupama extends Utilitaires {

	@Autowired
	private RepGroupama rep;

	private List<Groupama> groupamaList = new ArrayList<>();

	private String nomSITE = "GROUPAMA";
	private String ficval = "lien_groupama";
	private String dossierRESU = Parametres.REPERTOIRE_RESULT + "\\ASSU\\" + nomSITE;;
	private String url_accueil = "https://agences.groupama.fr/assurance";
	private String fichier_valeurs;
	private String regionSoumise;

	private int NbAgences;

	private int num_ligneficval;
	private int parcoursficval;

	private HtmlUnitDriver htmlUnitDriver;
	private BufferedWriter sortie;

	public Groupama saveData(Groupama t) {

		return rep.save(t);

	}

	public List<Groupama> saveDataAll(List<Groupama> t) {

		return rep.saveAll(t);
	}

	public List<Groupama> getElmnt() {

		return rep.findAll();
	}

	public Groupama getById(int id) {

		return (Groupama) rep.findById(id).orElse(null);
	}

	public String deleteProduct(int id) {
		try {
			rep.deleteById(id);

		} catch (Exception e) {
			e.printStackTrace();
		}

		return "removed";
	}

	public Groupama updateProduct(Groupama t) {
		// GroupamaODO Auto-generated method stub
		return null;
	}

	public Groupama setData(String regionSoumise, String nbAgences, String nbAgencesGroupamarouvees, String lienAgence,
			String nomAgence, String adresse, String tel, String fax, String horaires, String agenceHtm) {
		Groupama groupama = new Groupama();
		groupama.setRegionSoumise(regionSoumise).setNbAgences(nbAgences).setNbAgencesTrouvees(nbAgencesGroupamarouvees)
				.setLienAgence(lienAgence).setNomAgence(nomAgence).setAdresse(adresse).setTel(tel).setFax(fax)
				.setHoraires(horaires).setAgenceHtm(agenceHtm);
		this.saveData(groupama);
		return groupama;
	}

	public List<Groupama> showFirstRows() {
		List<Groupama> a = rep.getFirstRow(100, Groupama.class);
		return a;
	}

	public List<Groupama> showLastRows() {
		List<Groupama> a = rep.getLastRow(100, Groupama.class);
		return a;
	}

	public void setUp() throws Exception {
		FactoryBrowser fact = new FactoryBrowser();
		HtmlUnitBrowser hB = (HtmlUnitBrowser) fact.create("htmlUnit");
		htmlUnitDriver = hB.setOptions(false, false, false, 60000).initBrowserDriver();

	}

	public List<Groupama> open_GROUPAMA_byselenium() throws Exception {

		fichier_valeurs = Parametres.REPERTOIRE_DATA_SOURCES + ficval + ".txt";
		String html = "";
		Document doc = null;
		List<String> linkList = new ArrayList<>();
		Scanner sc = new Scanner(new BufferedInputStream(new FileInputStream(new File(fichier_valeurs))));

		if (!new File(dossierRESU).exists())
			new File(dossierRESU).mkdirs();

		log = new File(dossierRESU + "\\" + "LOG_" + nomSITE + ".csv");
		LogScrap log = new LogScrap("AssuranceGroupama.txt", LogConst.TYPE_LOG_FILE);
		num_ligneficval = ((new File(LogConst.STORE_LOG_PATH + LogConst.PREFIX_LOG_FILE + "_AssuranceGroupama.txt"))
				.exists()) ? Integer.parseInt(log.readLog().getLastLineProcessed().split("\\t")[4]) : 1;

		parcoursficval = 1;
		while (sc.hasNextLine()) {

			String lines[] = sc.nextLine().split("\\t");

			if (parcoursficval > num_ligneficval) {
				regionSoumise = lines[0];

				fichier = new File(dossierRESU + "\\" + "RESULTATS_" + nomSITE + "_" + regionSoumise + "_"
						+ System.currentTimeMillis() + ".txt");
				this.initialiser();

				String url = lines[2];
				System.out.println(url);
				try {
					htmlUnitDriver.get(url);
					System.out.println("on c'est connecté");
				} catch (Exception e) {
					if (e instanceof SocketTimeoutException) {
						this.tearDown();
						this.setUp();
						htmlUnitDriver.get(url_accueil + url);

					}
				}
				
				Thread.sleep(2000);
				
				html = htmlUnitDriver.getPageSource();
				doc = Jsoup.parse(html);
				Elements agenciesLinks = doc.select("#agencies > li > div > div.agencyinfos > div.agencylabel > a");
				if (!linkList.isEmpty())
					linkList.clear();
				if (!agenciesLinks.isEmpty()) {
					for (Element agencyLink : agenciesLinks) {
						linkList.add(agencyLink.attr("href"));

					}

				}
				System.out.println("++++++++++++++++++++++++++++++++++++");
				int counterAgencyVerified = 0;

				String agencyName = "", agencyAddress = "", agencySchedule = "", agencyPhone = "", agencyFax = "",
						agencyHtm = "";

				System.out.println("on finis de recupérer les liens maintenant lançon le scrap");
				NbAgences = linkList.size();
				counterAgencyVerified = NbAgences;
				for (String link : linkList) {

					System.out.println(link);
					url = "https://agences.groupama.fr" + link;
					try {
						htmlUnitDriver.get(url);
						System.out.println("on c'est connecté");
					} catch (Exception e) {
						if (e instanceof SocketTimeoutException) {
							this.tearDown();
							this.setUp();
							htmlUnitDriver.get(url_accueil + url);

						}
					}
					Thread.sleep(2000);

					html = htmlUnitDriver.getPageSource();
					doc = Jsoup.parse(html);
					Element bigBlockElementForAgenciesInfo2 = doc.select("div[class=\"presentation-bloc blocinfo\"]")
							.first();
					if (bigBlockElementForAgenciesInfo2 != null) {
						agencySchedule = "";
						agencyHtm = "";
						agencyName = "";
						agencyAddress = "";
						agencyPhone = "";
						agencyFax = "";

						agencyHtm = bigBlockElementForAgenciesInfo2.outerHtml().replaceAll("\\s{1,}", " ").trim();
						agencyHtm = this.normalize(agencyHtm);
						Element agencyInfoBlock = bigBlockElementForAgenciesInfo2
								.select("div[class=\"agency-bloc\"]>" + "div[class=\"info\"]").first();
						if (agencyInfoBlock != null) {
							// fetch agency name
							Element agencyNameElement = agencyInfoBlock.select("h1[class=\"name\"]").first();
							agencyName = (agencyNameElement != null) ? this.normalize(agencyNameElement.text().trim())
									: "";
							System.out.println("le nom de l'agence est: " + agencyName);

							// fetch agency address
							Element agencyAddressElement = agencyInfoBlock.select("div[class=\"agencyaddress\"]")
									.first();
							agencyAddress = (agencyAddressElement != null)
									? this.normalize(agencyAddressElement.text().trim())
									: "";
							System.out.println("l'adresse de l'agence est: " + agencyAddress);

							// fetch agency phone number
							Element agencyPhoneElement = agencyInfoBlock.select("a[class=\"agencytel\"]").first();
							agencyPhone = (agencyPhoneElement != null)
									? agencyPhoneElement.attr("href").replace("tel:", "")
									: "";
							System.out.println("le telephone de l'agence: " + agencyPhone);

							// fetch agency fax number
							Element agencyFaxElement = agencyInfoBlock.select("div[class=\"agencyfax\"]").first();
							agencyFax = (agencyFaxElement != null) ? agencyFaxElement.text().replaceAll("Fax :", "")
									: "";
							System.out.println("le fax de l'agence est: " + agencyFax);
						}
						Element agencyScheduleElement = bigBlockElementForAgenciesInfo2
								.select("div[class=\"horaires-bloc\"]").first();
						if (agencyScheduleElement != null) {
							// fetch agency schedule;
							Elements agencyScheduleList = agencyScheduleElement
									.select("div[class=\"agencyschedule\"] >div[class*=\"day\"]");
							if (agencyScheduleList != null) {
								for (Element tmp : agencyScheduleList) {
									agencySchedule += tmp.text() + " ";
								}
								
							}
						}
						
						agencySchedule = this.normalize(agencySchedule);
						
						System.out.println("l'horaire de l'agence est: " + agencySchedule);
						
						
						this.setData(regionSoumise, String.valueOf(NbAgences), String.valueOf(counterAgencyVerified), url,
						agencyName, agencyAddress, agencyPhone, agencyFax, agencySchedule, agencyHtm);
						
						System.out.println("--------------------------------------\n");

					}
					

				}

				log.writeLog(new Date(), regionSoumise, "no_error", NbAgences, parcoursficval, 0);

			}
			parcoursficval++;

		}
		sc.close();
		return groupamaList;

	}

	public void enregistrer(int NbAgences_verif, String lien_agence, String nom_agence, String adresse, String tel,
			String fax, String horaires, String agence) {

//    	this.fichier = new File(Parametres.REPERTOIRE + "RESULTATS_BNP.");
		try {

			sortie = new BufferedWriter(new FileWriter(fichier, true));
			sortie.write(regionSoumise + "\t" + NbAgences + "\t" + NbAgences_verif + "\t" + lien_agence + "\t"
					+ nom_agence + "\t" + adresse + "\t" + tel + "\t" + fax + "\t" + horaires + "\t" + agence + "\r\n");

			sortie.close();
		} catch (IOException e) {

			e.printStackTrace();
		}

	}

	/**
	 * La m�thode <code>initialiser()</code> permet d'ajouter la ligne d'ent�te dans
	 * le fichier r�sultat.
	 */

	public void initialiser() {

		try {
			// Attention pas d'accent dans les noms de colonnes !
			sortie = new BufferedWriter(new FileWriter(fichier, true));
			sortie.write("RegionSoumise" + "\t" + "NbAgences" + "\t" + "NbAgences_trouvees" + "\t"

					+ "LIEN_AGENCE" + "\t"

					+ "NOM_AGENCE" + "\t" + "ADRESSE" + "\t" + "TEL" + "\t" + "FAX" + "\t" + "HORAIRES" + "\t"
					+ "AGENCEHTM" + "\r\n");
			sortie.close();

		} catch (IOException e) {

			e.printStackTrace();
		}
	}

	public void tearDown() {
		htmlUnitDriver.quit();

	}

	public static boolean isAlertPresent(FirefoxDriver wd) {
		try {
			wd.switchTo().alert();
			return true;
		} catch (NoAlertPresentException e) {
			return false;
		}
	}

}
