package com.app.scrap;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.app.model.Groupama;
import com.app.model.Groupama_DOM;

@Repository
public interface RepGroupamaDom extends JpaRepository<Groupama_DOM,Integer>,RepositoryCustom<Groupama_DOM>  {

}
