package com.app.model;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;

@MappedSuperclass
public class PJAssurance {

	@Column(name="denomination",columnDefinition = "varchar")
	private String denomination;
	
	@Column(name="adresse",columnDefinition = "varchar")
	private String adresse;
	
	
	@Column(name="orias",columnDefinition = "varchar")
	private String orias;
	
	@Column(name="prestation",columnDefinition = "varchar")
	private String prestation;
	
	@Column(name="clientele",columnDefinition = "varchar")
	private String clientele;
	
	@Column(name="tel",columnDefinition = "varchar")
	private String tel;
	
	@Column(name="siren", columnDefinition = "varchar")
	private String siren;
	
	@Column(name="siret", columnDefinition = "varchar")
	private String siret;
	
	@Column(name="activite", columnDefinition = "varchar")
	private String activite;
	
	@Column(name="forme_juridique", columnDefinition = "varchar")
	private String formjuridique;
	
	@Column(name="code_NAF", columnDefinition = "varchar")
	private String codenaf;
	
	@Column(name="marque", columnDefinition = "varchar")
	private String marque;
	
	
	public String getFormjuridique() {
		return formjuridique;
	}

	public PJAssurance setFormjuridique(String formjuridique) {
		this.formjuridique = formjuridique;
		return this;
	}

	public String getCodenaf() {
		return codenaf;
	}

	public PJAssurance setCodenaf(String codenaf) {
		this.codenaf = codenaf;
		return this;
	}

	public String getMarque() {
		return marque;
	}

	public PJAssurance setMarque(String marque) {
		this.marque = marque;
		return this;
	}

	public String getDenomination() {
		return denomination;
	}

	public PJAssurance setDenomination(String denomination) {
		this.denomination = denomination;
		return this;
	}

	public String getAdresse() {
		return adresse;
	}

	public PJAssurance setAdresse(String adresse) {
		this.adresse = adresse;
		return this;
	}

	public String getOrias() {
		return orias;
	}

	public PJAssurance setOrias(String orias) {
		this.orias = orias;
		return this;
	}

	public String getPrestation() {
		return prestation;
	}

	public PJAssurance setPrestation(String prestation) {
		this.prestation = prestation;
		return this;
	}

	public String getClientele() {
		return clientele;
	}

	public PJAssurance setClientele(String clientele) {
		this.clientele = clientele;
		return this;
	}

	public String getTel() {
		return tel;
	}

	public PJAssurance setTel(String tel) {
		this.tel = tel;
		return this;
	}

	
	public String getSiren() {
		return siren;
	}
	public PJAssurance  setSiren(String siren) {
		this.siren = siren;
		return this;
	}
	public String getSiret() {
		return siret;
	}
	public PJAssurance  setSiret(String siret) {
		this.siret = siret;
		return this;
	}

	public String getActivite() {
		return activite;
	}

	public PJAssurance setActivite(String activite) {
		this.activite = activite;
		return this;
	}
}
