package com.app.scrap;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.app.model.April;
import com.app.model.April_DOM;

@Repository
public interface RepAprilDom extends JpaRepository<April_DOM,Integer>, RepositoryCustom<April_DOM> {

}
