package com.app.model;

import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="maaf", schema="scrap")
public class MAAF {

	
	@Id
	@SequenceGenerator(name="scrap.maaf_id_seq",
	                    sequenceName ="scrap.maaf_id_seq",
	                     allocationSize=1)
    @GeneratedValue( strategy=GenerationType.SEQUENCE,
                     generator="scrap.maaf_id_seq")
	  
	@Column(name="id",updatable=false)
	private int id;
	
	@Column(name="depsoumis", columnDefinition="varchar")
	private String depSoumis;
	
	@Column(name="nbvillesdep", columnDefinition="varchar")
	private String nbVilleDep;
	
	@Column(name="ville_soumise", columnDefinition="varchar")
	private String villeSoumise;
	
	@Column(name="nom_agence", columnDefinition="varchar")
	private String nomAgence;
	
	@Column(name="adresse", columnDefinition="varchar")
	private String adresse;
	
	@Column(name="tel", columnDefinition="varchar")
	private String tel;
	
	@Column(name="fax", columnDefinition="varchar")
	private String fax;
	
	@Column(name="horaires", columnDefinition="varchar")
	private String horaires;
	
	@Column(name="agencehtm", columnDefinition="varchar")
	private String agenceHtm;
	
	@Column(name="annee_du_scrap" ,columnDefinition="varchar(4) DEFAULT date_part('year',CURRENT_DATE)")
	private String anneeScrap;

	public MAAF() {
		LocalDate ld = new Date().toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
		anneeScrap = String.valueOf(ld.getYear() + 1);
	}
	
	public int getId() {
		return id;
	}

	public MAAF setId(int id) {
		this.id = id;
		return this;
	}

	public String getDepSoumis() {
		return depSoumis;
	}

	public MAAF setDepSoumis(String depSoumis) {
		this.depSoumis = depSoumis;
		return this;
	}

	public String getNbVilleDep() {
		return nbVilleDep;
	}

	public MAAF setNbVilleDep(String nbVilleDep) {
		this.nbVilleDep = nbVilleDep;
		return this;
	}

	public String getVilleSoumise() {
		return villeSoumise;
	}

	public MAAF setVilleSoumise(String villeSoumise) {
		this.villeSoumise = villeSoumise;
		return this;
	}

	public String getNomAgence() {
		return nomAgence;
	}

	public MAAF setNomAgence(String nomAgence) {
		this.nomAgence = nomAgence;
		return this;
	}

	public String getAdresse() {
		return adresse;
	}

	public MAAF setAdresse(String adresse) {
		this.adresse = adresse;
		return this;
	}

	public String getTel() {
		return tel;
	}

	public MAAF setTel(String tel) {
		this.tel = tel;
		return this;
	}

	public String getFax() {
		return fax;
	}

	public MAAF setFax(String fax) {
		this.fax = fax;
		return this;
	}

	public String getHoraires() {
		return horaires;
	}

	public MAAF setHoraires(String horaires) {
		this.horaires = horaires;
		return this;
	}

	public String getAgenceHtm() {
		return agenceHtm;
	}

	public MAAF setAgenceHtm(String agenceHtm) {
		this.agenceHtm = agenceHtm;
		return this;
	}

	public String getAnneeScrap() {
		return anneeScrap;
	}

	public MAAF setAnneeScrap(String anneeScrap) {
		this.anneeScrap = anneeScrap;
		return this;
	}
}
