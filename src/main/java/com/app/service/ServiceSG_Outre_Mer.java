package com.app.service;

import java.io.BufferedInputStream;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.net.SocketTimeoutException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Scanner;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import org.openqa.selenium.htmlunit.HtmlUnitDriver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.app.model.SG_Outre_Mer;

import com.app.scrap.RepSG_Outre_Mer;
import com.app.utils.BrowserUtils;
import com.app.utils.Parametres;
import com.app.utils.Utilitaires;

@Service
public class ServiceSG_Outre_Mer extends Utilitaires {

	String driverPath = "C:\\ChromeDriver\\";
	String nomSITE = "SG_antilles_guyane";
	String ficval = "sg_antille";

	String dossierRESU = Parametres.REPERTOIRE_RESULT + "\\BQ\\" + nomSITE;;
	String url_accueil = "https://www.banquedescaraibes.fr/";
	String fichier_valeurs;
	String CPSoumis;
	String ligne;
	// 97218 BASSE POINTE
	int num_ligne = 2;
	int NbAgences;
	int numreponse = 1;
	int DAB = 1;
	int HORS_SITE;
	int nbRecuperes;
	int num_ligneficval;
	int parcoursficval;
	int Resultats;

	HtmlUnitDriver htmlUnitDriver;
	BufferedWriter sortie;
	BufferedWriter sortie_log;

	Date Temps_Fin;

	@Autowired
	private RepSG_Outre_Mer rep;

	public SG_Outre_Mer saveData(SG_Outre_Mer t) {

		return rep.save(t);

	}

	public List<SG_Outre_Mer> saveDataAll(List<SG_Outre_Mer> t) {

		return rep.saveAll(t);

	}

	public List<SG_Outre_Mer> getElmnt() {

		return rep.findAll();
	}

	public SG_Outre_Mer getById(int id) {

		return rep.findById(id).orElse(null);
	}

	public String deleteProduct(int id) {
		try {
			rep.deleteById(id);

		} catch (Exception e) {
			e.printStackTrace();
		}

		return "removed";
	}

	public SG_Outre_Mer updateProduct(SG_Outre_Mer t) {
		// SG_Outre_MerODO Auto-generated method stub
		return null;
	}

	public SG_Outre_Mer setData(String lienSoumis, String cp, String nbAgences, String nomWeb, String adresse,
			String horairestxt, String telCom, String telPros, String agenceHtm) {
		SG_Outre_Mer sgAntille = new SG_Outre_Mer();
		sgAntille.setLienSoumis(lienSoumis).setCp(cp).setNbAgences(nbAgences).setNomWeb(nomWeb).setAdresse(adresse)
				.setTelCom(telCom).setTelPros(telPros).setHorairesTxt(horairestxt).setAgenceHtm(agenceHtm);
		this.saveData(sgAntille);
		return sgAntille;
	}

	public void setup() {
		htmlUnitDriver = BrowserUtils.HtmlUnitConstruct(true, false, false, 12000);
	}

	public void scrapSGA() throws FileNotFoundException {

		fichier_valeurs = Parametres.REPERTOIRE_DATA_SOURCES + ficval + ".txt";
		Scanner sc = new Scanner(new BufferedInputStream(new FileInputStream(new File(fichier_valeurs))));
		File f = new File(dossierRESU);
		if (!f.exists()) {
			f.mkdirs();
		}
		fichier = new File(dossierRESU + "\\" + "RESULTATS_" + nomSITE + "_" + System.currentTimeMillis() + ".txt");

		
		int i = 0;
		while (sc.hasNextLine()) {
			String line = sc.nextLine();
			if (i >= 1) {
				String[] strA = line.split("\t");

				String uri = "", lienSoumis, cp = "";

				lienSoumis = strA[0];
				cp = strA[1];
				this.NbAgences = Integer.parseInt(strA[3]);
				uri = url_accueil + lienSoumis;
				try {

					htmlUnitDriver.get(uri);
				} catch (Exception e) {
					if (e instanceof SocketTimeoutException)
						htmlUnitDriver.get(uri);
				}

				Document doc = null;
				String html = htmlUnitDriver.getPageSource();
				doc = Jsoup.parse(html);
				Element root = doc.selectFirst("#contenu");
				if (root != null) {
					String nom_agence = "", adresse = "", telCom = "", telPros = "", horairesTXT = "", DAB = "",
							agences_HTM = "";

					agences_HTM = this.normalize(root.outerHtml().replaceAll("\\s{1,}", " "));

					Element nameElement = root.selectFirst(
							"section.paragraph.paragraph--type--banner.paragraph--view-mode--default.c-heading > "
									+ "div.u-centered > " + "div.c-heading__block > " + "div > " + "h1");
					nom_agence = (nameElement != null) ? nameElement.text() : "";
					nom_agence = this.normalize(nom_agence);
					nom_agence = nom_agence.replaceAll("[^0-9a-zA-Z ]{1,}", "");

					System.out.println("le nom de l'agence " + nom_agence);

					Element adresseElement = root.selectFirst(
							"section.paragraph.paragraph--type--banner.paragraph--view-mode--default.c-heading > "
									+ "div.u-centered > " + "div.c-heading__block > " + "div > "
									+ "div.u-text-margin--none.c-heading__text > " + "div > " + "p");

					adresse = (adresseElement != null) ? this.normalize(adresseElement.text()) : "";
					System.out.println("l'adresse de l'agence est " + adresse);

					Element telComElement = root.selectFirst(
							"div.u-padding-vertical--lg.u-centered > " + "div > " + "div:nth-child(2) > " + "section > "
									+ "div.c-sticker-agence__content > " + "div.u-margin-bottom--sm > " + "span");
					telCom = (telComElement != null) ? telComElement.ownText().replaceAll("[^0-9]", "") : "";
					System.out.println("le numero commercial est " + telCom);

					Element telProsElement = root.selectFirst(
							"div.u-padding-vertical--lg.u-centered > " + "div > " + "div:nth-child(3) > " + "section > "
									+ "div.c-sticker-agence__content > " + "div.u-margin-bottom--sm > " + "span");
					telPros = (telProsElement != null) ? telProsElement.text().replaceAll("[^0-9]", "") : "";
					System.out.println("le numero pros est " + telPros);

					Elements horairesElements = root.select("div.u-padding-vertical--lg.u-centered > " + "div > "
							+ "div:nth-child(1) > " + "div > " + "div.c-sticker-agence__content > " + "div > "
							+ "div.c-sticker-agence__item.JS_add_active_on_current_day");
					if (horairesElements != null) {
						for (Element horairesElement : horairesElements) {
							horairesTXT += horairesElement.text() + " ";
						}
					}
					System.out.println("l'horaires est " + horairesTXT);
					
					this.setData(lienSoumis, cp, String.valueOf(NbAgences), nom_agence, adresse, horairesTXT, telCom,
							telPros, agences_HTM);
				}

			}
			i++;
		}
		sc.close();

	}

	public void tearDowns() {
		htmlUnitDriver.close();
	}

	public void enregistrer(String lienSoumis, String cp, String agences_HTM, String nom_agence, String adresse,
			String tel, String horairesTXT) {

//    	this.fichier = new File(Parametres.REPERTOIRE + "RESULTATS_BNP.");
		try {

			sortie = new BufferedWriter(new FileWriter(fichier, true));
			sortie.write(lienSoumis + "\t" + cp + "\t" + +NbAgences + "\t" + nom_agence + "\t" + adresse + "\t" + tel
					+ "\t" + horairesTXT + "\t" + DAB + "\t" + agences_HTM + "\r\n");

			sortie.close();
		} catch (IOException e) {

			e.printStackTrace();
		}

	}

	public void enregistrer_journal(int numreponse) {

//    	this.fichier = new File(Parametres.REPERTOIRE + "RESULTATS_BNP.");
		try {
			Temps_Fin = new Date();
			SimpleDateFormat formater = null;
			formater = new SimpleDateFormat("'le' dd/MM/yyyy '�' hh:mm:ss");
			String date = formater.format(Temps_Fin);
			date = formater.format(Temps_Fin);

			sortie_log = new BufferedWriter(new FileWriter(log, true));
			sortie_log.write(parcoursficval + ";" + "FinValeur" + ";" + CPSoumis + ";" + this.NbAgences + ";"
					+ nbRecuperes + ";" + (this.NbAgences - nbRecuperes) + ";" + date + "\r\n");
			sortie_log.close();
			long taille_fichier = fichier.length() / 1000000; // la length est en octets, on divise par 1000 pour mettre
																// en Ko et par 1000000 pour mettre en Mo

			if (taille_fichier > 4) {
				// INITIALISATION FICHIER DE RESULTAT
				this.fichier = new File(
						dossierRESU + "\\" + "RESULTATS_" + nomSITE + "_" + System.currentTimeMillis() + ".txt");
				// Attention pas d'accent dans les noms de colonnes !
				sortie = new BufferedWriter(new FileWriter(fichier, true));
				sortie.write("CodePostale" + "\t" + "NbAgences" + "\t" + "NOMWEB" + "\t" + "ADRESSE" + "\t" + "TEL"
						+ "\t" + "HORAIRESTXT" + "\t" + "DAB" + "\t" + "AGENCEHTM" + "\r\n");
				sortie.close();
			}

		} catch (IOException e) {

			e.printStackTrace();
		}

	}

	/**
	 * La m�thode <code>initialiser()</code> permet d'ajouter la ligne d'ent�te dans
	 * le fichier r�sultat.
	 */

	public void initialiser() {

		try {
			// Attention pas d'accent dans les noms de colonnes !
			sortie = new BufferedWriter(new FileWriter(fichier, true));
			sortie.write("DepSoumis" + "\t" + "NbAgences" + "\t" + "NOMWEB" + "\t" + "ADRESSE" + "\t" + "TEL" + "\t"
					+ "HORAIRESHTM" + "\t" + "HORAIRESTXT" + "\t" + "DAB" + "\t" + "SERVICES" + "\t" + "AGENCEHTM"
					+ "\r\n");
			sortie.close();

			File toto = new File(dossierRESU + "\\" + "LOG_" + nomSITE + ".csv");
			if (!toto.exists()) {
				// if (num_ligneficval == num_ligne) {
				/*
				 * sortie_log = new BufferedWriter(new FileWriter(log,true));
				 * sortie_log.write("NumLigne" + ";" +"FinValeur" + ";" + "DepSoumis" + ";" +
				 * "NbAgences" + ";" + "Nbrecuperes" + ";" + "Reste" + ";" + "Heure_Fin"+ "\r\n"
				 * ); sortie_log.close();
				 */
			}

		} catch (IOException e) {

			e.printStackTrace();
		}
	}

	public String getJson(Element root) {
		String rootContent = root.html().replaceAll("/{1,}<!\\[CDATA\\[", "");
		rootContent = rootContent.replaceAll("//\\]{1,}>", "").replaceAll("\\s{1,}", " ");
		String[] tmpArray = rootContent.split("var implantationsList =");
		String[] tmpArray2 = tmpArray[1].split("var polyList");

		return tmpArray2[0];
	}

	public List<SG_Outre_Mer> showFirstRows() {
		List<SG_Outre_Mer> a = rep.getFirstRow(100, SG_Outre_Mer.class);
		return a;
	}

	public List<SG_Outre_Mer> showLastRows() {
		List<SG_Outre_Mer> a = rep.getLastRow(100, SG_Outre_Mer.class);
		return a;
	}
}
