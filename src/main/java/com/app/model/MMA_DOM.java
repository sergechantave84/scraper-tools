package com.app.model;

import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="mma_dom", schema="scrap")
public class MMA_DOM {

	
	@Id
	@SequenceGenerator(name="scrap.mma_dom_id_seq",
	                    sequenceName ="scrap.mma_dom_id_seq",
	                     allocationSize=1)
    @GeneratedValue( strategy=GenerationType.SEQUENCE,
                     generator="scrap.mma_dom_id_seq")
	  
	@Column(name="id",updatable=false)
	private int id;
	
	@Column(name="regionsoumise", columnDefinition="varchar")
	private String regionSoumise;
	
	@Column(name="nbvillesreg", columnDefinition="varchar")
	private String nbVillesReg;
	
	@Column(name="numville", columnDefinition="varchar")
	private String numVille;
	
	@Column(name="ville_soumise", columnDefinition="varchar")
	private String villeSoumise;
	
	@Column(name="nbagences_ville", columnDefinition="varchar")
	private String nbAgencesVille;
	
	@Column(name="nom_agence", columnDefinition="varchar")
	private String nomAgence;
	
	@Column(name="adresse", columnDefinition="varchar")
	private String adresse;
	
	@Column(name="tel", columnDefinition="varchar")
	private String tel;
	
	@Column(name="noms_agents", columnDefinition="varchar")
	private String nomsAgents;
	
	@Column(name="orias", length=10)
	private String orias;
	
	@Column(name="horaires", columnDefinition="varchar")
	private String horaires;
	
	@Column(name="agencehtm", columnDefinition="varchar")
	private String agenceHtm;
	
	@Column(name="annee_du_scrap" ,columnDefinition="varchar(4) DEFAULT date_part('year',CURRENT_DATE)")
	private String anneeScrap;

	public MMA_DOM() {
		LocalDate ld = new Date().toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
		anneeScrap = String.valueOf(ld.getYear() + 1);
	}
	public int getId() {
		return id;
	}

	public MMA_DOM setId(int id) {
		this.id = id;
		return this;
	}

	public String getRegionSoumise() {
		return regionSoumise;
	}

	public MMA_DOM setRegionSoumise(String regionSoumise) {
		this.regionSoumise = regionSoumise;
		return this;
	}

	public String getNbVillesReg() {
		return nbVillesReg;
	}

	public MMA_DOM setNbVillesReg(String nbVillesReg) {
		this.nbVillesReg = nbVillesReg;
		return this;
	}

	public String getNumVille() {
		return numVille;
	}

	public MMA_DOM setNumVille(String numVille) {
		this.numVille = numVille;
		return this;
	}

	public String getVilleSoumise() {
		return villeSoumise;
	}

	public MMA_DOM setVilleSoumise(String villeSoumise) {
		this.villeSoumise = villeSoumise;
		return this;
	}

	public String getNbAgencesVille() {
		return nbAgencesVille;
	}

	public MMA_DOM setNbAgencesVille(String nbAgencesVille) {
		this.nbAgencesVille = nbAgencesVille;
		return this;
	}

	public String getNomAgence() {
		return nomAgence;
	}

	public MMA_DOM setNomAgence(String nomAgence) {
		this.nomAgence = nomAgence;
		return this;
	}

	public String getAdresse() {
		return adresse;
	}

	public MMA_DOM setAdresse(String adresse) {
		this.adresse = adresse;
		return this;
	}

	public String getTel() {
		return tel;
	}

	public MMA_DOM setTel(String tel) {
		this.tel = tel;
		return this; 
	}

	public String getNomsAgents() {
		return nomsAgents;
	}

	public MMA_DOM setNomsAgents(String nomsAgents) {
		this.nomsAgents = nomsAgents;
		return this;
	}

	public String getOrias() {
		return orias;
	}

	public MMA_DOM setOrias(String orias) {
		this.orias = orias;
		return this;
	}

	public String getHoraires() {
		return horaires;
	}

	public MMA_DOM setHoraires(String horaires) {
		this.horaires = horaires;
		return this;
	}

	public String getAgenceHtm() {
		return agenceHtm;
	}

	public MMA_DOM setAgenceHtm(String agenceHtm) {
		this.agenceHtm = agenceHtm;
		return this;
	}

	public String getAnneeScrap() {
		return anneeScrap;
	}

	public MMA_DOM setAnneeScrap(String anneeScrap) {
		this.anneeScrap = anneeScrap;
		return this;
	}
}
