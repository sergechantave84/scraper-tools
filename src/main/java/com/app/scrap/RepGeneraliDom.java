package com.app.scrap;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.app.model.Generali;
import com.app.model.Generali_DOM;

@Repository
public interface RepGeneraliDom extends JpaRepository<Generali_DOM,Integer>,RepositoryCustom<Generali_DOM> {

}
