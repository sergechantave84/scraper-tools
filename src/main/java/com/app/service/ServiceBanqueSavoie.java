package com.app.service;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.SocketTimeoutException;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Scanner;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import org.openqa.selenium.PageLoadStrategy;
import org.openqa.selenium.chrome.ChromeDriver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.app.model.BanqueSavoie;

import com.app.scrap.RepBanqueSavoie;

import com.app.utils.Parametres;
import com.app.utils.Utilitaires;

import fa.browserUtils.ChromeBrowser;
import fa.browserUtils.FactoryBrowser;
import fa.log.LogApp.LogScrap;
import fa.log.config.LogConst;

@Service
public class ServiceBanqueSavoie extends Utilitaires {

	static String driverPath = "C:\\ChromeDriver\\";

	String nomSITE = "BQ_SAVOIE";
	String ficval = "depBQ_SAVOIE";

	String url_AVTDEP = "http://agences.banquepopulaire.fr/bpgen/list.asp?dept=";
	String url_APSDEP = "&dataMask=______&typeId=BFBP_BS&env_info=10548";
	String url_debutAgence = "http://agences.banquepopulaire.fr/bpgen";

	int NbAgences;

	int nbRecuperes;
	int num_ligneficval;
	int parcoursficval;

	ChromeDriver chromeDriver;
	BufferedWriter sortie;

	@Autowired
	RepBanqueSavoie rep;

	public BanqueSavoie saveData(BanqueSavoie bqSavoie) {

		return rep.save(bqSavoie);
	}

	public List<BanqueSavoie> saveDataAll(List<BanqueSavoie> adreas) {

		return rep.saveAll(adreas);
	}

	public List<BanqueSavoie> getAll() {
		return rep.findAll();
	}

	public BanqueSavoie getAdreaById(int id) {

		return rep.findById(id).orElse(null);
	}

	public String deleteProduct(int id) {

		try {
			rep.deleteById(id);

		} catch (Exception e) {
			e.printStackTrace();
		}

		return "removed";

	}

	public List<BanqueSavoie> showFirstRows() {
		List<BanqueSavoie> a = rep.getFirstRow(100, BanqueSavoie.class);
		return a;
	}

	public List<BanqueSavoie> showLastRows() {
		List<BanqueSavoie> a = rep.getLastRow(100, BanqueSavoie.class);
		return a;
	}

	public BanqueSavoie setData(String depSoumis, String nbAgences, String enseigne, String nomWeb, String adresse,
			String tel, String fax, String horairesHtm, String horaires, String services, String dab,
			String agenceHtm) {
		BanqueSavoie bqSavoie = new BanqueSavoie();
		bqSavoie.setDepSoumis(depSoumis).setNbAgences(nbAgences).setEnseigne(enseigne).setNomWeb(nomWeb)
				.setAdresse(adresse).setTel(tel).setFax(fax).setHorairesHtm(horairesHtm).setHoraires(horaires)
				.setServices(services).setDab(dab).setAgenceHtm(agenceHtm);
		this.saveData(bqSavoie);

		return bqSavoie;
	}

	public void setUp() throws Exception {
		FactoryBrowser factBrowser = new FactoryBrowser();
		ChromeBrowser b = (ChromeBrowser) factBrowser.create("chrome");
		/*chromeDriver = b.setOptions(PageLoadStrategy.NORMAL, driverPath, 60000, 60000, 60000, false, false)
				.initBrowserDriver();*/ 
		
		final String binaryPath = "C:\\Program Files\\Google\\Chrome Beta\\Application\\chrome.exe";
		
		chromeDriver = b.setOptions(PageLoadStrategy.NORMAL, driverPath, 60000, 60000, 60000, false, false, binaryPath)
				.initBrowserDriver();
	}

	public void scrapBqSavoie() throws IOException, InterruptedException {

		String line = "";
		List<String> paths = new ArrayList<>();
		boolean isPageLoaded = false;

		Scanner sc = new Scanner(new InputStreamReader(
				new FileInputStream(new File(Parametres.REPERTOIRE_DATA_SOURCES + ficval + ".txt"))));
		String resultFolderName = Parametres.REPERTOIRE_RESULT + "\\BQ\\" + nomSITE;

		if (!new File(resultFolderName).exists()) {
			new File(resultFolderName).mkdirs();
		}

		this.fichier = new File(
				resultFolderName + "\\" + "RESULTATS_" + nomSITE + "_" + System.currentTimeMillis() + ".txt");
		LogScrap log = new LogScrap("BanqueSavoieLog.txt", LogConst.TYPE_LOG_FILE);

		if ((new File(LogConst.STORE_LOG_PATH + LogConst.PREFIX_LOG_FILE + "_BanqueSavoieLog.txt")).exists()) {

			num_ligneficval = Integer.parseInt(log.readLog().getLastLineProcessed().split("\t")[4]);

		} else {
			num_ligneficval = 1;
		}

		parcoursficval = 1;
		while (sc.hasNextLine()) {
			line = sc.nextLine();
			if (parcoursficval >= num_ligneficval) {
				String[] values = line.split("\\t");
				nbRecuperes = 0;
				String depSoumis = values[0];
				String uri = url_AVTDEP + depSoumis + url_APSDEP;
				try {
					System.out.println(uri);
					chromeDriver.get(uri);

				} catch (Exception e) {
					if (e instanceof SocketTimeoutException) {
						System.out.println(uri);
						chromeDriver.get(uri);
					}
				}
				isPageLoaded = this.listensIfPageFullyLoaded(chromeDriver);
				if (isPageLoaded) {
					try {
						Thread.sleep(5000);

					} catch (InterruptedException e) {

						System.out.println(e.getMessage());
						;
					}
					String pageSource = "";
					pageSource = chromeDriver.getPageSource();
					Document doc = Jsoup.parse(pageSource);
					Elements rootListAgencies = doc.select("#listPopup > table");
					NbAgences = rootListAgencies.size();
					if (!paths.isEmpty()) {
						paths.clear();
					}
					for (Element tmp : rootListAgencies) {
						Element href = tmp.selectFirst("tbody > tr:nth-child(1) > td:nth-child(3) > a");
						String strHref = href.attr("href");
						paths.add(strHref);
					}
					for (String path : paths) {
						String uri2 = url_debutAgence + "/" + path;
						try {
							System.out.println(uri2);
							chromeDriver.get(uri2);
						} catch (Exception e) {
							if (e instanceof SocketTimeoutException) {
								System.out.println(uri2);
								chromeDriver.get(uri2);
							}
						}
						isPageLoaded = this.listensIfPageFullyLoaded(chromeDriver);
						if (isPageLoaded) {
							Thread.sleep(5000);
							String nom_agence = "";
							String adresse = "";
							String tel = "";
							String fax = "";
							String enseigne = "BANQUE DE SAVOIE";
							String HorairesHTM = "";
							String HorairesTXT = "";
							String Services = "";
							int DAB = 0;
							String agences_HTM = "";

							pageSource = chromeDriver.getPageSource();
							doc = Jsoup.parse(pageSource);

							Element bigRoot = doc.selectFirst("body > div.cadre2 > div.bloc4g > table > tbody > tr");

							if (bigRoot != null) {
								agences_HTM = bigRoot.outerHtml();
								agences_HTM = agences_HTM.replaceAll("\\s{1,}", "");
								agences_HTM = this.normalize(agences_HTM);

								Element agencyNameElement = bigRoot.selectFirst("td.infoTxt>font");
								nom_agence = (agencyNameElement != null) ? agencyNameElement.text() : "";
								nom_agence = this.normalize(nom_agence);
								System.out.println("Nom de l'agence : " + nom_agence);

								String coordinateInfo = bigRoot.selectFirst("td.infoTxt").outerHtml();
								coordinateInfo = coordinateInfo.replaceAll("</font>", "skullLand");
								coordinateInfo = coordinateInfo.replaceAll("<b>Tél. :</b>", "skullLand");
								coordinateInfo = coordinateInfo.replaceAll("<b>Fax :</b>", "skullLand");
								coordinateInfo = coordinateInfo.replaceAll("<a .+>", "skullLand");

								String[] coordinateInfos = coordinateInfo.split("skullLand");

								adresse = coordinateInfos[1];
								adresse = adresse.replaceAll("&nbsp;", " ");
								adresse = adresse.replaceAll("<br>", " ");
								adresse = adresse.replaceAll("#CRLF#", " ");
								adresse = adresse.replaceAll("( ){2,}", " ");
								adresse = adresse.replaceAll(",", " ");
								adresse = this.normalize(adresse).toUpperCase();
								System.out.println("Adresse : " + adresse);

								tel = coordinateInfos[2];
								tel = tel.replaceAll("[^0-9]{1,}", " ");
								tel = tel.trim();

								System.out.println("Tel : " + tel);

								fax = coordinateInfos[3];
								fax = fax.replaceAll("[^0-9]{1,}", " ");
								fax = fax.trim();

								System.out.println("Fax : " + fax);

								Elements scheduleElements = bigRoot.select("td.infoTxt>table > tbody > tr");
								if (scheduleElements != null) {
									HorairesHTM = scheduleElements.outerHtml();
									HorairesHTM = HorairesHTM.replaceAll("\\s{1,}", "");
									HorairesTXT = "";
									for (Element scheduleElement : scheduleElements) {

										HorairesTXT += scheduleElement.text() + " ";

									}
									HorairesTXT = HorairesTXT.replaceAll("Horaires d'ouverture", "");
									

									if(!HorairesTXT.toLowerCase().contains("lundi")) {
										HorairesTXT ="Lundi ferme "+HorairesTXT ;
									}
									if(!HorairesTXT.toLowerCase().contains("samedi")) {
										HorairesTXT =HorairesTXT +"Samedi ferme ";
									}
									if(!HorairesTXT.toLowerCase().contains("dimanche")) {
										HorairesTXT =HorairesTXT +"Dimanche ferme ";
									}

									HorairesTXT = HorairesTXT.trim();
									System.out.println("Horaires " + HorairesTXT);

								}
								HorairesHTM = this.normalize(HorairesHTM);
								HorairesTXT = this.normalize(HorairesTXT);
								Element serviceElement = doc.selectFirst(
										"body > div.cadre2 > div.bloc4g > table > tbody > tr > td.infoMoinsTxt");
								Services = (serviceElement != null) ? serviceElement.text() : "";
								Services = Services.replaceAll("\\s{1,}", " ");
								Services = this.normalize(Services);
								System.out.println("Services : " + Services);

								if (Services.contains("Distributeur automatique de billets")) {
									DAB = 1;
								}
								
								System.out.println("DAB : " + DAB);

								this.setData(depSoumis, String.valueOf(NbAgences), enseigne, nom_agence, adresse, tel,
										fax, HorairesHTM, HorairesTXT, Services, String.valueOf(DAB), agences_HTM);
							}

						}

					}
				}
				log.writeLog(new Date(), depSoumis, "no_error", NbAgences, parcoursficval, 0);
			}
			parcoursficval++;
		}
	}

	public String getText(String subject, String firstStr, String lastStr) {
		int beginIndex = subject.indexOf(firstStr);
		int lastIndex = subject.indexOf(lastStr);
		String strCuted = subject.substring(beginIndex, lastIndex);
		return strCuted;
	}

	public void enregistrer(String agences_HTM, String nom_agence, String adresse, String tel, String fax,
			String HorairesHTM, String HorairesTXT, String Services, String enseigne, String depSoumis, int DAB) {

		try {

			sortie = new BufferedWriter(new FileWriter(fichier, true));
			sortie.write(depSoumis + "\t" + NbAgences + "\t" + enseigne + "\t" + nom_agence + "\t" + adresse + "\t"
					+ tel + "\t" + fax + "\t" + HorairesHTM + "\t" + HorairesTXT + "\t" + Services + "\t" + DAB + "\t"
					+ agences_HTM + "\r\n");

			sortie.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	/**
	 * La m�thode <code>initialiser()</code> permet d'ajouter la ligne d'ent�te dans
	 * le fichier r�sultat.
	 */

	public void initialiser() {

		try {
			// Attention pas d'accent dans les noms de colonnes !
			sortie = new BufferedWriter(new FileWriter(fichier, true));
			sortie.write("DepSoumis" + "\t" + "NbAgences" + "\t" + "ENSEIGNE" + "\t" + "NOMWEB" + "\t" + "ADRESSE"
					+ "\t" + "TEL" + "\t" + "FAX" + "\t" + "HORAIRESHTM" + "\t" + "HORAIRESTXT" + "\t" + "SERVICES"
					+ "\t" + "DAB" + "\t" + "AGENCEHTM" + "\r\n");
			sortie.close();

		} catch (IOException e) {

			e.printStackTrace();
		}
	}

	public void tearDowns() {
		chromeDriver.quit();
	}
}
