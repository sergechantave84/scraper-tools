package com.app.utils;

import java.io.File;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;

import org.openqa.selenium.OutputType;
import org.openqa.selenium.PageLoadStrategy;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.phantomjs.PhantomJSDriver;
import org.openqa.selenium.phantomjs.PhantomJSDriverService;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.gargoylesoftware.htmlunit.FailingHttpStatusCodeException;
import com.gargoylesoftware.htmlunit.WebClient;

public class BrowserUtils {
     
	protected static ChromeDriver chromeDriver=null;
	protected static HtmlUnitDriver htmlUnitDriver=null;
	protected static PhantomJSDriver gostDriver=null;
	protected static ChromeOptions chromeOptions=new ChromeOptions();
	
	/**
	 * create an instance of chrome driver
	 * @param driverPath : url to the chromedriver.exe in your hdd
	 * @param scriptTimeout :time to wait for an asynchronous script to finish execution beforethrowing an error 
	 * If the timeout is negative, then the script will be allowed to runindefinitely.
	 * @param pageLoadTimeout ; time to wait for a page load to complete before throwing an error.If the timeout is negative, page loads can be indefinite.
	 * @param implicitlyWait : time the driver should wait when searching for an element if it isnot immediately present. 
	 * @param PageLoadStrategy : Defines the current session’s page loading strategy. By default, when Selenium WebDriver loads a page, it follows the normal pageLoadStrategy. 
	 * It is always recommended to stop downloading additional resources (like images, css, js) when the page loading takes lot of time. the the differents values of page load strategy are:
	 * - PageLoadStrategy.NORMAL (This will make Selenium WebDriver to wait for the entire page is loaded. When set to normal, Selenium WebDriver waits until the load event fire is returned.By default normal is set to browser if none is provided.)
	 * -PageLoadStrategy.EAGER (This will make Selenium WebDriver to wait until the initial HTML document has been completely loaded and parsed, and discards loading of stylesheets, images and subframes.When set to eager, Selenium WebDriver waits until DOMContentLoaded event fire is returned.)
	 * -PageLoadStrategy.NONE (When set to none Selenium WebDriver only waits until the initial page is downloaded.)
	 * @return ChromeDriver
	 * */
	public static ChromeDriver  chromeConstruct(String driverPath, long scriptTimeout, long pageLoadTimeout,long implicitlyWait,PageLoadStrategy pageLoadStrategy ) {
		
			  System.setProperty("webdriver.chrome.driver", driverPath+"chromedriver.exe");
	    	  System.setProperty("webdriver.chrome.silentOutput", "true"); 
	    	  chromeOptions.setPageLoadStrategy(pageLoadStrategy);
	  		  chromeDriver = new ChromeDriver(chromeOptions);
	  		  chromeDriver.manage (). window (). maximize ();
	  		  chromeDriver.manage().deleteAllCookies();
	  		  chromeDriver.manage().timeouts().setScriptTimeout(scriptTimeout, TimeUnit.SECONDS);
	  		  chromeDriver.manage().timeouts().pageLoadTimeout( pageLoadTimeout, TimeUnit.SECONDS);
	          chromeDriver.manage().timeouts().implicitlyWait( implicitlyWait, TimeUnit.SECONDS);
	      	  return chromeDriver;
		
	
		
	}
	
	/**
	 * create HtmlUnitDriver instance
	 * @param secureSll: establishing an encrypted link between a server and a client—typically a web server (website) and a browser, or a mail server and a mail client.
	 * true to enable ssl.
	 * @param enableScriptError: make HtmlUnitDriver more verbose when it catch an javascript execution error, true to enable scriptError.
	 * @param enableCSS:  make HtmlUnitDriver more verbose when it catch an css execution error, true to enableCSS.
	 * @param pageLoadTimeout: time to wait for a page load to complete before throwing an error.If the timeout is negative, page loads can be indefinite.
	 * @return HtmlUnitDriver
	 * */

	 public static HtmlUnitDriver HtmlUnitConstruct(boolean secureSll,boolean enableScriptError,boolean enableCSS,int pageLoadTimeout) {
		
			htmlUnitDriver= new HtmlUnitDriver() 
		    	{
		    		 @Override
			    	 protected WebClient modifyWebClient(WebClient client) 
			    	 {
			    		 final WebClient webClient = super.modifyWebClient(client);
			    		 String stm=webClient.getBrowserVersion().toString();
			     	     System.out.println(stm);
			     	     webClient.getOptions().setUseInsecureSSL(secureSll);
			     	     webClient.getOptions().setTimeout(pageLoadTimeout);
						 webClient.getOptions().setThrowExceptionOnScriptError(enableScriptError); 
			     		 webClient.getOptions().setCssEnabled(enableCSS);
			     		 
			     		 return webClient;
			    	 }
		    	};
		    	htmlUnitDriver.manage().deleteAllCookies();
		    	htmlUnitDriver.manage().timeouts().pageLoadTimeout(60000, TimeUnit.SECONDS);
		    	htmlUnitDriver.manage().timeouts().implicitlyWait(120000,TimeUnit.SECONDS);
		    	return htmlUnitDriver;
		
		
	}
	/**
	 * create an instance of phantomJSdriver
	 * @param driverPath : url to the phantomJS.exe in your hdd
	 * @param takesScreenTshot: allow phantomJS to take screenshot, ste true to take screenshot
	 * @param enableScriptError: make HtmlUnitDriver more verbose when it catch an javascript execution error, true to enable scriptError
	 * @return PhantomJSDrive
	 * */
	  public static PhantomJSDriver PhantomJsConstruct(String driverPath, boolean takesScreenTshot,boolean enableScriptError) {
		
			   DesiredCapabilities desireCaps = new DesiredCapabilities();
			   desireCaps.setCapability(PhantomJSDriverService.PHANTOMJS_EXECUTABLE_PATH_PROPERTY,driverPath);
			   desireCaps.setCapability("takesScreenshot", takesScreenTshot);
			   desireCaps.setJavascriptEnabled(enableScriptError);
			   desireCaps.setCapability(PhantomJSDriverService.PHANTOMJS_CLI_ARGS, "--webdriver-loglevel=ERROR");
			   gostDriver= new PhantomJSDriver(desireCaps);
			   gostDriver.manage (). window (). maximize ();
			   gostDriver.manage().deleteAllCookies();
			   return gostDriver;
		
		
	}
	
	/**
	* use it if you want take screeshot of current page
	* @param pathTarget path to store screenshot example "C:\\screenshot\\image.jpeg"
	* */
	public static void takeScreenshotPage(WebDriver wd,String pathTarget) throws IOException {
		File srcFile = ((TakesScreenshot) wd).getScreenshotAs(OutputType.FILE);
        FileUtils.copyFile(srcFile, new File(pathTarget),true);
	}
	
	/**
	* use it if you want take screeshot of current element
	* @param pathTarget path to store screenshot, example "C:\\screenshot\\image.jpeg"
	* */
	public static void takeScreenshotOfElement(WebElement elementToTakeScreenshot,String pathTarget) throws IOException {
		File srcFile=elementToTakeScreenshot.getScreenshotAs(OutputType.FILE);
		FileUtils.copyFile(srcFile, new File(pathTarget),true);
	}
	
	/**
	 * use this methode if you want navigate through website
	 * */
	public static void openWebSite(WebDriver wd,String url) {
            wd.navigate().to(url);           		
	}
	
	public static void pressBrowserBackBtn(WebDriver wd) {
		 wd.navigate().back();
	}
	
	public static void refreshPage(WebDriver wd) {
		wd.navigate().refresh();
	}
	
	/**
	 * Clicking a link which opens in a new window will focus the new window or tab on screen, 
	 * but WebDriver will not know which window the Operating System considers active. To work with the new window you will need to switch to it.
	 * If you have only two tabs or windows open, and you know which window you start with, 
	 * by the process of elimination you can loop over both windows or tabs that WebDriver can see, and switch to the one which is not the original
	 * */
	public static void switchWindowOrTab(WebDriver wd, String linkOpensNewWindow) {
		String originalWindowID=wd.getWindowHandle();
		assert wd.getWindowHandles().size()==1;
		wd.findElement(By.linkText(linkOpensNewWindow)).click();
		
		for(String windowHandle: wd.getWindowHandles()) {
			if(!originalWindowID.contentEquals(windowHandle)) {
		        wd.switchTo().window(windowHandle);
		        break;
		    }
		}
		
	}
	
	/**
	 * If you need to work with frames or iframe
	 * */
	public static boolean switchToFrame(WebDriver wd, WebElement frameElement ) {
		   if(wd.switchTo().frame(frameElement)!=null) {
			   return true;
		   }
		   return false;
	}
	/**
	 * use it if you want leave Iframe
	 * */
	public static void leaveIFrame(WebDriver wd) {
		wd.switchTo().defaultContent();
	}
	
	/**
	 * close alert popup
	 * @param getMsgAlert if is equal to true this methode return alert message 
	 * */
	public static String closeAlertPopup(WebDriver wd,boolean getMsgAlert) {
		Alert alert= wd.switchTo().alert();
		alert.accept();
		if(getMsgAlert) {
			  return  alert.getText();
		}
		return "";
	}
	
	/**
	 * if you want to close confirm popup
	 *  @param getMsgAlert if is equal to true this methode return confirm message 
	 * */
	public static String closeConfirmPopup(WebDriver wd,boolean getMsgAlert) {
		Alert alert= wd.switchTo().alert();
		alert.dismiss();
		if(getMsgAlert) {
			  return  alert.getText();
		}
		return "";
	}
	
	/**
	 * Enter "webdriver" text and perform keyboard action
	 * */
	 public static void keydown(WebElement element, String valueTowrite,CharSequence key ) {
		 element.sendKeys(valueTowrite+key);
	 }
	 
	 /**
	  * if you want simulate action of pressing a modifier key(CONTROL, SHIFT, ALT)
	  * 
	  * @param key Either Keys.SHIFT, Keys.ALT or Keys.CONTROL. If theprovided key is none of those, IllegalArgumentException is thrown.
	  * @param wd is an instance of WebDriver
	  * @param sendKey is string
	  * @example if you want to simulate ctrl+a, key= Keys.CONTROL and sendKey="a"
	  * 
	  * **/
	 public static void keydown(WebDriver wd,CharSequence key,String sendKey) {
		 try {
			 Actions actionProvider= new Actions(wd);
			 Action keydown=actionProvider.keyDown(key).sendKeys(sendKey).build();
			 keydown.perform();
		 }catch(Exception e) {
			 e.printStackTrace();
		 }
	}
	 
	 /**
	  * use it if you want get http status of an web page
	  * @return integer : the value of http status
	  * */
	 public static int getHttpStatus(String url) {
			int statusCode=0;
		    WebClient webClient= new WebClient();
		    webClient.getOptions().setTimeout(6000000);
		    webClient.getOptions().setThrowExceptionOnScriptError(false); 
		    webClient.getOptions().setJavaScriptEnabled(false);
		    webClient.getOptions().setCssEnabled(false);
		    try{
		    	statusCode = webClient.getPage(url).getWebResponse().getStatusCode();
		    	webClient.close();
		    }catch(Exception e){
		    	 //e.printStackTrace();
		    	 if(e instanceof FailingHttpStatusCodeException) {
		    		 statusCode = ((FailingHttpStatusCodeException) e).getStatusCode();
		    	 }
		    }
		        
		    return statusCode;
		}
	 /**
	  * use it to check if web site  you scrap is blocked or not
	  * the http status of unblocked web site is 200
	  * */
	 public static boolean isBlocked(String url) {
		 int status=getHttpStatus(url);
		 
		 return status==200 ;
	 }
	 
	 public static void waitBeforeClick(WebElement element,WebDriver driver) {
		 Actions actions = new Actions(driver);
		 actions.moveToElement(element).click().build().perform();
	 }
	 
	 public static void  waitBeforeClick2(WebElement element,WebDriver driver) {
		 WebDriverWait wait= new WebDriverWait(driver, 20);
		 wait.until(ExpectedConditions.elementToBeClickable(element));
		 element.click();
	 }
}
