package com.app.service;

import java.io.BufferedInputStream;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;

import java.net.SocketTimeoutException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Scanner;

import org.json.JSONObject;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.app.model.April;
import com.app.scrap.RepApril;

import com.app.utils.Parametres;
import com.app.utils.Utilitaires;

import fa.browserUtils.FactoryBrowser;
import fa.browserUtils.HtmlUnitBrowser;
import fa.log.LogApp.LogScrap;
import fa.log.config.LogConst;

@Service
public class ServiceApril extends Utilitaires {

	@Autowired 
     private RepApril rep;
	
	    private List<April> april= new ArrayList<>();
		private String ficVal="regApril"; 
		private String nomSITE = "APRIL";
		private String dossierRESU =Parametres.REPERTOIRE_RESULT + "\\ASSU\\" +nomSITE ;;
		private String url_accueil = "https://agence.april.fr/fr/";
		
		private int NbAgences;
		private int NbAgences_page;
	
	    private int num_page;	
	    private int nbRecuperes;
	

		
		private HtmlUnitDriver wd;
		private BufferedWriter sortie;
		private BufferedWriter sortie_log;	
		
		private Date Temps_Fin;
		
		
	   
	  
	
	public April saveData(April t) {
		if(t instanceof April) {
			 return (April) rep.save((April)t);
		}
		return null ;
	}

	
	public List<April> saveDataAll(List<April> t) {
		
			 return  rep.saveAll(t);
		
		
	}

	
	public List<April> getElmnt() {

		return  (List<April>) rep.findAll();
	}

	public April getById(int id) {
		return (April)rep.findById(id).orElse(null);
	}

	public String deleteProduct(int id) {
		try{
			 rep.deleteById(id);
			 
		 }catch(Exception e){
			 e.printStackTrace();
		 }
		 
		 return "removed";
	}

	
	public April updateProduct(April t) {
		// AprilODO Auto-generated method stub
		return null;
	}

	public April setData( String regionSoumise,String nbAgences,String lienAgence,
			              String nomAgence,String adresse,String tel,String horaires,String agenceHAprilM) {
		April april= new April();
		april.setRegionSoumise(regionSoumise).
		      setNbAgences(nbAgences).
		      setLienAgence(lienAgence).
		      setNomAgence(nomAgence).
		      setAdresse(adresse).
		      setTel(tel).
		      setHoraires(horaires).
		      setAgenceHTM(agenceHAprilM);
	this.saveData(april);
		return april;
	}
	
	public List<April> showFirstRows() {
		 List<April> a=rep.getFirstRow(100, April.class);
		 return a;
	 }
	 
	 public List<April> showLastRows(){
		 List<April> a=rep.getLastRow(100, April.class);
		 return a;
	 }
	
	   public void setUp() throws Exception {
		 FactoryBrowser factB=new FactoryBrowser();
		 HtmlUnitBrowser htmlUnitBrowser=(HtmlUnitBrowser) factB.create("htmlUnit");
	     wd=htmlUnitBrowser.setOptions(true,false,false,60000).initBrowserDriver();
	   }
	   
	   
	   public List<April> scrapApril() throws IOException {
	       
	    	april.clear();
	    	String pageSource="";
	    	Document doc=null;
	    	JSONObject jo=null;
	    	File f=new File(dossierRESU);
	    	if(!f.exists())
	        {
	            f.mkdirs();
	        }
	    	int currentPage=1;
	    	fichier = new File(dossierRESU + "\\"+ "RESULTATS_" + nomSITE +   "_" + System.currentTimeMillis() + ".txt");
	    	LogScrap log =new LogScrap("AssuranceAprilLog.txt", LogConst.TYPE_LOG_FILE);
	        if((new File(LogConst.STORE_LOG_PATH+LogConst.PREFIX_LOG_FILE+"_AssuranceAprilLog.txt")).exists()) {
				 
	               currentPage=Integer.parseInt(log.readLog().getLastLineProcessed().split("\t")[4]);
				
			 }
	        Scanner sc=new Scanner(new BufferedInputStream(new FileInputStream(new File(Parametres.REPERTOIRE_DATA_SOURCES)+"\\"+ficVal+".txt")));
	    	int i=0;
	    	List<String> paths=new ArrayList<>();
	    	while(sc.hasNextLine()) 
	    	{
	    		 String currentLine=sc.nextLine();
	    		 if(i>=currentPage) 
	    		 {
	    			 String uri=url_accueil+currentLine;
	    			 try {
	    				   System.out.println(uri);
	    		    		wd.get(uri);
	    		     }catch(Exception e) {
	    		    		e.printStackTrace();
	    		    		if(e instanceof SocketTimeoutException) 
	    		    		{
	    		    			wd.get(uri);
	    		    		}
	    		     }
	    			 pageSource=wd.getPageSource();
	    		     doc=Jsoup.parse(pageSource);
	    		     //#lf-content > div.lf-geo-divisions__main__content > div > div.lf-geo-divisions__main__content__wrapper__locations > ul > li:nth-child(1)
	    		     Element rootElement=doc.selectFirst( "#lf-content>div.lf-geo-divisions__main__content>div>"+
	    		     		                               "div.lf-geo-divisions__main__content__wrapper__locations>ul");
	    		    
	    		     if(rootElement!=null) {
	    		    	 Elements hrefs=rootElement.select( "li > a.lf-location-default__content.lf-location-default__content--link."+
	    		    	 		                            "lf-geo-divisions__main__content__wrapper__locations__list__location__content");
	    		    	 
	    		    	 
	    	             if(!paths.isEmpty()) {
	    	            	 paths.clear();
	    	             }
	    		    	 for(Element href: hrefs) {
	    		    	    paths.add(href.attr("href")) ;           
	    		    	 	
	    		    	 }
	    		    	 NbAgences=paths.size();
	    		    	 for(String path:paths) 
	    		    	 {
	    		    		 String agencyURI="https://agence.april.fr"+path;
	    		    		 try {
	    	    				    System.out.println(agencyURI);
	    	    		    		wd.get(agencyURI);
	    	    		     }catch(Exception e) {
	    	    		    		e.printStackTrace();
	    	    		    		if(e instanceof SocketTimeoutException) 
	    	    		    		{
	    	    		    			wd.get(agencyURI);
	    	    		    		}
	    	    		     }
	    		    		 pageSource=wd.getPageSource();
	    	    		     doc=Jsoup.parse(pageSource);
	    	    		     Element rootResultContent=doc.selectFirst("#lf-content>main.lf-location__main");
	    	    		     
	    	    		     if(rootResultContent!=null) {
	    	    		    	 
	    	    		    	 String lien_agence="";
	    	    		    	 String nom_agence="";
	    	    		    	 String adresse="";
	   		                     String tel="";
	   		                     String horaires="";
	   		                     String agence="";
	   		                     
	   		                     lien_agence=agencyURI;
	   		                     
	   		                     agence=rootResultContent.outerHtml();
	   		                     agence=agence.replaceAll("\\s{2,}","");
	   		                     agence=this.normalize(agence);
	   		                     Element bigBlockElement=rootResultContent.selectFirst(".lf-location__main__infos>.lf-location__main__infos__left");
	   		                     
	   		                     if(bigBlockElement!=null) {
	   		                    	 Element titleElement=bigBlockElement.selectFirst("#lf-location-wcag-titre");
	   		                    	 nom_agence=(titleElement!=null) ? titleElement.text():"";
	   		                    	 nom_agence=this.normalize(nom_agence);
	   		                    	 System.out.println("nom agence "+nom_agence);
	   		                    	 
	   		                    	 Element addresseElement=bigBlockElement.selectFirst(".lf-parts-address.lf-location__main__infos__left__address");
	   		                    	 adresse=(addresseElement!=null)  ? addresseElement.text():"";
	   		                    	 adresse=this.normalize(adresse);
	   		                    	 System.out.println("adresse "+ adresse);
	   		                    	 
	   		                    	 Element phoneElement=bigBlockElement.selectFirst("div.lf-location__main__infos__left__cta >"+
	   		                    	 		                                          ".lf-location__main__infos__left__cta__cta-phone>div>a");
	   		                    	 tel=(phoneElement!=null) ?phoneElement.attr("href"):"";
	   		                    	 tel=tel.replaceAll("[^0-9]{1,}","");
	   		                    	 tel=this.normalize(tel);
	   		                    	 System.out.println("le tel est:"+tel);
	   		                    	 
	   		                    	 Elements scheduleElements = rootResultContent.select("#lf-parts-opening-hours>div.lf-parts-opening-hours__content.lf-location__main__highlight__hours__opening-hours__content>div");
	   		                    	 
	   		                    	 for(Element scheduleElement: scheduleElements) {
	   		                    		
	   		                    		      String strTmp=scheduleElement.text();
	   		                    		      horaires+=strTmp+" ";
	   		                    	 }
	   		                    	 horaires=horaires.replaceAll("Horaires d'ouverture d'aujourd'hui","");
	   		                    	 horaires=this.normalize(horaires);
	   		                    	
	   		                    	 april.add(this.setData(currentLine, String.valueOf(NbAgences), lien_agence, nom_agence, 
	   		                    			                adresse, tel, horaires, agence));
	   		                    	 System.out.println(horaires);
	   		                     }
	   		                     
	    	    		     }
	    		    	 }
	    		     }
	    		     log.writeLog(new Date(),currentLine,"no_error", NbAgences, i, (NbAgences-nbRecuperes));
	    		 }
	    		 i++;
	    	}
			return april;
	    }
	   
	    
	   public void enregistrer ( String valSom,String lien_agence,String nom_agence,String adresse,
	           String tel,String Horaires, String agence)
	{
	try {
	sortie = new BufferedWriter(new FileWriter(fichier,true));
	sortie.write( valSom+"\t"+NbAgences+"\t"+lien_agence+"\t"+nom_agence+ 
			    "\t"+adresse+"\t"+tel+"\t"+Horaires+"\t"+agence+"\r\n");
	sortie.close();
	} catch (IOException e) {

	}

	}

	public void enregistrer_journal(int numreponse) {
	try {
	Temps_Fin = new Date();
	SimpleDateFormat formater = null;
	formater = new SimpleDateFormat("'le' dd/MM/yyyy '�' hh:mm:ss");
	String date =  formater.format(Temps_Fin);
	date =  formater.format(Temps_Fin);

	sortie_log = new BufferedWriter(new FileWriter(log,true));
	sortie_log.write( num_page+";"+"FinValeur"+";"+NbAgences_page+";"+ 
	           nbRecuperes+";"+ (NbAgences_page - nbRecuperes)+";"+ 
				  date+"\r\n");
	sortie_log.close();
	long  taille_fichier = fichier.length()/1000000; 
	if(taille_fichier> 8) {
	fichier = new File(dossierRESU + "\\"+ "RESULTATS_" + nomSITE +   "_" + System.currentTimeMillis() + ".txt");
	sortie = new BufferedWriter(new FileWriter(fichier,true));
	sortie.write( "VALEUR_SOUMISE"+"\t"+"NbAgences"+"\t"+"LIEN_AGENCE"+"\t"+
			       "NOM_AGENCE"+"\t"+"ADRESSE"+"\t"+"TEL"+"\t"+
			       "HORAIRESHTM"+"\t"+"AGENCEHTM"+ "\r\n");
	sortie.close();
	}

	} catch (IOException e) {

	}

	}

	public void initialiser() {

	try {
	sortie = new BufferedWriter(new FileWriter(fichier,true));
	sortie.write( "VALEUR_SOUMISE"+"\t"+"NbAgences"+"\t"+"LIEN_AGENCE"+"\t"+ 
		        "NOM_AGENCE"+"\t"+"ADRESSE"+"\t"+"TEL"+"\t"+
		        "HORAIRESHTM"+"\t"+"AGENCEHTM"+ "\r\n");
	sortie.close();
	File toto = new File(dossierRESU + "\\"+  "LOG_" + nomSITE +  ".csv"); 
	if( !toto.exists() ){
	sortie_log = new BufferedWriter(new FileWriter(log,true));
	sortie_log.write( "num_page"+";"+"FinValeur"+";"+ 
	                  "NbAgences_page"+";"+"Nbrecuperes"+";"+ 
			              "Reste"+";"+"Heure_Fin"+"\r\n" );
	sortie_log.close();
	}

	} catch (IOException e) {


	}
	}
	    
	   
	    public void tearDown() {
	        try{
	        	wd.quit();
	        }catch(Exception e) {
	        	
	        }
	        
	    }
	    
	  
}
