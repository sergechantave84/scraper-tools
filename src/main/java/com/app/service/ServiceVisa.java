package com.app.service;


import java.io.BufferedWriter;
import java.io.File;

import java.io.FileWriter;
import java.io.IOException;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Scanner;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.openqa.selenium.Alert;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.NoAlertPresentException;
import org.openqa.selenium.PageLoadStrategy;
import org.openqa.selenium.UnhandledAlertException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.app.model.VISA;
import com.app.scrap.RepVISA;

import com.app.utils.Parametres;
import com.app.utils.Utilitaires;

import fa.browserUtils.ChromeBrowser;
import fa.browserUtils.FactoryBrowser;
import fa.log.LogApp.LogScrap;
import fa.log.config.LogConst;

@Service
public class ServiceVisa extends Utilitaires{

	@Autowired
	private RepVISA rep;

	static String driverPath = "C:\\ChromeDriver\\";
	String nomSITE = "VISA";
	String ficval = "VISA_CPCANTON";

	
	private List<com.app.model.VISA> visaList=new ArrayList<>();
	String dossierRESU = Parametres.REPERTOIRE_RESULT + "\\BQ\\" +nomSITE ;;
	String url_accueil = "https://www.visa.com/atmlocator/#(language:french,page:home)";
	String fichier_valeurs; 
	String codpostSoumis;
	String communeSoumise;
	String ligne;
    int num_ligne = 2;
	int AgenciesNumberPerPage;
	int PageNumbers;	
	int numreponse = 1;
    int numpage;
	int DAB = 1;
    int nbRecuperes_page;
	int num_ligneficval;
	int parcoursficval;
	int Resultats ;
    ChromeDriver chromeDriver;
	BufferedWriter sortie;
	BufferedWriter sortie_log;	
	Date Temps_Fin;
	Document doc =null;
	boolean bool=true;
	
	public VISA saveData(VISA t) {

		
			return rep.save(t);
		
	}

	
	public List<VISA> saveDataAll(List<VISA> t) {
		
			return  rep.saveAll(t);
		
	}

	
	public List<VISA> getElmnt() {

		return  rep.findAll();
	}

	
	public VISA getById(int id) {

		return  rep.findById(id).orElse(null);
	}

	
	public String deleteProduct(int id) {
		try {
			rep.deleteById(id);

		} catch (Exception e) {
			e.printStackTrace();
		}

		return "removed";
	}

	
	public VISA updateProduct(VISA t) {
		// VISAODO Auto-generated method stub
		return null;
	}
	public VISA setData( String cpSoumis,String nbPages,String numPage,String nbAgencesPage,String nomWeb,String voie,
			             String cp,String ville,String dab,String agenceHtm) {
		VISA visa=new VISA();
		visa.setCpSoumis(cpSoumis).
		     setNbPages(nbPages).
		     setNumPage(numPage).
		     setNbAgencesPage(nbAgencesPage).
		     setNomWeb(nomWeb).
		     setVoie(voie).
		     setCp(cp).
		     setVille(ville).
		     setDab(dab).
		     setAgenceHtm(agenceHtm);
		this.saveData(visa);
		return visa;
	}
	public List<VISA> showFirstRows() {
		 List<VISA> a=rep.getFirstRow(100, VISA.class);
			 return a;
	}
		 
	public List<VISA> showLastRows(){
			 List<VISA> a=rep.getLastRow(100, VISA.class);
			 return a;
	}
	public void setUp() throws Exception 
	{
		FactoryBrowser factBrowser=new FactoryBrowser();
		ChromeBrowser b=(ChromeBrowser) factBrowser.create("chrome");
		chromeDriver =b.setOptions(PageLoadStrategy.NORMAL,driverPath, 60000,60000,60000,false,false).initBrowserDriver();
    }
    
    
    public List<com.app.model.VISA> open_VISA_byselenium() throws Exception 
    {
        //for(int j=1; j<96;j++)
        //{
        	fichier_valeurs = Parametres.REPERTOIRE_DATA_SOURCES+ficval+".txt";
        	System.out.println(fichier_valeurs);
        	Scanner sc = new Scanner(new File(fichier_valeurs));
    		File directoryResultFile= new File(dossierRESU);
        	if(!directoryResultFile.exists())
        		  directoryResultFile.mkdirs(); 
            
        	fichier = new File(dossierRESU + "\\RESULTATS_" + nomSITE +"_"+ System.currentTimeMillis() + ".txt");
        	LogScrap log = new LogScrap("AssuranceVISA.txt", LogConst.TYPE_LOG_FILE);
    		num_ligneficval = ((new File(LogConst.STORE_LOG_PATH + LogConst.PREFIX_LOG_FILE + "_AssuranceVISA.txt"))
    				.exists()) ? num_ligneficval = Integer.parseInt(log.readLog().getLastLineProcessed().split("\\t")[4])
    						: 1;
            
             this.initialiser();
        	 parcoursficval = 1;
        	
        	
        	  while ( sc.hasNextLine() ) 
        	 {
        		String line = "",js="",html="";
        		line=sc.nextLine();
        		String[] fic_valeurs = line.split("\\t");
        		if (parcoursficval>=num_ligneficval) 
        		{
        			   WebElement searchBox2=null;
        			   WebElement boutonOK=null;
        			   WebElement didYouMeanHtml=null;
        			   if(bool==false) 
        			   {
        				   this.tearDown();
        				   this.setUp();
        			   }
        			   bool=true;
        			   JavascriptExecutor myExecutor2 = (JavascriptExecutor) chromeDriver; 
        			   AgenciesNumberPerPage = 0;
        		       PageNumbers = 0;
        			   Resultats= 1;
            	       nbRecuperes_page= 0;
                       codpostSoumis= fic_valeurs[2];
                       System.out.println( codpostSoumis);
                       communeSoumise= fic_valeurs[1];
                       codpostSoumis= codpostSoumis.replaceAll(", FRANCE", "");
                       String test="";
        			   try
        			   {
        				   chromeDriver.get(url_accueil);
            	           Thread.sleep(2000);
            	           searchBox2 = chromeDriver.findElementByCssSelector("input.searchInput");
                           js=String.format("arguments[0].value='%s,FRANCE'", codpostSoumis);
                           System.out.println(js);
                           myExecutor2.executeScript(js, searchBox2);
                           boutonOK = chromeDriver.findElementByCssSelector("form[class=\"visaATMSearchWrap\"]>button[class=\"visaATMSubmit\"]");
                           if (boutonOK!= null) 
                           {
                        	   js=String.format("arguments[0].click()", "");
                        	   myExecutor2.executeScript(js, boutonOK);
                           }
                           Thread.sleep(2000);
                           //chech if atm locator know the place or we just have to submit the zip code
                           didYouMeanHtml=chromeDriver.findElementByCssSelector("form[class=\"visaATMSearchWrap\"]> div[class=\"visaATMSearchDidYouMean\"]");
                           test=didYouMeanHtml.getAttribute("outerHTML");
        			   }
        			   catch(Exception e) 
        			   {
        				   if(e instanceof UnhandledAlertException) 
        				   {
        					  try 
        					  {
        						  String alertCibled=" Le serveur est momentanément indisponible";
        						  Alert alert=chromeDriver.switchTo().alert();
        						  String alertMessage=alert.getText();
        						  alert.accept();
        						  if( alertMessage.contains(alertCibled) ) 
                                  {
        							  this.tearDown();
        							  this.setUp();
        							  chromeDriver.get(url_accueil);
        	            	          Thread.sleep(2000);
        	            	          searchBox2 = chromeDriver.findElementByCssSelector("input.searchInput");
        	                          js=String.format("arguments[0].value='%s,FRANCE'", codpostSoumis);
        	                          System.out.println(js);
        	                          myExecutor2.executeScript(js, searchBox2);
        	                          boutonOK = chromeDriver.findElementByCssSelector("form[class=\"visaATMSearchWrap\"]>button[class=\"visaATMSubmit\"]");
        	                          if (boutonOK!= null) 
        	                          {
        	                        	   js=String.format("arguments[0].click()", "");
        	                        	   myExecutor2.executeScript(js, boutonOK);
        	                          }
        	                          Thread.sleep(2000);
        	                          //chech if atm locator know the place or we just have to submit the zip code
        	                          didYouMeanHtml=chromeDriver.findElementByCssSelector("form[class=\"visaATMSearchWrap\"]> div[class=\"visaATMSearchDidYouMean\"]");
        	                          test=didYouMeanHtml.getAttribute("outerHTML");
        							  
        						  }
        						  System.out.println(alertMessage);
        						  
        					  }catch(Exception f) 
        					  {
        						  if(f instanceof NoAlertPresentException) 
        						  {
        							  f.printStackTrace();
        						  }
        					  }
        				   }
        			   }
        			  
                       
                       System.out.println(test);
                       if( test.contains("ul")) 
                       {
                    	   JavascriptExecutor myExecutor3 = (JavascriptExecutor) chromeDriver;
                    	   try
                    	   {
                    		   chromeDriver.get(url_accueil);
                	           Thread.sleep(2000); 
                	           
                	           searchBox2 = chromeDriver.findElementByCssSelector("input.searchInput");
                               js=String.format("arguments[0].value='%1$s %2$s,FRANCE'", codpostSoumis,communeSoumise);
                               System.out.println(js);
                               myExecutor3.executeScript(js, searchBox2);
                               boutonOK = chromeDriver.findElementByCssSelector("form[class=\"visaATMSearchWrap\"]>button[class=\"visaATMSubmit\"]");
                               if (boutonOK!= null) 
                               {
                            	   js=String.format("arguments[0].click()", "");
                            	   myExecutor2.executeScript(js, boutonOK);
                               }
                               bool=false;  
                               
                    	   }
                    	   catch(Exception e) 
                    	   {
                    		   if(e instanceof UnhandledAlertException ) 
                    		   {
                    			   try 
                    			   {
                    				    String alertCibled=" Le serveur est momentanément indisponible";
             						    Alert alert=chromeDriver.switchTo().alert();
             						    String alertMessage=alert.getText();
             						    alert.accept();
             						    if( alertMessage.contains(alertCibled) ) 
                                        {
             							    this.tearDown();
             							    this.setUp();
             							    chromeDriver.get(url_accueil);
                            	            Thread.sleep(2000); 
                            	           
                            	            searchBox2 = chromeDriver.findElementByCssSelector("input.searchInput");
                                            js=String.format("arguments[0].value='%1$s %2$s,FRANCE'", codpostSoumis,communeSoumise);
                                            System.out.println(js);
                                            myExecutor3.executeScript(js, searchBox2);
                                            boutonOK = chromeDriver.findElementByCssSelector("form[class=\"visaATMSearchWrap\"]>button[class=\"visaATMSubmit\"]");
                                            if (boutonOK!= null) 
                                            {
                                        	   js=String.format("arguments[0].click()", "");
                                        	   myExecutor2.executeScript(js, boutonOK);
                                            }
                                            bool=false;
                                           
                                        }
                    			   }
                    			   catch(Exception f) 
                    			   {
                    				   if(f instanceof NoAlertPresentException) 
             						   {
             							  f.printStackTrace();
             						   } 
                    			   }
                    		   }
                    	   }
                    	   
                        }
                        Thread.sleep(4000);
                        System.out.println("up");
                      
                        html=chromeDriver.getPageSource();
                        doc=Jsoup.parse(html);
                        Element resultHtmlElement= doc.select("div[class=\"visaATMResultsInfo\"]").first();
                       
                        if(resultHtmlElement!=null) 
                        {
                    	   System.out.println("set");
                    	   
                    	   Elements listOfResult=null;
                    	   Elements paginationHtmElement= resultHtmlElement.select("ol[class=\"visaATMPagination\"]>li>a[data-pagination]");
                    	   PageNumbers=paginationHtmElement.size();
                    	   System.out.println("le nombre de page est: "+ PageNumbers);
                    	   //fetch all information in page number one
                    	   listOfResult= resultHtmlElement.select("ol>li[class*=\"visaATMResultListItem\"]");
                    	   numpage=1;
                    	   this.fetchALLInfo( listOfResult);
                    	   System.out.println("++++++++++++++++++++++++++++++");
                    	   
                    	   //fetch all information in the rest of page 
                    	   for(int i=1; i<PageNumbers; i++) 
                    	   {
                    		   numpage=i+1;
                    		   JavascriptExecutor myExecutor = (JavascriptExecutor) chromeDriver;
                    		   try 
                    		   {
                    			   js=String.format("arguments[0].click()","");
                        		   WebElement nextPageElement= chromeDriver.findElementByCssSelector("a[data-pagination=\"next\"]");
                        		   myExecutor.executeScript(js,nextPageElement);
                        		   
                        	   }
                    		   catch(Exception e) 
                    		   {
                    			   if(e instanceof  UnhandledAlertException) 
                    			   {
                    				   try 
                    				   {
                    					    String alertCibled=" Le serveur est momentanément indisponible";
                						    Alert alert=chromeDriver.switchTo().alert();
                						    String alertMessage=alert.getText();
                						    alert.accept();
                						    if( alertMessage.contains(alertCibled) ) 
                                            {
                						       js=String.format("arguments[0].click()","");
                                      		   WebElement nextPageElement= chromeDriver.findElementByCssSelector("a[data-pagination=\"next\"]");
                                      		   myExecutor.executeScript(js,nextPageElement);
                                      		  
                                            }
                						    System.out.println(alertMessage);
                    				   }
                    				   catch(Exception f) 
                    				   {
                    					   if(f instanceof NoAlertPresentException) 
                 						   {
                 							  f.printStackTrace();
                 						   } 
                    				   }
                    			   }
                    		   }
                    		   Thread.sleep(4000);
                    		   html=chromeDriver.getPageSource();
                    		   doc=Jsoup.parse(html);
                    		   resultHtmlElement= doc.select("div[class=\"visaATMResultsInfo\"]").first();
                               listOfResult= resultHtmlElement.select("ol>li[class*=\"visaATMResultListItem\"]");
                               this.fetchALLInfo(listOfResult);
                    		   System.out.println("++++++++++++++++++++++++++++++");
                    	   }
                        }
                       log.writeLog(new Date(),communeSoumise,"no_error", AgenciesNumberPerPage, parcoursficval,0); 
            	
        		 } 
        		 parcoursficval++;
        		
            } 
            sc.close();
			return visaList;
        	
        //}
    	
    }
    
    public void fetchALLInfo( Elements listOfResult) 
    {
    	 AgenciesNumberPerPage=listOfResult.size();
    	int j=0;
    	String agencyHTM="",agencyName="",agencyVoie="",agencyZIP="",agencyCity="";
    	for(Element result: listOfResult) 
  	    { 
    		 j++; 
    		 nbRecuperes_page=j;
    		 String agencyAddress="";String [] agencyAddressArray=null,agencyAddressArray2=null;
    		//fetch agency Htm
    		 agencyHTM= result.html().replaceAll("\\s{1,}","").trim();
    		 
    		 //fetch agency name
    		 Element agencyNameHtmlElement= result.select("p[class=\"visaATMPlaceLink\"]").first();
    		 agencyName=(agencyNameHtmlElement!=null) ? agencyNameHtmlElement.text():"";
    		 System.out.println("name: "+j+" "+agencyName);
    		 
    		 //fetch agency Addresse
    		 Element agencyAddresseHtmlElement= result.select("p[class=\"visaATMAddress\"]").first();
    		 agencyAddress=(agencyAddresseHtmlElement!=null)?agencyAddresseHtmlElement.html():"";//replaceAll(";" , ","):"";
    		 agencyAddressArray= agencyAddress.split("<br>");
    		 agencyVoie=(agencyAddressArray[0]!=null)? agencyAddressArray[0].replaceAll("&nbsp;",""):"";
    		 if( agencyAddressArray[1].contains("&nbsp;")) {
    			 agencyAddressArray[1]=agencyAddressArray[1].replaceAll("&nbsp;","");
    		 }
    		 System.out.println("juste pour verifié l'adresse: "+agencyAddressArray[1]);
    		 agencyAddressArray2=agencyAddressArray[1].split(",");
    		 agencyCity=(agencyAddressArray2[0]!=null)? agencyAddressArray2[0].replaceAll("<br>",""):"";
    		 agencyZIP=(agencyAddressArray2[1]!=null )? agencyAddressArray2[1]:"";
    		 System.out.println(agencyVoie+" "+ agencyCity+" "+agencyZIP);
    		 this.enregistrer( agencyHTM, agencyName, agencyVoie,  agencyZIP, agencyCity);
    		 this.enregistrer_journal ();
    		 visaList.add(this.setData( codpostSoumis,String.valueOf( PageNumbers),String.valueOf(numpage), 
    				                    String.valueOf(AgenciesNumberPerPage),agencyName,agencyVoie, 
    				                    agencyZIP, agencyCity, String.valueOf(DAB), agencyHTM));
    	}
    
    }
    
    public void enregistrer (
    		String agences_HTM,
            String nom_agence,
            String voie,
            String CP,
            String ville)  {

//    	this.fichier = new File(Parametres.REPERTOIRE + "RESULTATS_BNP.");
    	try {
    	
    		sortie = new BufferedWriter(new FileWriter(fichier,true));
    		sortie.write(	  codpostSoumis + "\t" 
    						+ PageNumbers + "\t" 
    						+ numpage + "\t" 
    						+ AgenciesNumberPerPage + "\t" 
    						+ nom_agence + "\t"
    						+ voie + "\t"
    						+ CP + "\t"
    						+ ville + "\t"
    						+ DAB + "\t"
    						+ agences_HTM + "\r\n");
        
    	
    		sortie.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
}
    
    public void enregistrer_journal ()  
    {


    	try {
    		Temps_Fin = new Date();
    		SimpleDateFormat formater = null;
    		formater = new SimpleDateFormat("'le' dd/MM/yyyy '�' hh:mm:ss");
    		String date =  formater.format(Temps_Fin);
    		date =  formater.format(Temps_Fin);
    		
    		sortie_log = new BufferedWriter(new FileWriter(log,true));
    		sortie_log.write(parcoursficval + ";"
    						+"FinValeur" + ";"
							+ codpostSoumis + ";" 
							+ this.numpage+ ";" 
							+ this.AgenciesNumberPerPage + ";" 
    						+ nbRecuperes_page + ";" 
    						+ (this.AgenciesNumberPerPage - nbRecuperes_page) + ";" 
    						+ date  + "\r\n"   );
    		sortie_log.close();
    		long  taille_fichier = fichier.length()/1000000; //la length est en octets, on divise par 1000 pour mettre en Ko et par 1000000 pour mettre en Mo
    		
    		if(taille_fichier> 8) {
    			//INITIALISATION FICHIER  DE RESULTAT
    	    	this.fichier = new File(dossierRESU + "\\"+ "RESULTATS_" + nomSITE +   "_" + System.currentTimeMillis() + ".txt");
    	    	//Attention pas d'accent dans les noms de colonnes !
            	sortie = new BufferedWriter(new FileWriter(fichier,true));
            	sortie.write("codpostSoumis" + "\t" 
            			+ "NbPages"+ "\t" 
            			+ "numPage"+ "\t" 
    					+ "NbAgences_page" + "\t" 
    					+ "NOMWEB" + "\t"
    					+ "VOIE" + "\t"
    					+ "CP" + "\t"
    					+ "VILLE" + "\t"
    					+ "DAB" + "\t"
    					+ "AGENCEHTM"+ "\r\n");
            	sortie.close();
    		}
    		
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
}
    
    /**
     * La m�thode <code>initialiser()</code> permet d'ajouter la ligne d'ent�te dans le fichier r�sultat.
     */
    
   
    public void initialiser() {
        
        try {
        	//Attention pas d'accent dans les noms de colonnes !
        	sortie = new BufferedWriter(new FileWriter(fichier,true));
        	sortie = new BufferedWriter(new FileWriter(fichier,true));
        	sortie.write("codpostSoumis" + "\t" 
        			+ "NbPages"+ "\t" 
        			+ "numPage"+ "\t" 
					+ "NbAgences_page" + "\t" 
					+ "NOMWEB" + "\t"
					+ "VOIE" + "\t"
					+ "CP" + "\t"
					+ "VILLE" + "\t"
					+ "DAB" + "\t"
					+ "AGENCEHTM"+ "\r\n");
        	sortie.close();
        	sortie.close();
        	
        
            if( !log.exists() ){
        	//if (num_ligneficval == num_ligne) {
	        	sortie_log = new BufferedWriter(new FileWriter(log,true));
	        	sortie_log.write("NumLigne" + ";" +"FinValeur" + ";" + "codpostSoumis" + ";" + "numpage" + ";" + "NbAgences_page" + ";"  + "nbRecuperes_page" + ";" + "Reste" + ";" + "Heure_Fin"+ "\r\n" );
	        	sortie_log.close();
        	}
        	
		} catch (IOException e) {
			
			e.printStackTrace();
		}
    }
    
   
    public void tearDown() {
        chromeDriver.close();
    	chromeDriver.quit();
        
    }
    
    public static boolean isAlertPresent(FirefoxDriver wd) {
        try {
            wd.switchTo().alert();
            return true;
        } catch (NoAlertPresentException e) {
            return false;
        }
    }

}
