package com.app.service;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.net.SocketTimeoutException;
import java.util.Date;
import java.util.Scanner;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.chrome.ChromeDriver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.app.model.Orias;
import com.app.scrap.RepOrias;
import com.app.utils.Parametres;

import fa.log.LogApp.LogScrap;
import fa.log.config.LogConst;

@Service
public class ServiceOrias extends Scraper {

	@Autowired
	RepOrias rep;

	private ChromeDriver ch;

	private String url = "https://www.orias.fr/";

	private String ficval = "numORIAS_relance2.txt ";

	public void setup() {
		ch = (ChromeDriver) this.setupCh(Parametres.CHROME_DRIVER_PATH,false,false,"",null);

	}

	@Override
	public void saveItem(String... args) {
		Orias orias = new Orias();
		orias.setAgent(args[0]).setAgentDateInscription(args[1]).setAgentDateSuppr(args[2]).setAGTPSI(args[3])
				.setAGTPSIDateInsript(args[4]).setAGTPSIDateSuppr(args[5]).setALPSI(args[6])
				.setAlpsiDateInsript(args[7]).setAlpsiDateSuppr(args[8]).setCIF(args[9]).setCifDateInsript(args[10])
				.setCifDateSuppr(args[11]).setCIP(args[12]).setCipDateInsript(args[13]).setCipDateSuppr(args[14])
				.setCOA_encaissementFond(args[15]).setCOA_inscritOrsuppr(args[16]).setCOBSP(args[17])
				.setCobspDateInsript(args[18]).setCobspDateSuppr(args[19]).setCourtierDateInsript(args[20])
				.setCourtierDateSuppr(args[21]).setCoutier(args[22]).setIaEnciassementFond(args[23])
				.setIaInscritOrSuppr(args[24]).setIEA(args[25]).setIEA_encaissementFond(args[26])
				.setIEADateInsript(args[27]).setIEADateSuppr(args[28]).setIEAinscritOrsuppr(args[29]).setIFP(args[30])
				.setIfpDateInsript(args[31]).setIfpDateSuppr(args[32]).setMAL(args[33]).setMalDateInsript(args[34])
				.setMalDateSuppr(args[35]).setMandataire(args[36]).setMandataire_Date_Suppr(args[37])
				.setMandataire_encaissementFond(args[38]).setMandataireDateInsript(args[39])
				.setMandataireinscritOrsuppr(args[40]).setMandatIA(args[41]).setMIA(args[42])
				.setMIA_encaissementFond(args[43]).setMiaDateInsript(args[44]).setMiaDateSuppr(args[45])
				.setMIAinscritOrsuppr(args[46]).setMIOBSP(args[47]).setMiobspDateInsript(args[48])
				.setMiobspDateSuppr(args[49]).setMOBSP(args[50]).setMobspDateInsript(args[51])
				.setMobspDateSuppr(args[52]).setMOBSPL(args[53]).setMobsplDateInsript(args[54])
				.setMobsplDateSuppr(args[55]).setNatureActiviteIA(args[56]).setNomAgentIA(args[57])
				.setDenominationSocial(args[58]).setNomCommercial(args[59]).setRCS(args[60]).setSiren(args[61])
				.setCodeNAF(args[62]).setNumImmatr(args[63]).setStatut(args[64]).setFormeJuridique(args[65])
				.setAdresse(args[66]).setReseauORIAS1(args[67]).setReseauORIAS2(args[68]).setReseauORIAS3(args[69])
				.setReseauORIAS4(args[70]).setReseauORIAS5(args[71]).setReseauORIAS6(args[72]).setReseauORIAS7(args[73]);
		rep.save(orias);

	}

	private String getNomIA() {
		String sourcePage = ch.getPageSource();
		Document doc = Jsoup.parse(sourcePage);
		Element list = null;
		list = doc.select("#row").first();
		if (list != null) {
			Element denomination = list.select("tbody > tr:nth-child(1) > td:nth-child(3) > a").first();
			return denomination.text().trim();
		} else {
			return "";
		}
	}

	private String[] getInfoAboutRadiation(Element dateElement, String suppr) {
		String dateSuppression = "", dateInscription = "", inscripOrsuppr = "";
		if (suppr.equals("1")) {
			dateSuppression = dateElement.text();
			inscripOrsuppr = "SUPPRIME";
			System.out.println("date de suppr: " + dateSuppression);
		} else {
			dateInscription = dateElement.text();
			inscripOrsuppr = "INSCRIT";
			System.out.println("date de inscr: " + dateInscription);
		}
		String[] response = { dateSuppression, dateInscription, inscripOrsuppr };
		return response;
	}

	@Override
	public void scrap() throws Exception {

		Scanner sc = new Scanner(
				new BufferedInputStream(new FileInputStream(new File(Parametres.REPERTOIRE_DATA_SOURCES + ficval))));
		LogScrap log = new LogScrap("Orias.txt", LogConst.TYPE_LOG_FILE);
		int num_ligneficval = ((new File(LogConst.STORE_LOG_PATH + LogConst.PREFIX_LOG_FILE + "_Orias.txt")).exists())
				? Integer.parseInt(log.readLog().getLastLineProcessed().split("\\t")[4])
				: 1;
		int currentLine = 1;
		while (sc.hasNextLine()) {
			String line = sc.nextLine();
			if (currentLine > num_ligneficval) {
				try {
					ch.get(url);
				} catch (Exception e) {
					if (e instanceof SocketTimeoutException) {
						this.setup();
						ch.get(url);
					}
				}
				System.out.println("num orias"+ line);
				JavascriptExecutor js = (JavascriptExecutor) ch;
				String jsScript = String.format(
						"let inputFields=document.querySelector(\"#searchInput\"); let sub=document.querySelector(\"#searchBean > div > input.btn.btn-primary\") ; if(inputFields!=null){inputFields.value='%s'; sub.click()}",
						line);
				System.out.println(jsScript);
				js.executeScript(jsScript);
				jsScript = String.format(
						"let statee=''; let tablee=document.querySelector(\"#row\"); if(tablee!=null){ statee='visible'} else{statee='hidden'} return statee",
						"");
				String str = (String) js.executeScript(jsScript);
				if (str.equals("visible")) {
				
					Document doc = Jsoup.parse(ch.getPageSource());
					Element oriasNumElement = doc.selectFirst("#row > tbody > tr > td:nth-child(2)");
					String orias = (oriasNumElement != null) ? oriasNumElement.text().replaceAll("\\s{1,}", "") : "";
					if (orias.equals(line)) {
						jsScript = String.format(
								"document.querySelector(\"#row > tbody > tr > td:nth-child(7) > a > img\").click()");
						js.executeScript(jsScript);
						Thread.sleep(3000);
						doc = Jsoup.parse(ch.getPageSource());
						Element resultBlock = doc.selectFirst("#mainint");
						String AGENT = "0", COURTIER = "0", MIA = "0", MAL = "0", MANDATAIRE = "0", COBSP = "0",
								MOBSPL = "0", MOBSP = "0", MIOBSP = "0", CIF = "0", ALPSI = "0", CIP = "0", IFP = "0",
								radie = "0", suppr = "0", IEA = "0", AGTPSI = "0", denominationSocial = "",
								nomCommercial = "", RCS = "", siren = "", codeNAF = "", numImmatr = "", statut = "",
								formeJuridique = "", adresse = "", typeOfAgent = "", agentDateSuppr = "",
								agentDateInsript = "", courtierDateSuppr = "", courtierDateInsript = "",
								miaDateSuppr = "", miaDateInsript = "", malDateSuppr = "", malDateInsript = "",
								mandataireDateSuppr = "", mandataireDateInsript = "", cobspDateSuppr = "",
								cobspDateInsript = "", mobsplDateSuppr = "", mobsplDateInsript = "",
								mobspDateSuppr = "", mobspDateInsript = "", miobspDateSuppr = "",
								miobspDateInsript = "", cifDateSuppr = "", cifDateInsript = "", alpsiDateSuppr = "",
								alpsiDateInsript = "", cipDateSuppr = "", cipDateInsript = "", ifpDateSuppr = "",
								ifpDateInsript = "", natureActiviteIA = "", nomAgentIA = "", IA_enciassementFond = "",
								mandantIA = "", IAinscritOrsuppr = "", reseauORIAS1 = "", reseauORIAS2 = "",
								reseauORIAS3 = "", reseauORIAS4 = "", reseauORIAS5 = "", reseauORIAS6 = "",
								reseauORIAS7 = "", COA_encaissementFond = "", COA_inscritOrsuppr = "",
								MANDATAIRE_encaissementFond = "", MANDATAIREinscritOrsuppr = "",
								MIA_encaissementFond = "", MIAinscritOrsuppr = "", IEADateSuppr = "",
								IEADateInsript = "", IEA_encaissementFond = "", IEAinscritOrsuppr = "",
								AGTPSIDateSuppr = "", AGTPSIDateInsript = "";
					
						if (resultBlock != null) {
							Elements resultContainers = resultBlock.select(".row-fluid");
							int e = 0;
							for (Element resultContainer : resultContainers) {
								e++;
								switch (e) {
								case 1: {
									Elements contactInformations = resultContainer
											.select("div[class*=\"roundedBox\"]> dl[class*=\"dl-horizontal\"]");
									int x = 0;
									for (Element contactInformation : contactInformations) {
										x++;
										Elements dd = contactInformation.select("dd");
										if (x == 1) {
											if (contactInformation.outerHtml().contains("Dénomination sociale")) {
												int y = 0;

												for (Element tmp : dd) {
													y++;
													String value = tmp.text();
													switch (y) {
													case 1: {
														denominationSocial = value;

														System.out.println("denomination social " + denominationSocial);
														break;
													}
													case 2: {
														nomCommercial = value;

														System.out.println("nom Commercial " + nomCommercial);
														break;

													}
													case 3: {
														RCS = value;
														;
														System.out.println("RCS " + RCS);
														break;
													}
													case 4: {
														siren = value;

														System.out.println("siren " + siren);
														break;
													}
													case 5: {
														codeNAF = value;

														System.out.println("codeNAF " + codeNAF);
														break;
													}
													}
												}
											} else {
												nomAgentIA = "";
												int y = 0;
												String prenom = "", nom = "";
												for (Element tmp : dd) {
													y++;
													String value = tmp.text();
													switch (y) {
													case 1: {

														nom = value;
														break;
													}
													case 2: {
														prenom = value;
														break;
													}
													}
												}
												nomAgentIA = nom + " " + prenom;

												System.out.println("le nom de l'agent est " + nomAgentIA);
											}
										} else {

											if (contactInformation.outerHtml().contains("Siren")) {
												int y = 0;
												for (Element tmp : dd) {
													y++;
													String value = tmp.text();
													switch (y) {
													case 1: {
														numImmatr = value;

														System.out.println("numero matricule " + numImmatr);
														break;
													}
													case 2: {
														RCS = value;

														System.out.println("RCS " + RCS);
														break;
													}
													case 3: {
														siren = value;

														System.out.println("siren " + siren);
														break;
													}
													case 4: {
														statut = value;

														System.out.println("statut " + statut);
														break;
													}
													case 5: {
														formeJuridique = value;

														System.out.println("forme juridique " + formeJuridique);
														break;
													}
													case 6: {
														codeNAF = value;

														System.out.println("codeNAF " + codeNAF);
														break;
													}
													}
												}
											} else {
												int y = 0;
												for (Element tmp : dd) {
													y++;
													String value = tmp.text();
													switch (y) {
													case 1: {
														numImmatr = value;

														System.out.println("numero matricule " + numImmatr);
														break;
													}
													case 2: {
														statut = value;

														System.out.println("statut " + statut);
														break;
													}
													case 3: {
														formeJuridique = value;

														System.out.println("forme juridique " + formeJuridique);
														break;
													}
													}
												}
											}
										}
									}
									break;
								}
								case 3: {
									Element addressElment = resultContainer.select("div[class*=\"roundedBox\"]")
											.first();
									adresse = addressElment.text().replaceAll("Adresse", "");

									System.out.println("adresse " + adresse);
									break;
								}
								case 5: {
									Element typeOfAgentElement = resultContainer.select("div[class*=\"roundedBox\"]")
											.first();
									typeOfAgent = typeOfAgentElement.text();

									radie = (typeOfAgent.contains("Cet intermédiaire est radié")) ? "1" : "0";

									System.out.println("radie " + radie);

									String tmps[] = null;
									Elements agentTypeContainers = typeOfAgentElement.select(".row-fluid");

									for (Element agentTypeContainer : agentTypeContainers) {
										Elements spans = agentTypeContainer.select("div[class^=\"span6\"]");
										Element natureActiviteAgentElmnt = typeOfAgentElement
												.select("h2:contains(Intermédiaire en assurance)~p").first();
										if (natureActiviteAgentElmnt != null) {
											natureActiviteIA = natureActiviteAgentElmnt.text();

											System.out.println(
													"la nature de l'activite de l'agent est: " + natureActiviteIA);

										}
										if (nomAgentIA.equals("")) {
											Element nomAgentIAElmnt = typeOfAgentElement
													.select("h2:contains(Intermédiaire en assurance)").first();
											if (nomAgentIAElmnt != null) {
												nomAgentIA = nomAgentIAElmnt.text()
														.replaceAll("Intermédiaire en assurance \\(|\\)", "");

											}
										}

										for (Element span : spans) {
											typeOfAgent = "";
											Element header = span.select("div>div.header").first();
											Element dateElement = span.select(
													"div.blocint3>dl.dl-horizontal.wide>dt:containsOwn(Date)~dd")
													.first();
											Element encaissemtnFond = span
													.select("div.blocint3>dl.dl-horizontal.wide>dd:nth-child(4)")
													.first();
											typeOfAgent = header.text();
											suppr = (typeOfAgent.contains("Supprimé")) ? "1" : "0";

											if (typeOfAgent.contains("(AGA)")) {

												AGENT = "1";

												System.out.println("AGENT " + AGENT);

												Elements mandantsAgents = span
														.select("#reg1 > table > tbody > tr > td:nth-child(3) > a");
												if (mandantsAgents != null) {
													int j = 0;
													tmps = new String[mandantsAgents.size()];
													for (Element mandantAgent : mandantsAgents) {
														tmps[j] = mandantAgent.text();
														j++;
													}

													String tmp0 = "";
													for (String tmp : tmps) {

														tmp0 += tmp + " ";
													}
													if (mandantIA.equals("")) {
														mandantIA = tmp0.trim();
													} else {
														mandantIA += " " + tmp0.trim();
													}
													System.out
															.println("les mandants de l'agent generale assurance est :"
																	+ mandantIA);

												}

												if (encaissemtnFond != null) {
													IA_enciassementFond = encaissemtnFond.text();

													System.out
															.println("IA encaissement de fond: " + IA_enciassementFond);
												}

												agentDateSuppr = this.getInfoAboutRadiation(dateElement, suppr)[0];

												agentDateInsript = this.getInfoAboutRadiation(dateElement, suppr)[1];
												;

												IAinscritOrsuppr = this.getInfoAboutRadiation(dateElement, suppr)[2];

											} /*
												 * else { Controler.Agent=AGENT; Controler.mandantIA=mandantIA;
												 * Controler.IA_enciassementFond=IA_enciassementFond;
												 * Controler.agentDateSuppr=agentDateSuppr;
												 * Controler.agentDateInsript=agentDateInsript;
												 * Controler.IAinscritOrsuppr=IAinscritOrsuppr;
												 * Controler.natureActiviteIA=natureActiviteIA; }
												 */

											if (typeOfAgent.contains("(COA)")) {

												COURTIER = "1";

												System.out.println("COURTIER " + COURTIER);

												if (encaissemtnFond != null) {
													COA_encaissementFond = encaissemtnFond.text();

													System.out.println(
															"COA encaissement de fond " + COA_encaissementFond);
												}

												courtierDateSuppr = this.getInfoAboutRadiation(dateElement, suppr)[0];

												courtierDateInsript = this.getInfoAboutRadiation(dateElement, suppr)[1];

												COA_inscritOrsuppr = this.getInfoAboutRadiation(dateElement, suppr)[2];

											} /*
												 * else { Controler.Courtier=COURTIER;
												 * Controler.COA_encaissementFond=COA_encaissementFond;
												 * Controler.courtierDateSuppr=courtierDateSuppr;
												 * Controler.courtierDateInsript=courtierDateInsript;
												 * Controler.COA_inscritOrsuppr=COA_inscritOrsuppr; }
												 */

											if (typeOfAgent.contains("(MA)")) {

												MANDATAIRE = "1";

												System.out.println("mandataire " + MANDATAIRE);

												if (encaissemtnFond != null) {
													MANDATAIRE_encaissementFond = encaissemtnFond.text();

													System.out.println("Mandataire encaissement de fond "
															+ MANDATAIRE_encaissementFond);
												}
												Elements mandantsAgents = span
														.select("#reg1 > table > tbody > tr > td:nth-child(3) > a");

												if (mandantsAgents != null) {
													int j = 0;
													tmps = new String[mandantsAgents.size()];
													for (Element mandantAgent : mandantsAgents) {
														tmps[j] = mandantAgent.text();
														j++;
													}

													String tmp0 = "";
													for (String tmp : tmps) {

														tmp0 += tmp + " ";
													}

													if (mandantIA.equals("")) {

														mandantIA = tmp0.trim();
													} else {

														mandantIA += " " + tmp0.trim();
													}

													System.out
															.println("les mandants de l'agent generale assurance est :"
																	+ mandantIA);

												}

												mandataireDateSuppr = this.getInfoAboutRadiation(dateElement, suppr)[0];

												mandataireDateInsript = this.getInfoAboutRadiation(dateElement,
														suppr)[1];

												MANDATAIREinscritOrsuppr = this.getInfoAboutRadiation(dateElement,
														suppr)[2];

											} /*
												 * else { Controler.Mandataire=MANDATAIRE;
												 * Controler.mandataireDateSuppr=mandataireDateSuppr;
												 * Controler.mandataireDateInsript=mandataireDateInsript;
												 * Controler.MANDATAIREinscritOrsuppr=MANDATAIREinscritOrsuppr; }
												 */

											if (typeOfAgent.contains("(MIA)")) {
												MIA = "1";

												System.out.println("MIA " + MIA);
												Elements mandantsAgents = span
														.select("#reg1 > table > tbody > tr > td:nth-child(3) > a");
												if (mandantsAgents != null) {
													int j = 0;
													tmps = new String[mandantsAgents.size()];
													for (Element mandantAgent : mandantsAgents) {
														tmps[j] = mandantAgent.text();
														j++;
													}

													String tmp0 = "";
													for (String tmp : tmps) {

														tmp0 += tmp + " ";
													}
													if (mandantIA.equals("")) {
														mandantIA = tmp0.trim();
													} else {
														mandantIA += " " + tmp0.trim();
													}
													System.out
															.println("les mandants de l'agent generale assurance est :"
																	+ mandantIA);

												}
												if (encaissemtnFond != null) {
													MIA_encaissementFond = encaissemtnFond.text();

													System.out.println("MIA encaissement " + MIA_encaissementFond);
												}

												miaDateSuppr = this.getInfoAboutRadiation(dateElement, suppr)[0];

												miaDateInsript = this.getInfoAboutRadiation(dateElement, suppr)[1];

												MIAinscritOrsuppr = this.getInfoAboutRadiation(dateElement, suppr)[2];

											} /*
												 * else{ Controler.MIA=MIA;
												 * Controler.MIA_encaissementFond=MIA_encaissementFond;
												 * Controler.miaDateSuppr=miaDateSuppr;
												 * Controler.miaDateInsript=miaDateInsript;
												 * Controler.MIAinscritOrsuppr=MIAinscritOrsuppr;
												 * 
												 * }
												 */

											if (typeOfAgent.contains("(MAL)")) {
												MAL = "1";

												System.out.println("MAL " + MAL);

												malDateInsript = this.getInfoAboutRadiation(dateElement, suppr)[1];
												malDateSuppr = this.getInfoAboutRadiation(dateElement, suppr)[0];

											} /*
												 * else { Controler.MAL=MAL; Controler.malDateInsript=malDateInsript;
												 * Controler.malDateSuppr=malDateSuppr; }
												 */

											if (typeOfAgent.contains("(COBSP)")) {
												COBSP = "1";

												System.out.println("COBSP " + COBSP);

												cobspDateInsript = this.getInfoAboutRadiation(dateElement, suppr)[1];
												cobspDateSuppr = this.getInfoAboutRadiation(dateElement, suppr)[0];

											} /*
												 * else { Controler.COBSP=COBSP;
												 * Controler.cobspDateInsript=cobspDateInsript;
												 * Controler.cobspDateSuppr=cobspDateSuppr; }
												 */

											if (typeOfAgent.contains("(MOBSPL)")) {
												MOBSPL = "1";

												System.out.println("MOBSPL " + MOBSPL);

												mobsplDateInsript = this.getInfoAboutRadiation(dateElement, suppr)[1];

												mobsplDateSuppr = this.getInfoAboutRadiation(dateElement, suppr)[0];

											} /*
												 * else{ Controler.MOBSPL=MOBSPL;
												 * Controler.mobspDateInsript=mobsplDateInsript;
												 * Controler.mobsplDateSuppr=mobsplDateSuppr; }
												 */

											if (typeOfAgent.contains("(MOBSPL)")) {
												MOBSPL = "1";

												System.out.println("MOBSPL " + MOBSPL);

												mobsplDateSuppr = this.getInfoAboutRadiation(dateElement, suppr)[0];

												mobsplDateInsript = this.getInfoAboutRadiation(dateElement, suppr)[1];

											} /*
												 * else { Controler.MOBSPL=MOBSPL;
												 * Controler.mobsplDateSuppr=mobsplDateSuppr;
												 * Controler.mobsplDateInsript=mobsplDateInsript; }
												 */

											if (typeOfAgent.contains("(MOBSP)")) {
												MOBSP = "1";

												System.out.println("MOBSP " + MOBSP);

												mobspDateInsript = this.getInfoAboutRadiation(dateElement, suppr)[1];

												mobspDateSuppr = this.getInfoAboutRadiation(dateElement, suppr)[0];

											} /*
												 * else { Controler.MOBSP= MOBSP;
												 * Controler.mobspDateInsript=mobspDateInsript;
												 * Controler.mobspDateSuppr=mobspDateSuppr; }
												 */

											// IOB
											if (typeOfAgent.contains("(MIOBSP)")) {
												MIOBSP = "1";

												System.out.println("MIOBSP " + MIOBSP);

												miobspDateInsript = this.getInfoAboutRadiation(dateElement, suppr)[1];
												miobspDateSuppr = this.getInfoAboutRadiation(dateElement, suppr)[0];

											} /*
												 * else { Controler.MIOBSP=MIOBSP;
												 * Controler.miobspDateInsript=miobspDateInsript;
												 * Controler.miobspDateSuppr=miobspDateSuppr; }
												 */

											if (typeOfAgent.contains("(CIF)")) {
												CIF = "1";

												System.out.println("CIF " + CIF);

												cifDateInsript = this.getInfoAboutRadiation(dateElement, suppr)[1];

												cifDateSuppr = this.getInfoAboutRadiation(dateElement, suppr)[0];

											} /*
												 * else { Controler.CIF=CIF; Controler.cifDateInsript=cifDateInsript;
												 * Controler.cifDateSuppr=cifDateSuppr; }
												 */

											if (typeOfAgent.contains("(ALPSI)")) {
												ALPSI = "1";

												System.out.println("ALPSI " + ALPSI);

												alpsiDateInsript = this.getInfoAboutRadiation(dateElement, suppr)[1];

												alpsiDateSuppr = this.getInfoAboutRadiation(dateElement, suppr)[0];
												;
											} /*
												 * else{ Controler.ALPSI=ALPSI;
												 * Controler.alpsiDateInsript=alpsiDateInsript;
												 * Controler.alpsiDateSuppr=alpsiDateSuppr; }
												 */

											if (typeOfAgent.contains("(CIP)")) {
												CIP = "1";

												System.out.println("CIP " + CIP);

												cipDateInsript = this.getInfoAboutRadiation(dateElement, suppr)[1];

												cipDateSuppr = this.getInfoAboutRadiation(dateElement, suppr)[0];

											} /*
												 * else { Controler.CIP=CIP; Controler.cipDateInsript=cipDateInsript;
												 * Controler.cipDateSuppr=cipDateSuppr; }
												 */

											if (typeOfAgent.contains("(IFP)")) {
												IFP = "1";

												System.out.println("IFP " + IFP);

												ifpDateInsript = this.getInfoAboutRadiation(dateElement, suppr)[1];

												ifpDateSuppr = this.getInfoAboutRadiation(dateElement, suppr)[0];

											} /*
												 * else { Controler.IFP=IFP; Controler.ifpDateInsript=ifpDateInsript;
												 * Controler.ifpDateSuppr=ifpDateSuppr; }
												 */

											if (typeOfAgent.contains("(IAE)")) {
												IEA = "1";

												System.out.println("IEA " + IEA);

												if (encaissemtnFond != null) {
													IEA_encaissementFond = encaissemtnFond.text();

													System.out.println(
															"IEA encaissement de fond " + IEA_encaissementFond);
												}

												IEADateInsript = this.getInfoAboutRadiation(dateElement, suppr)[1];

												IEADateSuppr = this.getInfoAboutRadiation(dateElement, suppr)[0];

												IEAinscritOrsuppr = this.getInfoAboutRadiation(dateElement, suppr)[2];

											} /*
												 * else { Controler.IEA=IEA;
												 * Controler.IEA_encaissementFond=IEA_encaissementFond;
												 * Controler.IEADateInsript=IEADateInsript;
												 * Controler.IEADateSuppr=IEADateSuppr;
												 * Controler.IEAinscritOrsuppr=IEAinscritOrsuppr; }
												 */

											if (typeOfAgent.contains("(AGTPSI)")) {
												AGTPSI = "1";

												System.out.println("AGTPSI " + AGTPSI);

												AGTPSIDateInsript = this.getInfoAboutRadiation(dateElement, suppr)[1];

												AGTPSIDateSuppr = this.getInfoAboutRadiation(dateElement, suppr)[0];

											} /*
												 * else { Controler.AGTPSI=AGTPSI;
												 * Controler.AGTPSIDateInsript=AGTPSIDateInsript;
												 * Controler.AGTPSIDateSuppr=AGTPSIDateSuppr; }
												 */
											if (tmps != null) {
												int strsSize = tmps.length;
												for (int j = 0; j < strsSize; j++) {
													switch (j) {
													case 0: {

														reseauORIAS1 = tmps[j];

														break;
													}
													case 1: {

														reseauORIAS2 = tmps[j];

														break;
													}
													case 2: {

														reseauORIAS3 = tmps[j];

														break;
													}
													case 3: {

														reseauORIAS4 = tmps[j];

														break;
													}
													case 4: {

														reseauORIAS5 = tmps[j];

														break;
													}
													case 5: {

														reseauORIAS6 = tmps[j];

														break;
													}
													case 6: {

														reseauORIAS7 = tmps[j];

														break;
													}
													}
												}

											}

										}
									}

									break;
								}
								}
							}
							this.saveItem(AGENT, agentDateInsript, agentDateSuppr, AGTPSI, AGTPSIDateInsript,
									AGTPSIDateSuppr, ALPSI, alpsiDateInsript, alpsiDateSuppr, CIF, cifDateInsript,
									cifDateSuppr,CIP,cipDateInsript,cipDateSuppr,COA_encaissementFond,COA_inscritOrsuppr,
									COBSP,cobspDateInsript,cobspDateSuppr, courtierDateInsript, courtierDateSuppr, COURTIER, 
									IA_enciassementFond,IAinscritOrsuppr, IEA,IEA_encaissementFond,IEADateInsript, IEADateSuppr,IEAinscritOrsuppr,
									IFP, ifpDateInsript,ifpDateSuppr, MAL, malDateInsript,malDateSuppr,MANDATAIRE,mandataireDateSuppr,
									MANDATAIRE_encaissementFond,mandataireDateInsript,MANDATAIREinscritOrsuppr,mandantIA,MIA, MIA_encaissementFond, 
									miaDateInsript,miaDateSuppr, MIAinscritOrsuppr, MIOBSP, miobspDateInsript, miobspDateSuppr,MOBSP,mobspDateInsript,
									mobspDateSuppr,MOBSPL, mobsplDateInsript,mobsplDateSuppr, natureActiviteIA,nomAgentIA,denominationSocial,nomCommercial,
									RCS,siren,codeNAF,numImmatr,statut,formeJuridique,adresse,reseauORIAS1,reseauORIAS2,reseauORIAS3,reseauORIAS4,
									reseauORIAS5,reseauORIAS6,reseauORIAS7);
						} // fin block result
					}
				}
				log.writeLog(new Date(), line,"no_error",1, currentLine, 0);
			}
			currentLine++;

		}

	}

	public void quit() {
		ch.quit();
		
	}

}
