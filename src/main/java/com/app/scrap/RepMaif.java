package com.app.scrap;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.app.model.MAIF;

@Repository
public interface RepMaif extends JpaRepository<MAIF,Integer>,RepositoryCustom<MAIF> {

}
