package com.app.service;

import java.io.BufferedInputStream;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.net.URLEncoder;
import java.text.Normalizer;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.json.JSONException;
import org.json.JSONObject;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.openqa.selenium.chrome.ChromeDriver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.app.model.PJAssuranceCP;
import com.app.model.PJAssuranceDep;
import com.app.scrap.RepPJAssuCP;
import com.app.scrap.RepPJAssuDep;
import com.app.utils.Parametres;

import fa.log.LogApp.LogScrap;
import fa.log.config.LogConst;

@Service
public class ServicePJAssurance extends Scraper implements SuperClassPJ {

	@Autowired
	private RepPJAssuDep repDEP;

	@Autowired
	private RepPJAssuCP repCP;

	private boolean isDep = false;
	private ChromeDriver ch;

	private String ficvalCp = "Pj_rubogracp_CP.txt";
	private String ficvalDep = "Pj_rubOGRAdep_DEP.txt";

	@Override
	public void saveItem(String... args) {

		if (isDep) {
			PJAssuranceDep pjDep = new PJAssuranceDep();
			pjDep.setActiviteSoumise(args[0]).setDep(args[1]).setDenomination(args[2]).setAdresse(args[3])
					.setOrias(args[4]).setPrestation(args[5]).setClientele(args[6]).setTel(args[7]).setSiren(args[8])
					.setSiret(args[9]).setActivite(args[10]);
			//repDEP.save(pjDep);
		} else {
			PJAssuranceCP pjCp = new PJAssuranceCP();
			pjCp.setActiviteSoumise(args[0]).setCp(args[1]).setDenomination(args[2]).setAdresse(args[3])
					.setOrias(args[4]).setPrestation(args[5]).setClientele(args[6]).setTel(args[7]).setSiren(args[8])
					.setSiret(args[9]).setActivite(args[10]);
			//repCP.save(pjCp);
		}

	}

	public void setup() {
		
		final String chrome ="C:\\Program Files\\Google\\Chrome\\Application\\chrome.exe";
		final String chromeBeta = "C:\\Program Files\\Google\\Chrome Beta\\Application\\chrome.exe";
		
		ch = (ChromeDriver) this.setupCh(Parametres.CHROME_DRIVER_PATH, true, false,
				"C:\\Users\\GEOMADA PC4\\AppData\\Local\\Google\\Chrome Beta\\User Data", "",chromeBeta);
	
	}
	
	@Override
	public void scrap() throws Exception {
		String url = "";
		Scanner sc = null;
		if (isDep) {
			sc = new Scanner(new BufferedInputStream(
					new FileInputStream(new File(Parametres.REPERTOIRE_DATA_SOURCES + ficvalDep))));
			url = "https://www.pagesjaunes.fr/annuaire/departement/";
		} else {
			sc = new Scanner(new BufferedInputStream(
					new FileInputStream(new File(Parametres.REPERTOIRE_DATA_SOURCES + ficvalCp))));
			url = "https://www.pagesjaunes.fr/annuaire/chercherlespros?quoiqui=";
		}
		LogScrap log = new LogScrap("PJAssurance.txt", LogConst.TYPE_LOG_FILE);
		int lastLine = 1;
		if ((new File(LogConst.STORE_LOG_PATH + LogConst.PREFIX_LOG_FILE + "_PJAssurance.txt").exists())){
			Scanner sc3 = new Scanner(new BufferedInputStream(
					new FileInputStream(new File(LogConst.STORE_LOG_PATH + LogConst.PREFIX_LOG_FILE + "_PJAssurance.txt"))));
			while(sc3.hasNextLine()) {
				lastLine=Integer.parseInt(sc3.nextLine().split("\t")[4]);
			}
		}
		int currentLine = 1;
		while (sc.hasNextLine()) {
			String[] lines = sc.nextLine().split("\\t");
			if (currentLine > lastLine) {
				int b = 0;
				String fileNameStore=String.format("PJassuranceStore/linkStore_%s_%s.txt", lines[1],lines[0]);
				System.out.println(fileNameStore);
				if (new File(fileNameStore).exists()) {
					Scanner sc1 = new Scanner(new BufferedInputStream(
							new FileInputStream(new File(fileNameStore))));
					int j=1;
					while (sc1.hasNextLine()) {
						String l=sc1.nextLine().split(",")[5];
						if(j>1)
						 b = Integer.parseInt(l);
						j++;
					}
				}
				String finalURL = "";
				if (isDep)
					finalURL = url + lines[1] + "/" + lines[0];
				else
					finalURL = url + lines[0] + "&ou="
							+ URLEncoder.encode(lines[2].replaceAll("-[0-9]{2,3}", "") + "(" + lines[1] + ")", "UTF-8")
							+ "&univers=pagesjaunes&idOu=";
				System.out.println("finalURL : "+finalURL);
				ch.get(finalURL);
				String pageSource = Normalizer.normalize(ch.getPageSource().toLowerCase(), Normalizer.Form.NFD)
						.replaceAll("[^\\p{ASCII}]", "");
				if (isDep)
					if (this.noResponse(pageSource)) {
						finalURL = "https://www.pagesjaunes.fr/annuaire/chercherlespros?quoiqui=" + lines[0] + "&ou="
								+ lines[1] + "&univers=pagesjaunes&idOu=";
						ch.get(finalURL);
						pageSource = Normalizer.normalize(ch.getPageSource().toLowerCase(), Normalizer.Form.NFD)
								.replaceAll("[^\\p{ASCII}]", "");
					}
				pageSource = this.iscaptcha(pageSource, ch);
				int stat = 0;
				if (!this.noResponse(pageSource)) {

					String pageNumber = "";
					Document doc = Jsoup.parse(pageSource);
					// Element blockInfo = doc.select("#tris-container > form").first();
					Element elementPageNumber = doc.selectFirst("#sel-compteur");
					String pageNumberTmp = elementPageNumber.text();
					Pattern pattern = Pattern.compile("/ \\d{1,}");
					Matcher match = pattern.matcher(pageNumberTmp);
					while (match.find())
						pageNumber = match.group().replaceAll("/", " ").trim();
					if (b == 0) {
						if(!new File(fileNameStore).exists())
						     this.intFile("PJassuranceStore/",  "linkStore_"+lines[1]+"_"+lines[0]+".txt");
						
						//code postale : lines[1]
						//activites : lines[0]
						/*	
						System.out.println("lines[0] : " +lines[0]+"lines[1] : "+lines[1]);
						
						Element elemAdresse = doc.selectFirst("div.bi-clic-mobile > div > div > div > a");
						
						String addresse = elemAdresse.text();
						
						System.out.println("Adresse : "+addresse);
						*/
						
						this.retrieveLinks(doc, lines[0], lines[1], stat,fileNameStore);
						int i = 2;
						if (Integer.parseInt(pageNumber) > 1) {
							while (i <= Integer.valueOf(pageNumber)) {
								
								if (i <= Integer.valueOf(pageNumber))
									ch.findElementById("pagination-next").click();
							    
								if(i==Integer.valueOf(pageNumber))
									stat = 1;
								Thread.sleep(4000);
								pageSource = Normalizer.normalize(ch.getPageSource().toLowerCase(), Normalizer.Form.NFD)
										.replaceAll("[^\\p{ASCII}]", "");
                                pageSource = this.iscaptcha(pageSource, ch);
								doc = Jsoup.parse(pageSource);
								this.retrieveLinks(doc, lines[0], lines[1], stat,fileNameStore);

								
								elementPageNumber = doc.selectFirst("#sel-compteur");
								pageNumberTmp = (elementPageNumber!=null)? elementPageNumber.text():"";
								pattern = Pattern.compile("/ \\d{1,}");
								match = pattern.matcher(pageNumberTmp);
								while (match.find())
									pageNumber = match.group().replaceAll("/", " ").trim();
								if(this.noResponse(pageSource))
									break;
								System.out.println("num page "+i+" re-page number " + pageNumber);
								i++;
							}
						}
					}

					// make scrap
					LogScrap log2 = new LogScrap("PJAssuranceLog2"+lines[1]+"_"+lines[0]+".txt", LogConst.TYPE_LOG_FILE);
					int secondLastLine =1;
					String logStr= "_PJAssuranceLog2" + lines[1] + "_" + lines[0] + ".txt";
					if ((new File(LogConst.STORE_LOG_PATH + LogConst.PREFIX_LOG_FILE+logStr)).exists()) {
						Scanner sc3 = new Scanner(new BufferedInputStream(
								new FileInputStream(new File(LogConst.STORE_LOG_PATH + LogConst.PREFIX_LOG_FILE+logStr))));
						while(sc3.hasNextLine()) {
							secondLastLine=Integer.parseInt(sc3.nextLine().split("\t")[4]);
						}
					}
					System.out.println(secondLastLine);
					int secondCurrentLine = 1;
					Scanner sc1 = new Scanner(new BufferedInputStream(
							new FileInputStream(new File(fileNameStore))));
					while (sc1.hasNextLine()) {
						String secondLines[] = sc1.nextLine().split(",");
						
						if (secondCurrentLine > secondLastLine) {

							
							String finalUrl ="";
							if(secondLines[2].contains("/pros/")) {
								finalUrl="https://www.pagesjaunes.fr"+secondLines[2];
							}else {
								//https://www.pagesjaunes.fr/pros/detail?code_etablissement=51844953&code_localite=06926400&code_rubrique=60100900
								/*finalUrl = "https://www.pagesjaunes.fr/pros/detail?bloc_id=" + secondLines[2].toUpperCase()
										+ "&no_sequence=0&code_rubrique=" + secondLines[3];*/
								
								finalUrl = "https://www.pagesjaunes.fr/pros/detail?code_etablissement=" + secondLines[2].toUpperCase()
										+ "&code_localite="+secondLines[1]+"&code_rubrique=" + secondLines[3];
								
							}
							System.out.println("Url : " +finalUrl);
							ch.get(finalUrl);
							pageSource = Normalizer.normalize(ch.getPageSource().toLowerCase(), Normalizer.Form.NFD)
									.replaceAll("[^\\p{ASCII}]", "");
							pageSource = this.iscaptcha(pageSource, ch);

							if (!this.noResponse(pageSource)) {
								doc = Jsoup.parse(pageSource);
								this.retrieveData(doc, lines[1], lines[0], secondLines[4]);
							}
							//log2.writeLog(new Date(), secondLines[0], "no_error", 0, secondCurrentLine, 0);

						}
						secondCurrentLine++;

					}

				}

				//log.writeLog(new Date(), lines[1], "no_error", 1, currentLine, 0);
			}
			currentLine++;
		}

	}

	// 08184140000002C0001&no_sequence=2&code_rubrique=60100900
	private void writeLink(String path, String content, String dep) throws IOException {
		
		BufferedWriter bw = new BufferedWriter(new FileWriter(new File(path), true));
		bw.write(content);
		bw.close();
	}
	
	private void intFile(String path,String str) throws IOException {
		File dir = new File(path);
		if (!dir.exists())
			dir.mkdirs();
		File f = new File(dir,str);
		f.createNewFile();
		BufferedWriter bw = new BufferedWriter(new FileWriter(f, true));
		bw.write("actV,place,blocid,rubrique,orias,stat\r\n");
		bw.close();
	}

	private void retrieveData(Document doc, String depSoumis, String activiteSoumis, String orias) {
		Element agencyNameElement = doc
				.selectFirst("#teaser-header > div.row > div.col-sm-7.col-md-8 > div > div.denom > h1");
		String nomAgence = (agencyNameElement != null) ? agencyNameElement.text() : "";
		System.out.println("nom : " + nomAgence);

		System.out.println("orias : "+orias);
		Elements adresseElements = doc.select(
				"#teaser-footer > div > div > div.address-container.marg-btm-s > a.teaser-item.black-icon.address.streetaddress.clearfix.map-click-zone.pj-lb.pj-link > span");
		String adresse ="";
		if(!adresseElements.isEmpty()) {
			for(Element adresseElement: adresseElements)
			adresse+=adresseElement.text()+" ";	
		}
		 
		System.out.println("adresse : " + adresse);
		Element telElement = doc
				.selectFirst("#teaser-footer > div > div > div.zone-fantomas > div > span > span.coord-numero.noTrad");
		String tel = (telElement != null) ? telElement.text() : "";
		System.out.println("tel : " + tel);

		Elements prestationsElements = doc.select(
				"#zone-info > div.zone-produits-presta-services-marques.fd-bloc > div.ligne.prestations.marg-btm-m.generique > ul > li");
		String prestations = "";
		if (!prestationsElements.isEmpty()) {
			for (Element prestationElement : prestationsElements) {
				prestations += prestationElement.text() + " ";
			}
		}
		System.out.println("prestations : " + prestations);

		Elements activitiesElements = doc.select("#zonemultiactivite > div > ul >li ");
		String activite = "";
		if (!activitiesElements.isEmpty()) {
			for (Element activityElement : activitiesElements) {
				activite += activityElement.text() + " ";
			}
		}
		System.out.println("activite : "+activite);
		String siret = "";
		Element blocEtabElement = doc.selectFirst("#zoneb2b > dl.info-etablissement.marg-btm-s.zone-b2b.txt_sm");
		if (blocEtabElement != null) {
			Element siretElement = blocEtabElement.selectFirst("dd:nth-child(2)");
			siret = (siretElement.text().matches("[0-9]{14,}")) ? siretElement.text() : "";
		}
		System.out.println("siret : " + siret);
		String siren = "";
		Element blocEntreprise = doc.selectFirst("#zoneb2b > dl.info-entreprise.marg-btm-s.zone-b2b.txt_sm");
		if (blocEntreprise != null) {
			Element sirenElement = blocEntreprise.selectFirst(" dd:nth-child(2) > strong");
			siren = (sirenElement.text().matches("[0-9]{6,}")) ? sirenElement.text() : "";
		}
		System.out.println("siren : " + siren);

		Elements clientsElements = doc.select(
				"#zone-informations-pratiques > div.row > div.col-sm-12.col-md-6.zone-a-propos > div.zone-info-clientele > div > ul > li");
		String client = "";
		if (!clientsElements.isEmpty()) {
			for (Element clientElement : clientsElements) {
				client += clientElement.text() + " ";
			}
		}
		
		System.out.println("--------------------------------------------------------------------\n");
		
		/*this.saveItem(activiteSoumis, depSoumis, nomAgence, adresse, orias, prestations, client, tel, siren, siret,
				activite);*/

	}

	public boolean isDep() {
		return isDep;
	}

	public ServicePJAssurance setDep(boolean isDep) {
		this.isDep = isDep;
		return this;
	}

  private void retrieveLinks(Document doc, String activiteSoumise, String depSoumise, int stat,String path) throws IOException {
	  
		Map<String,String> bloc_ids = this.retrieveBlocId(doc);
		String code_rubrique = this.retrieveCodeRubrique(doc);
	
		for (String bloc_id : bloc_ids.keySet()) {
			/*String content = activiteSoumise + "," + depSoumise + "," + bloc_id + "," + code_rubrique + "," + bloc_ids.get(bloc_id)
					+ "," + stat + "\r\n";*/
			
			String content = activiteSoumise + "," + depSoumise + "," + bloc_id + "," + code_rubrique + "," + bloc_ids.get(bloc_id)
			+ "," + stat + "\r\n";

			System.out.println("content links : " +content);
			this.writeLink(path, content, depSoumise);
		}

	}

	private Map<String,String> retrieveBlocId(Document doc) {
		Map<String,String> codeIds = new HashMap<>();
		Elements blocIdElements = doc.select("#listresults > div > ul > li ");

		for (Element blocIdElement : blocIdElements) {
			Element tmp =null;
			Elements oriasElement = blocIdElement.select(
					"div.bi-clic-mobile > div > div > p.bi-mentions.tag-full");
			 String orias=""; 
			if((oriasElement!=null) && (oriasElement.text().toLowerCase().contains("orias")))
			    orias = (oriasElement != null) ? oriasElement.text().replaceAll("[^0-9 ]","") : "";
			System.out.println("ORIAS : "+orias);
			
			
			if(blocIdElement.selectFirst("div.bi-ctas > button")!=null) {
				tmp=blocIdElement.selectFirst("div.bi-ctas > button");
				System.out.println(tmp.attr("data-pjsethisto"));
				
				JSONObject json = null;
				String str = "";
				try {
					json = new JSONObject(tmp.attr("data-pjsethisto"));
					str = json.getJSONObject("data").getJSONObject("fd").getString("id_bloc");
				} catch (Exception e) {
					if (e instanceof JSONException) {
						json = new JSONObject(tmp.attr("data-pjhistofantomas"));
						str=json.getString("data");
					}
				}

				codeIds.put(str,orias);
			}
			else {
				Element h=blocIdElement.selectFirst(" div > div:nth-child(2) > div > a");
				if(h!=null) {
					System.out.println(h.attr("href"));
					//codeIds.add(h.attr("href"));
					codeIds.put(h.attr("href"),orias);
				}
				
			}
			 
			
		}

		return codeIds;
	}

	private String retrieveCodeRubrique(Document doc) {
		Elements scripts = doc.select("script");
		String str = "";
		for (Element script : scripts) {
			str = script.html();
			if (str.contains("coderubrique")) {
				str = str.split("var _")[3].replaceAll("paramsstatpage = ", "");
				str = str.replaceAll("[^a-zA-Z0-9:,]", "").split(",")[6].replaceAll("[^0-9]", "").replaceAll("\\s{1,}","");
				return str;
				
			}
		}
		return "";
		
	}

	public void quit() {
		ch.close();
	}

}
