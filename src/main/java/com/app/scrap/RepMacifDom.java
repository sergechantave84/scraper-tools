package com.app.scrap;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.app.model.MACIF;
import com.app.model.MACIF_DOM;

@Repository
public interface RepMacifDom extends JpaRepository<MACIF_DOM,Integer>,RepositoryCustom<MACIF_DOM> {

}
