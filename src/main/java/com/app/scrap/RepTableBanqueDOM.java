package com.app.scrap;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.app.model.TableBanqueDOM;

@Repository
public interface RepTableBanqueDOM extends JpaRepository<TableBanqueDOM, Integer>{

}
