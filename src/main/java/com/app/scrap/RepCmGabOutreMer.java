package com.app.scrap;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.app.model.CM_GAB_Outre_Mer;

@Repository
public interface RepCmGabOutreMer extends JpaRepository<CM_GAB_Outre_Mer, Integer>,RepositoryCustom<CM_GAB_Outre_Mer> {

}
