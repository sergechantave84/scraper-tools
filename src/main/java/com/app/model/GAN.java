package com.app.model;

import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="gan", schema="scrap")
public class GAN {

	@Id
	@SequenceGenerator(name="scrap.gan_id_seq",
	                    sequenceName ="scrap.gan_id_seq",
	                     allocationSize=1)
    @GeneratedValue( strategy=GenerationType.SEQUENCE,
                     generator="scrap.gan_id_seq")
	  
	@Column(name="id",updatable=false)
	private int id;
	
	@Column(name="nbagences", columnDefinition="varchar")
	private String nbAgences;
	
    @Column(name="departement_soumis",columnDefinition = "varchar")
    private String depSoumis;
    
    @Column(name="lien_soumis", columnDefinition = "varchar")
    private String lienSoumis;
	
	@Column(name="lien_agence", columnDefinition="varchar")
	private String lienAgence;
	
	@Column(name="nom_agence", columnDefinition="varchar")
	private String nomAgence;
	
	@Column(name="adresse", columnDefinition="varchar")
	private String adresse;
	
	@Column(name="tel", columnDefinition="varchar")
	private String tel;
	
	@Column(name="nom_agents", columnDefinition="varchar")
	private String nomAgents;
	
	@Column(name="orias", columnDefinition="varchar")
	private String orias;
	
	@Column(name="horaires", columnDefinition="varchar")
	private String horaires;
	
	@Column(name="agencehtm", columnDefinition="varchar")
	private String agenceHtm;
	
	@Column(name="interlocuteurhtm",  columnDefinition="varchar")
	private String interlocuteurHtm;
	
	@Column(name="annee_du_scrap" ,columnDefinition="varchar(4) DEFAULT date_part('year',CURRENT_DATE)")
	private String anneeScrap;
	
	public GAN() {
		LocalDate ld= new Date().toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
		anneeScrap=String.valueOf(ld.getYear()+1);
	}
	
	public int getId() {
		return id;
	}
	public GAN setId(int id) {
		this.id = id;
		return this;
	}
	public String getNbAgences() {
		return nbAgences;
	}
	public GAN setNbAgences(String nbAgences) {
		this.nbAgences = nbAgences;
		return this;
	}
	
	public String getLienAgence() {
		return lienAgence;
	}
	public GAN setLienAgence(String lienAgence) {
		this.lienAgence = lienAgence;
		return this;
	}
	public String getNomAgence() {
		return nomAgence;
	}
	public GAN setNomAgence(String nomAgence) {
		this.nomAgence = nomAgence;
		return this;
	}
	public String getAdresse() {
		return adresse;
	}
	public GAN setAdresse(String adresse) {
		this.adresse = adresse;
		return this;
	}
	public String getTel() {
		return tel;
	}
	public GAN setTel(String tel) {
		this.tel = tel;
		return this;
	}
	public String getNomAgents() {
		return nomAgents;
	}
	public GAN setNomAgents(String nomAgents) {
		this.nomAgents = nomAgents;
		return this;
	}
	public String getOrias() {
		return orias;
	}
	public GAN setOrias(String orias) {
		this.orias = orias;
		return this;
	}
	public String getHoraires() {
		return horaires;
	}
	public GAN setHoraires(String horaires) {
		this.horaires = horaires;
		return this;
	}
	public String getAgenbceHtm() {
		return agenceHtm;
	}
	public GAN setAgenbceHtm(String agenbceHtm) {
		this.agenceHtm = agenbceHtm;
		return this;
	}
	public String getInterlocuteurHtm() {
		return interlocuteurHtm;
	}
	public GAN setInterlocuteurHtm(String interlocuteurHtm) {
		this.interlocuteurHtm = interlocuteurHtm;
		return this;
	}
	public String getAnneeScrap() {
		return anneeScrap;
	}
	public GAN setAnneeScrap(String anneeScrap) {
		this.anneeScrap = anneeScrap;
		return this;
	}

	public String getDepSoumis() {
		return depSoumis;
	}

	public GAN setDepSoumis(String depSoumis) {
		this.depSoumis = depSoumis;
		return this;
	}

	public String getLienSoumis() {
		return lienSoumis;
	}

	public GAN setLienSoumis(String lienSoumis) {
		this.lienSoumis = lienSoumis;
		return this;
	}
	
}
