package com.app.scrap;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.app.model.Mutualia;

@Repository
public interface RepMutuallia extends JpaRepository<Mutualia,Integer>,RepositoryCustom<Mutualia>{

}
