package com.app.scrap;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.app.model.AG2R;

@Repository
public interface RepAg2r extends JpaRepository<AG2R,Integer> ,RepositoryCustom<AG2R>{

}
