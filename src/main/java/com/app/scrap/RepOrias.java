package com.app.scrap;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.app.model.Orias;

@Repository
public interface RepOrias extends JpaRepository<Orias, Integer> {

}
