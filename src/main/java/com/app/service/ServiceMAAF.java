package com.app.service;

import java.io.BufferedInputStream;
import java.io.BufferedReader;

import java.io.File;
import java.io.FileInputStream;

import java.io.IOException;
import java.io.InputStreamReader;
import java.net.SocketTimeoutException;
import java.net.URL;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Scanner;

import javax.net.ssl.HttpsURLConnection;

import org.apache.commons.text.StringEscapeUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.app.model.MAAF;

import com.app.scrap.RepMaaf;

import com.app.utils.Parametres;
import com.app.utils.Utilitaires;

import fa.browserUtils.FactoryBrowser;
import fa.browserUtils.HtmlUnitBrowser;
import fa.log.LogApp.LogScrap;
import fa.log.config.LogConst;

@Service
public class ServiceMAAF extends Utilitaires {

	@Autowired
	private RepMaaf rep;

	private String ficval = "lien_maaf";

	private String url_accueil = "https://www.maaf.fr";
	private String fichier_valeurs;
	private String DepSoumis;
	private String agencyName = "", agencyAddress = "", agencyPhoneNumber = "", agencySchedule = "", agencyFax = "";

	private int NbVillesDEP;

	private int num_ligneficval;
	private int parcoursficval;

	private HtmlUnitDriver htmlUnitDriver;

	public MAAF setData(String depSoumis, String nbVilleDep, String villeSoumise, String nomAgence, String adresse,
			String tel, String fax, String horaires, String agenceHtm) {
		MAAF maaf = new MAAF();
		maaf.setDepSoumis(depSoumis).setNbVilleDep(nbVilleDep).setVilleSoumise(villeSoumise).setNomAgence(nomAgence)
				.setAdresse(adresse).setTel(tel).setFax(fax).setHoraires(horaires).setAgenceHtm(agenceHtm);
		rep.save(maaf);
		return maaf;
	}

	public void setUp() throws Exception {
		FactoryBrowser fact = new FactoryBrowser();
		HtmlUnitBrowser hB = (HtmlUnitBrowser) fact.create("htmlUnit");
		htmlUnitDriver = hB.setOptions(false, false, false, 60000).initBrowserDriver();
	}

	public void scrap() throws IOException {
		Document doc = null;
		String pageSource = null;
		fichier_valeurs = Parametres.REPERTOIRE_DATA_SOURCES + ficval + ".txt";

		Scanner sc = new Scanner(new BufferedInputStream(new FileInputStream(new File(fichier_valeurs))));

		LogScrap log = new LogScrap("AssuranceMAAF.txt", LogConst.TYPE_LOG_FILE);
		num_ligneficval = ((new File(LogConst.STORE_LOG_PATH + LogConst.PREFIX_LOG_FILE + "_AssuranceMAAF.txt")).exists())
				? Integer.parseInt(log.readLog().getLastLineProcessed().split("\\t")[4])
				: 1;

		parcoursficval = 0;
		while (sc.hasNextLine()) {
			String lines[] = sc.nextLine().split("\\t");
			if (parcoursficval > num_ligneficval) {

				DepSoumis = lines[0];
				String url = url_accueil + lines[1];
				URL urlb = new URL(url);
				HttpsURLConnection con = (HttpsURLConnection) urlb.openConnection();
				con.setRequestMethod("GET");
				con.setConnectTimeout(600000);
				con.setReadTimeout(60000);
				BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream(), "UTF-8"));

				StringBuilder strB = new StringBuilder();
				while ((pageSource = in.readLine()) != null) {
					strB.append(pageSource);
				}
				pageSource = StringEscapeUtils.unescapeHtml4(strB.toString());
				doc = Jsoup.parse(pageSource);
				Element agencyBigBlockJsoupElement = doc.select("div[class=\"liste-agences-block\"]").first();
				NbVillesDEP = 0;
				if (agencyBigBlockJsoupElement != null) {
					List<String> agenciesLinkList = new ArrayList<>();
					if (!agenciesLinkList.isEmpty()) {
						agenciesLinkList.clear();
					}
					Elements agenciesLinkJsoupElement = agencyBigBlockJsoupElement
							.select("ul.liste-agences--liste>li.liste-agences--liste__item");
					if (agenciesLinkJsoupElement != null) {
						for (Element elementTmp : agenciesLinkJsoupElement) {
							Element linkTmp = elementTmp.selectFirst("span.liste-agences--liste__item-text > h2 > a");
							String href = url_accueil + linkTmp.attr("href");
							agenciesLinkList.add(href);
							//System.out.println("lien : "+href);
						}
					}
					NbVillesDEP = agenciesLinkList.size();

					for (String strTmp : agenciesLinkList) {

						String agencyHtml = "";
						pageSource = null;
						urlb = new URL(strTmp);
						con = (HttpsURLConnection) urlb.openConnection();
						con.setRequestMethod("GET");
						con.setConnectTimeout(600000);
						con.setReadTimeout(60000);
						in = new BufferedReader(new InputStreamReader(con.getInputStream(), "UTF-8"));

						strB = new StringBuilder();
						while ((pageSource = in.readLine()) != null) {
							strB.append(pageSource);
						}
						pageSource = StringEscapeUtils.unescapeHtml4(strB.toString());

						if (pageSource != null) {
							doc = Jsoup.parse(pageSource);

							Element agencyHtmlJsoupElement = doc
									.select("div.mars-section.mars-section--primary.mars-section--primary_agences")
									.first();
							if (agencyHtmlJsoupElement != null) {
								agencyHtml = agencyHtmlJsoupElement.outerHtml();
								agencyHtml = this.normalize(agencyHtml.replaceAll("\\s{2,}", ""));
							}
							System.out.println("urlb : "+urlb);

							agencyName = "";
							// retrieve agency name
							Element agencyNameJsoupElement = doc
									.select("div.mars-section.mars-section--lightergrey.mars-section--min>div>div>h1")
									.first();
							agencyName = (agencyNameJsoupElement != null)
									? this.normalize(agencyNameJsoupElement.text().trim())
									: "";
							System.out.println("nom agence : " + agencyName);
							// retrieve agency coordinate
							Element agencyJsoupElement = doc.select("div[class=\"row agences-blocks\"]").first();
							if (agencyJsoupElement != null) {
								System.out.println("on est là");
								Element agencyJsoupElementColumnLeft = agencyJsoupElement
										.select("div[class=\"col-sm-6 block-gauche \"]").first();
								if (agencyJsoupElementColumnLeft != null) {
									Elements infoJsoupElement = agencyJsoupElementColumnLeft.select(
											"div[class=\"infos-block\"] > div[class=\"infos-block-div__liste\"] > "
													+ "ul[class=\"infos-block__liste\"] > li");

									agencyAddress = "";
									agencyPhoneNumber = "";
									agencySchedule = "";
									agencyFax = "";
									for (int i = 0; i < (infoJsoupElement.size()); i++) {

										switch (i) {
										case 0: {
											// retrieve agency address
											Element agencyAddressJsoupElement = infoJsoupElement.get(i)
													.select(".infos-block__liste-item-text").first();
											agencyAddress = (agencyAddressJsoupElement != null)
													? this.normalize(agencyAddressJsoupElement.text().trim())
													: "";
											System.out.println("agencyAddress : "+agencyAddress);
											break;
										}
										case 1: {
											// retrieve agency phone number
											Element agencyPhoneNumberJsoupElement = infoJsoupElement.get(i)
													.select(" span.infos-block__liste-item-text>span.visible-xs>a")
													.first();
											agencyPhoneNumber = (agencyPhoneNumberJsoupElement != null)
													? agencyPhoneNumberJsoupElement.attr("href").replaceAll("tel:", "")
															.trim()
													: "";
											System.out.println("agencyPhoneNumber : "+agencyPhoneNumber);
											break;
										}
										case 3: {
											// retrieve agency Schedule
											Elements agencyScheduleJsoupElement = infoJsoupElement.get(i).select(
													"span.infos-block__liste-item-text>span.infos-block__liste-item-text."
															+ "horairesAgences>" + "span");
											if (agencyScheduleJsoupElement != null) {
												for (Element elementTmp : agencyScheduleJsoupElement) {
													agencySchedule += (this.normalize(elementTmp.text().trim())) + " ";
												}
											}
											agencySchedule = this.normalize(agencySchedule);
											System.out.println("Horaire : "+agencySchedule);
											System.out.println("------------------------------------\n");
											break;
										}
										}

									}
								}
							}
							this.setData(DepSoumis, String.valueOf(NbVillesDEP), strTmp, agencyName, agencyAddress,
									agencyPhoneNumber, agencyFax, agencySchedule, agencyHtml);
						}

					}
				}
				log.writeLog(new Date(), DepSoumis, "no_error", NbVillesDEP, parcoursficval, 0);
			}

			parcoursficval++;
		}
		sc.close();

	}

}
