package com.app.scrap;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.app.model.Swisslife;

@Repository
public interface RepSwisslife extends JpaRepository<Swisslife,Integer>,RepositoryCustom<Swisslife>{

}
