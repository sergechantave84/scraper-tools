package com.app.scrap;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.app.model.MACIF;

@Repository
public interface RepMacif extends JpaRepository<MACIF,Integer>,RepositoryCustom<MACIF> {

}
