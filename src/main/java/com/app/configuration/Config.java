package com.app.configuration;

import javax.persistence.EntityManagerFactory;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;



@Configuration
public class Config {
 
	@Autowired 
	private EntityManagerFactory entityManagerFactory;
	
	@Bean
	public  SessionFactory getSesionFactory() {
		if(entityManagerFactory.unwrap(SessionFactory.class)==null) {
			throw new NullPointerException("fact not hib");
		}
		return entityManagerFactory.unwrap(SessionFactory.class);
	}
	
	/*@Bean
	public Repositories getRepository() {
		 return new Repositories();
	}*/
}
