package com.app.service;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;

import java.io.IOException;

import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.Normalizer;

import java.util.Date;
import java.util.List;
import java.util.Scanner;

import org.json.JSONObject;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;



import org.openqa.selenium.chrome.ChromeDriver;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import com.app.model.Palatine_Outre_Mer;
import com.app.scrap.RepPalatineOutreMer;
import com.app.utils.Parametres;


import fa.log.LogApp.LogScrap;
import fa.log.config.LogConst;



@Service
public class ServicePalatine_Outre_Mer extends Scraper {

	@Autowired
	RepPalatineOutreMer rep;

	String driverPath = "C:\\ChromeDriver\\";
	String nomSITE = "BQ_PALATINE";
	String ficval = "lien_palatine_outre_mer";
	String dossierRESU = Parametres.REPERTOIRE_RESULT + "\\BQ\\" + nomSITE;;
	String url_accueil = "https://www.palatine.fr/nos-agences.html";
	String fichier_valeurs;
	String DepSoumis;
	String ligne;

	int Resultats;
	int num_ligne = 2;
	int NbAgences = 0;
	int numreponse = 1;
	int nbRecuperes;
	int num_ligneficval = 1;
	int parcoursficval = 1;

	ChromeDriver wd;
	BufferedWriter sortie;
	BufferedWriter sortie_log;

	Date Temps_Fin;
	Document doc = null;

	public Palatine_Outre_Mer saveData(Palatine_Outre_Mer t) {

		return rep.save(t);

	}

	public List<Palatine_Outre_Mer> saveDataAll(List<Palatine_Outre_Mer> t) {

		return rep.saveAll(t);

	}

	public List<Palatine_Outre_Mer> getElmnt() {

		return rep.findAll();
	}

	public Palatine_Outre_Mer getById(int id) {

		return rep.findById(id).orElse(null);
	}

	public String deleteProduct(int id) {
		try {
			rep.deleteById(id);

		} catch (Exception e) {
			e.printStackTrace();
		}

		return "removed";
	}

	public void setData(String depSoumis, String nom_agence, String adresse, String tel, String fax, String horairesTXT,
			String nbAgences, String agences_id, String horairesHTM, String agences_HTM) {

		Palatine_Outre_Mer palatine = new Palatine_Outre_Mer();
		palatine.setDepSoumis(depSoumis).setNbAgences(nbAgences).setAgences_id(agences_id).setNom_agence(nom_agence)
				.setAdresse(adresse).setTel(tel).setFax(fax).setHorairesTXT(horairesTXT).setHorairesHTM(horairesHTM)
				.setAgences_HTM(agences_HTM);
		this.saveData(palatine);

	}

	public void scrap() throws IOException {
		fichier_valeurs = Parametres.REPERTOIRE_DATA_SOURCES + ficval + ".txt";
		Scanner sc = new Scanner(new BufferedInputStream(new FileInputStream(new File(fichier_valeurs))));

		if (!new File(dossierRESU).exists())
			new File(dossierRESU).mkdirs();

		LogScrap log = new LogScrap("BanquePalatine_DOM.txt", LogConst.TYPE_LOG_FILE);
		num_ligneficval = ((new File(LogConst.STORE_LOG_PATH + LogConst.PREFIX_LOG_FILE + "_BanquePalatine_DOM.txt"))
				.exists()) ? Integer.parseInt(log.readLog().getLastLineProcessed().split("\\t")[4]) : 1;

		int currentLine = 1;
		while (sc.hasNextLine()) {
			String lines[] = sc.nextLine().split("\\t");

			if (currentLine > num_ligneficval) {
				System.out.println(lines[1]);
				URL url = new URL(lines[1]);
				HttpURLConnection con = (HttpURLConnection) url.openConnection();
				con.setRequestMethod("GET");
				con.setRequestProperty("Content-Type", "text/html");
				con.setRequestProperty("Content-Encoding", "gzip");
				con.setRequestProperty("Vary", "Accept-Encoding");
				con.setRequestProperty("Content-Length", "0");
				con.setConnectTimeout(60000);
				con.setReadTimeout(60000);
				con.setInstanceFollowRedirects(true);

				BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream(), "ISO-8859-1"));
				String pageSource;
				StringBuilder strB = new StringBuilder();
				while ((pageSource = in.readLine()) != null) {
					strB.append(pageSource);
				}

				in.close();
				Document doc = Jsoup.parse(strB.toString());
				Element root = doc.selectFirst("body > script");

				String agencys[] = Normalizer.normalize(root.html().toString(), Normalizer.Form.NFD)
						.replaceAll("[^\\p{ASCII}]", "").split("AddPoi");
				int l = agencys.length;

				JSONObject scheduleJSON = null;
				JSONObject jsonDatas = null;
				JSONObject json = null;

				int nbAgences = 0;

				String nomAgence = "", zipCode = "", city = "", adresse = "", postalAdress = "", tel = "", fax = "",
						horairesHTM = "", horairesTXT = "", agencyId = "", agencesHTML = "";

				for (int i = 0; i < l - 1; i++) {
					nomAgence = "";
					zipCode = "";
					city = "";
					adresse = "";
					postalAdress = "";
					tel = "";
					fax = "";
					horairesHTM = "";
					horairesTXT = "";
					agencyId = "";
					agencesHTML = "";

					String strJSON = agencys[i + 1].replaceAll("[\\(\\)]", "").replaceAll("^['0-9,]{1,}\\{\"datas\"",
							"");
					strJSON = "{\"datas\"" + strJSON;

					json = new JSONObject(strJSON);
					jsonDatas = json.getJSONObject("datas");

					agencesHTML = jsonDatas.toString();

					nomAgence = (jsonDatas.has("label")) ? jsonDatas.getString("label") : "";
					System.out.println("le nom de l'agence " + nomAgence);

					zipCode = (jsonDatas.has("zip")) ? jsonDatas.getString("zip") : "";
					city = (jsonDatas.has("city")) ? jsonDatas.getString("city") : "";
					adresse = (jsonDatas.has("address1")) ? jsonDatas.getString("address1") + " " + zipCode + " " + city
							: "";
					System.out.println("l'adresse est " + adresse);

					agencyId = (jsonDatas.has("id")) ? jsonDatas.getString("id") : "";
					System.out.println("id " + agencyId);

					postalAdress = (jsonDatas.has("postalAddress")) ? jsonDatas.getString("postalAddress") : "";
					System.out.println("postalAdress " + postalAdress);

					tel = (jsonDatas.has("tel")) ? jsonDatas.getString("tel") : "";
					System.out.println("tel " + tel);

					fax = (jsonDatas.has("fax")) ? jsonDatas.getString("fax") : "";
					System.out.println("fax" + fax);

					scheduleJSON = (jsonDatas.has("schedule")) ? jsonDatas.getJSONObject("schedule") : null;
					if (scheduleJSON != null) {
						horairesHTM = scheduleJSON.toString();
						horairesTXT = "lundi " + scheduleJSON.getString("day1") + " mardi "
								+ scheduleJSON.getString("day2") + " mercredi " + scheduleJSON.getString("day3")
								+ " jeudi " + scheduleJSON.getString("day4") + " vendredi "
								+ scheduleJSON.getString("day5") + " samedi " + scheduleJSON.getString("day6");
						System.out.println(horairesTXT);
					}
					/*
					 * setData(String depSoumis, String nomAgence, String adresse, String tel,
					 * String fax, String horairesTXT, String nbAgences, String agencesId, String
					 * horairesHTM, String agencesHTML )
					 */
					this.setData(lines[0], nomAgence, adresse, tel, fax, horairesTXT, String.valueOf(nbAgences),
							agencyId, horairesHTM, agencesHTML);
				}

				log.writeLog(new Date(), lines[1], "no_errors", nbAgences, currentLine, 0);
				con.disconnect();
			}
			currentLine++;

		}
	}

	public void tearDown() {
		wd.close();
		wd.quit();

	}

	@Override
	public void saveItem(String... args) {
		// TODO Auto-generated method stub

	}

}
