package com.app.service;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.net.SocketTimeoutException;
import java.text.Normalizer;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Scanner;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.app.model.BECM;
import com.app.scrap.RepBECM;
import com.app.utils.Parametres;

import fa.log.LogApp.LogScrap;
import fa.log.config.LogConst;

@Service
public class ServiceBECM extends Scraper {

	@Autowired
	RepBECM rep;

	private HtmlUnitDriver hu;
	private String ficVal = "lien_agence_becm.txt";

	public void setup() {
		hu=(HtmlUnitDriver) this.setupHU();
	}

	public void logout() {
		hu.quit();
	}
	@Override
	public void saveItem(String... args) {
		BECM becm = new BECM();
		becm.setNbAgences(args[0]).setAdresse(args[1]).setHoraire(args[2]).setNomAgence(args[3]).setTelephone(args[4])
				.setDep(args[5]).setLienAgence(args[6]).setGab(args[7]);

		rep.save(becm);

	}

	@Override
	public void scrap() throws Exception {

		Scanner sc = new Scanner(
				new BufferedInputStream(new FileInputStream(new File(Parametres.REPERTOIRE_DATA_SOURCES + ficVal))));
		LogScrap log = new LogScrap("BanqueBECMLog.txt", LogConst.TYPE_LOG_FILE);
		int currentLine = ((new File(LogConst.STORE_LOG_PATH + LogConst.PREFIX_LOG_FILE + "_BanqueBECMLog.txt"))
				.exists()) ? Integer.parseInt(log.readLog().getLastLineProcessed().split("\t")[4]) : 1;

		List<String> liensAgences = new ArrayList<>();
		int counter = 1;
		while (sc.hasNextLine()) {
			String lines[] = sc.nextLine().split("\t");
			if (counter > currentLine) {
				System.out.println(lines[1]);
				try {
					hu.get(lines[1]);
				} catch (Exception e) {
					if (e instanceof SocketTimeoutException) {

						this.setup();
						hu.get(lines[1]);
					}
				}

				String pageSource = hu.getPageSource();
				Document doc = Jsoup.parse(pageSource);

				if (!liensAgences.isEmpty())
					liensAgences.clear();

				Elements lienAgencesElements = doc.select(
						"#ei_tpl_content > div > article > div.body > div > div.a_blocfctl.lister > table > tbody > tr> td> a");
				for (Element lienAgenceElement : lienAgencesElements) {
					liensAgences.add(lienAgenceElement.attr("href"));
				}

				for (String lienAgence : liensAgences) {
					String url = "https://www.becm.fr/fr/banques/entreprises/" + lienAgence;
					System.out.println(url);
					try {
						hu.get(url);
					} catch (Exception e) {
						if (e instanceof SocketTimeoutException) {

							this.setup();
							hu.get(url);
						}
					}
					pageSource = hu.getPageSource();
					doc = Jsoup.parse(pageSource);
					Element root = doc.select("#rslt").first();
					String nom_agence = "", adresse = "", LienAgence = "", tel = "", horaires = "", depSoumise = "";
					int gab = 0;
					if (root != null) {

						Elements agenciesLinkJElement = root.select("#rslt_lst> li>div.body");
						int NbAgences = agenciesLinkJElement.size();
						int nbRecuperes = 0;
						for (Element tmp : agenciesLinkJElement) {
							// TODO change
							nom_agence = "";
							adresse = "";
							LienAgence = "";
							tel = "";
							horaires = "";
							depSoumise = "";

							// retrieve agency link
							Element agencyNamaElement = tmp.select("span.lbl.titre3 > em > a").first();
							LienAgence = "https://www.creditmutuel.fr/cmag/fr/banques/contact/trouver-une-agence/"
									+ agencyNamaElement.attr("href");
							System.out.println("le lien de l'agences est " + LienAgence);

							// retrieve agency name
							nom_agence = (agencyNamaElement != null)
									? Normalizer.normalize(agencyNamaElement.text(), Normalizer.Form.NFKD).replaceAll(
											"[^\\p{ASCII}]", "")
									: "";
							System.out.println("le nom de l'agence est " + nom_agence);

							String gabState = Normalizer
									.normalize(tmp.select("span.lbl.titre3 > em").first().text().toLowerCase(),
											Normalizer.Form.NFKD)
									.replaceAll("[^\\p{ASCII}]","");
							gab=(gabState.contains("sans guichet")) ? 1: 0;
							
							// TODO change
							// retrieve agency tel
							Element agencyTelElement = tmp
									.selectFirst("span:nth-child(3)>span[itemprop=\"telephone\"]");
							tel = (agencyTelElement != null) ? agencyTelElement.text() : "";
							System.out.println(tel);

							// retrieve horaires
							Elements scheduleElements = tmp
									.select("span:nth-child(3)>div.schdl.schdl_short.g>table>tbody>tr");
							for (Element scheduleElement : scheduleElements) {
								horaires += scheduleElement.text() + " ";
							}
							horaires = Normalizer.normalize(horaires, Normalizer.Form.NFKD).replaceAll("[^\\p{ASCII}]",
									"");
							System.out.println("horaires " + horaires);

							// retrieve agency address
							Element agencyAddressElement = tmp.select("span:nth-child(3) > em > span.invisible")
									.first();
							adresse = (agencyAddressElement != null) ? agencyAddressElement.text() : "";
							adresse = Normalizer.normalize(adresse, Normalizer.Form.NFKD).replaceAll("[^\\p{ASCII}]",
									"");
							System.out.println(adresse);
							nbRecuperes++;
							
							this.saveItem(String.valueOf(NbAgences), adresse, horaires, nom_agence, tel, lines[0],
									LienAgence,String.valueOf(gab));

						}

					}

				}
				log.writeLog(new Date(), lines[0], "no_error", 0, counter, 0);
			}
			counter++;
		}

	}

}
