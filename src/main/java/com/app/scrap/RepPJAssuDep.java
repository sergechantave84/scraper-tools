package com.app.scrap;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.app.model.PJAssurance;
import com.app.model.PJAssuranceCP;
import com.app.model.PJAssuranceDep;


@Repository
public interface RepPJAssuDep extends JpaRepository<PJAssuranceDep, Integer> {

}
