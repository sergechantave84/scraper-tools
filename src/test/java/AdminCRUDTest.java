import static org.junit.Assert.assertTrue;


import java.util.List;


import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import com.app.model.TableBanque;
import com.app.scrap.Scrapper1Application;
import com.app.service.admin.AdminService;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = Scrapper1Application.class)
@TestPropertySource(locations = "classpath:test_application.properties")
public class AdminCRUDTest {

  @Autowired
  AdminService serv;
  
  

  public void test1() {
	  assertTrue(serv.populateAssuranceTable());
  }
  
  @Ignore
  @Test
  public void testCRUD() {
	  List<TableBanque> l=serv.getItem(TableBanque.class);
	  
	  for(TableBanque b:l) {
		  System.out.println(b.getName());
	  }
	  
  }
}
