package com.app.scrap;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import com.app.model.PJBanqueCP;


@Repository
public interface RepPJBanqueCp extends JpaRepository<PJBanqueCP, Integer> {

}
