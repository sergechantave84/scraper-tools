package com.app.scrap;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


import com.app.model.Assu2000;
import com.app.model.Assu2000_DOM;

@Repository
public interface RepAssu2000Dom extends JpaRepository<Assu2000_DOM,Integer>,RepositoryCustom<Assu2000_DOM> {

}
