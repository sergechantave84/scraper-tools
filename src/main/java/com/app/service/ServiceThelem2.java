package com.app.service;

import java.io.BufferedInputStream;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.net.SocketTimeoutException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Scanner;

import org.json.JSONArray;
import org.json.JSONObject;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.openqa.selenium.PageLoadStrategy;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.app.model.Thelem;

import com.app.scrap.RepThelem;
import com.app.utils.BrowserUtils;
import com.app.utils.Parametres;
import com.app.utils.Utilitaires;

import fa.browserUtils.FactoryBrowser;
import fa.browserUtils.HtmlUnitBrowser;
import fa.log.LogApp.LogScrap;
import fa.log.config.LogConst;

@Service
public class ServiceThelem2 extends Scraper {

	@Autowired
	private RepThelem rep;

	private ChromeDriver ch;

	HtmlUnitDriver htmlUnitDriver;

	private List<com.app.model.Thelem> thelemList = new ArrayList<>();

	public Thelem setData(String nbAgences, String lienAgence, String nomAgence, String adresse, String tel, String fax,
			String nomsAgents, String orias, String horaires, String agenceHtm) {
		Thelem thelem = new Thelem();
		thelem.setNbAgences(nbAgences).setLienAgence(lienAgence).setNomAgence(nomAgence).setAdresse(adresse).setTel(tel)
				.setFax(fax).setNomsAgents(nomsAgents).setOrias(orias).setHoraires(horaires).setAgenceHtm(agenceHtm);
		rep.save(thelem);
		return thelem;
	}

	public List<Thelem> showFirstRows() {
		List<Thelem> a = rep.getFirstRow(100, Thelem.class);
		return a;
	}

	public List<Thelem> showLastRows() {
		List<Thelem> a = rep.getLastRow(100, Thelem.class);
		return a;
	}

	public void setup() {
		

		final String chromeBeta = "C:\\Program Files\\Google\\Chrome Beta\\Application\\chrome.exe";

		ch = (ChromeDriver) this.setupCh(Parametres.CHROME_DRIVER_PATH, true, false,
				"C:\\Users\\GEOMADA PC4\\AppData\\Local\\Google\\Chrome Beta\\User Data", "", chromeBeta);
		
	}

	@Override
	public void scrap() throws IOException, InterruptedException {

		String ficval = "lien_thelem";
		String urlThelemAgences = "https://www.thelem-assurances.fr/agences/";
		String fichier_valeurs = Parametres.REPERTOIRE_DATA_SOURCES + ficval + ".txt";
		Scanner sc = new Scanner(new BufferedInputStream(new FileInputStream(new File(fichier_valeurs))));

		int agencyNumbers = 0;
		String pageSource = "";

		String lienAgences = "";
		String nomAgence = "";
		String adresse = "";
		String telephone = "";
		String fax = "";
		String nomAgents = "";
		String orias = "";
		String horaires = "";
		String agenceHTML = "";

		Document doc = null;

		LogScrap log = new LogScrap("AssuranceThelem.txt", LogConst.TYPE_LOG_FILE);
		int num_ligneficval = ((new File(LogConst.STORE_LOG_PATH + LogConst.PREFIX_LOG_FILE + "_AssuranceThelem.txt"))
				.exists()) ? num_ligneficval = Integer.parseInt(log.readLog().getLastLineProcessed().split("\\t")[4])
						: 1;

		int currentLine = 1;
		while (sc.hasNextLine()) {

			String line = sc.nextLine();

			if (currentLine > num_ligneficval) {
				String url = urlThelemAgences + line;
				System.out.println(url);

				ch.get(url);

				try {
					pageSource = ch.getPageSource();

					// System.out.println(pageSource);

					doc = Jsoup.parse(pageSource);

					Thread.sleep(2000);

					
					Element root = doc.selectFirst("#ito_ny_resultat");

					String result = root != null ? root.attr("data-result") : null;

					JSONArray jsonArrayRes = null;
					JSONObject json = null;

					// System.out.println(result);

					if (result != null) {
						
						jsonArrayRes = new JSONArray(result);

						System.out.println("Nombre de json : " + jsonArrayRes.length());
						
						int nbAgence =jsonArrayRes.length();

						for (int i = 0; i < jsonArrayRes.length(); ++i) {

							json = jsonArrayRes.getJSONObject(i);

							//System.out.println("json : " + json);

							nomAgence = (json.has("nom")) ? json.getString("nom").toUpperCase() : "";
							adresse = (json.has("adresse")) ? json.getString("adresse").toUpperCase() : "";

							telephone = (json.has("tel")) ? json.getString("tel") : "";
							
							lienAgences = (json.has("lien")) ? json.getString("lien") : "";

							nomAgents = (json.has("agentsName")) ? json.getString("agentsName") : "";
							
							horaires = (json.has("horaires")) ? json.getString("horaires") : "";
							
							if(json.has("nom") && json.has("adresse")) {
								//this.setData(String.valueOf(nbAgence), lienAgences, nomAgence, adresse, telephone, fax, nomAgents, orias, horaires, agenceHTML);
								
							}
							
							System.out.println("Nom : "+nomAgence);
							System.out.println("adresse : "+adresse);
							System.out.println("telephone : "+telephone);
							System.out.println("nom_agents : "+nomAgents);
							System.out.println("horaires : "+horaires);
							System.out.println("\n- - - - - - - - - - - - - - - - - - - - - - - - - - - - \n");

						}
					} else {
						System.err.println("Aucun resultat!");
					}
					
				} catch (Exception e) {
					e.printStackTrace();
				}

				//log.writeLog(new Date(), line, "no_error", agencyNumbers, currentLine, 0);
			}

			currentLine++;

		}

	}

	public void tearsDown() {
		System.out.println("scrap finis");
		ch.quit();
	}

	@Override
	public void saveItem(String... args) {
		// TODO Auto-generated method stub

	}
}
