package com.app.scrap;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.app.model.BP;

@Repository
public interface RepBP extends JpaRepository<BP,Integer>,RepositoryCustom<BP> {

}
