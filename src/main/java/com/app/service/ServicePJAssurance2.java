package com.app.service;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.Console;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.URLEncoder;
import java.text.Normalizer;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.openqa.selenium.chrome.ChromeDriver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.app.model.PJAssuranceCP;
import com.app.model.PJAssuranceDep;
import com.app.scrap.RepPJAssuCP;
import com.app.scrap.RepPJAssuDep;
import com.app.utils.Parametres;

import fa.log.LogApp.LogScrap;
import fa.log.config.LogConst;

@Service
public class ServicePJAssurance2 extends Scraper implements SuperClassPJ {

	@Autowired
	private RepPJAssuDep repDEP;

	@Autowired
	private RepPJAssuCP repCP;

	private boolean isDep = true;
	private ChromeDriver ch;

	private String ficvalCp = "Pj_rubogracp_CP.txt";
	private String ficvalDep = "Pj_rubOGRAdep_DEP.txt";

	@Override
	public void saveItem(String... args) {

		if (isDep) {
			PJAssuranceDep pjDep = new PJAssuranceDep();
			pjDep.setActiviteSoumise(args[0]).setDep(args[1]).setDenomination(args[2]).setAdresse(args[3])
					.setOrias(args[4]).setPrestation(args[5]).setClientele(args[6]).setTel(args[7]).setSiren(args[8])
					.setSiret(args[9]).setActivite(args[10]).setCodenaf(args[11]).setMarque(args[12]).setFormjuridique(args[13]);
			repDEP.save(pjDep);
		} else {
			PJAssuranceCP pjCp = new PJAssuranceCP();
			pjCp.setActiviteSoumise(args[0]).setCp(args[1]).setDenomination(args[2]).setAdresse(args[3])
					.setOrias(args[4]).setPrestation(args[5]).setClientele(args[6]).setTel(args[7]).setSiren(args[8])
					.setSiret(args[9]).setActivite(args[10]).setCodenaf(args[11]).setMarque(args[12]).setFormjuridique(args[13]);
			repCP.save(pjCp);
		}

	}

	public void setup() {
		
		//final String chrome ="C:\\Program Files\\Google\\Chrome\\Application\\chrome.exe";
		final String chromeBeta = "C:\\Program Files\\Google\\Chrome Beta\\Application\\chrome.exe";
		
		ch = (ChromeDriver) this.setupCh(Parametres.CHROME_DRIVER_PATH, true, false,
				"C:\\Users\\GEOMADA PC4\\AppData\\Local\\Google\\Chrome Beta\\User Data", "",chromeBeta);
	
	}
	
	@Override
	public void scrap() throws Exception {
		String url = "https://www.pagesjaunes.fr/recherche/";
		Scanner sc = null;
		LogScrap log;
		int lastLine = 1;
		
		if (isDep) {
			sc = new Scanner(new BufferedInputStream(
					new FileInputStream(new File(Parametres.REPERTOIRE_DATA_SOURCES + ficvalDep))));
			log = new LogScrap("PJAssuranceDep.txt", LogConst.TYPE_LOG_FILE);
			lastLine = readLog(isDep,"Dep");

		} else {
			sc = new Scanner(new BufferedInputStream(
					new FileInputStream(new File(Parametres.REPERTOIRE_DATA_SOURCES + ficvalCp))));
			log = new LogScrap("PJAssuranceCp.txt", LogConst.TYPE_LOG_FILE);
			lastLine = readLog(!isDep,"Cp");
		}
		
		
		int currentLine = 0;
		
		
		while (sc.hasNextLine()) {
			String[] lines = sc.nextLine().split("\\t");
			if (currentLine > lastLine) {
				
				String finalURL = url + lines[1] + "/" + lines[0];
				
				System.out.println("URL : "+finalURL);
				
				System.out.println("Département : "+lines[1] );
				
				ch.get(finalURL);
				
				String pageSource = Normalizer.normalize(ch.getPageSource().toLowerCase(), Normalizer.Form.NFD)
						.replaceAll("[^\\p{ASCII}]", "");

				pageSource = this.iscaptcha(pageSource, ch);
				
				
				System.out.println("PageSource is not found: "+isNotFound(pageSource) );
				
				if (!this.noResponse(pageSource) || !this.isNotFound(pageSource)) {

					String pageNumber = "";
					Document doc = Jsoup.parse(pageSource);
					
					Element elementPageNumber = doc.selectFirst("#sel-compteur");
					String pageNumberTmp = elementPageNumber.text();
					Pattern pattern = Pattern.compile("/ \\d{1,}");
					Matcher match = pattern.matcher(pageNumberTmp);
					while (match.find())
						pageNumber = match.group().replaceAll("/", " ").trim();
					
					System.out.println("Page 1 / ...");
					
					this.getResult(doc, lines[0], lines[1]);
					
					System.out.println("--------------------------------------------\n");
					
					int i = 2;
					if (Integer.parseInt(pageNumber) > 1) {
						while (i <= Integer.valueOf(pageNumber)) {
							
							if (i <= Integer.valueOf(pageNumber))
								ch.findElementById("pagination-next").click();
						    
							
							Thread.sleep(200);
							
							pageSource = Normalizer.normalize(ch.getPageSource().toLowerCase(), Normalizer.Form.NFD)
									.replaceAll("[^\\p{ASCII}]", "");
                            pageSource = this.iscaptcha(pageSource, ch);
                            
							doc = Jsoup.parse(pageSource);
							
							System.out.println("Page "+i+" / " + pageNumber);

							this.getResult(doc, lines[0], lines[1]);
							
							System.out.println("--------------------------------------------\n");
							
							elementPageNumber = doc.selectFirst("#sel-compteur");
							pageNumberTmp = (elementPageNumber!=null)? elementPageNumber.text():"";
							pattern = Pattern.compile("/ \\d{1,}");
							match = pattern.matcher(pageNumberTmp);
							while (match.find())
								pageNumber = match.group().replaceAll("/", " ").trim();
							if(this.noResponse(pageSource))
								break;
							
							
							i++;
						}
					}

				}

				log.writeLog(new Date(), lines[1], "no_error", 1, currentLine, 0);
			}
			currentLine++;
		}

	}
	
	/**
	 * 
	 * @param pageSource
	 * @return true if pageSource has not result of search or contains "probleme technique" else false
	 */
	private boolean isNotFound(String pageSource) {
		//System.out.println( !pageSource.contains("oups nous navons pas encore de reponse a cette recherche"));
		return pageSource.contains("oups nous navons pas encore de reponse a cette recherche") || pageSource.contains("probleme technique");
	}
	
	/**
	 * 
	 * @param isDep
	 * @param prefixe
	 * @return lastLine reading in the log file text.
	 * @throws FileNotFoundException
	 */
	private int readLog(boolean isDep, String prefixe) throws FileNotFoundException {
		int lastLine = 0;
		if(isDep) {
			if ((new File(LogConst.STORE_LOG_PATH + LogConst.PREFIX_LOG_FILE + "_PJAssurance"+prefixe+".txt").exists())){
				Scanner sc3 = new Scanner(new BufferedInputStream(
						new FileInputStream(new File(LogConst.STORE_LOG_PATH + LogConst.PREFIX_LOG_FILE + "_PJAssurance"+prefixe+".txt"))));
				while(sc3.hasNextLine()) {
					lastLine=Integer.parseInt(sc3.nextLine().split("\t")[4]);
				}
			}
		}
		return lastLine;
			
	}
	/**
	 * 
	 * @param doc
	 * @param activiteSoumis
	 * @param depSoumis
	 */
	
	private void getResult(Document doc, String activiteSoumis, String depSoumis) {
		
		Element dataElement = doc.selectFirst("#hita_maso");
		String res=null;		
		String nom="";
		String adresse = "";
		
		String prestation ="";
		String activite ="";
		String marque ="";
		String telephone="";
		String orias ="";
		String codenaf ="";
		String siren ="";
		String siret ="";
		String formejuridique="";
		String client="";
		
		JSONArray recs=null;
		
		JSONObject json =null;
		
		if(dataElement != null) {
			res = dataElement.attr("data-json-value");
			//System.out.println("Res : " +res);
			
			if(res!="") {
				
				recs=new JSONArray(res);
				
				System.out.println("Nombre de json : " +recs.length());
				
				for (int i = 0; i < recs.length(); ++i) {
					
					 json  = recs.getJSONObject(i);
					
					//System.out.println("json : " +json);
					nom=(json.has("denomination")) ? json.getString("denomination").toUpperCase() :"";
					adresse = (json.has("address")) ? json.getString("address").toUpperCase() :"";
					siren = (json.has("siren")) ? json.getString("siren") : "";
					codenaf = (json.has("codenaf")) ? json.getString("codenaf") :"";
					activite = (json.has("activities")) ? json.getString("activities") :"";
					marque = (json.has("marques")) ? json.getString("marques") :"";
					telephone = (json.has("tel")) ? json.getString("tel") : "";
					formejuridique = (json.has("formjuridique")) ? json.getString("formjuridique") : "";
					siret = (json.has("siret")) ? json.getString("siret") :"";
					orias = (json.has("orias")) ? json.getString("orias") :"";
					prestation = (json.has("prestations")) ? json.getString("prestations") :"";
					
					if(json.has("denomination") && json.has("address")) {
						this.saveItem(activiteSoumis, depSoumis, nom, adresse, orias, prestation, client, telephone, siren, siret, activite, codenaf, marque, formejuridique);
						
					}
					
					System.out.println("Nom : "+nom);
					System.out.println("adresse : "+adresse);
					System.out.println("activite : "+activite);
					System.out.println("activiteSoumis : "+activiteSoumis);
					System.out.println("depSoumis : "+depSoumis);
					System.out.println("telephone : "+telephone);
					System.out.println("siren : "+siren);
					System.out.println("codenaf : "+codenaf);
					System.out.println("marque : "+marque);
					System.out.println("siret : "+siret);
					System.out.println("orias : "+orias);
					System.out.println("formejuridique : "+formejuridique);
					System.out.println("prestation : "+prestation);
					System.out.println("\n- - - - - - - - - - - - - - - - - - - - - - - - - - - - \n");
				}
				
			
			}else {
				System.err.println("Aucun resultat!");
			}
		}
		
		
	}

/*
	public boolean isDep() {
		return isDep;
	}

	public ServicePJAssurance2 setDep(boolean isDep) {
		this.isDep = isDep;
		return this;
	}
*/
	public void quit() {
		ch.close();
		System.out.println("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\nScrap assurance terminé!\n!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\n");
	}

}
