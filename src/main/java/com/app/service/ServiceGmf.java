package com.app.service;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.SocketTimeoutException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Scanner;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.app.model.GMF;

import com.app.scrap.RepGmf;
import com.app.utils.BrowserUtils;
import com.app.utils.Parametres;
import com.app.utils.Utilitaires;

import fa.browserUtils.FactoryBrowser;
import fa.browserUtils.HtmlUnitBrowser;
import fa.log.LogApp.LogScrap;
import fa.log.config.LogConst;

@Service
public class ServiceGmf extends Utilitaires {

	@Autowired
	private RepGmf rep;

	static String driverPath = "C:\\ChromeDriver\\";

	private List<com.app.model.GMF> gmfList = new ArrayList<>();
	String nomSITE = "GMF";
	String ficval = "lien_gmf";
	String dossierRESU = Parametres.REPERTOIRE_RESULT + "\\ASSU\\" + nomSITE;;
	String url_accueil = "https://www.gmf.fr";
	String fichier_valeurs;
	String DepSoumis;
	String ligne;

	int num_ligne = 2;
	int NbAgences;
	int numreponse = 1;
	int nbRecuperes;
	int num_ligneficval;
	int parcoursficval;

	HtmlUnitDriver htmlUnitDriver;
	BufferedWriter sortie;
	BufferedWriter sortie_log;
	Date Temps_Fin;

	public GMF saveData(GMF t) {

		return rep.save(t);

	}

	public List<GMF> saveDataAll(List<GMF> t) {

		return rep.saveAll(t);

	}

	public List<GMF> getElmnt() {

		return (List<GMF>) rep.findAll();
	}

	public GMF getById(int id) {

		return rep.findById(id).orElse(null);
	}

	public String deleteProduct(int id) {
		try {
			rep.deleteById(id);

		} catch (Exception e) {
			e.printStackTrace();
		}

		return "removed";
	}

	public GMF updateProduct(GMF t) {
		// GMFODO Auto-generated method stub
		return null;
	}

	public GMF setData(String depSoumis, String nbAgences, String villeSoumise, String nomAgence, String adresse,
			String tel, String fax, String horaires, String agenceHtm) {
		GMF gmf = new GMF();
		gmf.setDepSoumis(depSoumis).setNbAgences(nbAgences).setVilleSoumise(villeSoumise).setNomAgence(nomAgence)
				.setAdresse(adresse).setTel(tel).setFax(fax).setHoraires(horaires).setAgenceHtm(agenceHtm);
		this.saveData(gmf);
		return gmf;
	}

	public List<GMF> showFirstRows() {
		List<GMF> a = rep.getFirstRow(100, GMF.class);
		return a;
	}

	public List<GMF> showLastRows() {
		List<GMF> a = rep.getLastRow(100, GMF.class);
		return a;
	}

	public void setUp() throws Exception {
		FactoryBrowser fact = new FactoryBrowser();
		HtmlUnitBrowser hB = (HtmlUnitBrowser) fact.create("htmlUnit");
		htmlUnitDriver = hB.setOptions(false, false, false, 60000).initBrowserDriver();
	}

	public List<com.app.model.GMF> open_GMF_byselenium() throws Exception {
		fichier_valeurs = Parametres.REPERTOIRE_DATA_SOURCES + ficval + ".txt";
		Scanner sc = new Scanner(new BufferedInputStream(new FileInputStream(new File(fichier_valeurs))));

		if (!new File(dossierRESU).exists())
			new File(dossierRESU).mkdirs();

		fichier = new File(dossierRESU + "\\" + "RESULTATS_" + nomSITE + "_" + System.currentTimeMillis() + ".txt");
		LogScrap log = new LogScrap("AssuranceGMF.txt", LogConst.TYPE_LOG_FILE);
		num_ligneficval = ((new File(LogConst.STORE_LOG_PATH + LogConst.PREFIX_LOG_FILE + "_AssuranceGMF.txt")).exists())
				? Integer.parseInt(log.readLog().getLastLineProcessed().split("\\t")[4])
				: 1;

		parcoursficval = 1;
		while (sc.hasNextLine()) {
			Document doc = null;
			List<String> listLink = new ArrayList<>();
			String line = sc.nextLine(), html = "";
			String[] fic_valeurs = line.split("\\t");
			if (parcoursficval >= num_ligneficval) {
				if (!listLink.isEmpty())
					listLink.clear();
				nbRecuperes = 0;
				DepSoumis = fic_valeurs[0];
				//String url = url_accueil + DepSoumis;
				String url = url_accueil + fic_valeurs[1];
				System.out.println("url : "+url);
				try {
					htmlUnitDriver.get(url);
					System.out.println("on c'est connecté");
				} catch (Exception e) {
					if (e instanceof SocketTimeoutException) {
						this.tearDown();
						this.setUp();
						htmlUnitDriver.get(url);

					}
				}
				html = htmlUnitDriver.getPageSource();
				doc = Jsoup.parse(html);
				Elements listAgencyLinks = doc
						.select("div.zo-page.bodywrapper > div.zo-content > section:nth-child(3) > "
								+ "div > div.block.sp-agencies > div.markers-list.js-markers-list > ul > li >h2>a ");
				String agencyName = "", agencyAddress = "", agencySchedule = "", agencyTel = "", agencyFax = "",
						agencyHtm = "";
				if (listAgencyLinks != null) {
					String str = "";
					for (Element tmp : listAgencyLinks) {
						str = tmp.attr("href");
						listLink.add(str);

					}
					NbAgences = listLink.size();

					for (String strTmp : listLink) {
						nbRecuperes++;
						String urlGMF = "https://www.gmf.fr";
						url = urlGMF + strTmp;
						System.out.println(url);
						try {
							htmlUnitDriver.get(url);
							System.out.println("on c'est connecté : "+url);
						} catch (Exception e) {
							if (e instanceof SocketTimeoutException) {
								this.tearDown();
								this.setUp();
								htmlUnitDriver.get(url);
							}
						}
						html = htmlUnitDriver.getPageSource();
						doc = Jsoup.parse(html);
						// fetch agency name
						Element infosContainer = doc.selectFirst(
								"body>" + "div.zo-page.bodywrapper>" + "div.zo-content>" + "section:nth-child(2)>"
										+ "div>" + "div>" + "div.agency-detail >" + "div.agency");
						if (infosContainer != null)

						{

							agencyName = "";
							agencyAddress = "";
							agencySchedule = "";
							agencyTel = "";
							agencyFax = "";
							agencyHtm = "";
							agencyHtm = infosContainer.outerHtml().replaceAll("\\s{1,}", " ").trim();
							//body > div.zo-page.bodywrapper > div.zo-content > section:nth-child(2) > div > div > div.agency-detail > div > div:nth-child(1) > h1
							Element agencyNameElement = infosContainer.select("div:nth-child(1)>h1[itemprop=\"name\"]")
									.first();
							agencyName = (agencyNameElement != null) ? this.normalize(agencyNameElement.text().trim())
									: "";
							System.out.println("le nom de l'agence est : " + agencyName);

							// fetch agency address
							Element agencyElementAddress = infosContainer
									.select("div:nth-child(1)>address[itemprop=\"address\"]").first();
							agencyAddress = (agencyElementAddress != null)
									? this.normalize(agencyElementAddress.text().replaceAll("\\s{1,}", " ").trim())
									: "";
							System.out.println("l'adresse de l'agence est : " + agencyAddress);

							// fetch agency tel
							Elements agencyPhoneElement = infosContainer.select("div:nth-child(1)>p");
							for (Element tmp : agencyPhoneElement) {
								String value = tmp.text();
								if (value.contains("Tel :")) {
									agencyTel = value.replaceAll("[^0-9]", "");
								} else {
									agencyFax = value.replaceAll("[^0-9]", "");
								}
							}
							
							System.out.println("tel : " + agencyTel);
							System.out.println("fax : " + agencyFax);


							// fetch agency schedule
							Elements agencyScheduleElement = infosContainer.select("table > tbody >tr");
							agencySchedule = "";
							if (agencyScheduleElement != null) {
								for (Element elmtTmp : agencyScheduleElement) {
									agencySchedule += elmtTmp.text().replaceAll("\\s{1,}", " ").trim() + " ";
								}
							}
							agencySchedule = this.normalize(agencySchedule);
							System.out.println("l'horraire de l'agence est : " + agencySchedule);

						}

						this.setData(DepSoumis, String.valueOf(NbAgences), strTmp, agencyName, agencyAddress, agencyTel,
								agencyFax, agencySchedule, agencyHtm);
						
						System.out.println("----------------------------------------------\n");
					}
					System.out.println("+++++++++++++++++++++\n");
				}
				log.writeLog(new Date(), DepSoumis, "no_error", NbAgences, parcoursficval, 0);
			}

			parcoursficval++;

		} // fin tant que fichier de valeurs non fini
		sc.close();
		return gmfList;
	}

	public void enregistrer(String ville, String nom_agence, String adresse, String tel, String fax, String Horaires,
			String agence) {

//    	this.fichier = new File(Parametres.REPERTOIRE + "RESULTATS_BNP.");
		try {

			sortie = new BufferedWriter(new FileWriter(fichier, true));
			sortie.write(DepSoumis + "\t" + NbAgences + "\t" + ville + "\t" + nom_agence + "\t" + adresse + "\t" + tel
					+ "\t" + fax + "\t" + Horaires + "\t" + agence + "\r\n");

			sortie.close();
		} catch (IOException e) {

			e.printStackTrace();
		}

	}

	public void enregistrer_journal(int numreponse) {

//    	this.fichier = new File(Parametres.REPERTOIRE + "RESULTATS_BNP.");
		try {
			Temps_Fin = new Date();
			SimpleDateFormat formater = null;
			formater = new SimpleDateFormat("'le' dd/MM/yyyy '�' hh:mm:ss");
			String date = formater.format(Temps_Fin);
			date = formater.format(Temps_Fin);

			sortie_log = new BufferedWriter(new FileWriter(log, true));
			sortie_log.write(parcoursficval + ";" + "FinValeur" + ";" + DepSoumis + ";" + this.NbAgences + ";"
					+ nbRecuperes + ";" + (this.NbAgences - nbRecuperes) + ";" + date + "\r\n");
			sortie_log.close();
			long taille_fichier = fichier.length() / 1000000; // la length est en octets, on divise par 1000 pour mettre
																// en Ko et par 1000000 pour mettre en Mo

			if (taille_fichier > 8) {
				// INITIALISATION FICHIER DE RESULTAT
				this.fichier = new File(
						dossierRESU + "\\" + "RESULTATS_" + nomSITE + "_" + System.currentTimeMillis() + ".txt");
				// Attention pas d'accent dans les noms de colonnes !
				sortie = new BufferedWriter(new FileWriter(fichier, true));
				sortie.write("DepSoumis" + "\t" + "NbAgences" + "\t" + "VILLE_SOUMISE" + "\t" + "NOM_AGENCE" + "\t"
						+ "ADRESSE" + "\t" + "TEL" + "\t" + "FAX" + "\t" + "HORAIRES" + "\t" + "AGENCEHTM" + "\r\n");
				sortie.close();
			}

		} catch (IOException e) {

			e.printStackTrace();
		}

	}

	/**
	 * La m�thode <code>initialiser()</code> permet d'ajouter la ligne d'ent�te dans
	 * le fichier r�sultat.
	 */

	public void initialiser() {

		try {
			// Attention pas d'accent dans les noms de colonnes !
			sortie = new BufferedWriter(new FileWriter(fichier, true));
			sortie.write("DepSoumis" + "\t" + "NbAgences" + "\t" + "VILLE_SOUMISE" + "\t" + "NOM_AGENCE" + "\t"
					+ "ADRESSE" + "\t" + "TEL" + "\t" + "FAX" + "\t" + "HORAIRES" + "\t" + "AGENCEHTM" + "\r\n");
			sortie.close();

			File toto = new File(dossierRESU + "\\" + "LOG_" + nomSITE + ".csv");
			if (!toto.exists()) {
				// if (num_ligneficval == num_ligne) {
				sortie_log = new BufferedWriter(new FileWriter(log, true));
				sortie_log.write("NumLigne" + ";" + "FinValeur" + ";" + "DepSoumis" + ";" + "NbAgences" + ";"
						+ "Nbrecuperes" + ";" + "Reste" + ";" + "Heure_Fin" + "\r\n");
				sortie_log.close();
			}

		} catch (IOException e) {

			e.printStackTrace();
		}
	}

	public void tearDown() {
		htmlUnitDriver.quit();

	}

}
