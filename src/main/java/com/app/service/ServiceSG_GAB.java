package com.app.service;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.SocketTimeoutException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Scanner;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.junit.Before;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.PageLoadStrategy;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.app.model.SG_GAB;

import com.app.scrap.RepSG_GAB;
import com.app.utils.BrowserUtils;
import com.app.utils.Parametres;
import com.app.utils.Utilitaires;

import fa.browserUtils.ChromeBrowser;
import fa.browserUtils.FactoryBrowser;
import fa.log.LogApp.LogScrap;
import fa.log.config.LogConst;

@Service
public class ServiceSG_GAB extends Utilitaires {

	@Autowired
	private RepSG_GAB rep;

	String driverPath = "C:\\ChromeDriver\\";
	private List<com.app.model.SG_GAB> sgGabList = new ArrayList<>();
	String nomSITE = "SG_GAB";
	String ficval = "departements_sans20";
	String dossierRESU = Parametres.REPERTOIRE_RESULT + "\\BQ\\" + nomSITE;;
	String url_accueil = "https://agences.societegenerale.fr/banque-assurance/";
	String fichier_valeurs;
	String CPSoumis;
	String ligne;
	int num_ligne = 2;
	int NbAgences;
	int numreponse = 1;
	int DAB = 1;
	int HORS_SITE;
	int nbRecuperes;
	int num_ligneficval;
	int parcoursficval;
	int Resultats;

	ChromeDriver chromeDriver;
	BufferedWriter sortie;
	BufferedWriter sortie_log;

	Date Temps_Fin;

	public SG_GAB saveData(SG_GAB t) {

		return rep.save(t);

	}

	public List<SG_GAB> saveDataAll(List<SG_GAB> t) {

		return rep.saveAll(t);

	}

	public List<SG_GAB> getElmnt() {

		return rep.findAll();
	}

	public SG_GAB getById(int id) {

		return rep.findById(id).orElse(null);
	}

	public String deleteProduct(int id) {
		try {
			rep.deleteById(id);

		} catch (Exception e) {
			e.printStackTrace();
		}

		return "removed";
	}
	
	public String deleteAll() {
		try {
			
			rep.deleteAll();
			System.out.println("Supprimee avec success");

		} catch (Exception e) {
			e.printStackTrace();
		}

		return "removed";
	}

	public SG_GAB updateProduct(SG_GAB t) {
		// SG_GABODO Auto-generated method stub
		return null;
	}

	public SG_GAB setData(String CPSoumis, String nbAgences, String nomWeb, String adresse, String dab,
			String hors_site, String agenceHtm, String codeGuichet) {

		SG_GAB sgGAB = new SG_GAB();
		sgGAB.setCpSoumis(CPSoumis).setNbAgences(nbAgences).setNomWeb(nomWeb).setAdresse(adresse).setDab(dab)
				.setHors_site(hors_site).setAgenceHtm(agenceHtm).setCodeGuichet(codeGuichet);
		this.saveData(sgGAB);
		return sgGAB;
	}

	public List<SG_GAB> showFirstRows() {
		List<SG_GAB> a = rep.getFirstRow(100, SG_GAB.class);
		return a;
	}

	public List<SG_GAB> showLastRows() {
		List<SG_GAB> a = rep.getLastRow(100, SG_GAB.class);
		return a;
	}

	public void setup() {
		FactoryBrowser factBrowser = new FactoryBrowser();
		ChromeBrowser b = (ChromeBrowser) factBrowser.create("chrome");
		final String binaryPath = "C:\\Program Files\\Google\\Chrome Beta\\Application\\chrome.exe";
		chromeDriver = b.setOptions(PageLoadStrategy.NORMAL, driverPath, 60000, 60000, 60000, false, false, binaryPath)
				.initBrowserDriver();
	}

	public List<com.app.model.SG_GAB> scrapSG_GAB() throws IOException, InterruptedException {
		boolean documentReadyState = false;
		JavascriptExecutor jsExecutor = (JavascriptExecutor) chromeDriver;
		String javaScript = "";
		fichier_valeurs = Parametres.REPERTOIRE_DATA_SOURCES + ficval + ".txt";
		Scanner sc = new Scanner(new File(fichier_valeurs));
		File f = new File(dossierRESU);
		if (!f.exists()) {
			f.mkdirs();
		}
		fichier = new File(dossierRESU + "\\" + "RESULTATS_" + nomSITE + "_" + System.currentTimeMillis() + ".txt");
		LogScrap log = new LogScrap("AssuranceSG_GAB.txt", LogConst.TYPE_LOG_FILE);
		num_ligneficval = ((new File(LogConst.STORE_LOG_PATH + LogConst.PREFIX_LOG_FILE + "_AssuranceSG_GAB.txt"))
				.exists()) ? num_ligneficval = Integer.parseInt(log.readLog().getLastLineProcessed().split("\\t")[4])
						: 1;

		parcoursficval = 0;
		while (sc.hasNextLine()) {
			String line = sc.nextLine();
			String[] fic_valeurs = line.split("\\t");
			if (parcoursficval >= num_ligneficval) {
				CPSoumis = fic_valeurs[0];
				String pageSource = "";
				try {
					chromeDriver.get(url_accueil);

				} catch (Exception e) {
					if (e instanceof SocketTimeoutException) {
						chromeDriver.get(url_accueil);

					}
				}
				/*documentReadyState = this.listensIfPageFullyLoaded(chromeDriver);

				if (documentReadyState) {
					Thread.sleep(10000);*/
					pageSource = chromeDriver.getPageSource();

					if (pageSource.contains("Information Coronavirus")) {
						try {
							WebElement annonceCovid = chromeDriver.findElement(By.cssSelector("#dlg-popin-0 > a"));
							annonceCovid.click();
						} catch (Exception e) {
							if (e instanceof NoSuchElementException) {

							}
						}

					}

					WebElement radioCheck = chromeDriver.findElement(By.cssSelector("#searchdab"));
					// radioCheck.click();
					javaScript = String.format("arguments[0].click()", "");
					jsExecutor.executeScript(javaScript, radioCheck);

					WebElement input = chromeDriver.findElement(By.cssSelector("#searchcity"));
					input.sendKeys(CPSoumis);

					WebElement btnSearch = chromeDriver
							.findElement(By.cssSelector("#searchform > fieldset.bloc.bloc-search > button"));
					// btnSearch.click();
					javaScript = String.format("arguments[0].click()", "");
					jsExecutor.executeScript(javaScript, btnSearch);

					/*documentReadyState = this.listensIfPageFullyLoaded(chromeDriver);
					if (documentReadyState) {
						Thread.sleep(10000);*/
						String agences_HTM = "", nom_agence = "", adresse = "", heure = "", heureNormal = "", heureRdv = "", codeGuichet = "";

						pageSource = chromeDriver.getPageSource();
						Document doc = Jsoup.parse(pageSource);
						Element root = doc.select("#agencies").first();

						if (root != null) {
							//Elements agenciesList = root.select("div > div.jspPane > li");
							Elements agenciesList = root.select("li");
							NbAgences = agenciesList.size();
							nbRecuperes = 0;
							System.out.println("NbAgences : " + NbAgences);
							for (Element tmp : agenciesList) {
								agences_HTM = "";
								nom_agence = "";
								adresse = "";
								codeGuichet="";

								DAB = 1;
								agences_HTM = this.normalize(tmp.outerHtml().replaceAll("\\s{1,}", ""));

								// retrieve agency name
								Element agencyNameElement = tmp.select("div.agency-inner > div.agencylabel > a")
										.first();
								nom_agence = (agencyNameElement != null) ? this.normalize(agencyNameElement.text())
										: "";
								System.out.println("nom agence : " + nom_agence);

								// retrieve agency address
								Element agencyAddressElement = tmp.select("div.agency-inner > div.agencyaddress")
										.first();
								adresse = (agencyAddressElement != null) ? this.normalize(agencyAddressElement.text())
										: "";
								System.out.println("adresse : " + adresse);
								
								Element codeGuichetElement=tmp.selectFirst("div.agency-inner > div.guichet-bloc > span:nth-child(2)");
								codeGuichet=(codeGuichetElement!=null) ? codeGuichetElement.text().replaceAll("[^0-9]",""):"";
								System.out.println("codeGuichet : "+codeGuichet);
								
								// retrieve agency horaires
								/*Elements agencyHorairesElementNormal = tmp.select("div.agency-inner > div.horaires > div.horaires-tab-wrapper > div:nth-child(2) > div");
								
								Elements agencyHorairesElementRdv = tmp.select("div.agency-inner > div.horaires > div.horaires-tab-wrapper > div:nth-child(4) > div");
								
								for(Element heureNormEl : agencyHorairesElementNormal) {
									
								}
								
								for(Element heureRdvEl : agencyHorairesElementRdv) {
									
								}
								
								
								System.out.println("heure : " + heure);*/

								if (agences_HTM.contains("class=\"btntel btn btn2 cta-phone\"")) {
									HORS_SITE = 1;
								} else {
									HORS_SITE = 0;
								}

								this.setData(CPSoumis, String.valueOf(NbAgences), nom_agence, adresse,
										String.valueOf(DAB), String.valueOf(HORS_SITE), agences_HTM, codeGuichet);
								nbRecuperes++;
							}
							//this.enregistrer_journal(nbRecuperes);
						} else {
							NbAgences = 0;
							DAB = 0;
							HORS_SITE = 0;
							this.enregistrer(agences_HTM, nom_agence, adresse);
							this.enregistrer_journal(0);

						}
					//}
				//}
				log.writeLog(new Date(), CPSoumis, "no_error", NbAgences, parcoursficval, 0);
			}
			parcoursficval++;
		}
		sc.close();
		return sgGabList;

	}

	public void tearDowns() {
		chromeDriver.close();
	}

	public void enregistrer(String agences_HTM, String nom_agence, String adresse) {

//    	this.fichier = new File(Parametres.REPERTOIRE + "RESULTATS_BNP.");
		try {

			sortie = new BufferedWriter(new FileWriter(fichier, true));
			sortie.write(CPSoumis + "\t" + NbAgences + "\t" + nom_agence + "\t" + adresse + "\t" + DAB + "\t"
					+ HORS_SITE + "\t" + agences_HTM + "\r\n");

			sortie.close();
		} catch (IOException e) {

			e.printStackTrace();
		}

	}

	public void enregistrer_journal(int numreponse) {

//    	this.fichier = new File(Parametres.REPERTOIRE + "RESULTATS_BNP.");
		try {
			Temps_Fin = new Date();
			SimpleDateFormat formater = null;
			formater = new SimpleDateFormat("'le' dd/MM/yyyy '�' hh:mm:ss");
			String date = formater.format(Temps_Fin);
			date = formater.format(Temps_Fin);

			sortie_log = new BufferedWriter(new FileWriter(log, true));
			sortie_log.write(parcoursficval + ";" + "FinValeur" + ";" + CPSoumis + ";" + this.NbAgences + ";"
					+ nbRecuperes + ";" + (this.NbAgences - nbRecuperes) + ";" + date + "\r\n");
			sortie_log.close();
			long taille_fichier = fichier.length() / 1000000; // la length est en octets, on divise par 1000 pour mettre
																// en Ko et par 1000000 pour mettre en Mo

			if (taille_fichier > 4) {
				// INITIALISATION FICHIER DE RESULTAT
				this.fichier = new File(
						dossierRESU + "\\" + "RESULTATS_" + nomSITE + "_" + System.currentTimeMillis() + ".txt");
				// Attention pas d'accent dans les noms de colonnes !
				sortie = new BufferedWriter(new FileWriter(fichier, true));
				sortie.write("DepSoumis" + "\t" + "NbAgences" + "\t" + "NOMWEB" + "\t" + "ADRESSE" + "\t" + "TEL" + "\t"
						+ "HORAIRESHTM" + "\t" + "HORAIRESTXT" + "\t" + "DAB" + "\t" + "SERVICES" + "\t" + "AGENCEHTM"
						+ "\r\n");
				sortie.close();
			}

		} catch (IOException e) {

			e.printStackTrace();
		}

	}

	/**
	 * La m�thode <code>initialiser()</code> permet d'ajouter la ligne d'ent�te dans
	 * le fichier r�sultat.
	 */

	public void initialiser() {

		try {
			// Attention pas d'accent dans les noms de colonnes !
			sortie = new BufferedWriter(new FileWriter(fichier, true));
			sortie.write("CPSoumis" + "\t" + "NbAgences" + "\t" + "NOMWEB" + "\t" + "ADRESSE" + "\t" + "DAB" + "\t"
					+ "HORS_SITE" + "\t" + "AGENCEHTM" + "\r\n");
			sortie.close();

			File toto = new File(dossierRESU + "\\" + "LOG_" + nomSITE + ".csv");
			if (!toto.exists()) {
				// if (num_ligneficval == num_ligne) {
				sortie_log = new BufferedWriter(new FileWriter(log, true));
				sortie_log.write("NumLigne" + ";" + "FinValeur" + ";" + "DepSoumis" + ";" + "NbAgences" + ";"
						+ "Nbrecuperes" + ";" + "Reste" + ";" + "Heure_Fin" + "\r\n");
				sortie_log.close();
			}

		} catch (IOException e) {

			e.printStackTrace();
		}
	}
}
