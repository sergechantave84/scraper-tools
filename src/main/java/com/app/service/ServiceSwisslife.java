
package com.app.service;

import java.io.BufferedInputStream;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Scanner;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import org.openqa.selenium.NoAlertPresentException;

import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.app.model.Swisslife;

import com.app.scrap.RepSwisslife;

import com.app.utils.Parametres;
import com.app.utils.Utilitaires;

import fa.browserUtils.FactoryBrowser;
import fa.browserUtils.HtmlUnitBrowser;
import fa.log.LogApp.LogScrap;
import fa.log.config.LogConst;

@Service
public class ServiceSwisslife extends Utilitaires {

	@Autowired
	private RepSwisslife rep;

	String driverPath = "C:\\ChromeDriver\\";
	String nomSITE = "SWISSLIFE";
	String ficval = "lien_swisslife";
	String dossierRESU = Parametres.REPERTOIRE_RESULT + "\\ASSU\\" + nomSITE;
	String url_accueil = "https://agences.swisslife-direct.fr";
	String fichier_valeurs;
	String depSoumis;
	String VilleSoumise;
	String ligne;

	int num_ligne = 2;
	int NbVilles;
	int NbAgences;
	int numreponse = 1;
	int nbRecuperes;
	int num_ligneficval;
	int parcoursficval;
	int Resultats;

	HtmlUnitDriver htmlUnitDriver;
	BufferedWriter sortie;
	BufferedWriter sortie_log;

	Date Temps_Fin;

	

	public Swisslife setData(String cantonSoumis, String nbAgences, String nbSwissliferouves, String nomAgent,
			String adresse, String tel, String agenceHtm, String orias) {
		Swisslife sw = new Swisslife();
		sw.setCantonSoumis(cantonSoumis).setNbAgences(nbAgences).setNbTrouves(nbSwissliferouves).setNomAgent(nomAgent)
				.setAdresse(adresse).setTel(tel).setOrias(orias).setAgenceHtm(agenceHtm);
		rep.save(sw);
		return sw;

	}

	
	public void setUp() throws Exception {
		FactoryBrowser factBrowser = new FactoryBrowser();
		HtmlUnitBrowser hB = (HtmlUnitBrowser) factBrowser.create("htmlUnit");
		htmlUnitDriver = hB.setOptions(false, false, false, 60000).initBrowserDriver();
	}

	public void open_SWISSLIFE_byselenium() throws Exception {
		// boolean dReadyState=false;
		Document doc = null;
		fichier_valeurs = Parametres.REPERTOIRE_DATA_SOURCES + ficval + ".txt";
		List<String> paths = new ArrayList<>();
		Scanner sc = new Scanner(new BufferedInputStream(new FileInputStream(new File(fichier_valeurs))));
		
		
		LogScrap log = new LogScrap("AssuranceSwisslife.txt", LogConst.TYPE_LOG_FILE);
		num_ligneficval = ((new File(LogConst.STORE_LOG_PATH + LogConst.PREFIX_LOG_FILE + "_AssuranceSwisslife.txt"))
				.exists()) ? num_ligneficval = Integer.parseInt(log.readLog().getLastLineProcessed().split("\\t")[4])
						: 1;

		parcoursficval = 1;
		String html = "";
		while (sc.hasNextLine()) {
			
			String[] fic_valeurs = sc.nextLine().split("\\t");
			if (parcoursficval > num_ligneficval) {
				depSoumis = fic_valeurs[0];
				String uri = url_accueil + fic_valeurs[1];
				try {
					System.out.println("le departement soumis est : " + uri);
					htmlUnitDriver.get(uri);
				} catch (Exception e) {
					this.setUp();
					htmlUnitDriver.get(uri);
				}
				html = htmlUnitDriver.getPageSource();
				doc = Jsoup.parse(html);
				if (!paths.isEmpty()) {
					paths.clear();
				}
				Elements pathElements = doc
						.select("#agencies > li > div > div.agencyblock2 > div.agencyinfos > div.agencylabel > a");
				if (!pathElements.isEmpty()) {
					for (Element pathElement : pathElements) {
						paths.add(pathElement.attr("href"));
					}
					if (!paths.isEmpty()) {
						NbAgences = paths.size();
						for (String path : paths) {
							
							nbRecuperes++;
							String uriMoreInfo = url_accueil + path;
							System.out.println("uri soumis est : " + uriMoreInfo);
							try {
								htmlUnitDriver.get(uriMoreInfo);
							} catch (Exception e) {
								htmlUnitDriver.get(uriMoreInfo);
							}
							html = htmlUnitDriver.getPageSource();
							doc = Jsoup.parse(html);
							Element root = doc.selectFirst(
									"#aside > div.presentation-bloc > " + "div > div.agency-bloc > div.info");
							if (root != null) {
								String agentName = "", orias = "", agencyPhone = "", agencyAddress = "", agencyHtm = "";
								agencyHtm = this.normalize(root.outerHtml().replaceAll("\\s{1,}", ""));

								Element agentNameElement = root.selectFirst("h3.name");
								agentName = (agentNameElement != null)
										? this.normalize(agentNameElement.text().toLowerCase())
										: "";
								agentName = agentName.replaceAll("agent général d'assurance swisslife", "");
								System.out.println("nom agent : " + agentName);

								Element oriasElement = root.selectFirst(".orias");
								orias = (oriasElement != null) ? oriasElement.text().replaceAll("[^0-9 ]{1,}", "") : "";
								System.out.println("orias : " + orias);

								Element phoneElement = root.selectFirst("div.agencytel-bloc > div.tel-bloc > a");
								agencyPhone = (phoneElement != null)
										? phoneElement.attr("href").replaceAll("[^0-9]{1,}", "")
										: "";
								System.out.println("phone : " + agencyPhone);

								Element adresseElement = root
										.selectFirst("div.agencyaddress-bloc>" + "div.address-bloc");
								agencyAddress = (adresseElement != null) ? this.normalize(adresseElement.text()) : "";
								System.out.println("adresse : " + agencyAddress);
								System.out.println("--------------------------------\n ");

								this.setData(depSoumis, String.valueOf(NbAgences), String.valueOf(NbAgences), agentName,
										agencyAddress, agencyPhone, agencyHtm, orias);
							}
						}
					}
				}
				log.writeLog(new Date(), depSoumis, "no_error", NbAgences, parcoursficval, 0);
			}
			parcoursficval++;
		}

	}

	

	public void tearDown() {
		htmlUnitDriver.quit();

	}

	

}
