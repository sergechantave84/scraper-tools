package com.app.scrap;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


import com.app.model.CA_Outre_Mer;

@Repository
public interface RepCaOutreMer extends JpaRepository<CA_Outre_Mer, Integer>, RepositoryCustom<CA_Outre_Mer> {

}
