package com.app.scrap;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.app.model.Thelem;
import com.app.model.Thelem_DOM;

@Repository
public interface RepThelemDom extends JpaRepository<Thelem_DOM,Integer>,RepositoryCustom<Thelem_DOM> {

}
