package com.app.model;

import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity

@Table(name="banque_BNP_dep_outre_mer", schema="scrap")
public class BNP_Outre_Mer {

	@Id
	@SequenceGenerator(name="scrap.banque_BNP_dep_outre_mer_id_seq",
	                     sequenceName ="scrap.banque_BNP_dep_outre_mer_id_seq",
	                     allocationSize=1)
    @GeneratedValue( strategy=GenerationType.SEQUENCE,
                     generator="scrap.banque_BNP_dep_outre_mer_id_seq")
	  
	@Column(name="id",updatable=false)
	private int id;
	
	@Column(name="valeursoumise",columnDefinition="varchar")
	private String valeurSoumise;
	
	@Column(name="nbagences",columnDefinition="varchar")
	private String nbAgences;
	
	@Column(name="nomweb",columnDefinition="varchar")
	private String nomWeb;
	
	@Column(name="formatagence",columnDefinition="varchar")
	private String formatAgence;
	
	@Column(name="adresse",columnDefinition="varchar")
	private String adresse;
	
	@Column(name="adresse_comp",columnDefinition="varchar")
	private String adresseComp;
	
	@Column(name="cp",columnDefinition="varchar")
	private String cp;
	
	@Column(name="ville",columnDefinition="varchar")
	private String ville;
	
	@Column(name="tel",columnDefinition="varchar")
	private String tel;
	
	@Column(name="fax",columnDefinition="varchar")
	private String fax;
	
	@Column(name="horaires",columnDefinition="varchar")
	private String horaires;
	
	@Column(name="gab",columnDefinition="varchar")
	private String gab;
	
	@Column(name="entreprise",columnDefinition="varchar")
	private String entreprise;
	
	@Column(name="particulier",columnDefinition="varchar")
	private String particulier;
	
	@Column(name="professionnel",columnDefinition="varchar")
	private String professionel;
	
	@Column(name="change",columnDefinition="varchar")
	private String change;
	
	@Column(name="coffre_fort",columnDefinition="varchar")
	private String coffreFort;
	
	@Column(name="depot_permanent",columnDefinition="varchar")
	private String depotPermanent;
	
	@Column(name="automate_billet",columnDefinition="varchar")
	private String automateBillet;
	
	@Column(name="automate_cheque",columnDefinition="varchar")
	private String automateCheque;
	
	@Column(name="latitude",columnDefinition="varchar")
	private String latitude;
	
	@Column(name="longitude",columnDefinition="varchar")
	private String longitude;
	
	@Column(name="annee_du_scrap" ,columnDefinition="varchar(4) DEFAULT date_part('year',CURRENT_DATE)")
	private String anneeScrap;
	

	public BNP_Outre_Mer() {
		LocalDate ld= new Date().toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
		anneeScrap=String.valueOf(ld.getYear()+1);
	}
	public int getId() {
		return id;
	}
	public BNP_Outre_Mer setId(int id) {
		this.id = id;
		return this;
	}
	public String getValeurSoumise() {
		return valeurSoumise;
	}
	public BNP_Outre_Mer setValeurSoumise(String valeurSoumise) {
		this.valeurSoumise = valeurSoumise;
		return this;
	}
	public String getNbAgences() {
		return nbAgences;
	}
	public BNP_Outre_Mer setNbAgences(String nbAgences) {
		this.nbAgences = nbAgences;
		return this;
	}
	public String getNomWeb() {
		return nomWeb;
	}
	public BNP_Outre_Mer setNomWeb(String nomWeb) {
		this.nomWeb = nomWeb;
		return this;
	}
	public String getFormatAgence() {
		return formatAgence;
	}
	public BNP_Outre_Mer setFormatAgence(String formatAgence) {
		this.formatAgence = formatAgence;
		return this;
	}
	public String getAdresse() {
		return adresse;
	}
	public BNP_Outre_Mer setAdresse(String adresse) {
		this.adresse = adresse;
		return this;
	}
	public String getAdresseComp() {
		return adresseComp;
	}
	public BNP_Outre_Mer setAdresseComp(String adresseComp) {
		this.adresseComp = adresseComp;
		return this;
	}
	public String getCp() {
		return cp;
	}
	public BNP_Outre_Mer setCp(String cp) {
		this.cp = cp;
		return this;
	}
	public String getVille() {
		return ville;
	}
	public BNP_Outre_Mer setVille(String ville) {
		this.ville = ville;
		return this;
	}
	public String getTel() {
		return tel;
	}
	public BNP_Outre_Mer setTel(String tel) {
		this.tel = tel;
		return this;
	}
	public String getFax() {
		return fax;
	}
	public BNP_Outre_Mer setFax(String fax) {
		this.fax = fax;
		return this;
	}
	public String getHoraires() {
		return horaires;
	}
	public BNP_Outre_Mer setHoraires(String horaires) {
		this.horaires = horaires;
		return this;
	}
	public String getGab() {
		return gab;
	}
	public BNP_Outre_Mer setGab(String gab) {
		this.gab = gab;
		return this;
	}
	public String getParticulier() {
		return particulier;
	}
	public BNP_Outre_Mer setParticulier(String particulier) {
		this.particulier = particulier;
		return this;
	}
	public String getProfessionel() {
		return professionel;
	}
	public BNP_Outre_Mer setProfessionel(String professionel) {
		this.professionel = professionel;
		return this;
	}
	public String getChange() {
		return change;
	}
	public BNP_Outre_Mer setChange(String change) {
		this.change = change;
		return this;
	}
	public String getCoffreFort() {
		return coffreFort;
	}
	public BNP_Outre_Mer setCoffreFort(String coffreFort) {
		this.coffreFort = coffreFort;
		return this;
	}
	public String getDepotPermanent() {
		return depotPermanent;
	}
	public BNP_Outre_Mer setDepotPermanent(String depotPermanent) {
		this.depotPermanent = depotPermanent;
		return this;
	}
	public String getAutomateBillet() {
		return automateBillet;
	}
	public BNP_Outre_Mer setAutomateBillet(String automateBillet) {
		this.automateBillet = automateBillet;
		return this;
	}
	public String getAutomateCheque() {
		return automateCheque;
	}
	public BNP_Outre_Mer setAutomateCheque(String automateCheque) {
		this.automateCheque = automateCheque;
		return this;
	}
	public String getLatitude() {
		return latitude;
	}
	public BNP_Outre_Mer setLatitude(String latitude) {
		this.latitude = latitude;
		return this;
	}
	public String getLongitude() {
		return longitude;
	}
	public BNP_Outre_Mer setLongitude(String longitude) {
		this.longitude = longitude;
		return this;
	}
	public String getAnneeScrap() {
		return anneeScrap;
	}
	public BNP_Outre_Mer setAnneeScrap(String anneeScrap) {
		this.anneeScrap = anneeScrap;
		return this;
	}
	public String getEntreprise() {
		return entreprise;
	}
	public BNP_Outre_Mer setEntreprise(String entreprise) {
		this.entreprise = entreprise;
		return this;
	}
}
