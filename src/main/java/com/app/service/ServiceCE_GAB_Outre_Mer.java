package com.app.service;

import java.io.BufferedInputStream;

import java.io.File;
import java.io.FileInputStream;

import java.net.SocketTimeoutException;

import java.util.Date;

import java.util.Scanner;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import org.openqa.selenium.htmlunit.HtmlUnitDriver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.app.model.CE_GAB_Outre_Mer;
import com.app.scrap.RepCeGabOutreMer;

import com.app.utils.Parametres;


import fa.log.LogApp.LogScrap;
import fa.log.config.LogConst;

@Service
public class ServiceCE_GAB_Outre_Mer extends Scraper {

	@Autowired
	private RepCeGabOutreMer rep;

	
	String nomSITE = "CE_GAB_Outre_Mer";
	String ficval = "liens_ce_gab_outre_mer";
	String dossierRESU = Parametres.REPERTOIRE_RESULT + "\\BQ\\" + nomSITE;;
	String url_accueil = "https://www.agences.caisse-epargne.fr";
	String fichier_valeurs;
	String cp;
	String VilleSoumise;
	String ligne;


	
	int NbAgences;
   int DAB = 1;
	int HORS_SITE;
	int nbRecuperes;
	int num_ligneficval;
	int parcoursficval;

	static HtmlUnitDriver htmlUnitDriver;

	
	
	/*public void setData(String depSoumis, String nbAgences, String nomWeb, String adresse, String dab,
			String horsSite, String agenceHtm) {
		
		CE_GAB_Outre_Mer ceGab = new CE_GAB_Outre_Mer();
		ceGab.setDepSoumis(depSoumis).setNbAgences(nbAgences).setNomWeb(nomWeb).setAdresse(adresse).setDab(dab)
				.setHorsSite(horsSite).setAgenceHtm(agenceHtm);
		rep.save(ceGab);
	}*/

	

	public void setup() {
		htmlUnitDriver=(HtmlUnitDriver) this.setupHU();
		
	}
	
	@Override
	public void saveItem(String... args) {
		
		CE_GAB_Outre_Mer ceGab = new CE_GAB_Outre_Mer();
		ceGab.setDepSoumis(args[0]).setNbAgences(args[1]).setNomWeb(args[2]).setAdresse(args[3]).setDab(args[4])
				.setHorsSite(args[5]).setAgenceHtm(args[6]);
		rep.save(ceGab);
	}

	@Override
	public void scrap() throws Exception {
		fichier_valeurs = Parametres.REPERTOIRE_DATA_SOURCES + ficval + ".txt";
		Scanner sc = new Scanner(new BufferedInputStream(new FileInputStream(new File(fichier_valeurs))));

		
		LogScrap log = new LogScrap("BanqueCE_GAB_DOM.txt", LogConst.TYPE_LOG_FILE);
		num_ligneficval = ((new File(LogConst.STORE_LOG_PATH + LogConst.PREFIX_LOG_FILE + "_BanqueCE_GAB_DOM.txt")).exists())
				? Integer.parseInt(log.readLog().getLastLineProcessed().split("\\t")[4])
				: 1;

		parcoursficval = 1;
		while (sc.hasNextLine()) {
			
			String[] fic_valeurs = sc.nextLine().split("\\t");
			String url = "";
			if (parcoursficval > num_ligneficval) {
				cp = fic_valeurs[0];
				url = url_accueil + fic_valeurs[1];
				System.out.println(url);
				try {
					htmlUnitDriver.get(url);

				} catch (Exception e) {
					if (e instanceof SocketTimeoutException) {
						htmlUnitDriver.get(url);

					}
				}

				Document doc = null;
				String pageSource = htmlUnitDriver.getPageSource();
				doc = Jsoup.parse(pageSource);
				//System.out.println(pageSource);
				Element rootJElement = doc.selectFirst("body > div.em-page > main");
				//body > div.em-page > main > div.em-results-wrapper > div > div.em-results__list-wrapper > div > div > div.jspPane > ul > li:nth-child(1)
				//rootJElement = doc.selectFirst("main");
				//System.out.println(rootJElement);
				if (rootJElement != null) {
					Elements agencyListJElement = rootJElement.select("div.em-results-wrapper > div > div.em-results__list-wrapper > div > ul > li");
					NbAgences = agencyListJElement.size();
					
					//System.out.println(agencyListJElement);
					
					System.out.println("NbAgences : " + NbAgences);
					
					nbRecuperes = 0;
				

					for (Element tmp : agencyListJElement) {
						String agences_HTM = "", nom_agence = "", adresse = "";
						agences_HTM = tmp.outerHtml().trim().replaceAll("\\s{1,}", " ");
						agences_HTM = this.normalize(agences_HTM);
						if (agences_HTM.contains("agencytel")) {
							HORS_SITE = 0;
						} else {
							HORS_SITE = 1;
						}
						
						System.out.println("HORS_SITE : " + HORS_SITE);
						
						// retrieve agency name
						Element agencyNameJElement = tmp.select("h2").first();
						nom_agence = (agencyNameJElement != null) ? this.normalize(agencyNameJElement.text()) : "";
                        System.out.println("nom_agence : " + nom_agence);
						// retrieve agency adresse
						Element agencyAddressJElement = tmp.select("div.em-poi-card__address").first();
						adresse = (agencyAddressJElement != null) ? this.normalize(agencyAddressJElement.text().trim()) : "";
                        System.out.println("adresse : " + adresse);
						nbRecuperes++;
						/*ceGab.setDepSoumis(args[0]).setNbAgences(args[1]).setNomWeb(args[2]).setAdresse(args[3]).setDab(args[4])
						.setHorsSite(args[5]).setAgenceHtm(args[6]);*/
						this.saveItem(cp, String.valueOf(NbAgences), nom_agence, adresse, String.valueOf(DAB),
								String.valueOf(HORS_SITE), agences_HTM);

					}

				}

				log.writeLog(new Date(), url, "no_error", NbAgences, parcoursficval, NbAgences - nbRecuperes);
			}
			parcoursficval++;
		}

		sc.close();
		
		
	}


	

	public void tearDown() {
		htmlUnitDriver.quit();

	}

	

}
