package com.app.scrap;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.app.model.AXA;
import com.app.model.AXA_DOM;

@Repository
public interface RepAXADom extends JpaRepository<AXA_DOM,Integer>,RepositoryCustom<AXA_DOM>{

}
