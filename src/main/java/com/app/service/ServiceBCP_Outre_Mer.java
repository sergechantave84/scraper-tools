package com.app.service;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.SocketTimeoutException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Scanner;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.PageLoadStrategy;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.app.model.BCP_Outre_Mer;
import com.app.scrap.RepBcp;
import com.app.scrap.RepBcpOutreMer;
import com.app.utils.BrowserUtils;
import com.app.utils.Parametres;
import com.app.utils.Utilitaires;

import fa.browserUtils.ChromeBrowser;
import fa.browserUtils.FactoryBrowser;

@Service
public class ServiceBCP_Outre_Mer extends Utilitaires {

	@Autowired
	private RepBcpOutreMer rep;

	private static String driverPath = "C:\\chromeDriver\\";

	private List<BCP_Outre_Mer> bcpList = new ArrayList<BCP_Outre_Mer>();
	private String nomSITE = "BCP_Outre_Mer";
	private String ficval = Parametres.OUTRE_MER_NOM_FICHIER;
	private String dossierRESU = Parametres.REPERTOIRE_RESULT + "\\BQ\\" + nomSITE;;
	private String url_accueil = "https://fc1.1bis.com/bcp-v2/search.asp";
	private String fichier_valeurs;

	private static ChromeDriver chromeDriver;
	private String ValeurSoumise;

	private int nbRecuperes;
	private int num_ligneficval;
	private int parcoursficval;
	private int nbAgences;
	private int num_ligne = 2;
	private int nbID;

	private BufferedWriter sortie;
	private BufferedWriter sortie_log;
	private Date Temps_Fin;

	public BCP_Outre_Mer saveData(BCP_Outre_Mer t) {

		return rep.save((BCP_Outre_Mer) t);

	}

	public List<BCP_Outre_Mer> saveDataAll(List<BCP_Outre_Mer> t) {
		return rep.saveAll(t);
	}

	public List<BCP_Outre_Mer> getElmnt() {

		return (List<BCP_Outre_Mer>) rep.findAll();
	}

	public BCP_Outre_Mer getById(int id) {

		return (BCP_Outre_Mer) rep.findById(id).orElse(null);
	}

	public String deleteProduct(int id) {
		try {
			rep.deleteById(id);

		} catch (Exception e) {
			e.printStackTrace();
		}

		return "removed";
	}

	public BCP_Outre_Mer updateProduct(BCP_Outre_Mer t) {

		return null;
	}

	public BCP_Outre_Mer setData(String valeurSoumise, String nbAgences, String nbIdBCP_Outre_Merrouves,
			String numAgence, String nomWeb, String adresse, String tel, String horairesHtm,
			String horairesBCP_Outre_Merxt, String agenceHtm) {
		BCP_Outre_Mer bcp = new BCP_Outre_Mer();
		bcp.setValeurSoumise(valeurSoumise).setNbAgences(nbAgences).setNbIdTrouves(nbIdBCP_Outre_Merrouves)
				.setNumAgence(numAgence).setNomWeb(nomWeb).setAdresse(adresse).setTel(tel).setHorairesHtm(horairesHtm)
				.setHorairesTxt(horairesBCP_Outre_Merxt).setAgenceHtm(agenceHtm);
		this.saveData(bcp);
		return bcp;
	}

	public List<BCP_Outre_Mer> showFirstRows() {
		List<BCP_Outre_Mer> a = rep.getFirstRow(100, BCP_Outre_Mer.class);
		return a;
	}

	public List<BCP_Outre_Mer> showLastRows() {
		List<BCP_Outre_Mer> a = rep.getLastRow(100, BCP_Outre_Mer.class);
		return a;
	}

	public void setup() {
		
		FactoryBrowser factBrowser = new FactoryBrowser();
		ChromeBrowser b = (ChromeBrowser) factBrowser.create("chrome");
		/*chromeDriver = b.setOptions(PageLoadStrategy.NORMAL, driverPath, 60000, 60000, 60000, true, false)
				.initBrowserDriver();*/
		final String binaryPath = "C:\\Program Files\\Google\\Chrome Beta\\Application\\chrome.exe";
		
		chromeDriver = b.setOptions(PageLoadStrategy.NORMAL, driverPath, 60000, 60000, 60000, false, false, binaryPath)
				.initBrowserDriver();
		
		//chromeDriver = BrowserUtils.chromeConstruct(driverPath, 60000, 60000, 10, PageLoadStrategy.NORMAL);
	}

	public List<BCP_Outre_Mer> scrapBCP_Outre_Mer() throws IOException {
		String pageSource = "", jsScript = "return document.readyState";
		boolean documentReadyState = false;
		Document doc = null;
		JavascriptExecutor jsExecutor = (JavascriptExecutor) chromeDriver;

		fichier_valeurs = Parametres.REPERTOIRE_DATA_SOURCES + ficval + ".txt";
		Scanner sc = new Scanner(new BufferedInputStream(new FileInputStream(new File(fichier_valeurs))));
		File f = new File(dossierRESU);
		if (!f.exists()) {
			f.mkdirs();
		}
		fichier = new File(dossierRESU + "\\" + "RESULTATS_" + nomSITE + "_" + System.currentTimeMillis() + ".txt");
		log = new File(dossierRESU + "\\" + "LOG_" + nomSITE + ".csv");
		File toto = new File(dossierRESU + "\\" + "LOG_" + nomSITE + ".csv");
		if (toto.exists()) {
			String fichierLog = dossierRESU + "\\" + "LOG_" + nomSITE + ".csv";
			InputStream ips = new FileInputStream(fichierLog);
			InputStreamReader ipsr = new InputStreamReader(ips);
			BufferedReader br = new BufferedReader(ipsr);
			String ligne;
			num_ligneficval = 0;
			while ((ligne = br.readLine()) != null) {
				num_ligneficval = this.compter(ligne, "(\\d+);FinValeur;", true);
			}
			br.close();
		} else {
			num_ligneficval = num_ligne;
		}
		this.initialiser();
		parcoursficval = 1;

		while (sc.hasNextLine()) {
			jsScript = "return document.readyState";
			String line = sc.nextLine();
			String[] temp = line.split("\\t");
			if (parcoursficval >= num_ligneficval) {

				String agencyName = "", agencyAddress = "", agencyPhone = "", agencySchedule = "",
						agencyScheduleHTML = "", agencyHtml = "";
				ValeurSoumise = temp[0];
				String linkAddress = url_accueil + "?cityId="+ValeurSoumise;
				List<WebElement> choice = null;
				try {
					chromeDriver.get(linkAddress);

				} catch (Exception e) {
					if (e instanceof SocketTimeoutException) {
						chromeDriver.get(linkAddress);

					}
				}
				try {

					/*Thread.sleep(1000);

					WebElement input = chromeDriver.findElement(By.cssSelector("#searchcity"));

					input.sendKeys(ValeurSoumise + Keys.ENTER);

					Thread.sleep(2000);*/

					pageSource = chromeDriver.getPageSource();
					doc = Jsoup.parse(pageSource);
					String textIfNoresponse = doc.outerHtml();

					Element rootResult = doc.selectFirst("#agencies");
					Element noResult = doc.selectFirst("#agencies > div > div > li.agency.noresult");
					
					if(noResult!=null) {
						System.out.println("Aucun resultat!");
					}else if(rootResult!=null) {
						Elements agenciesListJElements = rootResult.select("li.agency.agency-type-bcp");


						if (textIfNoresponse.contains("Aucune réponse ne correspond à vos critères de recherche")) {
							System.out.println("Aucune reponse");
							nbRecuperes = 0;
							nbID = 0;
							nbAgences = 0;
							this.enregistrer(agencyName, agencyAddress, agencyPhone, agencyScheduleHTML, agencySchedule,
									agencyHtml, 0);
							this.enregistrer_journal(nbRecuperes);
						} else if (/*rootResult != null && noResult == null*/ agenciesListJElements != null) {

							nbID = agenciesListJElements.size();
							nbAgences = agenciesListJElements.size();
							System.out.println("nbAgences : " + nbAgences);

							int responseNumber = 0;
							for (Element elmnt : agenciesListJElements) {
								agencyScheduleHTML = "";
								agencySchedule = "";
								agencyHtml = elmnt.outerHtml().trim().replaceAll("\\s{1,}", "");
								agencyHtml = this.normalize(agencyHtml);

								Element primaryInfoJElement = elmnt.select(".infoprimary").first();
								Element secondaryInfoJElement = elmnt.select(".infosecondary").first();

								// retrieve agency name
								Element agencyNameJElement = primaryInfoJElement.select("h2").first();
								agencyName = (agencyNameJElement != null) ? agencyNameJElement.text() : "";
								agencyName = this.normalize(agencyName);

								// retrieve agency address
								Element agencyAddressJElement = primaryInfoJElement.select("div.agencyaddress").first();
								agencyAddress = (agencyAddressJElement != null)
										? agencyAddressJElement.text().replaceAll("\\s{1,}", " ")
										: "";
								agencyAddress = this.normalize(agencyAddress);

								// retrieve agency tel
								Element agencyPhoneJElement = secondaryInfoJElement.select(".agencytel").first();
								agencyPhone = (agencyPhoneJElement != null)
										? agencyPhoneJElement.text().replaceAll("[^0-9]", "")
										: "";
								agencyPhone = this.normalize(agencyPhone);

								// retrieve agency schedule
								Elements agencyScheduleJELements = secondaryInfoJElement
										.select(".schedule>div[class*=\"day\"]");
								for (Element elmnt1 : agencyScheduleJELements) {
									agencyScheduleHTML += elmnt1.outerHtml().trim();
									agencySchedule += elmnt1.text().trim() + " ";
								}
								agencySchedule = this.normalize(agencySchedule);
								agencyScheduleHTML = this.normalize(agencyScheduleHTML);
								agencyScheduleHTML = agencyScheduleHTML.replaceAll("\\s{1,}", "");
								responseNumber++;

								this.setData(ValeurSoumise, String.valueOf(nbAgences), String.valueOf(nbID),
										String.valueOf(responseNumber), agencyName, agencyAddress, agencyPhone,
										agencyScheduleHTML, agencySchedule, agencyHtml);
								
								this.enregistrer(agencyName, agencyAddress, agencyPhone, agencyScheduleHTML, agencySchedule,
										agencyHtml, 0);
								this.enregistrer_journal(nbRecuperes);

							}
							nbRecuperes = responseNumber;

						}else {
							
							System.out.println("Nous n'avons pas reconnu l'adresse indiquee");
							nbRecuperes = 0;
							nbID = 0;
							nbAgences = 0;
							this.enregistrer(agencyName, agencyAddress, agencyPhone, agencyScheduleHTML, agencySchedule,
									agencyHtml, 0);
							this.enregistrer_journal(nbRecuperes);
							
						}
					}else {
						System.out.println("Non resultat trouvé");
					}
					

				} catch (Exception e) {

					e.printStackTrace();

				}
			}
			parcoursficval++;
		}
		sc.close();
		return bcpList;
	}

	public void enregistrer(String nomAgence, String adresse, String tel, String HorairesHTM, String HorairesTXT,
			String bloc_detail_agenceHTM, int numreponse) {

//	    	this.fichier = new File(Parametres.REPERTOIRE + "RESULTATS_BCP_Outre_Mer.txt");
		try {

			sortie = new BufferedWriter(new FileWriter(fichier, true));
			sortie.write(ValeurSoumise + "\t" + this.nbAgences + "\t" + nbID + "\t" + numreponse + "\t" + nomAgence
					+ "\t" + adresse + "\t" + tel + "\t" + HorairesHTM + "\t" + HorairesTXT + "\t"
					+ bloc_detail_agenceHTM + "\r\n");

			sortie.close();
		} catch (IOException e) {

			e.printStackTrace();
		}

	}

	public void enregistrer_journal(int numreponse) {

//	    	this.fichier = new File(Parametres.REPERTOIRE + "RESULTATS_BCP_Outre_Mer.txt");
		try {
			Temps_Fin = new Date();
			SimpleDateFormat formater = null;
			formater = new SimpleDateFormat("'le' dd/MM/yyyy '�' hh:mm:ss");
			String dateTXT = formater.format(Temps_Fin);
			dateTXT = formater.format(Temps_Fin);

			sortie_log = new BufferedWriter(new FileWriter(log, true));
			sortie_log.write(parcoursficval + ";" + "FinValeur" + ";" + ValeurSoumise + ";" + this.nbAgences + ";"
					+ nbRecuperes + ";" + (this.nbAgences - nbRecuperes) + ";" + dateTXT + "\r\n");
			sortie_log.close();

		} catch (IOException e) {

			e.printStackTrace();
		}

	}

	/**
	 * La m�thode <code>initialiser()</code> permet d'ajouter la ligne d'ent�te dans
	 * le fichier r�sultat.
	 */

	public void initialiser() {

		try {
			// Attention pas d'accent dans les noms de colonnes !
			sortie = new BufferedWriter(new FileWriter(fichier, true));
			sortie.write("ValeurSoumise" + "\t" + "NbAgences" + "\t" + "nbIDTrouves" + "\t" + "numAgence" + "\t"
					+ "NOMWEB" + "\t" + "ADRESSE" + "\t" + "TEL" + "\t" + "horaires_HTM" + "\t" + "horaires_TXT" + "\t"
					+ "agence_HTM" + "\r\n");
			sortie.close();

			File toto = new File(dossierRESU + "\\" + "LOG_" + nomSITE + ".csv");
			if (!toto.exists()) {
				// if (num_ligneficval == num_ligne) {
				sortie_log = new BufferedWriter(new FileWriter(log, true));
				sortie_log.write("NumLigne" + ";" + "FinValeur" + ";" + "ValeurSoumise" + ";" + "NbAgences" + ";"
						+ "Nbrecuperes" + ";" + "Reste" + ";" + "Heure_Fin" + "\r\n");
				sortie_log.close();
			}

		} catch (IOException e) {

			e.printStackTrace();
		}
	}

	public void tearDown() {
		chromeDriver.quit();

	}
}
