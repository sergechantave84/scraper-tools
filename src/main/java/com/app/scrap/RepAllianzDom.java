package com.app.scrap;

import org.springframework.stereotype.Repository;
import org.springframework.data.jpa.repository.JpaRepository;
import com.app.model.Allianz;
import com.app.model.Allianz_DOM;

@Repository
public interface RepAllianzDom extends JpaRepository<Allianz_DOM,Integer> , RepositoryCustom<Allianz_DOM>{

}
