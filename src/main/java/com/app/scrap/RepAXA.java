package com.app.scrap;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.app.model.AXA;

@Repository
public interface RepAXA extends JpaRepository<AXA,Integer>,RepositoryCustom<AXA>{

}
