package com.app.scrap;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.app.model.AG2R;
import com.app.model.AG2R_DOM;

@Repository
public interface RepAg2r2Dom extends JpaRepository<AG2R_DOM,Integer> ,RepositoryCustom<AG2R_DOM>{

}
