package com.app.model;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="page_jaune_assurance_dep", schema="scrap")
public class PJAssuranceDep extends PJAssurance{

	@Id
	@SequenceGenerator(name="scrap.page_jaune_assurance_dep_id_seq",
	                  sequenceName ="scrap.page_jaune_assurance_dep_id_seq",
	                  allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE,
	generator = "scrap.page_jaune_assurance_dep_id_seq")
	@Column(name="id", unique=true, nullable = false)
	private int id;
	
	@Column(name="activite_soumise",columnDefinition = "varchar")
	private String activiteSoumise;
	
	@Column(name="dep_soumise",columnDefinition = "varchar")
	private String dep;
	
	
	@Column(name="annee_scrap", columnDefinition = "varchar")
	private String anneeScrap;
	
	public PJAssuranceDep() {
		LocalDate dt=new Date().toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
		anneeScrap=String.valueOf(dt.getYear()+1);
	}
	public String getDep() {
		return dep;
	}

	public PJAssuranceDep setDep(String dep) {
		this.dep = dep;
		return this;
	}

	

	public String getAnneeScrap() {
		return anneeScrap;
	}

	public PJAssuranceDep setAnneeScrap(String anneeScrap) {
		this.anneeScrap = anneeScrap;
		return this;
	}
	public String getActiviteSoumise() {
		return activiteSoumise;
	}
	public PJAssuranceDep setActiviteSoumise(String activiteSoumise) {
		this.activiteSoumise = activiteSoumise;
		return this;
	}
	
	
	
}
