package com.app.scrap;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


import com.app.model.BP_GAB_Outre_Mer;

@Repository
public interface RepBpGabOutreMer extends JpaRepository<BP_GAB_Outre_Mer, Integer>,RepositoryCustom<BP_GAB_Outre_Mer> {

}
