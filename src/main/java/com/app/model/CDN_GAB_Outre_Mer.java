package com.app.model;

import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "banque_cdn_gab_dep_outre_mer", schema = "scrap")
public class CDN_GAB_Outre_Mer {

	@Id
	@SequenceGenerator(name = "scrap.banque_cdn_gab_dep_outre_mer_id_seq", sequenceName = "scrap.banque_cdn_gab_dep_outre_mer_id_seq", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "scrap.banque_cdn_gab_dep_outre_mer_id_seq")

	@Column(name = "id", updatable = false)
	private int id;

	@Column(name = "depsoumis", columnDefinition = "varchar")
	private String depSoumis;

	@Column(name = "nbagences", columnDefinition = "varchar")
	private String nbAgences;

	@Column(name = "nomweb", columnDefinition = "varchar")
	private String nomWeb;

	@Column(name = "adresse", columnDefinition = "varchar")
	private String adresse;

	@Column(name = "gabCDN", columnDefinition = "varchar")
	private String gabCDN;

	@Column(name = "gabSG", columnDefinition = "varchar")
	private String gabSG;

	@Column(name = "dab", columnDefinition = "varchar")
	private String dab;

	@Column(name = "hors_site", columnDefinition = "varchar")
	private String horsSite;

	@Column(name = "agencehtm", columnDefinition = "varchar")
	private String agenceHtm;

	@Column(name = "annee_du_scrap", columnDefinition = "varchar(4) DEFAULT date_part('year',CURRENT_DATE)")
	private String anneeScrap;

	public CDN_GAB_Outre_Mer() {
		LocalDate ld = new Date().toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
		anneeScrap = String.valueOf(ld.getYear() + 1);
	}

	public int getId() {
		return id;
	}

	public CDN_GAB_Outre_Mer setId(int id) {
		this.id = id;
		return this;
	}

	public String getDepSoumis() {
		return depSoumis;
	}

	public CDN_GAB_Outre_Mer setDepSoumis(String depSoumis) {
		this.depSoumis = depSoumis;
		return this;
	}

	public String getNbAgences() {
		return nbAgences;
	}

	public CDN_GAB_Outre_Mer setNbAgences(String nbAgences) {
		this.nbAgences = nbAgences;
		return this;
	}

	public String getNomWeb() {
		return nomWeb;
	}

	public CDN_GAB_Outre_Mer setNomWeb(String nomWeb) {
		this.nomWeb = nomWeb;
		return this;
	}

	public String getAdresse() {
		return adresse;
	}

	public CDN_GAB_Outre_Mer setAdresse(String adresse) {
		this.adresse = adresse;
		return this;
	}

	public String getGabCDN() {
		return gabCDN;
	}

	public CDN_GAB_Outre_Mer setGabCDN(String gabCDN) {
		this.gabCDN = gabCDN;
		return this;
	}

	public String getGabSG() {
		return gabSG;
	}

	public CDN_GAB_Outre_Mer setGabSG(String gabSG) {
		this.gabSG = gabSG;
		return this;
	}

	public String getDab() {
		return dab;
	}

	public CDN_GAB_Outre_Mer setDab(String dab) {
		this.dab = dab;
		return this;
	}

	public String getHorsSite() {
		return horsSite;
	}

	public CDN_GAB_Outre_Mer setHorsSite(String horsSite) {
		this.horsSite = horsSite;
		return this;
	}

	public String getAgenceHtm() {
		return agenceHtm;
	}

	public CDN_GAB_Outre_Mer setAgenceHtm(String agenceHtm) {
		this.agenceHtm = agenceHtm;
		return this;
	}

	public String getAnneeScrap() {
		return anneeScrap;
	}

	public CDN_GAB_Outre_Mer setAnneeScrap(String anneeScrap) {
		this.anneeScrap = anneeScrap;
		return this;
	}
}
