package com.app.model;

import java.text.SimpleDateFormat;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="eovi", schema="scrap")
public class EOVI {

	@Id
	@SequenceGenerator(name="scrap.eovi_id_seq",
	                    sequenceName ="scrap.eovi_id_seq",
	                     allocationSize=1)
    @GeneratedValue( strategy=GenerationType.SEQUENCE,
                     generator="scrap.eovi_id_seq")
	  
	@Column(name="id",updatable=false)
	private int id;
	
	@Column(name="depsoumis", length=500)
	private String depSoumis;
	
	@Column(name="nbagences", length=4)
	private String nbAgences;
	
	@Column(name="villesoumise", length=500)
	private String villeSoumise;
	
	@Column(name="nomagence", length=500)
	private String nomAgence;
	
	@Column(name="adresse", length=500)
	private String adresse;
	
	@Column(name="tel", length=20)
	private String tel;
	
	@Column(name="horaires", length=500)
	private String horaires;
	
	@Column(name="agencehtm", columnDefinition="varchar")
	private String agenceHtm;
	
	@Column(name="annee_du_scrap" ,columnDefinition="varchar(4) DEFAULT date_part('year',CURRENT_DATE)")
	private String anneeScrap;
	
	public EOVI() {
		SimpleDateFormat f=null;
		Date d=new Date();
		f=new SimpleDateFormat("yyyy");
		anneeScrap=f.format(d).toString();
	}
	
	public int getId() {
		return id;
	}
	public EOVI setId(int id) {
		this.id = id;
		return this;
	}
	public String getDepSoumis() {
		return depSoumis;
	}
	public EOVI setDepSoumis(String depSoumis) {
		this.depSoumis = depSoumis;
		return this;
	}
	public String getNbAgences() {
		return nbAgences;
	}
	public EOVI setNbAgences(String nbAgences) {
		this.nbAgences = nbAgences;
		return this;
	}
	public String getVilleSoumise() {
		return villeSoumise;
	}
	public EOVI setVilleSoumise(String villeSoumise) {
		this.villeSoumise = villeSoumise;
		return this;
	}
	public String getNomAgence() {
		return nomAgence;
	}
	public EOVI setNomAgence(String nomAgence) {
		this.nomAgence = nomAgence;
		return this;
	}
	public String getAdresse() {
		return adresse;
	}
	public EOVI setAdresse(String adresse) {
		this.adresse = adresse;
		return this;
	}
	public String getTel() {
		return tel;
	}
	public EOVI setTel(String tel) {
		this.tel = tel;
		return this;
	}
	public String getHoraires() {
		return horaires;
	}
	public EOVI setHoraires(String horaires) {
		this.horaires = horaires;
		return this;
	}
	public String getAgenceHtm() {
		return agenceHtm;
	}
	public EOVI setAgenceHtm(String agenceHtm) {
		this.agenceHtm = agenceHtm;
		return this;
	}
	public String getAnneeScrap() {
		return anneeScrap;
	}
	public EOVI setAnneeScrap(String anneeScrap) {
		this.anneeScrap = anneeScrap;
		return this;
	}
}
