package com.app.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "table_banque_dom", schema = "scrap_tools")
public class TableBanqueDOM extends SuperTable{

	@Id
	@SequenceGenerator(name = "scrap_tools.table_banque_dom_id_seq", sequenceName = "scrap_tools.table_banque_dom_id_seq", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "scrap_tools.table_banque_dom_id_seq")
	private int id;
}
