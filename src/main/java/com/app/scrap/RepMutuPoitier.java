package com.app.scrap;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.app.model.Mutuelle_Poitier;

@Repository
public interface RepMutuPoitier extends JpaRepository<Mutuelle_Poitier,Integer>,RepositoryCustom<Mutuelle_Poitier> {

}
