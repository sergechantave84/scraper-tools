package com.app.service;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.Date;
import java.util.Scanner;

import javax.net.ssl.HttpsURLConnection;

import org.apache.commons.text.StringEscapeUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.app.model.BanqueCaraibe;
import com.app.scrap.RepBanqueCaraibe;
import com.app.utils.Parametres;

import fa.log.LogApp.LogScrap;
import fa.log.config.LogConst;

@Service
public class ServiceBanqueCaraibes extends Scraper {

	@Autowired
	RepBanqueCaraibe rep;
	
	@Override
	public void saveItem(String... args) {
		BanqueCaraibe bc = new BanqueCaraibe();
		bc.setVille(args[0]).setNom(args[1]).setAdresse(args[2]).setHoraires(args[3]).setTel_com(args[4])
				.setTel_pro(args[5]);
	    	
        rep.save(bc);
	}

	@Override
	public void scrap() throws Exception {
		Scanner sc=new Scanner(new BufferedInputStream(new FileInputStream(new File(Parametres.REPERTOIRE_DATA_SOURCES+"lien_caraibes_banques.txt"))));
		LogScrap log = new LogScrap("BanqueCaraibesLog.txt", LogConst.TYPE_LOG_FILE);
		int currentLine = ((new File(LogConst.STORE_LOG_PATH + LogConst.PREFIX_LOG_FILE + "_BanqueCaraibesLog.txt"))
				.exists()) ? Integer.parseInt(log.readLog().getLastLineProcessed().split("\t")[4]) : 1;

		int counter = 1;
		
		while(sc.hasNextLine()) {
			
			String lines[]= sc.nextLine().split("\t");
			if(counter> currentLine) {
				System.out.println(lines[1]);
				URL url= new URL(lines[1]);
				HttpsURLConnection con=(HttpsURLConnection) url.openConnection();
				con.setRequestMethod("GET");
				con.setReadTimeout(600000);
				con.setConnectTimeout(600000);
				con.setUseCaches(false);
				con.setAllowUserInteraction(false);
				con.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
				con.setRequestProperty("Cache-Control","no-cache,no-store,must-revalidate");
				int responceCode = con.getResponseCode();
				BufferedReader br=null;
				String pageSource="";
				StringBuilder strB=new StringBuilder();
				if (responceCode == HttpsURLConnection.HTTP_OK) {
				     br= new BufferedReader(new InputStreamReader(con.getInputStream(),"UTF-8"));
				     while((pageSource=br.readLine())!=null) {
							strB.append(pageSource);
						}
				}
				
				pageSource=StringEscapeUtils.unescapeHtml4(strB.toString());
				
				Document doc=Jsoup.parse(pageSource);
				Element root=doc.selectFirst("#contenu");
				String ville="";
				if(root!=null) {
					ville=lines[0];
					String nom="";
					String adresse="";
					String horaires="";
					String telCom="";
					String telPro="";
					
					
					Element nameElement= root.selectFirst("section.paragraph.paragraph--type--banner.paragraph--view-mode--default.c-heading> "
							+ "div.u-centered > div.c-heading__block > div > h1");
					nom=(nameElement!=null)? this.normalize(nameElement.text()) :"";
					System.out.println("nom "+nom);
					
					Element adresseElement=root.selectFirst("section.paragraph.paragraph--type--banner.paragraph--view-mode--default.c-heading > div.u-centered > "
							+ "div.c-heading__block > div > div.u-text-margin--none.c-heading__text > div > p:nth-child(1)");
					adresse=(adresseElement!=null)? this.normalize(adresseElement.text()) :"";
					System.out.println("adresse "+adresse);
					
					Element horairesElememnt= root.selectFirst("div.u-padding-vertical--lg > div > div:nth-child(1) > "
							+ "div > div.c-sticker-agence__content > div");
					horaires=(horairesElememnt!=null) ?horairesElememnt.text() :"";
					System.out.println("horaires "+horaires);
					
					Element telComElement=root.selectFirst("div.u-padding-vertical--lg> "
							+ "div > div:nth-child(2) > section > div.c-sticker-agence__content > div.u-margin-bottom--sm > span");
					telCom=(telComElement!=null)? telComElement.text().replaceAll("[^0-9]",""): "";
					System.out.println("tel com "+telCom);
					
					Element telProElement=root.selectFirst("div.u-padding-vertical--lg > "
							+ "div > div:nth-child(3) > section > div.c-sticker-agence__content > div.u-margin-bottom--sm > span");
					telPro=(telProElement!=null)? telProElement.text().replaceAll("[^0-9]", ""):"";
					System.out.println("tel pro"+telPro);
					
					//bc.setVille(args[0]).setNom(args[1]).setAdresse(args[2]).setHoraires(args[3]).setTel_com(args[4])
					//.setTel_pro(args[5]);
					this.saveItem(ville,nom,adresse,horaires,telCom,telPro);
					
					
			
				}
				log.writeLog(new Date(), ville, "no_error", 0, counter, 0);
			}
			counter++;
		}

	}

}
