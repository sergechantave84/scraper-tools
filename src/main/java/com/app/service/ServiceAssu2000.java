package com.app.service;

import java.io.BufferedInputStream;
import java.io.BufferedReader;

import java.io.File;
import java.io.FileInputStream;

import java.io.InputStreamReader;

import java.net.URL;

import java.util.Date;

import java.util.Scanner;

import javax.net.ssl.HttpsURLConnection;

import org.apache.commons.text.StringEscapeUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.app.model.Assu2000;
import com.app.scrap.RepAssu2000;

import com.app.utils.Parametres;

import fa.log.LogApp.LogScrap;
import fa.log.config.LogConst;

@Service
public class ServiceAssu2000 extends Scraper {

	@Autowired
	private RepAssu2000 rep;

	String nomSITE = "ASSU2000";
	String dataSourceFileName = "lien_Assu2000";
	String dossierRESU = Parametres.REPERTOIRE_RESULT + "\\ASSU\\" + nomSITE;;
	String url_accueil = "https://agence.assu2000.fr/fr/ile-de-france/arrondissement-de-paris";

	int NbAgences;
	int NbAgences_page;
	int nb_pages;

	int num_page;
	int nbRecuperes;
	int parcoursficval;
	int Resultats;

	@Override
	public void saveItem(String... args) {
		Assu2000 assu2000 = new Assu2000();
		assu2000.setDep(args[0]).setLienAgence(args[1]).setNomAgence(args[2]).setAdresse(args[3]).setTel(args[4])
				.setHoraires(args[5]);
		rep.save(assu2000);

	}

	public void scrap() throws Exception {

		Scanner sc = new Scanner(new BufferedInputStream(
				new FileInputStream(new File(Parametres.REPERTOIRE_DATA_SOURCES + dataSourceFileName + ".txt"))));

		LogScrap log = new LogScrap("AssuranceAssu2000Log.txt", LogConst.TYPE_LOG_FILE);
		int currentLine = ((new File(LogConst.STORE_LOG_PATH + LogConst.PREFIX_LOG_FILE + "_AssuranceAssu2000Log.txt"))
				.exists()) ? currentLine = Integer.parseInt(log.readLog().getLastLineProcessed().split("\t")[4]) : 1;

		parcoursficval = 1;

		while (sc.hasNextLine()) {
			String[] lines = sc.nextLine().split("\\t");

			if (parcoursficval > currentLine) {
				System.out.println(lines[1]);
				URL url = new URL(lines[1]);
				HttpsURLConnection con = (HttpsURLConnection) url.openConnection();
				con.setRequestMethod("GET");
				con.setReadTimeout(600000);
				con.setConnectTimeout(60000);
				BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream(), "UTF-8"));
				String pageSource;
				StringBuilder strB = new StringBuilder();
				while ((pageSource = in.readLine()) != null) {
					strB.append(pageSource);
				}
				pageSource = StringEscapeUtils.unescapeHtml4(strB.toString());
				Document doc = Jsoup.parse(pageSource);
				Element root = doc.selectFirst("body > div.em-page > div > section");
				if (root != null) {
					String nomAgence = "";
					String adresse = "";
					String tel = "";
					String horaires = "";
					Element agencyDetailsElement = root
							.selectFirst("div.em-details__poi-card.agent.em-poi-card.em-poi-card--details");
					Element nameElement = doc.selectFirst("body > div.em-page > header > div.em-header__title > h1");
					nomAgence = (nameElement != null) ? this.normalize(nameElement.text().toUpperCase()) : "";
					System.out.println("nom agence : " + nomAgence);

					Element adresseElement = agencyDetailsElement
							.selectFirst("div.em-details__poi-card-bloc > address");
					adresse = (adresseElement != null) ? this.normalize(adresseElement.text()) : "";
					System.out.println("adresse : " + adresse);

					Element telElement = agencyDetailsElement.selectFirst("div.em-poi-card__buttons > div > a");
					tel = (telElement != null) ? telElement.attr("href").replaceAll("[^0-9]", "") : "";
					System.out.println("telephone : "+ tel);

					Element scheduleElement = doc.selectFirst("body > div.em-page > div > section > section > div");
					horaires = (scheduleElement != null) ? this.normalize(scheduleElement.text()) : "";
					System.out.println("horaires : "+ horaires);

					//assu2000.setDep(args[0]).setLienAgence(args[1]).setNomAgence(args[2]).setAdresse(args[3]).setTel(args[4]);
					this.saveItem(lines[0], lines[1], nomAgence, adresse, tel, horaires);
					
					System.out.println("------------------------------------------------\n");

				}

				log.writeLog(new Date(), lines[1], "no_error", NbAgences, parcoursficval, 0);
			}

			parcoursficval++;
		}

	}

}
