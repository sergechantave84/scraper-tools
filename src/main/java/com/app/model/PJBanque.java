package com.app.model;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;

@MappedSuperclass
public class PJBanque {

	@Column(name="denomination",columnDefinition = "varchar")
	private String denomination;
	
	@Column(name="adresse",columnDefinition = "varchar")
	private String adresse;
	
	
	@Column(name="orias",columnDefinition = "varchar")
	private String orias;
	
	@Column(name="prestation",columnDefinition = "varchar")
	private String prestation;
	
	@Column(name="DAB",columnDefinition = "varchar")
	private String dab;
	
	@Column(name="tel",columnDefinition = "varchar")
	private String tel;
	
	@Column(name="activite",columnDefinition = "varchar")
	private String activite;
	
	
	
	public String getDenomination() {
		return denomination;
	}

	public PJBanque setDenomination(String denomination) {
		this.denomination = denomination;
		return this;
	}

	public String getAdresse() {
		return adresse;
	}

	public PJBanque setAdresse(String adresse) {
		this.adresse = adresse;
		return this;
	}

	public String getOrias() {
		return orias;
	}

	public PJBanque setOrias(String orias) {
		this.orias = orias;
		return this;
	}

	public String getPrestation() {
		return prestation;
	}

	public PJBanque setPrestation(String prestation) {
		this.prestation = prestation;
		return this;
	}

	public String getDab() {
		return dab;
	}

	public PJBanque setDab(String dab) {
		this.dab= dab;
		return this;
	}

	public String getTel() {
		return tel;
	}

	public PJBanque setTel(String tel) {
		this.tel = tel;
		return this;
	}

	public String getActivite() {
		return activite;
	}

	public PJBanque setActivite(String activite) {
		this.activite = activite;
		return this;
	}
}
