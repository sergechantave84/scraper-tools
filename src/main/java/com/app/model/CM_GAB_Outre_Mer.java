package com.app.model;

import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="banque_cm_gab_dep_outre_mer", schema="scrap")
public class CM_GAB_Outre_Mer {

	@Id
	@SequenceGenerator(name="scrap.banque_cm_gab_dep_outre_mer_id_seq",
	                    sequenceName ="scrap.banque_cm_gab_dep_outre_mer_id_seq",
	                     allocationSize=1)
    @GeneratedValue( strategy=GenerationType.SEQUENCE,
                     generator="scrap.banque_cm_gab_dep_outre_mer_id_seq")
	  
	@Column(name="id",updatable=false)
	private int id;
	
	@Column(name="communsoumise", columnDefinition="varchar")
	private String communeSoumise;
	
	@Column(name="nbagences", columnDefinition="varchar")
	private String nbAgences;
	
	@Column(name="nomweb", columnDefinition="varchar")
	private String nomWeb;
	
	@Column(name="adresse", columnDefinition="varchar")
	private String adresse;
	
	@Column(name="gabcic", columnDefinition="varchar")
	private String gabCIC; 
	
	@Column(name="gabcm", columnDefinition="varchar")
	private String gabCM;
	
	@Column(name="agencehtm", columnDefinition="varchar")
	private String agenceHtm;
	
	@Column(name="annee_du_scrap" ,columnDefinition="varchar(4) DEFAULT date_part('year',CURRENT_DATE)")
	private String anneeScrap;

	public CM_GAB_Outre_Mer () {
		LocalDate ld = new Date().toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
		anneeScrap = String.valueOf(ld.getYear() + 1);
	}

	public int getId() {
		return id;
	}

	public CM_GAB_Outre_Mer  setId(int id) {
		this.id = id;
		return this;
	}

	public String getCommuneSoumise() {
		return communeSoumise;
	}

	public CM_GAB_Outre_Mer  setCommuneSoumise(String communeSoumise) {
		this.communeSoumise = communeSoumise;
		return this;
	}

	public String getNbAgences() {
		return nbAgences;
	}

	public CM_GAB_Outre_Mer  setNbAgences(String nbAgences) {
		this.nbAgences = nbAgences;
		return this;
	}

	public String getNomWeb() {
		return nomWeb;
	}

	public CM_GAB_Outre_Mer  setNomWeb(String nomWeb) {
		this.nomWeb = nomWeb;
		return this;
	}

	public String getAdresse() {
		return adresse;
	}

	public CM_GAB_Outre_Mer  setAdresse(String adresse) {
		this.adresse = adresse;
		return this;
	}

	public String getGabCIC() {
		return gabCIC;
	}

	public CM_GAB_Outre_Mer  setGabCIC(String gabCIC) {
		this.gabCIC = gabCIC;
		return this;
	}

	public String getGabCM() {
		return gabCM;
	}

	public CM_GAB_Outre_Mer  setGabCM(String gabCM) {
		this.gabCM = gabCM;
		return this;
	}

	public String getAgenceHtm() {
		return agenceHtm;
	}

	public CM_GAB_Outre_Mer  setAgenceHtm(String agenceHtm) {
		this.agenceHtm = agenceHtm;
		return this;
	}

	public String getAnneeScrap() {
		return anneeScrap;
	}

	public CM_GAB_Outre_Mer  setAnneeScrap(String anneeScrap) {
		this.anneeScrap = anneeScrap;
		return this;
	}
}
