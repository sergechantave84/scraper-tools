package com.app.scrap;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.app.model.HSBC;

@Repository
public interface RepHSBC extends JpaRepository<HSBC, Integer>,RepositoryCustom<HSBC>{

}
