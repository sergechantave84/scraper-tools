package com.app.scrap;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.app.model.CIC_Outre_Mer;

@Repository
public interface RepCicOutreMer extends JpaRepository<CIC_Outre_Mer, Integer>, RepositoryCustom<CIC_Outre_Mer> {

}
