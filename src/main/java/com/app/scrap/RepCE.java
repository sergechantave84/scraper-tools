package com.app.scrap;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.app.model.CE;

@Repository
public interface RepCE extends JpaRepository<CE,Integer>,RepositoryCustom<CE>{

}
