package com.app.model;

import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;





@Entity
@Table(name="Aesio", schema="scrap")
public class Aesio {

	@Id
	@SequenceGenerator(name="scrap.Aesio_id_seq",
	                   sequenceName ="scrap.Aesio_id_seq",
	                   allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE,
	                generator = "scrap.Aesio_id_seq")
	private int id;
	
	
	@Column(name="nom_web", columnDefinition = "varchar")
	private String nomWeb;
	
	@Column(name="departement_soumise", columnDefinition = "varchar")
	private String depSoumis;
	
	@Column (name="nombre_agence", columnDefinition = "varchar")
	private String nombreAgence;
	
	@Column(name="adresse", columnDefinition = "varchar")
	private String adresse;
	
	@Column(name="telephone", columnDefinition = "varchar")
	private String telephone;
	
	@Column(name="horaires", columnDefinition = "varchar")
	private String horaires;
	
	@Column(name="lien_agence", columnDefinition = "varchar")
	private String lienAgence;
	
	@Column(name="agence_HTML",columnDefinition = "varchar")
	private String agenceHTML;
	
	@Column(name="annee_du_Scrap",columnDefinition = "varchar")
	private String anneeScrap;

	public Aesio() {
		LocalDate ld= new Date().toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
		anneeScrap=String.valueOf(ld.getYear()+1);
		
	}
	public String getNomWeb() {
		return nomWeb;
	}

	public Aesio setNomWeb(String nomWeb) {
		this.nomWeb = nomWeb;
		return this;
	}

	public String getNombreAgence() {
		return nombreAgence;
	}

	public Aesio setNombreAgence(String nombreAgence) {
		this.nombreAgence = nombreAgence;
		return this;
	}

	public String getAdresse() {
		return adresse;
	}

	public Aesio setAdresse(String adresse) {
		this.adresse = adresse;
		return this;
	}

	public String getTelephone() {
		return telephone;
	}

	public Aesio setTelephone(String telephone) {
		this.telephone = telephone;
		return this;
	}

	public String getHoraires() {
		return horaires;
	}

	public Aesio setHoraires(String horaires) {
		this.horaires = horaires;
		return this;
	}

	public String getAgenceHTML() {
		return agenceHTML;
	}

	public Aesio setAgenceHTML(String agenceHTML) {
		this.agenceHTML = agenceHTML;
		return this;
	}

	public String getAnneeScrap() {
		return anneeScrap;
	}

	public Aesio setAnneeScrap(String anneeScrap) {
		this.anneeScrap=anneeScrap;
		return this;
	}
	public String getDepSoumis() {
		return depSoumis;
	}
	public Aesio setDepSoumis(String depSoumis) {
		this.depSoumis = depSoumis;
		return this;
	}
	public String getLienAgence() {
		return lienAgence;
	}
	public Aesio setLienAgence(String lienAgence) {
		this.lienAgence = lienAgence;
		return this;
	}
	
	
}
