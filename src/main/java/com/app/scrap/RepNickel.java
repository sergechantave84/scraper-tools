package com.app.scrap;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.app.model.Nickel;

@Repository
public interface RepNickel extends JpaRepository<Nickel, Integer>,RepositoryCustom<Nickel>{

}
