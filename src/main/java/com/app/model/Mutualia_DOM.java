package com.app.model;

import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="mutualia_dom", schema="scrap")
public class Mutualia_DOM {
	
	@Id
	@SequenceGenerator(name="scrap.mutualia_dom_id_seq",
	                    sequenceName ="scrap.mutualia_dom_id_seq",
	                     allocationSize=1)
    @GeneratedValue( strategy=GenerationType.SEQUENCE,
                     generator="scrap.mutualia_dom_id_seq")
	  
	@Column(name="id",updatable=false)
	private int id;
	
	@Column(name="nbagences",  columnDefinition="varchar")
	private String nbAgences;
	
	@Column(name="lien_agence",  columnDefinition="varchar")
	private String lienAgence;
	
	@Column(name="nom_agence",  columnDefinition="varchar")
	private String nomAgence;
	
	@Column(name="adresse",  columnDefinition="varchar")
	private String adresse;
	
	@Column(name="tel",  columnDefinition="varchar")
	private String tel;
	
	@Column(name="horaires",  columnDefinition="varchar")
	private String horaires;
	
	@Column(name="horaireshtm", columnDefinition="varchar")
	private String horairesHtm;
	
	@Column(name="agencehtm", columnDefinition="varchar")
	private String agenceHtm;
	
	@Column(name="annee_du_scrap" ,columnDefinition="varchar(4) DEFAULT date_part('year',CURRENT_DATE)")
	private String anneeScrap;

	public Mutualia_DOM() {
		LocalDate ld = new Date().toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
		anneeScrap = String.valueOf(ld.getYear() + 1);
	}
	
	public int getId() {
		return id;
	}

	public Mutualia_DOM setId(int id) {
		this.id = id;
		return this;
	}

	public String getNbAgences() {
		return nbAgences;
	}

	public Mutualia_DOM setNbAgences(String nbAgences) {
		this.nbAgences = nbAgences;
		return this;
	}

	public String getLienAgence() {
		return lienAgence;
	}

	public Mutualia_DOM setLienAgence(String lienAgence) {
		this.lienAgence = lienAgence;
		return this;
	}

	public String getNomAgence() {
		return nomAgence;
	}

	public Mutualia_DOM setNomAgence(String nomAgence) {
		this.nomAgence = nomAgence;
		return this;
	}

	public String getAdresse() {
		return adresse;
	}

	public Mutualia_DOM setAdresse(String adresse) {
		this.adresse = adresse;
		return this;
	}

	public String getTel() {
		return tel;
	}

	public Mutualia_DOM setTel(String tel) {
		this.tel = tel;
		return this;
	}

	public String getHoraires() {
		return horaires;
	}

	public Mutualia_DOM setHoraires(String horaires) {
		this.horaires = horaires;
		return this;
	}

	public String getHorairesHtm() {
		return horairesHtm;
	}

	public Mutualia_DOM setHorairesHtm(String horairesHtm) {
		this.horairesHtm = horairesHtm;
		return this;
	}

	public String getAgenceHtm() {
		return agenceHtm;
	}

	public Mutualia_DOM setAgenceHtm(String agenceHtm) {
		this.agenceHtm = agenceHtm;
		return this;
	}

	public String getAnneeScrap() {
		return anneeScrap;
	}

	public Mutualia_DOM setAnneeScrap(String anneeScrap) {
		this.anneeScrap = anneeScrap;
		return this;
	}

}
