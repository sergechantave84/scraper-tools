import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import com.app.scrap.Scrapper1Application;
import com.app.service.ServiceBecmGab;

@RunWith(SpringRunner.class)
@SpringBootTest(classes=Scrapper1Application.class)
@TestPropertySource(locations = "classpath:test_application.properties")
public class TestBECM_GAB {

	@Autowired
	ServiceBecmGab srv;
	
	@Test
	public void test() throws Exception {
		srv.setup();
		srv.scrap();
	}
}
